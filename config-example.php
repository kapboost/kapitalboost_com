<?php
ini_set("display_errors", true);
error_reporting(E_ALL & ~E_WARNING & ~E_PARSE & ~E_NOTICE & ~E_DEPRECATED);

#######################################################################
# Database Configuration
///////////////////////////////////////////////////////////////////////
# Server settings (don't forget to uncomment if will update this file to server)
define("SQL_USER", "kapitalboost");
define("SQL_PASSWORD", 'kapital2016');
define("SQL_HOST", "localhost");
define("SQL_DB", "kapitalboost"); //production database
// define("SQL_DB", "state_kapitalboost"); //staging database
define("SQL_DEBUG", FALSE);

#local settings (don't forget to comment if will update this file to server)
// define("SQL_USER", "root");
// define("SQL_PASSWORD", '');
// define("SQL_HOST", "localhost");
// define("SQL_DB", "kapitalboost");
// define("SQL_DEBUG", FALSE);

#######################################################################
//$serverdir = "http://90.0.0.253/basic/images";
$session_length = 7;
$CS_LOGIN_TIMEOUT = 120000000000000000000000000000000000000000000000; // in sec ==> not confirmed yet 04/08/2003
#######################################################################
# Constant Definition
///////////////////////////////////////////////////////////////////////
# Server settings (don't forget to uncomment if will update this file to server)
define("PROJECT_NAME", "Kapitalboost.com");
define("SERVER_BASE", "https://kapitalboost.com/");
define("SERVER_BASE_SECURE", "https://kapitalboost.com/");
define ("OVERWRITE_LOGIN_SESSION", TRUE);

#local settings (don't forget to comment if will update this file to server)
// define("PROJECT_NAME", "Kapitalboost.com");
// define("SERVER_BASE", "http://kapboost.com/");
// define("SERVER_BASE_SECURE", "https://kapboost.com/");
// define ("OVERWRITE_LOGIN_SESSION", TRUE);


#######################################################################
# Pager Setting
///////////////////////////////////////////////////////////////////////
define('PAGER_SCROLL_PAGE', 10);
define('PAGER_PER_PAGE', 9);
define('PAGER_PER_PAGE_CONSOLE', 10);
//define('PAGER_PER_PAGE_PRODUCTS', 12);
define('PAGER_INACTIVE_PAGE_TAG', 'class="active"');
define('PAGER_PREVIOUS_PAGE_TEXT', '&lt;');
define('PAGER_NEXT_PAGE_TEXT', '&gt;');
define('PAGER_FIRST_PAGE_TEXT', '&lt;&lt;');
define('PAGER_LAST_PAGE_TEXT', '&gt;&gt;');
define('PAGER_TAG_CONTAINER', 'li');

define('SMTP_HOST', '');
define('SMTP_USERNAME', '');
define('SMTP_PASSWORD', '');
define('EMAIL_FROM_NAME', 'Kapital Boost');
define('EMAIL_FROM', 'admin@kapitalboost.com');
define('ADMIN_EMAIL', ''); 
#######################################################################
# Website Configuration
///////////////////////////////////////////////////////////////////////

define('SMARTY_DATETIME_FORMAT', '%d/%m/%Y %l:%M%p');
define('SMARTY_DATE_FORMAT', '%d/%m/%Y');
define('SMARTY_TIME_FORMAT', '%l:%M%p');

$config['group_status_options'] = array( 0=>' Offline', 1=>' Online' );

$config['member_type_options'] = array( 0=>' Non Member', 1=>' Individual', 2=>' Corporate' );
$config['member_title_options'] = array( 'Mr'=>'Mr', 'Mrs'=>'Mrs', 'Ms'=>'Ms', 'Mdm'=>'Mdm', 'Dr'=>'Dr', 'Prof'=>'Prof', 'Corporate'=>'Corporate' );
$config['member_gender_options'] = array( 0=>' all', 1=>' Male', 2=>' Female' );
$config['member_marital_status_options'] = array( 1=>' Married', 2=>' Single' );
$config['member_religion_options'] = array( 1=>'Buddhist', 2=>'Christian', 3=>'Muslim', 4=>'Taoism', 5=>'Hinduism', 6=>'Indian', 9=>'Others' );

$config['banner_status_options'] = array( 0=>' Offline', 1=>' Online' );
$config['testimonial_status_options'] = array( 0=>' Offline', 1=>' Online' );

$config['project_type_options'] = array( 'sme'=>'SME Funding', 'donation'=>'Donation', 'private'=>'Private Crowdfunding' );
$config['payment_status'] = array(0=>'Pending', '1' => 'Success', '2' => 'Failed', '9'=>'Cancel');

$config['provider'] = array('Facebook');

#USED ON QUERY FRONT SITE
define('OFFLINE', 0);
define('ONLINE', 1);
define('SHELVED', 2);


?>
