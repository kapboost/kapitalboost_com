<?php

if ($_SERVER['HTTP_HOST'] == 'www.kapitalboost.com'){
      header("Location: http://kapitalboost.com".$_SERVER['REQUEST_URI']);
}
date_default_timezone_set('Asia/Singapore'); // set timezone in php
//REQUEST_URI

session_start();

ini_set("display_errors", true);
error_reporting(E_ALL & ~E_NOTICE);

$root = realpath(dirname(__FILE__)) . '/';
define('DOC_ROOT', $root);

if (isset($_GET['debug']) && $_GET['debug'] == 1) {
       print_r($_SESSION);
}

include_once(DOC_ROOT . "config.php");

include_once(DOC_ROOT . 'libs/smarty/smarty-3/Smarty.class.php');
include_once(DOC_ROOT . 'libs/kgPager.class.php');
include_once(DOC_ROOT . "Classes/core.DB.php");
include_once(DOC_ROOT . "libs/DVSecurity.php");
include_once(DOC_ROOT . 'libs/Metakeywords.php');

$currdatetime = date("Y-m-d H:i:s");

$sys_msg = array();

//#######################################################################
//#Database Connection
///////////////////////////////////////////////////////////////////////
$db = new DB();
//$sqllink = $db->connect(SQL_HOST, SQL_USER, SQL_PASSWORD);
$db->connect(SQL_HOST, SQL_USER, SQL_PASSWORD, SQL_DB);

//#######################################################################
//# CONSOLESPINE - REQUIRES db.class.php and an open connection called $db
///////////////////////////////////////////////////////////////////////
$smarty = new smarty();

$smarty->compile_check = true;
$smarty->debugging = false;

#DV9 Security Class
$DVSecurity = new DVSecurity;
$DVSecurity->clean_all();


include_once(DOC_ROOT . "Classes/class.DV_Session.php");
include_once(DOC_ROOT . "Classes/modules/class.Front.php");
include_once(DOC_ROOT . "Classes/modules/class.Blocks.php");

$DV_Session = new DV_Session($session_length);

$session = "";
$cookie_id = "";

if (!isset($_SESSION['cookie']['cookie_id'])) {

       //*****************************************
       // session defined
       if (isset($_SESSION['cookie']['cookie']) != "") {
              $session = $_SESSION['cookie']['cookie'];
       }

       if ($session != '') {
              if (!$DV_Session->ValidSession($session)) {
                     $DV_Session->MakeSession();
              }
       } else {
              $DV_Session->MakeSession();
       }

       if ($_SESSION['cookie']['cookie_id'] < 1 || $_SESSION['cookie']['cookie'] == '') {
              $sys_msg['error'][] = "Invalid cookie ID or session[{$_SESSION[cookie][cookie_id]}][{$_SESSION[cookie][cookie]}]";
              $loadmodule = 0;
       } else {
              $cookie_id = $_SESSION['cookie']['cookie_id'];
              $session = $_SESSION['cookie']['cookie'];
       }
} else {
       $cookie_id = $_SESSION['cookie']['cookie_id'];
       $session = $_SESSION['cookie']['cookie'];
}

//No Ajax operation
$block = new Blocks;





$smarty->assign('ContentMiddle', $block->LoadContentMiddle());

//$page_title ="Islamic Crowdfunding - Business Finance | Kapital Boost";
$page_title = "Kapital Boost: Islamic crowdfunding - Business Finance";
//$page_title = $_SESSION["page_title"];
//$meta_desc = $_SESSION['meta_desc'];
//$meta_desc = "Kapital Boost is a Singapore-based, Shariah compliant crowdfunding platform that helps small businesses grow BIG.  Visit our website, for enquiries call +65 9477 3150";
$meta_desc = "Kapital Boost is Asia’s first Islamic P2P crowdfunding platform for SMEs. Learn more about getting funding or investing ethically with us today.";

$meta_keyword = "Kapital Boost, Islamic crowdfunding, Islamic Finance, Islamic Economy, Invoice financing, Asset purchasing, Business finance, Murabaha";
$image_url = "";

if (isset($_SESSION['meta_keyword']) && !empty($_SESSION['meta_keyword'])) {
       $meta_keyword = $_SESSION['meta_keyword'];

       unset($_SESSION['meta_keyword']);
}

if (isset($_SESSION['meta_desc']) && !empty($_SESSION['meta_desc'])) {
       $meta_desc = $_SESSION['meta_desc'];

       unset($_SESSION['meta_desc']);
}

if (isset($_SESSION['page_title']) && !empty($_SESSION['page_title'])) {
       $page_title = $_SESSION['page_title'];

       unset($_SESSION['page_title']);
}

if (isset($_SESSION['image_url']) && !empty($_SESSION['image_url'])) {
       $image_url = $_SESSION['image_url'];

       unset($_SESSION['image_url']);
}
list($country, ) = $db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");

foreach ($country as $row) {
       $member_country_options[$row['name']] = $row['name'];
}

$smarty->assign("member_country_options", $member_country_options);

$smarty->assign('meta_keyword', $meta_keyword);
$smarty->assign('meta_desc', $meta_desc);
$smarty->assign('page_title', $page_title);
$smarty->assign('image_url', $image_url);

$smarty->assign('Header', $block->LoadHeader());
$smarty->assign('Footer', $block->LoadFooter());
//$smarty->assign('HomeContent', $block->LoadHomeContent());

if ($_GET['class'] == '') {
       $smarty->assign('show_gallery', 1);
}
if (isset($_GET["email"]) && $_GET["email"] != "failed") {
       $smarty->assign("email", $_GET["email"]);
       $smarty->assign("username", $_GET["username"]);
} else if (isset($_GET["email"]) && $_GET["email"] == "failed") {
       $smarty->assign("email", "0");
} else {
       $smarty->assign("email", "");
       $smarty->assign("username", "");
}

//if(isset($_SESSION['DV9ADMIN']) || $_GET['DV9ADMIN'] == 1 || $_GET['class'] == "Ipay88" || $_GET['class'] == "Order"){
$_SESSION['DV9ADMIN'] = 1;




$smarty->display("TPL_Mainlayout.tpl");
?>
