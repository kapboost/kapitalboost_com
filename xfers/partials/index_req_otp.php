<!-- Do not Edit ^ -->
<!DOCTYPE html>
<html>
<head>
  <title>Xfers Payments</title>
  <link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="/assets/css/new-kp-main.css" />
<link rel="stylesheet" href="/assets/css/new-kp-home.css" />
<link rel="stylesheet" href="/assets/css/kp-about.css" />
<link rel="stylesheet" href="/assets/css/kp-default.css" />
<link rel="stylesheet" href="/assets/css/kp-blog.css" />
<link rel="stylesheet" href="/assets/css/kp-event.css" />
<link rel="stylesheet" type="text/css" href="/payment_design/filesNew/style/gaya.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body ng-app="xfers" ng-controller="xfersCtrl">

<center>
  <!-- start header -->
<div id="menu">
    <div id="menu-back"></div>
            <center>
                                <ul class="newMenu">
 <li><a href="https://kapitalboost.com/dashboard/payment-history"> Portfolio </a></li>
<li><a href="https://kapitalboost.com/dashboard/profile"> Manage Profile </a></li>
                      
                       
                    </ul>
            <ul class="dasUl">
                         <li>
                            <a href="dashboard/payment-history" style="cursor:pointer;">
                               Payment History
                            </a>
                         </li>
<!--
                         <li>
                            <a href="dashboard/invest-history" style="cursor:pointer;">
                              Portfolio
                            </a>
                         </li>
-->
                         <li>
                            <a href="dashboard/profile"style="cursor:pointer;">
                                Profile
                            </a>
                         </li>
                     </ul>
            </center>
    <div id="menu-front">
        <div id="menu-front-inner">
            <div id="menu-logo">
                <a href="">
                <img alt="Kapital Boost Logo" id="menu-logo-img" src="/payment_design/filesNew/images/xfersLogo.png" />
                </a>
            </div>
            <ul id="kb-menu">
<li><a href="">home</a>
                </li>
                <li><a href="about">About</a>
                </li>
                <li><a href="campaign/">Campaigns</a>
                </li>
                <li><a id="eth-log">Get Funded</a>
                </li>
                <li><a href="how-it-works">How It Works</a>
                </li>  
                                <li><a id="eth-log" class="loginRegisMenu" style="color: #609edf;">Login</a> 
                </li>
                <li><a id="reg" class="loginRegisMenu" style="color: #609edf;">Register</a>
                </li>
                            </ul>
            <div id="rwd-menu">
                <div id="rwd-menu-inner">
                    <div class="rwd-menu-each"></div>
                    <div class="rwd-menu-each"></div>
                    <div class="rwd-menu-each"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="logpopupWrap">
 <div class="logpopupInner">
  <div class="logpopupClose">
   <span class="glyphicon glyphicon-remove"></span>
  </div>
  <div class="logpopupTitle">
   Are you sure want to logout?
  </div>
  <ul class="logpopupYesNo">
   <li><a href="logout">Yes</a></li>
   <li><a id="logpopupClose">No</a></li>
  </ul>
 </div>
</div>

 
    <script>
$(document).ready(function(){

 $('#testbro').click(function(){
  $('.logpopupWrap').fadeIn(500);
 });

 $('#logpopupClose').click(function(){
  $('.logpopupWrap').fadeOut(500);
 });

 $('.logpopupClose').click(function(){
  $('.logpopupWrap').fadeOut(500);
 });
});

/*
$("#dialog").dialog({
   autoOpen: false,
   modal: true,
   resizable: false,
   height: "auto",
   width: 400,
   buttons : {
        "Yes" : function() {
            window.location.href="https://kapitalboost.com/logout";           
        },
        "No" : function() {
          $(this).dialog("close");
        }
      }
    });

$("#testbro").on("click", function(e) {
    e.preventDefault();
    $("#dialog").dialog("open");
});
*/
    </script>
  <!-- end header -->



  <!-- start xfers otp -->
  <div class="xfersOtp">
    <div class="XOInner">
      <div class="XOLogo">
        <img src="filesNew/images/xfersLogo.png" />
      </div>
      <div class="XOTitle">
        <p>
          Verify your account
        </p>
      </div>
      <div class="XOForm">
        <form ng-submit="xfersToken()">
          <input type="text" ng-model="otp" placeholder="Please enter your one time password" />
          <button>
            Verify
          </button>
        </form>
      </div>
    </div>
  </div>
  <!-- end x fers otp -->
</center>



  <!-- start footer -->
    <div id="footer">
    <div id="footer-inner">
        <div id="footer-main">
            <div id="footer-main-1">
                <h3 class="foot-header">About</h3>
                <p><a href="about">About</a></p>
                <p><a href="how-it-works">How it works</a></p>
                <p><a href="media">Media</a></p>                            
            </div>
            <div id="footer-main-2">
                <h3 class="foot-header">General</h3>
                <p><a href="campaign/">Campaigns</a></p>
                <p><a href="blog">Blog</a></p>
                <p><a href="partners">Partners</a></p>
            </div>
            <div id="footer-main-3">
                <h3 class="foot-header">Support</h3>
                                <p><a id="eth-log" style="cursor:pointer;">Get Funded</a></p>
                                <p><a href="faq">FAQs</a></p>
                <p><a href="contact-us">Contact Us</a></p>
            </div>
            <div id="footer-main-5">
                <h3 class="foot-header">Legal</h3>
                <p><a href="https://kapitalboost.com/legal#term">Term of use</a></p>
                <p><a href="https://kapitalboost.com/legal#privacy">Privacy Policy</a></p>
                <p><a href="https://kapitalboost.com/legal#risk">Risk Statement</a></p>
            </div>
            <div id="footer-main-4">
                <h3 class="foot-header foot-header-2">Follow Us</h3>
                
                <p>
                    <a class="sc-fb-icon" href="https://www.facebook.com/kapitalboost" target="_blank"><img alt="" src="../assets/images/facebook.png" /></a>
                    <a class="sc-ln-icon" href="https://www.linkedin.com/company/6597611?trk=tyah&trkInfo=clickedVertical:company,clickedEntityId:6597611,idx:1-1-1,tarId:1440685694219,tas:kapital%20boost" target="_blank"><img alt="" src="../assets/images/linkedin.png" /></a>
                    <a class="sc-tw-icon" href="https://twitter.com/kapitalboost" target="_blank"><img alt="" src="../assets/images/twitter-white.png" /></a>
                </p>
                <div id="ssl-con"><img alt="" id="ssl-img" src="../assets/images/SSL-CERT.png" /></div>
            </div>
        </div>
        
        <div id="footer-copy">&copy; COPYRIGHT 2015 &bull; KAPITAL BOOST PTE LTD &bull; SINGAPORE 201525866W</div>
        <div id="footer-disclaimer">
            <div id="disclaimer-content">
                <p>This website and the contents herein do not constitute as any financial advice, investment advice or solicitation for the purposes of making financial investments in Singapore or other territories. Kapital Boost Pte. Ltd. is a firm specialising in the matching of opportunities between our registered members and small to medium-sized enterprises. With regard to these opportunities, the need and onus to do due diligence lies squarely with our members as we do not profess to advise on the same. All dealings and transactions are directly with the businesses, project owners or authorized agents we refer to our members.</p>
                <p>Kapital Boost Pte. Ltd. is not licensed and/or registered under the Securities & Futures Act of Singapore or the Financial Advisor's Act under the Monetary Authority of Singapore and thus cannot offer, solicit, advice or recommend the buying and selling of investment securities or any other financial and banking products from the public.</p>
            </div>
        </div>
  </div>
  </div>
  <!-- end footer -->
<!-- ENd UI -->

  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
   <script type="text/javascript">

    var app = angular.module("xfers", []);
    app.controller("xfersCtrl", function($scope, $http){
      $scope.otp = "";
      
      
      $scope.xtoken = {};
      $scope.xballance = {};
      $scope.xcheckout = {};
      $scope.params = {};
      $scope.currentUser = {};
      
      var chargeparams = function(){
        return $scope.params = JSON.parse(localStorage.getItem('kapitaluser'));
      }; chargeparams();
      var getToken = function(){
        return $scope.xtoken = JSON.parse(localStorage.getItem('xferstoken'));
      }; getToken();
      var getBallance = function(){
        return $scope.xballance = JSON.parse(localStorage.getItem('userBallance'));
      }; getBallance();
      var getCheckout = function(){
        return $scope.xcheckout = JSON.parse(localStorage.getItem('xferscheckout'));
      }; getCheckout();
      var getPhone = function(){
        return $scope.currentphone = JSON.parse(localStorage.getItem('user'));
      }

      $scope.xfersToken = function(){
        getPhone();
        var url = "apis/token.php";
        
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
                "xfersotp": $scope.otp,
                "xfersphone": $scope.currentphone.phone
            }
        }).success(function(res) {
            localStorage.setItem('xferstoken', JSON.stringify(res));
            window.location = "ballance.php";
        }).error(function(err) {
            alert("No Internet Connection");
        });
      } 
      // Xfers TOken
      
      var xfersOTP = function(){
        getPhone();
        var url = "apis/sendOTP.php";
        
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
                "sendotp" : "AASSLLKKKAS",
                "xfersphone": $scope.currentphone.phone
            }
        }).success(function(res) {
            localStorage.setItem('xfersotp', JSON.stringify(res));
        }).error(function(err) {
            alert("No Internet Connection");
        });
      } 
      // Xfers send token OTP
      xfersOTP();
    });
  </script>
</body>
</html>