<!DOCTYPE html>
<html>
<head>
    <title>Xfers User Bllance</title>
    <!-- start style css -->
    <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../assets/bootstrap/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="../assets/css/new-kp-main.css" />
    <link rel="stylesheet" href="../assets/css/new-kp-home.css" />
    <link rel="stylesheet" href="../assets/css/kp-about.css" />
    <link rel="stylesheet" href="../assets/css/kp-default.css" />
    <link rel="stylesheet" href="../assets/css/kp-blog.css" />
    <link rel="stylesheet" href="..//assets/css/kp-event.css" />
    <link rel="stylesheet" type="text/css" href="assets/css/gaya.css">
    <script src="assets/js/jquery.min.js"></script>
</head>
<body ng-app="xfers" ng-controller="xfersCtrl">
    <center>
    <!-- start header -->
<div id="menu">
    <div id="menu-back"></div>
            <center>
                                <ul class="newMenu">
 <li><a href="https://kapitalboost.com/dashboard/payment-history"> Portfolio </a></li>
<li><a href="https://kapitalboost.com/dashboard/profile"> Manage Profile </a></li>


                    </ul>
            <ul class="dasUl">
                         <li>
                            <a href="dashboard/payment-history" style="cursor:pointer;">
                               Payment History
                            </a>
                         </li>
<!--
                         <li>
                            <a href="dashboard/invest-history" style="cursor:pointer;">
                              Portfolio
                            </a>
                         </li>
-->
                         <li>
                            <a href="dashboard/profile"style="cursor:pointer;">
                                Profile
                            </a>
                         </li>
                     </ul>
            </center>
    <div id="menu-front">
        <div id="menu-front-inner">
            <div id="menu-logo">
                <a href="">
                <img alt="Kapital Boost Logo" id="menu-logo-img" src="../assets/images/logo.png" />
                </a>
            </div>
            <ul id="kb-menu">
<li><a href="">home</a>
                </li>
                <li><a href="about">About</a>
                </li>
                <li><a href="campaign/">Campaigns</a>
                </li>
                <li><a id="eth-log">Get Funded</a>
                </li>
                <li><a href="how-it-works">How It Works</a>
                </li>
                                <li><a id="eth-log" class="loginRegisMenu" style="color: #609edf;">Login</a>
                </li>
                <li><a id="reg" class="loginRegisMenu" style="color: #609edf;">Register</a>
                </li>
                            </ul>
            <div id="rwd-menu">
                <div id="rwd-menu-inner">
                    <div class="rwd-menu-each"></div>
                    <div class="rwd-menu-each"></div>
                    <div class="rwd-menu-each"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="logpopupWrap">
 <div class="logpopupInner">
  <div class="logpopupClose">
   <span class="glyphicon glyphicon-remove"></span>
  </div>
  <div class="logpopupTitle">
   Are you sure want to logout?
  </div>
  <ul class="logpopupYesNo">
   <li><a href="logout">Yes</a></li>
   <li><a id="logpopupClose">No</a></li>
  </ul>
 </div>
</div>


    <script>
$(document).ready(function(){

 $('#testbro').click(function(){
  $('.logpopupWrap').fadeIn(500);
 });

 $('#logpopupClose').click(function(){
  $('.logpopupWrap').fadeOut(500);
 });

 $('.logpopupClose').click(function(){
  $('.logpopupWrap').fadeOut(500);
 });
});

/*
$("#dialog").dialog({
   autoOpen: false,
   modal: true,
   resizable: false,
   height: "auto",
   width: 400,
   buttons : {
        "Yes" : function() {
            window.location.href="https://kapitalboost.com/logout";
        },
        "No" : function() {
          $(this).dialog("close");
        }
      }
    });

$("#testbro").on("click", function(e) {
    e.preventDefault();
    $("#dialog").dialog("open");
});
*/
    </script>
    <!-- end header -->



    <!-- start xfers transferinfo -->
    <div class="xfersThankYou">
        <div class="XTYInner">
            <div class="XTYText">
                <p>
                    Thank you for you're your
                    <?php
                      $type = $_GET["type"];
                      if ($type == "donation") {
                        echo "Donation";
                      } else if($type == "sme"){
                        echo "Fund";
                      }
                    ?>
                    transfer of <br />
                    <strong>SGD <?php echo $_GET["amount"]; ?></strong> to the <storng><?php echo $_GET["campaign"]; ?></storng> crowdfunding project.
                </p>
            </div>
            <div class="XTYText2">
                <p>
                    If you have any questions, please email us at
                    <span>
                      <a href="mailto:support@kapitalboost.com">support@kapitalboost.com</a>
                    </span>
                </p>
            </div>
            <div class="XTYText3">
                <p>
                    Sincerely, <br />
                    The Kapital Boost Team
                </p>
            </div>
            <div class="XTYText4">
                <p>
                    Help support the project by letting others know
                </p>
            </div>
            <ul class="XTYLink">
                <li>
                    <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/<?php echo $_GET["campaignid"]; ?>">
                        <img src="assets/img/linkGoogle.jpg" />
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/<?php echo $_GET["campaignid"]; ?>">
                        <img src="assets/img/linkFacebook.jpg" />
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/<?php echo $_GET["campaignid"]; ?>">
                        <img src="assets/img/linkTwitter.jpg" />
                    </a>
                </li>
                <li>
                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/<?php echo $_GET["campaignid"]; ?>">
                        <img src="assets/img/linkLinkedIn.jpg" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- end x fers transferinfo -->
</center>



    <!-- start footer -->
        <div id="footer">
    <div id="footer-inner">
        <div id="footer-main">
            <div id="footer-main-1">
                <h3 class="foot-header">About</h3>
                <p><a href="about">About</a></p>
                <p><a href="how-it-works">How it works</a></p>
                <p><a href="media">Media</a></p>
            </div>
            <div id="footer-main-2">
                <h3 class="foot-header">General</h3>
                <p><a href="campaign/">Campaigns</a></p>
                <p><a href="blog">Blog</a></p>
                <p><a href="partners">Partners</a></p>
            </div>
            <div id="footer-main-3">
                <h3 class="foot-header">Support</h3>
                                <p><a id="eth-log" style="cursor:pointer;">Get Funded</a></p>
                                <p><a href="faq">FAQs</a></p>
                <p><a href="contact-us">Contact Us</a></p>
            </div>
            <div id="footer-main-5">
                <h3 class="foot-header">Legal</h3>
                <p><a href="https://kapitalboost.com/legal#term">Term of use</a></p>
                <p><a href="https://kapitalboost.com/legal#privacy">Privacy Policy</a></p>
                <p><a href="https://kapitalboost.com/legal#risk">Risk Statement</a></p>
            </div>
            <div id="footer-main-4">
                <h3 class="foot-header foot-header-2">Follow Us</h3>

                <p>
                    <a class="sc-fb-icon" href="https://www.facebook.com/kapitalboost" target="_blank"><img alt="" src="../assets/images/facebook.png" /></a>
                    <a class="sc-ln-icon" href="https://www.linkedin.com/company/6597611?trk=tyah&trkInfo=clickedVertical:company,clickedEntityId:6597611,idx:1-1-1,tarId:1440685694219,tas:kapital%20boost" target="_blank"><img alt="" src="../assets/images/linkedin.png" /></a>
                    <a class="sc-tw-icon" href="https://twitter.com/kapitalboost" target="_blank"><img alt="" src="../assets/images/twitter-white.png" /></a>
                </p>
                <div id="ssl-con"><img alt="" id="ssl-img" src="../assets/images/SSL-CERT.png" /></div>
            </div>
        </div>

        <div id="footer-copy">&copy; COPYRIGHT 2015 &bull; KAPITAL BOOST PTE LTD &bull; SINGAPORE 201525866W</div>
        <div id="footer-disclaimer">
            <div id="disclaimer-content">
                <p>This website and the contents herein do not constitute as any financial advice, investment advice or solicitation for the purposes of making financial investments in Singapore or other territories. Kapital Boost Pte. Ltd. is a firm specialising in the matching of opportunities between our registered members and small to medium-sized enterprises. With regard to these opportunities, the need and onus to do due diligence lies squarely with our members as we do not profess to advise on the same. All dealings and transactions are directly with the businesses, project owners or authorized agents we refer to our members.</p>
                <p>Kapital Boost Pte. Ltd. is not licensed and/or registered under the Securities & Futures Act of Singapore or the Financial Advisor's Act under the Monetary Authority of Singapore and thus cannot offer, solicit, advice or recommend the buying and selling of investment securities or any other financial and banking products from the public.</p>
            </div>
        </div>
    </div>
    </div>
    <!-- end footer -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/angular.min.js"></script>
    <script type="text/javascript">
            var app = angular.module("xfers", []);
            app.controller("xfersCtrl", function($scope, $http){

                var chargeparams = function(){
                return $scope.params = JSON.parse(localStorage.getItem('kapitaluser'));
              }; chargeparams();

            });
    </script>
</body>
</html>
