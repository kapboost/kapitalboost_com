<html>
<head>
  <title>Kapitalboost</title>
</head>
<style type='text/css'>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
</style>
<body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
  <table height='100%' width='100%' bgcolor='#305e8e'>
    <tbody>
      <tr>
        <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
          <center>
            <div style='display: inline-block;width: 640px;background: #00b0fd;'>
              <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
                <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                 Dear ".$member_name.",
                </p>
                <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                  <!-- Content Here -->
                </p>
                <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                  Best regards, <br>
                  Kapital Boost team
                </p>
              </div>
              <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                <p style='font-size: 16px;color: white;width:100%'>
                  &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
                </p>
              </div>
            </div>
          </center>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
