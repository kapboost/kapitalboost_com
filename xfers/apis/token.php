<?php

/**
 * Kapitalboost OTP and Access Token
 */
class KapitalOTP {

       public function insert($o, $a, $i) {
              /*
               * $o = OTP
               * $a = Access Token
               * $i = Id
               */
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     printf("Connect failed: %s\n", mysqli_connect_error());
                     exit();
              }

              $otp = mysqli_real_escape_string($mysqli, $o);
              $at = mysqli_real_escape_string($mysqli, $a);
              $id = mysqli_real_escape_string($mysqli, $i);

              $sql = 'UPDATE tmember SET member_otp= "' . $otp . '", member_access_token="' . $at . '" WHERE member_mobile_no = "' . $id . '"';
              $query = mysqli_query($mysqli, $sql);

              $select = "SELECT * FROM tmember WHERE member_mobile_no ='$id'";
              $result = mysqli_query($mysqli, $select);
              if (mysqli_num_rows($result) > 0) {
                     $row = mysqli_fetch_array($result);
                     $member_title = $row["member_title"];
                     $first = $row["member_firstname"];
                     $last = $row["member_surname"];
                     $email = $row["member_email"];
                     $dob  = $row["dob"];
                     $resarea = $row["residental_area"];
                     $member_nationality = $row["member_country"];
                     $nric = $row["nric"];
                     $current_location = $row["current_location"];
                     $filenric = $row["nric_file"];
                     $filenricback = $row["nric_file_back"];
                     $addressProof = $row["address_proof"];
                     $gender = "";
                     if ($member_title == "mr") {
                            $gender = "male";
                     } else if ($member_title == "ms" || $member_title == "mrs") {
                            $gender = "female";
                     }

                     $arr_xudata = array(
                         "first_name" => $first,
                         "last_name" => $last,
                         "email" => $email,
                         "date_of_birth" => $dob,
                         "gender" => $gender,
                         "address_line_1" => $resarea,
                         "nationality" => $member_nationality,
                         "identity_no" => $nric,
                         "country" => $current_location,
                         "id_front_url" => "https://kapitalboost.com/assets/nric/datanric/$filenric",
                         "id_back_url" => "https://kapitalboost.com/assets/nric/datanricback/$filenricback",
                         "proof_of_address_url" => "https://kapitalboost.com/assets/nric/addressproof/$addressProof"
                     );

                     $xudata = json_encode($arr_xudata);

                     $curl = curl_init();

                     curl_setopt_array($curl, array(
                         CURLOPT_URL => "https://www.xfers.io/api/v3/user",
                         CURLOPT_RETURNTRANSFER => true,
                         CURLOPT_SSL_VERIFYPEER => false,
                         CURLOPT_ENCODING => "",
                         CURLOPT_MAXREDIRS => 10,
                         CURLOPT_TIMEOUT => 30,
                         CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                         CURLOPT_CUSTOMREQUEST => "PUT",
                         CURLOPT_POSTFIELDS => $xudata,
                         CURLOPT_HTTPHEADER => array(
                             "cache-control: no-cache",
                             "content-type: application/json",
                             "postman-token: e6a82e16-8aa8-fe93-0aa8-5f74f10ccb8d",
                             "x-xfers-user-api-key: $at"
                         ),
                     ));

                     curl_exec($curl);


                     $sql = "UPDATE  tmember SET  xfers_info =  '" . $xudata . "' WHERE member_mobile_no =  '" . $id . "'";

                     $query = mysqli_query($mysqli, $sql);
              }

              if ($query) {
                     return true;
              } else {
                     return false;
              }
       }

}

if (isset($_POST["xfersotp"])) {
       $xfers_app_key = "uFDFJ7rc_s6ghryiKmdaT5YU6bQ22en1hwLGCwiw4Lg";
       $xfers_app_scret_key = "iuWKg9u9nBnTog_WpeNx73KB4nUAXSi9NVnq6jEGE2Q";
       $xfers_user_key = "4yphUCpZxym-xEUzNcsHyxK3LFQ5m4X4uxsLKmu3Boc";


       $curl = curl_init();
       $otp = $_POST["xfersotp"];
       $phone = $_POST["xfersphone"];
       $ephone = urlencode($phone);
       $needsecure = $phone . $otp . $xfers_app_scret_key;

       $secure = sha1($needsecure);
       curl_setopt_array($curl, array(
           CURLOPT_URL => "https://www.xfers.io/api/v3/authorize/get_token?otp=$otp&phone_no=$ephone&signature=$secure",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "GET",
           CURLOPT_HTTPHEADER => array(
               "cache-control: no-cache",
               "postman-token: 404f95ed-9c6c-a073-fa18-cb7ee5785d82",
               "x-xfers-app-api-key: uFDFJ7rc_s6ghryiKmdaT5YU6bQ22en1hwLGCwiw4Lg"
           ),
       ));

       $response = curl_exec($curl);
       $err = curl_error($curl);

       curl_close($curl);

       if ($err) {
              echo "cURL Error #:" . $err;
       } else {
              $check = json_decode($response);
              if ($check->msg == "success") {
                     $save = KapitalOTP::insert($otp, $check->user_api_token, $phone);
                     if ($save) {
                            echo "SUccess";
                     } else {
                            echo "Error";
                     }
              }
       }
}
?>