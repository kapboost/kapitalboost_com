<?php

/**
  * Kapitalboost OTP and Access Token
  */
  class KapitalOTP
  {
    public function getuserballance($phone){
      /*
      * $phone = PHONE
      *
      * OTP is member_otp , Access Token is member_access_token and Id is member_id or the mean is unique value from database
      * i call Id but after in production i will use email :D
      * Created Bye Mochamad Imam Ghozalie
       */
      $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

      /* check connection */
        if (mysqli_connect_errno()) {
            return "Error";
        }

        $p = mysqli_real_escape_string($mysqli,$phone);

        $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "'.$p.'"';
        
        $query = mysqli_query($mysqli, $sql);
        if ($query) {
          $data = mysqli_fetch_array($query);
          return $data;
        } else {
          return false;
        }
    }
  }


if (isset($_POST["gettransferinfo"])) {
  $phone = $_POST["gettransferinfo"];

  $a = KapitalOTP::getuserballance($phone);
  if ($a) {
    $access_token = $a[0];
    
    $curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://www.xfers.io/api/v3/user/transfer_info",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "x-xfers-user-api-key: $access_token"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
  } else {
    echo "Error";
  }
};
?>