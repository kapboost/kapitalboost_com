<?php

/**
 * Kapitalboost OTP and Access Token
 */
class KapitalOTP {

       public function insert($o, $a, $i) {
              /*
               * $o = OTP
               * $a = Access Token
               * $i = Id
               *
               * OTP is member_otp , Access Token is member_access_token and Id is member_id or the mean is unique value from database
               * i call Id but after in production i will use email :D
               * Created Bye Mochamad Imam Ghozalie
               */
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     printf("Connect failed: %s\n", mysqli_connect_error());
                     exit();
              }

              $otp = mysqli_real_escape_string($mysqli, $o);
              $at = mysqli_real_escape_string($mysqli, $a);
              $id = mysqli_real_escape_string($mysqli, $i);

              $sql = 'UPDATE tmember SET member_otp= "' . $otp . '", member_access_token="' . $at . '" WHERE id = "' . $id . '"';

              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     return true;
              } else {
                     return false;
              }
       }

       public function getuserballance($phone) {
              /*
               * $phone = PHONE
               *
               * OTP is member_otp , Access Token is member_access_token and Id is member_id or the mean is unique value from database
               * i call Id but after in production i will use email :D
               * Created Bye Mochamad Imam Ghozalie
               */
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     printf("Connect failed: %s\n", mysqli_connect_error());
                     exit();
              }

              $p = mysqli_real_escape_string($mysqli, $phone);

              $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "' . $p . '"';

              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     $res = mysqli_fetch_array($query);
                     return $res;
              } else {
                     return false;
              }
       }

       public function updateXfers($token) {
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');


              $sql = "UPDATE tinv SET status = 'Paid' WHERE token_pp = '$token'";


              $itinv = mysqli_query($mysqli, $sql);
              if (!$itinv) {
                     return "Error !" . mysqli_error();
              }
       }

}

$data = json_decode(file_get_contents('php://input'));

$cphone = $data->phone;

$a = KapitalOTP::getuserballance($cphone);
if ($a) {
       $access_token = $a[0];
       $data->user_api_token = $access_token;
       $datas = json_encode($data);

       $curl = curl_init();

       curl_setopt_array($curl, array(
           CURLOPT_URL => "https://www.xfers.io/api/v3/charges.php",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => "$datas",
           CURLOPT_HTTPHEADER => array(
               "cache-control: no-cache",
               "content-type: application/json",
               "postman-token: 18534473-e035-2327-1a44-219ce4c34438",
               "x-xfers-user-api-key: 4yphUCpZxym-xEUzNcsHyxK3LFQ5m4X4uxsLKmu3Boc"
           ),
       ));

       $result = curl_exec($curl);
       $err = curl_error($curl);
// $err = false;
       curl_close($curl);

       if ($err) {
              echo "cURL Error #:" . $err;
       } else {

              $response = json_decode($result, true);
              if ($response['status'] == 'completed' or $response['status'] == 'paid') {
                     //   # Transaction successful
                     include_once("email/email.payments.php");

                     $user = $data->a_name;
                     $campaign_name = $data->description;
                     $amount = $data->amount;
                     $token = $data->token;
                     $email = $data->member_email;
                     $campaign_type = $data->campaign_type;
                     if ($campaign_type == 'sme' or $campaign_type == 'private') {
                            //$xmsgsuccess = tplEmail($user, $campaign_name, $amount, "commitment");
                     } else {
                            $xmsgsuccess = tplEmail($user, $campaign_name, $amount, "donation");
                            /*
                              Sending Email
                             */
                            include_once("phpmailer.php");
                            $mail = new PHPMailer;
                            $mail->Encoding = "base64";
                            $mail->CharSet = "UTF-8";
                            $mail->From = "admin@kapitalboost.com";
                            $mail->FromName = "Kapital Boost";
                            $mail->AddReplyTo("admin@kapitalboost.com", "Reply to Kapital Boost");
                            $to = $data->member_email;
                            $mail->AddBCC("admin@kapitalboost.com");
                            $mail->addAddress($to);
                            $mail->isHTML(true);
                            $mail->Subject = "Payment Completed";
                            $mail->Body = $xmsgsuccess;
                            $mail->addCustomHeader("X-AntiAbuse: This header was added to track abuse, please include it with any abuse report");
                            $mail->addCustomHeader("X-AntiAbuse: Primary Hostname - kapitalboost.com");
                            $mail->addCustomHeader("X-AntiAbuse: Sender Address Domain - kapitalboost.com");
                            $mail->addCustomHeader("X-Authenticated-Sender: admin@kapitalboost.com");
                            $mail->addCustomHeader("X-Source-Dir: :/kapitalboost.com/public_html");
                            if (!$mail->send()) {
                                   echo "Mailer Error: " . $mail->ErrorInfo;
                            }
                            $mail->ClearAddresses();
                            $mail->ClearBCCs();
                            /*
                              End Sending Email
                             */
                     }

                     $uXfersPaid = KapitalOTP::updateXfers($token);


                     $resXfers = array(
                         "status" => "Success" . $uXfersPaid,
                         "error" => false
                     );
                     header('Content-Type: application/json');
                     echo json_encode($resXfers);
              } else {
                     # debit unsuccessful, can be insufficient funds or KYC error.
                     echo "Transaction Failed!" . 'Error : ' . $uXfersPaid;
                     # meta data would contain error message if anything
                     echo $response['meta_data']['error_code'];
              }
       }
       // Enf Of API Charges
} else {
       echo "Error";
}
?>
