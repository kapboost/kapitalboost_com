<?php
 
  function tplEmail($name,$cname,$amount,$cType){
         $msg = "<html>
       <head>
              <title>Kapital Boost</title>
       </head>
       <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
              <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
                     <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
                            <img src='https://kapitalboost.com/email/logo.jpg'  style='margin: 3% 0 0 3%;width:30%;'/>
                            <p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
                                   Dear " . $name . ",
                            </p>
                            <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                   Thank you for your ".$cType." of <b>SGD " . $amount . "</b> for the <b>" . $cname . "</b> campaign.
                            </p>
                            
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   May your kindness be rewarded in multiplefold.
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   Best Regards,<br><br>Kapital Boost
                            </p>
                     </div>
                     <div style='width:100%;height:7px;background-color:#00b0fd'></div>
                     <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                            <p style='font-size: 12px;color: white;text-align:center'>
                                   &copy; COPYRIGHT 2017 - KAPITAL BOOST PTE LTD
                            </p>
                     </div>
              </div>
       </body>
</html>   ";
                 return $msg;
                 
  }



?>
