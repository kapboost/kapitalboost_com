<?php
session_start();
if (isset($_POST["username"]) && isset($_POST["password"])) {

	$name = test_input($_POST["username"]);
	$password = test_input($_POST["password"]);
	include_once 'mysql.php';
	$mysql = new mysql();

	if ($mysql->Connection()) {
		$login = $mysql->MemberLogin($name, $password);
		if ($login > 0) {
			$_SESSION["MemberEmail"] = $name;
			// $_SESSION["MemberName"] = $name;
			header("Location: index.php");
		} else if ($login == 0) {
			$error = "Wrong Password";
		} else if ($login == -1) {
			$error = "Wrong Admin Name";
		}
	}
}

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive admin dashboard and web application ui kit. ">
    <meta name="keywords" content="login, signin">

    <title>Kapital Boost: Islamic crowdfunding - Business Finance</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">

    <!-- Styles -->
    <link href="assets/css/core.min.css" rel="stylesheet">
    <link href="assets/css/app.min.css" rel="stylesheet">
    <link href="assets/css/style.min.css" rel="stylesheet">
	
	 <!-- FB -->
  <meta property='fb:app_id' content="1419559388071407"/>
  <meta property='og:type' content="website" />
  <meta property='og:image' content="{if $image_url}{$image_url}{else}https://kapitalboost.com/assets/images/Slide1.jpg{/if}" />
  <meta property='og:title' content="{$page_title}" />
  <meta property='og:site_name' content="Kapital Boost Crowdfunding" />
  <meta property='og:description' content="{$meta_desc|strip_tags|strip|truncate:240}" />
  <meta property='og:url' content="https://kapitalboost.com" />
  <meta property='og:image:width' content="336" />
  <meta property='og:image:height' content="201" />

    <!-- Favicons -->
    <link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
    <link rel="icon" href="../assets/images/kp-fav.png">
	<script>
	  window.fbAsyncInit = function () {
                            FB.init({
                                   appId: '261151691029080',
                                   cookie: true,
                                   xfbml: true,
                                   version: 'v2.8'
                            });
                            FB.AppEvents.logPageView();
                     };
                     function checkLoginState() {
                            $("#fbCover").fadeIn();
                            $("#lightbox").fadeOut(100);
                            $("#reg-lightbox").fadeOut(100);
                            FB.getLoginStatus(function (response) {
                                   if (response.status === 'connected') {
                                          var token = response.authResponse.accessToken;
                                          FB.api('/me?fields=id,name,gender,location', function (response) {
                                                 console.log(JSON.stringify(response));
                                                 FB.api('/me/permissions', function (response) {
                                                        var email = 1;
                                                        for (i = 0; i < response.data.length; i++) {
                                                               if (response.data[i].permission == 'email' && response.data[i].status == 'declined') {
                                                                      $("#fbCover").fadeOut();
                                                                      alert("Please provide us your email address via Facebook login");
                                                                      email = 0;
                                                               }
                                                        }
                                                        if (email == 1) {
                                                               $.post("index.php?class=Member&method=Fblogin", {token: token}, function (reply) {
                                                                      if (reply == "1") {
                                                                             location.reload();
                                                                      }
                                                               });
                                                        }

                                                 });

                                          });
                                   }
                            });
                     }

                     (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) {

                                   return;
                            }
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js";
                            fjs.parentNode.insertBefore(js, fjs);
                     }(document, 'script', 'facebook-jssdk'));
	</script>
  </head>

  <body>


    <div class="row min-h-fullscreen center-vh p-20 m-0">
      <div class="col-12">
        <div class="card card-shadowed px-50 py-30 w-400px mx-auto" style="max-width: 100%">
          <h5 class="text-uppercase">Sign in</h5>
          <br>

          <form class="form-type-material">
            <div class="form-group">
              <input type="text" class="form-control" id="username" name="username">
              <label for="username">Username</label>
            </div>

            <div class="form-group">
              <input type="password" class="form-control" id="password" name="password">
              <label for="password">Password</label>
            </div>

            <div class="form-group flexbox flex-column flex-md-row">
              <label class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" checked>
                <span class="custom-control-indicator"></span>
                <span class="custom-control-description">Remember me</span>
              </label>

              <a class="text-muted hover-primary fs-13 mt-2 mt-md-0" href="#">Forgot password?</a>
            </div>

            <div class="form-group">
              <button class="btn btn-bold btn-block btn-info" type="submit">Login</button>
            </div>
          </form>

          <div class="divider">Or Sign In With</div>
          <div class="text-center">
            <a class="btn btn-square btn-facebook" href="#"><i class="fa fa-facebook"></i></a>
            <a class="btn btn-square btn-google" href="#"><i class="fa fa-google"></i></a>
            <a class="btn btn-square btn-twitter" href="#"><i class="fa fa-twitter"></i></a>
          </div>
        </div>
        <p class="text-center text-muted fs-13 mt-20">Don't have an account? <a class="text-primary fw-500" href="#">Sign up</a></p>
      </div>


      <footer class="col-12 align-self-end text-center fs-13">
        <p class="mb-0"><small>Copyright © 2017 <a href="http://thetheme.io/theadmin">TheAdmin</a>. All rights reserved.</small></p>
      </footer>
    </div>




    <!-- Scripts -->
    <script src="assets/js/core.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <script src="assets/js/script.min.js"></script>
	<script>
	
                     function checkLoginState() {
                            $("#fbCover").fadeIn();
                            $("#lightbox").fadeOut(100);
                            $("#reg-lightbox").fadeOut(100);
                            FB.getLoginStatus(function (response) {
                                   if (response.status === 'connected') {
                                          var token = response.authResponse.accessToken;
                                          FB.api('/me?fields=id,name,gender,location', function (response) {
                                                 console.log(JSON.stringify(response));
                                                 FB.api('/me/permissions', function (response) {
                                                        var email = 1;
                                                        for (i = 0; i < response.data.length; i++) {
                                                               if (response.data[i].permission == 'email' && response.data[i].status == 'declined') {
                                                                      $("#fbCover").fadeOut();
                                                                      alert("Please provide us your email address via Facebook login");
                                                                      email = 0; 
                                                               }
                                                        }
                                                        if (email == 1) {
                                                               $.post("https://kapitalboost.com/index.php?class=Member&method=Fblogin", {token: token}, function (reply) {
                                                                      if (reply == "1") {
																			console.log(reply);
                                                                            window.location.replace("https://kapitalboost.com/member/");
                                                                      }
                                                               });
                                                        }

                                                 });

                                          });
                                   }
                            });
                     }
	</script>

  </body>
</html>

