<?php

class mysql
{

    public $dbhost = null;
    public $dbuser = null;
    public $dbpass = null;
    public $dbname = null;
    public $conn   = null;
    public $result = null;

    public function __construct()
    {
		// production database
		$mysqlfile = parse_ini_file('../../.rDir/.MySQL.ini');
		$this->dbhost = $mysqlfile['dbhost'];
		$this->dbuser = $mysqlfile['dbuser'];
		$this->dbpass = $mysqlfile['dbpass'];
		$this->dbname = $mysqlfile['dbname'];
	   
		// staging database
		// $this->dbhost = "localhost";
		// $this->dbuser = "kbadminsite";
		// $this->dbpass = "yqUF057OR7ZnId3SdAH0bJMuwTIcXZf";
		// $this->dbname = "state_kapitalboost";

        // $this->dbhost = "localhost";
        // $this->dbuser = "root";
        // $this->dbpass = "";
        // $this->dbname = "kapitalboost";
    }

    public function Connection()
    {
        $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
        if (mysqli_connect_error()) {
            return false;
        }
        mysqli_set_charset($this->conn, 'utf8');
        return true;
    }

    // Function connect to sql

    public function CloseConnection()
    {
        if ($this->conn != null) {
            mysqli_close($this->conn);
        }
    }

    // Function disconnect to sql

    public function MemberLogin($name, $password)
    {
        $id  = 0;
        $sql = $this->conn->prepare("SELECT * FROM tmember WHERE member_email = ?");
        $sql->bind_param("s", $name);
        $sql->execute();
        $this->result = $sql->get_result();
        $row          = $this->result->fetch_assoc();
        if (count($this->result) == 1) {
            if ($row['member_password'] == md5($password)) {
                $id                              = $row['member_id'];
                $_SESSION["MemberName"]          = $row['member_firstname'];
                $_SESSION["MemberId"]            = $row['member_id'];
                $_SESSION["MemberCountry"]       = $row['current_location'];
                $_SESSION["MemberPhone"]         = $row['member_mobile_no'];
                $_SESSION['cookie']['member_id'] = $_SESSION["MemberId"];
            } else {
                $id = 0;
            }
        } else {
            $id = -1;
        }
        return $id;
    }

    public function GetMmeberTransactionHistory($mId)
    {
        $sql    = "SELECT  * from twallet_transaction where member_id like '%$mId%' order by date desc";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $transDate[$i]   = $row['date'];
                $transDesc[$i]   = $row['description'];
                $transStats[$i]  = $row['stats'];
                $transAmount[$i] = $row['amount'];
                $i++;
            }
            return array($transDate, $transDesc, $transStats, $transAmount);
        }
    }

    public function GetMmeberBalance($mId)
    {
        $sql    = "SELECT wallet FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result)['wallet'];
    }
	
    public function GetMemberGender($mId)
    {
        $sql    = "SELECT member_gender FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result)['member_gender'];
    }
	
    public function GetFrontImage($mId)
    {
        $sql    = "SELECT nric_file FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result)['nric_file'];
    }
	
    public function GetBackImage($mId)
    {
        $sql    = "SELECT nric_file_back FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result)['nric_file_back'];
    }
	
    public function GetProofAddress($mId)
    {
        $sql    = "SELECT address_proof FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result)['address_proof'];
    }

    public function GetInvestmentList($memberId)
    {
        $campaingId = $investAmount = $invStatus = $invId = $investDate = $campaignName = $campaignSnippet = $campaignReturn = $campaignRisk = $campaignBackground = array();
        $sql        = "SELECT  tinv.campaign_id, total_funding, DATE_FORMAT(date, '%d %M %Y') as date, status, campaign_name, campaign_snippet, project_return, risk, campaign_background, tinv.id as tinvid FROM tinv inner join tcampaign WHERE member_id = '$memberId' and tinv.campaign_id=tcampaign.campaign_id and (tcampaign.project_type='sme' or tcampaign.project_type='private') ORDER BY id DESC";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $campaingId[$i]         = $row['campaign_id'];
                $investAmount[$i]       = $row['total_funding'];
                $invStatus[$i]          = $row['status'];
                $invId[$i]              = $row['tinvid'];
                $investDate[$i]         = $row['date'];
                $campaignName[$i]       = $row['campaign_name'];
                $campaignSnippet[$i]    = $row['campaign_snippet'];
                $campaignReturn[$i]     = $row['project_return'];
                $campaignRisk[$i]       = $row['risk'];
                $campaignBackground[$i] = $row['campaign_background'];

                $i++;
            }
            return array($campaingId, $investAmount, $invStatus, $invId, $investDate, $campaignName, $campaignSnippet, $campaignReturn, $campaignRisk, $campaignBackground);
        }
    }
    public function GetDonationList($memberId)
    {
        $donationId = $donationAmount = $donationStatus = $donationId = $donationDate = $donationName = $donationSnippet = $donationReturn = $donationRisk = $donationBackground = array();
        $sql        = "SELECT  tinv.campaign_id, total_funding, DATE_FORMAT(date, '%d %M %Y') as date, status, campaign_name, campaign_snippet, project_return, risk, campaign_background, tinv.id as tinvid FROM tinv inner join tcampaign WHERE member_id = '$memberId' and tinv.campaign_id=tcampaign.campaign_id and tcampaign.project_type='donation' ORDER BY id DESC";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $donationId[$i]         = $row['campaign_id'];
                $donationAmount[$i]     = $row['total_funding'];
                $donationStatus[$i]     = $row['status'];
                $donationId[$i]         = $row['tinvid'];
                $donationDate[$i]       = $row['date'];
                $donationName[$i]       = $row['campaign_name'];
                $donationSnippet[$i]    = $row['campaign_snippet'];
                $donationReturn[$i]     = $row['project_return'];
                $donationRisk[$i]       = $row['risk'];
                $donationBackground[$i] = $row['campaign_background'];

                $i++;
            }
            return array($donationId, $donationAmount, $donationStatus, $donationId, $donationDate, $donationName, $donationSnippet, $donationReturn, $donationRisk, $donationBackground);
        }
    }

    public function GetFiveInvestmentList($memberId)
    {
        $campaingId = $investAmount = $invStatus = $investDate = $campaignName = $campaignSnippet = $campaignReturn = $campaignRisk = $campaignBackground = array();
        $sql        = "SELECT  tinv.campaign_id, total_funding, DATE_FORMAT(date, '%d %M %Y') as date, status, campaign_name, campaign_snippet, project_return, risk, campaign_background FROM tinv inner join tcampaign WHERE member_id = '$memberId' and tinv.campaign_id=tcampaign.campaign_id and (tcampaign.project_type='sme' or tcampaign.project_type='private') ORDER BY id DESC LIMIT 5";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $campaingId[$i]         = $row['campaign_id'];
                $investAmount[$i]       = $row['total_funding'];
                $invStatus[$i]          = $row['status'];
                $investDate[$i]         = $row['date'];
                $campaignName[$i]       = $row['campaign_name'];
                $campaignSnippet[$i]    = $row['campaign_snippet'];
                $campaignReturn[$i]     = $row['project_return'];
                $campaignRisk[$i]       = $row['risk'];
                $campaignBackground[$i] = $row['campaign_background'];

                $i++;
            }
            return array($campaingId, $investAmount, $invStatus, $investDate, $campaignName, $campaignSnippet, $campaignReturn, $campaignRisk, $campaignBackground);
        }
    }

    public function GetFiveDonationList($memberId)
    {
        $donationId = $donateAmount = $donateStatus = $donateDate = $donateName = $donateSnippet = $donateReturn = $donateRisk = $donateBackground = array();
        $sql        = "SELECT  tinv.campaign_id, total_funding, DATE_FORMAT(date, '%d %M %Y') as date, status, campaign_name, campaign_snippet, project_return, risk, campaign_background FROM tinv inner join tcampaign WHERE member_id = '$memberId' and tinv.campaign_id=tcampaign.campaign_id and tcampaign.project_type='donation' ORDER BY id DESC LIMIT 5";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $donationId[$i]       = $row['campaign_id'];
                $donateAmount[$i]     = $row['total_funding'];
                $donateStatus[$i]     = $row['status'];
                $donateDate[$i]       = $row['date'];
                $donateName[$i]       = $row['campaign_name'];
                $donateSnippet[$i]    = $row['campaign_snippet'];
                $donateReturn[$i]     = $row['project_return'];
                $donateRisk[$i]       = $row['risk'];
                $donateBackground[$i] = $row['campaign_background'];

                $i++;
            }
            return array($donationId, $donateAmount, $donateStatus, $donateDate, $donateName, $donateSnippet, $donateReturn, $donateRisk, $donateBackground);
        }
    }

    public function GetMemberTotalInvestment($memberId)
    {
        $stmt = $this->conn->prepare("SELECT SUM(tinv.total_funding) as totalAmount FROM tinv inner join tcampaign WHERE member_id = ?  and tcampaign.campaign_id=tinv.campaign_id and (tcampaign.project_type='sme' or tcampaign.project_type='private')");
        $stmt->bind_param("i", $memberId);
        $stmt->execute();
        if ($stmt->bind_result($memberTotalInvestment)) {
            if ($stmt->fetch()) {
                return $memberTotalInvestment;
            }
        }
    }
	
    public function GetTotalPendingWithdrawal($memberId)
    {
        $stmt = $this->conn->prepare("SELECT SUM(amount) as totalAmount FROM wallet_withdraw WHERE member_id = ? and status=0");
        $stmt->bind_param("i", $memberId);
        $stmt->execute();
        if ($stmt->bind_result($memberTotalPendingWithdrawal)) {
            if ($stmt->fetch()) {
                return $memberTotalPendingWithdrawal;
            }
        }
    }

    public function GetMemberToken($memberId)
    {
        $stmt = $this->conn->prepare("select member_access_token from tmember where member_id = ?");
        $stmt->bind_param("i", $memberId);
        $stmt->execute();
        if ($stmt->bind_result($access_token_member)) {
            if ($stmt->fetch()) {
                return $access_token_member;
            }
        }
    }
	
    public function GetMemberEmail($memberId)
    {
        $stmt = $this->conn->prepare("select member_email from tmember where member_id = ?");
        $stmt->bind_param("i", $memberId);
        $stmt->execute();
        if ($stmt->bind_result($member_email)) {
            if ($stmt->fetch()) {
                return $member_email;
            }
        }
    }

    public function GetMemberAverageReturn($memberId)
    {

        $sql = "SELECT * from tcampaign inner join tinv where tinv.member_id ='$memberId' and tcampaign.campaign_id=tinv.campaign_id order by tcampaign.campaign_id ";
        $result             = mysqli_query($this->conn, $sql);
        $totalProjectReturn = 0;
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $return[$i]   = $row['project_return'];
                $duration[$i] = $row['funding_summary'];
                preg_match_all('!\d+!', $duration[$i], $matches);
                $investDuration = implode(' ', $matches[0]);
                if ($investDuration == "" or $investDuration == null) {$investDuration = 1;}
                if ($return[$i] == "" or $return[$i] == null or $return[$i] == 'donation') {$return[$i] = 0;}
                $projectReturn      = ($return[$i] / $investDuration) * 12;
                $totalProjectReturn = $totalProjectReturn + $projectReturn;
                // echo "'<script>console.log(\"totalProjectReturn : $totalProjectReturn\")</script>'";
                $i++;
            }

            $sqls = "select tinv.campaign_id from tinv inner join tcampaign where member_id ='$memberId' and tcampaign.project_type='sme' and tcampaign.campaign_id=tinv.campaign_id";
            $results         = mysqli_query($this->conn, $sqls);
            $projectInvested = mysqli_num_rows($results);

            $averageReturn = $totalProjectReturn / ($projectInvested);

            return $averageReturn;
        }
    }

    public function GetProjectNews($memberId)
    {
        $Id = $campaingId = $campaignName = $campaignBackground = $updateTitle = $updateContent = $updateDate = array();
        $sql        = "SELECT tupdate.id, tupdate.campaign_id, DATE_FORMAT(tupdate.date, '%d %M %Y') as date, campaign_name, campaign_background, title, content, tupdate.id as idupdate FROM tcampaign inner join tupdate WHERE tcampaign.campaign_id=tupdate.campaign_id and tupdate.campaign_id in (select campaign_id from tinv where member_id = '$memberId') ORDER BY id DESC";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
				$Id[$i]         		= $row['id'];
                $campaingId[$i]         = $row['campaign_id'];
                $updateDate[$i]         = $row['date'];
                $campaignName[$i]       = $row['campaign_name'];
                $campaignBackground[$i] = $row['campaign_background'];
                $updateTitle[$i]        = $row['title'];
                $updateContent[$i]      = $row['content'];
                $updateID[$i]           = $row['idupdate'];

                $i++;
            }
            return array($Id, $campaingId, $campaignName, $campaignBackground, $updateTitle, $updateContent, $updateDate, $updateID);
        }
    }

    public function GetSingleProjectNews($idupdate)
    {
        $campaingId = $campaignName = $campaignBackground = $updateTitle = $updateContent = $updateDate = array();
        $sql        = "SELECT * from tupdate where id='$idupdate'";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $campaingId[$i]         = $row['campaign_id'];
                $updateDate[$i]         = $row['date'];
                $campaignName[$i]       = $row['campaign_name'];
                $campaignBackground[$i] = $row['campaign_background'];
                $updateTitle[$i]        = $row['title'];
                $updateContent[$i]      = $row['content'];
                $updateID[$i]           = $row['idupdate'];
                $images1[$i]           	= $row['image'];
                $images2[$i]           	= $row['image2'];
                $images3[$i]           	= $row['image3'];
                $images4[$i]           	= $row['image4'];
                $images5[$i]           	= $row['image5'];
                $images6[$i]           	= $row['image6'];
                $images7[$i]           	= $row['image7'];

                $i++;
            }
            return array($campaignName, $campaignBackground, $updateTitle, $updateContent, $updateDate, $images1, $images2, $images3, $images4, $images5, $images6, $images7);
        }
    }

    public function GetCountryList()
    {
        $countryName = $countryCode = array();
        $sql         = "SELECT * from tcountry order by name asc";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $countryName[$i] = $row['name'];
                $countryCode[$i] = $row['id'];
                $i++;
            }
            return array($countryName, $countryCode);
        }
    }

    public function GetMemberList()
    {
        $memberName = $memberEmail = array();
        $sql        = "SELECT * from tmember order by member_firstname asc";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $memberName[$i]  = $row['member_firstname'];
                $memberEmail[$i] = $row['member_email'];
                $i++;
            }
            return array($memberName, $memberEmail);
        }
    }

    public function GetMemberSingleList($membersEmail)
    {
        $memberName = $memberEmail = $memberId = $memberCountry = $memberPhone = array();
        $sql        = "SELECT * from tmember where member_email like '%$membersEmail%' order by member_firstname asc";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $memberName[$i]    = $row['member_firstname'];
                $memberEmail[$i]   = $row['member_email'];
                $memberId[$i]      = $row['member_id'];
                $memberCountry[$i] = $row['current_location'];
                $memberPhone[$i]   = $row['member_mobile_no'];
                $i++;
            }
            return array($memberName, $memberEmail, $memberId, $memberCountry, $memberPhone);
        }
    }

    public function GetNationalityList()
    {
        $nationalityName = $nationalityCode = array();
        $sql             = "SELECT * from tnationality order by nationality asc";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $nationalityName[$i] = $row['nationality'];
                $nationalityCode[$i] = $row['id_nationality'];
                $country[$i] = $row['country'];
                $i++;
            }
            return array($nationalityName, $nationalityCode, $country);
        }
    }

    public function GetMemberData($memberId)
    {
        $sql = "SELECT * from tmember where member_id = $memberId";
        // echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {

                $memberFirstName[$i]        = $row['member_firstname'];
                $memberLastName[$i]         = $row['member_surname'];
                $memberCountry[$i]          = $row['current_location'];
                $memberUsername[$i]         = $row['member_username'];
                $memberDateOfBirth[$i]      = $row['dob'];
                $memberEmail[$i]            = $row['member_email'];
                $memberNationality[$i]      = $row['member_country'];
                $memberPhone[$i]            = $row['member_mobile_no'];
                $memberAddress[$i]          = $row['residental_area'];
                $memberPassword[$i]         = $row['member_password'];
                $memberPassportNo[$i]       = $row['nric'];
                $memberPassportFullName[$i] = $row['member_icname'];
                $memberIdType[$i]           = $row['ic_option'];
                $memberIdCountry[$i]        = $row['ic_country'];
                $memberNRICFront[$i]        = $row['nric_file'];
                $memberNRICBack[$i]         = $row['nric_file_back'];
                $memberAddressProof[$i]     = $row['address_proof'];
                $memberCheck1[$i]           = $row['cek1'];
                $memberCheck2[$i]           = $row['cek2'];
                $memberCheck3[$i]           = $row['cek3'];
                $memberCheck4[$i]           = $row['cek4'];
                $memberCheck5[$i]           = $row['cek5'];
                $memberCheck6[$i]           = $row['cek6'];
                $memberCheck7[$i]           = $row['cek7'];
                $memberCheck8[$i]           = $row['cek8'];
                $memberCheck9[$i]           = $row['cek9'];
                $memberCheck10[$i]          = $row['cek10'];
                $memberCheck11[$i]          = $row['cek11'];
                $memberCheck12[$i]          = $row['cek12'];
                $memberCheck13[$i]          = $row['cek13'];
                $member_gender[$i]          = $row['member_gender'];
                $member_status[$i]          = $row['member_status'];
                $i++;
            }
            return array($memberFirstName, $memberLastName, $memberCountry, $memberUsername, $memberDateOfBirth, $memberEmail, $memberNationality, $memberPhone, $memberAddress, $memberPassword, $memberPassportNo, $memberPassportFullName, $memberIdType, $memberIdCountry, $memberNRICFront, $memberNRICBack, $memberAddressProof, $memberCheck1, $memberCheck2, $memberCheck3, $memberCheck4, $memberCheck5, $memberCheck6, $memberCheck7, $memberCheck8, $memberCheck9, $memberCheck10, $memberCheck11, $memberCheck12, $memberCheck13, $member_gender, $member_status);
        }
    }

    public function UpdateNricFront($nric_newname)
    {
        $stmt = $this->conn->prepare("UPDATE tmember SET nric_file = '$nric_newname' WHERE member_id=" . $_SESSION["MemberId"]);
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function AddTrail($trail)
    {
        $stmt = $this->conn->prepare("INSERT INTO trail(event,member_id,date) VALUES ('$trail','" . $_SESSION["MemberId"] . "',now())");
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function UpdateNricBack($nric_back_newname)
    {
        $stmt = $this->conn->prepare("UPDATE tmember SET nric_file_back = '$nric_back_newname' WHERE member_id=" . $_SESSION["MemberId"]);
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function UpdateProofAddress($ap_newname)
    {
        $stmt = $this->conn->prepare("UPDATE tmember SET address_proof = '$ap_newname' WHERE member_id=" . $_SESSION["MemberId"]);
        // echo "'<script>console.log(\"amount : UPDATE tmember SET address_proof = '$ap_newname' WHERE member_id=".$_SESSION["MemberId"]."\")</script>'";
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function UpdateDataMember($country, $uname, $pass, $last, $first, $nationality, $nric, $ic_type, $ic_option, $ic_name, $ic_country, $dob, $resarea, $cek1, $cek2, $cek3, $cek4, $cek5, $cek6, $cek7, $cek8, $cek9, $cek10, $cek11, $cek12, $cek13, $nope, $member_status)
    {
        $stmt = $this->conn->prepare("UPDATE tmember SET current_location = '$country',member_username = '$uname',member_password = '$pass',member_surname = '$last',member_firstname = '$first',member_country = '$nationality',nric= '$nric',ic_type='$ic_type',ic_option = '$ic_option',member_icname = '$ic_name',ic_country = '$ic_country',dob = '$dob',residental_area = '$resarea',cek1 = '$cek1',cek2 = '$cek2', cek3 = '$cek3',cek4 = '$cek4',cek5 = '$cek5',cek6 = '$cek6',cek7 = '$cek7',cek8 = '$cek8',cek9 = '$cek9',cek10 = '$cek10',cek11 = '$cek11', cek12 = '$cek12', cek13 = '$cek13',member_mobile_no = '$nope',member_status = '$member_status' WHERE member_id = " . $_SESSION["MemberId"]);
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function GetSingleCampaignForPayout($cId)
    {
        $sql = $this->conn->prepare("SELECT campaign_name, m_payout FROM tcampaign WHERE campaign_id = $cId");
        // echo "'<script>console.log(\"amount : SELECT campaign_name, m_payout FROM tcampaign WHERE campaign_id = $cId\")</script>'";
        $sql->execute();
        $this->result = $sql->get_result();
        $row          = $this->result->fetch_assoc();
        if (count($this->result) == 1) {
            $cName   = $row["campaign_name"];
            $mPayout = $row["m_payout"];
            return array($cName, $mPayout);
        } else {
            return "No Campaign";
        }
    }
	
    public function GetSinglePayoutOutSchedule($invId, $bulan, $status)
    {
        $sql = $this->conn->prepare("SELECT * FROM tinv WHERE id = $invId");
        $sql->execute();
        $this->result = $sql->get_result();
        $row          = $this->result->fetch_assoc();
        if (count($this->result) == 1) {
            $singleAmount   = $row[$bulan];
            $singleStatus = $row[$status];
        }
		return array($singleAmount, $singleStatus);
    }

    public function InsertNewWithdrawRequest($amount, $member_id)
    {
        $stmt = $this->conn->prepare("INSERT INTO wallet_withdraw(member_id,amount,date,status) VALUES ('$member_id', '$amount', now(), '0')");
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }
	
	public function InsertNewSupportTicket($content, $member_id, $subject)
    {
        $stmt = $this->conn->prepare("INSERT INTO support_ticket(member_id,question,title,status,time) VALUES ('$member_id', '$content', '$subject', '0',now())");
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function InsertNewSupportTicketReply($ticketId, $reply, $member_id)
    {
        $stmt = $this->conn->prepare("INSERT INTO support_ticket_reply(member_id,ticket_id,reply,time) VALUES ('$member_id', '$ticketId', '$reply', now())");
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    public function GetSupportTicketDetail($ticketId, $member_id)
    {
        $sql    = "SELECT * from support_ticket where id='$ticketId' and member_id='$member_id' order by id desc";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $id[$i]      = $row['id'];
                $subject[$i] = $row['title'];
                $date[$i]    = $row['time'];
                $content[$i] = $row['question'];
                $status[$i]  = $row['status'];
                $i++;
            }
            return array($id, $subject, $date, $content, $status);
        }
    }

    public function GetSupportTicketReplay($ticketId)
    {
        $sql    = "SELECT * from support_ticket_reply where ticket_id='$ticketId' order by id asc";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $reply[$i]     = $row['reply'];
                $date[$i]      = $row['time'];
                $member_id[$i] = $row['member_id'];
                $i++;
            }
            return array($date, $reply, $member_id);
        }
    }
    
    //this function return the information of member's bank's informations
    public function GetBankFullInfo($mId)
    {
        $sql    = "SELECT bank_name, account_name, account_number, swift_code FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result);
    }

    // update bank data, for bank-data
    public function UpdateBankData($bank_name, $account_name, $account_number, $swift_code, $mem_id)
    {
        $stmt = $this->conn->prepare("UPDATE tmember SET bank_name = '$bank_name',account_name = '$account_name',account_number = '$account_number',swift_code = '$swift_code' WHERE member_id = '$mem_id' ");
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

    //  updating wallet, for withdraw.php
    public function updateWallet($new_value, $mem_id)
    {
        $stmt = $this->conn->prepare("UPDATE tmember SET wallet = '$new_value' WHERE member_id = '$mem_id' ");
        $stmt->execute();
        $mId = mysqli_insert_id($this->conn);
        return $mId;
    }

	//function to get detail of member withdraw history
    public function GetMemberWithdrawHistory($mId)
    {
        $sql    = "SELECT  * from wallet_withdraw where member_id like '%$mId%' order by id desc";
		// echo "'<script>console.log(\"sql : $sql\")</script>'";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $date[$i]   = $row['date'];
                $amount[$i]   = $row['amount'];
                $status[$i]  = $row['status'];
                $proof_payment[$i] = $row['proof_payment'];
                $date_transfer[$i] = $row['date_transfer'];
                $i++;
            }
            return array($date, $amount, $status, $proof_payment, $date_transfer);
        }
    }
	

	public function GetMemberDataDetail($mId)
    {
        $sql    = "SELECT * FROM tmember WHERE member_id = '$mId'";
        $result = mysqli_query($this->conn, $sql);
        return mysqli_fetch_assoc($result);
    }
	
	//function to get getfunded list submitted by member
    public function GetMemberGetFunded($mId)
    {
		$memEmail 	= $this->GetMemberEmail($mId) ;
        $sql    	= "SELECT  * from tgetfunded where email like '%$memEmail%' order by getfunded_id desc";
        $result 	= mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $company[$i]   		= $row['company'];
                $year[$i]   		= $row['year'];
                $revenue[$i]  		= $row['est_an_rev'];
                $fundPeriod[$i] 	= $row['funding_period'];
                $pDate[$i] 			= $row['date'];
                $finType[$i] 		= $row['fin_type'];
                $currency[$i] 		= $row['currency'];
                $i++;
            }
            return array($company, $year, $revenue, $fundPeriod, $pDate, $finType, $currency);
        }
    }
    
    public function getLegal(){
        $sql = "SELECT * FROM tgeneral_page where page_id = '16'";
        $result = mysqli_query($this->conn, $sql);

        if (mysqli_num_rows($result) > 0) {
            $i = 0;

            while ($row = mysqli_fetch_assoc($result)) {
                $page_content[$i] = $row['page_content'];
                $i++;
            }

            return array($page_content);
        }
    }
	
	//function to delete campaign update list submitted by member
    public function DeleteCampaignUpdate($Id)
    {
		$Id			= $Id;
		$sql		= 'DELETE FROM tupdate where id IN ('.$Id.')';		
        $result 	= mysqli_query($this->conn, $sql);
		
		return $result;		
    }

    function getDatas($table, $field_id = null, $id = null, $array = false) {
        $query = "SELECT * FROM $table WHERE $field_id = $id";
  
        $result = $this->conn->query($query);
  
        $err = $this->conn->error;
  
        // print_r($err);
        $objs = array();
        if (mysqli_num_rows($result) > 1 || $array) {
          while($row = mysqli_fetch_assoc($result)) {
              $objs[] = $row;
          }
        }else{
          $objs = $result->fetch_object();
        }
  
        // print_r(isset($objs->id));
        // exit();
  
        return $objs;
    }
  
    function saveDatas($fields, $table, $fileUpload = []) {
        $fieldNames = implode(',', array_keys($fields));
        $fieldValues = "'";
  
        $i = 0;
        foreach ($fields as $key => $value) {
          $fieldValues = $fieldValues . $value;
          if ((count($fields) - 1) != $i) { $fieldValues = $fieldValues . "', '"; }else{ $fieldValues = $fieldValues . "'"; }
          $i++;
        }
  
        $query = "INSERT INTO $table ($fieldNames) VALUES ($fieldValues)";
  
        // echo $query;
        // exit();
  
        $result = $this->conn->query($query);
  
        $err = $this->conn->error;
  
        // print_r($err);
        // exit();
  
        return array($result, $err, $this->conn->insert_id);
  
    }
  
    function updateDatas($fields, $table, $field_id, $id) {
        $valueSets = array();
      //   echo $fields['nama'];
      //   print_r($fields);
      //   die;
  
        foreach ($fields as $key => $value) {
          $valueSets[] = $key . " = '" . $value . "'";
        }
  
  
        $query = "UPDATE $table SET ".join(", ",$valueSets)." WHERE $field_id='$id'";
      //   echo $query;
      //   exit();
  
        $result = $this->conn->query($query);
  
        $err = $this->conn->error;
  
        // print_r($err);
        // exit();
  
        return array($result, $err, $id);
    }
  
    function removeData($table, $field_id, $id){
        $query = "DELETE FROM $table WHERE $field_id = $id";
  
        $result = $this->conn->query($query);
        $err = $this->conn->error;
  
        return array($result, $err, $id);
    }

    public function GetBankCodeList()
    {
        $bankCode = $bankName = array();
        $sql         = "SELECT * from bic_code order by bank_name asc";
        $result = mysqli_query($this->conn, $sql);
        if (mysqli_num_rows($result) > 0) {
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $bankCode[$i] = $row['bank_code'];
                $bankName[$i] = $row['bank_name'];
                $i++;
            }
            return array($bankCode, $bankName);
        }
    }

}
