<?php
session_start();
$page = 'myproject';
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();

if ($mysql->Connection()) {
	// get member transaction history
	list($company, $year, $revenue, $fundPeriod, $pDate, $finType, $currency) = $mysql->GetMemberGetFunded($_SESSION["MemberId"]);
}

include("header.php");
include("sidebar.php")
?>
<!-- Main container -->
<main>
<div class="main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<header class="card-header">
					<h4 class="card-title"><strong>Submitted</strong> projects</h4>
					<div class="card-header-actions">
						<a href="https://kapitalboost.com/get-funded"><button class="btn btn-sm btn-info">Submit New Project</button></a>
					</div>
				</header>
				
				<div class="card-body">
					<table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th width="15%">Date</th>
								<th width="18%">Financing Type</th>
								<th>Projects</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								for ($i = 0; $i < count($company); $i++) {
									$invDateHid = date("Ymd", strtotime($pDate[$i]));
							?>

									<tr class="even">
										<td>
											<?= $i + 1 ?>
										</td>
										<td>
											<span style='display:none;'><?= $invDateHid ?></span><?= $pDate[$i] ?>
										</td>
										<td>
											<?= $finType[$i] ?>
										</td>
										<td>
											<div class="row">
												<div class="col-md-3">
													Company <br />
													Year Established <br />
													Est. Annual Revenue <br />
													Funding Period <br />
												</div>
												<div class="col-md-6">
													: <b> <?= $company[$i] ?></b> <br />
													: <?= $year[$i] ?> <br />
													: <?= $currency[$i] ?> <?= number_format($revenue[$i],1,".",",") ?> <br />
													: <?= $fundPeriod[$i] ?> <br />
												</div>
											</div>
										</td>
									</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div><!--/.main-content -->
<?php
include("footer.php");
?>
</body>
</html>
