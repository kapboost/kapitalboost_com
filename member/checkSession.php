<?php
session_start();


if(!isset($_SESSION["MemberEmail"]) or !isset($_SESSION["MemberId"]) or $_SESSION["MemberEmail"]=="" or $_SESSION["MemberId"]=="" or $_SESSION["MemberId"]==NULL or $_SESSION["MemberEmail"]==NULL){ 
	session_unset();
	session_destroy();
	// header("Location: https://kapitalboost.com");
	echo '<script type="text/javascript">
		  window.location = "https://kapitalboost.com"
		  </script>';
}

if (!isset($_SESSION['CREATED'])) {
	$_SESSION['CREATED'] = time();
} else if (time() - $_SESSION['CREATED'] > 86400) {
    // session started more than 1 day ago
	session_unset();
	session_destroy();
	header('Location: login.php');
} else if (time() - $_SESSION['CREATED'] <= 86400) {
	$_SESSION['CREATED'] = time();
}

?>