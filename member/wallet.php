<?php
session_start();
$page = 'dashboard';
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
	// get member transaction history
	list($transDate, $transDesc, $transStats, $transAmount) = $mysql->GetMmeberTransactionHistory($_SESSION["MemberId"]);
	$mysql->AddTrail("Member Visit KB Wallet Dashboard Web"); //add trail to database
	$walletBalance 			= $mysql->GetMmeberBalance($_SESSION["MemberId"]); //get member totak wallet balance
	$bankInfo 				= $mysql->GetBankFullInfo($_SESSION["MemberId"]); // get member bank data
	$totalPendingWithdrawal	= $mysql->GetTotalPendingWithdrawal($_SESSION["MemberId"]); // get total amount of pending withdrawal
}

$bankLastNumber	= substr($bankInfo["account_number"], -4);
$c 				= $_GET["c"];

if (empty($bankInfo['bank_name']) || empty($bankInfo['account_name']) || empty($bankInfo['account_number']) || empty($bankInfo['swift_code'])) {
	$bankButton = "Add New Bank Information";
} else {
	$bankButton = "Change Bank Information";
}

include("header.php");
include("sidebar.php")
?>
<!-- Main container -->
<main>
<div class="main-content">
	<!-- Notification -->
	<?php if($c == md5("SuccessKBWithdraw")){?>
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			Congratulations! Your withdrawal request is on progress...
		</div>
	<?php } ?>
	<?php if($c == md5("SuccessKBBankData")){?>
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			Congratulations! Your bank data is updated...
		</div>
	<?php } ?>
	<!-- End of Notification -->

	<div class="row">
		<div class="col-md-4">
			<div class="card card-shadowed">
				<h4 class="card-title"><strong>KB-Wallet</strong> balance</h4>
				<div class="card-body">
					<h1>$<?=number_format($walletBalance, 1, '.', ',');?></h1>
					<?php
						if($totalPendingWithdrawal!=0){
					?>
						<p class="lead">
							<strong>Pending withdrawal : $<?=number_format($totalPendingWithdrawal, 1, '.', ',');?></strong>
						</p>
					<?php
						} else {
					?>
						<p><em>There is no pending withdrawal</em></p>
					<?php } ?>
				</div>
				<footer class="card-footer flexbox">
					<a href='withdraw.php'  style="width: 50%;">
						<div class="btn btn-info btn-block" style="font-size: 12px; padding: 5px 5px; ">Withdraw fund</div>
					</a>
					<a href='withdraw-list.php'  style="width: 50%">
						<div class="btn btn-cyan btn-block" style="font-size: 12px; padding: 5px 5px; ">Withdrawal history</div>
					</a>
				</footer>
			</div>

			<div class="card card-shadowed">
				<h4 class="card-title"><strong>Bank</strong> accounts</h4>
				<div class="card-body">
					<?php
						if (empty($bankInfo['bank_name']) || empty($bankInfo['account_name']) || empty($bankInfo['account_number'])) {
					?>
							<p><em>You don't have any bank information. Please create a new one.</em></p>
					<?php
						} else {
					?>

						<div class="row">
							<div class="col-md-5">
								<p> Bank Name	</p>
								<p> Acc. Name	</p>
								<p> Acc. Number	</p>
								<p> Swift Code	</p>
							</div>
							<div class="col-md-7">
								<p> :  <b> <?=$bankInfo["bank_name"]?>		</b></p>
								<p> :  <b> <?=$bankInfo["account_name"]?>	</b></p>
								<p> :  <b> x-<?=$bankLastNumber?>			</b></p>
								<p> :  <b> <?=$bankInfo["swift_code"]?>		</b></p>
							</div>
						</div>

					<?php } ?>
				</div>
				<footer class="card-footer flexbox">
					<a href='bank-data.php'  style="width: 100%"><div class="btn btn-info btn-block"><?=$bankButton?></div></a>
				</footer>
			</div>	
		</div>

		<div class="col-md-8">
			<div class="card">
				<h4 class="card-title"><strong>KB-Wallet</strong> activity</h4>
				<div class="card-body">
					<table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th>Date</th>
								<th>Description</th>
								<th>Amount (SGD)</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>No</th>
								<th>Date</th>
								<th>Description</th>
								<th>Amount (SGD)</th>
							</tr>
						</tfoot>
						<tbody>
							<?php 
								for ($i = 0; $i < count($transAmount); $i++) {
									if ($transStats[$i]=="Top Up Balance"){
										$transStats[$i] = "Top-up";
									} else if ($transStats[$i]=="Reduce Balance") {
										$transStats[$i] = "Withdrawal";
									}
									$invDateHid = date("Ymd", strtotime($transDate[$i]));
							?>

									<tr class="even" <?php if ($transStats[$i]=="Withdrawal" or $transStats[$i]=="Investment"){echo "style='color:#e74c3c'";}?>>
										<td>
											<?= $i + 1 ?>
										</td>
										<td>
											<span style='display:none;'><?= $invDateHid ?></span><?= $transDate[$i] ?>
										</td>
										<td>
											<b><?= $transStats[$i] ?></b> <br /><?= $transDesc[$i] ?>
										</td>
										<td>
											<?php 
												if ($transStats[$i]=="Withdrawal" or $transStats[$i]=="Investment"){
													echo "- ";
												}
											?>
											SGD <?= number_format($transAmount[$i],1,".",",") ?>
										</td>
									</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div><!--/.main-content -->
<?php
include("footer.php");
?>
</body>
</html>
