<?php

$timestamp = "2019-02-04 00:37:15";
$start_date = date($timestamp);

$expires = strtotime('+7 days', strtotime($timestamp));
//$expires = date($expires);

$date_diff=($expires-strtotime($timestamp)) / 86400;

echo "Start: ".$timestamp."<br>";
echo "Expire: ".date('Y-m-d H:i:s', $expires)."<br>";

echo round($date_diff, 0)." days left";

if(strtotime($expires)<strtotime("today")){
    echo "past";
} elseif(strtotime($expires)==strtotime("today")){
    echo "today";
} else {
    echo "future";
}

?>