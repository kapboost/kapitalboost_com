<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Responsive admin dashboard and web application ui kit.">
	<meta name="keywords" content="dashboard, index, main, picker">

	<title>Dashboard &mdash; Kapital Boost</title>

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">

	<!-- Styles -->
	<link href="assets/css/core.min.css" rel="stylesheet">
	<link href="assets/css/app.min.css" rel="stylesheet">
	<link href="assets/css/style.min.css" rel="stylesheet">

	<!-- Favicons -->
	<link rel="apple-touch-icon" href="assets/img/apple-touch-icon.png">
	<link rel="icon" href="assets/img/logo-alone.png">
	
	<script>
		(function (i, s, o, g, r, a, m) {
			   i['GoogleAnalyticsObject'] = r;
			   i[r] = i[r] || function () {
					  (i[r].q = i[r].q || []).push(arguments)
			   }, i[r].l = 1 * new Date();
			   a = s.createElement(o),
					   m = s.getElementsByTagName(o)[0];
			   a.async = 1;
			   a.src = g;
			   m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-88456967-1', 'auto');
		ga('send', 'pageview');
	</script>
</head>

<body>

<!-- Preloader -->
<div class="preloader">
	<div class="spinner-dots">
		<span class="dot1"></span>
		<span class="dot2"></span>
		<span class="dot3"></span>
	</div>
</div>