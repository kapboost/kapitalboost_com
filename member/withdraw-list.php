<?php
session_start();
$page = 'dashboard';
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
	list($date, $amount, $status, $proof_payment, $date_transfer) = $mysql->GetMemberWithdrawHistory($_SESSION["MemberId"]);
	$mysql->AddTrail("Member Visit Withdraw History Web");
}

include("header.php");
include("sidebar.php")
?>
<!-- Main container -->
<main>
<div class="main-content">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<h4 class="card-title"><strong>KB-Wallet</strong> withdraw activity</h4>
				<div class="card-body">
					<table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th>Date Requested</th>
								<th>Amount (SGD)</th>
								<th>Status</th>
								<th>Proof of Transfer</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th width="10%">No</th>
								<th>Date Requested</th>
								<th>Amount (SGD)</th>
								<th>Status</th>
								<th>Proof of Transfer</th>
							</tr>
						</tfoot>
						<tbody>
							<?php 
								for ($i = 0; $i < count($date); $i++) {
									$invDateHid = date("Ymd", strtotime($date[$i]));
							?>

									<tr class="even">
										<td>
											<?= $i + 1 ?>
										</td>
										<td>
											<span style='display:none;'><?= $invDateHid ?></span><?= $date[$i] ?>
										</td>
										<td>
											SGD <?= number_format($amount[$i],1,".",",") ?>
										</td>
										<td>
											<?php 
												if ($status[$i]==0){ 
													echo "On Progress";
												} else {
											?>
													Paid on <?= $date_transfer[$i] ?> 
											<?php } ?> 
										</td>
										<td style="text-align: center;">
											<?php
												if ($status[$i]==0){ 
													echo "Your withdrawal still on progress..";
												} else { 
											?> 
													<a href="../assets/images/blog/<?= $proof_payment[$i] ?>">
														<button class="btn btn-w-md btn-outline btn-info">Download</button>
													</a>
											<?php } ?>
										</td>
									</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div><!--/.main-content -->
<?php
	include("footer.php");
?>
</body>
</html>
