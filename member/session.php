<?php
session_start();
include("header.php");
include("sidebar.php");
include_once 'checkSession.php';
include_once 'mysql.php';

$mysql = new mysql();
if ($mysql->Connection()) {
	list($memberName, $memberEmail) = $mysql->GetMemberList();
}
function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}
// echo "'<script>console.log(\"submit : ".$_POST['submit']."\")</script>'";
if (($_POST['submit'])=="Changes Session"){
	$memberchoice = test_input($_POST['memberchoice']);
	if ($mysql->Connection()) {
		list($membersName, $membersEmail, $membersId, $membersCountry, $membersPhone) = $mysql->GetMemberSingleList($memberchoice);
	}  
	
	if (isset($membersName)){
		// unset($_SESSION['cookie']);
		// unset($_SESSION["MemberEmail"]); 
		// session_destroy();
		for ($a = 0; $a < count($membersEmail); $a++) {
			session_start();
			$_SESSION["MemberName"] = $membersName[$a];
			$_SESSION["MemberId"] = $membersId[$a];
			echo "'<script>console.log(\"membersId : ".$membersEmail[$a]."\")</script>'";
			$_SESSION["MemberCountry"] = $membersCountry[$a];
			$_SESSION["MemberPhone"] = $membersPhone[$a];
			$_SESSION["MemberEmail"] = $membersEmail[$a];
			echo "'<script>console.log(\"session : ".$_SESSION["MemberEmail"]."\")</script>'";
			$_SESSION['cookie']['member_id'] = $membersId[$a];
			echo "'<script>console.log(\"member id : ".$_SESSION["MemberId"]."\")</script>'";
		}	
	}	
}


?>
<style>
.dropdown-menu {
	top: 0 !important;
}
</style>
<!-- Main container -->
<main>
	<div class="main-content">
		<div class="row">
			<br><br>
		</div>
		<form action="" method="POST" enctype="multipart/form-data">
			<div class="row">
				<div class="col-12">
					<div class="card">
						<h4 class="card-title"><strong>Change Session</strong></h4>
						<div class="card-body row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="require">Select Member</label>
									<select data-provide="selectpicker" data-live-search="true" class="form-control" name="memberchoice" id="memberchoice" required>
										<option>Select Member</option>
										<?php
										for ($s = 0; $s < count($memberEmail); $s++) {
										?>
											<option value="<?= $memberEmail[$s] ?>"> <?= $memberEmail[$s] ?> - <?= $memberName[$s] ?> </option>
										<?php 
										}
										?>
									</select>
								</div>
								<footer class="card-footer text-right">
									<button class="btn btn-secondary" type="reset">Cancel</button>
									<input class="btn btn-info" type="submit" name="submit" value="Changes Session">
								</footer>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div><!--/.main-content -->
	
	<?php
	include("footer.php");
	?>
</body>
</html>