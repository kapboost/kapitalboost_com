<?php
// session_start();
// include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();
include("header.php");
include("sidebar.php");

if ($mysql->Connection()) {
  list($id, $subject, $date, $content, $status) = $mysql->GetSupportTicketDetail($ticketId);
}

?>

<!-- Main container -->
<main>
	<header class="header">
		<div class="header-action">
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#tab-ticket" role="tab">My Tickets </a>
				</li>
			</ul>
		</div>
	</header>

	<?php
		$xcod=isset($_GET['xcod']) ? $_GET['xcod'] : '';
		if ($xcod==md5("suKses")){
	?>
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Success!</strong> Your ticket already submitted.
	</div>
	<?php } ?>

	<div class="tab-content card card-body">
		<div class="tab-pane active" id="tab-ticket" role="tabpanel"> 
			<div class="main-content">
				<div class="row">
					<hr>
					<table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
						<thead>
							<tr>
								<th width="10%">No</th>
								<th>Subject</th>
								<th width="20%">Created on</th>
								<th width="10%">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$number = 0;
							for ($i = 0; $i < count($subject); $i++) { 
								echo "
								<tr>
									<td>$number</td>
									<td><a href='ticket-detail.php?ticketid=$id[$i]'>$subject[$i]</a></td>
									<td>$date[$i]</td>
									<td>$status[$i]</td>
								</tr>";
								$number++;
							}  
							?>
						</tbody>
					</table>
				</div>
			</div><!--/.main-content -->
		</div>
	</div>

<?php

include("footer.php");

?>
</body>
</html>