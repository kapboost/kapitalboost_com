<?php
// session_start();
// include_once 'checkSession.php';

include_once 'mysql.php';
$mysql = new mysql();

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

if (($_POST['submit'])=="Submit Reply"){
	$replys = test_input($_POST['reply']);
	$ticketIds = test_input($_POST['ticketId']);
	$member_ids = $_SESSION["MemberId"];

	if ($mysql->Connection()) {
		$mysql->InsertNewSupportTicketReply($ticketIds, $replys, $member_ids);
	}
}

$ticketId=isset($_GET['ticketid']) ? $_GET['ticketid'] : '';
$memberId = $_SESSION["MemberId"];

if ($mysql->Connection()) {
	list($id, $subject, $date, $content, $status) = $mysql->GetSupportTicketDetail($ticketId, memberId);
	list($dateReply, $reply, $member_id) = $mysql->GetSupportTicketReplay($ticketId);
	// $mysql->AddTrail("Member Visit Campaign News Detail Dashboard Web: $campaignName");
}



include("header.php");
include("sidebar.php")
?>

<!-- Main container -->
<main>
	<div class="main-content">
		<div class="row">
			<div class="col-md-12 col-lg-12">
				<div class="card">
					<?php
						for ($i = 0; $i < count($subject); $i++) { 
					?>
					<header class="card-header">
						<h4 class="card-title"><strong><?php echo "$subject[$i]"; ?></strong></h4>
					</header>
					<div class="card-body">
						<p><i><?php echo "$date[$i]"; ?></i></p>
						<p><?php echo "$content[$i]"; ?></p>
					<?php } ?>
						<hr>
						<?php
							for ($i = 0; $i < count($reply); $i++) { 
						?>
						<p><i><?php echo "$dateReply[$i]"; ?></i></p>
						<p><?php echo "$reply[$i]"; ?></p>
						<?php } ?>
					</div>
				</div>
				
				<div class="card">
					<h4 class="card-title"><strong>Reply</strong></h4>
					<form action="" method="POST" enctype="multipart/form-data">
						<div class="card-body row"> 
							<div class="col-md-12">
								<div class="form-group">
									<label>Your reply</label>
									<input class="form-control" type="text" name="reply" id="reply" value="" required>
									<input class="form-control" type="hidden" name="ticketId" id="ticketId" value="<?php echo $ticketId;?>" required>
								</div>
							</div>
						</div>
						<footer class="card-footer text-right">
							<input class="btn btn-info" type="submit" name="submit" value="Submit Reply">
						</footer>
					</form>
				</div>
			</div>
		</div>
	</div><!--/.main-content -->

	<?php
	include("footer.php");
	?>
</body>
</html> 