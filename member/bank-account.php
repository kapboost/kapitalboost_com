<?php
session_start();
$page = 'bank-account';
include "header.php";
include "sidebar.php";
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();

//   echo $_SESSION['MemberId']; exit();

	$arrayName = array(
		'member_id' => $_SESSION['MemberId']
	);

  if ($mysql->Connection()) {
  	$new_array = array_merge($_POST, $arrayName);
    
    if (count($_POST) > 0) {

      if(isset($_GET['id'])) {
        list($result, $err, $insert_id ) = $mysql->updateDatas($new_array, 'bank_account', 'id', $_GET['id']);

          if ($result) {
            $msg = "Bank Account Updated";
          }        
      }else{
        list($result, $err, $insert_id ) = $mysql->saveDatas($new_array, 'bank_account');

        if (!$result) {
          print_r($err);
        }
        if ($result) {
          $msg = "Bank Account Added";
        }
      }
    }

    if (isset($_GET['delete']) && isset($_GET['id']) ) {
      list($result, $err, $insert_id ) = $mysql->removeData('bank_account', 'id', $_GET['id']);

      if (!$result) {
  			print_r($err);
  		}
      if ($result) {
        $msg = "Data Berhasil Dihapus";
      }
    }

    $data_bank_account = $mysql->getDatas('bank_account', 'member_id', $_SESSION['MemberId'], true);

    $count = count($data_bank_account);
    // echo $count; exit();

    // print_r($data_bank_account); exit();
  }

?>

<main style="margin-top: 20px;">
  <?php if (isset($msg)): ?>
    <div class="alert alert-success" role="alert">
      <?= $msg ?>
    </div>
  <?php endif; ?>

  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header flex-start">
          
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <h4><strong>Details of Bank Account</strong></h4>
              </div>
              <?php if($count < 1) { ?>
              <div class="col-lg-6">
                <div class="pull-right">
                  <button type="button" class="btn btn-primary btn-sm add-bank-account" data-toggle="modal" data-target="#formBankAccount">
                    Add Your Bank Account
                  </button>
                </div>
              </div>
              <?php } ?>
            </div><hr>
            
            <table class="table table-border table-striped">
              <thead>
                <tr>
                  <th style="width: 5%;">No.</th>
                  <th style="width: 23%;">Account Holder Name</th>
                  <th style="width: 32%;">Bank Name</th>                  
                  <th style="width: 15%;">Account Number</th>
                  <!-- <th style="width: 15%;">Account Domicile</th> -->
                  <th style="width: 10%;">Menu</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1; foreach ($data_bank_account as $bank_account): ?>
                  <?php
                      $data_bic = $mysql->getDatas('bic_code', 'bank_code', $bank_account['bank_code']);
                  ?>
                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $bank_account['account_name'] ?></td>  
                    <td><?= $data_bic->bank_name ?></td> 
                    <td><?= $bank_account['account_number'] ?></td>
                    <!-- <td><//?= $bank_account['country_of_bank_account'] ?></td> -->
                    <td>
                      <a href="javascript:void(0);" class="btn btn-xs btn-warning edit-bank-account" data-toggle="modal" data-target="#formBankAccount" data-url="?id=<?= $bank_account['id'] ?>" data-index="<?= $no-1 ?>" style="display:block;">
                        <i class="fa fa-pencil"></i>
                        Edit
                      </a>

                      <!-- <a href="<//?= "./bank-account.php?delete=true&id=".$bank_account['id'] ?>" class="btn btn-xs btn-danger btn-hapus" style="margin-top: 5px;display:block;">
                        <i class="fa fa-trash"></i>
                        Delete
                      </a> -->
                    </td>
                  </tr>
                <?php $no+=1; endforeach; ?>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="formBankAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document" style="width: 41%;">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Details of Bank Account</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <form action="" method="post">
          <div class="modal-body">
            <input type="hidden" name="member_id" class="form-control" id="member_id" value="<?= $_SESSION['MemberId'] ?>">
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="nama_lengkap">Account Holder Name *</label>
                  <input type="text" class="form-control" name="account_name" id="account_name">
                  <span id="error_name"></span> 
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <label for="alamat_email">Bank Name *</label>
                    <select class="form-control" id="bank_code" name="bank_code">
                        <option class="select-type" value="">-- Choose Bank Name --</option>
                        <?php
                          foreach ($data_bank_account as $bank_account):
                              $data_bic = $mysql->getDatas('bic_code', 'bank_code', $bank_account['bank_code']);
                          endforeach;

                          if ($mysql->Connection()) {
                            list($bankCode, $bankName) = $mysql->GetBankCodeList();
                          }

                          for ($i = 0; $i < count($bankCode); $i++) {
                            echo "<option value='$bankCode[$i]' ".($bankCode[$i] == $data_bic->bank_code ? 'selected' : '').">$bankName[$i]</option>";
                          } ?>                                                                                                                                
                    </select>
                  <span id="error_bank"></span>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-lg-7">
                <div class="form-group">
                  <label for="no_telepon">Account Number *</label>
                  <input type="text" class="form-control" name="account_number" id="account_number">
                  <small><b>Notes: Please input numerical digits only </b></small>
                  <span id="error_number"></span>
                </div>
              </div>
            </div>

            <!-- <div class="row">
              <div class="col-lg-7">
                <div class="form-group">
                  <label for="no_telepon">Account Domicile *</label>
                  <input type="text" class="form-control" name="country_of_bank_account" id="country_of_bank_account">
                  <span id="error_domicile"></span>
                </div>
              </div>
            </div> -->
                        
          </div>
          
          <div class="modal-footer">
            <button type="reset" class="btn btn-default">Reset</button>
            <button type="submit" class="btn btn-primary btn-validasi">Save</button>
          </div>
          
        </form>
      </div>
    </div>
  </div>
	<?php include("footer.php"); ?>
</main>

<?php $passing_bank_account = json_encode($data_bank_account); ?>

<style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }

    input[type=number] {
        -moz-appearance:textfield; /* Firefox */
    }
</style>

<script src="/member/assets/js/select2.min.js"></script>
<script src="/member/assets/js/jquery.mask.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#account_number').bind('keyup paste', function(){
      this.value = this.value.replace(/[^0-9]/g, '');
    });

    // var elements = document.getElementsByTagName("INPUT");
    // for (var i = 0; i < elements.length; i++) {
    //     elements[i].oninvalid = function(e) {
    //         e.target.setCustomValidity("");
    //         if (!e.target.validity.valid) {
    //             e.target.setCustomValidity("DIISI WOY");
    //         }
    //     };
    //     elements[i].oninput = function(e) {
    //         e.target.setCustomValidity("");
    //     };
    // }

    $data_bank_account = JSON.parse('<?= $passing_bank_account ?>');

    $(".edit-bank-account").click(function() {
      data_index = $(this).data('index');
      data_url = $(this).data('url');
      

      $("#formBankAccount form").attr('action', data_url);

      $.each($data_bank_account[data_index], function(key, value) {
        if ($("[name="+key+"]")) {
          if ($("[name="+key+"]").attr('type') == 'text' || $("[name="+key+"]").prop('tagName') == 'SELECT' || $("[name="+key+"]").attr('type') == 'email') {
            $("[name="+key+"]").val(value);
          }else if($("[name="+key+"]").prop('tagName') == 'TEXTAREA') {
            $("[name="+key+"]").text(value);
          }
        }
      });
    });

    $(".add-bank-account").click(function() {
      $("#formBankAccount form").attr('action', '');
      $("#formBankAccount form").attr('action', '/member/bank-account.php');
    });

    setTimeout(function(){
      $(".alert").fadeOut(500);
    }, 3000);

    $(".btn-hapus").click(function() {
      if (confirm("Are you sure want to delete this bank account?")) {
        return true;
      } else {
        return false;
      }
    });

    $(".btn-validasi").click(function() {
      var account_name = document.getElementById("account_name");	
      var bank_code = document.getElementById("bank_code");	
      var account_number = document.getElementById("account_number");	
      var account_domicile = document.getElementById("country_of_bank_account");	

			if (account_name.value == "") {
        document.getElementById("error_name").innerHTML = "<p style='color: #c0392b; font-size: 12px;'><i>Please fill out this field<i></p>";
        return false;
			} else if (bank_code.value == "") {
        document.getElementById("error_bank").innerHTML = "<p style='color: #c0392b; font-size: 12px;'><i>Please fill out this field<i></p>";
        return false;
			} else if (account_number.value == "") {
        document.getElementById("error_number").innerHTML = "<p style='color: #c0392b; font-size: 12px;'><i>Please fill out this field<i></p>";
        return false;
			} else if (account_domicile.value == "") {
        document.getElementById("error_domicile").innerHTML = "<p style='color: #c0392b; font-size: 12px;'><i>Please fill out this field<i></p>";
        return false;
			} else {
        return true;
      }

    });

  });
  
  function setInputFilter(textbox, inputFilter) {
  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      }
    });
   });
  }


  // Install input filters.

  setInputFilter(document.getElementById("no_rek"), function(value) {
   return /^\d*$/.test(value); });

  
  
</script>
</body>
</html>
