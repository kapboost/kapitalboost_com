<?php
session_start();
include "header.php";
include "sidebar.php";
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($mysql->Connection()) {
	$walletBalance	= $mysql->GetMmeberBalance($_SESSION["MemberId"]); //get current wallet balance
	$bankInfo		= $mysql->GetBankFullInfo($_SESSION["MemberId"]); //get bank data of user
}

//post bank data to database
if (($_POST['submit']) == "Save Changes") {
    $bank_name      = test_input($_POST['bank_name']);
    $account_name   = test_input($_POST['account_name']);
    $account_number = test_input($_POST['account_number']);
    $swift_code     = test_input($_POST['swift_code']);

    if ($mysql->Connection()) {
        $mysql->UpdateBankData($bank_name, $account_name, $account_number, $swift_code, $_SESSION["MemberId"]); //update user databank on database
        $mysql->AddTrail("Bank Info Update Profile Dashboard Web"); //add trail to database

		//redirect to wallet mainpage
        echo '<script type="text/javascript">
			window.location = "wallet.php?c='.md5("SuccessKBBankData").'"
		</script>';
    }
}
?>
<!-- Main container -->
<main>
	<div class="main-content">
		<br>
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<h4 class="card-title"><strong>Input Your Bank Data</strong></h4>
					<div class="card-body">
						<form action="" method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="require" for="input-required">Bank Name</label>
									<input type="text" class="form-control" name="bank_name" id="input-normal" value="<?=$bankInfo['bank_name']?>" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="require" for="input-required">Acount/IBAN Number</label>
									<input type="text" class="form-control" name="account_number" id="input-normal" value="<?=$bankInfo['account_number']?>" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label class="require" for="input-required">Account Name</label>
									<input type="text" class="form-control" name="account_name" id="input-normal" value="<?=$bankInfo['account_name']?>" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="input-normal">Swift Code</label>
									<input type="text" class="form-control" name="swift_code" id="input-normal" value="<?=$bankInfo['swift_code']?>">
								</div>
							</div>
							<div class="col-md-12">
								<input class="btn btn-info" type="submit" name="submit" value="Save Changes">
							</div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div><!--/.main-content -->
<?php
include "footer.php";
?>

</body>
</html>
