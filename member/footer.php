<!-- Footer -->
<footer class="site-footer">
	<div class="row">
		<div class="col-md-12">
			<p class="text-center text-md-left">Copyright © 2018 <a href="http://kapitalboost.com">Kapital Boost</a>. All rights reserved.</p>
		</div>
	</div>
</footer>
<!-- END Footer -->

</main>
<!-- END Main container -->


<!-- Scripts -->
<script src="assets/js/core.min.js"></script>
<script src="assets/js/app.min.js"></script>
<script src="assets/js/script.min.js"></script>
