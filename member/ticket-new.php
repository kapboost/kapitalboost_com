<?php
session_start();
include_once 'checkSession.php';

include_once 'mysql.php';
$mysql = new mysql();

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

if (($_POST['submit'])=="Submit Ticket"){
	$content = test_input($_POST['content']);
	$subject = test_input($_POST['subject']);
	$member_id = $_SESSION["MemberId"];

	if ($mysql->Connection()) {
		$mysql->InsertNewSupportTicket($content, $member_id, $subject);
		$suksesNotif = md5("suKses");
		header("Location: ticket.php?xcod=$suksesNotif");
		exit();
	}
}

include("header.php");
include("sidebar.php")
?>

<!-- Main container -->
<main>
	<div class="main-content"> 
		<div class="row">
			<div class="col-md-12 col-lg-12">		
				<div class="card">
					<h4 class="card-title"><strong>Submit Ticket</strong></h4>
					<form action="" method="POST" enctype="multipart/form-data">
						<div class="card-body row">
							<div class="col-md-12">
								<div class="form-group">
									<label class="require">Name</label>
									<input class="form-control" type="text" name="name" id="name" value="<?php echo $_SESSION["MemberName"]; ?>"  required readonly>
								</div>
								<div class="form-group">
									<label class="require">Email</label>
									<input class="form-control" type="text" name="email" id="email" value="<?php echo $_SESSION["MemberEmail"]; ?>"  required readonly>
								</div>
								<div class="form-group">
									<label class="require">Subject</label>
									<input class="form-control" type="text" name="subject" id="subject" value=""  required>
								</div>
								<div class="form-group">
									<label class="require">Content</label>
									<textarea class="form-control" name="content" id="content" rows="6"></textarea>
								</div>
							</div>
						</div>
					<footer class="card-footer text-right">
						<input class="btn btn-info" type="submit" name="submit" value="Submit Ticket">
					</footer>
					</form>	
				</div>
			</div>
		</div>
	</div><!--/.main-content -->

	<?php
	include("footer.php");
	?>
</body>
</html> 