<?php
session_start();
$page = 'dashboard';
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if ($mysql->Connection()) {
	$walletBalance 		= $mysql->GetMmeberBalance($_SESSION["MemberId"]);
	$bankInfo 			= $mysql->GetBankFullInfo($_SESSION["MemberId"]);
	$bankLastNumber 	= substr($bankInfo["account_number"], -4);
}

if (($_POST['submit']) == "Withdraw") {
    $amountRequested	= test_input($_POST["nominal_value"]);
    $new_value     		= $walletBalance - $amountRequested;
    if ($new_value < 0) {
        echo "' <script> alert(\" Your balance is not sufficient! \")  </script> '";
		echo "<script>window.location = window.location.href;</script>";
    } elseif ($new_value > $walletBalance) {
        echo "' <script> alert(\" Enter positive value! \")  </script> '";
		echo "<script>window.location = window.location.href;</script>";
    } elseif ($amountRequested == 0) {
        echo "' <script> alert(\" Value can not be 0! \")  </script> '";
		echo "<script>window.location = window.location.href;</script>";
    } else {
        
        if ($mysql->Connection()) {
            $mysql->InsertNewWithdrawRequest($amountRequested, $_SESSION["MemberId"]);
            $mysql->updateWallet($new_value, $_SESSION["MemberId"]);
			$memberData = $mysql->GetMemberDataDetail($_SESSION["MemberId"]);
            $mysql->AddTrail("Member Withdraw");
			
			$message = "
			 <html>
				   <head>
						  <title>Kapital Boost's Withdrawal Request</title>
				   </head>
				   <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
						  <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
								 <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
										<img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
										<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
											   Dear " . $memberData['member_firstname'] . ",
										</p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
											   Your request to withdraw SGD$amountRequested from your e-wallet is currently being processed. Please allow 2-5 working days for the funds to reach your bank account.
										</p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
												Once transferred, we will upload the proof of transfer into your <a href='https://kapitalboost.com/member/withdraw-list.php'>Withdrawal Activity Page</a>.
										</p>
										 
										<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
											   Best regards,<br><br>Kapital Boost
										</p>
								 </div>
								 <div style='width:100%;height:7px;background-color:#00b0fd'></div>
								 <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
										<p style='font-size: 12px;color: white;text-align:center'>
											   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
										</p>
								 </div>
						  </div>
				   </body>
			</html>
			";

			include_once 'email.php';
			$email = new email();
			// echo "'<script>console.log(\"sql : ".$memberData['member_email']."\")</script>'";  
			$email->SendEmail($memberData['member_email'], "admin@kapitalboost.com", "Kapital Boost - Withdrawal Request", $message);
						
            echo '<script type="text/javascript">
			  window.location = "wallet.php?c='. md5("SuccessKBWithdraw").'"
			  </script>';
        }
    }
}

if (empty($bankInfo['bank_name']) || empty($bankInfo['account_name']) || empty($bankInfo['account_number'])) {
    echo '';
    echo '<script type="text/javascript">
	  window.location = "bank-data.php"
	  </script>';
}

include("header.php");
include("sidebar.php")
?>
<!-- Main container -->
<main>
<div class="main-content">
	<div class="row">		
		<div class="col-md-6">
			<div class="card">
				<h4 class="card-title"><strong>Withdraw</strong> balance</h4>
				<form action="" method="POST" enctype="multipart/form-data">
					<div class="card-body">
						<p class="lead"> 
							<strong>Available Balance</strong> <br />
							$<?=number_format($walletBalance, 1, '.', ',');?>
						</p>
						<p class="lead"> 
							<strong>Bank Account</strong> <br />
							<?=$bankInfo["bank_name"]?>,  x-<?=$bankLastNumber?>
						</p>
						<p class="lead"> 
							<strong>How much would you like to withdraw?</strong> <br />
							<div class="input-group"  style="width:50%;">
								<span class="input-group-addon">$</span>
								<input type="number" class="form-control" name="nominal_value" aria-label="Amount (to the nearest dollar)" value="" required>
							</div>
						</p>
					</div>
					<footer class="card-footer text-right">
						<input class="btn btn-info" name="submit" type="submit" value="Withdraw">
					</footer>
				</form>
			</div>
		</div>
	</div>

</div><!--/.main-content -->
<?php
	include("footer.php");
?>
</body>
</html>
