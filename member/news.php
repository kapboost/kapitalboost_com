<?php
session_start();
include_once 'checkSession.php';
$page = "cupdate";
include_once 'mysql.php';
$mysql = new mysql();

if ($mysql->Connection()) {
	//get all campaign updates based on member investment
	list($Id, $campaingId, $campaignName, $campaignBackground, $updateTitle, $updateContent, $updateDate, $updateID) = $mysql->GetProjectNews($_SESSION["MemberId"]); 
	$mysql->AddTrail("Member Visit Campaign Update News Dashboard Web"); //add trail to database
}
$totalNews	= count($campaingId);
include("header.php");
include("sidebar.php")
?>

<!-- Main container -->
<main>
<div class="main-content">

	<!-- <div class="row">
		<div class="media-list-body bg-opacity-0 b-0">
			<div class="media align-items-center">
				<input type="checkbox" class="check_all"/>
				<button type="button" name="btn_delete" id="btn_delete" class="btn btn-danger">Delete selected</button>
				<a class="" style="min-width: 710px;"></a>
			</div>
		</div>
	</div>   -->

	<div class="row">
		<div class="media-list-body bg-white b-1">
			<?php
				for ($i = 0; $i < count($campaingId); $i++) {
					// $updateContent[$i] = substr($updateContent[$i],0,40);
			?>
			<div class="media align-items-center">
				<input type="checkbox" class="chk_boxes1" name="id[]" value="<?php echo $Id[$i] ?>"/>
				
				<a href="#">
					<img class="avatar" src="https://kapitalboost.com/assets/images/campaign/<?=$campaignBackground[$i]; ?>" >
				</a>

				<a class="media-body text-truncate" href="news-detail.php?updateid=<?=$updateID[$i];?>"  style="min-width: 770px;">
					<h4>[<?=$campaignName[$i]; ?>] - <?=$updateTitle[$i]; ?></h4>
					<small>
						<span class="">
							<?php 
								if (strlen($updateContent[$i])<200) {
									echo $updateContent[$i];
								} else {
									$string = substr($updateContent[$i],0,160).'...'; 
									echo strip_tags($string);
								}
							?>
						</span>
					</small>
				</a>
				<a class="text-lighter fs-18 opacity-0" href="#"><i class="ti-clip"></i></a>
				<time><?=$updateDate[$i]; ?></time>
			</div>
			<hr style="margin:0;padding:0;">
			<?php }	?>
		</div>
		<?php
		
			if ($totalNews == 0){
				echo "
				<div class='media align-items-center' style=' width:100%;'>
				 <p style='text-align:center; width:100%;'>You don't have any recent campaigns update...</p>
				</div>
				";
			}
		
		?>
	</div>  
</div><!--/.main-content -->

<?php
include("footer.php");
?>
</body>

<script>
  $(document).ready(function(){
	  $('.chk_boxes1').click(function(){
		  if($(this).is(':checked')){
			  $(this).closest('tr').addClass('removeRow');
		  } else {
			  $(this).closest('tr').removeClass('removeRow');
		  }
	  });


	$('#btn_delete').click(function(){
	  if(confirm("Are you sure you want to delete this update?")){
		var id = [];

		$(':checkbox:checked').each(function(i){
		  id[i] = $(this).val();
		});

		if(id.length === 0){
		  alert("Please select at least one data");
		}else{
			
			id_array = new Array()
			i = 0;
			$("input.chk_boxes1:checked").each(function(){
				id_array[i]=$(this).val();
				i++;
			})	
		
		  $.ajax({
			url:'API.php',
			method:'POST',
			// data:{id:id},
			data: {action:'deleteCampaignUpdate', id:''+id_array},
			success:function(){
			  // for(var i=0; i<id.length; i++){
				// $('tr#'+id[i]+'').fadeOut('slow');
			  // }
			  location.reload();
			}
		  });
		}
	  } else {
		return false;
	  }
	});

	$('.check_all').click(function() {
		$('.chk_boxes1').prop('checked', this.checked);
		if($(this).is(':checked')){
			$('.check').addClass('removeRow');
		} else {
			$('.check').removeClass('removeRow');
		}
	});
  });
</script>

</html>