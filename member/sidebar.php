
<!-- Sidebar -->
<aside class="sidebar sidebar-icons-right sidebar-icons-boxed sidebar-expand-lg">
  <header class="sidebar-header" style="background-color: #fff !important;">
    
    <span class="logo">
      <!--Investor Dashboard -->
	  <a href="https://kapitalboost.com/"><img src="https://kapitalboost.com/assets/images/logo-baru.png" alt="KapitalBoost Logo" style="max-width: 200px; height: auto;"></a>
    </span>
    
    
  </header>

  <nav class="sidebar-navigation">
    <ul class="menu">
     
     <li class="menu-item <?php echo ($page == 'dashboard') ? 'active' : '';?>">
      <a class="menu-link" href="index.php">
        <span class="icon fa fa-home"></span>
        <span class="title">Dashboard</span>
      </a>
    </li>
    <li class="menu-item <?php echo ($page == 'portfolio') ? 'active' : '';?>">
      <a class="menu-link" href="investment.php">
        <span class="icon fa fa-money"></span>
        <span class="title">Portfolio</span>
      </a>
    </li>
    <li class="menu-item <?php echo ($page == 'cupdate') ? 'active' : '';?>">
      <a class="menu-link" href="news.php">
        <span class="icon fa fa-send-o"></span>
        <span class="title">Campaign Updates</span>
      </a>
    </li>
    <li class="menu-item <?php echo ($page == 'profile') ? 'active' : '';?>">
      <a class="menu-link" href="profile.php">
        <span class="icon fa fa-user"></span>
        <span class="title">Profile</span>
      </a>
    </li>
    <li class="menu-item <?php echo ($page == 'bank-account') ? 'active' : '';?>">
      <a class="menu-link" href="bank-account.php">
        <span class="icon fa fa-bank"></span>
        <span class="title">Bank Account</span>
      </a>
    </li>
    <li class="menu-item <?php echo ($page == 'myproject') ? 'active' : '';?>">
      <a class="menu-link" href="my-project.php">
        <span class="icon fa fa-briefcase"></span>
        <span class="title">My Projects</span>
      </a>
    </li>
    <!--<li class="menu-item">
      <a class="menu-link" href="https://kapitalboost.com/campaign/">
        <span class="icon fa fa-server"></span>
        <span class="title">View Open Campaigns</span>
      </a>
    </li>-->
    <li class="menu-item">
      <a class="menu-link" href="logout.php">
        <span class="icon fa fa-power-off"></span>
        <span class="title">Logout</span>
      </a>
    </li>

  </ul>
</nav>

</aside>
<!-- END Sidebar -->


<!-- Topbar -->
<header class="topbar">
  <div class="topbar-left">
    <span class="topbar-btn sidebar-toggler"><i>&#9776;</i></span>
    <h3><i><?php echo $_SESSION["MemberName"]; ?>'s dashboard </i></h3>
  </div>

      <!--<div class="topbar-right">
        <ul class="topbar-btns">
          <li class="dropdown">
            <span class="topbar-btn" data-toggle="dropdown"><img class="avatar" src="assets/img/avatar/1.jpg" alt="..."></span>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="profile.php"><i class="ti-user"></i> Profile</a>
              <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.php"><i class="ti-power-off"></i> Logout</a>
            </div>
          </li>
        </ul>

      </div>-->
    </header>
    <!-- END Topbar -->

