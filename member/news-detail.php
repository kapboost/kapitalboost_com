<?php
session_start();
$page = "cupdate";
include_once 'checkSession.php';

include_once 'mysql.php';
$mysql = new mysql();

$updateID = isset($_GET['updateid']) ? $_GET['updateid'] : '';

if ($mysql->Connection()) {
	// get detail of a campaign update
	list($campaignName, $campaignBackground, $updateTitle, $updateContent, $updateDate, $images1, $images2, $images3, $images4, $images5, $images6, $images7) = $mysql->GetSingleProjectNews($updateID);
	$mysql->AddTrail("Member Visit Campaign News Detail Dashboard Web: $campaignName"); //add trail to database
}
$dateNew = date("d F Y", strtotime($updateDate[0]));
include("header.php");
include("sidebar.php");
$checkLastExtImages1 = substr($images1[0], -4);
$checkLastExtImages2 = substr($images2[0], -4);
$checkLastExtImages3 = substr($images3[0], -4);
$checkLastExtImages4 = substr($images4[0], -4);
$checkLastExtImages5 = substr($images5[0], -4);
$checkLastExtImages6 = substr($images6[0], -4);
$checkLastExtImages7 = substr($images7[0], -4);
?>

<!-- Main container -->
<main>
<div class="main-content">
	<div class="row">
		<div class="col-md-12 col-lg-12">
			<div class="card">
				<header class="card-header">
					<h4 class="card-title"><strong><?=$campaignName[0];?></strong>
					</h4>
					
				</header>
				<div class="card-body">
					<p> <h3 style="text-align:center;"> <?=$updateTitle[0];?>  <br />
					<small class="sidetitle">Updated on <?=$dateNew;?></small></h3></p>
					<p><?=$updateContent[0];?></p>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
	 <?php if($images1[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
			<?php
				if ($checkLastExtImages1 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images1[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages1 == ".png" or $checkLastExtImages1 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images1[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	 <?php if($images2[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
		  
			<?php
				if ($checkLastExtImages2 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images2[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages2 == ".png" or $checkLastExtImages2 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images2[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	 <?php if($images3[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
			<?php
				if ($checkLastExtImages3 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images3[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages3 == ".png" or $checkLastExtImages3 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images3[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	 <?php if($images4[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
			<?php
				if ($checkLastExtImages4 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images4[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages4 == ".png" or $checkLastExtImages4 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images4[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	 <?php if($images5[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
			<?php
				if ($checkLastExtImages5 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images5[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages5 == ".png" or $checkLastExtImages5 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images5[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	 <?php if($images6[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
			<?php
				if ($checkLastExtImages6 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images6[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages6 == ".png" or $checkLastExtImages6 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images6[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	 <?php if($images7[0]!="") { ?>
		   
	  <div class="col-md-4 col-lg-4">
		<div class="card">
		  <div class="card-body">
			<?php
				if ($checkLastExtImages7 == ".pdf") {
			?>
					<a href="https://www.kapitalboost.com//assets/images/blog/<?=$images7[0]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
			<?php
				} else if ($checkLastExtImages7 == ".png" or $checkLastExtImages7 == ".jpg") {
			?>
					<img src="https://www.kapitalboost.com//assets/images/blog/<?php echo $images7[0]; ?>" style="width:100%;height:auto;">
			<?php } ?>
		  </div>
		</div>
	  </div>
	  
	  <?php } ?>
	</div>
</div><!--/.main-content -->

<?php
include("footer.php");
?>
</body>
</html> 