<?php
$subject = "Account Activation";
$message = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta  name="viewport" content="width=display-width, initial-scale=1.0, maximum-scale=1.0," />
        <title>Biz</title>
    
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet" type=\'text/css\'/>
        
        <style type="text/css">
            html { 
                width: 100%; 
            }
            body 
                {margin:0; padding:0; width:100%; -webkit-text-size-adjust:none; -ms-text-size-adjust:none;
            }
            img { 
                display: block !important; border:0; -ms-interpolation-mode:bicubic;
            }

            .ReadMsgBody { 
                width: 100%;
            }
            .ExternalClass {
                width: 100%;
            }
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
                line-height: 100%; 
            }
            .images {
                display:block !important; width:100% !important;
            }

            .heading {
                font-family: Open Sans, Arial, Helvetica Neue, Helvetica, sans-serif !important;
            }	
            .MsoNormal {
                font-family:\'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
            }			
             p {
                margin:0 !important; padding:0 !important;
             }

            .im {
                color: #222222 !important;
            }
              
             a {
                font-family:\'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
             }		
            .display-button td, .display-button a  {
                font-family: \'Open Sans\', Arial, Helvetica Neue, Helvetica, sans-serif !important;
            }
            .display-button a:hover {
                text-decoration:none !important;
            }	


            /* MEDIA QUIRES */

              @media only screen and (max-width:640px) {
              
                body {
                    width:auto !important;
                }
                table[class=display-width] {
                    width:100% !important;
                }
                table[class=display-width-1] {
                    width:80% !important;
                }
                table[class=responsive] {
                    width:280px !important;
                }
                td[class=hide-height] {
                    display:none !important;
                }
            }

            @media only screen and (max-width:480px) {
            
                table[class=display-width] table {
                    width:100% !important;
                }
                table[class=display-width] .button-width .display-button {
                    width:auto !important;
                }
                table[class=display-width] table[class=responsive] {
                    width:280px !important;
                }
                .menu-hide-height {
                    display:none !important;
                }
            }
            
            @media only screen and (max-width:350px) {	
            
                table[class=display-width] table[class=responsive] {
                    width:100% !important;
                }								
            }	
            
            @media only screen and (max-width:320px) {	
            
                .menu {
                    font-size:11px !important;
                }								
            }	
            
        </style>
    </head>
<body>

    <!-- VIEW ONLINE STARTS -->
        
    <table align="center" bgcolor="#ecf0f1" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td height="30"></td>
        </tr>
    </table>		
        
    <!-- VIEW ONLINE ENDS -->

    <!-- HEADER MENU STARTS -->
    
    <table align="center" bgcolor="#ecf0f1" border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>			
            <tr>
                <td align="center">
                    <!--SECTION TABLE-680-->
                    <table align="center" border="0" bgcolor="#ecf0f1" cellpadding="0" cellspacing="0" class="display-width" width="680">
                        <tbody>
                            <tr>
                                <td align="center" style="padding:0 30px;">
                                    <!--SECTION TABLE-600-->
                                    
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">				
                                        <tbody>
                                            <tr>
                                                <td height="10"></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                
                                                    <!--TABLE LEFT-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="25%" style=" border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
                                                        <tbody>
                                                            <tr>
                                                                <td align="center" valign="top" style="color:#666666;">
                                                                    <a href="#" style="color:#666666; text-decoration:none;">
                                                                     <img src="http://www.kapitalboost.com/assets/images/logo-baru.png" alt="Kapital Boost" width="150" height="25" />
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>  
                                                    
                                                    <!--TABLE MIDDLE-->
                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="1" style=" border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
                                                        <tbody>
                                                            <tr>
                                                                <td height="20"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>					
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10"></td>
                                            </tr>
                                        </tbody>	
                                    </table>
                                    <!--SECTION TABLE-600 ENDS-->
                                </td>
                            </tr>
                        </tbody>	
                    </table>
                    <!--SECTION TABLE-680 ENDS-->
                </td>
            </tr>					
        </tbody>	
    </table>
    <!-- HEADER MENU ENDS -->
    
    <!-- ONE COLUMN : ABOUT STARTS -->
    <table align="center" bgcolor="#ecf0f1" border="0" cellpadding="0" cellspacing="0" width="100%">
							<tbody>
								<tr>
									<td align="center">
										<!--SECTION TABLE-680-->
										
										<table align="center" border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="display-width" width="680">
											<tbody>
												<tr>
													<td align="center" style="padding:0 30px;">
														<!--SECTION TABLE-600-->
													
														<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">
															<tbody>
																<tr>
																	<td height="60"></td>
																</tr>	
																<tr>
																	<td align="left">													
																		<table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%">
																			<tbody>											
																				<tr>
																					<td align="left" class="MsoNormal" style="color:#666666;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; line-height:24px;">
																						<p>
																							<b>Hi Wei Siang</b>, <br /><br />
																							Your account is almost ready! To activate, please click below.
																						</p>
																					</td>
																				</tr>															
																			</tbody>	
																		</table>													
																	</td>
																</tr>
																<tr>
																	<td height="30"></td>
																</tr>
																<tr>
																	<td align="center" valign="middle" class="button-width">
																		<table align="center" bgcolor="#56a1d9" border="0" cellspacing="0" cellpadding="0" class="display-button" width="50%"> <!-- USING TABLE AS BUTTON -->
																			<tr>
																				<td align="center" valign="middle" class="MsoNormal" width="50%" style="color:#ffffff;font-family:sans-serif,Arial,Helvetica,Lato;font-size:14px;font-weight:bold;letter-spacing:1px;padding:16px 16px;text-transform:capitalize;">
																					<a href="https://kapitalboost.com/verify/990aeafc8581e267a497730f125c1f851527566879" style="color:#ffffff;text-decoration:none;margin: 0 auto;">Activate My Account</a>
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
																<tr>
																	<td height="30"></td>
																</tr>	
																<tr>
																	<td align="left">													
																		<table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%">
																			<tbody>											
																				<tr>
																					<td align="left" class="MsoNormal" style="color:#666666;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; line-height:24px;">
																						<p>
																							If you have any questions, feel free to contact us at <a href="mailto:support@kapitalboost.com" style="text-decoration:none;">support@kapitalboost.com</a>.
																						</p>
																					</td>
																				</tr>															
																			</tbody>	
																		</table>													
																	</td>
																</tr>
																<tr>
																	<td height="60"></td>
																</tr>
															</tbody>	
														</table>
														
														<!--SECTION TABLE-600 ENDS-->
													</td>
												</tr>														
											</tbody>	
										</table>
										<!--SECTION TABLE-680 ENDS-->
									</td>
								</tr>					
							</tbody>	   
                        </table>
                        <!-- ONE COLUMN : ABOUT ENDS -->
			
			
			<!-- FOOTER-1 STARTS -->
			
			<table align="center" bgcolor="#ecf0f1" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
						
							<!--SECTION TABLE-680-->
							
							<table align="center" border="0" bgcolor="#ecf0f1" cellpadding="0" cellspacing="0" class="display-width" width="680">
								<tbody>							
									<tr>
										<td align="center" style="padding:0 30px;">
										
											<!--SECTION TABLE-600-->
											
											<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">				
												<tbody>	
													<tr>
														<td height="20"></td>
													</tr>
													<tr>
														<td>  
														
															<!-- TABLE LEFT -->
															
															<table align="left" border="0" class="display-width" cellspacing="0" cellpadding="0" width="45%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
																<tr>		
																	<td align="left">
																		<table align="left" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;">
																			<tr>
																				<td align="left" class="heading" style="color:#222222;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:18px;font-weight:600;letter-spacing:1px;line-height:28px;text-transform:capitalize;">
																					Address
																				</td>
																			</tr>	
																			<tr>
																				<td height="10"></td>
																			</tr>
																			<tr>
																				<td align="left" class="MsoNormal" style="color:#222222; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif;font-size:14px;line-height:24px;letter-spacing:1px;">
																					150 Changi Rd #04-07 <br /> Singapore 419973
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>
															
															<table align="left" border="0" cellpadding="0"  cellspacing="0" class="display-width" width="45" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
																<tr>
																	<td style="line-height:40px;" height="40" width="5%"></td>
																</tr>															
															</table>
															

															<!-- TABLE CENTER -->
															<table align="left" border="0" class="display-width" cellspacing="0" cellpadding="0" width="45%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
																<tr>		
																	<td align="left">
																		<table align="left" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;">
																			<tr>
																				<td align="left" class="heading" style="color:#222222;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:18px;font-weight:600;letter-spacing:1px;line-height:28px;text-transform:capitalize;">
																					Contact Us
																				</td>
																			</tr>	
																			<tr>
																				<td height="10"></td>
																			</tr>
																			<tr>
																				<td align="left" class="MsoNormal" style="color:#222222; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:15px; font-weight:400; line-height:24px; letter-spacing:1px;">
																					Mail: <a href="mailto:hello@kapitalboost.com " style="color:#222222;text-decoration:none;">hello@kapitalboost.com </a>
																					<br />
																					Phone: +65 6342 5473 
																				</td>
																			</tr>
																			
																		</table>
																	</td>
																</tr>
															</table>

															<table align="left" border="0" cellpadding="0"  cellspacing="0" class="display-width" width="1" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
																<tr>
																	<td style="line-height:40px;" height="40" width="1"></td>
																</tr>															
															</table>
																
															<!-- TABLE RIGHT -->
															<!-- <table align="right" border="0" class="display-width" cellspacing="0" cellpadding="0" width="25%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;"> -->
																<!-- <tr>		 -->
																	<!-- <td align="left"> -->
																		<!-- <table align="left" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;"> -->
																			<!-- <tr> -->
																				<!-- <td align="left" class="heading" style="color:#222222;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:18px;font-weight:600;letter-spacing:1px;line-height:28px;text-transform:capitalize;"> -->
																					<!-- Follow Us -->
																				<!-- </td> -->
																			<!-- </tr>	 -->
																			<!-- <tr> -->
																				<!-- <td height="20"></td> -->
																			<!-- </tr> -->
																			<!-- <tr>		 -->
																				<!-- <td align="left"> -->
																					<!-- <table align="left" border="0" cellspacing="0" cellpadding="0" style="width:auto !important;"> -->
																						<!-- <tr> -->
																							<!-- <td align="center"> -->
																								<!-- <a href="#" style="color:#666666; text-decoration:none;"> -->
																									<!-- <img src="http://www.pennyblacktemplates.com/demo/pbt/biz/images/25x25x1.png" alt="25x25x1" width="25" style="margin:0;border:0;padding:0;display:block;" /> -->
																								<!-- </a> -->
																							<!-- </td> -->
																							<!-- <td width="10"></td> -->
																							<!-- <td align="center"> -->
																								<!-- <a href="#" style="color:#666666; text-decoration:none;"> -->
																									<!-- <img src="http://www.pennyblacktemplates.com/demo/pbt/biz/images/25x25x2.png" alt="25x25x2" width="25" style="margin:0;border:0;padding:0;display:block;" /> -->
																								<!-- </a> -->
																							<!-- </td> -->
																							<!-- <td width="10">&nbsp;</td> -->
																							<!-- <td align="center"> -->
																								<!-- <a href="#" style="color:#666666; text-decoration:none;"> -->
																									<!-- <img src="http://www.pennyblacktemplates.com/demo/pbt/biz/images/25x25x3.png" alt="25x25x3" width="25" style="margin:0;border:0;padding:0;display:block;" /> -->
																								<!-- </a> -->
																							<!-- </td>																				 -->
																						<!-- </tr> -->
																					<!-- </table> -->
																				<!-- </td> -->
																			<!-- </tr> -->
																		<!-- </table> -->
																	<!-- </td> -->
																<!-- </tr>													 -->
															<!-- </table>					 -->
														</td>
													</tr>
													<tr>
														<td height="20"></td>
													</tr>
												</tbody>	
											</table>
											<!--SECTION TABLE-600 ENDS-->
										</td>
									</tr>							
								</tbody>	
							</table>
							<!--SECTION TABLE-680 ENDS-->
						</td>
					</tr>					
				</tbody>	
			</table>
			
			<!-- FOOTER-1 ENDS -->
			<!-- FOOTER-2 STARTS -->
			
			<table align="center" bgcolor="#ecf0f1" border="0" cellpadding="0" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td align="center">
						
							<!--SECTION TABLE-680-->
							
							<table align="center" border="0" bgcolor="#ecf0f1" cellpadding="0" cellspacing="0" class="display-width" width="680">
								<tbody>							
									<tr>
										<td align="center" style="padding:0 30px;">
										
											<!--SECTION TABLE-600-->
											
											<table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">				
												<tbody>							
													<tr>
														<td>
															
															<table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width" width="36%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
																<tr>		
																	<td align="center">
																		<table align="center" border="0" cellspacing="0" cellpadding="0" style="width:auto !important">
																			<tr>
																				<td align="left" class="MsoNormal" style="color:#222222; font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif;font-size:14px;line-height:24px;letter-spacing:1px;">
																					&copy; 2018 KapitalBoost. All Rights Reserved.
																				</td>
																			</tr>
																		</table>
																	</td>
																</tr>
															</table>	
															
															<table align="left" border="0" cellpadding="0"  cellspacing="0" class="display-width" width="1" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;">
																<tr>
																	<td style="line-height:20px;" height="20" width="1"></td>
																</tr>
															</table>
															
															<table align="right" border="0" cellpadding="0" cellspacing="0" class="display-width" width="16%" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto;">
																<tr>
																	<td align="center">
																		
																	</td>
																</tr>
															</table>													
															
														</td>			
													</tr>	
													<tr>
														<td height="20"></td>
													</tr>	
												</tbody>	
											</table>
											
											<!--SECTION TABLE-600 ENDS-->
										</td>
									</tr>							
								</tbody>	
							</table>
							
							<!--SECTION TABLE-680 ENDS-->
						</td>
					</tr>					
				</tbody>	
			</table>

		</body>
		</html>
    
    ';

    
        include_once('../Classes/sendgrid-php/sendgrid-php.php');
        date_default_timezone_set('Asia/Singapore');
        $subject = $subject;
        $to = new SendGrid\Email("Wei Siang", "w_siang@hotmail.com");
        $content_email = $message;
        $content = new SendGrid\Content("text/html", $content_email);
        $from = new SendGrid\Email("Kapital Boost", "info@kapitalboost.com");
        $mail = new SendGrid\Mail($from, $subject, $to, $content);
        $apiKey = "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";
        $sg = new \SendGrid($apiKey);
        $response = $sg->client->mail()->send()->post($mail);
        
    


?>