<?php
session_start();

$page = 'profile';
include("header.php");
include("sidebar.php");

include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();

$c = $_GET["c"];
if ($mysql->Connection()) {
    list($countryName, $countryCode) = $mysql->GetCountryList(); //Get all country name from database
	list($nationalityName, $nationalityCode, $countryNational) = $mysql->GetNationalityList(); //Get all nationality name from database
	//Get all member data
	list($memberFirstName, $memberLastName, $memberCountry, $memberUsername, $memberDateOfBirth, $memberEmail, $memberNationality, $memberPhone, $memberAddress, $memberPassword, $memberPassportNo, $memberPassportFullName, $memberIdType, $memberIdCountry, $memberNRICFront, $memberNRICBack, $memberAddressProof, $memberCheck1, $memberCheck2, $memberCheck3, $memberCheck4, $memberCheck5, $memberCheck6, $memberCheck7, $memberCheck8, $memberCheck9, $memberCheck10, $memberCheck11, $memberCheck12, $memberCheck13, $member_gender, $member_status) = $mysql->GetMemberData($_SESSION["MemberId"]);
    $mysql->AddTrail("Member Visit Profile Dashboard Web");//add trail to database

    list($legal_content) = $mysql->getLegal();
}

$legal_content = $legal_content[0];
$legal_risiko = explode("~~~^_^~~~", $legal_content);

$checkLastExtFront   = substr($memberNRICFront[0], -4); //get extention
$checkLastExtBack    = substr($memberNRICBack[0], -4); //get extention
$checkLastExtAddress = substr($memberAddressProof[0], -4); //get extention

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
// echo "'<script>console.log(\"submit : ".$_POST['submit']."\")</script>'";
if (($_POST['submit']) == "Save Changes") {
    $uname = test_input($_POST['uname']);
    $first = test_input($_POST['firstname']);
    $last  = test_input($_POST['lastname']);
    $pass  = test_input($_POST["hiddenpassword"]);
    if (isset($_POST['password']) && test_input($_POST["password"]) != "") {
        $pass = md5(test_input($_POST['password']));
    } else {
        $pass = test_input($_POST["hiddenpassword"]);
    }

    $nationality 			= test_input($_POST['nationality']);
    $member_email 			= test_input($_POST['email']);
    $country     			= test_input($_POST['current_location']);
    $nric        			= test_input($_POST['nric']);
    $ic_option   			= test_input($_POST["ic_option"]);
    $ic_name     			= test_input($_POST["ic_name"]);
    $ic_country  			= test_input($_POST["ic_country"]);
    $dob         			= test_input($_POST['dob']);
    $resarea     			= test_input($_POST['resarea']);
    $nope        			= test_input($_POST['nope']);
    $cek1        			= test_input($_POST['check_1']);
    $cek2        			= test_input($_POST['check_2']);
    $cek3        			= test_input($_POST['check_3']);
    $cek4        			= test_input($_POST['check_4']);
    $cek5        			= test_input($_POST['check_6']);
    $cek6        			= test_input($_POST['check_7']);
    $cek7        			= test_input($_POST['check_8']);
    $cek8        			= test_input($_POST['check_9']);
    $cek9        			= test_input($_POST['check_10']);
    $cek10       			= test_input($_POST['check_11']);
    $cek11       			= test_input($_POST['check_12']);
    $cek12       			= test_input($_POST['check_13']);
    $cek13       			= test_input($_POST['check_14']);
    $member_status       	= test_input($_POST['member_status']);
	$access_token_member  	= $mysql->GetMemberToken($_SESSION["MemberId"]);
	$gender  				= $mysql->GetMemberGender($_SESSION["MemberId"]);
	$xdob					= strtotime($dob);
	$xfersdob				= date('Y-m-d', $xdob);

	// echo "'<script>console.log(\"xfersdob : $xfersdob\")</script>'";

	//Gender Initiation
	if ($gender == "F") {
		$gender = "female";
	} else {
		$gender = "male";
	}



    // NRIC Front
    $nric_imgname  = $_FILES["nric_file"]["name"];
    $nric_pisah    = explode(".", $nric_imgname);
    $nric_imgtype  = end($nric_pisah);
    $nric_file_img = $_FILES["nric_file"]["tmp_name"];
    $nric_newname  = time() . rand() . "." . $nric_imgtype;
    // NRIC Back
    if (isset($_POST["passport"])) {
        $nric_back_newname = 'passport.png';
    } else {
        $nric_imgname_back  = $_FILES["nric_file_back"]["name"];
        $nric_pisah_back    = explode(".", $nric_imgname_back);
        $nric_imgtype_back  = end($nric_pisah_back);
        $nric_file_img_back = $_FILES["nric_file_back"]["tmp_name"];
        $nric_back_newname  = time() . rand() . "." . $nric_imgtype_back;
    }
    // Address Proof
    $apName     = $_FILES["address_proof"]["name"];
    $apExt      = explode(".", $apName);
    $apType     = end($apExt);
    $ap_file    = $_FILES["address_proof"]["tmp_name"];
    $ap_newname = time() . rand() . "." . $apType;


    if (!empty($nric_file_img)) {
        if ($mysql->Connection()) {
            $mysql->UpdateNricFront($nric_newname);
        }
        move_uploaded_file($nric_file_img, "../assets/nric/datanric/" . $nric_newname);
    }
    if (!empty($nric_file_img_back) || $nric_back_newname == 'passport.png') {
        if ($mysql->Connection()) {
            $mysql->UpdateNricBack($nric_back_newname);
        }
        if ($nric_back_newname != "passport.png") {
            move_uploaded_file($nric_file_img_back, "../assets/nric/datanricback/" . $nric_back_newname);
        }
    }
    if (!empty($ap_file)) {
        if ($mysql->Connection()) {
            $mysql->UpdateProofAddress($ap_newname);
        }
        move_uploaded_file($ap_file, "../assets/nric/addressproof/" . $ap_newname);
    }


	if ($member_status == 0) {
		// echo "'<script>console.log(\"masuk 0 \")</script>'";
		// if (empty($nric_file_img)){
		// 	echo '<script type="text/javascript">
		// 	alert("Your Identification Card / Passport (front) still empty");
		// 	window.location.href = "profile.php";
		// 	</script>';
		// } else if (empty($nric_file_img_back)){
		// 	echo '<script type="text/javascript">
		// 	alert("Your Identification Card / Passport (back) still empty");
		// 	window.location.href = "profile.php";
		// 	</script>';
		// } else if (empty($ap_file)){
		// 	// echo "'<script>console.log(\"masuk if \")</script>'";
		// 	echo '<script type="text/javascript">
		// 	alert("Your Proof of Address still empty");
		// 	window.location.href = "profile.php";
		// 	</script>';
		if (empty($memberNRICFront[0])){
			echo '<script type="text/javascript">
			alert("Please upload your Identification Card / Passport (Front) information");
			window.location.href = "profile.php";
			</script>';
		} else if (empty($memberNRICBack[0])){
			echo '<script type="text/javascript">
			alert("Please upload your Identification Card / Passport (Back) information");
			window.location.href = "profile.php";
			</script>';
		} else if (empty($memberAddressProof[0])){
			// echo "'<script>console.log(\"masuk if \")</script>'";
			echo '<script type="text/javascript">
			alert("Please upload your Proof of Address information");
			window.location.href = "profile.php";
			</script>';
		} else {
			$member_status = 1;

			$message = "
				 <html>
					   <head>
							  <title>Kapital Boost's User Neew to be Reviewed</title>
					   </head>
					   <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
							  <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
									 <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
											<img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
											<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
												   Dear Admin,
											</p>
											<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
												   New user need to be reviewed :  <br>
												   Name 		: $first $last <br>
												   Email 		: $member_email <br>
												   Country		: $country  <br>
											</p>
											<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
												   Best regards,<br><br>Kapital Boost
											</p>

									 </div>
									 <div style='width:100%;height:7px;background-color:#00b0fd'></div>
									 <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
											<p style='font-size: 12px;color: white;text-align:center'>
												   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
											</p>
									 </div>
							  </div>
					   </body>
				</html>
				";

				include_once 'email.php';
				$email = new email();
				// echo "'<script>console.log(\"sql : ".$memberData['member_email']."\")</script>'";
				$email->SendEmail("arief@kapitalboost.com", "admin@kapitalboost.com", "Kapital Boost - User Need to be Reviewed", $message);
		}
	}

    $ic_type = "$ic_country $ic_option No.";

	$frontImg 	= $mysql->GetFrontImage($_SESSION["MemberId"]);
	$backImg	= $mysql->GetBackImage($_SESSION["MemberId"]);
	$apImg 		= $mysql->GetProofAddress($_SESSION["MemberId"]);

	// echo "'<script>console.log(\"gender : $gender\")</script>'";

	//================================== Xfers =============================

	$arr_xudata = array(
		"first_name" => $first,
		"last_name" => $last,
		"mother_maiden_name" => $last,
		"email" => $member_email,
		"date_of_birth" => $xfersdob,
		"gender" => $gender,
		"address_line_1" => $resarea,
		"nationality" => $nationality,
		"identity_no" => $nric,
		"country" => $country,
		"id_front_url" => "https://kapitalboost.com/assets/nric/datanric/$frontImg",
		"selfie_2id_url" => "https://kapitalboost.com/assets/nric/datanric/$frontImg",
		"id_back_url" => "https://kapitalboost.com/assets/nric/datanricback/$backImg",
		"proof_of_address_url" => "https://kapitalboost.com/assets/nric/addressproof/$apImg"
	);

	$xudata = json_encode($arr_xudata);

	$curl = curl_init();
	if ($country == 'SINGAPORE') {
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://www.xfers.io/api/v3/user",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => $xudata,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"x-xfers-user-api-key: $access_token_member"
			),
		));
	} else if ($country == 'INDONESIA') {
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://id.xfers.com/api/v3/user",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 20,
			CURLOPT_TIMEOUT => 60,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => "$xudata",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"x-xfers-user-api-key: $access_token_member"
			),
		));
	}

	$response = curl_exec($curl);

	//code below is show api response

	$http_response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	echo "Verbose information:\n<pre>", !rewind($verbose),  htmlspecialchars(stream_get_contents($verbose)), "</pre>\n";
	curl_close($curl);

	echo PHP_EOL . "INFO: HTTP Response Code was: " . $http_response_code . PHP_EOL;

	if ( $response === false )
	{
	echo PHP_EOL . "ERROR: curl_exec() has failed." . PHP_EOL;
	}
	else
	{
	echo PHP_EOL . "INFO: Response Follows..." . PHP_EOL;
	echo PHP_EOL . $response;
	}


	//================================== Xfers =============================

    if ($mysql->Connection()) {
        $mysql->UpdateDataMember($country, $uname, $pass, $last, $first, $nationality, $nric, $ic_type, $ic_option, $ic_name, $ic_country, $dob, $resarea, $cek1, $cek2, $cek3, $cek4, $cek5, $cek6, $cek7, $cek8, $cek9, $cek10, $cek11, $cek12, $cek13, $nope, $member_status);

        $mysql->AddTrail("Member Update Profile Dashboard Web");

        echo '<script type="text/javascript">
		  window.location = "profile.php?c='.md5("SuccessKBProfilCompleted").'"
		  </script>';
	}

}

?>
<style>
.dropdown-menu {
  top: 0 !important;
}
</style>
<!-- Main container -->
<main>
<div class="main-content">
	<div class="row">
		<br><br>
	</div>
	<!-- Notification -->
	<?php if($c == md5("SuccessKBProfilCompleted")){?>
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			Congratulations! Your profile is updated...
		</div>
	<?php } ?>
	<?php if($member_status[0] == 1){?>
		<div class="alert alert-danger" role="alert">
			Your profile is currently being reviewed. Please allow us 1 to 2 working days for it to be completed.
		</div>
	<?php } ?>
	<!-- End of Notification -->

	<form action="" method="POST" enctype="multipart/form-data">
		<?php
			for ($i = 0; $i < count($memberEmail); $i++) {
		?>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<h4 class="card-title"><strong>Personal Information</strong></h4>
						<div class="card-body row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="require">Country of Residence</label>
									<select data-provide="selectpicker" data-live-search="true" class="form-control" name="current_location" id="current_location" required>
										<option>Select Your Country</option>
										<?php
											for ($s = 0; $s < count($countryName); $s++) {
										?>
												<option value="<?=$countryName[$s]?>"
													<?php if ($memberCountry[$i] == $countryName[$s]) {echo "selected";}?>> <?=$countryName[$s]?>
												</option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label class="require">Username</label>
									<input class="form-control" type="text" name="uname" id="uname" value="<?=$memberUsername[$i]?>" required>
								</div>

								<div class="form-group">
									<label class="require">First Name</label>
									<input class="form-control" type="text" name="firstname" id="firstname" value="<?=$memberFirstName[$i]?>"  required>
								</div>

								<div class="form-group">
									<label class="require">Last Name</label>
									<input class="form-control" type="text" name="lastname" id="lastname" value="<?=$memberLastName[$i]?>"  required>
								</div>

								<div class="form-group">
									<label class="require">Email</label>
									<input class="form-control" type="text" name="email" id="email" value="<?=$memberEmail[$i]?>"  required readonly>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="require">Nationality</label>
									<select data-provide="selectpicker" data-live-search="true" class="form-control" name="nationality" id="nationality" required>
										<option>Select Your Nationality</option>
										<?php
											for ($d = 0; $d < count($nationalityName); $d++) {
										?>
											<option value="<?=$nationalityName[$d]?>"
												<?php if ($memberNationality[$i] == $nationalityName[$d]) {echo "selected";}?>> <?=$countryNational[$d]?>
											</option>
										<?php } ?>
									</select>
								</div>

								<div class="form-group">
									<label class="require">Date of Birth</label>
									<input type="date" class="form-control" name="dob" id="dob" value="<?=$memberDateOfBirth[$i]?>"  required>
								</div>

								<div class="form-group" >
									<label class="require">Handphone No.</label>
									<input class="form-control" type="number" name="nope" id="nope" value="<?=$memberPhone[$i]?>"  required>
									<input class="form-control" type="hidden" name="member_status" id="member_status" value="<?=$member_status[$i]?>">
								</div>

								<div class="form-group">
									<label class="require">Residential Address</label>
									<input class="form-control" type="text" name="resarea" id="resarea" value="<?=$memberAddress[$i]?>" required>
								</div>

								<div class="form-group">
									<label class="require">Change Password</label>
									<input class="form-control" type="password" name="password" id="password" value="">
									<input class="form-control" type="hidden" name="hiddenpassword" id="hiddenpassword" value="<?=$memberPassword[$i]?>"  required>
								</div>

							</div>
						</div>
					</div>
				</div>

				<div class="col-12">
					<div class="card">
						<h4 class="card-title"><strong>Personal Identification Information</strong></h4>

						<div class="card-body row">
							<div class="col-md-6">
								<div class="form-group">
									<label class="require">NRIC/Passport No</label>
									<input class="form-control" type="text" name="nric" id="nric" value="<?=$memberPassportNo[$i]?>"  required>
								</div>

								<div class="form-group">
									<label class="require">Full name as shown in Passport / ID</label>
									<input class="form-control" type="text" name="ic_name" id="ic_name" value="<?=$memberPassportFullName[$i]?>"  required>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label class="require">Type of Identification Number</label>
									<select data-provide="selectpicker" class="form-control" name="ic_option" id="ic_option" required>
										<option value="">Select Your Identification Number Type</option>
										<option value="IC" <?php if ($memberIdType[$i] == "IC") {echo "selected";}?>> Identity Card </option>
										<option value="FIN" <?php if ($memberIdType[$i] == "FIN") {echo "selected";}?>> FIN </option>
										<option value="Passport" <?php if ($memberIdType[$i] == "Passport") {echo "selected";}?>> Passport </option>
									</select>
								</div>

								<div class="form-group">
									<label class="require">Issuing country</label>
									<select data-provide="selectpicker" data-live-search="true" class="form-control" name="ic_country" id="ic_country" required>
										<option value="">Select Your Issuing Country</option>
										<?php
											for ($s = 0; $s < count($countryName); $s++) {
										?>
												<option value="<?=$countryName[$s]?>" <?php if ($memberIdCountry[$i] == $countryName[$s]) {echo "selected";}?>>
													<?=$countryName[$s]?>
												</option>
									<?php }	?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-12">
					<div class="card">
						<h4 class="card-title"><strong>Upload Proof of Identification</strong></h4>
						<div class="card-body row">
							<div class="col-md-4">
								<div class="form-group">
									<p>
										<label>
											<b>Identification Card / Passport (front)</b>  <br/>
											(For Singapore-based investors, please use NRIC, FIN or 11B only.)<br/>
											*.jpg and *.pdf formats only  <br/>
										</label>
									</p>
									<?php
										if ($checkLastExtFront == ".pdf") {
									?>
											<a href="../assets/nric/datanric/<?=$memberNRICFront[$i]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
									<?php
										} else if ($checkLastExtFront == ".png" or $checkLastExtFront == ".jpg") {
									?>
											<img src="../assets/nric/datanric/<?=$memberNRICFront[$i]?>">
									<?php } ?>

									<br/><br/>
									<div class="file-group file-group-inline">
										<button class="btn btn-info file-browser" type="button">Browse</button>
										<input type="file"  name="nric_file" id="nric_file">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<p>
										<label>
											<b>Identification Card (back)</b> <br/>
											(For Singapore-based investors, please use NRIC, FIN or 11B only.)<br/>
											*.jpg and *.pdf formats only <br/>
										</label>
									</p>
									<?php
										if($checkLastExtBack==".pdf"){
									?>
											<a href="../assets/nric/datanricback/<?= $memberNRICBack[$i] ?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
									<?php
										} else if($checkLastExtBack==".png" or $checkLastExtBack==".jpg"){
									?>
											<img src="../assets/nric/datanricback/<?= $memberNRICBack[$i] ?>">
									<?php }	?>

									<br/><br/>
									<div class="file-group file-group-inline">
										<button class="btn btn-info file-browser" type="button">Browse</button>
										<input type="file"  name="nric_file_back" id="nric_file_back">
									</div>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<p>
										<label>
											Proof of Address <br> (Bank statement, utility bill or government issued letter dated no more than 3 months old. Please note that telco bills and insurance letters are not accepted.) <br>  *.jpg and *.pdf formats only
											<span data-clipboard-text="fa fa-info-circle" data-provide="tooltip" data-tooltip-color="info" title="" data-original-title="Provide a picture of your utility bill or bank statement that is less than three months old. Make sure that the date is shown in the picture.">
												<span class="fa fa-info-circle"></span>
											</span>
										</label>
									</p>
									<?php
										if ($checkLastExtAddress == ".pdf" or $checkLastExtAddress == ".PDF") {
									?>
											<a href="../assets/nric/addressproof/<?=$memberAddressProof[$i]?>" class="btn btn-label btn-primary"><label><i class="ti-import"></i></label> Download File</a>
									<?php
										} else if ($checkLastExtAddress == ".png" or $checkLastExtAddress == ".jpg" or $checkLastExtAddress == "jpeg" or $checkLastExtAddress == ".PNG" or $checkLastExtAddress == ".JPG" or $checkLastExtAddress == "JPEG") {
									?>
											<img src="../assets/nric/addressproof/<?=$memberAddressProof[$i]?>">
									<?php } ?>

									<br/><br/>
									<div class="file-group file-group-inline">
										<button class="btn btn-info file-browser" type="button">Browse</button>
										<input type="file" name="address_proof" id="address_proof">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-12">
					<div class="card">
						<h4 class="card-title"><strong>Additional Information</strong></h4>
						<div class="card-body row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Which of these financial products have you invested in?</label>
									<div class="col custom-controls-stacked">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks" <?php if ($memberCheck1[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Stocks</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" <?php if ($memberCheck2[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Bonds</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" <?php if ($memberCheck3[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Mutual funds</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" <?php if ($memberCheck4[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Alternative investments (specify)</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_6" id="CAT_Custom_20047261_157746_4" value="None of the above" <?php if ($memberCheck5[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">None of the above</span>
										</label>
										<br>
									</div>
								</div>
							</div>

							<div class="col-md-6">
								<div class="form-group">
									<label>Which of these financial products have you invested in?</label>
									<div class="col custom-controls-stacked">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_7" id="CAT_Custom_20043779_157746_0" value="Attractive returns" <?php if ($memberCheck6[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Attractive returns</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_8" id="CAT_Custom_20043779_157746_1" value="Short tenor" <?php if ($memberCheck7[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Short tenor</span>
										</label>
											<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_9" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" <?php if ($memberCheck8[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Social/ethical reasons</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_10" id="CAT_Custom_20043779_157746_3" value="Investment diversification" <?php if ($memberCheck9[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Investment diversification</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_11" id="CAT_Custom_20043779_157746_4" value="Transparency" <?php if ($memberCheck10[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Transparency</span>
										</label>
										<label class="custom-control custom-checkbox">
											<input type="checkbox" class="custom-control-input" name="check_12" id="CAT_Custom_20043779_157746_5" value="Others" <?php if ($memberCheck11[$i] != "") {echo "checked";}?>>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Others</span>
										</label>
										<br>
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="col custom-controls-stacked">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="check_13" id="pernyataan-risiko" value="yes" <?php if ($memberCheck12[$i] != "") {echo "checked";}?>>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">I have read, understood and agreed to the <a href="https://kapitalboost.com/legal#term" target="_blank">Terms of Use</a>, <a href="https://kapitalboost.com/legal#privacy" target="_blank"> Privacy Policy </a> and <a href="https://kapitalboost.com/legal#risk" target="_blank">Risk Statement</a> set out by Kapital Boost.  </span>
									</label>
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="check_14" id="CAT_Custom_20043781_157746_1" value="yes" <?php if ($memberCheck13[$i] != "") {echo "checked";}?>>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">I agree to share my personal information with Xfers, a third party payment gateway partner of Kapital Boost. For more information on Xfers, click <a target="_blank" href="https://xfers.io">here.</a></span>
									</label>
								</div>
							</div>
						</div>

						<footer class="card-footer text-right">
							<button class="btn btn-secondary" type="reset">Cancel</button>
							<?php if ($member_status[$i] != 1) { ?>
								<input class="btn btn-info" type="submit" name="submit" value="Save Changes">
							<?php } else { ?>
								<button class="btn btn-danger" name="under_verification" disabled>Your Profile is Under Verification... </button>
							<?php } ?>
						</footer>
					</div>
				</div>
			</div>
		<?php } ?>
	</form>
</div><!--/.main-content -->
<?php
	include "footer.php";
?>

<!-- Modal -->
<div class="modal fade" id="risikoLegal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title" id="myModalLabel">Risk Statements</h4>
      </div>
      <div class="modal-body">
        <?= $legal_risiko[2] ?>
        <hr>
        <center>
	        <p style="margin-bottom: 35px;">I have read, understood and agreed with the <b>Risk Statement</b> set by Kapital Boost. And I hereby declare that the funds that I use are not sourced from money laundering, and for funding terrorism.</p>
			<button type="button" class="btn btn-danger" data-dismiss="modal">Not Agree</button>
			<button type="button" class="btn btn-primary" id="agreeRisiko">Agree</button>
        </center>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$("#pernyataan-risiko").click(function() {
			if ($("[name=check_13]").prop('checked') == true) {
				$("#risikoLegal").modal('show');
			}
			return false;
		});

		$("#agreeRisiko").click(function() {
			$("[name=check_13]").prop('checked', true);
			$("#risikoLegal").modal('hide');
			$("#term-check").removeClass('error');
        	$("#term-notif").removeClass('show');
		});
	})
</script>

</body>
</html>
