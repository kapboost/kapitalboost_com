<?php
session_start();
$page = 'dashboard';
include("header.php");
include("sidebar.php");
include_once 'checkSession.php';

include_once 'mysql.php';
$mysql = new mysql();

if ($mysql->Connection()) {
	$mysql->AddTrail("Member Visit Dashboard Web"); //add trail to database
	
	list($campaingId, $investAmount, $invStatus, $investDate, $campaignName, $campaignSnippet, $campaignReturn, $campaignRisk, $campaignBackground) = $mysql->GetFiveInvestmentList($_SESSION["MemberId"]); //get the 5 newest investment
	list($donationId, $donateAmount, $donateStatus, $donateDate, $donateName, $donateSnippet, $donateReturn, $donateRisk, $donateBackground) = $mysql->GetFiveDonationList($_SESSION["MemberId"]); //get the 5 newest donation
	//Get all member data
	list($memberFirstName, $memberLastName, $memberCountry, $memberUsername, $memberDateOfBirth, $memberEmail, $memberNationality, $memberPhone, $memberAddress, $memberPassword, $memberPassportNo, $memberPassportFullName, $memberIdType, $memberIdCountry, $memberNRICFront, $memberNRICBack, $memberAddressProof, $memberCheck1, $memberCheck2, $memberCheck3, $memberCheck4, $memberCheck5, $memberCheck6, $memberCheck7, $memberCheck8, $memberCheck9, $memberCheck10, $memberCheck11, $memberCheck12, $memberCheck13, $member_gender, $member_status) = $mysql->GetMemberData($_SESSION["MemberId"]);
	
	$memberTotalInvestment 	= $mysql->GetMemberTotalInvestment($_SESSION["MemberId"]); // get member total investment
	$averageReturns 		= $mysql->GetMemberAverageReturn($_SESSION["MemberId"]); //get total average returns per year
	$walletBalance 			= $mysql->GetMmeberBalance($_SESSION["MemberId"]); //get current wallet balance
}

$totalNewInvest 	= count($campaingId);
$totalNewDonation 	= count($donationId);

?>

<!-- Main container -->
<main>
<div class="main-content">
	<?php 
	if ($memberUsername[0]=="" or $memberNationality[0]=="" or $memberPhone[0]=="" or $memberAddress[0]=="" or $memberPassportNo[0]=="" or $memberIdCountry[0]=="" or $memberNRICFront[0]=="" or $memberAddressProof[0]=="") {?>
	<div class="alert alert-danger" role="alert">
		Sorry, your profile is not complete. Please complete it on  <a href="profile.php" class="alert-link">profile page</a>.
	</div>
	<?php } ?>
	
	<?php if($member_status[0] == 1){?>
		<div class="alert alert-danger" role="alert">
			Your profile is currently being reviewed. Please allow us 1 to 2 working days for it to be completed.
		</div>
	<?php } ?>
	<!-- statistic section -->
	<!--<div class="callout callout-info" role="alert">
		<h5>Heads up Arief!</h5>
		<p>Would you like to tell us about your experience using Kapitalboost? This will be very helpful to improve our services. <a href=""><strong>Click here</strong></a></p>
	</div>
	<!-- statistic section -->
	<div class="row">  
		<div class="col-md-3">
			<a href="wallet.php">
				<div class="card card-body bg-purple" style="background-color: #274060 !important;">
					<div class="flexbox">
						<!--<span class="ti-wallet fs-40"></span>-->
						<img src="../assets/images/n-wallet.png" width="48" height="48">
						<span class="fs-26 fw-100">$<?= number_format($walletBalance, 1, '.', ','); ?></span>
					</div>
					<div class="text-right">KB-Wallet balance<br><small><em>(click for details)</em></small></div>
				</div>
			</a>
		</div>

		<div class="col-md-3">
			<div class="card card-body bg-purple" style="background-color: #395B99 !important;">
				<div class="flexbox">
					<!--<span class="ti-wallet fs-40"></span>-->
					<img src="../assets/images/n-wallet.png" width="48" height="48">
					<span class="fs-26 fw-100"><?php if($_SESSION["MemberCountry"]=="SINGAPORE"){echo "$";}else if($_SESSION["MemberCountry"]=="INDONESIA"){echo "Rp";}?><span id="balance"></span></span>
				</div>
				<div class="text-right">Current Xfers balance<br><small><em>&nbsp;&nbsp;&nbsp;</em></small></div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card card-body bg-pink" style="background-color: #3FA5EA  !important;">
				<div class="flexbox">
					<img src="../assets/images/n-money.png" width="48" height="48">
					<span class="fs-26 fw-100">$<?= number_format($memberTotalInvestment, 1, '.', ','); ?></span>
				</div>
				<div class="text-right">Total amount invested<br><small><em>&nbsp;&nbsp;&nbsp;</em></small></div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card card-body bg-cyan"  style="background-color: #42CAFD !important;">
				<div class="flexbox">
					<img src="../assets/images/n-money-back.png" width="48" height="48">
					<span class="fs-26 fw-100"> <?= number_format($averageReturns, 1, '.', ','); ?>%</span>
				</div>
				<div class="text-right">Average annualised return<br><small><em>&nbsp;&nbsp;&nbsp;</em></small></div>
			</div>
		</div>
		<!-- end statistic section -->
		<!-- last 5 investment section -->
		<div class="col-md-12"> 
			<div class="card">
				<div class="card-header">
					<h5 class="card-title"><strong>Latest investments</strong></h5>
					<a class="btn btn-xs btn-light" href="https://kapitalboost.com/member/investment.php">View Portfolio</a>
				</div>

				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th width="">Campaigns</th>
							<th width="15%">Funding Amount</th>
							<th width="15%">Investment Date</th>
							<th width="15%">Principal Payment</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0; $i < count($campaingId); $i++) {
						?>
							<tr>
								<td><?= $campaignName[$i] ?></td>
								<td>$<?= $investAmount[$i] ?></td>
								<td><?= $investDate[$i] ?></td>
								<td><?= $invStatus[$i] ?></td>
							</tr>
						<?php }	
						if ($totalNewInvest == 0){
							echo "
							<tr>
								<td colspan='4' align='center'> You don't have any recent investments...</td>
							</tr>
							";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- end last 5 investment section -->
		<!-- last 5 donation section -->
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h5 class="card-title"><strong>Latest donations</strong></h5>
					<a class="btn btn-xs btn-light" href="https://kapitalboost.com/member/investment.php#tab-donation">View Donations</a>
				</div>

				<table class="table table-striped table-hover table-responsive">
					<thead>
						<tr>
							<th width="">Campaigns</th>
							<th width="15%">Donation Amount</th>
							<th width="15%">Donation Date</th>
							<th width="15%">Principal Payment</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0; $i < count($donationId); $i++) {
						?>
							<tr>
								<td><?= $donateName[$i] ?></td>
								<td>$<?= $donateAmount[$i] ?></td>
								<td><?= $donateDate[$i] ?></td>
								<td><?= $donateStatus[$i] ?></td>
							</tr>

						<?php }	
						if ($totalNewDonation == 0){
							echo "
							<tr>
								<td colspan='4' align='center'> You don't have any recent donations...</td>
							</tr>
							";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
		<!-- end last 5 donation section -->
	</div>
</div><!--/.main-content -->

<?php

include("footer.php");

?>
<script>
$(document).ready(function () {

	var country = '<?php echo $_SESSION["MemberCountry"]; ?>';
	if (country == 'SINGAPORE') {
		var url = "../xfers/apis/ballance.php";
	} else {
		var url = "../xfers_indo/apis/ballance.php";
	}
	$.ajax({
		url: url,
		type: 'post',
		data:{
			"getuserballance":'<?php echo $_SESSION["MemberPhone"]; ?>'
		},
		headers:{
			'Content-Type':'application/x-www-form-urlencoded'
		},
		dataType: 'json',
		success: function (data) {
			$("#balance").text(data.available_balance);
		}
	});

});
</script>

</body>
</html>