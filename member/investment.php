<?php
session_start();
include_once 'checkSession.php';
include_once 'mysql.php';
$mysql = new mysql();
$page = "portfolio";
include("header.php");
include("sidebar.php");

include_once '../libs/helper.php';

// check_payout_calculation($_SESSION['MemberId']);

check_payout_calculation($_SESSION['MemberId']);
// echo $_SESSION['MemberId'];
// print_r($result);
// exit();

if ($mysql->Connection()) {
	//Get all investment list
	list($campaingId, $investAmount, $invStatus, $invId, $investDate, $campaignName, $campaignSnippet, $campaignReturn, $campaignRisk, $campaignBackground) = $mysql->GetInvestmentList($_SESSION["MemberId"]);
	//Get all donation list
	list($donationId, $donationAmount, $donationStatus, $donationId, $donationDate, $donationName, $donationSnippet, $donationReturn, $donationRisk, $donationBackground) = $mysql->GetDonationList($_SESSION["MemberId"]);
	$mysql->AddTrail("Member Visit Investment Dashboard Web"); //add trail to database
	
	$totalNewInvest 	= count($campaingId);
	$totalNewDonation 	= count($donationId);
}
?>

<!-- Main container -->
<main>
<header class="header">
	<div class="header-action">
		<ul class="nav nav-tabs" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#tab-sme" role="tab">SME</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#tab-donation" role="tab">Donation </a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="tab" href="#tab-payment" role="tab">Payment Schedule </a>
			</li>
		</ul>
	</div>
</header>


<div class="tab-content card card-body">
	<!-- SME Campaign section -->
	<div class="tab-pane active" id="tab-sme" role="tabpanel">
		<div class="main-content">
			<?php
				for ($i = 0; $i < count($campaingId); $i++) {
			?>
			<div class="row">
				<div class="col-12" >
					<div class="card">
						<div class="row no-gutters">
							<a class="col-4 bg-img d-none d-md-block" style="background-image: url(https://kapitalboost.com/assets/images/campaign/<?= $campaignBackground[$i] ?>)" href="#"></a>
							<div class="col-md-8">
								<div class="card-body">
									<h3>
										<a class="hover-primary text-uppercase fs-15 fw-400 ls-1" href="#">
											<?= $campaignName[$i] ?>
										</a> 
										&nbsp;&nbsp;&nbsp; 
										<?php if ($invStatus[$i]=="Unpaid"){?>
											<a href="https://kapitalboost.com/dashboard/payment-detail/<?= $invId[$i] ?>">
												<button class="btn btn-w-md btn-bold btn-info">Pay Now</button>
											</a>
										<?php } ?>
									</h3>
									<hr class="w-90px ml-0 bt-2 border-primary hr-sm">
									<p>
										<?= $campaignSnippet[$i] ?> 
									</p>
									<div class="row">
										<div class="col-md-3">
											<p> 
												<strong>Investment Amount</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong>: $<?= $investAmount[$i] ?> </strong> </span>
											</p>
										</div> 
										<div class="col-md-3">
											<p> 
												<strong>Principal Payment</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p <?php if ($invStatus[$i]=="Unpaid"){echo 'style="color: #E75926; font-weight: 900;"';}else{echo 'style="color: #97CE68; font-weight: 400;"';}?>>
												<span> <strong>: <?= $invStatus[$i] ?> </strong> </span>
											</p>
										</div>
										<div class="col-md-3">
											<p> 
												<strong>Returns</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong>: <?= $campaignReturn[$i] ?>% </strong> </span>
											</p>
										</div>
										<div class="col-md-3">
											<p> 
												<strong>Investment Date</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong>:  <?= $investDate[$i] ?>  </strong> </span>
											</p>
										</div>
										<div class="col-md-3">
											<p> 
												<strong>Risk Level</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong>: <?= $campaignRisk[$i] ?> </strong> </span>
											</p>
										</div>
										<div class="col-md-3">
											<p> 
												<!--<button class="btn btn-bold btn-primary">Payment Schedule</button>-->
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong></strong> </span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
				<div class="divider fs-16"></div>
				</div>
			</div>
			<?php }	
			if ($totalNewInvest == 0){
				echo "
				 <p style='text-align:center;'>You don't have any recent investments...</p>
				";
			}
			
			?>
		</div>
	</div>
	<!-- end SME Campaign section -->
	<!-- Donation Campaign section -->
	<div class="tab-pane" id="tab-donation" role="tabpanel">
		<div class="main-content">
			
				<?php
					for ($i = 0; $i < count($donationId); $i++) {
				?>
			<div class="row">
				<div class="col-12">
					<div class="card">
						<div class="row no-gutters">
							<a class="col-4 bg-img d-none d-md-block" style="background-image: url(https://kapitalboost.com/assets/images/campaign/<?= $donationBackground[$i] ?>)" href="#"></a>
							<div class="col-md-8">
								<div class="card-body">
									<h3>
										<a class="hover-primary text-uppercase fs-15 fw-400 ls-1" href="#">
											<?= $donationName[$i] ?>
										</a>
										&nbsp;&nbsp;&nbsp; 
										<?php if ($donationStatus[$i]=="Unpaid"){?>
										<a href="https://kapitalboost.com/dashboard/payment-detail/<?= $donationId[$i] ?>">
											<button class="btn btn-w-md btn-bold btn-info">Pay Now</button>
										</a>
										<?php } ?>
									</h3>
									<hr class="w-90px ml-0 bt-2 border-primary hr-sm">

									<p>
										<?= $donationSnippet[$i] ?>
									</p>
									<div class="row">
										<div class="col-md-3">
											<p> 
												<strong>Donation Amount</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong>: $<?= $donationAmount[$i] ?> </strong> </span>
											</p>
										</div> 
										<div class="col-md-3">
											<p> 
												<strong>Principal Payment</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p <?php if ($donationStatus[$i]=="Unpaid"){echo 'style="color: #E75926; font-weight: 900;"';}else{echo 'style="color: #97CE68; font-weight: 400;"';}?>>
												<span> <strong>: <?= $donationStatus[$i] ?> </strong> </span>
											</p>
										</div>
										<div class="col-md-3">
											<p> 
												<strong>Donation Date</strong>
											</p>
										</div>
										<div class="col-md-3">
											<p>
												<span> <strong>:  <?= $donationDate[$i] ?>  </strong> </span>
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12">
					<div class="divider fs-16"></div>
				</div>
				</div>
				<?php 
				}	
				if ($totalNewDonation == 0){
					echo "
					 <p style='text-align:center;'> You don't have any recent donations... </p> 
					";
				}
				?>
			
		</div>
	</div>
	<!-- End Donation Campaign section -->
	<!-- Payment schedule section -->
	<div class="tab-pane" id="tab-payment" role="tabpanel"> 
		<div class="main-content">
			<div class="row">
				<h3>Payment Schedule</h3>
				<hr>
				<table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
					<thead>
						<tr>
							<th>Campaign name</th>
							<th>Investment date</th>
							<th>Percentage payout</th>
							<th>Payout amount</th>
							<th>Payout date</th>
							<th>Payout Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$number = 0;
						for ($i = 0; $i < count($campaingId); $i++) {
							if ($mysql->Connection()) {
								list($cName, $mPayout) 	= $mysql->GetSingleCampaignForPayout($campaingId[$i]);
								$payoutA 				= explode("==", $mPayout);
								$amountA 				= $dateA = $statusA = array();
								
								for ($k = 0; $k < count($payoutA); $k++) {
									$payouts 		= explode("~", $payoutA[$k]);
									$countPayouts 	= count($payouts);
									
									if ($countPayouts==3){
										$amountA[$k]	= $payouts[0];
										$dateA[$k] 		= $payouts[1];
										$statusA[$k] 	= $payouts[2];
									}
								}

								for ($j = 0; $j < 12; $j++) {
									$newDate 		= date("d F Y", strtotime($dateA[$j]));
									$newDateHid 	= date("Ymd", strtotime($dateA[$j]));
									$invDateHid 	= date("Ymd", strtotime($investDate[$i]));
									$percentProfit 	= $amountA[$j];
									$marginMonthly 	= 0;

									if ($percentProfit!="" or $percentProfit!=null) {
										$number				= $number+1;
										$marginTotal 		= ($campaignReturn[$i]/100)*$investAmount[$i];
										$totalAmount 		= $marginTotal+$investAmount[$i];
										$percentageTotalAmt	= $percentProfit/100;
										$marginMonthly 		= $percentProfit*$totalAmount/100;
										$status				= "";
										
										//if we pay the investor not follow master payout schedule
										$bulanCek 							= $j+1;
										list($singleAmount, $singleStatus)  = $mysql->GetSinglePayoutOutSchedule($invId[$i],"bulan_$bulanCek", "status_$bulanCek");
										
										if ($singleAmount != $marginMonthly or $singleStatus != $statusA[$j]){
											$marginMonthly	= $singleAmount;
											$statusA[$j]	= $singleStatus;
										}
										
										if ($statusA[$j]==1){
											$status="On Going";
										} else if ($statusA[$j]==2){
											$status="Paid";
										} else if ($statusA[$j]==3){
											$status="Delayed";
										}
										
										
										echo "
											<tr>
												<td>
													$campaignName[$i]
												</td>
												<td>
													<span style='display:none;'>$invDateHid </span>$investDate[$i]
												</td>
												<td>
													$percentProfit%
												</td>
												<td>
													SGD ".number_format($marginMonthly, 1, '.', ',')." 
												</td>
												<td> 
													<span style='display:none;'>$newDateHid </span> $newDate 
												</td>
												<td>
													$status
												</td>
											</tr>
											";
									}  
								}
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- end Payment schedule section -->
</div>
<?php

include("footer.php");

?>
</body>
</html>