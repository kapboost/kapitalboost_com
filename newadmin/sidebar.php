
<!-- Sidebar -->
<aside class="sidebar sidebar-expand-lg">
  <header class="sidebar-header" style="background-color: #282830 !important;">
    
    <span class="logo">
      Admin Dashboard
    </span>
    
    
  </header>

  <nav class="sidebar-navigation" style="background-color: #34495E !important;">
    <ul class="menu">
     <li class="menu-category">&nbsp;&nbsp;&nbsp;&nbsp;</li>
     <li class="menu-item">
      <a class="menu-link" href="index.php">
        <span class="icon fa fa-home"></span>
        <span class="title">Dashboard</span>
      </a>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="#">
        <span class="icon fa fa-money"></span>
        <span class="title">Campaigns</span>
		<span class="arrow"></span>
      </a>
	  <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="campaigns.php">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">All Campaigns</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Add New Campaign</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/support/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Campaign Updates</span>
                </a>
              </li>
            </ul>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="#">
        <span class="icon fa fa-money"></span>
        <span class="title">Members</span>
		<span class="arrow"></span>
      </a>
	  <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">All Members</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Add New Member</span>
                </a>
              </li>

            </ul>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="#">
        <span class="icon fa fa-user"></span>
        <span class="title">Investments</span>
		<span class="arrow"></span>
      </a>
	  <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">All Investments</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Xfers</span>
                </a>
              </li>
              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Bank Transfer</span>
                </a>
              </li>
              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Paypal</span>
                </a>
              </li>

            </ul>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="#">
        <span class="icon fa fa-send-o"></span>
        <span class="title">Inbox</span>
		<span class="arrow"></span>
      </a>
	   <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">Get Funded List</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Enquiries List</span>
                </a>
              </li>

            </ul>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="#">
        <span class="icon fa fa-server"></span>
        <span class="title">Blogs</span>
		<span class="arrow"></span>
      </a>
	   <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">All Posts</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Add New Post</span>
                </a>
              </li>

            </ul>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="#">
        <span class="icon fa fa-server"></span>
        <span class="title">Pages</span>
		<span class="arrow"></span>
      </a>
	   <ul class="menu-submenu">
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">Events</span>
                </a>
              </li>
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">Contact Us</span>
                </a>
              </li>
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">Partners</span>
                </a>
              </li>
              <li class="menu-item">
                <a class="menu-link" href="../samples/invoicer/index.html">
				  <span class="icon ti-view-grid"></span>
                  <span class="title">Career</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">About Us (US)</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">About Us (ID)</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">FAQ (US)</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">FAQ (ID)</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Legal (US)</span>
                </a>
              </li>

              <li class="menu-item">
                <a class="menu-link" href="../samples/job/index.html">
                  <span class="icon ti-view-grid"></span>
                  <span class="title">Legal (ID)</span>
                </a>
              </li>

            </ul>
    </li>
    <li class="menu-item">
      <a class="menu-link" href="logout.php">
        <span class="icon fa fa-power-off"></span>
        <span class="title">Logout</span>
      </a>
    </li>

  </ul>
</nav>

</aside>
<!-- END Sidebar -->


<!-- Topbar -->
<header class="topbar">
  <div class="topbar-left">
    <span class="topbar-btn sidebar-toggler"><i>&#9776;</i></span>
    <h3><i>Admin Dashboard KapitalBoost  </i></h3>
  </div>

      <!--<div class="topbar-right">
        <ul class="topbar-btns">
          <li class="dropdown">
            <span class="topbar-btn" data-toggle="dropdown"><img class="avatar" src="assets/img/avatar/1.jpg" alt="..."></span>
            <div class="dropdown-menu dropdown-menu-right">
              <a class="dropdown-item" href="profile.php"><i class="ti-user"></i> Profile</a>
              <a class="dropdown-item" href="#"><i class="ti-settings"></i> Settings</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="logout.php"><i class="ti-power-off"></i> Logout</a>
            </div>
          </li>
        </ul>

      </div>-->
    </header>
    <!-- END Topbar -->

