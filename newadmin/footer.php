

<!-- Footer -->
<footer class="site-footer">
  <div class="row">
    <div class="col-md-12">
      <p class="text-center text-md-left">Copyright © 2017 <a href="http://kapitalboost.com">Kapital Boost</a>. All rights reserved.</p>
    </div>

  </div>
</footer>
<!-- END Footer -->

</main>
<!-- END Main container -->


<!-- Scripts -->
<script src="../member/assets/js/core.min.js"></script>
<script src="../member/assets/js/app.min.js"></script>
<script src="../member/assets/js/script.min.js"></script>
