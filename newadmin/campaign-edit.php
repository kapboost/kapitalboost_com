<?php
session_start();
include("header.php");
include("sidebar.php");
// include_once 'checkSession.php';

include_once '../admin/mysql.php';
$mysql = new mysql();


if ($mysql->Connection()) {
   list($cId, $cName, $projectType, $releaseDate, $closingDate, $totalFund, $minAmount, $tenor, $return, $currentFund) = $mysql->GetAllCampaignsList($keyword);
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

?>

<!-- Main container -->
<main>
		<header class="header bg-ui-general">
        <div class="header-action">
          <ul class="nav nav-tabs nav-justified nav-tabs-info">
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#international" >International</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#bahasa" >Indonesia</a>
                  </li>
                  </li>
                </ul>
        </div>
      </header>
  <div class="main-content">
     <form class="card">
         
          <div class="card-body">
			<div class="tab-content">
                 <div class="tab-pane fade active show" id="international">
                  <div class="form-row">
					  <div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Campaign Name</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Total Funding AMT</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Item to be purchased</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Minimum Investment</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Project Type</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Tenor</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">SME Sub Type</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Curr Funding AMT</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Donation Sub Type</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Shipping Rewards</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Project Return</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Industry</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Campaign Password</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-6">
						<label for="inputEmail4" class="col-form-label">Auto Contract</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Risk</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Release Date</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Closing Date</label>
						<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
					  </div>
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Country</label>
						<input type="password" class="form-control" id="inputPassword4" placeholder="Password">
					  </div>
                </div>
                  <div class="form-row">
					  <div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Campaign Logo</label>
						<div class="file-group">
						  <button class="btn btn-primary file-browser" type="button">Browse</button>
						  <input type="file">
						</div>
					  </div>
					  <div class="form-group col-md-3">
						<label for="inputEmail4" class="col-form-label">Campaign Background</label>
						<div class="file-group">
						  <button class="btn btn-primary file-browser" type="button">Browse</button>
						  <input type="file">
						</div>
					  </div>
					 
					  <div class="form-group col-md-6">
						<label for="inputPassword4" class="col-form-label">Campaign Snippet</label>
						<textarea class="form-control" id="textarea" rows="4"></textarea>
					  </div>
					   </div>
                </div>

             </div>
			 <div class="tab-pane fade active show" id="bahasa">
                  

             </div>
			

         
          
          
        </div>
        </div>
        </form>
      </div><!--/.main-content -->
      
      <?php

      include("footer.php");

      ?>
 

   </body>
   </html>