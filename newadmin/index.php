<?php
session_start();
include("header.php");
include("sidebar.php");
// include_once 'checkSession.php';

// include_once 'mysql.php';
// $mysql = new mysql();



?>

<!-- Main container -->
<main>
  <div class="main-content">
    <div class="row">
      <br><br>
    </div>
    <div class="row">

      <div class="col-md-4">
        <div class="card card-body bg-purple" style="background-color: #395B99 !important;">
          <div class="flexbox">
            <!--<span class="ti-wallet fs-40"></span>-->
            <img src="../assets/images/n-wallet.png" width="48" height="48">
            <span class="fs-40 fw-100"><span id="balance"></span></span>
          </div>
          <div class="text-right">Current e-wallet balance</div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-body bg-pink" style="background-color: #3FA5EA  !important;">
          <div class="flexbox">
            <img src="../assets/images/n-money.png" width="48" height="48">
            <span class="fs-40 fw-100">$</span>
          </div>
          <div class="text-right">Total amount invested</div>
        </div>
      </div>

      <div class="col-md-4">
        <div class="card card-body bg-cyan"  style="background-color: #42CAFD !important;">
          <div class="flexbox">
            <img src="../assets/images/n-money-back.png" width="48" height="48">
            <span class="fs-40 fw-100"> %</span>
          </div>
          <div class="text-right">Average annualised return</div>
        </div>
      </div>

         
          
          
        </div>
      </div><!--/.main-content -->
      
      <?php

      include("footer.php");

      ?>
 

   </body>
   </html>