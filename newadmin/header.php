<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Responsive admin dashboard and web application ui kit.">
  <meta name="keywords" content="dashboard, index, main, picker">

  <title>Admin Dashboard &mdash; Kapital Boost</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,300i" rel="stylesheet">

  <!-- Styles -->
  <link href="../member/assets/css/core.min.css" rel="stylesheet">
  <link href="../member/assets/css/app.min.css" rel="stylesheet">
  <link href="../member/assets/css/style.min.css" rel="stylesheet">

  <!-- Favicons -->
  <link rel="apple-touch-icon" href="../member/assets/img/apple-touch-icon.png">
  <link rel="icon" href="../member/assets/img/logo-alone.png">
</head>

<body>
  
  <!-- Preloader -->
  <div class="preloader">
    <div class="spinner-dots">
      <span class="dot1"></span>
      <span class="dot2"></span>
      <span class="dot3"></span>
    </div>
  </div>