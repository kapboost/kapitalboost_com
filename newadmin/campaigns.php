<?php
session_start();
include("header.php");
include("sidebar.php");
// include_once 'checkSession.php';

include_once '../admin/mysql.php';
$mysql = new mysql();


if ($mysql->Connection()) {
   list($cId, $cName, $projectType, $releaseDate, $closingDate, $totalFund, $minAmount, $tenor, $return, $currentFund) = $mysql->GetAllCampaignsList($keyword);
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}

?>

<!-- Main container -->
<main>
  <div class="main-content">
     <div class="card">
         
          <div class="card-body">

			<table class="table table-striped table-bordered" cellspacing="0" data-provide="datatables">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Campaign Name</th>
                  <th>Campaign Type</th>
                  <th>Release Date</th>
                  <th>Closing Date</th>
                  <th>Current Funding Amt</th>
                  <th>Total Funding</th>
                  <th>Tenor</th>
                  <th>Action</th>
				  
                </tr>
              </thead>
              <tbody>
			   <?php
				  for ($i = 0; $i < count($cId); $i++) {
					 ?>
                <tr>
                   <td><?= $i + 1 ?></td>
					<td><b><a href="/admin/campaign-edit.php?c=<?= $cId[$i] ?>"><?= $cName[$i] ?></a></b></td>
					<td><?= $projectType[$i] ?></td>
					<td><?= $releaseDate[$i] ?></td>
					<td><?= $closingDate[$i] ?></td>
					<td><?= $currentFund[$i] ?></td>
					<td><?= $totalFund[$i] ?></td>
					<td><?= $tenor[$i] ?></td>
                  <td><div class="btn-group">
                      <button class="btn btn-info dropdown-toggle" data-toggle="dropdown">Action</button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">Contract Info</a>
                        <a class="dropdown-item" href="#">Refresh Funding</a>
                        <a class="dropdown-item" href="#">Master Payout</a>
                        <a class="dropdown-item" href="#">Campaign Report</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </div></td>
                </tr>
				<?php
                              }
                              ?>
			</tbody>
			</table>

         
          
          
        </div>
        </div>
      </div><!--/.main-content -->
      
      <?php

      include("footer.php");

      ?>
 

   </body>
   </html>