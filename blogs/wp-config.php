<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'blog_kapitalboost');

/** MySQL database username */
define('DB_USER', 'blog');

/** MySQL database password */
define('DB_PASSWORD', 'blog4kb');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ')t@cGQwGQtgb_=72T?U^W0^fZP1Oi%T?WTvn@P@LeO[-NUEc==0pKd_.]v>8G||O');
define('SECURE_AUTH_KEY',  ' ?G=0O<x|H;M2z;7x $cPFNkR~<NY:G|uWbhXekk=l%>&j.^3U 2`O-{O/1qf2X6');
define('LOGGED_IN_KEY',    's~3>T[*k`eG[n5V82cMfdFGI%8C:lpJ{h&+P$zuo&7tnwQq7s(4g#C!.F!H7Cl2Y');
define('NONCE_KEY',        'BJ?u`icQ2+,?4s`>$g}~/qhcYol:#j:rpx%5eiXqWp.0A$xs>06],)0:~Dcn3}cP');
define('AUTH_SALT',        'PU+H/K+4`evm}08(I*-^@{gccDH$5d~4HE$Ka88#x5YneJN^_)b/a8zsTo7nxO%J');
define('SECURE_AUTH_SALT', '`Pv))uTGS+flVVl551HGVyYFR>B.3qN%D@I~fG5N.gWghc0jtHg]K#_9nm9|EMA<');
define('LOGGED_IN_SALT',   '86GT[&6&MMq2},qR$=)T>g~O<B+@]sie^2`W%j/,FP*T;}S-_d2g{KT>}D*AwnV;');
define('NONCE_SALT',       'dAZQ?_Zhq1%~u1ez5*(!4[R. O:7-^GDYF:g}l&*8+0sfCBw4 WKRE{OH]%csoY1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
