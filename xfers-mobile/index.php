<?php
if (isset($_POST["xferspayment"])) {
  $curl = curl_init();
  header("Content-Access-Allow-Origin : *");
  $currency = $_POST["currency"];
  $amount = $_POST["amount"];
  $notify_url = "https://kapitalboost.com";
  $order_id = $_POST["orderid"];
  $firstname = $_POST["firstname"];
  $lastname = $_POST["lastname"];
  $description = $_POST["description"];

  curl_setopt_array($curl, array(
    CURLOPT_URL => "https://www.xfers.io/api/v3/charges",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => "{ \r\n  \"currency\": \"$currency\", \r\n  \"amount\"  : \"$amount\",\r\n  \"redirect\": \"false\", \r\n  \"notify_url\": \"$notify_url\", \r\n  \"return_url\": \"$notify_url\", \r\n  \"cancel_url\": \"$notify_url\", \r\n  \"order_id\": \"$order_id\", \r\n  \"description\": \"$description\", \r\n  \"shipping\": \"0\", \r\n  \"tax\": \"0\", \r\n  \"items\": [\r\n      { \"description\": \"$description\", \"price\": $amount, \"quantity\": 1, \"name\": \"$description\" }\r\n    ], \r\n  \"meta_data\": { \r\n      \"firstname\": \"$firstname\", \r\n      \"lastname\": \"$lastname\"\r\n   } \r\n}\r\n",
    CURLOPT_HTTPHEADER => array(
      "Content-Access-Allow-Origin : *",
      "cache-control: no-cache",
      "content-type: application/json",
      "x-xfers-user-api-key: 4yphUCpZxym-xEUzNcsHyxK3LFQ5m4X4uxsLKmu3Boc"
    ),
  ));
if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }

  $response = curl_exec($curl);
  $err = curl_error($curl);

  curl_close($curl);

  if ($err) {
    echo "cURL Error #:" . $err;
  } else {
    echo $response;
  }
}

?>