<div id="container">
       <center>
              <div class="partnersWrap">
                     <div class="partnersInner">

                            <h2 class="allNewTitle">Legal<span id="menu_slide" class="plus">+</span></h2>

                            <ul class="nav nav-tabs howUl" id="tabs">
                                   <li class="active" id="termLink"><a href="https://kapitalboost.com/legal#term" data-toggle="tab">Terms of Use</a></li>
                                   <li id="privacyLink" class=""><a href="https://kapitalboost.com/legal#privacy" data-toggle="tab">Privacy Policy</a></li>
                                   <li class="" id="riskLink"><a href="https://kapitalboost.com/legal#risk" data-toggle="tab">Risk Statement</a></li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                   <div class="tab-pane active" id="term">
                                          <p style="text-align:justify"> </p>

                                          <h1><strong>Terms of Use</strong></h1>
                                          <p></p>

                                          <p style="text-align:left"> </p>

                                          <p style="text-align:justify">This website and the contents herein do not constitute as any financial advice, investment advice or solicitation for the purposes of making financial investments in Singapore or other territories. Kapital Boost Pte. Ltd. is a firm specialising in the matching of opportunities between our registered members and small to medium-sized enterprises. With regard to these opportunities, the need and onus to do due diligence lies squarely with our members as we do not profess to advise on the same. All dealings and transactions are directly with the businesses, project owners or authorized agents we refer to our members.</p>

                                          <p style="text-align:justify">Kapital Boost Pte. Ltd. is not licensed and/or registered under the Securities & Futures Act of Singapore or the Financial Advisor's Act under the Monetary Authority of Singapore and thus cannot offer, solicit, advice or recommend the buying and selling of investment securities or any other financial and banking products from the public.</p>

                                          <p style="text-align:justify">Further, we state that in preparing and procuring the information within this document, whilst reasonable care is taken to ensure reliability and accuracy, Kapital Boost Pte. Ltd. and its affiliates, directors, employees, and partners provides no guarantee with regard to its content and completeness and does not accept any liability for losses which might arise from making use of this information.</p>

                                          <p style="text-align:justify">The opinions expressed in this document are those of Kapital Boost Pte. Ltd. at the time of writing. If nothing is indicated to the contrary, all figures are unaudited. The recipient is, in particular, recommended to check that the information provided is in line with his/her own circumstances with regard to any legal, regulatory, tax or other consequences, if necessary with the help of a professional advisor.</p>

                                          <p style="text-align:justify">This document may not be reproduced either in part or in full without the written permission of Kapital Boost Pte. Ltd. It is expressly not intended for persons who, due to their nationality or place of residence, are not permitted access to such information under local law.</p>

                                          <p style="text-align:justify">Every investment involves risk, especially with regard to fluctuations in value and return. Investments in foreign currencies involve the additional risk that the foreign currency might lose value against the investor's reference currency. It should be noted that historical returns and scenarios are no guarantee of future performance. Thank you for reading these notices. If you require additional information or clarifications from us on this matter, please email support@kapitalboost.com.</p>
                                   </div>

                                   <div class="tab-pane" id="privacy">
                                          <p style="text-align:justify"> </p>

                                          <h1><strong>Privacy Policy</strong></h1>

                                          <p> </p>

                                          <p style="text-align:left"> </p>

                                          <p style="text-align:justify">Kapital Boost takes your privacy very seriously. We ask that you read this privacy policy (the “Policy”) carefully as it contains important information about what to expect when Kapital Boost collects personal information about you and how Kapital Boost will use your personal data.</p>

                                          <p style="text-align:justify">This Policy applies to information we collect on www.kapitalboost.com and its sub-domains about:</p>

                                          <p style="margin-left:36pt; text-align:justify">i) visitors to our website (the “Site”); and</p>

                                          <p style="margin-left:36pt; text-align:justify">ii) people who register their emails with us.</p>

                                          <p style="text-align:justify"> </p>

                                          <h3 style="text-align:justify"><strong><span style="color:#1f497d">1.  Information collected from all visitors to our website</span></strong></h3>

                                          <p style="text-align:justify">We will obtain personal data about you when you visit us. When you visit us, we may monitor the use of this Site through the use of cookies and similar tracking devices. For example, we may monitor the number of times you visit our Site or which pages you go to. This information helps us to build a profile of our users. Some of this data will be aggregated or statistical, which means that we will not be able to identify you individually. Please see further the section on “Our Use of Cookies” below.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">2.  Information collected from users who register their emails with us</span></strong></h3>

                                          <p style="text-align:justify">When you register your emails with us, we may use it for the following:</p>

                                          <p style="margin-left:36pt; text-align:justify">(a) will send you newsletters, contact you about products and services we think may be of interest to you.</p>

                                          <p style="margin-left:36pt; text-align:justify">(b) Your information may also be used by Club Ethis, a private network owned by Ethis Pte Ltd, our affiliated company, in contacting you about their products and services.</p>

                                          <p style="text-align:justify">If you agree to us providing you with these information, you can always opt out at a later date. If you would rather not receive these information, please send an email message to support@kapitalboost.com with the subject UNSUBSCRIBE.</p>

                                          <p style="text-align:justify">By submitting your email you consent to the use of that email as set out in this policy.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">3.   Our use of cookies</span></strong></h3>

                                          <p style="text-align:justify">Cookies are text files placed on your computer to collect standard Internet log information and visitor behaviour information. The information is used to track visitor use of the Site and to compile statistical reports on Site activity. For further information about cookies visit www.aboutcookies.org or www.allaboutcookies.org. You can set your browser not to accept cookies and the above websites tell you how to remove cookies from your browser. However, in a few cases some of our Site features may not function if you remove cookies from your browser.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">4.   How we protect your information</span></strong></h3>

                                          <p style="text-align:justify">We are committed to maintaining the security of personal information. We have put in place appropriate security procedures and technical and organisational measures to safeguard your personal information such as securing our websites with SSL.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">5.   Access to your information and updating and correcting your information</span></strong></h3>

                                          <p style="text-align:justify">Subject to the exceptions referred to in section 21(2) of the Personal Data Protection Act 2012 (No. 26 of 2012) of Singapore (“PDPA”), you have the right to request a copy of the information that we hold about you. If you would like a copy of some or all of your personal information, please send an email to support@kapitalboost.com. We may make a small charge for this service.</p>

                                          <p style="text-align:justify">We want to ensure that your personal information is accurate and up to date. If any of the information that you have provided to Kapital Boost changes, for example if you change your email address, name or payment details, or if you wish to cancel your registration, please let us know the correct details by sending an email to support@kapitalboost.com. You may ask us, or we may ask you, to correct information you or we think is inaccurate, and you may also ask us to remove information which is inaccurate.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">6.   Non-disclosure</span></strong></h3>

                                          <p style="text-align:justify">We do not sell, trade, or otherwise transfer to third parties your personally identifiable information.  This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, as long as these parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours and others’ rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">7.   Links to other websites</span></strong></h3>

                                          <p style="text-align:justify">Our Site may contains links to other websites. This privacy policy only applies to this website so when you link to other websites you should read their own privacy policies.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">8.   Sale of business</span></strong></h3>

                                          <p style="text-align:justify">If Kapital Boost’s business is sold or integrated with another business your details may be disclosed to our advisers and any prospective purchasers and their advisers and will be passed on to the new owners of the business.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">9.   Changes to privacy policy</span></strong></h3>

                                          <p style="text-align:justify">We keep our privacy policy under regular review. If we change our privacy policy we will post the changes on this page, and place notices on other pages of the Site, so that you may be aware of the information we collect and how we use it at all times. This privacy policy was last updated on 27th day of August 2015.</p>

                                          <h3 style="text-align:justify"><br />
                                                 <strong><span style="color:#1f497d">10.  How to contact us</span></strong></h3>

                                          <p style="text-align:justify">We welcome your views about our Site and our privacy policy. If you would like to contact us with any queries or comments please send an email to support@kapitalboost.com.</p>

                                          <p> </p>
                                   </div>

                                   <div class="tab-pane" id="risk">
                                          <p style="text-align:left"> </p>

                                          <h1><strong>Risk Statement</strong></h1>

                                          <p> </p>

                                          <h3> </h3>

                                          <p style="text-align: justify;"><span style="color:#1f497d"><strong>Overview</strong></span></p>

                                          <p style="text-align: justify;">Kapital Boost enables you to invest directly in SMEs by funding, together with others, their business needs. A contractual relationship is established between each individual investor and the SME. Kapital Boost is not a party to these contracts but agrees to facilitate and administer them under our Terms and Conditions.</p>

                                          <p style="text-align: justify;"> </p>

                                          <p style="text-align: justify;">It is important to remember that investments carry a degree of risk; in this case when SMEs, due to changing business conditions, are unable to fulfill their financial obligations to investors. When investing, your principal is at risk.</p>

                                          <p style="text-align: justify;"> </p>

                                          <p style="text-align: justify;"><span style="color:#1f497d"><strong>Due diligence</strong></span></p>

                                          <p style="text-align: justify;">Kapital Boost performs a high standard of screening and due diligence on every SMEs before they are offered to our investors. As part of our initial screening, SMEs seeking funding will have to meet the following minimum requirements: 1) being in operation for at least one year, 2) having minimum annual sales of SGD100, 000 and 3) having a positive free cash flow in the past 12 months. We believe these requirements can assist in identifying SMEs with viable and sustainable businesses.</p>

                                          <p style="text-align: justify;">Kapital Boost also offers overseas investing in developing markets like Indonesia and Malaysia, which pose higher operating risks. To address this, Kapital Boost builds partnerships with SME/entrepreneur groups. We offer their members an alternative funding opportunities to support business growth. In return, our partners assist in initial screening, background checks, and monitoring of SME members. Partners can also assist in ensuring timely payments by SMEs to Kapital Boost members. In Indonesia, we have partnered with the largest entrepreneur community Tangan di Atas (TDA).</p>

                                          <p style="text-align: justify;"> </p>

                                          <p style="text-align: justify;"><span style="color:#1f497d"><strong>Diversification</strong></span></p>

                                          <p style="text-align: justify;">We cannot stress enough the importance of investment diversification to reduce overall risk. We advise Kapital Boost members to invest smaller amounts in different funding campaigns, rather than putting all their investments into one to reduce the chances of losses. This helps protect investors money from the effects of any one SME missing a payment or defaulting. Especially while we are growing our funding offerings, we also advise Kapital Boost members to invest in other asset classes, particularly in sectors that offer different risk profiles to that of the small business sectors.</p>

                                          <p style="text-align: justify;"> </p>

                                          <p style="text-align: justify;"><span style="color:#1f497d"><strong>Missed payments</strong></span></p>

                                          <p style="text-align: justify;">Missed payments will be pursued by Kapital Boost. The SME will be immediately contacted upon a missed scheduled payment to offer an explanation. Investors will be updated accordingly. Continued reminders for payments will be made to the business until they are fully paid.</p>

                                          <h3 style="text-align: justify;"> </h3>

                                          <p style="text-align: justify;"><span style="color:#1f497d"><strong>Defaults</strong></span></p>

                                          <p style="text-align: justify;">Overdue payments past 60 days will be considered a default. In such a scenario, Kapital Boost will offer investors several options to pursue:</p>

                                          <p style="text-align: justify;"> </p>

                                          <ul>
                                                 <li style="text-align: justify;">Restructure the funding agreement to allow for a longer payment term. Investors may require additional securities from the business to secure the payment of the rescheduled financing.</li>
                                                 <li style="text-align: justify;">For a secured deal, legally liquidate secured assets at market value to recover for the loss in investment principal.</li>
                                                 <li style="text-align: justify;">If there is proof of negligence in the use of investment funds, take legal action against the business and shareholders and/or directors. Legal actions may also be taken to require the business to surrender information, which may be crucial in determining its ability to repay its financial obligations. Any legal actions taken against the business will only be taken after getting investors' consent.</li>
                                          </ul>

                                          <p style="text-align: justify;"> </p>

                                          <p style="text-align: justify;">Which options are taken will depend on a majority vote (based on investment value) by investors.</p>

                                          <p> </p>

                                          <p><span style="color:#1f497d"><strong>Platform failure</strong></span></p>

                                          <p style="text-align:left">In a scenario where Kapital Boost ceases to operate as a crowdfunding platform, SMEs are still liable to service its financial obligations to investors. All financing arrangements are legally binding between SMEs and investors and would continue to be enforceable if Kapital Boost goes out of business.</p>

                                          <p style="text-align:left">Kapital Boost does not hold investors money except that we are an agent transferring cash from investors to SMEs, and vice versa. Therefore Kapital Boost holds no claim to the money you invest.</p>

                                          <p> </p>
                                   </div>
                            </div>
                     </div>

                     <script>
                            $(document).ready(function () {
                                   $(window).load(function () {
                                          $('html, body').stop(true, true).animate({'scrollTop': $('#tabs').offset().top - 90});
                                          var hash = window.location.hash;
                                          if (hash != "") {
                                                 $('#tabs li').each(function () {
                                                        $(this).removeClass('active');
                                                 });
                                                 $('#myTabContent div').each(function () {
                                                        $(this).removeClass('in active');
                                                 });
                                                 var link = "";
                                                 $('#tabs > li').each(function () {
                                                        link = $(this).find('a').attr('href');
                                                        if (link == hash) {
                                                               $(this).addClass('active');
                                                        }
                                                 });
                                                 $('#myTabContent div').each(function () {

                                                        link = $(this).attr('id');
                                                        if ('#' + link == hash) {
                                                               $('' + hash + 'Link').addClass('active');
                                                               $(this).addClass('in active');
                                                        }
                                                 });
                                          }
                                   });
                            });
                     </script>
              </div>
</div>
