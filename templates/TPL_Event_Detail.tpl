
<div id="blog-1" style="display:block;overflow:auto;">
       <div id="blog-1-back">
              <div id="blog-1-inner">
                     <p class="description"></p>
                     <div class="wrapper clear">
                            <div class="post-list">
                                   <div id="cateventoutput">
                                          
                                                 <div class="blog-post">
                                                        <h1 class="post-title">{$content.event_title}</h1>
                                                        <div class="post-details">{$content.release_date|date_format:"%A, %B %e, %Y"} </div>
                                                        <div class="post-body">

                                                               <img alt="" src="../assets/images/event/{$content.image}" style="max-width: 100%;width: auto;" />
                                                               <br />
                                                               <br />
                                                               <br />

                                                               {$content.event_content}
															   
                                                               <div class="campaigns-industry">
                                                                      <p class="campaigns-industry-name"><p> Categories: <img class="campaigns-marker" src="../../assets/images/tag.png" /> {section name=outer loop=$tags}<a href="https://kapitalboost.com/index.php?class=Event&method=LoadList&tag={$tags[outer]}">{$tags[outer]}</a> {/section}</p>
                                                               </div>

                                                        </div>
                                                 </div>
                                   </div>
                            </div>
                     </div>
              </div>
       </div>
</div>

<script type="text/javascript" src="assets/js/kp-event.js"></script>