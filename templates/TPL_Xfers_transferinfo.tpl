<link rel="stylesheet" type="text/css" href="/xfers/assets/css/gaya.css">

<div class="partnersWrap" ng-app="xfers" ng-controller="xfersCtrl">
       <div class="partnersInnerDas" style="width: 100%; margin: 0px">
              <center>
                     <!-- start xfers transferinfo -->
                     <div class="xfersTransferinfo">
                            <div class="XTInner">
                                   <div class="XTLogo">
                                          <img src="/assets/images/xfersLogo.png" />
                                   </div>
                                   <div class="XTTitle">
                                          <p>
                                                 Deposit
                                          </p>
                                   </div>
                                   <div class="XTText">
                                          <p>
                                                 To deposit funds into your e-Wallet, please proceed to make a bank transfer to the following account. Once deposited,
                                                 your e-Wallet balance will be updated within 2-4 hours.
                                          </p>
                                   </div>
                                   <ul class="XTInfo">
                                          <li>
                                                 <div>
                                                        <p>
                                                               Bank name
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               :
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               {literal}
                                                                      {{bank.bank_name_full}}
                                                               {/literal}
                                                        </p>
                                                 </div>
                                          </li>

                                          <li>
                                                 <div>
                                                        <p>
                                                               Bank account number
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               :
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               {literal}
                                                                      {{bank.bank_account_no}}
                                                               {/literal}
                                                        </p>
                                                 </div>
                                          </li>
                                          <li>
                                                 <div>
                                                        <p>
                                                               Bank code
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               :
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               {literal}
                                                                      {{bank.bank_code}}
                                                               {/literal}
                                                        </p>
                                                 </div>
                                          </li>

                                          <li>
                                                 <div>
                                                        <p>
                                                               Unique_ID
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               :
                                                        </p>
                                                 </div>
                                                 <div>
                                                        <p>
                                                               {literal}
                                                                      {{bank.unique_id}}
                                                               {/literal}
                                                        </p>
                                                 </div>
                                          </li>
                                   </ul>
                                   <div class="XTText2">
                                          <p style="text-align: center;margin: 0 0 0 0;">
                                                 <i>Please leave your Unique ID in the comments field during the bank transfer</i>
                                          </p>
                                   </div>
                                   <div class="XTNote">
                                          <p>
                                                 Note: Deposits must be sent from accounts that match your <br />
                                                 verified legal name. Please only send deposits denominated in {literal}{{currency}}{/literal}
                                          </p>
                                   </div>
                                   <div class="XTButton">
                                          <form action="/dashboard/payment/xfers-ballance?mid={$mid}">
                                                 <button style="color:#FFFFFF">
                                                        OK
                                                 </button>
                                          </form>
                                   </div>
                            </div>
                     </div>
                     <!-- end x fers transferinfo -->


              </center>
       </div>
</div>
{literal}
       <script src="/xfers/assets/js/angular.min.js"></script>
       <script type="text/javascript">

              var app = angular.module("xfers", []);
              app.controller("xfersCtrl", function ($scope, $http) {

                     $scope.bank = {};

                     var getPhone = function () {
                            return $scope.cphone = JSON.parse(localStorage.getItem('user'));
                     };
                     var getCountry = function () {
                            return $scope.cCountry = JSON.parse(localStorage.getItem('country'));
                     };
                     var country = getCountry().country;
                     if(country == 'SINGAPORE'){
                            $scope.currency = "SGD";
                     }else{
                            $scope.currency = "IDR";
                     }
                     $scope.refresh = function () {
                            getPhone();
                            var country = getCountry().country;
                            if (country === 'SINGAPORE') {
                                   var url = "/xfers/apis/transferInfo.php";
                            } else {
                                   var url = "/xfers_indo/apis/transferInfo.php";
                            }

                            $http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "gettransferinfo": $scope.cphone.phone
                                   }
                            }).success(function (res) {
                                   localStorage.setItem('transfer', JSON.stringify(res));
                                   $scope.bank = res;

                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });
                     };

                     var getTransferInfo = function () {
                            getPhone();
                            var country = getCountry().country;
                            if (country === 'SINGAPORE') {
                                   var url = "/xfers/apis/transferInfo.php";
                            } else {
                                   var url = "/xfers_indo/apis/transferInfo.php";
                            }

                            $http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "gettransferinfo": $scope.cphone.phone
                                   }
                            }).success(function (res) {
                                   console.log(res);
                                   localStorage.setItem('transfer', JSON.stringify(res));
                                   $scope.bank = res;
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });
                     };
                     // Xfers send token OTP
                     // 
                     getTransferInfo();
              });
       </script>
{/literal}