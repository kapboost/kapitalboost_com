<div id="menu">
       <div id="menu-back"></div>
       <center>
             <!-- <ul class="newMenu" style="background-color: #f6f6f6; border-top-left-radius: 0px; border-top-right-radius: 0px; border-bottom-right-radius: 10px; border-bottom-left-radius: 10px; box-shadow: black 7px 7px 10px;">
                     <li><a href="/dashboard/payment-history"> Portfolio </a></li>
                     <li><a href="/dashboard/profile"> Manage Profile </a></li>
                     {if  $xfers_token != ""}
                     <li style="color:#0073e6">eWallet Balance - {if $country === 'SINGAPORE'}SGD {elseif $country === 'INDONESIA'}IDR {/if} <span id="balance"></span></li>
                     {else}
                     {if $phone != ""}   
                            <!--<li style="color:#0073e6"><a href="/dashboard/payment/xfers-ballance#Activate">Activate eWallet</a></li>-->
                     {else}
                            <!--<form>
                                   <li style="color:#0073e6"><a href="/dashboard/payment/xfers-ballance#Activate">Activate eWallet</a></li>
                            </form>
                            <!--<li style="color:#0073e6"><a href="/dashboard/payment/xfers-ballance#Activate">Activate eWallet</a></li>-->
                            {/if}
                     
                     {/if}

              <!--</ul>-->
              <!--<ul class="dasUl">
                     <li>
                            <a href="dashboard/payment-history" style="cursor:pointer;">
                                   Payment History
                            </a>
                     </li>

                     <li>
                            <a href="dashboard/profile"style="cursor:pointer;">
                                   Profile
                            </a>
                     </li>
              </ul>-->
       </center>
       <div id="menu-front">
              <div id="menu-front-inner">
                     <div id="menu-logo">
                            <a href="">
                                   <img alt="Kapital Boost Logo" id="menu-logo-img" src="assets/images/logo-baru.png" />
                            </a>
                     </div>
                     <ul id="kb-menu">
                            <li><a href="/">home</a>
                            </li>
                            <li><a href="about">About</a>
                            </li>
							{if $member_id}
                            <li><a href="campaign/">Campaigns</a>
                            </li>
							{else}
							<li><a id="eth-log" class="loginRegisMenu">Campaigns</a> 
                            </li>
							{/if}
                            <li><a href="get-funded">Get Funded</a>
                            </li>                           

                            <li><a href="how-it-works">How It Works</a>
                            </li>  


                            {if $member_id}
								{if $member_name}
                                   
                                  
                                   <li>
                                          <b><a href="member/" class="loginRegisMenus" style="color: #609edf;">{$member_name}'s Dashboard</a></b> 
                                   </li> 
								    {else}
                                   <li>
                                          <b><a href="member/" class="loginRegisMenus" style="color: #609edf;">{$member_firstname}'s Dashboard</a></b> 
                                   </li> 
								   {/if}
								    <li>
                                          <a id="testbro" class="loginRegisMenus" style="color: #609edf;">Log Out</a> 
                                   </li>     
                            {else}
                                   <li><a id="eth-log" class="loginRegisMenu" style="color: #609edf;">Login</a> 
                                   </li>
                                   <li><a id="reg" class="loginRegisMenu" style="color: #609edf;">Register</a>
                                   </li>
                            {/if}
							<!-- <li><a href="http://www.kapitalboost.co.id"> <img src="assets/images/idn.png" style="max-width: 15px; margin-bottom: 3.5px;"> Bahasa</a> -->
                            <!-- </li>   -->
                     </ul>
                     <div id="rwd-menu">
                            <div id="rwd-menu-inner">
                                   <div class="rwd-menu-each"></div>
                                   <div class="rwd-menu-each"></div>
                                   <div class="rwd-menu-each"></div>
                            </div>
                     </div>
              </div>
       </div>
</div>

<div class="logpopupWrap" id="logoutDiv">
       <div class="logpopupInner">
              <div class="logpopupClose">
                     <span class="glyphicon glyphicon-remove"></span>
              </div>
              <div class="logpopupTitle">
                     Are you sure want to logout?
              </div>
              <ul class="logpopupYesNo">
                     <li>
                            <a id="logoutBtn">
                                   <p>
                                          Yes
                                   </p>
                            </a>
                     </li>
                     <li>
                            <a id="logpopupClose">
                                   <p>
                                          No
                                   </p>
                            </a>
                     </li>
              </ul>
       </div>
</div>

{literal}
       <script>
              $(document).ready(function () {

                     $('#testbro').click(function () {
                                   //$('.logpopupWrap').fadeIn(300);
                                   $("#logoutDiv").fadeIn(300);
                                   
                     });
                     $("#logoutBtn").on("click",function(){
                            FB.logout();
                            window.location = "logout";
                     });
                     $('#logpopupClose').click(function () {
                            $('.logpopupWrap').fadeOut(300);
                     });
                     $('.logpopupClose').click(function () {
                            $('.logpopupWrap').fadeOut(300);
                     });
                     var country = '{/literal}{$country}{literal}';
                     if (country == 'SINGAPORE') {
                                   var url = "/xfers/apis/ballance.php";
                            } else {
                                   var url = "/xfers_indo/apis/ballance.php";
                            }
                     $.ajax({
                            url: url,
                            type: 'post',
                            data:{
                                   "getuserballance":'{/literal}{$phone}{literal}'
                            },
                            headers:{
                                   'Content-Type':'application/x-www-form-urlencoded'
                            },
                            dataType: 'json',
                            success: function (data) {
                                   $("#balance").text(data.available_balance);
                            }
                     });
                     
                     /*$http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "getuserballance": $scope.cphone.phone
                                   }
                            }).success(function (res) {
                                   $scope.xballance = res;
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });*/
              });
              /*
               $("#dialog").dialog({
               autoOpen: false,
               modal: true,
               resizable: false,
               height: "auto",
               width: 400,
               buttons : {
               "Yes" : function() {
               window.location.href="https://kapitalboost.com/logout";           
               },
               "No" : function() {
               $(this).dialog("close");
               }
               }
               });
               
               $("#testbro").on("click", function(e) {
               e.preventDefault();
               $("#dialog").dialog("open");
               });
               */
       </script>
{/literal}
