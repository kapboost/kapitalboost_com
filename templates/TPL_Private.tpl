<!-- get_campaign-->
<script type="text/javascript" src="assets/js/kp-main.js"></script>
<link rel="stylesheet" href="assets/css/new-kp-campaigns.css" />
<div class="allBanner">
       <img src="../assets/images/campaign.jpg" />
       <div class="allTitle campaignWrap">
              <div class="campaignInner">
                     <center>
                            <div class="campaignTitle">
                                   <p>Campaigns</p>
                            </div>
                            <p class="page-blurb"></p>
                            <form method="POST" id="payment" action="">
                                   <select name="project_type" id="project_type" class="form-control" data-live-search="true" data-style="btn-default">
                                          <option value="https://kapitalboost.com/campaign/all"> Select Campaign Type </option>
                                          <option value="https://kapitalboost.com/campaign/sme/">SME Crowdfunding</option>
                                          <option value="https://kapitalboost.com/campaign/donation/">Donation</option>

                                          <!-- <option value="https://kapitalboost.com/campaign/private/">Private Crowdfunding</option> -->
                                   </select>
                            </form>
                     </center>
              </div>
       </div>
</div>
<div id="container">
       <div id="campaigns">
              <div id="campaigns-inner">

                     <div class="campaignInnerHp">
                            <center>
                                   <div class="campaignTitle">
                                          <p>Campaigns</p>
                                   </div>
                                   <p class="page-blurb"></p>
                                   <form method="POST" id="payment" action="">
                                          <select name="project_type" id="project_type" class="form-control" data-live-search="true" data-style="btn-default">
                                                 <option value="https://kapitalboost.com/campaign/all"> Select Campaign Type </option>
                                                 <option value="https://kapitalboost.com/campaign/sme/">SME Crowdfunding</option>
                                                 <option value="https://kapitalboost.com/campaign/donation/">Donation</option>

                                                 <!-- <option value="https://kapitalboost.com/campaign/private/">Private Crowdfunding</option> -->
                                          </select>
                                          <button type="submit" value="search" class="filter_search">Search</button>
                                   </form>
                            </center>
                     </div>

                     <ul id="campaigns-list">
                            {section name=outer loop=$content}
                            {if $member_id}
                            <li onclick="window.location.href = 'campaign/view/{$content[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each">
                                   {else}
                            <li id="eth-log" style="cursor:pointer;" class="cam-li-each">
                                   {/if}
                                   <div class="campaigns-list-con">
                                          <input type="hidden" class="total-amount" value="{$content[outer].total_funding_amt}" />
                                          <input type="hidden" class="curr-amount" value="{$content[outer].curr_funding_amt}" />

                                          <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$content[outer].campaign_background}')"></div>
                                          <div class="campaigns-list-middle">
                                                 <h4 class="campaigns-title">{$content[outer].campaign_name}</h4>
                                                 <span class="campaigns-company">{$content[outer].classification}<br /><br /></span>
                                                 <div class="campaigns-desc">
                                                        <p>{$content[outer].campaign_description|strip_tags|strip|truncate:120}</p>
                                                 </div>
                                                 <!--
                                                                                            <p class="campaigns-riskP">
                                                                                                 Risk: {if $content[outer].risk=="High"}
                                                                                                 <label class="label label-danger">High</label>
                                                                                                 {elseif $content[outer].risk=="Medium"}
                                                                                                 <label class="label label-warning">Medium</label>
                                                                                                 {else}
                                                                                                 <label class="label label-success">Low</label>
                                                                                                 {/if}
                                                                                             </p>
                                                 -->
                                                 {if $member_id}
                                                 <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$content[outer].campaign_id}">read more</a>
                                                 </p>
                                                 {else}
                                                 <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">read more</a>
                                                 </p>
                                                 {/if}
                                          </div>
                                          <div class="campaigns-list-bottom">
                                                 <div class="campaigns-list-bottom-1">
                                                        <div class="campaigns-country">
                                                               <img class="campaigns-marker" src="../../assets/images/marker.png" /> 
                                                               <p class="campaigns-country-name">{$content[outer].country}</p>
                                                        </div>
                                                        <div class="campaigns-industry">
                                                               <img class="campaigns-marker" src="../../assets/images/industry-icon.png" /> 
                                                               <p class="campaigns-industry-name">{$content[outer].industry}</p>
                                                        </div>
                                                        <div style="clear:fix"></div>
                                                 </div>
                                                 <div class="campaigns-bar">
                                                        <div class="campaigns-bar-inner"></div>
                                                 </div>
                                                 <ul class="campaigns-data">
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$content[outer].total_funding_amt|number_format}</p>
                                                               <p>Target</p>
                                                        </li>                                                                                
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$content[outer].curr_funding_amt|number_format}</p>
                                                               <p>Funded</p>
                                                        </li>
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$content[outer].project_return} %</p>
                                                               <p>Returns</p>
                                                        </li>

                                                        {if $content[outer].duration <= 0}
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">0</p>
                                                               <p>Tenor</p>
                                                        </li>
                                                        {else}
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$content[outer].duration}</p>
                                                               <p>Tenor</p>
                                                        </li>
                                                        {/if}
                                                        {if $content[outer].days_left <= 0}
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">0, Closed</p>
                                                               <p>Days Left</p>
                                                        </li>
                                                        {else}
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$content[outer].days_left}</p>
                                                               <p>Days Left</p>
                                                        </li>
                                                        {/if}
                                                        <li style="background: white;">
                                                               <div class="" style="font-size: 14px;text-align: center;">
                                                                      {if $content[outer].risk=="High"}
                                                                      <label class="label label-danger">High</label><br />
                                                                      {elseif $content[outer].risk=="Medium"}
                                                                      <label class="label label-warning">Medium</label><br />
                                                                      {else}
                                                                      <label class="label label-success">Low</label><br />
                                                                      {/if}
                                                                      Risk rating 
                                                               </div>
                                                        </li>

                                                        <!--
                                                                                                    <li style="background: white;">
                                                                                                        <p class="campaigns-data-bold">S$ {$content[outer].minimum_investment|number_format}</p>
                                                                                                        <p>Minimum</p>
                                                                                                    </li> 
                                                        -->
                                                 </ul>
                                          </div>
                                   </div>
                            </li>
                            </li>
                            {/section}
                     </ul>
                     <center>
                            <div class="pagination" style="margin: 0;">{$pager}</div>
                     </center>
              </div>
       </div>
</div>
<script>
       {literal}
       document.getElementById('payment').project_type.onchange = function () {
              var newaction = this.value;
              document.getElementById('payment').action = newaction;
       }
       ;
               {/literal}
</script>



<script>
                       {literal}
               $(function () {
                      //$('#private-lightbox-new').fadeIn();
                      window.location.href = "https://kapitalboost.com/campaign/private/1";
               });
                       {/literal}
</script>