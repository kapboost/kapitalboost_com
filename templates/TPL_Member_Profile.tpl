<!-------------------------------------------- main content area wrapper -------------------------------------------->
<div class="subpage-main-content-area-wrapper">
       <div class="subpage-content-wrapper subpage-content-wrapper-2">

              <!-------------------------------------------- subpage breadcrumb area wrapper -------------------------------------------->
              <div class="breadcrumb-wrapper-2">
                     <div class="breadcrumb-wrapper-inner-2">
                            <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   {$breadcrumb}
                            </ol>
                     </div>
              </div>
              <!-------------------------------------------- //subpage breadcrumb area wrapper -------------------------------------------->

              <!-------------------------------------------- subpage mobile breadcrumb area wrapper -------------------------------------------->
              <div class="breadcrumb-mobile-wrapper">
                     <div class="breadcrumb-wrapper-inner">
                            <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   {$breadcrumb}
                            </ol>
                     </div>
              </div>
              <!-------------------------------------------- //subpage mobile breadcrumb area wrapper -------------------------------------------->
              <div class="clearfix"></div>
              {if $error_msg}
              <div class="row">
                     <div class="col-xs-12">
                            <div class="alert alert-danger">
                                   {section name=b loop=$error_msg}
                                   <div>{$error_msg[b]}</div>
                                   {/section}
                            </div>
                     </div>
                     <Div class="clear"></Div>
              </div>
              {/if}

              {if $success_msg}
              <div class="row">
                     <div class="col-xs-12">
                            <div class="alert alert-success">
                                   <div>{$success_msg}</div>
                            </div>
                     </div>
                     <Div class="clear"></Div>
              </div>
              {/if}


              <!--------------------------------------- mobile sidebar nav wrapper --------------------------------------->
              {$sidebar_mobile}
              <!--------------------------------------- //mobile sidebar nav wrapper --------------------------------------->
              <div class="clearfix"></div>




              <!-------------------------------------------- member area wrapper -------------------------------------------->
              <div class="inner-container">
                     <!-------------------------------------------- member content left area wrapper -------------------------------------------->
                     {$sidebar}
                     <!-------------------------------------------- //member content left area wrapper -------------------------------------------->


                     <!-------------------------------------------- member content right area wrapper -------------------------------------------->
                     <div class="member-content-right-area-wrapper">
                            <form action="" method="post" id="register_form">
                                   <div class="member-content-right-area-wrapper-inner">

                                          <!--------------------------------------- main header --------------------------------------->
                                          <div class="panel-main-header-wrapper">
                                                 <div class="panel-main-header-wrapper-inner">Personal Detail</div>
                                          </div>
                                          <!--------------------------------------- main header --------------------------------------->
                                          <br>

                                          <!-------------------------------------------- member login detail wrapper -------------------------------------------->
                                          <div class="member-data-wrapper">
                                                 <p class="header-2">Login Detail</p>
                                                 <br>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Email Address*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_email" value="{$member_email}">
                                                               <div class="member-profile-info-text"><span>This will serve as your LOGIN ID</span></div>
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Password*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="password" class="form-control form-field member-profile-field" name="member_password" placeholder="Password">
                                                               <div class="member-profile-info-text"><span>Max 15 Character</span></div>
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Confirm Password*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="password" class="form-control form-field member-profile-field" name="retype_password" placeholder="Confirm Password">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                          </div>
                                          <!-------------------------------------------- //member login detail wrapper -------------------------------------------->
                                          <br>


                                          <!-------------------------------------------- member personal detail wrapper -------------------------------------------->
                                          <div class="member-data-wrapper">
                                                 <p class="header-2">My Profile</p>
                                                 <br>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Full Name*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_givenname" value="{$member_givenname}">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>

                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Contact No*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_contact_no" value="{$member_contact_no}">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>

                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Mobile No*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_mobile_no" value="{$member_mobile_no}">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>

                                          </div>
                                          <!-------------------------------------------- //member personal detail wrapper -------------------------------------------->
                                          <br>



                                          <!-------------------------------------------- member billing detail wrapper -------------------------------------------->
                                          <div class="member-data-wrapper">
                                                 <p class="header-2">Billing Address</p>
                                                 <br>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Address*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <textarea name="member_street_1" cols="" rows="" class="form-control form-field member-profile-field">{$member_street_1}</textarea>
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">City*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_city" value="{$member_city}" placeholder="City">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">State*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_state" value="{$member_state}" placeholder="State">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Country*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <select name="member_country" class="form-control form-field member-profile-field">
                                                                      <option value=""  >--Select Country--</option>
                                                                      {section name=a loop=$country}
                                                                      <option value="{$country[a].name}"   {if $member_country==$country[a].name}selected="selected"{/if}>{$country[a].name}</option>
                                                                      {/section}
                                                               </select>
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-profile-label-wrapper label-black">Postal Code*</label>
                                                        <div class="member-profile-field-wrapper">
                                                               <input type="text" class="form-control form-field member-profile-field" name="member_postal_code" value="{$member_postal_code}" placeholder="Postal Code">
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                          </div>
                                          <!-------------------------------------------- //member billing detail wrapper -------------------------------------------->
                                          <br>
                                          <br>

                                          <!-------------------------------------------- member billing detail wrapper -------------------------------------------->
                                          <div class="member-data-wrapper">
                                                 <p class="header-2">Subscribe</p>
                                                 <br>
                                                 <div class="form-group">
                                                        <input type="checkbox" name="promotion" value="1" {if $promotion == 1}checked="checked"{/if}/>
                                                               <label class="label-black">Yes! Please keep me updated with Temple latest exclusive offers and promotions.</label>
                                                        <div class="clearfix"></div>
                                                 </div>
                                          </div>
                                          <!-------------------------------------------- //member billing detail wrapper -------------------------------------------->
                                          <br>
                                          <br>

                                          <button class="btn btn-main">Save Changes</button>

                                   </div>
                            </form>
                     </div>
                     <!-------------------------------------------- //member content right area wrapper -------------------------------------------->
                     <div class="clearfix"></div>
              </div>
              <!-------------------------------------------- //member area wrapper -------------------------------------------->


       </div>
</div>
<!-------------------------------------------- //main content area wrapper -------------------------------------------->
