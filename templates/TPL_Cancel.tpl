<!-------------------------------------------- main content area wrapper -------------------------------------------->
<div class="subpage-main-content-area-wrapper">
	<div class="subpage-content-wrapper subpage-content-wrapper-2">
		<div class="inner-container">
			
			<div class="checkout-receipt-area-wrapper">
			<!-------------------------------------------- main content left area wrapper -------------------------------------------->
			<div class="main-content-area-wrapper">
				<!--------------------------------------- receipt section --------------------------------------->
				<div class="panel-wrapper checkout-receipt-wrapper">
					<div class="panel-header-wrapper top-panel-header">
						<div class="panel-inner" style="font-style:normal !important; text-transform:uppercase">Cancel</div>
					</div>
					<br>
					<div class="panel-inner checkout-receipt-content-wrapper">
						<p class="checkout-receipt-main-header">Thank You for Placing your order with Temple</p><br>
						<p><font color="black">Your payment for <strong class="c-green">{$transaction_id}</strong> is failed / canceled. Please make payment manually to complete your order status.</p>
						<p>Please fell free to contact  our Internet Team at <strong class="c-green">021-xxxxxx</strong> for any assistance. Our bussiness operating hour are from Monday to Friday between 8.00am to 17.00pm.</font></p><br>
						<p><font color="black">
						Have a Great Day<br>
						Best regards,<br>
						<span class="checkout-receipt-name">Temple Team.</span>
						</font></p>
					</div>
				</div>
				<!--------------------------------------- //receipt section --------------------------------------->
				<br>
				
				<!-------------------------------------------- main btn area wrapper -------------------------------------------->
				<div class="checkout-main-btn-area-wrapper">
					<!--<button class="btn btn-main btn-left" onclick="window.location.href='receipt'">Print Receipt</button>-->
					<button class="btn btn-main btn-left" onclick="window.location.href='member/profile'">Manage Account</button>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<!-------------------------------------------- //main btn area wrapper -------------------------------------------->
			</div>
			<!-------------------------------------------- //main content left area wrapper -------------------------------------------->

			<div class="clearfix"></div>
			
			
			
			
			<!-------------------------------------------- main btn area wrapper -------------------------------------------->
			<div class="checkout-main-btn-area-wrapper mobile">
				<!--<button class="btn btn-main btn-left" onclick="window.location.href='receipt'">Print Receipt</button>-->
				<button class="btn btn-main btn-left" onclick="window.location.href='member/profile'">Manage Account</button>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
			<!-------------------------------------------- //main btn area wrapper -------------------------------------------->
		
		
		
			</div>
		
		</div>    
	</div>
</div>
<!-------------------------------------------- //main content area wrapper -------------------------------------------->
<script>
{literal}
$(document).ready(function(){
	$('#cart_total_change').html('0');
});
</script>
{/literal}
