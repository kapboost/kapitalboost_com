{if $member_id}
<div style="margin-top:250px; margin-left:15px;">
    <form action="index.php?class=Payment&method=UploadBukti" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="encrypt" value="{$encrypt}">

        <div class="form-group">
            <label> Name : </label>
            <input type="text" name="name" value="{$member_name}"class="form-control" readonly>
        </div>
        <div class="form-group">
            <label>Bank Name : </label>
            <input type="text" name="bank_name" class="form-control">
        </div>

        <div class="form-group">
            <label>Upload Image : </label>
            <input type="file" name="img" >
            {if $image != ''}
            <br />
            <div class="graph-wrapper" style="margin-left:-40px;">
                <ul class="image_wrapper">
                    <li>
                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/blog/{$image}" class="image-link" title="{$image}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/blog/{$image}&w=150" alt=""></a><br>
                    </li>
                </ul>
            </div>
            <div class="clear"></div>
            {/if}
        </div>
        <button value="submit"> Submit </button>
    </form>
</div>
{else}
<div class="row" align="center">
    <div class="col-xs-12" style="text-align: center; color: red; margin-top:200px;">
        <div class="alert alert-warning">
            <div>You don't have permission!</div>
        </div>
    </div>
</div>
{/if}