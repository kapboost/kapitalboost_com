<script type="text/javascript" src="assets/js/kp-main.js"></script>
<link rel="stylesheet" href="assets/css/new-kp-campaigns.css" />
<div class="allBanner">
       <img src="../assets/images/campaign.jpg" />
       <div class="allTitle campaignWrap">
              <div class="campaignInner" style="background-color:#e6e6e6">
                     <center>
                            <div class="campaignTitle">
                                   <p>{if $la=='id'}{else}Campaigns{/if}</p>
                            </div>
                            <form method="POST" action="https://kapitalboost.com/campaign/get_campaign">
                                   <select name="project_type" id="project_type" class="form-control" data-style="btn-default">
                                          <option value="all"> {if $la=='id'}{else}Select Campaign Type{/if} </option>
                                          <option value="sme">{if $la=='id'}{else}SME Funding{/if}</option>
                                          <option value="donation">{if $la=='id'}{else}Donation{/if}</option>
                                          <!-- <option value="private">{if $la=='id'}{else}Private Crowdfunding{/if}</option> -->

                                   </select>
                                   <button type="submit" value="search" class="filter_search">{if $la=='id'}{else}SEARCH{/if}</button>
                            </form>
                     </center>
              </div>
       </div>
</div>

<div id="container">
       <div id="campaigns" class="container-fluid">
              <!--<div id="campaigns-inner">-->
              <script>
                     var campaigns = [];
              </script>
              <div id="campaigns-list" class="row" style="padding:0 20px">
                     {section name=outer loop=$content}

                            {if $member_id}
                                   <li onclick="window.location.href = 'campaign/view/{$content[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {else}
                                   <li class="eth-logs cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4" id="eth-log" style="cursor:pointer;">
                                          <script>
                                                 {literal}
                                                        campaigns.push('{/literal}{$content[outer].campaign_id}{literal}');
                                                 {/literal}
                                          </script>
                                   {/if}
                                   <div class="campaigns-list-con" >
                                          <input type="hidden" class="total-amount" value="{$content[outer].total_funding_amt}" />
                                          <input type="hidden" class="curr-amount" value="{$content[outer].curr_funding_amt}" />

                                          <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$content[outer].campaign_background}')"></div>
                                          <div class="campaigns-list-middle">
                                                 <h4 class="campaigns-title">
                                                        {if $content[outer].acronim != ""}
                                                               {$content[outer].acronim}
                                                        {else}
                                                               {$content[outer].campaign_name}
                                                        {/if}
                                                 </h4>
                                                 <span class="campaigns-company">{if $la=='id'}{$content[outer].classification_id}{else}{$content[outer].classification}{/if}<br /><br /></span>
                                                 <div class="campaigns-desc">
                                                        <p>{if $la=='id'}{$content[outer].campaign_snippet_id}{else}{$content[outer].campaign_snippet}{/if}</p>
                                                 </div>

                                                 {if $member_id}
                                                        <p class="campaigns-read-more"><span class="campaigns-read-more-underline" href="campaign/view/{$content[outer].campaign_id}">{if $content[outer].sme_subtype == "" } &nbsp;&nbsp;&nbsp; {else}
															   {if $content[outer].sme_subtype == "INVOICE  FINANCING" }
															   <span style="color:#446CB3 !important;">{$content[outer].sme_subtype}</span>
															   {else}
															   {$content[outer].sme_subtype}{/if}{/if}</span>
                                                        </p>
                                                 {else}
                                                        <p class="campaigns-read-more"><span class="campaigns-read-more-underline" id="eth-log">{if $content[outer].sme_subtype == "" } &nbsp;&nbsp;&nbsp; {else}
															   {if $content[outer].sme_subtype == "INVOICE  FINANCING" }
															   <span style="color:#446CB3 !important;">{$content[outer].sme_subtype}</span>
															   {else}
															   {$content[outer].sme_subtype}{/if}{/if}</span>
                                                        </p>
                                                 {/if}
                                          </div> 
                                          <div class="campaigns-list-bottom">
                                                 <div class="campaigns-list-bottom-1">
                                                        <div class="campaigns-country">
                                                               <img class="campaigns-marker" src="../../assets/images/marker.png" />
                                                               <p class="campaigns-country-name">{$content[outer].country}</p>
                                                        </div>
                                                        <div class="campaigns-industry">
                                                               {if $content[outer].project_type=='donation'}
                                                                      <img class="campaigns-marker" src="../../assets/images/donate-icon.png" style="width:20px" />
                                                               {else}
                                                                      <img class="campaigns-marker" src="../../assets/images/industry-icon.png" />
                                                               {/if}
                                                               <p class="campaigns-industry-name">{$content[outer].industry}</p>
                                                        </div>
                                                        <div style="clear:fix"></div>
                                                 </div>
                                                 <div class="campaigns-bar" {if $content[outer].sme_subtype == "INVOICE  FINANCING" } style="background-color:#D2D7D3 !important;" {/if}>
                                                        {if $content[outer].project_type=='donation'}
                                                               <div class="campaigns-bar-inner campaigns-bar-inner-green"></div>
                                                        {else}
                                                               <div class="campaigns-bar-inner"  {if $content[outer].sme_subtype == "INVOICE  FINANCING" } style="background-color:#446CB3 !important;" {/if}></div>
                                                        {/if}
                                                 </div>
                                                 <ul class="campaigns-data">
                                                        <li style="background: white; {if $member_country === 'INDONESIA'}height:75px !important;{/if} ">
                                                               <p class="campaigns-data-bold">S$ {$content[outer].total_funding_amt|number_format}</p>
                                                               {if $member_country === 'INDONESIA'}
                                                                      <!-- <p class="campaigns-data-bold-id">(IDR {$content[outer].total_funding_amt_id|number_format})</p> -->
                                                               {/if}
                                                               <p>{if $la=='id'}{else}Target{/if}</p>
                                                        </li>
                                                        <li style="background: white; {if $member_country === 'INDONESIA'}height:75px !important;{/if} ">
                                                               <p class="campaigns-data-bold">S$ {$content[outer].curr_funding_amt|number_format}</p>
                                                               {if $member_country === 'INDONESIA'}
                                                                      <!-- <p class="campaigns-data-bold-id">(IDR {$content[outer].curr_funding_amt_id|number_format})</p> -->
                                                               {/if}
                                                               <p>{if $la=='id'}{else}Funded{/if}</p>
                                                        </li>
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$content[outer].project_return} %</p>
                                                               <p>{if $la=='id'}{else}Returns{/if}</p>
                                                        </li>


                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$content[outer].funding_summary}</p>
                                                               <p>{if $la=='id'}{else}Tenor{/if}</p>
                                                        </li>

                                                        {if $content[outer].days_left <= 0}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">0, Closed</p>
                                                                      <p>{if $la=='id'}{else}Days left{/if}</p>
                                                               </li>
                                                        {else}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$content[outer].days_left}</p>
                                                                      <p>{if $la=='id'}{else}Days left{/if}</p>
                                                               </li>
                                                        {/if}
                                                        <li style="background: white;">
                                                               <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                                                      {if $content[outer].risk=="C" || $content[outer].risk=="C+" || $content[outer].risk=="High"}
                                                                             <label class="label label-danger" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$content[outer].risk}{/if}</label><br />
                                                                      {elseif $content[outer].risk=="B" || $content[outer].risk=="B+" || $content[outer].risk=="B-" || $content[outer].risk=="Medium"}
                                                                             <label class="label label-warning" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$content[outer].risk}{/if}</label><br />
                                                                      {elseif $content[outer].risk=="A" || $content[outer].risk=="A-" || $content[outer].risk=="Low"}
                                                                             <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$content[outer].risk}{/if}</label><br />
                                                                      {else}
                                                                             <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">N/A</label><br />
                                                                      {/if}
                                                                      <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div> 
                                                               </div>
                                                        </li>

                                                        <!--
                                                                                                    <li style="background: white;">
                                                                                                        <p class="campaigns-data-bold">S$ {$content[outer].minimum_investment|number_format}</p>
                                                                                                        <p>Minimum</p>
                                                                                                    </li>
                                                        -->
                                                 </ul>

                                          </div>
                                   </div>
                            </li>
                     {/section}
              </div>
              <center>
                     <div class="pagination" style="margin: 0;">{$pager}</div>
              </center>
       </div>
</div>
<script>
       {literal}

              $(document).ready(function () {
                     console.log(JSON.parse(localStorage.getItem('user')));
              });
       {/literal}
</script>
