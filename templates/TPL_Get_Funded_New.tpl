<section class="new-get-funded">
	<div class="jumbotron" style="background-image: url('https://kapitalboost.com/staging/valuepenguin/images/hd-gambar.jpg');">
		<div class="overlay">
		  <div class="container">
		  	<img src="https://kapitalboost.com/staging/valuepenguin/images/kb-logo.png" class="get-funded-logo" alt="Kapital Boost Logo">
		  	<div class="row">
		  		<div class="col-md-6">
		  			<h1 class="color-white">Grow BIG Together</h1>
						<p class="lead color-white">Kapital Boost offers a peer-to-peer crowdfunding platform for SMEs to raise financing from retail investors. We use Islamic finance structures with simple approval process and at competitive rates. Apply now!</p>
		  		</div>
		  		<div class="col-md-6 col-lg-5">
		  			<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="https://kapitalboost.com/state/st3/assets/img/why-us/3.png" alt="Fast and Simple">
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Fast and Simple</h4>
						    <p>Get approved within days. Raise financing in less than a week.</p>
						  </div>
						</div>
						<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="https://kapitalboost.com/state/st3/assets/img/why-us/2.png" alt="Competitive Fund">
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Competitive Fund</h4>
						    <p>Our funding cost is favourable to most non-bank financing options.</p>
						  </div>
						</div>
						<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="assets/images/jaminan-asset.png" alt="Fast and Simple">
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Without Collateral</h4>
						    <p>Get financing without collateral, only invoice</p>
						  </div>
						</div>
		  		</div>
		  	</div>
		  </div>
		</div>
	</div>

	<section id="requirements">
		<div class="container">
			<div class="row">
				<div class="col-lg-12" align="center">
					<h2 class="section-title">Get Funded</h2>
				</div>
				<div class="col-md-6">
					<h3>Eligibility criteria:</h3>
					<ul>
						<li>Incorporated in Indonesia or Singapore</li>
						<li>Been in operations for more than 1 year</li>
						<li>Annual sales of more than IDR1bn or SGD100,000</li>
						<li>Positive free cash flow in the past 12 months</li>
					</ul>
				</div>

				<div class="col-md-6">
					<h3>Suitable for:</h3>
					<ul>
						<li>Purchasing assets (raw materials, equipments, supplies etc) based on existing purchase/work order or</li>
						<li>Financing invoices issued to established companies such as MNCs, public-listed companies, or state-owned enterprises</li>
						<li>Short-term financing of less than six months</li>
					</ul>
				</div>

				<div class="col-lg-12 contact">
					<p>Not clear on our funding requirements? Speak to us at +65 9865 9648/+62 812 8787 4136 or email hello@kapitalboost.com.</p>
				</div>
			</div>
		</div>
	</section>

	<div id="form" class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="section-title">Apply for Funding</h2>
				<br>
			</div>
			<div class="col-lg-12">
				<form id="getfundedform" style="width:100%" name="getfundedform" method="post" enctype="multipart/form-data" action="getfunded/register" onsubmit="return checkCheckBoxes(this);">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022884">Full name <span class="req">*</span></label>
								{if $member_id}
									<input type="text" maxlength="4000" name="full_name" class="form-control" value="{$member_firstname} {$member_surname}" readonly/>
									<input type="hidden" name="owner" value="{$member_name}"/>
								{else}
									<input type="text" maxlength="4000" name="full_name" class="form-control" value="" required />
								{/if}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="EmailAddress">Email address <span class="req">*</span></label>
								{if $member_id}
									<input type="email" name="email" id="email" class="form-control" maxlength="255" value="{$member_email}" readonly/>
								{else}
									<input type="email" name="email" id="email" class="form-control" maxlength="255" value="" required />
								{/if}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022885">Mobile number <span class="req">*</span></label>
								<input type="number" maxlength="4000" name="mobile" id="mobile" class="form-control" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022886">Company Name<span class="req">*</span></label>
								<input type="text" maxlength="4000" name="company" class="form-control" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022889">Year established <span class="req">*</span></label>
								<input type="number" maxlength="255" name="year" class="form-control" required/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-4">
									<div class="form-group">
										<label for="CAT_Custom_20022890">Currency <span class="req">*</span></label>
										<select name="currency" class="form-control" required>
											<option value="">-- Please select --</option>
											<option value="IDR">IDR</option>
											<option value="MYR">MYR</option>
											<option value="SGD">SGD</option>
										</select> 
									</div>
								</div>
								<div class="col-xs-8">
									<div class="form-group">
										<label for="CAT_Custom_20022891">Annual revenue <span class="req">*</span></label>
										<input type="number" maxlength="4000" name="est" class="form-control" required />
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022895">Funding period <span class="req">*</span></label>
								<select name="period" class="form-control" required>
									<option value="">-- Please select --</option>
									<option value="2 months">2 months</option>
									<option value="3 months">3 months</option>
									<option value="4 months">4 months</option>
									<option value="5 months">5 months</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022895">What financing solution are you seeking?<span class="req">*</span></label>
								<select name="fin_type" class="form-control" required>
									<option value="">-- Please select --</option>
									<option value="Invoice Financing">Invoice Financing </option>
									<option value="Asset Purchase Financing">Asset Purchase Financing</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<!-- <label for="CAT_Custom_20022895"></label> -->
							<div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
							<p><span class="req">*</span> <i>Required</i></p>
						</div>
						<div class="col-md-6">
							<input type="checkbox" name="confirm" id="CAT_Custom_20022898_0" required/> <span class="req">*</span>I confirm the information provided are correct and accurate.
							<br/>
							<input type="checkbox" name="termAndCondition" id="termAndCondition" required/> <span class="req">*</span>I have read and agreed to the <a href="https://kapitalboost.com/legal#term">Terms of Use</a> and <a href="https://kapitalboost.com/legal#privacy">Privacy Policy</a>.
						</div>
						
						<input type="hidden" name="source" value="/get-funded-new" />
						
						<div class="clear"></div>
						<div class="col-md-12" align="center">
							<button class="cat_button bluebtn" type="submit" id="getfunded-submit">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<section id="what-we-do">
		<div class="parallax-window" data-parallax="scroll" data-image-src="https://kapitalboost.com/staging/valuepenguin/images/bg-2.png"></div>
		
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/PcIBqRsKRlc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="col-md-6">
					<h3>What We Do</h3>
					<p class="color-grey-light">Kapital Boost aims to tackle the lack of financing available to SMEs in Southeast Asia. We help level the playing field by offering these businesses a crowdfunding platform to access temporary liquidity for goods and capital purchases.</p>
					<p class="color-grey-light">We also address the shortage of attractive Islamic-based investments. By funding SMEs, Kapital Boost members (funders) have the opportunity to earn attractive returns on short-term, Shariah-structured deals. In other words, our members get rewarded for doing good.  </p>
				</div>
			</div>
		</div>
	</section>

	<div id="featured-on" class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="section-title">Featured On</h2>
			</div>
			<div class="col-lg-12">
				<div class="features-on">
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/bh.png" alt="Logo Berita Harian">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/bloomberg.png" alt="Logo Bloomberg">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/crowdfund_insider.png" alt="Logo Crowdfunding">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/markets.png" alt="Logo The Edge Market">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/newbfm.png" alt="Logo BFM">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/salam.png" alt="Logo Global Islamic Economy Gateway">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/straits.png" alt="Logo The Straits">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/techinasia.png" alt="Logo Techinasia">
					</div>
				</div>
			</div>
		</div>
	</div>

	<section id="testimonial">
		<div class="container">
			<div class="row">
				<div class="col-lg-12" align="center">
					<h2 class="section-title">Testimonial</h2>
				</div>
			</div>
			<div class="row list-testinomial">
				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">When Produsen Batik needed additional funds for expansion, Kapital Boost provided an important alternative financing solution. Apart from the requirements which were straight-forward, the cost of funding is reasonable. Working with Kapital Boost also gave us comfort, as its funding is Shariah-based.</p>
						<h5>Abdul Rahman Hantiar, SME Owner</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_30.png" alt="" class="round"></span></center>
					</div>
				</div>

				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Have always been seeking halal investments for an average joe. No better avenue than Kapital Boost. Trusted, friendly and very reliable. Have experienced consistent results. Best of all, totally Shariah-focused and thus halal growth with a peace of mind.</p>
						<h5>Shaik Shahul Hameed, Investor</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_35.png" alt="" class="round"></span></center>
					</div>
				</div>

				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Financing via Kapital Boost helped to rapidly grow our business. The cost was reasonable and we like the non-interest based structure.</p>
						<h5>Niko Mustika Dewa Purnomohadjo, SME owner</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/NikoFix.jpg" alt="" class="round"></span></center>
					</div>
				</div>

				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Kapital Boost offers entrepreneurs a much needed alternative source of funding to banks. It gives businesses a relatively fast access to funds which is important for fast-growing companies. Moreover, investors are able to invest in exciting businesses and be part of their success story.</p>
						<h5>Djuan Onn Abdul Rahman, SME owner</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_25.jpg" alt="" class="round"></span></center>
					</div>
				</div>

				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Kapital Boost gives customers like me an avenue to invest with a small capital. The investment returns are attractive and the short-term tenure allows me to be nimble with my money. Surely, investing involves risks. But I believe Kapital Boost has done proper due diligence to ensure the risk is manageable.</p>
						<h5>Zulkarnain B., Investor</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_33.png" alt="" class="round"></span></center>
					</div>
				</div>

				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Kapital boost provides a Syariah compliant platform for investors who seek better returns. At the same time, it also conducts its own due dillegence to get the best deals which is sustainable & ethical. Moreover, it allows members to donate to less fortunate communities through its donation crowdfunding.</p>
						<h5>Muhammad Farid Then, Investor</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_32.png" alt="" class="round"></span></center>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container" id="compliance">
		<div class="row">
			<div class="col-lg-12" align="center">
				<h2 class="section-title">Shariah Compliance</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 item img">
				<img src="https://kapitalboost.com/assets/images/FSAC-logo.png" alt="FSAC logo">
			</div>
			<div class="col-md-7 item">
				<p>Kapital Boost's Murabaha crowdfunding structure is certified Shariah compliant by the Financial Shariah Advisory & Consultancy (FSAC), a consultancy unit of Pergas Investment Holdings (Singapore).</p>
			</div>
		</div>
	</div>

	<section id="funded-button">
		<div class="container">
			<div class="row">
				<div class="col-sm-6" align="center">
					<p class="lead">Ready to get funded, apply today!</p>
				</div>
				<div class="col-sm-6">
					<a href="/get-funded-new#form" class="btn btn-primary btn-block">Apply</a>
				</div>
			</div>
		</div>
	</section>
</section>

<script type="text/javascript">
	$("#menu").hide();

	$(document).ready(function() {
		$('#getfunded-submit' ).click(function() {
			fbq('track', 'GetFunded', {
        content_name: 'Get funded submit', 
      });
			
			// $("#getfundedform").submit();
		});

		$('.parallax-window').parallax({
			speed: 0.7
		});

		$(".list-testinomial").slick({
			dots: true,
			autoplay: true,
			buttons: true
		});

		$('.features-on').slick({
			infinite: true,
		  slidesToShow: 5,
		  slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			centerPadding: '30',
		  arrows: false,
			responsive: [
		    {
		      breakpoint: 640,
		      settings: {
		        centerMode: true,
		        centerPadding: '0',
		        slidesToShow: 3
		      }
		    },
		  ]
		});
	})
</script>