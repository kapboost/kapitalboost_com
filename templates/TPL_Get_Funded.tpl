<div class="allBanner">
       <img src="../assets/images/getfunded.jpg" />
</div>
<div id="container" >

       <center>
              <div class="partnersWrap" style="background-color:#f2f2f2">
                     <div class="partnersInner">
                            <!--
                                            <div id="default" style="background-color:#f5f5f5;font-size: 14px;">
                                                <div id="default-1">
                                                    <div id="default-1-back" >
                                                        <img src="../assets/images/getfunded.jpg" style="width: 100%;margin-top: 69px;">
                            -->
                            <div id="default-1-inner-s" style="text-align: left;/* padding-top:50px; */">

                                   <div id="get-fund-main">
                                          <div id="get-fund-form">
											<div class="w50">
                                                 <form id="getfundedform" style="width:100%" name="getfundedform" method="post" enctype="multipart/form-data" action="getfunded/register" onsubmit="return checkCheckBoxes(this);">

                                                        <div class="w100">
                                                               <label for="CAT_Custom_20022884">Full name <span class="req">*</span>
                                                               </label>
                                                               <br />
                                                               {if $member_id}
                                                               <input type="text" maxlength="4000" name="full_name" class="cat_textbox" value="{$member_firstname} {$member_surname}" readonly/>
                                                               <input type="hidden" name="owner" value="{$member_name}"/>
                                                               {else}
                                                               <input type="text" maxlength="4000" name="full_name" class="cat_textbox" value="" required />
                                                               {/if}
                                                        </div>
                                                        <div class="w100">
                                                               <label for="EmailAddress">Email address <span class="req">*</span>
                                                               </label>
                                                               <br />
                                                               {if $member_id}
                                                               <input type="email" name="email" id="email" class="cat_textbox" maxlength="255" value="{$member_email}" readonly/>
                                                               {else}
                                                               <input type="email" name="email" id="email" class="cat_textbox" maxlength="255" value="" required />
                                                               {/if}
                                                        </div>

                                                        <div class="clear"></div>

                                                        <div class="w100">
                                                               <label for="CAT_Custom_20022885">Mobile number <span class="req">*</span></label>
                                                               <br />
                                                               <input type="number" maxlength="4000" name="mobile" id="mobile" class="cat_textbox" required />
                                                        </div>
														<div class="w100">
                                                               <label for="CAT_Custom_20022886">Company Name<span class="req">*</span></label>
                                                               <br />
                                                               <input type="text" maxlength="4000" name="company" class="cat_textbox" required />
                                                        </div>

                                                        <!-- <div class="w50"> -->
                                                               <!-- <label for="CAT_Custom_20022888">Country <span class="req">*</span></label> -->
                                                               <!-- <br /> -->
                                                               <!-- <select name="country" id="country" class="cat_dropdown"> -->
                                                                      <!-- <option value="">-- Please Select --</option> -->
                                                                      <!-- <option value="SINGAPORE">SINGAPORE</option> -->
                                                                      <!-- <option value="MALAYSIA">MALAYSIA</option> -->
                                                                      <!-- <option value="INDONESIA">INDONESIA</option> -->
                                                               <!-- </select> -->
                                                        <!-- </div> -->

                                                        <div class="clear"></div>


                                                        <!-- <div class="w25"> -->
                                                               <!-- <label for="CAT_Custom_20022887">Industry <span class="req">*</span></label> -->
                                                               <!-- <br /> -->
                                                               <!-- <input type="text" maxlength="4000" name="industri" class="cat_textbox" required/> -->
                                                        <!-- </div> -->



                                                        <div class="clear"></div>

														<div class="w100">
                                                               <label for="CAT_Custom_20022889">Year established <span class="req">*</span></label>
                                                               <br />
                                                               <input type="number" maxlength="255" name="year" class="cat_textbox" required/>
                                                        </div>
                                                        <div class="w25">
                                                               <label for="CAT_Custom_20022890">Currency <span class="req">*</span></label>
                                                               <br />
                                                               <select name="currency" class="cat_dropdown" style="width: 80%;" required>
                                                                      <option value="">-- Please select --</option>
                                                                      <option value="IDR">IDR</option>
                                                                      <option value="MYR">MYR</option>
                                                                      <option value="SGD">SGD</option>
                                                               </select>
                                                        </div>


                                                        <div class="w75">
                                                               <label for="CAT_Custom_20022891">Annual revenue <span class="req">*</span></label>
                                                               <br />
                                                               <input type="number" maxlength="4000" name="est" class="cat_textbox" required />
                                                        </div>

                                                        <div class="clear"></div>

                                                        <div class="w100">
                                                               <label for="CAT_Custom_20022895">Funding Amount <span class="req">*</span></label>
                                                               <br />
                                                               <input type="number" maxlength="4000" name="fund" class="cat_textbox" required />
                                                               <br />

                                                        </div>

                                                        <div class="w100">
                                                               <label for="CAT_Custom_20022895">Funding period <span class="req">*</span></label>
                                                               <br />
                                                               <select name="period" class="cat_dropdown" required>
                                                                      <option value="">-- Please select --</option>

                                                                      <option value="2 months">2 months</option>
                                                                      <option value="3 months">3 months</option>
                                                                      <option value="4 months">4 months</option>
                                                                      <option value="5 months">5 months</option>

                                                               </select>
                                                               <br />

                                                        </div>
														<div class="w100">
                                                               <label for="CAT_Custom_20022895">What financing solution are you seeking?<span class="req">*</span></label>
                                                               <br />
                                                               <select name="fin_type" class="cat_dropdown" required>
                                                                      <option value="">-- Please select --</option>
                                                                      <option value="Invoice Financing">Invoice Financing </option>
                                                                      <option value="Asset Purchase Financing">Asset Purchase Financing</option>
                                                               </select>
                                                               <br />
                                                        </div>

                                                        <div class="w100">
                                                               <label for="CAT_Custom_20022895">How did you know us?<span class="req">*</span></label>
                                                               <br />
                                                               <select name="how_you_know" class="cat_dropdown" required>
                                                                    <option value="">-- Please select --</option>
                                                                    <option value="1">Search Engine</option>
                                                                    <option value="2">Ads</option>
                                                                    <option value="3">Social Media</option>
                                                                    <option value="4">Friend/Family</option>
                                                                    <option value="5">Community Event</option>
                                                                    <option value="6">News/Blog/Magazine</option>
                                                                    <option value="7">Other</option>
                                                               </select>
                                                               <br />
                                                        </div>

														<div class="w100">
                                                               <label for="CAT_Custom_20022895"></label>
                                                               <br />
                                                               <div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
                                                               <br />
                                                        </div>




                                                        <div class="clear"></div>
                                                        <p><span class="req">*</span> <i>Required</i></p>
                                                        <br />
                                                        <center>

                                                               <div class="gff-div" style="text-align:left">
                                                                      <input type="checkbox" name="confirm" id="CAT_Custom_20022898_0" required/> <span class="req">*</span>I confirm the information provided are correct and accurate.
                                                               </div>
                                                               <div class="gff-div" style="text-align:left">
                                                                      <input type="checkbox" name="termAndCondition" id="termAndCondition" required/> <span class="req">*</span>I have read and agreed to the <a href="https://kapitalboost.com/legal#term">Terms of Use</a> and <a href="https://kapitalboost.com/legal#privacy">Privacy Policy</a>.
                                                               </div>

                                                               <input class="cat_button bluebtn" type="submit" value="Submit" id="getfunded-submit" />
                                                        </center>

                                                 </form>
												</div>
												<div class="w50">
													<h1 style="text-align:left;">Get Funded</h1>
													<br />
													<br />
													<h3>Eligibility criteria:</h3>
													<p><ul>
														<li>Incorporated in Indonesia or Singapore</li>
														<li>Been in operations for more than 1 year</li>
														<li>Annual sales of more than IDR1bn or SGD100,000 </li>
														<li>Positive free cash flow in the past 12 months </li>
													</ul></p>
													<br />
													<h3>Suitable for:</h3>
													<p><ul>
														<li>Purchasing assets (raw materials, equipments, supplies etc) based on existing purchase/work order or </li>
														<li>Financing invoices issued to established companies such as MNCs, public-listed companies, or state-owned enterprises </li>
														<li>Short-term financing of less than six months </li>

													</ul></p>
													<br />
													<p>Not clear on our funding requirements?
													<br />Speak to us at +65 9865 9648/+62 812 8787 4136 or email hello@kapitalboost.com. </p>
												</div>
                                          </div>
                                   </div>
                            </div>
                            <!--
                                                    </div>
                                                </div>
                                            </div>
                            -->
                     </div>
              </div>
       </center>
</div>
</form>
{if $pesan_sukses}
<!-- start popup -->
<div class="contactWrap">
       <center>
              <div class="contactInner">
                     <div class="contactClose">
                            <span class="glyphicon glyphicon-remove"></span>
                     </div>
                     <p class="contactTitle">
                            <strong>Thank you!</strong>
                     </p>
                     <p class="contactText">
                            You have successfully submitted a funding request. <br />We shall contact you within the next 24 hours.
                     </p>
              </div>
       </center>
</div>
{/if}
<!-- end popup -->
{literal}
<script>
       $(document).ready(function () {
              $(window).load(function () {
                     $('.contactWrap').fadeIn(200);
                     $('.contactClose').click(function () {
                            $('.contactWrap').fadeOut(200);
                     });
              });
       });
</script>
<script>
       $(document).ready(function () {
              $('#CustomInput').change(function () {
                     if ($(this).val() == 'Yes') {
                            $('.S5Input3').slideDown();
                            $('#labelNew').slideDown();
                            $('#attachboxpo').slideDown();
                     } else {
                            $('.S5Input3').slideUp();
                            $('#labelNew').slideUp();
                            $("#attachboxpo").slideUp();
                     }
              });

              $("#getfunded-submit").click(function() {

                mixpanel.track("UKM Apply to Funding", {
                  "Full Name": $("[name='full_name']").val(),
                  "Email Address": $("[name='email']").val(),
                  "Phone Number": $("[name='mobile']").val(),
                  "Company": $("[name='company']").val()
                });
              })
       });
</script>
{/literal}
{literal}
<script type="text/javascript" language="JavaScript">
       function checkCheckBoxes(theForm) {
              if (theForm.confirm.checked == false)
              {
                     alert('Please confirm that the information provided are correct and accurate by checking the box.');
                     return false;
              } else {
                     return true;
              }
       }

</script>
{/literal}

<script type="text/javascript" src="assets/js/kp-contact.js"></script>
{literal}
<style>

       @media screen and (max-width: 550px){
              input{
                     border-color:#999999 !important;
              }
              .w50{
                     width: 100% !important;
              }

              .w25{
                     width: 50% !important;
              }

              #attachbox{
                     width:90% !important;
              }
       }
       input{
              border-color:#999999 !important;
       }
       {/literal}
</style>
