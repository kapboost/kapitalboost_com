<center>
<div class="allBanner">
     <img src="../assets/images/partners.jpg" />
     <div class="allTitle">
        <p></p>
     </div>
</div>
<div class="partnersWrap">
    <div class="partnersInner">
<!--
<div id="default" style="display: inline-block;position: relative;">
    <div id="default-1" style="display: inline-block;position: relative;">
        <div id="default-1-back" style="display: inline-block;position: relative;">
            <div id="default-1-inner" style="text-align: left;">
-->
<h2 class="allNewTitle">Partners</h2>
{section name=outer loop=$content}
                <ul id="kb-partners-list">
                    <li>
                        <a href="{$content[outer].partner_url}" target="_blank">
                            <p>
                                <img alt="" style="width:300px !important; height:180px !important; overflow: hidden;" src="../../assets/images/partner/{$content[outer].partner_image}" />
                            </p>
                        </a>
                    </li>
                    <li>
                            <h3 class="partnerTitle">{$content[outer].partner_name}</h3>
                            <p class="partnersDes">{$content[outer].partner_description}</p>
                            <p><a href="{$content[outer].partner_url}" class="visitWeb" target="_blank">Visit Web</a></p>
                    </li>
                 </ul>
                    {/section}
<!--
    </div>
            </div>
        </div>
    </div>
</div>
-->
</div>
</center>