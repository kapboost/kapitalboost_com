<style type="text/css">
	.container {
		margin-top: 80px;
		margin-bottom: 50px;
	}
	.msg {
		text-align: center;
		margin: 50px 0px 20px;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			{if $already_paid == '1'}
				<center>
					<br>
					<img src="/assets/icon/wallet.png" width="300">
					<h1 class="msg">
						Please note that you have already made payment for this campaign.
					</h1>
					<p>
						Your payment has already been recorded in our database. You may check your dashboard for more info.
					</p>
					<br>
					<a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Homepage</a>
				</center>
			{else if $message_error != ""}
				<center>
					<br>
					<img src="/assets/icon/payfailed.png" width="300">
					<h1 class="msg">
						Ops, Your Payment Failed!
					</h1>
					<p>
						Please contact us to get more information.
					</p>
					<br>
					<a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Homepage</a>
				</center>
			{else}
				<center>
					<br>
					<img src="/assets/icon/wallet.png" width="300">
					<h1 class="msg">
						Your payment is successful.
					</h1>
					<p>
						Thank you for your investment of<br>
						<b>SGD {$tinv['total_funding']} (IDR {number_format($tinv['total_funding_id'])})</b> in the <b>{$tinv['campaign']}</b> campaign
					</p>
					<br>
					<a href="/"><i class="fa fa-arrow-left fa-fw"></i> Back to Homepage</a>
				</center>
			{/if}
		</div>
	</div>
</div>