<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<center>
<div class="partnersWrap">
    <div class="partnersInnerDas">
<!--
<div id="default" style="display: inline-block;position: relative;">
    <div id="default-1" style="display: inline-block;position: relative;">
        <div id="default-1-back" style="display: inline-block;position: relative;">
            <div id="default-1-inner" style="text-align: left;width:93%;">
-->
<h1 class="h1-1">Invested Campaign History</h1>{$hancok}
<div class="dasContent">
<table id="myTable2" class="display nowrap" cellspacing="0" width="100%">
        <thead>
      <tr>
        <th width="3%">No</th>
        <th width="13%">Investor Name</th>
        <th width="10%">Campaign Name</th>
        <th width="10%">Funding Amount</th>
        <th width="15%">Date Of Payment </th>
        <th width="10%"> Payment Type</th>
        <th width="5%"> Status</th>
      </tr>
    </thead>
<tbody>

    {section name=outer loop=$invest}
      <tr class="even">
        <td>{$smarty.section.outer.index+1}</td>
        <td>{$invest[outer].nama|default:'-'}</td>
     <td>{$invest[outer].campaign|default:'-'}</td>
<td>{$invest[outer].total_funding|default:'-'}</td>
<td>{$invest[outer].date|date_format:"%d-%m-%Y %H:%M"}</td>
<td>{$invest[outer].tipe|default:'-'}</td>
{if $invest[outer].status=='Paid'}
<td><label class="label label-success"> Open </label></td>
{else}
<td><label class="label label-default"> Close </label></td>
{/if}
      </tr>
    {sectionelse}
      <tr class="even">
        <td colspan=7>
          <center><i>No Record Found ...</i></center>
        </td>
      </tr>
    {/section}
    </tbody>
  </table>
</div>
<!--
            </div>
        </div>
    </div>
</div>
-->
    </div>
</div>
</center>
<script>
{literal}
$(document).ready(function(){
    $('#myTable2').DataTable({
        "bFilter": false,
        "bSortable": false,
        "bInfo": false
});
});
{/literal}
</script>
