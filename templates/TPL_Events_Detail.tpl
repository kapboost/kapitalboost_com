<link rel="stylesheet" href="assets/css/kp-event.css" />
<div id="event">
    <div id="event-1">
        <div id="event-1-back">
            <div id="event-1-inner">
	             {section name=outer loop=$content}
                <h1 id="event-1-h1">{$content[outer].event_name}</h1>
                <div id="event-1-para-1">
	                <a href="#"><img alt="" src="../../assets/images/event/{$content[outer].event_image}" /></a>
                    <p>{$content[outer].event_description}</p>
                </div>
                {/section}
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/js/kp-event.js"></script>
