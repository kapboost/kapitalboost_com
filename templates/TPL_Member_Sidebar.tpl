{if $mobile}
<!--------------------------------------- mobile sidebar nav wrapper --------------------------------------->
<div class="mobile-sidebar-nav-wrapper">
	<nav class="navbar navbar-default mobile-sidebar-nav-container" role="navigation">
		<div class="mobile-sidebar-nav-top-bg"></div>
		<div class="mobile-sidebar-nav-content-wrapper">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed mobile-sidebar-nav-toggle" data-toggle="collapse" data-target="#mobile-sidebar-nav-droplist">
				<span class="mobile-sidebar-nav-indicator-text">MEMBER TOOLS</span>
				<span class="sr-only">Toggle navigation</span>
				<div class="mobile-sidebar-nav-btn">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</div>
				</button>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="mobile-sidebar-nav-bottom-bg"></div>
								
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="mobile-sidebar-nav-droplist">
			<ul class="nav navbar-nav mobile-sidebar-nav">
				<li><a href="member/profile">MEMBER PROFILE</a></li>
				<li><a href="address-book">ADDRESS BOOK</a></li>
				<li><a href="order-history">ORDER HISTORY</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</nav>                
</div>
<!--------------------------------------- //mobile sidebar nav wrapper --------------------------------------->
{else}
<!-------------------------------------------- member content left area wrapper -------------------------------------------->
<div class="member-content-left-area-wrapper">
	<!-------------------------------------------- sidebar nav area wrapper -------------------------------------------->
	<div class="sidebar-nav-area-wrapper">
		<div class="sidebar-nav-panel-header-wrapper">
			<div class="inner-container" style="color:#fff;">
				Hi,
				<span class="header-3">{$member_name}</span>
			</div>
		</div>
		<div class="sidebar-nav-content-wrapper">
			<ul class="sidebar-nav">
				<li class="member-nav-profile"><a href="member/profile" {if $selected=="profile"}class="active"{/if}>Member Profile</a></li>
				<li class="member-nav-address"><a href="address-book" {if $selected=="address-book"}class="active"{/if}>Address Book</a></li>
				<li class="member-nav-history"><a href="order-history" {if $selected=="order-history"}class="active"{/if}>Order History</a></li>
			</ul>
		</div>
	</div>
	<!-------------------------------------------- //sidebar nav area wrapper -------------------------------------------->
</div>
<!-------------------------------------------- //member content left area wrapper -------------------------------------------->
{/if}