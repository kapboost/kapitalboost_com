<center>
                <div id="reg-lightbox-main">
                    <div id="reg-lightbox-close">
                        <span id="reg-close" class="glyphicon glyphicon-remove"></span>
                    </div>
                    <form name="registerform" id="registerform" method="post" action="">
                        <div class="regt">Membership Registration</div>
                        <!-- <div class="regt"><div class="regr"><span style="color:red">*</span>required</div></div> -->
                        <div id="emailexist">
                            <p>
                                You already have an account in Kapital Boost. <br />
                                Please click the button below to retrieve your password.
                            </p>

                            <div class="column-full">
                                <input type="button" href="/member/forgot-password" value="Forgot Password" id="gotoforgot"/>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-row">
                            <div class="column-one marginLeftSpecial"><div class="reg-title" style="margin-bottom: 10px;"></div>
                                <select name="member_gender" id="member_gender" class="reg_dropdown" required>
                                    <option value="">Title</option>
                                    <option value="mr">Mr.</option>
                                    <option value="mrs">Mrs.</option>
                                    <option value="ms">Ms.</option>
                                </select>
                            </div>
                            <div class="column-three"><div class="reg-title"></div>
                                <input placeholder="First Name" type="text"  name="member_firstname" class="inputAlign reg_textbox" value="" required />
                            </div>
                            <div class="column-three"><div class="reg-title"></div>
                                <input placeholder="Last Name" type="text"  name="member_surname" class="inputAlign reg_textbox" value="" required/>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="column-full">
                                <div class="reg-title" style="margin-bottom: 10px;"></div>
                                <select name="member_country" class="inputAlign member_country" id="member_kota" data-live-search="true" data-style="btn-default">
                                    <option value="">Select your country</option>
                                    {html_options options=$member_country_options selected=$member_country}
                                </select>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="column-full">
                                <div class="reg-title" style="margin-top: 10px;"></div>
                                <input placeholder="Username" type="text" name="member_username" class="inputAlign reg_textbox" value="" id="regis1" required/>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="column-full">
                                <div class="reg-title"></div>
                                <input placeholder="Email Address" type="text" name="member_email" class="inputAlign reg_textbox" value="" required>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="column-half">
                                <div class="reg-title"></div>
                                <input placeholder="Password" type="password" name="member_password" class="inputAlign reg_textbox" value="" id="regis2" required/>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="column-half">
                                <div class="reg-title"></div>
                                <input placeholder="Confirm Password" type="password" name="retype_password" class="inputAlign reg_textbox" value="" id="regis3" required/>
                            </div>
                        </div>
                        <div class="form-row">
                            <center>
                                <div class="column-full" style="margin: 15px 0;">
                                    <!--<div class="g-recaptcha" data-sitekey="6LdFbQgUAAAAAMj7KgOws8B9rou7RwvChNl8P9l4"></div>-->
                                </div>
                        </div>
                        <div class="form-row">
                            <div class="column-full">
                                <input id="register-submit" type="submit" value="Submit" />
                            </div>
                        </div>
                        </center>
                    </form>
                </div>
</center>