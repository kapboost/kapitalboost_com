<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
{if $member_id}
       <center>
              <div class="partnersWrap">
                     <div class="partnersInnerDas">

                            <h1 class="h1-1">{if $la == 'id'}{else}Portfolio{/if}</h1>
                            <table id="myTable" class="display nowrap" cellspacing="0" width="100%">
                                   <thead>
                                          <tr>
                                                 <th width="3%">{if $la == 'id'}{else}No{/if}</th>
                                                 <th width="20%">{if $la == 'id'}{else}Campaign Name{/if}</th>
                                                 <th width="10%">{if $la == 'id'}{else}Funding Amount{/if}</th>
                                                 <th width="10%">{if $la == 'id'}{else}Investment Date{/if} </th>
                                                 <th width="10%">{if $la == 'id'}{else}Campaign End Date{/if}</th>
                                                 <th width="10%">{if $la == 'id'}{else}Payment Type{/if}</th>
                                                 <th width="5%">{if $la == 'id'}{else}Principal Payment{/if}</th>
                                          </tr>

                                   </thead>
                                   <tbody>
                                          {section name=outer loop=$list}
                                                 <tr class="even">

                                                        <td>{$smarty.section.outer.index+1}</td>
                                                        <td style="cursor:pointer" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'">{$list[outer].campaign|default:'-'}</td>
                                                        <td style="cursor:pointer" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'">{$list[outer].total_funding|default:'-'}</td>

                                                        <td style="cursor:pointer" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'">{$list[outer].date|date_format:"%d-%m-%Y %H:%M"}</td>

                                                        <td style="cursor:pointer" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'">{$list[outer].closing_date|date_format:"%d-%m-%Y"}</td>

                                                        <td style="cursor:pointer" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'">{$list[outer].tipe|default:'-'}</td>

                                                        {if $list[outer].status=='Paid'}
                                                               <td style="cursor:pointer" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'"><label class="label label-success">{if $la == 'id'}{else}Paid{/if}</label></td>

                                                        {else}

                                                               <td style="cursor:pointer;text-align:left" onclick="window.location.href = 'https://kapitalboost.com/dashboard/payment-detail/{$list[outer].id}'"><label class="label label-default">{if $la == 'id'}{else}Unpaid{/if}</label></td>
                                                        {/if}




                                                 </tr>
                                          {sectionelse}

                                                 <tr class="even">
                                                        <td colspan=9>
                                          <center><i>{if $la == 'id'}{else}No Record Found ...{/if}</i></center>
                                          </td>
                                          </tr>
                                   {/section}
                                   </tbody>
                            </table>
                            <!--
                                        </div>
                                    </div>
                                </div>
                            </div>
                            -->
                     </div>
              </div>
       </center>
       {literal}
              <script>
                     $(document).ready(function () {
                            $('#myTable').DataTable({
                                   "bFilter": false,
                                   "bSortable": false,
                                   "bInfo": false,
                                   "dom": '<"toolbar">frtip'
                            });
                     });
              </script>
       {/literal}
       {if $bank_msg}
              <div class="bankSuccessWrap">
                     <div class="bankSuccessInner">
                            <div class="bankSuccessClose">
                                   <a href="dashboard/payment-history"><span class="glyphicon glyphicon-remove"></span></a>
                            </div>
                            <div class="bankSuccessTitle">
                                   {if $la == 'id'}{else}Payment Message{/if}
                            </div>
                            <div class="bankSuccessText">
                                   {$bank_msg}
                            </div>
                     </div>
              </div>
       {/if}



       <script>
              {literal}
              /* start bank success */
              $(document).ready(function () {
                     //$("#lightbox").fadeIn();
                     $(window).load(function () {
                            $('.bankSuccessWrap').fadeIn(800);

                            $('.bankSuccessClose').click(function () {
                                   $('.bankSuccessWrap').fadeOut(800);
                            });
                     });
              });
              /* end bank success */
              {/literal}
       </script>






{else}

       <script>
              {literal}
                     
                     $(document).ready(function(){
                            $("#close-box").hide();
                            $("#lightbox").fadeIn(200);
                            $("#lightbox").off("click");
                     });

              {/literal}
       </script>

{/if}
