<div id="footer">
       <div id="footer-inner">
              <div id="footer-main">
                     <div id="footer-main-1">
                            <h3 class="foot-header">About</h3>
                            <p><a class="footer-body"  href="about">About</a></p>
                            <p><a class="footer-body"  href="how-it-works">How it works</a></p>
                            <p><a class="footer-body"  href="media">Media</a></p>                            
                            <p><a class="footer-body"  href="career">Career</a></p>                            
                     </div>
                     <div id="footer-main-2">
                            <h3 class="foot-header">General</h3>
                            <p><a class="footer-body"  href="campaign/">Campaigns</a></p>
                            <p><a class="footer-body"  href="blog">Blog</a></p>
                            <p><a class="footer-body"  href="partners">Partners</a></p>
                     </div>
                     <div id="footer-main-3">
                            <h3 class="foot-header">Support</h3>
                            <p><a class="footer-body"  href="get-funded">Get Funded</a></p>
                            <p><a class="footer-body"  href="faq">FAQs</a></p>
                            <p><a class="footer-body"  href="contact-us">Contact Us</a></p>
                     </div>
                     <div id="footer-main-5">
                            <h3 class="foot-header">Legal</h3>
                            <p><a class="footer-body" id="legal-term"  href="https://kapitalboost.com/legal#term">Term of use</a></p>
                            <p><a class="footer-body" id="legal-privacy"  href="https://kapitalboost.com/legal#privacy">Privacy Policy</a></p>
                            <p><a class="footer-body" id="legal-risk"  href="https://kapitalboost.com/legal#risk">Risk Statement</a></p>
                     </div>
                     <div id="footer-main-4">
                            <h3 class="foot-header foot-header-2">Follow Us</h3>

                            <p>
                                   <a class="footer-body"  class="sc-fb-icon" href="https://www.facebook.com/kapitalboost" target="_blank"><img style="width:39px;height:39px" src="assets/images/facebook1.png" /></a>
                                   <!--<a class="footer-body"  class="sc-ln-icon" href="https://www.linkedin.com/company/6597611?trk=tyah&trkInfo=clickedVertical:company,clickedEntityId:6597611,idx:1-1-1,tarId:1440685694219,tas:kapital%20boost" target="_blank"><img alt="" src="assets/images/linkedin.png" /></a>-->
                                   <a class="footer-body"  class="sc-tw-icon" href="https://twitter.com/kapitalboost" target="_blank"><img alt="" src="assets/images/twitter1.png" style="margin-left: 4px;" /></a>
                                   <a class="footer-body"  class="sc-tw-icon" href="https://www.instagram.com/kapital.boost/" target="_blank"><img alt="" src="assets/images/instagram1.png" style="margin-left: 4px;" /></a>
                            </p>
                            <div id="ssl-con">
                                   <a class="footer-body"  target="_blank" href="https://www.ssllabs.com/ssltest/analyze.html?d=kapitalboost.com" ><img src="../assets/images/ssl-lab.png" style="padding:1px;background-color:white;border-radius:5px;text-align:center;width:60%"></a>
                            </div>
                            <div class="ssl_logo_new" style="display: none;">
                                   <a class="footer-body"  href="https://play.google.com/store/apps/details?id=com.kapitalboost" target="_blank"><img alt="Android App" src="/assets/images/Android.png" width="113px" /></a>
                                   
                            </div>
                     </div> 
              </div>

              <div id="footer-copy">&copy; COPYRIGHT 2018 &bull; KAPITAL BOOST PTE LTD &bull; SINGAPORE 201525866W</div>
              <div id="footer-disclaimer">
                     <div id="disclaimer-content">
                            {if $la=='id'}
                            
                            {else}
                                   <p>This website and the contents herein do not constitute as any financial advice, investment advice or solicitation for the purposes of making financial investments in Singapore or other territories. Kapital Boost Pte. Ltd. is a firm specialising in the matching of opportunities between our registered members and small to medium-sized enterprises. With regard to these opportunities, the need and onus to do due diligence lies squarely with our members as we do not profess to advise on the same. All dealings and transactions are directly with the businesses, project owners or authorized agents we refer to our members.</p>
                            <p>Kapital Boost Pte. Ltd. is not licensed and/or registered under the Securities & Futures Act of Singapore or the Financial Advisor's Act under the Monetary Authority of Singapore and thus cannot offer, solicit, advice or recommend the buying and selling of investment securities or any other financial and banking products from the public.</p>{/if}
                     </div>
              </div>

       </div>
       {literal}
       <script type="text/javascript">
              var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
              (function () {
                     var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                     s1.async = true;
                     s1.src = 'https://embed.tawk.to/57331214c6045a5c6e8d66c1/default';
                     s1.charset = 'UTF-8';
                     s1.setAttribute('crossorigin', '*');
                     s0.parentNode.insertBefore(s1, s0);
              })();

              Tawk_API = Tawk_API || {};
              Tawk_API.visitor = {
				  name: "Name",
				  email: "Email Address"
			  };

       </script>
       {/literal}
      
       <div id="footer-end"></div>
</div>
