<center>
       <div class="thankYou" style="background-color:white">
              {if $error_msg}
                     <div class="row">
                            <div class="col-xs-12" style="text-align: center; color: red;">
                                   <div class="alert alert-danger">
                                          {$error_msg}
                                   </div>
                            </div>
                     </div>
              {/if}

              {if $bank_msg && $project_type=='donation' }
                     <div class="TYInner">
                            <p class="thanks-title">
                                   Thank you for your donation of <br> 
                                   <b>SGD {$funding} </b> in the <b>{$campaign_name}</b> campaign.
                            </p>
                            </p>
                            <p class="TYText">
                            <p>
                                   Please proceed to make payment to Kapital Boost according to the instructions below.
                            </p>

                            <p>
                            <table style="border: 1px solid #cccccc; ">
                                   <tr>
                                          <th style =" padding:10px;">PAYMENT INSTRUCTIONS</th>
                                   </tr>
                                   <tr>
                                          <td style =" padding:10px;">Please transfer funds to the following account:</td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">Account name: <b>Kapital Boost Pte Ltd</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">Bank name:<b> Overseas Chinese Banking Corp. (OCBC)</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">Account number: <b>6232 3373 1001</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">SWIFT code: <b>OCBCSGSG</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">SWIFT Bank code: <b>7339</b></td>
                                   </tr>

                            </table>
                            <br>
                            <p>
                                   If you have questions, please email <a href="mailto:support@kapitalboost.com">support@kapitalboost.com</a>.
                            </p>
                            </p>
                            <hr >
                            <p class="TYText2">
                                   Help support the project by letting others know
                            </p>
                            <ul class="TYLink">
                                   <li>
                                          <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/{$e_campaign_id}">
                                                 <img src="assets/images/linkGoogle.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/{$e_campaign_id}">
                                                 <img src="assets/images/linkFacebook.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/{$e_campaign_id}" target="_blank">
                                                 <img src="assets/images/linkTwitter.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/{$e_campaign_id}">
                                                 <img src="assets/images/linkLinkedIn.jpg" />
                                          </a>
                                   </li>
                            </ul>
                     </div>
              {elseif $bank_msg && $project_type!='donation' }
                     <div class="TYInner">
                            <p class="TYTitle">
                            <p class="thanks-title">
                                   Thank you for your commitment of <br> 
                                   <b>SGD {$funding} </b> in the <b>{$campaign_name}</b> campaign.
                            </p>
                            </p>
                            <p class="TYText">
                            <p>
                                   Please proceed to make payment to Kapital Boost according to the instructions below.
                            </p>

                            <p>
                            <table style="border: 1px solid #cccccc; ">
                                   <tr>
                                          <th style =" padding:10px;">PAYMENT INSTRUCTIONS</th>
                                   </tr>
                                   <tr>
                                          <td style =" padding:10px;">Please transfer funds to the following account:</td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">Account name: <b>Kapital Boost Pte Ltd</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">Bank name:<b> Overseas Chinese Banking Corp. (OCBC)</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">Account number: <b>6232 3373 1001</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">SWIFT code: <b>OCBCSGSG</b></td>
                                   </tr>
                                   <tr>
                                          <td  style =" padding:10px;">SWIFT Bank code: <b>7339</b></td>
                                   </tr>

                            </table>
                            <br>
                            <p>
                                   If you have questions, please email <a href="mailto:support@kapitalboost.com">support@kapitalboost.com</a>.
                            </p>
                            </p>

                            <hr >
                            <p class="TYText2">
                                   Help support the project by letting others know
                            </p>
                            <ul class="TYLink">
                                   <li>
                                          <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/{$e_campaign_id}">
                                                 <img src="assets/images/linkGoogle.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/{$e_campaign_id}">
                                                 <img src="assets/images/linkFacebook.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/{$e_campaign_id}" target="_blank">
                                                 <img src="assets/images/linkTwitter.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/{$e_campaign_id}">
                                                 <img src="assets/images/linkLinkedIn.jpg" />
                                          </a>
                                   </li>
                            </ul>
                     </div>

              {/if}

              {if $paypal_msg}
                     <div class="TYInner">
                            <p class="TYTitle">
                            <p class="thanks-title">
                                   Thank you for your generous {if $campaign_subtype != ''}funding{else}donation{/if} of <br>
                                   <b>SGD {$item_price} </b> for the <b>{$campaign_name}</b> campaign.
                            </p>

                            <p>
                                   May your kindness be rewarded in multiplefold.
                            </p>
                            </p>
                            <p class="TYText">
                            <p><br><br>
                                   Best regards, <br><br>
                                   Kapital Boost
                            </p>
                            </p>
                            <br />
                            <hr >
                            <p class="TYText2">
                                          Spread goodness and let your friends and family know by sharing this project
                                  
                            </p>
                            <ul class="TYLink">
                                   <li>
                                          <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/{$paypal_campaign_id}">
                                                 <img src="assets/images/linkGoogle.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/{$paypal_campaign_id}">
                                                 <img src="assets/images/linkFacebook.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/{$paypal_campaign_id}" target="_blank">
                                                 <img src="assets/images/linkTwitter.jpg" />
                                          </a>
                                   </li>
                                   <li>
                                          <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/{$paypal_campaign_id}">
                                                 <img src="assets/images/linkLinkedIn.jpg" />
                                          </a>
                                   </li>
                            </ul>
                     </div>
              {/if}
       </div>
</center>
<!-- end thank you page -->



<script>
       {literal}
              /* start bank success */
              $(document).ready(function () {
                     $(window).load(function () {
                            $('.bankSuccessWrap').fadeIn(800);

                            $('.bankSuccessClose').click(function () {
                                   $('.bankSuccessWrap').fadeOut(800);
                            });
                     });
              });
              /* end bank success */
       {/literal}
</script>
