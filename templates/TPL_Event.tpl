<link rel="stylesheet" href="assets/css/kp-event.css" />
<div class="allBanner">
     <img src="../assets/images/events.jpg" />
     <div class="allTitle">
        <p></p>
     </div>
</div>
<div id="event">
    <div id="event-1">
        <div id="event-1-back">
            <div id="event-1-inner">
<h2 class="allNewTitle">Event</h2>
                <div id="event-1-para-2"></div>
                <div id="event-list">
	                {section name=outer loop=$content}
	                {if $content[outer].image}
                    <div class="events-each">
                        <div class="events-img">
                            <a href="event/{$content[outer].slug}"><img alt="" src="../assets/images/event/{$content[outer].image}" /></a>
                        </div>
                        <div class="events-info">
                            <h3><a href="event/{$content[outer].slug}">{$content[outer].event_title}</a></h3>
                            <p>{$content[outer].event_content|strip_tags|strip|truncate:250}</p>
                            <p><a href="event/{$content[outer].slug}">Read More</a></p>
                        </div>
                    </div>
{else}
<div class="events-each-sm">
                        <div class="events-img-sm">
                            <a href="event/{$content[outer].slug}"></a>
                        </div>
                        <div class="events-info-sm">
                            <h3><a href="event/{$content[outer].slug}">{$content[outer].event_title}</a></h3>
                            <p>{$content[outer].event_content|strip_tags|strip|truncate:250}</p>
                            <p><a href="event/{$content[outer].slug}">Read More</a></p>
                        </div>
                    </div>
{/if}
                    {/section}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="assets/js/kp-main.js"></script>
<script type="text/javascript" src="assets/js/kp-event.js"></script>


<!--<link rel="stylesheet" href="assets/css/kp-event.css" />
 <div id="event-1">
    <div id="event-1-back">
        <div id="event-1-inner">
            <h1 id="event-1-h1">Kapital Boost Event</h1>
            <p class="description"></p>
            <div class="wrapper clear">
                <div class="post-list">
                    <div id="cateventoutput">
                        {section name=outer loop=$content}
                        <div class="event-post">
                            <h2 class="post-title"> <a href="event/{$content[outer].event_id}">{$content[outer].event_title}</a> </h2>
                            <div class="post-details">{$content[outer].release_date|date_format:"%A, %B %e, %Y"} </div>
                            <div class="post-body">
                                {$content[outer].event_content}
                            </div>
                        </div>
                        {/section}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/js/kp-event.js"></script>-->