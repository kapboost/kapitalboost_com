<div id="container">
                <div id="about">

<div class="allBanner">
     <img src="../assets/images/aboutus.jpg" />
     <div class="allTitle">
        <p>About Us</p>
     </div>
</div>			
                    <div id="about-1" style="min-height: none;">
                        <div id="about-1-back">
                            <div id="about-1-inner">
                                <!--strong>
                                    <h3><span style="font-size: 22px; color: #1f497d;">What we offer</span></h3>
                                </strong>
                                <p>
									<span style="font-size: 18px; color: #3f3f3f;">
                                    Kapital Boost helps small businesses in Asia grow BIG. Small businesses are essential to the community but may be disadvantaged from lack of access to funds. Our Singapore-based hybrid Crowdfunding platform levels the playing field by providing liquidity for goods and capital purchases for these businesses. In return, these deals give our Crowdfunding Community potentially high, short-term returns. Our Crowdfunding Investments are Ethical and follow Shariah principles to protect socio-economic welfare.
                                    </span>
                                </p>
                                <strong>
                                    <h3><span style="font-size: 22px; color: #1f497d;">For small businesses</span></h3>
                                </strong>
                                <p>
									<span style="font-size: 18px; color: #3f3f3f;">
                                    Kapital Boost provides working capital facility via an asset purchase-financing structure (also known as Murabaha). We engage in Crowdfunding to buy inventories and capital goods for small businesses, based on the value of pending purchase orders. These businesses then pay for the goods on a deferred basis with a profit markup to our Crowdfunding Community. The payment term is matched to that of incoming purchase order payments. Together with our members, Kapital Boost help small businesses grow by shortening their cash conversion cycles. We offer competitive rates coupled with a fast and friendly approval process versus other non-bank financing.</span>
                                </p>
                                <strong>
                                    <h3><span style="font-size: 22px; color: #1f497d;">For investors (members)</span></h3>
                                </strong>
                                <p>
									<span style="font-size: 18px; color: #3f3f3f;"><span style="font-size: large; color: #3f3f3f;">Kapital Boost provides ethical Crowdfunding investments with a focus on financing the asset purchase needs of small businesses.&nbsp;</span>We
                                    offer our members opportunities to invest and earn high returns with a quick turnaround of 90 to 270 days. We focus on risk
                                    reduction for investors and employ a robust Due Diligence &amp; Screening Process - analysing operating and credit history,
                                    past cash flow, corporate governance, counterparty risk, and assess social media mileage - to determine the best funding opportunities
                                    for our members.&nbsp;</span>
                                </p>
                                <p>
                                </p>
                                <p>
									<span style="font-size: 18px;"><span style="font-size: 18px; color: #3f3f3f;">For more information on our services, please see</span>
                                    <span style="font-size: 18px; color: #1f497d;"> </span><span style="font-size: 18px; color: #0070c0;"><em>'</em><a href="howitworks.html"><span style="font-size: 18px; color: #0070c0;"><em>How</em></span>                            <span style="font-style: italic; font-size: 18px; color: #0070c0;">it</span> <span style="font-style: italic; font-size: 18px; color: #0070c0;">works</span>
                                    </a><em>'</em>
                                    </span><span style="font-size: 18px; color: #0070c0;"> </span><span style="font-size: 18px; color: #3f3f3f;">and</span> <em><span style="font-size: 18px; color: #0070c0;">'</span><a href="faq.html"><span style="font-size: 18px; color: #0070c0;">FAQs</span></a><span style="font-size: 18px; color: #0070c0;">'.</span></em>
                                    </span>
                                </p-->
                            </div>
                        </div>
                    </div>
                    <div id="about-2">
                        <div id="about-2-inner">
                            <h1 id="about-2-h1">The Team</h1>
                            <!--div>
                                <p style="text-align: center;"><span style="color: #3f3f3f;">Experienced, knowledgeable, resourceful. We have a diverse team with backgrounds in Entrepreneurship, Islamic Finance, and Investment Banking. &nbsp;We work together to focus on one goal - helping small businesses thrive. &nbsp;</span>
                                </p>
                                <br />
                            </div>
                            <div id="about-2-team">
                                <div class="about-2-team-each">
                                    <div class="about-2-team-pic">
                                        <div class="about-2-team-pic-inner" style="background-image: url(http://clubethis2.businesscatalyst.com/img/witoyo-200x200.png);"></div>
                                    </div>
                                    <div class="about-2-team-desc">
                                        <h2 class="about-2-team-name">Ronald Wijaya</h2>
                                        <p class="about-2-team-desc-1"><strong>Partner, Country head - Indonesia</strong></p>
                                        <p class="about-2-team-desc-3">
                                            <a href="https://id.linkedin.com/in/ronaldwijaya" target="_blank"><img alt="" class="link-icon" src="images/linked-in-blue.png" /> </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="about-2-team-each">
                                    <div class="about-2-team-pic">
                                        <div class="about-2-team-pic-inner" style="background-image: url(http://clubethis2.businesscatalyst.com/img/erly-kba.png);"></div>
                                    </div>
                                    <div class="about-2-team-desc">
                                        <h2 class="about-2-team-name">Erly Witoyo</h2>
                                        <p class="about-2-team-desc-1"><strong><span style="font-size: 18px; color: #1f497d;">Managing Partner,</span></strong>
                                        </p>
                                        <p class="about-2-team-desc-1"><strong><span style="font-size: 18px; color: #1f497d;">Risk Management</span></strong>
                                        </p>
                                        <p class="about-2-team-desc-3">
                                            <a href="https://www.linkedin.com/in/erlywitoyo" target="_blank"><img alt="" class="link-icon" src="images/linked-in-blue.png" /> </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="about-2-team-each">
                                    <div class="about-2-team-pic">
                                        <div class="about-2-team-pic-inner" style="background-image: url(http://clubethis2.businesscatalyst.com/img/umar-200x200.png);"></div>
                                    </div>
                                    <div class="about-2-team-desc">
                                        <h2 class="about-2-team-name">Umar Munshi</h2>
                                        <p class="about-2-team-desc-1"><strong><span style="font-size: 18px; color: #1f497d;">Partner, Strategic</span></strong>
                                        </p>
                                        <p class="about-2-team-desc-1"><strong><span style="font-size: 18px; color: #1f497d;">Development</span></strong> </p>
                                        <p class="about-2-team-desc-3">
                                            <a href="https://www.linkedin.com/in/umarmunshi" target="_blank"><img alt="" class="link-icon" src="images/linked-in-blue.png" /> </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="about-2-team">
                                <div class="about-2-team-each">
                                    <div class="about-2-team-pic">
                                        <div class="about-2-team-pic-inner" style="background-image: url(http://clubethis2.businesscatalyst.com/img/zaki-kb.png);"></div>
                                    </div>
                                    <div class="about-2-team-desc">
                                        <h2 class="about-2-team-name">Zaki Masturi</h2>
                                        <p class="about-2-team-desc-1"><strong><span style="font-size: 18px; color: #1f497d;">Marketing</span></strong>
                                        </p>
                                        <p class="about-2-team-desc-3">
                                            <a href="https://sg.linkedin.com/pub/zaki-masturi/b8/9b4/30" target="_blank"><img alt="" class="link-icon" src="images/linked-in-blue.png" /> </a>
                                        </p>
                                    </div>
                                </div>
                                <div class="about-2-team-each">
                                    <div class="about-2-team-pic">
                                        <div class="about-2-team-pic-inner" style="background-image: url(http://clubethis2.businesscatalyst.com/img/nur-fairuz.png);"></div>
                                    </div>
                                    <div class="about-2-team-desc">
                                        <h2 class="about-2-team-name">Siti Nur Fairuz Khalil</h2>
                                        <p class="about-2-team-desc-1"><strong>Intern</strong></p>
                                        <p class="about-2-team-desc-3">
                                            <a href="https://sg.linkedin.com/in/siti-nur-fairuz-khalil-568b58a4" target="_blank"><img alt="" class="link-icon" src="images/linked-in-blue.png" /> </a>
                                        </p>
                                    </div>
                                </div>
                            </div-->
                        </div>
                    </div>
                </div>
            </div>