<!-------------------------------------------- main content area wrapper -------------------------------------------->
<div class="subpage-main-content-area-wrapper">
       <div class="subpage-content-wrapper subpage-content-wrapper-2">

              <!-------------------------------------------- subpage breadcrumb area wrapper -------------------------------------------->
              <div class="breadcrumb-wrapper-2">
                     <div class="breadcrumb-wrapper-inner-2">
                            <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   {$breadcrumb}
                            </ol>
                     </div>
              </div>
              <!-------------------------------------------- //subpage breadcrumb area wrapper -------------------------------------------->

              <!-------------------------------------------- subpage mobile breadcrumb area wrapper -------------------------------------------->
              <div class="breadcrumb-mobile-wrapper">
                     <div class="breadcrumb-wrapper-inner">
                            <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   {$breadcrumb}
                            </ol>
                     </div>
              </div>
              <!-------------------------------------------- //subpage mobile breadcrumb area wrapper -------------------------------------------->
              <div class="clearfix"></div>
              {if $error_msg}
              <div class="row">
                     <div class="col-xs-12">
                            <div class="alert alert-danger">
                                   {section name=b loop=$error_msg}
                                   <div>{$error_msg[b]}</div>
                                   {/section}
                            </div>
                     </div>
                     <Div class="clear"></Div>
              </div>
              {/if}

              {if $success_msg}
              <div class="row">
                     <div class="col-xs-12">
                            <div class="alert alert-success">
                                   <div>{$success_msg}</div>
                            </div>
                     </div>
                     <Div class="clear"></Div>
              </div>
              {/if}


              <!-------------------------------------------- member register area wrapper -------------------------------------------->
              <div class="member-register-area-wrapper">
                     <form method="post" id="register_form" name="register_form" action="member/register">
                            <input type="hidden" name="identifier" value="{if $identifier}{$identifier}{else}{$register_identifier}{/if}" />
                            <div class="inner-container">
                                   <p class="header-1">Member Registration </p>
                                   <p>Sign up now for free and enjoy special benefit exclusively for our members.</p>
                                   <br>


                                   <!-------------------------------------------- member register login detail wrapper -------------------------------------------->
                                   <div class="panel-wrapper">
                                          <div class="panel-header-wrapper top-panel-header">
                                                 <div class="panel-inner"><font color="white">Login Detail</font></div>
                                          </div>
                                          <br>
                                          <div class="col-md-6">
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Email Address*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="email" class="form-control form-field" name="member_email" value="{$member_email}" id="member_email" placeholder="Email Address" style="margin-bottom:8px;" required>
                                                               <span>(to be used as user ID for login)</span>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                          </div>
                                          <div class="col-md-6">
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Password*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="password" class="form-control form-field" name="member_password" value="{$member_password}" id="member_password" placeholder="Password" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Confirm Password*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="password" class="form-control form-field" name="retype_password" id="retype_password" value="{$retype_password}" placeholder="Confirm Password" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                          </div>
                                          <div class="clearfix"></div>
                                   </div>
                                   <!-------------------------------------------- //member register login detail wrapper -------------------------------------------->
                                   <br>

                                   <!-------------------------------------------- member register personal detail wrapper -------------------------------------------->
                                   <div class="panel-wrapper">
                                          <div class="panel-header-wrapper top-panel-header">
                                                 <div class="panel-inner"><font color="white">Personal Data</font></div>
                                          </div>
                                          <br>
                                          <div class="col-md-6">
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Full Name*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_givenname" id="member_givenname"  value="{if $member_givenname}{$member_givenname}{else}{$register_givenname}{/if}" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <!--<div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Birth Date*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_birthday" id="member_birthday"  value="{if $member_birthday}{$member_birthday}{else}{$register_birthday}{/if}" readonly="readonly" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>-->
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Address*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <textarea name="member_street_1" id="member_street_1" cols="" rows="" class="form-control form-field member-register-address-field" required>{if $member_street_1}{$member_street_1}{else}{$register_street}{/if}</textarea>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>

                                          </div>
                                          <div class="col-md-6">
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">City*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_city" id="member_city" value="{if $member_city}{$member_city}{else}{$register_city}{/if}" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">State*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_state" id="member_state"  value="{if $member_state}{$member_state}{else}{$register_state}{/if}" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Country*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <select name="member_country" class="form-control form-field">
                                                                      <option value=""  >--Select Country--</option>
                                                                      {section name=a loop=$country}
                                                                      <option value="{$country[a].name}"   {if $member_country==$country[a].name}selected="selected"{elseif $country[a].name == "INDONESIA"}selected="selected"{/if}>{$country[a].name}</option>
                                                                      {/section}
                                                               </select>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Postal Code*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_postal_code" id="member_postal_code" value="{if $member_postal_code}{$member_postal_code}{else}{$register_postal_code}{/if}" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Contact No*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_contact_no" id="member_contact_no" value="{if $member_contact_no}{$member_contact_no}{else}{$register_contact_no}{/if}" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                                 <div class="form-group">
                                                        <label for="" class="control-label member-register-label-wrapper">Phone No*</label>
                                                        <div class="member-register-field-wrapper">
                                                               <input type="text" class="form-control form-field" name="member_mobile_no" id="member_mobile_no" value="{if $member_mobile_no}{$member_mobile_no}{else}{$register_mobile_no}{/if}" required>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                 </div>
                                          </div>
                                          <div class="clearfix"></div>
                                   </div>
                                   <!-------------------------------------------- //member register personal detail wrapper -------------------------------------------->
                                   <br>


                                   <div class="checkbox">
                                          <label>
                                                 <input type="checkbox" name="promotion" value="1">
                                                 Yes, I would like to subcribe to free newsletter from Temple Project
                                          </label>
                                   </div>      
                                   <br><br>                          


                                   <button class="btn btn-main" type="submit">Submit</button>

                            </div>
                     </form>
              </div>
              <!-------------------------------------------- //member register area wrapper -------------------------------------------->

       </div>
       <!--div class="main-footer-area-wrapper-top"></div-->
</div>
<!-------------------------------------------- //main content area wrapper -------------------------------------------->