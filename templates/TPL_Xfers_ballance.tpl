<link rel="stylesheet" type="text/css" href="/xfers/assets/css/gaya.css">

<div class="partnersWrap">
       <div class="partnersInnerDas" style="width: 100%; margin: 0px">
              <center>
                     <!-- start xfers balance -->
                     <div class="xfersBalance" ng-app="xfers" ng-controller="xfersCtrl">
                            <div class="XBInner">
                                   <div class="XBLogo">
                                          <img src="/assets/images/xfersLogo.png" />
                                   </div>
                                   <div class="XBTitle">
                                          <p>
                                                 e-Wallet Account Balance
                                          </p>
                                   </div>
                                   <div class="XBYourBalance">
                                          <div>
                                                 <span>You have</span>
                                                 {if $country === 'SINGAPORE'}
                                                        <p>SGD {$ballance}</p>
                                                 {else}
                                                        <p>IDR {$ballance}</p>
                                                 {/if}
                                          </div>
                                          <div style="display: none;">
                                                 <span class="glyphicon glyphicon-refresh" ng-click="userBallance()"></span>
                                                 <p>
                                                        Refresh
                                                 </p>
                                          </div>
                                   </div>
                                   <div class="XBButton">
                                          <div>
                                                 <form>
                                                        <button ng-click="xfersCheckout()">
                                                               {if $country === 'SINGAPORE'}
                                                                      Pay Now - SGD {$amount}
                                                               {else}
                                                                      Pay Now - IDR {$amount}
                                                               {/if}
                                                        </button>
                                                 </form>
                                          </div>
                                          <div>
                                                 <form action="/dashboard/payment/xfers-transferinfo?mid={$mid}" method="POST">
                                                        <button style="margin-top:20px; color:#FFFFFF;background-color:#999999" type="submit">
                                                               Deposit
                                                        </button>
                                                 </form>
                                          </div>
                                          <div>
                                                 <br>
                                          </div>
                                          {$status}
                                   </div>
                            </div>
                     </div>
                     <!-- end x fers balance -->
              </center>
       </div>
</div>
{literal}

       <script src="/xfers/assets/js/angular.min.js"></script>
       <script type="text/javascript">

              var app = angular.module("xfers", []);
              app.controller("xfersCtrl", function ($scope, $http) {
                     $scope.xballance = {};
                     $scope.xcheckout = {};
                     $scope.params = {};

                     var chargeparams = function () {
                            return $scope.params = JSON.parse(localStorage.getItem('kapitaluser'));
                     };
                     chargeparams();
                     var getPhone = function () {
                            return $scope.cphone = JSON.parse(localStorage.getItem('user'));
                     };
                     var getCampaign = function () {
                            return $scope.cCampaign = JSON.parse(localStorage.getItem('campaign'));
                     };
                     var getCountry = function () {
                            return $scope.cCountry = JSON.parse(localStorage.getItem('country'));
                     };

                     $scope.userBallance = function () {
                            getPhone();
                            var country = getCountry().country;
                            if (country == 'SINGAPORE') {
                                   var url = "/xfers/apis/ballance.php";
                            } else {
                                   var url = "/xfers_indo/apis/ballance.php";
                            }

                            $http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "getuserballance": $scope.cphone.phone
                                   }
                            }).success(function (res) {
                                   $scope.xballance = res;
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });
                     };

                     var isuserBallance = function () {
                            getPhone();
                            var country = getCountry().country;
                            if (country === 'SINGAPORE') {
                                   var url = "/xfers/apis/ballance.php";
                            } else {
                                   var url = "/xfers_indo/apis/ballance.php";
                            }

                            $http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "getuserballance": $scope.cphone.phone
                                   }
                            }).success(function (res) {
                                   $scope.xballance = res;
                                   $scope.country = country;
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });
                     };

                     isuserBallance();

                     $scope.xfersCheckout = function () {
                            var country = getCountry().country;
                            if (country == 'SINGAPORE') {
                                   var url = "/xfers/apis/charge.php";
                            } else {
                                   var url = "/xfers_indo/apis/charge.php";
                            }
                            chargeparams();
                            getPhone();
                            getCampaign();

                            var paramslha = {
                                   "amount": $scope.params.amount,
                                   "campaign_type": $scope.params.campaign_type,
                                   "currency": $scope.params.currency,
                                   "redirect": "false",
                                   "notify_url": "https://kapitalboost.com",
                                   "return_url": "https://kapitalboost.com",
                                   "cancel_url": "https://kapitalboost.com",
                                   "order_id": $scope.params.order_id,
                                   "description": $scope.params.description,
                                   "debit_only": true,
                                   "refundable": false,
                                   "phone": $scope.cphone.phone,
                                   "shipping": "0",
                                   "tax": "0",
                                   "member_email": $scope.params.member_email,
                                   "a_name": $scope.params.member_name,
                                   "token": $scope.params.token,
                                   "items": [
                                          {
                                                 "description": $scope.params.description,
                                                 "price": $scope.params.amount,
                                                 "quantity": 1,
                                                 "name": $scope.params.items[0].name
                                          }
                                   ],
                                   "meta_data": {
                                          "firstname": $scope.params.firstname,
                                          "lastname": $scope.params.lastname
                                   }
                            };

							<!-- console.log(paramslha); -->
                            $http({
                                   method: 'POST',
                                   url: url,
                                   data: paramslha
                            }).success(function (res) {
                                   localStorage.setItem('status', JSON.stringify(res));
                                   if (res.error == false) {
                                          window.location.href = "/dashboard/payment/xfers-thankyou?campaign=" + $scope.params.description + "&campaignid=" + $scope.cCampaign.campaign_id + "&amount=" + $scope.params.amount + "&type=" + $scope.cCampaign.campaign_type + "&country=" + country;
                                   } else {
                                          if (res == "Transaction Failed!Error : INSUFFICIENT_FUND") {
                                                 $scope.status = "Your Xfers Wallet has insufficient funds for payment to Kapital Boost. Please proceed to deposit funds into your e-Wallet.";
                                          } else {
                                                 $scope.status = res;
                                          }
                                   }
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });

                     };
                     // Xfers User Ballance
              });
       </script>

{/literal}