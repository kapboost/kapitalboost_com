{if !$member_id && $content.project_type != 'donation' && $content.campaign_id != 75}
       <script>
              {literal}

                     $(document).ready(function () {
                            $("#close-box").hide();
                            $("#lightbox").fadeIn(200);
                            $("#lightbox").off("click");
                     });

              {/literal}
       </script>
{/if}

{if $member_id || $error_msg=="permit" || $content.project_type=='donation' || $content.campaign_id == 75}

       <link rel="stylesheet" href="assets/css/new-kp-campaigns.css" />

       <input type="hidden" class="ind-total-amount" value="{$content.total_funding_amt}" />
       <input type="hidden" class="ind-curr-amount" value="{$content.curr_funding_amt}" />
       <div id="campaigns-detail">
              <div id="campaigns-detail-1" style="background-image:url('../assets/images/campaign/{$content.campaign_background}')">
                     <div style="background-color: rgba(255,255,255,0.6);">
                            <div id="campaigns-detail-1-inner">
                                   <div id="campaigns-detail-1-inner-left">
                                          {if $content.campaign_logo}  
                                                 <img id="campaign-logo" src="../assets/images/campaign/{$content.campaign_logo}" />
                                          {/if}
                                   </div>
                                   <div id="campaigns-detail-1-inner-right">
                                          <div class="campaigns-detail-1-para">
                                                 <h1 class="campaigns-detail-h1">{$content.campaign_name}
                                                 </h1>
                                          </div>
                                          <p class="campaigns-company">{if $la=='id'}{$content[outer].classification_id}{else}{$content.classification}{/if}</p>
                                          <div class="campaigns-detail-1-para detail-1-para-middle">
                                                 <div class="cam-country">
                                                        <img class="cam-marker" src="../assets/images/marker.png">
                                                        <p class="cam-country-name">{$content.country}</p>
                                                 </div>
                                                 <div class="cam-industry">
                                                        {if $content.project_type=='donation'}
                                                               <img class="cam-marker" src="../assets/images/donate-icon.png" style="width:20px" />
                                                        {else}
                                                               <img class="cam-marker" src="../assets/images/industry-icon.png">
                                                        {/if}
                                                        <p class="cam-industry-name">{$content.industry}</p>
                                                 </div>
                                          </div>
                                   </div>
                                   <div class="invest-now-btn-div">
                                          {if $content.days_left <= 0}
                                                 <a  class="silverbtn invest-btn invest-now-btn" style="font-weight: none; color:#224f77">{if $la=='id'}{else}Closed{/if}</a>
                                          {else if $content.project_type=='private' && $closedPw != 'correct'}
                                                 <a  class="silverbtn invest-btn invest-now-btn" style="font-weight: none; color:#224f77">{if $la=='id'}{else}Disallowed{/if}</a>
                                          {else}
                                                 {if $content.project_type=='donation'}
                                                        {if $member_id}
                                                               <a data-toggle="modal" id="secondInvest" class="greenbtn invest-btn  invest-now-btn investOpen" style="font-weight: none;">{if $content.campaign_subtype=='hybrid'}{if $la=='id'}{else}Fund{/if}{else}{if $la=='id'}{else}Donate{/if}{/if} now</a>
                                                        {else}
                                                               <a data-toggle="modal" id="eth-log-2" class="greenbtn invest-btn  invest-now-btn" style="font-weight: none;">{if $content.campaign_subtype=='hybrid'}{if $la=='id'}{else}Fund{/if}{else}{if $la=='id'}{else}Donate{/if}{/if} now</a>
                                                        {/if}
                                                 {else}
                                                        <a data-toggle="modal" class="bluebtn invest-btn investOpen invest-now-btn" style="font-weight: none;">{if $la=='id'}{else}invest now{/if}</a>
                                                 {/if}

                                          {/if}
                                   </div>
                            </div>
                     </div>
              </div>

              <div id="campaigns-detail-2">
                     <div id="campaigns-detail-2-inner">
                            <div id="campaigns-detail-2-inner-left" class="leftRight textCenter">
                                   {if ($content.days_left <= 0 && $closedPw != 'correct' && $content.project_type != 'donation') || ($content.project_type=='private' && $closedPw != 'correct')}
                                          {if $la=='id'}{else}{$content.campaign_description|truncate}{/if}<br><br><br>
                                   {else}
                                          {if $la=='id' && $content.campaign_description_id != ''}{$content.campaign_description_id}{else}{$content.campaign_description}{/if}<br><br><br>
                                   {/if}
                            </div>
                            <div id="campaigns-detail-2-inner-right">
                                   <div class="stats-box">
                                          <div id="stats-box">
                                                 <h2 class="campaigns-detail-h2">{if $la=='id'}{else}Overview{/if} </h2>
                                                 <ul id="stats-list">
                                                        <li>
                                                               <div class="campaigns-bar">
                                                                      {if $content.project_type == 'donation'}
                                                                             <div class="campaigns-bar-inner-green campaigns-bar-inner "></div>
                                                                      {else}
                                                                             <div class="campaigns-bar-inner "></div>
                                                                      {/if}
                                                               </div>
                                                        </li>
                                                        <li>
                                                               <p class="stats-title">{if $la=='id'}{else}Target funding{/if}</p>
                                                               <p class="stats-num total-amount"><span style="float:right">S$ {$content.total_funding_amt|number_format}</span>{if $current_location === 'INDONESIA'}<br><span style="font-size:12px"> (IDR {$content.total_funding_amt_id|number_format})</span>{/if}
                                                               </p>
                                                        </li>
                                                        <li>
                                                               <p class="stats-title">{if $la=='id'}{else}Funded{/if}</p>
                                                               <p class="stats-num"><span style="float:right">S$ {$content.curr_funding_amt|number_format}</span>{if $current_location === 'INDONESIA'}<br><span style="font-size:12px"> (IDR {$content.curr_funding_amt_id|number_format})</span>{/if}
                                                               </p>
                                                        </li>
                                                        <li>
                                                               <p class="stats-title">{if $la=='id'}{else}Min investment{/if}</p> 
                                                               <p class="stats-num curr-amount">S$ {if $current_location === 'INDONESIA' && $content.minimum_investment_id != 0} {$content.minimum_investment_id|number_format} {else}{$content.minimum_investment|number_format} {/if}
                                                               </p>
                                                        </li>
                                                        <li>
                                                               <p class="stats-title">{if $la=='id'}{else}Returns{/if}</p>
                                                               <p class="stats-num">{$content.project_return}%</p>
                                                        </li>
                                                        {if $content.days_left <= 0}
                                                               <li>
                                                                      <p class="stats-title">{if $la=='id'}{else}Days left{/if}</p>
                                                                      <p class="stats-num">{if $la=='id'}{else}0, Closed{/if}</p>
                                                               </li>                                                               
                                                        {else}
                                                               <li>
                                                                      <p class="stats-title">{if $la=='id'}{else}Days left{/if}</p>
                                                                      <p class="stats-num">{$content.days_left}</p>
                                                               </li>
                                                        {/if}

                                                        <li>
                                                               <p class="stats-title">{if $la=='id'}{else}Tenor{/if}</p>
                                                               <p class="stats-num">{$content.funding_summary}</p>
                                                        </li>
                                                 </ul>
                                          </div>
                                          <center>
                                                 {if $content.days_left <= 0}
                                                        <a class="silverbtn invest-btn" style="font-weight: none;color:#224f77">{if $la=='id'}{else}closed{/if}</a>
                                          </div>
                                   {else if $content.project_type=='private' && $closedPw != 'correct'}
                                          <a class="silverbtn invest-btn" style="font-weight: none;color:#224f77">{if $la=='id'}{else}Disallowed{/if}</a>
                                   </div>
                            {else}
                                   {if $content.project_type=='donation'}

                                   </div>
                                   <center>
                                          {if $member_id}
                                                 <a id="secondInvest" data-toggle="modal" class="greenbtn invest-btn investOpen" style="width:200px;padding-left:0;padding-right:0">{if $content.campaign_subtype=='hybrid'}{if $la=='id'}{else}Fund{/if}{else}{if $la=='id'}{else}donate{/if}{/if} {if $la=='id'}{else}now{/if}</a>
                                          {else}
                                                 <a id="eth-log-2" data-toggle="modal" class="greenbtn invest-btn " style="width:200px;padding-left:0;padding-right:0;">{if $content.campaign_subtype=='hybrid'}{if $la=='id'}{else}Fund{/if}{else}{if $la=='id'}{else}donate{/if}{/if} {if $la=='id'}{else}now{/if}</a>
                                          {/if}

                                          <a data-toggle="modal" class="greenbtn invest-btn enquireOpen" style="font-weight: none;margin-top: -15px;padding-right: 0;padding-left: 0;width:200px;margin-left: 4px">{if $la=='id'}{else}Enquire{/if}</a>

                                   {else}
                                          <a id="secondInvest" data-toggle="modal" class="bluebtn invest-btn investOpen" style="width:200px;padding-left:0;padding-right:0">{if $la=='id'}{else}invest now{/if}</a>

                                          <a data-toggle="modal" class="bluebtn invest-btn enquireOpen" style="margin-top: -15px;padding-right: 0;padding-left: 0;width:200px;margin-left:4px">{if $la=='id'}{else}Enquire{/if}</a>

                                   {/if}
                                   <br/>
                            {/if}
                     </center>
                     <div id="">
                            <div id="gallery-box">
                                   <h2 class="campaigns-detail-h2">{if $la=='id'}{else}Gallery{/if}</h2>
                                   {if ($content.days_left <= 0 && $closedPw != 'correct'  && $content.project_type != 'donation') || ($content.project_type == 'private' && $closedPw != 'correct')}
                                   {else}
                                          <ul id="gallery-list">
                                                 {if $content.image_1!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_1}')" data-img-url="assets/images/campaign/{$content.image_1}" data-img-name="{$image_name[0]}"  id="481"></li>
                                                        {/if}
                                                        {if $content.image_2!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_2}')" data-img-url="assets/images/campaign/{$content.image_2}" data-img-name="{$image_name[1]}" id="482"></li>
                                                        {/if}
                                                        {if $content.image_3!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_3}')" data-img-url="assets/images/campaign/{$content.image_3}" data-img-name="{$image_name[2]}" id="483"></li>
                                                        {/if}
                                                        {if $content.image_4!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_4}')" data-img-url="assets/images/campaign/{$content.image_4}" data-img-name="{$image_name[3]}" id="484"></li>
                                                        {/if}
                                                        {if $content.image_5!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_5}')" data-img-url="assets/images/campaign/{$content.image_5}" data-img-name="{$image_name[4]}" id="485"></li>
                                                        {/if}
                                                        {if $content.image_6!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_6}')" data-img-url="assets/images/campaign/{$content.image_6}" data-img-name="{$image_name[5]}" id="486"></li>
                                                        {/if}
                                                        {if $content.image_7!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_7}')" data-img-url="assets/images/campaign/{$content.image_7}" data-img-name="{$image_name[6]}" id="487"></li>
                                                        {/if}
                                                        {if $content.image_8!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_8}')" data-img-url="assets/images/campaign/{$content.image_8}" data-img-name="{$image_name[7]}" id="488"></li>
                                                        {/if}
                                                        {if $content.image_9!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_9}')" data-img-url="assets/images/campaign/{$content.image_9}" data-img-name="{$image_name[8]}" id="489"></li>
                                                        {/if}
                                                        {if $content.image_10!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_10}')" data-img-url="assets/images/campaign/{$content.image_10}" data-img-name="{$image_name[9]}" id="490"></li>
                                                        {/if}
                                                        {if $content.image_11!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_11}')" data-img-url="assets/images/campaign/{$content.image_11}" data-img-name="{$image_name[10]}" id="491"></li>
                                                        {/if}
                                                        {if $content.image_12!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_12}')" data-img-url="assets/images/campaign/{$content.image_12}" data-img-name="{$image_name[11]}" id="492"></li>
                                                        {/if}
                                                        {if $content.image_13!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_13}')" data-img-url="assets/images/campaign/{$content.image_13}" data-img-name="{$image_name[12]}" id="493"></li>
                                                        {/if}
                                                        {if $content.image_14!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_14}')" data-img-url="assets/images/campaign/{$content.image_14}" data-img-name="{$image_name[13]}" id="494"></li>
                                                        {/if}
                                                        {if $content.image_15!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_15}')" data-img-url="assets/images/campaign/{$content.image_15}" data-img-name="{$image_name[14]}" id="495"></li>
                                                        {/if}
                                                        {if $content.image_16!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_16}')" data-img-url="assets/images/campaign/{$content.image_16}" data-img-name="{$image_name[15]}" id="496"></li>
                                                        {/if}
                                                        {if $content.image_17!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_17}')" data-img-url="assets/images/campaign/{$content.image_17}" data-img-name="{$image_name[16]}" id="497"></li>
                                                        {/if}
                                                        {if $content.image_18!=""}
                                                        <li style="background-image:url('../assets/images/campaign/{$content.image_18}')" data-img-url="assets/images/campaign/{$content.image_18}" data-img-name="{$image_name[17]}" id="498"></li>
                                                        {/if}
                                          </ul>
                                   {/if}
                                   <div class="galSticky"></div>
                            </div>
                     </div>

                     <div>
                            {if $pdfsArray[0] || $pdfsArray[1] || $pdfsArray[2] || $pdfsArray[3] || $pdfsArray[4] || $pdfsArray[5] || $pdfsArray[6] || $pdfsArray[7]}
                                   <h2 class="campaigns-detail-h2">{if $la=='id'}{else}Documents{/if}</h2>

                            {/if}
                            <ul>
                                   {if ($content.days_left <= 0 && $closedPw != 'correct' && $content.project_type != 'donation') || ($content.project_type == 'private' && $closedPw != 'correct')}
                                   {else}
                                          {section name=number loop=$pdfsArray}
                                                 {if $pdfsArray[number]}
                                                        <input type="hidden" value="{$pdfsDesArray[number]}" />
                                                        <input type="hidden" value="{$pdfsArray[number]}"/>
                                                        <li style="float:left"><a href="https://kapitalboost.com/assets/images/campaign-pdf/{$pdfsArray[number]}" target="_blank">{$pdfsDesArray[number]}</a></li>
                                                        <br>
                                                 {/if}
                                          {/section}
                                   {/if}
                                   <br/>
                                   <br/>
                            </ul>
                     </div>
              </div>
       </div>
</div>
<div id="campaigns-detail-3">
       <div id="campaigns-detail-3-inner">
              <h2 class="con-h2" id="OPSticky">{if $la=='id'}{else}Other Projects{/if}</h2>

              <ul id="campaigns-list">
                     {section name=outer loop=$other1}
                            {if $member_id}
                                   <li onclick="window.location.href = 'campaign/view/{$other1[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {else}
                                   <li id="eth-log" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {/if}
                                   <div class="campaigns-list-con">
                                          <input type="hidden" class="total-amount" value="{$other1[outer].total_funding_amt}" />
                                          <input type="hidden" class="curr-amount" value="{$other1[outer].curr_funding_amt}" />

                                          <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$other1[outer].campaign_background}')"></div>
                                          <div class="campaigns-list-middle">
                                                 <h4 class="campaigns-title">{$other1[outer].campaign_name}</h4>
                                                 <span class="campaigns-company">{if $la=='id'}{$other1[outer].classification_id}{else}{$other1[outer].classification}{/if}</span>
                                                 <div class="campaigns-desc">
                                                        {if $la=='id'}{$other1[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$other1[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}
                                                 </div>

                                                 {if $member_id}
                                                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$other1[outer].campaign_id}">{if $la=='id'}{else}read more{/if}</a>
                                                        </p>
                                                 {else}
                                                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">{if $la=='id'}{else}read more{/if}</a>
                                                        </p>
                                                 {/if}
                                          </div>
                                          <div class="campaigns-list-bottom">
                                                 <div class="campaigns-list-bottom-1">
                                                        <div class="campaigns-country">
                                                               <img class="campaigns-marker" src="../../assets/images/marker.png" /> 
                                                               <p class="campaigns-country-name">{$other1[outer].country}</p>
                                                        </div>
                                                        <div class="campaigns-industry">
                                                               <img class="campaigns-marker" src="../../assets/images/industry-icon.png" /> 
                                                               <p class="campaigns-industry-name">{$other1[outer].industry}</p>
                                                        </div>
                                                        <div style="clear:fix"></div>
                                                 </div>
                                                 <div class="campaigns-bar">
                                                        <div class="campaigns-bar-inner"></div>
                                                 </div>
                                                 <ul class="campaigns-data">
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$other1[outer].total_funding_amt|number_format}</p>
                                                               <p>{if $la=='id'}{else}Target{/if}</p>
                                                        </li>                                                                                
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$other1[outer].curr_funding_amt|number_format}</p>
                                                               <p>{if $la=='id'}{else}Funded{/if}</p>
                                                        </li>
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$other1[outer].project_return} %</p>
                                                               <p>{if $la=='id'}{else}Return{/if}</p>
                                                        </li>

                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$other1[outer].funding_summary}</p>
                                                               <p>{if $la=='id'}{else}Tenor{/if}</p>
                                                        </li>

                                                        {if $other[outer].days_left <= 0}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{if $la=='id'}{else}0, Closed{/if}</p>
                                                                      <p>{if $la=='id'}{else}Days Left{/if}</p>
                                                               </li>
                                                        {else}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$other1[outer].days_left}</p>
                                                                      <p>{if $la=='id'}{else}Days Left{/if}</p>
                                                               </li>
                                                        {/if}
                                                        <li style="background: white;">
                                                               <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                                                      {if $other1[outer].risk=="High"}
                                                                             <label class="label label-danger">{if $la=='id'}{else}High{/if}</label><br />
                                                                      {elseif $other1[outer].risk=="Medium"}
                                                                             <label class="label label-warning">{if $la=='id'}{else}Medium{/if}</label><br />
                                                                      {else}
                                                                             <label class="label label-success">{if $la=='id'}{else}Low{/if}</label><br />
                                                                      {/if}
                                                                      <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                                                               </div>
                                                        </li>
                                                 </ul>
                                          </div>
                                   </div>
                            </li>
                     {/section}
                     {section name=outer loop=$other2}
                            {if $member_id}
                                   <li onclick="window.location.href = 'campaign/view/{$other2[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {else}
                                   <li id="eth-log" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {/if}
                                   <div class="campaigns-list-con">
                                          <input type="hidden" class="total-amount" value="{$other2[outer].total_funding_amt}" />
                                          <input type="hidden" class="curr-amount" value="{$other2[outer].curr_funding_amt}" />

                                          <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$other2[outer].campaign_background}')"></div>
                                          <div class="campaigns-list-middle">
                                                 <h4 class="campaigns-title">{$other2[outer].campaign_name}</h4>
                                                 <span class="campaigns-company">{if $la=='id'}{$other2[outer].classification_id}{else}{$other2[outer].classification}{/if}</span>
                                                 <div class="campaigns-desc">
                                                        {if $la=='id'}{$other2[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$other2[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}
                                                 </div>

                                                 {if $member_id}
                                                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$other2[outer].campaign_id}">{if $la=='id'}{else}read more{/if}</a>
                                                        </p>
                                                 {else}
                                                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">{if $la=='id'}{else}read more{/if}</a>
                                                        </p>
                                                 {/if}
                                          </div>
                                          <div class="campaigns-list-bottom">
                                                 <div class="campaigns-list-bottom-1">
                                                        <div class="campaigns-country">
                                                               <img class="campaigns-marker" src="../../assets/images/marker.png" /> 
                                                               <p class="campaigns-country-name">{$other2[outer].country}</p>
                                                        </div>
                                                        <div class="campaigns-industry">
                                                               <img class="campaigns-marker" src="../../assets/images/industry-icon.png" /> 
                                                               <p class="campaigns-industry-name">{$other2[outer].industry}</p>
                                                        </div>
                                                        <div style="clear:fix"></div>
                                                 </div>
                                                 <div class="campaigns-bar">
                                                        <div class="campaigns-bar-inner"></div>
                                                 </div>
                                                 <ul class="campaigns-data">
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$other2[outer].total_funding_amt|number_format}</p>
                                                               <p>{if $la=='id'}{else}Target{/if}</p>
                                                        </li>                                                                                
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$other2[outer].curr_funding_amt|number_format}</p>
                                                               <p>{if $la=='id'}{else}Funded{/if}</p>
                                                        </li>
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$other2[outer].project_return} %</p>
                                                               <p>{if $la=='id'}{else}Return{/if}</p>
                                                        </li>

                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$other2[outer].funding_summary}</p>
                                                               <p>{if $la=='id'}{else}Tenor{/if}</p>
                                                        </li>

                                                        {if $other[outer].days_left <= 0}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{if $la=='id'}{else}0, Closed{/if}</p>
                                                                      <p>{if $la=='id'}{else}Days Left{/if}</p>
                                                               </li>
                                                        {else}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$other2[outer].days_left}</p>
                                                                      <p>{if $la=='id'}{else}Days Left{/if}</p>
                                                               </li>
                                                        {/if}
                                                        <li style="background: white;">
                                                               <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                                                      {if $other2[outer].risk=="High"}
                                                                             <label class="label label-danger">{if $la=='id'}{else}High{/if}</label><br />
                                                                      {elseif $other2[outer].risk=="Medium"}
                                                                             <label class="label label-warning">{if $la=='id'}{else}Medium{/if}</label><br />
                                                                      {else}
                                                                             <label class="label label-success">{if $la=='id'}{else}Low{/if}</label><br />
                                                                      {/if}
                                                                      <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                                                               </div>
                                                        </li>
                                                 </ul>
                                          </div>
                                   </div>
                            </li>
                     {/section}
                     {section name=outer loop=$other3}
                            {if $member_id}
                                   <li onclick="window.location.href = 'campaign/view/{$other3[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {else}
                                   <li id="eth-log" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                   {/if}
                                   <div class="campaigns-list-con">
                                          <input type="hidden" class="total-amount" value="{$other3[outer].total_funding_amt}" />
                                          <input type="hidden" class="curr-amount" value="{$other3[outer].curr_funding_amt}" />

                                          <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$other3[outer].campaign_background}')"></div>
                                          <div class="campaigns-list-middle">
                                                 <h4 class="campaigns-title">{$other3[outer].campaign_name}</h4>
                                                 <span class="campaigns-company">{if $la=='id'}{$other3[outer].classification_id}{else}{$other3[outer].classification}{/if}</span>
                                                 <div class="campaigns-desc">
                                                        {if $la=='id'}{$other3[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$other3[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}
                                                 </div>

                                                 {if $member_id}
                                                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$other3[outer].campaign_id}">{if $la=='id'}{else}read more{/if}</a>
                                                        </p>
                                                 {else}
                                                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">{if $la=='id'}{else}read more{/if}</a>
                                                        </p>
                                                 {/if}
                                          </div>
                                          <div class="campaigns-list-bottom">
                                                 <div class="campaigns-list-bottom-1">
                                                        <div class="campaigns-country">
                                                               <img class="campaigns-marker" src="../../assets/images/marker.png" /> 
                                                               <p class="campaigns-country-name">{$other3[outer].country}</p>
                                                        </div>
                                                        <div class="campaigns-industry">
                                                               <img class="campaigns-marker" src="../../assets/images/industry-icon.png" /> 
                                                               <p class="campaigns-industry-name">{$other3[outer].industry}</p>
                                                        </div>
                                                        <div style="clear:fix"></div>
                                                 </div>
                                                 <div class="campaigns-bar">
                                                        <div class="campaigns-bar-inner"></div>
                                                 </div>
                                                 <ul class="campaigns-data">
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$other3[outer].total_funding_amt|number_format}</p>
                                                               <p>{if $la=='id'}{else}Target{/if}</p>
                                                        </li>                                                                                
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">S$ {$other3[outer].curr_funding_amt|number_format}</p>
                                                               <p>{if $la=='id'}{else}Funded{/if}</p>
                                                        </li>
                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$other3[outer].project_return} %</p>
                                                               <p>{if $la=='id'}{else}Return{/if}</p>
                                                        </li>

                                                        <li style="background: white;">
                                                               <p class="campaigns-data-bold">{$other3[outer].funding_summary}</p>
                                                               <p>{if $la=='id'}{else}Tenor{/if}</p>
                                                        </li>

                                                        {if $other[outer].days_left <= 0}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{if $la=='id'}{else}0, Closed{/if}</p>
                                                                      <p>{if $la=='id'}{else}Days Left{/if}</p>
                                                               </li>
                                                        {else}
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$other3[outer].days_left}</p>
                                                                      <p>{if $la=='id'}{else}Days Left{/if}</p>
                                                               </li>
                                                        {/if}
                                                        <li style="background: white;">
                                                               <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                                                      {if $other3[outer].risk=="High"}
                                                                             <label class="label label-danger">{if $la=='id'}{else}High{/if}</label><br />
                                                                      {elseif $other3[outer].risk=="Medium"}
                                                                             <label class="label label-warning">{if $la=='id'}{else}Medium{/if}</label><br />
                                                                      {else}
                                                                             <label class="label label-success">{if $la=='id'}{else}Low{/if}</label><br />
                                                                      {/if}
                                                                      <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                                                               </div>
                                                        </li>
                                                 </ul>
                                          </div>
                                   </div>
                            </li>
                     {/section}
              </ul>

       </div>
</div>





<!-- start popup -->
<div class="investWrap">
       <center>
              <div class="investInner">
                     <div class="investClose">
                            <span class="glyphicon glyphicon-remove"></span>
                     </div>
                     <div class="investBody" >
                            
								   
										<h3 style="color:#1eb2f6;text-align:center;">INTENT OF INVESTMENT </h3>
										<br /><br />
										<p style="text-align:justify;">Thank you for your interest to invest in the Jalilah’s Satay Solo crowdfunding deal. To proceed, please send an email to <a href="mailto:hello@kapitalboost.com">hello@kapitalboost.com</a> with the following, so we can send you the investment (Wakalah) agreement for e-signing.</p>
										<br />
										<p style="text-align:justify;">
										<ol>
										<li style="text-align:justify;">Amount you would like to invest in the Jalilah’s Satay crowdfunding deal</li>
										<li style="text-align:justify;">Copy of your IC or passport.</li>
										</ol></p> 
										<br />
										<p style="text-align:justify;">If you have any questions, please don’t hesitate to contact <a href="mailto:hello@kapitalboost.com">hello@kapitalboost.com</a> or <a href="mailto:shariff.raffi@gmail.com">shariff.raffi@gmail.com.</a></p>
										<br /><br />
										<p style="text-align:justify;">Best regards,</p>
										<br />
										<p style="text-align:justify;"><strong>Jalilah's Satay</strong></p>
								   
								   
                                   <br ><br>
                          

                     </div>
              </div>
       </center>
</div>
<!-- end popup -->

{if ($content.days_left <= 0 && $closedPw != 'correct' && $content.project_type != 'donation') || ($content.project_type=='private' && $closedPw != 'correct')}

       <div id="closed">
              <div id="login-box">
                     <div id="login-top">
                            <a href="/campaign/" >X</a>
                     </div>
                     <form target="" method="POST">
                            <input id="redirect-eth-log-privates" type="hidden" name="redirect" value=""/>
                            <div id="login-middle">
                                   <img id="login-logo" alt="" src="assets/images/kapitalboost-logo.png" />
                                   <h1 id="login-h1" class="hidden">{if $la=='id'}{else}Private Password{/if}</h1>
                                   <h2 class="login-private">
                                          {if $content.project_type=='private'}
                                                 {if $la=='id'}{else}A password is required to view this page. This can be obtained directly from the business owner or upon request to the Kapital Boost team (please email to <a href="mailto:hello@kapitalboost.com" target="_blank">hello@kapitalboost.com</a>).{/if}
                                          {else}
                                                 {if $la=='id'}{else}This campaign is closed for investment. If you are an investor and want to access the campaign writeup, please request for the password from <a href="mailto:hello@kapitalboost.com">hello@kapitalboost.com</a>{/if}
                                          {/if}
                                   </h2>

                                   <input type="password" id="close-pass" name="closedPw"  style="width:100%"/>
                                   {if $closedPw == 'wrong'}
                                          <h4 style='color:red;text-align:center;margin-top:20px'>{if $la=='id'}{else}The password you've entered is incorrect{/if}</h4>
                                   {/if}

                            </div>
                            <div id="login-bottom">
                                   <div id="log-bottom-1">
                                          <input type="submit"id="close-submit"  />

                                   </div>
                            </div>

                     </form>
              </div>

       </form>
</div>
</div>


{/if}

<!-- start enquire -->
<div class="enquireWrap">
       <div class="enquireInner">
              <div class="enquireClose">
                     <span class="glyphicon glyphicon-remove"></span>
              </div>
              <div class="enquireTitle">
                     {if $la=='id'}{else}Enquire{/if}
              </div>
              <div class="enquireBody">
                     <!--<form method="POST" action="index.php?class=Campaign&method=GetContent&campaign_id={$content.campaign_id}">-->
                     <input type="text" id="eName"  value="{$member_name}" readonly />
                     <input type="text" id="eEmail"  value="{$member_email}" readonly />
                     <input type="text" id="eProject" value="{$content.campaign_name}" readonly />
                     <input type="hidden" id="eToken" value="{$token}" />
                     <textarea id="eMsg" placeholder="{if $la=='id'}{else}Type your enquiry and click Submit. We will respond within 1 working day.{/if}" required></textarea>
                     <button class="btn btn-primary" id="enquiryBtn" type="button">{if $la=='id'}{else}Submit{/if}</button>
                     <!--</form>-->
              </div>
       </div>
</div>
<!-- end enquire -->

<!-- start enquire -->

<div class="enquireWrapMsg">
       <div class="enquireInnerMsg">
              <div class="enquireCloseMsg">
                     <span class="glyphicon glyphicon-remove"></span>
              </div>
              <div class="enquireTitleMsg">
                     {if $la=='id'}{else}Enquire{/if}
              </div>
              <div class="enquireTextMsg">
                     {if $la=='id'}{else}Thank you for your inquiry!<br>
                     Our team is looking into it and will reply to you shortly.<br>
                     If your enquiry is urgent, you may call us at +65 9865 9648 from Monday to Friday during our office hours.{/if}
              </div>
       </div>
</div>

<!-- end enquire -->


<div id="real-lightbox">
       <span class="glyphicon glyphicon-chevron-right galleryControl1" id="galleryControl1"></span>
       <span class="glyphicon glyphicon-chevron-left galleryControl2" id="galleryControl2"></span>
       <div id="real-lightbox-inner">
              <div id="real-lightbox-inner-top">
                     <div id="real-close-box">
                            <span class="glyphicon glyphicon-remove"></span>
                     </div>
                     <div id="real-lightbox-inner-Title">

                     </div>
              </div>
              <div id="real-lightbox-inner-middle">
              </div>
       </div>
</div>


{elseif $error_msg=='wrongpw'}

       <div class="row" align="center">
              <div class="col-xs-12" style="text-align: center; color: red; margin-top:200px;">
                     <div class="alert alert-warning">
                            <div>{if $la=='id'}{else}The password you have entered is incorrect. The password can be obtained directly from the business owner or upon request to the Kapital Boost team {/if}
                            </div>
                            <div>{if $la=='id'}{else}(please email to hello@kapitalboost.com).{/if}
                            </div>
                     </div>
              </div>
       </div>
{/if}

<script>

       {if $content.project_type == "private" && $pass != $content.private_password}
              {literal}
                     $(function () {
                            $('#private-lightbox').fadeIn();
                     });
              {/literal}
       {/if}

       {literal}
              $(function () {

                     var $ind_curr_amt = $('.ind-curr-amount').val();
                     var $ind_total_amt = $('.ind-total-amount').val();
                     var $ind_bar_width = ($ind_curr_amt / $ind_total_amt) * 100;
                     console.log($ind_curr_amt);
                     console.log($ind_total_amt);
                     if ($ind_bar_width > 100)
                            $ind_bar_width = 100;
                     $('.campaigns-bar-inner').css({width: $ind_bar_width + '%'});


              });


       {/literal}
</script>
<script>
       {literal}

              $(document).ready(function () {


                     /* start invest now */
                     $('.investOpen').click(function () {
                            $('.investWrap').fadeIn(300);
                     });

                     $('.investClose').click(function () {
                            $('.investWrap').fadeOut(300);
                     });
                     /* end invest now */

                     /* start invest now */
                     $('.enquireOpen').click(function () {
                            $('.enquireWrap').fadeIn(300);
                     });

                     $('.enquireClose').click(function () {
                            $('.enquireWrap').fadeOut(300);
                     });
                     /* end invest now */

                     $("#enquiryBtn").on('click', function () {
                            $('.enquireWrap').fadeOut(100);
                            var name = $("#eName").val();
                            var email = $("#eEmail").val();
                            var project = $("#eProject").val();
                            var token = $("#eToken").val();
                            var msg = $("#eMsg").val();

                            $.post("index.php?class=Campaign&method=Enquiry", {name: name, email: email, project: project, msg: msg, token: token});

                            $('.enquireWrapMsg').fadeIn(400);

                     });

                     $(document).on('click', '#gallery-list li', function () {
                            var $img_url = $(this).attr('data-img-url');
                            var $title = $(this).attr('title');
                            var $Id = $(this).attr('id');
                            var $cmname = $(this).attr('data-img-name');
                            var galleryTotal = $('#gallery-list li').length;
                            var galleryCount = 480 * 1 + galleryTotal - 1;
                            if ($Id <= 481) {
                                   $('#galleryControl2').hide();
                            }
                            if ($Id => galleryCount) {
                                   $('#galleryControl1').hide();
                            }
                            if ($Id <= galleryCount) {
                                   $('#galleryControl1').show();
                            }
                            if ($Id > 481) {
                                   $('#galleryControl2').show();
                            }
                            $('#real-lightbox-inner-Title').html('<p><strong> ' + $cmname + '  </strong></p>');
                            $('#real-lightbox-inner-middle').html('<img src=" ' + $img_url + ' " id="' + $Id + '" width="100%"/>');
                            $('#real-lightbox').fadeIn();
                     });




                     $('#galleryControl1').click(function () {
                            var test1 = $('#real-lightbox-inner-middle img').attr('id');
                            var test2 = test1 * 1 + 1;
                            var targetImg = $('#' + test2 + '').attr('data-img-url');
                            var $cmname = $('#' + test2 + '').attr('data-img-name');
                            var galleryTotal1 = $('#gallery-list li').length;
                            var galleryCount1 = 480 * 1 + galleryTotal1 - 1;
                            if (test2 <= 481) {
                                   $('#galleryControl2').hide();
                            }
                            if (test2 => galleryCount1) {
                                   $('#galleryControl1').hide();
                            }
                            if (test2 <= galleryCount1) {
                                   $('#galleryControl1').show();
                            }
                            if (test2 > 481) {
                                   $('#galleryControl2').show();
                            }
                            $('#real-lightbox-inner-Title').html('<p><strong> ' + $cmname + '  </strong></p>');
                            $('#real-lightbox-inner-middle').html('<img src="' + targetImg + '" id="' + test2 + '" width="100%"/>');
                     });
                     $('#galleryControl2').click(function () {
                            var test12 = $('#real-lightbox-inner-middle img').attr('id');
                            var test22 = test12 * 1 - 1;
                            var targetImg2 = $('#' + test22 + '').attr('data-img-url');
                            var $cmname = $('#' + test22 + '').attr('data-img-name');
                            var galleryTotal2 = $('#gallery-list li').length;
                            var galleryCount2 = 480 * 1 + galleryTotal2 - 1;
                            if (test22 <= 481) {
                                   $('#galleryControl2').hide();
                            }
                            if (test22 => galleryCount2) {
                                   $('#galleryControl1').hide();
                            }
                            if (test22 <= galleryCount2) {
                                   $('#galleryControl1').show();
                            }
                            if (test22 > 481) {
                                   $('#galleryControl2').show();
                            }
                            $('#real-lightbox-inner-Title').html('<p><strong> ' + $cmname + '  </strong></p>');
                            $('#real-lightbox-inner-middle').html('<img src="' + targetImg2 + '" id="' + test22 + '" width="100%"/>');
                     });
              });
              /* start validasi */
              $(document).ready(function () {
                     $('.formTest').submit(function (event) {
                            var minimInvest = $('#child').val();
                            var minimTarget = $('input[name="minimInvest"]').val();
                            var paymentMethod = $("#paymentID").val();
                            if (paymentMethod === "") {
                                   alert("Oops, please select the Payment Method");
                                   event.preventDefault();
                            } else {
                                   if ((minimInvest * 1) < (minimTarget * 1)) {
                                          alert('Oops, please input amount higher than minimum investment');
                                          event.preventDefault();
                                   }

                            }
                     });

                     /* start enquire msg */
                     $('.enquireCloseMsg').click(function () {
                            $('.enquireWrapMsg').fadeOut(800);
                     });
                     /* end enquire msg */

              });
              /* end validasi */



              function validAngka(a)
              {
                     var exRate = '{/literal}{$exRate}{literal}';
                     console.log(exRate);
                     if (!/^[0-9.]+$/.test(a.value)) {
                            a.value = a.value.substring(0, a.value.length - 1000);
                     } else {
                            var result = a.value * exRate;
                            result = result.formatMoney(0);
                            document.getElementById("idrAmount").innerHTML = result;
                            document.getElementById("amountIdr").value = result;
                            document.getElementById("amountIdrValue").value = Math.round(a.value * exRate);
                     }
                     if (a.value == "") {
                            document.getElementById("idrAmount").innerHTML = "0";
                            document.getElementById("amountIdr").value = "0";
                            document.getElementById("amountIdrValue").value = "0";
                     }
              }

              Number.prototype.formatMoney = function (c, d, t) {
                     var n = this,
                             c = isNaN(c = Math.abs(c)) ? 2 : c,
                             d = d == undefined ? "." : d,
                             t = t == undefined ? "," : t,
                             s = n < 0 ? "-" : "",
                             i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
                             j = (j = i.length) > 3 ? j % 3 : 0;
                     return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
              };

              function onlyNumbers(evt) {
                     // Mendapatkan key code 
                     var charCode = (evt.which) ? evt.which : event.keyCode;
                     // Validasi hanya tombol angka
                     if (charCode > 31 && (charCode < 48 || charCode > 57))
                            return false;
                     return true;
              }
       {/literal}
</script>


