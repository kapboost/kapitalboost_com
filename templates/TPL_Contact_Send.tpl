<div id="default">
    <div id="default-1">
        <div id="default-1-back">
            <div id="default-1-inner" style="text-align: left;">
                
                <div class="clearfix"></div>
                {if $error}
                <div class="row">
                    <div class="col-xs-12" style="text-align: center; color: red;">
                        <div class="alert alert-danger">
                            {$error}
                        </div>
                    </div>
                </div>
                {/if}

                {if $sukses}
                <div class="row">
                    <div class="col-xs-12" style="text-align: center; color: green;">
                        <div class="alert alert-success">
                          <div>{$sukses}</div>
                        </div>
                    </div>
                </div>
                {/if}
        
                <!-------------------------------------------- member register area wrapper -------------------------------------------->
                 
            </div>
        </div>
    </div>
</div>