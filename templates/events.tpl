<link rel="stylesheet" href="/assets/css/events.css" />

{literal}

       <script type="text/javascript">

              $('.carousel').carousel({
                     interval: 6000
              });
              $(document).ready(function () {
                     $("#signup").on("click", function () {
                            var email = $("#email").val();
                            if (email != "") {
                                   $.post("index.php?class=Member&method=MailList", {email: email}, function (reply) {
                                          $("#email").val('');
                                          $("#success").css('display', 'block');
                                   });

                            }
                     });
              });
       </script>
{/literal}

<!--header-->
<div class="header">
       <div class="header-top">
              <div class="container">
              </div>
       </div>

</div>
<section class="color ss-style-bigtriangle ">
       <div id="service" class="Services">
              <div class="service-section">
                     <div class="container">
                            <h2 class="allNewTitle">UPCOMING EVENTS</h2>

                     </div>
              </div>

              {if $coming}
                     <div class="container">
                            <div class="col-md-6">
                                   <a href="{$coming.event_link}" target='_blank'>
                                          <img class="img-responsive animated wow fadeInLeft" src="/assets/images/events/{$coming.event_image}" height="300px" style="margin:auto">
                                   </a>

                            </div>

                            <div class="col-md-6 service-grid1" style="margin-bottom:100px">
                                   <p>
                                          {$coming.event_description}
                                   </p>
                                   <a id="registerBtn" class="btn btn-primary" href="{$coming.event_link}" target="_blank">
                                          Register Now <span class="glyphicon glyphicon-chevron-right">
                                          </span>
                                   </a>
                            </div>
                     </div>
              {else}

                     <h3 style='color:black;text-align:center'>Apologies! There are no upcoming events.<br> Sign up for our mailing list to receive information on all our future events.</h3>
                     <input id="email" type="email" placeholder="Email Address" style="font-size:1.1em;width:300px;background-color:#f2f2f2;display:block;margin:30px auto" />
                     <div style="text-align: center">
                            <button id="signup" class="btn btn-primary" style="text-align:center;margin:auto;width:250px;padding:10px;font-size:1.1em">Sign Up for Mailing List</button>
                     </div>
                     <p id='success' style="display: none;color: green;text-align: center;margin-top: 10px">You have successfully subscribed to Kapital Boost's mailing list.</p>
              {/if}


       </div>

</section>


<!--past eventss portfolio not arranged well-->
<div id="portfolio" class="portfolio" style="margin-top:100px !important">
       <div class="container" style='width:100%;margin:0;padding:0'>
              <div class="port-section">


                     <div class="carousel slide" data-ride="carousel" id="sm-slider">
                            <h3>PAST EVENTS</h3>
                            <a class="left carousel-control" href="#sm-slider" role="button" data-slide="prev" style='z-index: 10'>
                                   <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            </a>
                            <a class="right carousel-control" href="#sm-slider" role="button" data-slide="next" style='z-index:10'>
                                   <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            </a>
                            <div class="carousel-inner" >
                                   {section name=outer loop=$past}
                                          {if $past[outer].event_id==$past[0].event_id}
                                                 <div class="item active">
                                                 {else}
                                                        <div class="item">
                                                        {/if}

                                                        <div class="home-5-testimonial">
                                                               <center>
                                                                      <img style="height:300px" src="/assets/images/events/{$past[outer].event_image}"/>
                                                               </center>
                                                               <p  style="color:white;width:70%;margin:40px auto;font-size:1.2em">{$past[outer].event_description}</p>


                                                        </div>
                                                 </div>
                                          {/section}
                                   </div>
                            </div>
                     </div>
              </div>
       </div>



