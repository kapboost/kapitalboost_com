<div id="default_new" style="background: #f2f2f2;margin-bottom: 0;">
       <center>
              <!-- start forgot password -->
              <form method="post" id="" name="login_form" action="member/forgot-password" autocomplete="off">
                     <div class="forgotPassword">
                            <div class="clearfix"></div>
                            {if $error_msg}
                                   <div class="row" style="padding:20px">
                                          <div class="col-xs-12" style="text-align: center; color: red;">
                                                 <div class="alert alert-danger">
                                                        {section name=b loop=$error_msg}
                                                               <div>{$error_msg[b]}</div>
                                                        {/section}
                                                 </div>
                                          </div>
                                   </div>
                            {/if}

                            {if $success_msg}
                                   <div class="row">
                                          <div class="col-xs-12" style="text-align: center; ">
                                                 <div class="" style="font-weight: bold;margin-bottom: 15px">
                                                        <div>{$success_msg}</div>
                                                 </div>
                                          </div>
                                   </div>
                            {/if}
                            <div class="FPInner">
                                   <p class="FPTitle">
                                          Retrieve password
                                   </p>
                                   <input type="email"  id="email"  name="member_email" placeholder="Please enter your email address" required>
                                   <button type="submit" class="">
                                          Submit
                                   </button>
                            </div>
                     </div>
              </form>
              <!-- end forgot password -->
       </center>
       
</div>