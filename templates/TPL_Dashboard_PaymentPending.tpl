<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script>
       {
              literal
       }
       var numbers = 1;
       {
       /literal}
</script>
<center>
       <div class="partnersWrap">
              <div class="partnersInnerDas">
                     <h1 class="h1-1">Payments Pending</h1>

                     <table id="myTable" class="display nowrap" cellspacing="0" width="100%">
                            <thead>
                                   <tr>
                                          <th width="3%">No</th>
                                          <th width="20%">Campaign Name</th>
                                          <th width="10%">Funding Amount</th>
                                          <th width="8%">Commitment Date </th>
                                          <th width="10%">Campaign Closing Date</th>
                                          <th width="10%"> Payment Type</th>
                                          <th width="5%">Payment</th>
                                   </tr>
                            </thead>
                            <tbody>
                                   {section name=outer loop=$listPP}

                                   <tr class="even">

                                          {if $listPP[outer].payment_type=="paypal"}
                            <form action="https://www.paypal.com/cgi-bin/webscr" method="POST">
                                   <!-- Params FOr Paypal -->
                                   <input type="hidden" name="business" value="erly@kapitalboost.com">
                                   <input type="hidden" name="cmd" value="_xclick">
                                   <input type="hidden" name="item_name" value="{$listPP[outer].campaign_name}">
                                   <input type="hidden" name="return" value="https://kapitalboost.com/payment">
                                   <input type="hidden" name="item_number" value="{$listPP[outer].campaign_id}">
                                   <input type="hidden" name="credits" value="510">
                                   <input type="hidden" name="tx" value="TransactionID">
                                   <input type="hidden" name="at" value="ODJuzlFwX-WNtbrnJhd_EB33SXbFXYt7xkWb8AGPGQp-eqi5l0wKkTr8it0">
                                   <input type="hidden" name="cpp_header_image" value="https://kapitalboost.com/assets/images/logo.png">
                                   <input type="hidden" name="no_shipping" value="1">
                                   <input type="hidden" name="currency_code" value="{$listPP[outer].currency}">
                                   <input type="hidden" name="handling" value="0">
                                   <input type="hidden" name="amount" value="{$listPP[outer].amount}">
                                   <input type="hidden" name="custom" value="{$listPP[outer].campaign_name}|{$username}|{$member_email}|{$member_country}|{$listPP[outer].campaign_owner}|{$listPP[outer].token}|{$listPP[outer].campaign_type}|{$listPP[outer].closing_date}|{$listPP[outer].campaign_id}">
                                   <input type="hidden" name="campaign_id" value="{$listPP[outer].campaign_id}">

                                   {elseif $listPP[outer].payment_type=="Bank transfer"}
                                   <form action="payment/bank" method="POST">
                                          <!-- Params For Bank Transfer -->

                                          <input type="hidden" name="amount" value="{$listPP[outer].amount}">
                                          <input type="hidden" name="custom" value="{$listPP[outer].campaign_name}|{$username}|{$member_email}|{$member_country}
                                                 |{$listPP[outer].campaign_owner}|{$listPP[outer].token}|{$listPP[outer].campaign_type}|{$listPP[outer].closing_date}">
                                          <input type="hidden" name="campaign_id" value="{$listPP[outer].campaign_id}">
                                          <input type="hidden" name="token" value="{$listPP[outer].token}">
                                          {elseif $listPP[outer].payment_type=="xfers"}
                                          <form action="xfers/" method="POST">

                                                 <!-- Params FOr Xfers -->
                                                 <input type="hidden" name="xfersphone" value="{$phone}">
                                                 <input type="hidden" name="member_email" value="{$member_email}">
                                                 <input type="hidden" name="order_id" value="{$listPP[outer].order_id}">
                                                 <input type="hidden" name="amount" value="{$listPP[outer].amount}">
                                                 <input type="hidden" name="username" value="{$username}">
                                                 <input type="hidden" name="lastname" value="{$lastname}">
                                                 <input type="hidden" name="item_name" value="{$listPP[outer].campaign_name}">
                                                 <input type="hidden" name="item_item_id_1" value="1">
                                                 <input type="hidden" name="currency" value="SGD">
                                                 <input type="hidden" name="item_description_1" value="Kapitalboost Campaign">
                                                 <input type="hidden" name="item_quantity_1" value="1">
                                                 <input type="hidden" name="project_type" value="{$listPP[outer].campaign_type}">
                                                 <input type="hidden" name="campaign_id" value="{$listPP[outer].campaign_id}">
                                                 <input type="hidden" name="campaign_token" value="{$listPP[outer].token}">
                                                 {else}
                                                 <form action="" method="POST">
                                                        {/if}



                                                        <!--  -->
                                                        <!--<td>{$listPP[outer].ppid|default:'-'}</td>-->
                                                        <script>
                                                               {
                                                                      literal
                                                               }
                                                               document.writeln("<td>" + numbers + "</td>");
                                                               numbers++;
                                                               {
                                                               /literal}
                                                        </script>
                                                        <td>{$listPP[outer].campaign_name|default:'-'}</td>
                                                        <td>SGD {$listPP[outer].amount|default:'-'}</td>
                                                        <td>{$listPP[outer].invest_date|date_format:"%d-%m-%Y" }</td>
                                                        <td>{$listPP[outer].closing_date|date_format:"%d-%m-%Y"}</td>
                                                        <td>{$listPP[outer].payment_type}</td>

                                                        {if $listPP[outer].status=='waiting' && $listPP[outer].payment_type=="Bank transfer"}
                                                        <td><input type="submit" disabled="" name="paynow" style="padding:0;width:100%;cursor:pointer; color:#1eb2f6; margin:3px 0 3px 0; background-color: gray;border:none;" value="Pending"></td>
                                                        {elseif $listPP[outer].status=='open'}              
                                                        <td><input type="submit" name="paynow" style="padding:0;width:100%;cursor:pointer; margin:3px 0 3px 0;" value="Pay Now"></td>              
                                                        {else}
                                                        <td><input type="submit" disabled="" name="paynow" style="padding:0;width:100%;cursor:pointer; color:#FFFFFF; margin:3px 0 3px 0; background-color: gray;border:none;" value="Paid"></td>
                                                        {/if}


                                                 </form>
                                                 </tr>
                                                 {sectionelse}
                                                 <tr class="even">
                                                        <td colspan=9>
                                                 <center><i>No Record Found ...</i></center>
                                                 </td>
                                                 </tr>
                                                 {/section}

                                                 </tbody>
                                                 </table>
                                                 </div>
                                                 </div>

                                                 </center>
