<link rel="stylesheet" type="text/css" href="/xfers/assets/css/gaya.css">

<div class="partnersWrap" ng-app="xfers" ng-controller="xfersCtrl">
<div class="partnersInnerDas" style="width: 100%; margin: 0px">
<center>
  <!-- start xfers transferinfo -->
    <div class="xfersThankYou">
        <div class="XTYInner">
            <div class="XTYText">

                  {if $type == "donation"}
                    <p>
                      Thank you for your generous donation of <br>
                    <div></div>
                      <b>{if $country === 'SINGAPORE'}SGD{/if} {if $country === 'INDONESIA'}IDR{/if} {$amount} </b> for the <b>{$campaign_name}</b> Campaign.
                    </p>
                  {/if}

                  {if $type != "donation"}

                  <p>
                    Thank you for your fund transfer of <br>
                    <b>{if $country === 'SINGAPORE'}SGD{/if} {if $country === 'INDONESIA'}IDR{/if} {$amount} </b> for the <b>{$campaign_name}</b> Campaign.
                  </p>

                  {/if}

            </div>
            <div class="XTYText2">
              {if $type == "donation"}
                <p>
                  <br>The payment status can be viewed in the Portfolio page of your Dashboard.
                  <br>May your kindness be rewarded in multiplefold.
                </p>
              {/if}
                {if $type != "donation"}
                <p>
                  <br>Our administrative team will confirm the receipt of funds and update your payment status shortly. 
                  The payment status can be viewed in the Portfolio page of your Dashboard.

                </p>
                {/if}
                <p>
                    If you have any questions, please email us at
                    <span>
                      <a href="mailto:support@kapitalboost.com">support@kapitalboost.com</a>
                    </span>
                </p>
            </div>
            
            <div class="XTYText4">
                <p>
                    Help support the project by letting others know
                </p>
            </div>
            <ul class="XTYLink">
                <li>
                    <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/{$campaign_id}">
                        <img src="/xfers/assets/img/linkGoogle.jpg" />
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/{$campaign_id}">
                        <img src="/xfers/assets/img/linkFacebook.jpg" />
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/{$campaign_id}">
                        <img src="/xfers/assets/img/linkTwitter.jpg" />
                    </a>
                </li>
                <li>
                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/{$campaign_id}">
                        <img src="/xfers/assets/img/linkLinkedIn.jpg" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- end x fers transferinfo -->


</center>
</div>
</div>
{literal}

{/literal}
