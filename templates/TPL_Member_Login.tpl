
<!-------------------------------------------- main content area wrapper -------------------------------------------->
<div class="subpage-main-content-area-wrapper">
       <div class="subpage-content-wrapper subpage-content-wrapper-2">

              <!-------------------------------------------- subpage breadcrumb area wrapper -------------------------------------------->
              <div class="breadcrumb-wrapper-2">
                     <div class="breadcrumb-wrapper-inner-2">
                            <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   {$breadcrumb}
                            </ol>
                     </div>
              </div>
              <!-------------------------------------------- //subpage breadcrumb area wrapper -------------------------------------------->

              <!-------------------------------------------- subpage mobile breadcrumb area wrapper -------------------------------------------->
              <div class="breadcrumb-mobile-wrapper">
                     <div class="breadcrumb-wrapper-inner">
                            <ol class="breadcrumb">
                                   <li><a href="">Home</a></li>
                                   {$breadcrumb}
                            </ol>
                     </div>
              </div>
              <!-------------------------------------------- //subpage mobile breadcrumb area wrapper -------------------------------------------->
              <div class="clearfix"></div>
              {if $error_msg}
              <div class="row">
                     <div class="col-xs-12">
                            <div class="alert alert-danger">
                                   {section name=b loop=$error_msg}
                                   <div>{$error_msg[b]}</div>
                                   {/section}
                            </div>
                     </div>
              </div>
              {/if}
              <!-------------------------------------------- member login area wrapper -------------------------------------------->
              <div class="member-login-area-wrapper">
                     <div class="col-sm-6">
                            <form method="post" id="login_form" name="login_form" action="member/login" autocomplete="off">
                                   <input type="hidden" name="redirect" value="{$redirect}" />
                                   <input type="hidden" name="guest_checkout" id="guest_checkout"  value="{$guest_checkout}" />
                                   <p class="header-1">Return Visitor ? </p>
                                   <div class="t-center">
                                          <a href="member/facebook" type="button" class="btn-login-with-facebook"><span>LOGIN with FACEBOOK</span></a>
                                          <br><br>
                                          <h4 class="c-pink t-center" style="margin:0;">--- OR ---</h4>
                                   </div>
                                   <br><br>
                                   <p>Please login with your Temple Account</p><br>
                                   <div class="form-group">
                                          <label for="" class="control-label member-login-label-wrapper">Email Address</label>
                                          <div class="member-login-field-wrapper">
                                                 <input type="email" class="form-control form-field" required="required" {literal}pattern="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"{/literal} id="email" placeholder="Email Address" name="member_email" value="">
                                          </div>
                                          <div class="clearfix"></div>
                                   </div>
                                   <div class="form-group">
                                          <label for="" class="control-label member-login-label-wrapper">Password</label>
                                          <div class="member-login-field-wrapper">
                                                 <input type="password" class="form-control form-field" name="member_password" placeholder="Password" style="margin-bottom:8px;">
                                                 <a href="member/forgot-password" style="color:#A48F3E;">Forget your Password ?</a>
                                          </div>
                                          <div class="clearfix"></div>
                                   </div>
                                   <div class="member-login-btn-wrapper">
                                          <button class="btn btn-main" type="submit">Login Now</button>
                                          {if $redirect}
                                          <button class="btn btn-main" type="button" onclick="$('#guest_checkout').val('1');$('#login_form').submit();">Continue as Guest</button>
                                          {/if}
                                   </div>
                                   <br><br>
                            </form>
                     </div>
                     <div class="col-sm-6">
                            <p class="header-1">Register New Account</p>
                            <p><strong>Welcome to Temple.</strong></p>
                            <div class="t-center">
                                   <a href="member/facebook" type="button" class="btn-login-with-facebook"><span>SIGNUP with FACEBOOK</span></a>
                                   <br><br>
                                   <h4 class="c-pink t-center" style="margin:0;">--- OR ---</h4>
                            </div>
                            <br><br>
                            <p>Create an account with us is quick and easy. It will allow you to securely store your payment and delivery details for future orders.</p>
                            <br>
                            <button class="btn btn-main" onclick="window.location.href = 'member/register'">Register Now</button>
                            <br><br>
                     </div>
                     <div class="clearfix"></div>
              </div>
              <!-------------------------------------------- //member login area wrapper -------------------------------------------->

       </div>
       <!--div class="main-footer-area-wrapper-top"></div-->
</div>
<!-------------------------------------------- //main content area wrapper -------------------------------------------->
