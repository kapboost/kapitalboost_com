<link rel="stylesheet" href="assets/css/kp-event.css" />
<div id="event">
    <div id="event-1">
        <div id="event-1-back">
            <div id="event-1-inner">
                <h1 id="event-1-h1">Events</h1>
                <div id="event-1-para-1">
                    <p>Kapital Boost organizes and participates in events in Southeast Asia to educate our community and society. We share our experiences, best practices, insider strategies and Crowdfunding investment concepts with our audience.</p>
                    <p>Join our events and gain invaluable, actionable knowledge.</p>
                </div>
                <div id="event-1-para-2"></div>
                <div id="event-list">
	                {section name=outer loop=$content}
	                
                    <div class="events-each">
                        <div class="events-img">
                            <a href="index.php?class=Events&method=GetContent&event_id={$content[outer].event_id}"><img alt="" src="../../assets/images/event/{$content[outer].event_image}" /></a>
                        </div>
                        <div class="events-info">
                            <h3>{$content[outer].event_name}</h3>
                            <p>{$content[outer].event_description}</p>
                            <p><a href="index.php?class=Events&method=GetContent&event_id={$content[outer].event_id}">Read More</a></p>
                        </div>
                    </div> 
                    {/section} 
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/js/kp-event.js"></script>
