<div id="home">
       <div id="home-1">
              <div id="home-1-inner"class="carousel slide" data-ride="carousel">
                     <ol class="carousel-indicators">
                            <li data-target="#home-1-inner" data-slide-to="0" class="active"></li>
                            <li data-target="#home-1-inner" data-slide-to="1"></li>
                            <li data-target="#home-1-inner" data-slide-to="2"></li>
                     </ol>
                     <div class="carousel-inner">
                            <div class="homeBanner1 item active">
                                   <div class="homeBannerTextWadah1">
                                          <p class="homeBannerText1">
                                                 {if $la=='id'}Indonesian Words{else}Funding Community{/if}<br>
                                                 Growth
                                          </p>
                                          <p class="homeBannerText1">{if $la=='id'}{else}Islamic P2P crowdfunding for SMEs{/if}</p>
                                   </div>
                            </div>
                            <div class="homeBanner2 item">
                                   <div class="homeBannerTextWadah2">
                                          <p class="homeBannerText2">
                                                 {if $la=='id'}{else}Invest ethically and support<br>
                                                 promising SMEs{/if}
                                          </p>
                                   </div>
                            </div>
                            <div class="homeBanner3 item">
                                   <div class="homeBannerTextWadah3">
                                          <p class="homeBannerText3">
                                                 {if $la=='id'}{else}Invest for the hereafter{/if}
                                          </p>
                                          <p class="homeBannerText3">
                                                 {if $la=='id'}{else}Donate to social projects in Asia{/if}
                                          </p>

                                   </div>


                            </div>



                     </div>

              </div>
       </div>


       <script>
              var campaigns = [];
       </script>
       <div id="home-2" class="home-con">
              <div id="home-2-inner" class="container-fluid" style="padding: 0 20px">
                     <h2 class="con-h2">{if $la=='id'}{else}SME Campaigns{/if}</h2>
                     <ul id="campaigns-list" class="row">
                            {section name=outer loop=$content}

                                   {if $member_id}
                                          <li onclick="window.location.href = 'campaign/view/{$content[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each  col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                          {else}
                                          <li   id="eth-log" style="cursor:pointer;" class="eth-logs  col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                 <script>
                                                        {literal}
                                                               campaigns.push('{/literal}{$content[outer].campaign_id}{literal}');
                                                        {/literal}
                                                 </script>
                                          {/if}
                                          <div class="campaigns-list-con">

                                                 <input type="hidden" class="total-amount" value="{$content[outer].total_funding_amt}" />
                                                 <input type="hidden" class="curr-amount" value="{$content[outer].curr_funding_amt}" />

                                                 <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$content[outer].campaign_background}')"></div>
                                                 <div class="campaigns-list-middle">
                                                        <h4 class="campaigns-title">
                                                               {if $content[outer].acronim != ""}
                                                                      {$content[outer].acronim}
                                                               {else}
                                                                      {$content[outer].campaign_name}
                                                               {/if}
                                                        </h4>
                                                        <span class="campaigns-company">{if $la=='id'}{$content[outer].classification_id}{else}{$content[outer].classification}{/if}</span>
                                                        <div class="campaigns-desc">
                                                               <p>{if $la=='id'}{$content[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$content[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}</p>
                                                        </div>

                                                        {if $member_id}
                                                               <p class="campaigns-read-more"><span class="campaigns-read-more-underline" href="campaign/view/{$content[outer].campaign_id}">{if $content[outer].sme_subtype == "" } &nbsp;&nbsp;&nbsp; {else}
															   {if $content[outer].sme_subtype == "INVOICE  FINANCING" }
															   <span style="color:#446CB3 !important;">{$content[outer].sme_subtype}</span>
															   {else}
															   {$content[outer].sme_subtype}{/if}{/if}</span>
                                                               </p>
                                                        {else}
                                                               <p class="campaigns-read-more"><span class="campaigns-read-more-underline" id="eth-log"  >{if $content[outer].sme_subtype == "" } &nbsp;&nbsp;&nbsp; {else}
															   {if $content[outer].sme_subtype == "INVOICE  FINANCING" }
															   <span style="color:#446CB3 !important;">{$content[outer].sme_subtype}</span>
															   {else}
															   {$content[outer].sme_subtype}{/if}{/if}</span>
                                                               </p>
                                                        {/if} 
                                                 </div>
                                                 <div class="campaigns-list-bottom">
                                                        <div class="campaigns-list-bottom-1">
                                                               <div class="campaigns-country">
                                                                      <img class="campaigns-marker" src="../../assets/images/marker.png" />
                                                                      <p class="campaigns-country-name">{$content[outer].country}</p>
                                                               </div>
                                                               <div class="campaigns-industry">
                                                                      <img class="campaigns-marker" src="../../assets/images/industry-icon.png" />
                                                                      <p class="campaigns-industry-name">{$content[outer].industry}</p>
                                                               </div>
                                                               <div style="clear:fix"></div>
                                                        </div>
                                                        <div class="campaigns-bar" {if $content[outer].sme_subtype == "INVOICE  FINANCING" } style="background-color:#D2D7D3 !important;" {/if} >
                                                               <div class="campaigns-bar-inner" {if $content[outer].sme_subtype == "INVOICE  FINANCING" } style="background-color:#446CB3 !important;" {/if} ></div>
                                                        </div>
                                                        <ul class="campaigns-data">
                                                               <li style="background: white; {if $member_country === 'INDONESIA'}height:75px !important;{/if} ">
                                                                      <p class="campaigns-data-bold">S$ {$content[outer].total_funding_amt|number_format}</p>
                                                                      {if $member_country === 'INDONESIA'}
                                                                             <!-- <p class="campaigns-data-bold-id">(IDR {$content[outer].total_funding_amt_id|number_format})</p> -->
                                                                      {/if}
                                                                      <p>{if $la=='id'}{else}Target{/if}</p>
                                                               </li>
                                                               <li style="background: white; {if $member_country === 'INDONESIA'}height:75px !important;{/if} ">
                                                                      <p class="campaigns-data-bold">S$ {$content[outer].curr_funding_amt|number_format}</p>
                                                                      {if $member_country === 'INDONESIA'}
                                                                             <!-- <p class="campaigns-data-bold-id">(IDR {$content[outer].curr_funding_amt_id|number_format})</p> -->
                                                                      {/if}
                                                                      <p>{if $la=='id'}{else}Funded{/if}</p>
                                                               </li>
                                                               
                                                               
                                                               
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$content[outer].project_return} %</p>
                                                                      <p>{if $la=='id'}{else}Returns{/if}</p>
                                                               </li> 


                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$content[outer].funding_summary}</p>
                                                                      <p>{if $la=='id'}{else}Tenor{/if}</p>
                                                               </li>

                                                               {if $content[outer].days_left <= 0}
                                                                      <li style="background: white;">
                                                                             <p class="campaigns-data-bold">0</p>
                                                                             <p>{if $la=='id'}{else}Days left{/if}</p>
                                                                      </li>
                                                               {else}
                                                                      <li style="background: white;">
                                                                             <p class="campaigns-data-bold">{$content[outer].days_left}</p>
                                                                             <p>{if $la=='id'}{else}Days left{/if}</p>
                                                                      </li>
                                                               {/if}
                                                               <li style="background: white;">
                                                                      <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
																				  {if $content[outer].risk=="C" || $content[outer].risk=="C+" || $content[outer].risk=="High"}
																						 <label class="label label-danger" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$content[outer].risk}{/if}</label><br />
																				  {elseif $content[outer].risk=="B" || $content[outer].risk=="B+" || $content[outer].risk=="B-" || $content[outer].risk=="Medium"}
																						 <label class="label label-warning" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$content[outer].risk}{/if}</label><br />
																				  {elseif $content[outer].risk=="A" || $content[outer].risk=="A-" || $content[outer].risk=="Low"}
																						 <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$content[outer].risk}{/if}</label><br />
																				  {else}
																						 <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">N/A</label><br />
																				  {/if}
                                                                             <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                                                                      </div>
                                                               </li>
                                                        </ul>
                                                 </div>
                                          </div>
                                   </li>


                            {/section}
                     </ul>

                     <div style="clear:both"></div>

                     <h2 class="con-h2">{if $la=='id'}{else}Donation Campaigns{/if}</h2>

                     <ul id="campaigns-list" class="row">
                            {section name=outer loop=$donation}
                                   {if $member_id}
                                          <li onclick="window.location.href = 'index.php?class=Campaign&method=GetContent&campaign_id={$donation[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each  col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                          {else}
                                          <li id="eth-log" style="cursor:pointer;" class="cam-li-each eth-logs  col-xs-12 col-sm-6 col-md-4 col-lg-4">
                                                 <script>
                                                        {literal}
                                                               campaigns.push('{/literal}{$donation[outer].campaign_id}{literal}');
                                                        {/literal}
                                                 </script>
                                          {/if}
                                          <div class="campaigns-list-con">

                                                 <input type="hidden" class="total-amount" value="{$donation[outer].total_funding_amt}" />
                                                 <input type="hidden" class="curr-amount" value="{$donation[outer].curr_funding_amt}" />

                                                 <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$donation[outer].campaign_background}')"></div>
                                                 <div class="campaigns-list-middle">
                                                        <h4 class="campaigns-title">{$donation[outer].campaign_name}</h4>
                                                        <span class="campaigns-company">{if $la=='id'}{$donation[outer].classification_id}{else}{$donation[outer].classification}{/if}</span>
                                                        <div class="campaigns-desc">
                                                               <p>{if $la=='id'}{$donation[outer].campaign_snippet_id}{else}{$donation[outer].campaign_snippet}{/if}</p>
                                                        </div>
                                                        {if $member_id}
                                                               <p class="campaigns-read-more"><span class="campaigns-read-more-underline" href="/campaign/view/{$donation[outer].campaign_id}">{if $content[outer].sme_subtype == "" } &nbsp;&nbsp;&nbsp; {else} &nbsp;&nbsp;&nbsp; {/if}</span>
                                                               </p>
                                                        {else}
                                                               <p class="campaigns-read-more"><span class="campaigns-read-more-underline" id="eth-log">{if $content[outer].sme_subtype == "" } &nbsp;&nbsp;&nbsp; {else} &nbsp;&nbsp;&nbsp; {/if}</span>
                                                               </p>
                                                        {/if}
                                                 </div>
                                                 <div class="campaigns-list-bottom">
                                                        <div class="campaigns-list-bottom-1">
                                                               <div class="campaigns-country">
                                                                      <img class="campaigns-marker" src="../../assets/images/marker.png" />
                                                                      <p class="campaigns-country-name">{$donation[outer].country}</p>
                                                               </div>
                                                               <div class="campaigns-industry">
                                                                      {if $donation[outer].project_type=='donation'}
                                                                             <img class="campaigns-marker" src="../../assets/images/donate-icon.png" style="width:20px" />
                                                                      {else}
                                                                             <img class="campaigns-marker" src="../../assets/images/industry-icon.png" />
                                                                      {/if}
                                                                      <p class="campaigns-industry-name">{$donation[outer].industry}</p>
                                                               </div>
                                                               <div style="clear:fix"></div>
                                                        </div>
                                                        <div class="campaigns-bar">
                                                               <div class="campaigns-bar-inner campaigns-bar-inner-green"></div>
                                                        </div>
                                                        <ul class="campaigns-data">
                                                               
                                                               <li style="background: white; {if $member_country === 'INDONESIA'}height:75px !important;{/if}">
                                                                      <p class="campaigns-data-bold">S$ {$donation[outer].total_funding_amt|number_format}</p>
                                                                      {if $member_country === 'INDONESIA'}
                                                                             <!-- <p class="campaigns-data-bold-id">(IDR {$donation[outer].total_funding_amt_id|number_format})</p> -->
                                                                      {/if}
                                                                      <p>{if $la=='id'}{else}Target{/if}</p>
                                                               </li>
                                                               <li style="background: white; {if $member_country === 'INDONESIA'}height:75px !important;{/if}">
                                                                      <p class="campaigns-data-bold">S$ {$donation[outer].curr_funding_amt|number_format}</p>
                                                                      {if $member_country === 'INDONESIA'}
                                                                             <!-- <p class="campaigns-data-bold-id">(IDR {$donation[outer].curr_funding_amt_id|number_format})</p> -->
                                                                      {/if}
                                                                      <p>{if $la=='id'}{else}Funded{/if}</p>
                                                               </li>
                                                               
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{$donation[outer].project_return} %</p>
                                                                      <p>{if $la=='id'}{else}Return{/if}</p>
                                                               </li>
                                                               <li style="background: white;">
                                                                      <p class="campaigns-data-bold">{if $la=='id'}{else}Donation{/if}</p>
                                                                      <p>{if $la=='id'}{else}Tenor{/if}</p>
                                                               </li>
                                                               {if $donation[outer].days_left <= 0}
                                                                      <li style="background: white;">
                                                                             <p class="campaigns-data-bold">0</p>
                                                                             <p>{if $la=='id'}{else}Days left{/if}</p>
                                                                      </li>
                                                               {else}
                                                                      <li style="background: white;">
                                                                             <p class="campaigns-data-bold">{$donation[outer].days_left}</p>
                                                                             <p>{if $la=='id'}{else}Days left{/if}</p>
                                                                      </li>
                                                               {/if}
                                                               <li style="background: white;">
                                                                      <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                                                              {if $donation[outer].risk=="C" || $donation[outer].risk=="C+"}
																					 <label class="label label-danger" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$donation[outer].risk}{/if}</label><br />
																			  {elseif $donation[outer].risk=="B" || $donation[outer].risk=="B+"|| $donation[outer].risk=="B-"}
																					 <label class="label label-warning" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$donation[outer].risk}{/if}</label><br />
																			  {elseif $donation[outer].risk=="A" || $donation[outer].risk=="A-"}
																					 <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$donation[outer].risk}{/if}</label><br />
																			  {else}
																					 <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">N/A</label><br />
																			  {/if}
                                                                             <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                                                                      </div>
                                                               </li>
                                                        </ul>
                                                 </div>
                                          </div>
                                   </li>
                            {/section}
                     </ul>

                     <div style="clear:both"></div>
                     <center>
                            <a class="bluebtn" style="margin: 25px 0 35px 0;" href="campaign/">{if $la=='id'}{else}VIEW ALL PROJECTS{/if}</a>
                     </center>
              </div>
       </div>
       <div id="home-3" class="home-con">
              <div id="home-3-inner">
                     <h2 class="con-h2" style="color:#fff;">{if $la=='id'}{else}What We Do{/if}</h2>
                     <div id="home-3-what-we-do">
                            {if $la=='id'}{else}<p>Kapital Boost aims to tackle the lack of financing available to small businesses in Southeast Asia. We help level the playing field
                                   by offering these businesses a crowdfunding platform to access temporary liquidity for goods and capital purchases.
                            </p>
                            <p>We also address the shortage of attractive Islamic-based investments. By funding small businesses, Kapital Boost members (funders)
                                   have the opportunity to earn attractive returns on short-term, Shariah-structured deals. In other words, our members get rewarded
                                   for doing good. &nbsp;
                            </p>{/if}
                     </div>
                     <p class="home-para-center"><a class="kb-blue-buttons" style="" id="VWButton" style="cursor: pointer;">{if $la=='id'}{else}Watch Video{/if}</a></p>
              </div>
       </div>
       <div id="home-4" class="home-con container">
              <h2 class="con-h2">{if $la=='id'}{else}Why Kapital Boost{/if}</h2>
              <div class="row">
                     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 home-4-divs">
                            <div class="home-4-list-top"><img alt="" src="assets/images/why-4.png" />
                            </div>
                            <p>{if $la=='id'}{else}Fast and simple approval process{/if}
                            </p>
                     </div>
                     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 home-4-divs">
                            <div class="home-4-list-top"><img alt="" src="assets/images/why-1.png" />
                            </div>
                            <p>{if $la=='id'}{else}Competitive funding cost{/if}
                            </p>
                     </div>
                     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 home-4-divs">
                            <div class="home-4-list-top"><img alt="" src="assets/images/why-4-(1).png" />
                            </div>
                            <p>{if $la=='id'}{else}Increase business exposure{/if}</p>
                     </div>
                     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 home-4-divs">
                            <div class="home-4-list-top" ><img alt="" src="assets/images/why-1-(1).png" />
                            </div>
                            <p>{if $la=='id'}{else}Transparency. No hidden fees/charges{/if}
                            </p>
                     </div>
                     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 home-4-divs">
                            <div class="home-4-list-top"><img alt="" src="assets/images/why-2.png" />
                            </div>
                            <p>{if $la=='id'}{else}Short-term investments with attractive returns{/if}
                            </p>
                     </div>
                     <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 home-4-divs">
                            <div class="home-4-list-top"><img alt="" src="assets/images/why-3.png" />
                            </div>
                            <p>{if $la=='id'}{else}Thorough screening and due diligence process{/if}
                            </p>
                     </div>
              </div>
       </div>
       <div id="home-5" class="home-conTes">
              <div class="carousel slide" data-ride="carousel" id="sm-slider">
                     <div id="home-5-inner">
                            <h2 class="con-h2" style="color:#fff;">Testimonials</h2>
                            <a class="left carousel-control" href="#sm-slider" role="button" data-slide="prev">
                                   <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                            </a>
                            <a class="right carousel-control" href="#sm-slider" role="button" data-slide="next">
                                   <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                            </a>
                            <div class="carousel-inner" style="min-height: 425px;">
                                   {section name=outer loop=$testi}
                                          <div class="item active">
                                                 <div class="home-5-testimonial">
                                                        <p class="testi-content">{if $la=='id'}{$testi[outer].testimonial_content_id}{else}{$testi[outer].testimonial_content}{/if}</p>
                                                        <p class="testi-person">{$testi[outer].testimonial_subject}</p>
                                                        <center>
                                                               <img style="width:100px;height:100px;border:2px solid white;border-radius: 50%;" src="../assets/images/testimonial/{$testi[outer].testimonial_image}"/>
                                                        </center>
                                                 {/section}

                                                 {section name=outer loop=$testi2}
                                                        {if $testi2[outer].testimonial_id != 25}
                                                               {$testi2[outer].div_atas}{if $la=='id'}{$testi[outer].testimonial_content_id}{else}{$testi2[outer].testimonial_content}{/if}
                                                               <p class="testi-person">{$testi2[outer].testimonial_subject}</p>
                                                               <center>
                                                                      <img style="width:100px;height:100px;border:2px solid white;border-radius: 50%;" src="../assets/images/testimonial/{$testi2[outer].testimonial_image}"/>
                                                               </center>
                                                        {/if}
                                                 {/section}

                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>
       </div>
       <div id="home-6">
              <h2 class="con-h2">{if $la=='id'}{else}Featured On{/if}</h2>
              <div style="margin-bottom:25px">
                     <div class="home-6-img"><img width="70%" src="/assets/images/bloomberg1.png"></div>
                     <div  class="home-6-img"><img width="70%"  src="/assets/images/salam.png"></div>
                     <div class="home-6-img"><img width="70%"  src="/assets/images/CrowdfundLogo.jpg"></div>
                     <div class="home-6-img"><img width="70%"  src="/assets/images/techinasia.png"></div>
                     <div class="home-6-img"><img  width="70%" src="/assets/images/straits.png"></div>
                     <div  class="home-6-img"><img width="70%"  src="/assets/images/markets.png"></div>
                     <div  class="home-6-img" ><img  width="70%" src="/assets/images/newbfm.png"></div>
                     <div class="home-6-img" ><img width="70%"  src="/assets/images/bh.png"></div>
              </div>
       </div>

       <div id="home-7" class="home-con" style="display: inline-block;">
              <div id="home-7-inner">
                     <h2 class="con-h2">{if $la=='id'}{else}Recognitions{/if}</h2>
                     <div class="center">
                            <div class="recongDivs">
                                   <img alt="" src="assets/images/rice-bowl.png" >
                                   <h3 class="recongTextsh"><strong>{if $la=='id'}{else}Winner{/if}</strong></h3>
                                   <p class="recongTexts">{if $la=='id'}{else}Rice Bowl Startup Awards <br> 2015{/if}</p>

                            </div>
                            <div class="recongDivs">
                                   <img alt="" src="assets/images/SFF.jpg" style="height: 145px;padding-top:25px" >
                                   <h3 class="recongTextsh"><strong>{if $la=='id'}{else}Finalist{/if}</strong></h3>
                                   <p class="recongTexts">{if $la=='id'}{else}MAS Fintech Festival <br> 2016{/if}</p>
                            </div>
                            <div class="recongDivs">
                                   <img alt="" src="assets/images/giff.png" style="height: 165px;padding-top:45px" >
                                   <h3 class="recongTextsh"><strong>{if $la=='id'}{else}Runner-up{/if}</strong></h3>
                                   <p class="recongTexts">{if $la=='id'}{else}ASEAN Fintech Challenge <br> 2016{/if}</p>
                            </div>

                     </div>
              </div>
       </div>
       <div id="home-8" class="home-con" >
              <div id="home-8-inner">
                     <h2 class="con-h2" style="margin-bottom: 20px">{if $la=='id'}{else}Shariah compliance{/if}</h2>
                     <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5" id="home-8Img"><img src="assets/images/FSAC-logo.png"></div>
                     <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7" id="home-8Text">{if $la=='id'}{else}Kapital Boost's Murabaha crowdfunding structure is certified Shariah compliant by the Financial Shariah Advisory & Consultancy (FSAC), a consultancy unit of Pergas Investment Holdings (Singapore).{/if}</div>
              </div>
       </div>


       <!-- start watch video -->
       <div class="watchVideo">
              <center>
                     <div class="WVInner">
                            <div class="VWClose">
                                   <span class="glyphicon glyphicon-remove"> </span>
                            </div>
                            <div class="WVYouTube">
                                   <iframe frameborder="0" src="https://www.youtube.com/embed/PcIBqRsKRlc"> </iframe>
                            </div>
                     </div>
              </center>
       </div>
       <!-- end watch video -->
</div>
