<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="/xfers/assets/css/gaya.css">

<center>
       <div class="xfersThankYou">

              <!-- Bank | SME & Private -->

              <div class="XTYInner">
                     {if $campaign_type!="donation" && $payment_type=="Bank Transfer"}
                     <div class="">

                            <div class="XTYText">
                                   <p>
                                          Thank you for your commitment of <br>
                                          <b>SGD {$amount}</b> in the <b>{$campaign_name}</b> campaign.
                                   </p>
                            </div>
                            <div class="XTYText2">
                                   <p align="center">
                                          Please allow us one working day to prepare your contract. Once ready, it will be sent to your registered email address. <br>Do keep a lookout for it.
                                   </p>
                                   <p align="center">
                                          Meanwhile, you may proceed to make payment to Kapital Boost according to the instructions below. Please note that completion of the investment process is conditional upon successful payment transfer and electronic signing of the Contract.
                                   </p>
                                   <table style="border: 1px solid #cccccc; ">
                                          <tr>
                                                 <th style =" padding:10px;">PAYMENT INSTRUCTIONS</th>
                                          </tr>
                                          <tr>
                                                 <td style =" padding:10px;">Please transfer funds to the following account:</td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Account name: <b>Kapital Boost Pte Ltd</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Bank name:<b> Overseas Chinese Banking Corp. (OCBC)</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Account number: <b>6232 3373 1001</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">SWIFT code: <b>OCBCSGSG</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Bank code: <b>7339</b></td>
                                          </tr>

                                   </table>

                                   <p>
                                          To view your list of payments, please visit the <a href="/dashboard/payment-history">Payment</a> page on your dashboard.
                                   </p>

                            </div>

                     </div>
                     {/if}

                     {if $campaign_type!="donation" && $payment_type=="Xfers" OR $payment_type=="xfers"}
                     <div class="">
                            <div class="XTYText">
                                   <p>
                                          Thank you for your commitment of <b>SGD {$amount}</b> in the <b>{$campaign_name}</b> campaign.
                                   </p>
                            </div>
                            <div class="XTYText2">
                                   <p>
                                          Please allow us one working day to prepare your Contract.
                                          Once ready,
                                          it will be sent to your registered email address. Do keep a lookout for it.
                                   </p>
                                   <p>
                                          You have selected to make payment via Xfers.
                                          Kindly note that only those with Internet banking-enabled Singapore bank accounts are eligible to use Xfers.
                                          To proceed with payment,
                                          please click on the link below.
                                   </p>
                                   <a class="button-kpb-g" href="/dashboard/payment-detail/{$ppid}">Pay Now</a>
                                   <p>
                                          Please note that completion of the investment process is conditional upon successful payment transfer and electronic signing of the Contract.
                                   </p>
                                   <p>
                                          To view your list of payments, please visit the <a href="/dashboard/payment-history">Payment</a> page on your dashboard.
                                   </p>

                            </div>

                     </div>
                     {/if}

                     {if $campaign_type=="donation" && $payment_type=="Bank Transfer"}
                     <div class="">
                            <div class="XTYText">
                                   <p>
                                          Thank you for your donation of <br>
                                          <b>SGD {$amount}</b> in the <b>{$campaign_name}</b> campaign.
                                   </p>
                            </div>
                            <div class="XTYText2">
                                   <p>
                                          Please proceed to make payment to Kapital Boost according to the instructions below.
                                   </p>
                                   <table style="border: 1px solid #cccccc; ">
                                          <tr>
                                                 <th style =" padding:10px;">PAYMENT INSTRUCTIONS</th>
                                          </tr>
                                          <tr>
                                                 <td style =" padding:10px;">Please transfer funds to the following account:</td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Account name: <b>Kapital Boost Pte Ltd</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Bank name:<b> Overseas Chinese Banking Corp. (OCBC)</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">Account number: <b>6232 3373 1001</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">SWIFT code: <b>OCBCSGSG</b></td>
                                          </tr>
                                          <tr>
                                                 <td  style =" padding:10px;">SWIFT Bank code: <b>7339</b></td>
                                          </tr>

                                   </table>
                                   <p>
                                          To view your list of payments, please visit the <a href="/dashboard/payment-history">Payment</a> page on your dashboard.
                                   </p>

                            </div>

                     </div>
                     {/if}

                     {if $campaign_type=="donation" && $payment_type=="Xfers"}
                     <div class="">
                            <div class="XTYText">
                                   <p>
                                          Thank you for your donation of <b>SGD {$amount}</b> in the <b>{$campaign_name}</b> campaign.

                                   </p>
                            </div>
                            <div class="XTYText2">
                                   <p>
                                          You have selected to make payment via Xfers.
                                          Kindly note that only those with Internet banking-enabled Singapore bank accounts are eligible to use Xfers.
                                          To proceed with payment,
                                          please click on the link below.
                                   </p>
                                   <p>
                                          <a class="button-kpb-g" href="/dashboard/payment-detail/{$ppid}">Pay Now</a>
                                   </p>
                                   <p>
                                          To view your list of payments, please visit the <a href="/dashboard/payment-history">Payment</a> page on your dashboard.
                                   </p>

                            </div>

                     </div>
                     {/if}

                     {if $campaign_type!="donation" && $payment_type=="Paypal"}
                     <div class="">
                            <div class="XTYText">
                                   <p>
                                          Thank you for your donation of <br>
                                          <b>SGD {$amount}</b> in the <b>{$campaign_name}</b> campaign.

                                   </p>
                            </div>
                            <div class="XTYText2">
                                   <p>
                                          Please allow us one working day to prepare your Contract. Once ready, it will be sent to your registered email address. Do keep a lookout for it.
                                   </p>
                                   <a class="button-kpb-g" href="/dashboard/payment-detail/{$ppid}">Pay Now</a>
                                   <p>
                                          Please note that completion of the investment process is conditional upon successful payment transfer and electronic signing of the Contract.
                                   </p>
                                   <p>
                                          To view your list of payments, please visit the <a href="/dashboard/payment-history">Payment</a> page on your dashboard.
                                   </p>

                            </div>
                            <div class="XTYText3">
                                   <p>
                                          Best regards, <br />
                                          Kapital Boost team
                                   </p>
                            </div>
                     </div>
                     {/if}

                     {if $campaign_type=="donation" && $payment_type=="Paypal"}
                     <div class="">
                            <div class="XTYText">
                                   <p>
                                          Thank you for your donation of <br>
                                          <b>SGD {$amount}</b> in the <b>{$campaign_name}</b> campaign.
                                   </p>
                            </div>
                            <div class="XTYText2">
                                   <p>You may proceed to make payment using Paypal by clicking on the link below.
                                   </p>
                                   <br>
                                   <a class="button-kpb-g" href="/dashboard/payment-detail/{$ppid}">Pay Now</a>
                                   <br>
                                   <p>
                                          To view your list of payments, please visit the <a href="/dashboard/payment-history">Payment</a> page on your dashboard.
                                   </p>

                            </div>
                            <div class="XTYText3">
                                   <p>
                                          Best regards, <br />
                                          Kapital Boost team
                                   </p>
                            </div>
                     </div>
                     {/if}
              </div>
       </div>
       <center>
