<link rel="stylesheet" href="assets/css/kp-contact.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="allBanner">
       <img src="../assets/images/contactus.jpg" />
       <div class="allTitle">
              <p></p>
       </div>
</div>
<div id="contact">
       <div id="contact-1">
              <div id="contact-1-back">
                     <div id="contact-1-inner">
                            <div id="contact-1-main">
                                   <h2 class="allNewTitle">{if $la == 'id'}{else}Contact Us{/if}</h2>
                                   <!-- div id="contact-1-main-right">
                                   {$contentright}
                                  <h2 id="contact-info-h2"><span style="font-size: 22px; color: #1f497d;">Contact Information</span></h2>
                                  <p>
                                     <span class="allContentText"><strong>General Issues</strong>
                                     <br />
                                     <span class="allContentTextEtc3">
                                     support@kapitalboost.com
                                     </span></span>
                                  </p>
                                  <p>
                                     <span class="allContentText"><strong>Membership &amp; partnership</strong><br />
                                     <span class="allContentTextEtc3">
                                     hello@kapitalboost.com
                                     </span></span>
                                  </p>
                                  <p>
                                     <span class="allContentText"><strong>Media&nbsp;inquiries</strong><br />
                                     <span class="allContentTextEtc3">
                                     erly@kapitalboost.com
                                     </span></span>
                                  </p>
                                  <p>
                                     <span class="allContentText"><strong>Administrative Office</strong>
                                     <br />
                                     <span class="allContentTextEtc3">
                                     397A Changi Rd, Singapore 419844
                                     <br />
                                     9477 3150</span></span>
                                  </p>
                               </div>-->
                                   <div id="contact-1-main-left">
                                          {$contentleft}
                                          <!--p><span class="allContentTextEtc3">Please leave us a message. Do clearly state your objectives so we may respond accurately. Feedback on our services - positive or negative - is also much welcomed.</span></p>
                                          <p><span class="allContentTextEtc3">For businesses seeking financing, please provide us with the following information:</span></p>
                                          <ul>
                                             <li><span class="allContentTextEtc3">Company name</span></li>
                                             <li><span class="allContentTextEtc3">Country of incorporation</span></li>
                                             <li><span class="allContentTextEtc3">Registration number</span></li>
                                             <li><span class="allContentTextEtc3">Industry</span></li>
                                             <li><span class="allContentTextEtc3">Year established</span></li>
                                             <li><span class="allContentTextEtc3">Annual revenue in the past 12 months</span></li>
                                             <li><span class="allContentTextEtc3">Amount of funding required</span></li>
                                             <li><span class="allContentTextEtc3">Use of proceeds</span></li>
                                          </ul>
                                          <div><span class="allContentTextEtc3">Business owners, we also need you to provide us with your e-mail address in the link below (Score yourself) to determine your credit rating based on social media activities. Visit www.friendlyscore.com for more info.&nbsp;</span></div>
                                          <script type="text/javascript" id="__fs-widget">
                                          {literal}
                                             var __lc = {};
                                             __lc.application = 1079;
                                             __lc.borrowerIdentifier = undefined; // use your borrower identifier from database
                                             __lc.lang = undefined; // leave undefined to use English
                                             __lc.buttonText = "Score yourself"; // you can change this text
                                             
                                             (function() {
                                                 var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                                                 lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.friendlyscore.com/widgets.js';
                                                 var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
                                             })();
                                          {/literal}
                                        </script>
                                        <span class="allContentText"><br />
                                        </span-->
                                          <br />
                                          <form name="catwebformform6194" method="post" enctype="multipart/form-data" action="sendform/" id="contactItem" >
                                                 <table style="width: 100%;" cellspacing="0" cellpadding="2" border="0">
                                                        <tbody>
                                                               <!--tr>
                                                                  <td>
                                                                     <label for="Title">Title</label>
                                                                     <br />
                                                                     <select name="Title" id="Title" class="cat_dropdown_smaller">
                                                                        <option value="1107911">DR</option>
                                                                        <option value="1107910">MISS</option>
                                                                        <option value="1107907" selected="selected">MR</option>
                                                                        <option value="1107908">MRS</option>
                                                                        <option value="1107909">MS</option>
                                                                     </select>
                                                                  </td>
                                                               </tr>
                                                               <tr>
                                                                  <td>
                                                                     <label for="FirstName">First Name <span class="req">*</span>
                                                                     </label>
                                                                     <br />
                                                                     <input type="text" name="FirstName" id="FirstName" class="cat_textbox" maxlength="255" style="border: 1px solid #cccccc;" /> 
                                                                  </td>
                                                               </tr-->
                                                               <tr>
                                                                      <td>
                                                                             <label for="LastName">{if $la == 'id'}{else}Name{/if} <span class="req">*</span>
                                                                             </label>
                                                                             <br />
                                                                             <input type="text" name="nama_lengkap" id="LastName" class="cat_textbox" maxlength="255" style="border: 1px solid #cccccc;" required/> 
                                                                      </td>
                                                               </tr>
                                                               <tr>
                                                                      <td>
                                                                             <label for="EmailAddress">{if $la == 'id'}{else}Email Address{/if} <span class="req">*</span>
                                                                             </label>
                                                                             <br />
                                                                             <input type="email" name="email" id="EmailAddress" class="cat_textbox" maxlength="255" style="border: 1px solid #cccccc;" required/> 
                                                                      </td>
                                                               </tr>
                                                               <tr>
                                                                      <td>
                                                                             <label for="CAT_Custom_19979554">{if $la == 'id'}{else}Message{/if} <span class="req">*</span>
                                                                             </label>
                                                                             <br />
                                                                             <textarea name="pesan" form="contactItem" id="CAT_Custom_19979554" cols="10" rows="4" class="cat_listbox"  style="border: 1px solid #cccccc;" required></textarea>
                                                                      </td>
                                                               </tr>
                                                               <tr>
                                                                      <td>
                                                                             <label for="CAT_Custom_19979554">
                                                                             </label>
                                                                             <br />
                                                                             <div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
                                                                      </td>
                                                               </tr>
                                                               <tr>
                                                                      <td>
                                                                             <span style=""><span class="req" style="font-size: 16px;">*</span> <label style="font-weight: normal;font-style: italic">{if $la == 'id'}{else}Required{/if}</label>
                                                                                    <br />
                                                                                    <!--<div class="g-recaptcha" data-sitekey="6LfYhwgUAAAAAMqAML_HLndbEVdy3cF8blYobat-"></div>-->
                                                                                    <button
                                                                                           id="catwebformbutton" style="margin-top:0;"
                                                                                           > Submit
                                                                                    </button>
                                                                                    <br />
                                                                                    <!--<input class="cat_button" type="submit" name="submit" value="Submit" id="catwebformbutton" style="margin-top: 0;" />-->
                                                                             </span>
                                                                      </td>
                                                               </tr>


                                                        </tbody>

                                                 </table>
                                          </form>
                                          <br/>
                                          <span class="allContentText"><strong>Administrative Office: </strong><span class="allContentTextEtc3"> 116 Changi Rd #05-00,
                                                        <span class="allContentTextEtc3">
                                                               Singapore 419718
                                                        </span>
                                                 </span>
                                          </span>
                                                 <br />
                                                 {literal}

                                                        <!--<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC4nVCgFqVSNykE9BMFeHi6cueeCkQNbZk"></script>-->
                                                        <script>// var myCenter = new google.maps.LatLng(1.3168867, 103.902031);
                                                               var myCenter = new google.maps.LatLng(1.316920, 103.901270);

                                                               function initialize()
                                                               {
                                                                      var mapProp = {
                                                                             center: myCenter,
                                                                             zoom: 11,
                                                                             mapTypeId: google.maps.MapTypeId.ROADMAP
                                                                      };

                                                                      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                                                                      var marker = new google.maps.Marker({
                                                                             position: myCenter,
                                                                      });

                                                                      marker.setMap(map);
                                                               }

                                                               google.maps.event.addDomListener(window, 'load', initialize);
                                                        </script>

                                                        <script>

                                                               function SubmitBtn() {
                                                                      var name = document.getElementById("LastName");
                                                                      var email = document.getElementById("EmailAddress");
                                                                      var message = document.getElementById("CAT_Custom_19979554");

                                                                      if (name.checkValidity() && email.checkValidity() && message.checkValidity()) {
                                                                             if (message.length > 4000) {
                                                                                    message.value.substring(0, 3999);
                                                                             }
                                                                             $("#contactItem").submit();

                                                                      } else {
                                                                             alert("Please fill in all the required fields");
                                                                             grecaptcha.reset();
                                                                      }


                                                               }


                                                               // var myCenter = new google.maps.LatLng(1.316975, 103.902185);
                                                               var myCenter = new google.maps.LatLng(1.316920, 103.901270);

                                                               function initialize()
                                                               {
                                                                      var mapProp = {
                                                                             center: myCenter,
                                                                             zoom: 11,
                                                                             mapTypeId: google.maps.MapTypeId.ROADMAP
                                                                      };

                                                                      var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                                                                      var marker = new google.maps.Marker({
                                                                             position: myCenter,
                                                                      });

                                                                      marker.setMap(map);
                                                               }

                                                               google.maps.event.addDomListener(window, 'load', initialize);
                                                        </script>
                                                 {/literal}
                                                 <span class="allContentText"><strong>Phone: </strong><span class="allContentTextEtc3"> +65 9865 9648</span></span>
                                                 <!-- <span class="allContentText"><strong>Phone: </strong><span class="allContentTextEtc3"> +65 9477 3150</span></span> -->
                                                 <br />
                                                 <span class="allContentText"><strong>Email: </strong><span class="allContentTextEtc3"> <a href="mailto:hello@kapitalboost.com? " >hello@kapitalboost.com</a>
                                                        </span></span>
                                                 <br />
												  <br />
												  <span class="allContentText"><strong>Indonesia Representative Office: </strong><span class="allContentTextEtc3"> <br>Rukan Puri Mansion blok B no. 7 Jalan Lingkar Luar Barat Raya <br>
												  Kembangan – Jakarta Barat 11610 Indonesia
												  </span>
														 <br />
												  <span class="allContentText"><strong>Office Hour: </strong><span class="allContentTextEtc3"> Monday - Friday (8am - 5pm)
												  </span>
                                                 <br />
												  <span class="allContentText"><strong>Phone: </strong><span class="allContentTextEtc3"> +62215401817
												  </span>
                                                 <br />
                                                 <br />
                                                 <br />
                                                 <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d498.5955658439614!2d103.90203104165586!3d1.3168866833982749!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe9bc290a07cf9c00!2sGuthrie+Building!5e0!3m2!1sen!2sza!4v1491805944098" width="900" height="500" frameborder="0" style="border:0" allowfullscreen></iframe> -->

                                                 <br />
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7652183484884!2d103.8990244142654!3d1.316454462049741!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1810731bc98b%3A0x3e1d7d573d36ab21!2s116+Changi+Rd%2C+05%2C+Singapore+419718!5e0!3m2!1sen!2sid!4v1554978106059!5m2!1sen!2sid" width="900" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
    

                                   </div>
                            </div>

                     </div>
              </div>
       </div>
</div>


{if $sukses}
       <!-- start popup -->
       <div class="contactWrap">
              <center>
                     <div class="contactInner">
                            <div class="contactClose">
                                   <span class="glyphicon glyphicon-remove"></span>
                            </div>
                            <p class="contactTitle">
                                   <strong>Thank you for getting in touch!</strong> 
                            </p>
                            <p class="contactText">
                                   Our team is looking into it and will reply to you shortly. If your enquiry is urgent, you may call us at +65 9865 9648 from Monday to Friday during our office hours.
                            <p style="text-align: center;font-size: 17px;">
                                   <br />
                                   Talk to you soon,
                                   <br />
                                   Kapital Boost
                            </p>
                            </p>
                     </div>
              </center>
       </div>
{/if}
<!-- end popup -->

{literal}
       <script>
              $(document).ready(function () {
                     $(window).load(function () {
                            $('.contactWrap').fadeIn(200);
                            $('.contactClose').click(function () {
                                   $('.contactWrap').fadeOut(200);
                            });
                     });
              });
       </script>
{/literal}

<script type="text/javascript" src="assets/js/kp-contact.js"></script>
