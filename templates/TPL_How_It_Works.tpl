<div class="allBanner HIYBanner">
     <!-- <img src="../assets/images/howitworks.jpg" /> -->
       <center>
              <div class="videoYt"><iframe class="HS2YT" frameborder="0" src="https://www.youtube.com/embed/PcIBqRsKRlc">     </iframe>
              </div>
       </center>
       <!--
            <div class="allTitle">
            <center>
       -->
       <!--
           <div class="howWrap">
               <div class="howTitle">
                   <p></p>
               </div>
               <ul class="howUl">
                   <li class="active">
                       <p><a href="how-it-works#invest">Investing</a></p>
                   </li>
                   <li>
                       <p><a href="how-it-works#raise">How to Raise</a></p>
                   </li>
                   <li>
                       <p><a href="how-it-works#murabaha">Murabaha</a></p>
                   </li>
                   <li>
                       <p><a href="how-it-works#mudharaba">Mudharaba</a></p>
                   </li>
               </ul>
           </div>
       -->
       <!--
           </center>
            </div>
       -->
</div>
<center>
       <!--
           <ul class="howUlHp">
               <li>
                   <p><a href="how-it-works#invest">How to Invest</a></p>
               </li>
               <li>
                   <p><a href="how-it-works#raise">How to Raise</a></p>
               </li>
               <li>
                   <p><a href="how-it-works#murabaha">Murabaha</a></p>
               </li>
               <li>
                   <p><a href="how-it-works#mudharaba">Mudharaba</a></p>
               </li>
           </ul>
       -->
       <div class="partnersWrap">
              <div class="partnersInner" style="text-align: left;">
                     <!--
                     <div id="default">
                     <div id="default-1">
                     <div id="default-1-back">
                     -->
                     <!--
                     <div class="videoYt"><iframe class="HS2YT" frameborder="0" src="https://www.youtube.com/embed/"></iframe></div>
                     </div>
                     -->
                     <!--
                     <div class="default-custom" id="default-1-inner" style="text-align: left;">
                     -->
                     <h2 class="allNewTitle">How it works <span id="menu_slide" class="plus">+</span></h2>
                     <!--
                     <div class="HIYDropMenu">
                         <p id="dropContent1">What would you like to know about? <span class="caret"></span></p>
                         <p id="dropContent2">What would you like to know about? <span class="caret"></span></p>
                         <ul class="HIYDropMenuItem" id="dropItem">
                             <li><p><a href="how-it-works#invest">How to invest</a></p></li>
                             <li><p><a href="how-it-works#raise">How to raise funds</a></p></li>
                             <li><p><a href="how-it-works#murabaha">Murabaha financing</a></p></li>
                             <li><p><a href="how-it-works#mudharaba">Mudharabah financing</a></p></li>
                         </ul>
                     </div>
                     -->

                     <div class="howWrap">
                            <ul class="nav nav-tabs howUl">
                                   <li class="active">
                                          <p><a href="#invest" data-toggle="tab">Investing</a></p>
                                   </li>
                                   <li>
                                          <p><a href="#raise" data-toggle="tab">Raising funds</a></p>
                                   </li>
                                   <li>
                                          <p><a href="#murabaha" data-toggle="tab">Murabaha</a></p>
                                   </li>
                                   <li>
                                          <p><a href="#mudharaba" data-toggle="tab">Mudharaba</a></p>
                                   </li>
                            </ul>
                     </div>

                     <div class="tab-content">
                            <div class="tab-pane active" id="invest">
                                   <p> </p>
                                   <h3 id="invest"><span  class="allContentTextEtc8"><strong>How to invest (for Kapital Boost members)</strong></span></h3>
                                      
                                   <div class="how-to-list">
                                          <ul>

                                                 <li><div class="how-to-list-img"><img class="how-img" src="assets/images/browse-finding-campaign.png" /></div>
                                                        <div class="how-to-list-header">Browse Funding<br> Campaigns</div>
                                                        <div class="how-to-list-text">
                                                               <ul>
                                                                      <li>Browse SME funding campaigns across different sectors and countries</li>
                                                                      <li>Invest from as low as SGD500 on short- term, legally-structured funding opportunities with attractive returns</li>
                                                               </ul>
                                                        </div>
                                                 </li>

                                                 <li><div class="how-to-list-img"><img class="how-img" src="assets/images/Conduct-due-diligence.png" /></div>
                                                        <div class="how-to-list-header">Conduct Due<br> Diligence & Analysis</div>
                                                        <div class="how-to-list-text">
                                                               <ul>
                                                                      <li>Conduct your own due diligence using the in-depth information provided</li>
                                                                      <li>Have questions? Contact us at support@kapitalboost.com or get in touch with business owners directly</li>
                                                               </ul>
                                                        </div>
                                                 </li>

                                                 <li><div class="how-to-list-img"><img class="how-img1" src="assets/images/find-sme-earn-attractive-returns.png" /></div>
                                                        <div class="how-to-list-header">Fund SMEs, Earn Attractive Returns</div>
                                                        <div class="how-to-list-text">
                                                               <ul>
                                                                      <li>Determine the amount you want to invest</li>
                                                                      <li>Earn annualised return on investments of 15-24%</li>
                                                                      <li>Support community growth by funding SMEs</li>
                                                               </ul>
                                                        </div>
                                                 </li>

                                          </ul>
                                   </div>
                            </div>

                            <div class="tab-pane" id="raise">
                                   <p> </p>
                                   <h3 id="raise"><strong><span class="allContentTextEtc8">How to raise funds (for SMEs)</span></strong></h3>

                                   <div class="how-to-list">
                                          <ul>

                                                 <li><div class="how-to-list-img"><img class="how-img" src="assets/images/Meet-the-requirements.png" /></div>
                                                        <div class="how-to-list-header">Meet The<br> Requirements</div>
                                                        <div class="how-to-list-text">
                                                               <ul>     
                                                                      <li>At least one year of operation</li>
                                                                      <li>Minimum annual sales revenue of SGD100,000</li>
                                                                      <li>Historical positive free cash flow</li>
                                                                      <li>Have purchase order or work order that requires funding</li>
                                                               </ul>
                                                        </div>
                                                 </li>

                                                 <li><div class="how-to-list-img"><img class="how-img" src="assets/images/due-diligence-and-verification.png" /></div>
                                                        <div class="how-to-list-header">Due Diligence &<br> Verification</div>
                                                        <div class="how-to-list-text">
                                                               <ul>
                                                                      <li>Collection and verification of information on SME</li>
                                                                      <li>Use of proprietary credit scoring to determine SME's risk profile</li>
                                                                      <li>Determine the deal size, tenor, and asset purchase markup</li>
                                                               </ul>
                                                        </div>
                                                 </li>

                                                 <li><div class="how-to-list-img"><img class="how-img2" src="assets/images/get-funded.png" /></div>
                                                        <div class="how-to-list-header">Get Funded</div>
                                                        <div class="how-to-list-text">
                                                               <ul>
                                                                      <li>Launch of crowdfunding campaign</li>
                                                                      <li>Funds are transferred to SME upon campaign success</li>
                                                                      <li>SME purchases assets on behalf of financiers; buys assets from financiers at an agreed date in future</li>
                                                               </ul>
                                                        </div>
                                                 </li>

                                          </ul>
                                   </div>
                            </div>

                            <div class="tab-pane" id="murabaha">
                                   <p> </p>
                                   <p><h3 id="murabaha"><strong><span class="allContentTextEtc9"><span class="allContentTextEtc8">Murabaha (cost-plus) Crowdfunding</span></span></strong></h3></p>

                                   <p class="textJustify"><span style="color:#3f3f3f"><span class="allContentText">Kapital Boost offers funding to small businesses through a Murabaha or “cost-plus” arrangement – the purchase of an asset by investors for a small business, with the agreement to sell the product to the small business at a marked-up price some time in the future. The payment and asset purchasing flow between Kapital Boost, the small business, and the seller/supplier are shown and explained below.</span><strong><em> </em></strong></span></p>

                                   <p style="text-align:left"><span style="color:#595959; font-size:18px"><img alt="" src="assets/images//Murabaha.png" class="howImageContent2" /></span></p>

                                   <p> </p>


                                   <h3> </h3>


                                   <center>
                                          <div class="leftRight">
                                                 <p> 
                                                        <!-- 
                                                        <span class="allContentText"><a class="HIWButton leftRight" id="image-popup"> <em>See infographic     </em></a>
                                                        -->
                                                        </span></p><br />
                                          </div>
                            </div>

                            <div class="tab-pane" id="mudharaba">
                                   <p> </p>
                                   <p><h3 id="mudharaba"><span class="allContentTextEtc8"><strong>Mudharaba (profit sharing) Crowdfunding</strong></span></h3>
                                   </p>
                                   <div>
                                          <p class="textJustify">We offer a Mudharaba crowdfunding for businesses or projects which are not able to raise financing using a Murabaha structure. The key requirement for Mudharaba is that a profit sharing ratio for a financing project is determined between Kapital Boost, investors and the small business prior to launching a crowdfunding campaign. Based on financial projections on the project, investors can estimate the return it can expect to get at the end of the investment period. </p>

                                          <p class="textJustify">This funding structure is more suited for projects which has clarity on the expected revenue and costs. To reduce credit risks for investors, Kapital Boost also prefer businesses with existing purchase orders, which require funding, or businesses having financial support from a strong parent.   </p>
                                   </div>

                                   <p style="text-align:center"><span class="allContentTextEtc8"><img alt="" src="assets/images//Mudharabapic2.png" class="howImageContent" /></span></p>

                                   <ol><br />
                                          <li class="textJustify">After a Kapital Boost member (financier) commits to investing in a crowdfunding campaign, a Mudharaba agreement is signed between the financier and the small business. The agreement clearly states the role of the small business as an expert in the investment of capital and the role of the financier as the provider of capital. Based on their roles, a profit sharing ratio for the project is determined between the two parties. </li>
                                          <br />
                                          <li class="textJustify"> Following project completion, the earnings are calculated. The investment principal is returned to financiers and whatever profits are earned from the project are divided between the financier and small business based on the agreed profit ratio. </li>
                                          <br />
                                          <li class="textJustify">
                                                 <div>A loss arising from the project will be borne by the financiers, except in the case of 'negligence of duty' by the small business. </div>
                                                 <span class="allContentText"> <em> </em></span> <em> </em></li>
                                   </ol>
                            </div>
                     </div>

                     <center>
                            <!--
                            <div class="leftRight">
                                <a class="HIWButton leftRight" id="image-popup-s"><em>See infographic </em></a>
                            </div>
                            -->
                     </center>
                     <!--
                     </div>
                     -->


                     <!--
                     </div>
                     </div>
                     </div>
                     -->
              </div>
       </div>

</center>
<center>
       <div id="image-popup-wadah">
              <div id="image-popup-content">
                     <div id="close-popup">
                            <h2>X</h2>
                     </div>
                     <img src="assets/images/KB-Inforgraphic-murabaha-final-2.jpg" />
              </div>
       </div>

       <div id="image-popup-wadah-s">
              <div id="image-popup-content-s">
                     <div id="close-popup-s">
                            <h2>X</h2>
                     </div>
                     <img src="assets/images/Neuentity-Mudharabah.png" />
              </div>
       </div>
</center>
