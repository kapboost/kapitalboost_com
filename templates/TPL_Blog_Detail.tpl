
<div id="blog-1">
              <div id="blog-1-inner">
                     <p class="description"></p>
                     <div class="wrapper clear">
                            <div class="post-list">
                                   <div id="catblogoutput">

                                                 <div class="blog-post">
                                                        <h1 class="post-title">{$content.blog_title}</h1>
                                                        <div class="post-details">{$content.release_date|date_format:"%A, %B %e, %Y"} </div>
                                                        <div class="post-body">

                                                               <img alt="" src="../assets/images/blog/{$content.image}" style="max-width: 100%;width: auto;" />
                                                               <br />
                                                               <br />
                                                               <br />

                                                               {$content.blog_content}

                                                               <div id="share"></div>

                                                               <div class="blog-tag">
                                                                      <p class="blog-tag-name"> Categories: <i class="fa fa-tags fa-2x fa-fw campaigns-marker"></i> {section name=outer loop=$tags}<a href="https://kapitalboost.com/index.php?class=Blog&method=LoadList&tag={$tags[outer]}">{$tags[outer]}</a> {/section}</p>
                                                               </div>

                                                        </div>
                                                 </div>
                                   </div>
                                   <div class="subs">
                                     <div id="mc_embed_signup" class="container">
                                       <div class="row">
                                         <div class="col-md-5 col-md-offset-1">
                                           <br>
                                           <h3>Don't miss another article</h3>
                                           <p>Get fresh and insightful blog contents</p>
                                           <p>delivered to your inbox</p>
                                         </div>
                                         <div class="col-md-5">
                                           <form action="https://kapitalboost.us11.list-manage.com/subscribe/post?u=6f68fd3109ce3ac1e85a54f6b&amp;id=c2bdf8bd59" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                                             <div id="mc_embed_signup_scroll">
                                               <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                                               <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                               <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6f68fd3109ce3ac1e85a54f6b_c2bdf8bd59" tabindex="-1" value=""></div>
                                               <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>                                             </div>
                                           </form>
                                         </div>
                                       </div>
                                     </div>

                                     <!--End mc_embed_signup-->

                                   </div>
                            </div>
                     </div>
              </div>
</div>

<script type="text/javascript" src="assets/js/kp-blog.js"></script>
<script type="text/javascript" src="assets/js/jssocials.min.js"></script>
<script>
    $("#share").jsSocials({
        shares: ["twitter", "facebook", "linkedin", "whatsapp"]
    });
</script>
