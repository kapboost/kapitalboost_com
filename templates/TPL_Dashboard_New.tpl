<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<div class="partnersWrap">
    <div class="partnersInnerDas">
<div class="dasContent">
<table id="myTable" class="display nowrap" cellspacing="0" width="100%">
        <thead>
      <tr>
        <th width="3%">No</th>
        <th width="13%">Investor Name</th>
        <th width="10%">Investor Email</th>
        <th width="10%">Country</th>
        <th width="12%">Campaign Name</th>
        <th width="10%"> Payment Type</th>
        <th width="10%">Total Funding</th>
        <th width="15%">Date Of Payment </th>
        <th width="5%"> Status</th>     
      </tr>
    </thead>
<tbody>
    {section name=outer loop=$list}
      <tr class="even">
        <td>{$smarty.section.outer.index+1}</td>
        <td><a href="index.php?class=Dashboard&method=InvestData&id={$list[outer].id}">{$list[outer].nama}</a></td>
        <td>{$list[outer].email}</td>
        <td>{$list[outer].country|default:'-'}</td>
<td>{$list[outer].campaign|default:'-'}</td>
<td>{$list[outer].tipe|default:'-'}</td>
<td>{$list[outer].total_funding|default:'-'}</td>
        <td>{$list[outer].date|date_format:"%d-%m-%Y %H:%M"}</td>
{if $list[outer].status=='Paid'}
<td><label class="label label-success"> Paid </label></td>
{else}
<td><label class="label label-warning"> Unpaid </label></td>
{/if}
        
      </tr>
    {sectionelse}
      <tr class="even">
        <td colspan=9>
          <center><i>No Record Found ...</i></center>
        </td>
      </tr>
    {/section}
    </tbody>
  </table>
   </div>
  </div>
</div>
<script>
{literal}
$(document).ready(function(){
    $('#myTable').DataTable();
});
{/literal}
</script>