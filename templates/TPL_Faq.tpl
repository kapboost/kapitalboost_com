<center>
			<div id="container">
<center>
<div class="partnersWrap">
    <div class="partnersInner">
<!--
                <div id="default">
                    <div id="default-1">
                        <div id="default-1-back">
                            <div id="default-1-inner" style="text-align: left;">
-->
                                <p style="text-align: center;"><strong></strong></p>
                                <h1><strong><span style="font-size: 28px;">FAQs</span></strong></h1>
                                <p><strong></strong></p>
                                <h3><strong><span style="font-size: 24px; color: #1f497d;"><br />
                                    </span></strong>
                                </h3>
                                <!--h3><strong><span style="font-size: 24px; color: #1f497d;">For small businesses</span></strong></h3>
                                <h3><span style="font-size: 16px;"><br />
                                    </span>
                                </h3>
                                <h3><span style="font-size: 20px;"><em>What type of funding is offered by Kapital Boost?</em></span></h3>
                                <div><span style="font-size: 18px; color: #3f3f3f;">Through crowdfunding, financiers offer capital to small businesses for the purchase of assets. This is a cost-plus profit arrangement (also known as Murabaha in Islamic Finance), wherein financiers buy assets for businesses at market value and immediately sell the asset to the business at a marked-up price on a deferred payment basis (see infographics on &lsquo;How it works&rsquo;). The typical funding period is 3-12 months depending on the SMEs cash flow strength and timing of receivable payments on existing orders.&nbsp;&nbsp;</span></div>
                                <div><span style="font-size: 20px;"><br />
                                    </span>
                                </div>
                                <div>
                                    <h3><span style="font-size: 20px;"><em>Who are eligible to get funding? What are the requirements?&nbsp;</em></span></h3>
                                    <div><span style="font-size: 20px; color: #3f3f3f;">T<span style="font-size: 18px;">o be eligible for funding from Kapital Boost, a business will need to fulfill the following requirements:&nbsp;</span></span></div>
                                    <div>
                                        <span style="font-size: 20px;">
                                            <span style="font-size: 18px; color: #3f3f3f;">
                                            </span>
                                            <ul>
                                                <li><span style="font-size: 18px; color: #3f3f3f;">At least one year of operation;</span></li>
                                                <li><span style="font-size: 18px; color: #3f3f3f;">Revenue of at least SGD100,000 in the past financial year;</span></li>
                                                <li><span style="font-size: 18px; color: #3f3f3f;">Positive free cash flow in the past year; and</span></li>
                                                <li><span style="font-size: 18px; color: #3f3f3f;">Preferably with receivables from existing orders or sales contracts with fixed payment terms.</span></li>
                                            </ul>
                                            <div><span style="font-size: 18px; color: #3f3f3f;">If you do not meet these requirements, please contact us to discuss how we can help you.</span></div>
                                            <div><br />
                                            </div>
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>How do I apply for funding? What information is required for the application?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">Click on 'Contact us' and provide us with information on your business and asset purchase needs. A Kapital Boost representative will contact you to request for additional information including company registration details, historical financial statements or bank records, and business plan or company presentation.&nbsp;&nbsp;</span></div>
                                    <div><span style="font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>How quickly can I get a Capital Boost funding?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">Between two to five weeks. This assumes five working days for Kapital Boost to conduct due diligence on the SME and a maximum of four weeks for the crowdfunding campaign.&nbsp;</span></div>
                                    <div><span style="font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>What are the terms of the deferred payments for asset purchase?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">Most deferred payments will be amortising on a monthly basis to reduce repayment risk for financiers. For businesses that prefer a bullet payment at the end of the contract period, the profit margin on the asset purchase is likely to be higher to account for the increased risk premium. &nbsp;</span></div>
                                    <div><span style="font-size: 18px; color: #3f3f3f;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>What differentiates Kapital Boost to other fund providers?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">Compared with most non-bank money lenders, we provide lower funding costs. Versus banks, we offer a higher approval rate, less administrative paperwork, and less restrictive covenants. Additionally, Kapital Boost&rsquo;s funding adheres to Shariah principles, which are ethical. The use of an asset purchase/cost-plus profit structure versus a loan increases the sharing of risk and reward between businesses and financiers.&nbsp;</span></div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <div>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>How much does Kapital Boost&rsquo;s charge for crowdfunding?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;"><span style="font-size: 18px;">A successful campaign will be charged an administrative fee of 5% of the total funding amount. A failed funding campaign will be charged a fixed fee of $50 to cover our administrative costs.&nbsp;</span>
                                        <span style="font-size: 18px;">The profit margin on asset sales will be determined by Kapital Boost with the business owner's consent, and will depend on the repayment period and risk profile of the business.&nbsp;</span>&nbsp;</span>
                                    </div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>Is there a penalty for early repayment?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">No. Kapital Boost encourages an accelerated payout (asset purchase) by businesses. </span><span style="font-size: 18px; color: #595959;">&nbsp;</span></div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>Is this type of funding secured?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">Yes and no. There is no collateral involved in this financing structure. However, assets purchased businesses will belong to the financiers until after they are fully paid for.&nbsp;</span></div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>Does Kapital Boost offer funding to SMEs outside Singapore?&nbsp;&nbsp;</em></span></h3>
                                    <div>
                                        <span style="font-size: 18px; color: #3f3f3f;">Yes, although at this time only to Indonesia and Malaysia. We intend to expand our services to other ASEAN countries over the medium term.
                                        </span>
                                        <p><strong><span style="font-size: 20px;"><br />
                                            </span></strong>
                                        </p>
                                        <h3><span style="font-size: 24px; color: #1f497d;">For financiers (members)</span></h3>
                                        <div style="font-weight: bold;"><span style="font-size: 20px;"><br />
                                            </span>
                                        </div>
                                        <span style="font-size: 16px;"></span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>What steps are taken to reduce the funding risk to financiers?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">Several steps are taken to reduce the risk to financiers. First, Kapital Boost screens for businesses with stronger credit profile to provide funding to. Second, Kapital Boost prioritises funding of businesses with existing sales receivables, of which the proceeds can be used to purchase contracted assets from financiers. And third, we believe the asset purchase funding model lowers the overall risk to financiers. The financing structure prevents businesses from using the funding for unintended purposes (e.g. for owner's personal use). The initial ownership of the assets by the financiers also offer some form of collateral for the funding.&nbsp;</span></div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>How does Kapital Boost screen businesses for funding eligibility?&nbsp;&nbsp;</em></span></h3>
                                    <div>
                                        <span style="font-size: 18px; color: #3f3f3f;">Businesses must meet the following requirements:
                                        </span>
                                        <ul>
                                            <li><span style="text-indent: -18pt; font-size: 18px; color: #3f3f3f;">At least one year of operation</span></li>
                                            <li><span style="text-indent: -18pt; font-size: 18px; color: #3f3f3f;">At least SGD100,000 of sales in the past year</span></li>
                                            <li><span style="text-indent: -18pt; font-size: 18px; color: #3f3f3f;">Positive free cash flow in the past year</span></li>
                                        </ul>
                                        <span style="font-size: 18px; color: #3f3f3f;"><span style="font-size: 18px;">Additionally, we have a credit risk scoring system based on the company&rsquo;s business profile, financial position, and corporate governance. On a risk scoring scale of 1 (lowest) to 10 (highest), we only allow businesses with a risk scale of below 3 to raise a crowdfunding campaign through Kapital Boost. We have also partnered with UK startup Friendly Score (www. friendlyscore.com)</span>&nbsp;to determine the credit-worthiness of the business owner based on his/her social media activities.&nbsp;</span>
                                    </div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>Can you provide more information on Kapital Boost&rsquo;s scoring system?&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">The three core areas we focus on are the company&rsquo;s business profile, financial position, and corporate governance.&nbsp;&nbsp;</span></div>
                                    <div><span style="font-size: 18px; color: #3f3f3f;"><br />
                                        </span>
                                    </div>
                                    <div>
                                        <span style="font-size: 18px; color: #3f3f3f;"><em>1. Business risk:</em>
                                        </span>
                                        <ul>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Industry profile (e.g. industry competitive landscape, profitability, life cycle/growth potential, economic sensitivity, and regulatory risk)</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Company profile (e.g. size &ndash; in terms of annual sales, historical profit, operating track record, differentiation)</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Management (e.g. experience, risk profile, risk mitigation)</span></li>
                                        </ul>
                                        <span style="font-size: 18px; color: #3f3f3f;"><em><span style="font-size: 18px;">2.&nbsp;</span><span style="font-size: 18px;">&nbsp;</span>Financial risk:</em>
                                        </span>
                                        <ul>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Cash flow (operating cash flow/interest, historical free cash flow)</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Operating performance (net income margin, sales/net profit growth)</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Capital structure (Total debt/EBITDA, total debt/assets)</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Liquidity (current ratio)</span></li>
                                        </ul>
                                        <span style="font-size: 18px; color: #3f3f3f;"><em>3. Corporate governance risk:</em>
                                        </span>
                                        <ul>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Credit score of largest shareholder</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Clarity of company strategy</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Availability of a business plan</span></li>
                                            <li><span style="font-size: 18px; color: #3f3f3f;">Availability of historical financial statements &ndash; audited or not</span></li>
                                        </ul>
                                        <span style="font-size: 18px; color: #3f3f3f;"><span style="font-size: 18px;">We give a score of 1 to 10 for each of these factors (1 being the least risky; 10 being the most risky). Based on weighted average score, we get a final credit risk score.&nbsp;</span>
                                        <span style="font-size: 18px;">Only businesses with a score of below 3 will be eligible to apply for financing through Kapital Boost.</span>&nbsp;</span>
                                    </div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>Should investors solely rely on Kapital Boost due diligence process?&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">No. While we utilize the Kapital Boost credit score to pre-screen all businesses and select the best for inclusion in our platform, we are not responsible for formal due diligence on the funding investments. The need and onus to do due diligence lies squarely with the financiers as we do not profess to advise on the same. All dealings and transactions are directly with businesses and financiers.&nbsp;</span></div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>What happens in a default?&nbsp;&nbsp;</em></span></h3>
                                    <div><span style="font-size: 18px; color: #3f3f3f;">In the event of a default, Kapital Boost will immediately engage with the business to find out the reason for the missed payment. We will attempt to find a solution which will satisfy the financiers and the business. If after eight weeks there is a failure to come to an agreement between the financiers and the business, and there is a continued default, the financiers may take legal actions to either force the payment of asset purchase by the business or to take back the assets. Note that Kapital Boost as a broker, will not be involved in the legal matters between the financier and the business.&nbsp;</span></div>
                                    <div><span style="text-decoration: underline; font-size: 20px;"><br />
                                        </span>
                                    </div>
                                    <h3><span style="font-size: 20px;"><em>How are the agreements between parties finalised?&nbsp;&nbsp;</em></span></h3>
                                    <div>
                                        <span style="font-size: 18px; color: #3f3f3f;">All contracts/agreements will be sent electronically to to the investors. Signing will also be done electronically using Echosign. Once signed, the contract will be binding under Singapore Law.&nbsp;
                                        </span>
                                        <p><span style="font-size: 16px;">&nbsp;</span></p>
                                    </div-->
                                </div>
<!--
                            </div>
                        </div>
                    </div>
                </div>
-->
   </div>
</center>
            </div>
</center>