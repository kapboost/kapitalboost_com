<style type="text/css">
  .btn-green {
    background: #27ae60;
    border: none;
    padding: 12px 20px;
    margin-top: 20px;
    color: white;
    min-width: 250px;
  }.btn-green:hover {
    background: #219050;
    color: white;
  }
</style>

{if !$member_id}
  {literal}
    <script>
      $(document).ready(function () {
        $("#close-box").hide();
        $("#lightbox").fadeIn(200);
        $("#lightbox").off("click");
      });
    </script>
  {/literal}
{else}
  {if $mismatched == '1'}
    <center>
      <div class="partnersWrap">
        <div class="partnersInner">
          <h1 style="margin-top :100px">You do not have permission to view this page!</h1>
        </div>
      </div>
    </center>
  {else}
    <center>
      <div class="partnersWrap">
        <div class="partnersInner" style="text-align: left;">
          {if $status=='Paid'}
            <legend style="margin-top: 80px;">{if $la == 'id'}{else}Your Payment Detail{/if}</legend>
            <div class="payItem">
              <div class="col-xs-12 col-md-6">
                {literal}<style>.texts,.monthForm{font-size:14px !important}</style>{/literal}
                <div class="form-group">
                  <label>{if $la == 'id'}{else}Name : {/if}</label>
                  <input type="text" name="fullname" value="{$nama}" class="form-control texts" readonly>
                </div>

                <div class="form-group">
                  <label>{if $la == 'id'}{else}Email Address : {/if}</label>
                  <input type="text" name="email" value="{$email}" class="form-control texts" readonly>
                </div>

                <div class="form-group">
                  <label>{if $la == 'id'}{else}Country : {/if}</label>
                  <input type="text" name="country" value="{$country}" class="form-control texts" readonly>
                </div>

                <div class="form-group">
                  <label>{if $la == 'id'}{else}Campaign Name : {/if}</label>
                  <input type="text" name="campaign_name" value="{$campaign}" class="form-control texts" readonly>
                </div>
              </div>

              <div class="col-xs-12 col-md-6">
                <div class="form-group">
                  <label>{if $la == 'id'}{else}Payment Type : {/if}</label>
                  <input type="text" name="tipe" value="{$tipe}" class="form-control texts" readonly>
                </div>
                <div class="form-group">
                  <label>{if $la == 'id'}{else}Total Funding : {/if}</label>
                  <input type="text" name="t_funding" value="{$total_funding}" class="form-control texts" readonly>
                </div>
                <div class="form-group">
                  <label>{if $la == 'id'}{else}Payment Create Date : {/if}</label>
                  <input type="text" name="date" value="{$date}" class="form-control texts" readonly>
                </div>
                <div class="form-group">
                  <label>{if $la == 'id'}{else}Payment Status:{/if}<br /> </label>
                  {if $status=='Paid'}
                    <label class="label label-success">{if $la == 'id'}{else} Paid {/if}</label>
                  {else}
                    <label class="label label-warning">{if $la == 'id'}{else} Unpaid {/if}</label>
                  {/if}
                </div>
              </div>
            </div>
            <br /><br />
            <legend style="margin-top: 0;">{if $la == 'id'}{else}Payout schedule{/if}</legend>
            <div class="month">
              <div class="container-fluid">
                <div class="row">
                  {if $bulan_1 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 1 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_1|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_1|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_1 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[0] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[0]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}

                  {if $bulan_2 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 2 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_2|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_2|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_2 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[1] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[1]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}
                </div>

                <div class="row">
                  {if $bulan_3 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 3 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_3|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_3|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_3 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[2] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[2]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}

                  {if $bulan_4 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 4 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_4|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_4|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_4 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p> 
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[3] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[3]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}
                </div>

                <div class="row">
                  {if $bulan_5 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 5 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_5|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_5|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_5 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[4] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[4]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}

                  {if $bulan_6 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 6 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_6|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_6|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_6 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[5] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[5]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}
                </div>

                <div class="row">
                  {if $bulan_7 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 7 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_7|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_7|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_7 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[6] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[6]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}

                  {if $bulan_8 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 8 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_8|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_8|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_8 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[7] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[7]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}
                </div>


                <div class="row">
                  {if $bulan_9 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 9 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_9|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_9|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_9 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[8] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[8]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}


                  {if $bulan_10 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 10 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_10|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_10|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_10 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[9] != ''}
                        <div style="">
                          <p class="proofText">Proof of payout</p>
                          <img src="/assets/images/payouts/{$images[9]}" width="100%">
                        </div>
                        {/if}
                      </div>
                    </div>
                  {/if}
                </div>

                <div class="row">
                  {if $bulan_11 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 11 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_11|number_format:0}</p>
                          </div> 
                          <div class="monItem2">
                            <p class="monText">{$tanggal_11|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_11 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[10] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[10]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}

                  {if $bulan_12 > 0}
                    <div class="col-xs-12 col-md-6">
                      <div class="payoutDivs">
                        <label>Payout 12 : </label>
                        <div class="monContent">
                          <div class="monItem2">
                            <p class="monText">$ {$bulan_12|number_format:0}</p>
                          </div>
                          <div class="monItem2">
                            <p class="monText">{$tanggal_12|date_format:"%d/%m/%Y"}</p>
                          </div>
                          {if $status_12 == '2'}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-success">Paid</label>
                              </p>
                            </div>
                          {else}
                            <div class="monItem2">
                              <p class="monText">
                                <label class="label label-default">On Going</label>
                              </p>
                            </div>
                          {/if}
                        </div>
                        {if $images[11] != ''}
                          <div style="">
                            <p class="proofText">Proof of payout</p>
                            <img src="/assets/images/payouts/{$images[11]}" width="100%">
                          </div>
                        {/if}
                      </div>
                    </div>
                  {/if}
                </div>

              </div>
            </div>
          {else}

            {if $tipe == 'Bank Transfer'}
              <center class="TYInner">
                <p class="thanks-title">
                  {if $la == 'id'}{else}Thank you for your{/if} {if $project_type == 'donation'}{if $campaign_subtype_chosen == 'interest'}{if $la == 'id'}{else}interest-free loan{/if}{else}{if $la == 'id'}{else}donation{/if}{/if}{else} {if $la == 'id'}{else}commitment{/if}{/if} {if $la == 'id'}{else}of{/if} <br> 
                  <b>SGD {$total_funding} {if $member_country == 'INDONESIA'}(IDR {$total_funding_id}){/if}</b>{if $la == 'id'}{else} in the <b>{$campaign}</b> campaign.{/if}
                </p>
                <br>
                <p class="TYText">
                  To finalize the investment, you are required to complete the following:
                </p>
                <ul style="list-style-type:none;padding: 0;">
                  <li>
                    (1) Review and digitally sign the Investor Agreement which will be sent to your email within one (1) working day AND
                  </li>
                  <li>
                    (2) Make payment for your commitment amount
                  </li>
                </ul>
                <br>
                <h4 style="font-size: 16px;font-weight: bold;">
                  (i) Payment method - Bank transfer
                </h4>
                <div style="padding-left: 20px;">
                  <p>
                    Please make a transfer to the following account:
                  </p>

                  {if $member_country == 'INDONESIA'}
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Account name{/if}</div><b>: PT Kapital Boost Indonesia</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Bank name{/if} </div><b>: Bank Permata Syariah</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Account number{/if} </div><b>: 007 0178 9040</b></div>
                  {else}
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Account name{/if}</div><b>: Kapital Boost Pte Ltd</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Bank name{/if} </div><b>: CIMB Bank Berhad, Singapore</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Account number{/if} </div><b>: 2000 562 641</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}SWIFT code{/if}</div><b>: CIBBSGSG</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Bank code{/if}</div><b>: 7986</b></div>
                    <div><div style="width:150px;display:inline-block">{if $la == 'id'}{else}Branch code{/if}</div><b>: 001</b></div>
                  {/if}
                  
                  <p style="margin-top: 15px;">
                    {if $la == 'id'}{else} Once transferred, please click below to submit the proof of payment {/if}
                  </p>
                </div>

                <form action="/dashboard/confirm/bank" method="GET">
                  <input type="hidden" name="type" value="{$campaign_type}" />
                  <input type="hidden" name="campaign_id" value="{$campaign_id}" />
                  <input type="hidden" name="amount" value="{$total_funding}" />
                  <input type="hidden" name="campaign_name" value="{$campaign}" />
                  <input type="hidden" name="token" value="{$token_pp}" />
                  
                  <div style="padding-left: 20px;margin-bottom: 35px;">
                    <input type="submit" style="margin:20px 0px;" value="{if $la == 'id'}{else}Attach transfer slip{/if}" />
                  </div>
                  
                  <h4 style="font-size: 16px;font-weight: bold;">
                    (ii) Payment method - Pay with Wallet*
                  </h4>

                  <div style="padding-left: 20px;">
                    {if $pay_with_wallet}
                      <a href="/dashboard/pay-with-wallet/{$order_id}" class="btn btn-green" style="margin-top: 10px;">Pay With Wallet</a>
                    {else}
                      <button type="button" class="btn btn-green" style="margin-top: 10px;" data-toggle="modal" data-target="#myModal">
                        Pay With Wallet
                      </button>
                    {/if}
                    <p style="font-size: 12px;font-weight: thin;font-style: italic;margin-top: 5px;">
                      *This option is only available for non-Singaporean investors intending to re-invest their payouts
                    </p>
                    <!-- <div class="row"> -->
                      <!-- <div class="col-md-4">
                        <input type="submit" style="margin:20px 0px;" value="{if $la == 'id'}{else}Attach transfer slip{/if}" />
                      </div> -->
                      <!-- <div class="col-md-6">
                        {if $pay_with_wallet}
                          <a href="/dashboard/pay-with-wallet/{$order_id}" class="btn btn-green">Pay With Wallet</a>
                        {else}
                          <button type="button" class="btn btn-green" data-toggle="modal" data-target="#myModal">
                            Pay With Wallet
                          </button>
                        {/if}
                        <p>
                          *This option is only available for non-Singaporean investors intending to re-invest their payouts
                        </p>
                      </div> -->
                    <!-- </div> -->
                  </div>

                  <!-- Modal -->
                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-body" align="center">
                          <img src="/assets/icon/payfailed.png" width="300">
                          <h2 style="font-size: 28px;margin-top: 15px;margin-bottom: 40px;">
                            Sorry, you have insufficient funds in your e-wallet.
                          </h2>
                          <button type="button" class="btn btn-default btn-xs" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>

                <!-- <p> -->
                  <!-- {if $la == 'id'}{else}If you wish to complete this step later, you may refer to the email sent to you and follow the instructions provided.{/if} -->
                <!-- </p> -->

                <hr >
                <!-- <p class="TYText2">
                  {if $project_type == 'donation'}
                    {if $la == 'id'}{else}Spread the goodness and let your friends and family know by sharing this campaign{/if}
                  {else}
                    {if $la == 'id'}{else}Help support the campaign by letting others know{/if}
                  {/if}
                </p> -->
                <ul class="TYLink">
                  <li>
                    <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/{$id}">
                      <img src="assets/images/linkGoogle.jpg" />
                    </a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/{$id}">
                      <img src="assets/images/linkFacebook.jpg" />
                    </a>
                  </li>
                  <li>
                    <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/{$id}" target="_blank">
                      <img src="assets/images/linkTwitter.jpg" />
                    </a>
                  </li>
                  <li>
                    <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/{$id}">
                      <img src="assets/images/linkLinkedIn.jpg" />
                    </a>
                  </li>
                </ul>
              </center>

            {elseif $tipe == 'Xfers' || $tipe == 'xfers' }
              {if $member_country == 'INDONESIA'}
                <form name="xfersForm" action="xfers_indo/" method="post">
                  <input type="hidden" name="currency" value="IDR">
                  <input type="hidden" name="amount" value="{$total_funding_id}">
              {else}
                <form name='xfersForm' action="xfers/" method="post">
                  <input type="hidden" name="currency" value="SGD">
                  <input type="hidden" name="amount" value="{$total_funding}">
              {/if}
                <input type="hidden" name="xfersphone" value="{$phone}">
                <input type="hidden" name="member_email" value="{$member_email}">
                <input type="hidden" name="order_id" value="{$order_id}">
                <input type="hidden" name="username" value="{$username}">
                <input type='hidden' name='member_id' value='{$member_id}' >
                <input type="hidden" name="member_country" value='{$member_country}' >
                <input type="hidden" name="lastname" value="{$lastname}">
                <input type="hidden" name="item_name" value="{$campaign}">
                <input type="hidden" name="item_item_id_1" value="1">
                <input type="hidden" name="item_description_1" value="Kapitalboost Campaign">
                <input type="hidden" name="item_quantity_1" value="1">
                <input type="hidden" name="project_type" value="{$project_type}">
                <input type="hidden" name="campaign_id" value="{$campaign_id}">
                <input type="hidden" name="campaign_token" value="{$token_pp}">
              </form>

              <h2 style="text-align:center;margin:100px">Redirecting in a second</h2>
              <div class="roundloader"></div>
              {literal}
                <script>
                  window.onload = function () {
                    document.forms['xfersForm'].submit();
                  }
                </script>
              {/literal}

            {else} <!-- Paypal -->
              <form name="paypalForm" action="https://www.paypal.com/cgi-bin/webscr" method="POST">
                <!-- Params FOr Paypal -->
                <input type="hidden" name="business" value="erly@kapitalboost.com">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="item_name" value="{$campaign}">
                <input type="hidden" name="return" value="https://kapitalboost.com/payment">
                <input type="hidden" name="item_number" value="{$campaign_id}">
                <input type="hidden" name="credits" value="510">
                <input type="hidden" name="tx" value="TransactionID">
                <input type="hidden" name="at" value="ODJuzlFwX-WNtbrnJhd_EB33SXbFXYt7xkWb8AGPGQp-eqi5l0wKkTr8it0">
                <input type="hidden" name="cpp_header_image" value="https://kapitalboost.com/assets/images/logo.png">
                <input type="hidden" name="no_shipping" value="1">
                <input type="hidden" name="currency_code" value="SGD">
                <input type="hidden" name="handling" value="0">
                <input type="hidden" name="amount" value="{$total_fundpaypal}">
                <!-- value="campaign_name | $username | $member_email | $member_country | campaign_owner | token | campaign_type| closing_date" -->
                <input type="hidden" name="custom" value="{$campaign}|{$username}|{$member_email}|{$member_country}|campaign_owner1|{$token_pp}|{$project_type}">
                <input type="hidden" name="campaign_id" value="{$campaign_id}">
              </form>
              <h2 style="text-align:center;margin:100px">Redirecting in a second</h2>
              <div class="roundloader"></div>

              {literal}
                <script>
                  window.onload = function () {
                    document.forms['paypalForm'].submit();
                  }
                </script>
              {/literal}
            {/if}

          {/if}
        </div>
      </div>
    </center>
  {/if}
{/if}