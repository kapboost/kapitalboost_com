<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="/xfers/assets/css/gaya.css">

<center>
       <div>
              <img alt="Kapital Boost Logo" id="menu-logo-img" src="assets/images/logo-baru.png" style="margin-top: 10px;" />
       </div>
       <div class="xfersThankYou" style="margin:5px 0 0 0;width:100%;height:640px;">

              <center>
                     <form id='form' method="post" name="login_form" action="/dashboard/payment/bank-thankyou" autocomplete="off" enctype="multipart/form-data" class="CPay" style="margin-top: 70px;">
                            <div class="FPInner">
                                   <p class="FPTitle">
                                          Please submit your payment detail to Kapital Boost
                                   </p>
                                   <p>
                                          {$bank_campaign_name} Campaign
                                   </p>
                                   <input type="text" name="bank_name" id='bank_name' placeholder="Bank name" value="" required>
                                   <input type="text" name="account_name" id='account_name' placeholder="Account holder’s name" value="" required>
                                   <input type="text" name="account_number" id='account_number' placeholder="Bank account number" value="" required>
                                   <input type="hidden" name="type" value="{$bank_type}">
                                   <input type="hidden" name="campaign_id" value="{$bank_campaign_id}">
                                   <input type="hidden" name="amount" value="{$bank_amount}">
                                   <p style="margin-top:20px">Transaction Receipt</p>
                                   <input type="file" name="bukti" id='receipt' value="" required style="margin-top:5px">
                                   <input type="hidden" name="action" value="{$bank_token}" />
								   <br />
								   <div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
								   <br />
                                   <button id='subBtn' style="margin-top: 20px;">
                                          Submit
                                   </button>
                            </div>
                     </form>
              </center>
       </div>
</center>
{literal}
       <script>
              var uploaded = 0;
              $(document).ready(function () {

                     $("#receipt").on("change", function () {
                            uploaded = 1;
                            console.log(uploaded);
                     });

                     $("#subBtn").on("click", function () {
                            if ($("#bank_name").val() == "") {
                                   alert("Please fill in bank name");
                                   return;
                            }
                            if ($("#account_name").val() == "") {
                                   alert("Please fill in account holder's name");
                                   return;
                            }
                            if ($("#account_number").val() == "") {
                                   alert("Please fill in bank account number");
                                   return;
                            }
                            if (uploaded == 0) {
                                   alert("Please upload transaction receipt");
                                   return;
                            }
                            $("#form").submit();

                     });

              });

       </script>
{/literal}


