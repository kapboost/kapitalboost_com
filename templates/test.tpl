<html>
                            <head>
                              <title>Kapital Boost</title>
                            </head>
                            <style type='text/css'>
                            @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
                            a {
                              text-decoration: none;
                            }
                            a:hover {
                              text-decoration: none;
                              cursor: pointer;
                            }
                            button {
                              cursor: pointer;
                            }
                            button:hover {
                              cursor: pointer;
                            }
                            </style>
                            <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;font-family: 'Open Sans';'>
                            <table height='100%' width='100%' bgcolor='#305e8e'>
                            <tbody>
                            <tr>
                            <td style='height: 100%;width: 100%;padding: 20px 10%;' bgcolor='#305e8e'>
                            <center>
                              <div style='display: inline-block;position: relative;height: auto;width: 640px;padding: 0 0 10px 0;margin: 0 0 0 0;background: #00b0fd;'>
                                <div style='display: inline-block;position: relative;height: auto;width: 100%;padding: 0;margin: 0;background: white;text-align: left;'>
                                  <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 35px 0 35px;' />
                                  <p style='display: block;font-size: 24px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                                    Hi Owen,
                                  </p>
                                  <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 30px 35px;'>
                                    Your account is almost ready! To activate, please click below.
                                  </p>
                                  <a href='https://kapitalboost.com/verify/206c3cd5c1f77dac353ee334097db7ee1492071289' style='position: relative;background: #00b0fd;outline: none;border: none;padding: 15px 30px;margin: 0 0 10px 35px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;'>
                                    ACTIVATE MY ACCOUNT
                                  </a>
                                  <p style='display: block;font-size: 18px;text-align: left;margin: 30px 35px 35px 35px;'>
                                    If you have any questions, feel free to contact us at <a href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
                                  </p>
                                  <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
                                    Best regards,<br />
                                    Kapital Boost team
                                  </p>
                                </div>
                              </div>
                              <div style='display: inline-block;position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                                <p style='font-size: 12px;color: white;'>
                                  &copy; COPYRIGHT 2017 - KAPITAL BOOST PTE LTD
                                </p>
                              </div>
                            </center>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                            </body>
                            </html>
                            