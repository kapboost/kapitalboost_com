<link rel="stylesheet" type="text/css" href="/xfers/assets/css/gaya.css">

<div class="partnersWrap" ng-app="xfers" ng-controller="xfersCtrl">
       <div class="partnersInnerDas" style="width: 100%; margin: 0px">
              <center>

                     <!-- start xfers otp -->
                     <div class="xfersOtp">
                            <div class="XOInner">
                                   <div class="XOLogo">
                                          <img src="/xfers/assets/img/xfersLogo.png" />
                                   </div>
                                   <div class="XOTitle">
                                          <p>
                                                 Account Verification
                                          </p>
                                   </div>
                                   <p style="margin: 10px 30px">
                                          Please enter the 6-digit OTP that was sent via SMS to your mobile number reigstered with Kapital Boost.
                                   </p>
                                   <div class="XOForm">
                                          <form ng-submit="xfersToken()">
                                                 <input type="text" value="{$error} "ng-model="otp" />
                                                 <button>
                                                        Verify
                                                 </button>
                                          </form>
                                   </div>
                            </div>
                     </div>
                     <!-- end x fers otp -->
              </center>

              </center>
       </div>
</div>
{literal}
       <script src="/xfers/assets/js/angular.min.js"></script>
       <script>
              var activate = window.location.hash.substring(1);
              var member_id = '{/literal}{$mid}{literal}';
              console.log(activate);
              if (activate != "Activate") {
                     spge = "OTP wrong, please fill the correct OTP again!";
                     alert(spge);
              }

       </script>

       <script type="text/javascript">

              var app = angular.module("xfers", []);
              app.controller("xfersCtrl", function ($scope, $http) {
                     $scope.otp = "";


                     $scope.xtoken = {};
                     $scope.xballance = {};
                     $scope.xcheckout = {};
                     $scope.params = {};
                     $scope.currentUser = {};

                     var chargeparams = function () {
                            return $scope.params = JSON.parse(localStorage.getItem('kapitaluser'));
                     };
                     chargeparams();
                     var getToken = function () {
                            return $scope.xtoken = JSON.parse(localStorage.getItem('xferstoken'));
                     };
                     getToken();
                     var getBallance = function () {
                            return $scope.xballance = JSON.parse(localStorage.getItem('userBallance'));
                     };
                     getBallance();
                     var getCheckout = function () {
                            return $scope.xcheckout = JSON.parse(localStorage.getItem('xferscheckout'));
                     };
                     getCheckout();
                     var getPhone = function () {
                            return $scope.currentphone = JSON.parse(localStorage.getItem('user'));
                     };
                     var getCountry = function () {
                            return $scope.cCountry = JSON.parse(localStorage.getItem('country'));
                     };

                     $scope.xfersToken = function () {
                            getPhone();
                            var country = getCountry().country;
                            if (country === 'SINGAPORE') {
                                   var url = "/xfers/apis/token.php";
                            } else {
                                   var url = "/xfers_indo/apis/token.php";
                            }

                            $http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "xfersotp": $scope.otp,
                                          "xfersphone": $scope.currentphone.phone
                                   }
                            }).success(function (res) {
                                   localStorage.setItem('xferstoken', JSON.stringify(res));
                                   if (activate == "Activate") {
                                          window.location = "/dashboard/payment/xfers-transferinfo";
                                   } else {
                                          window.location = "/dashboard/payment/xfers-ballance?";
                                   }
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });
                     }
                     // Xfers TOken

                     var xfersOTP = function () {
                            getPhone();
                            var country = getCountry().country;
                            if (country === 'SINGAPORE') {
                                   var url = "/xfers/apis/sendOTP.php";
                            } else {
                                   var url = "/xfers_indo/apis/sendOTP.php";
                            }

                            $http({
                                   method: 'POST',
                                   url: url,
                                   headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                   transformRequest: function (obj) {
                                          var str = [];
                                          for (var p in obj)
                                                 str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                                          return str.join("&");
                                   },
                                   data: {
                                          "sendotp": "AASSLLKKKAS",
                                          "xfersphone": $scope.currentphone.phone
                                   }
                            }).success(function (res) {
                                   localStorage.setItem('xfersotp', JSON.stringify(res));
                            }).error(function (err) {
                                   alert("No Internet Connection");
                            });
                     }
                     // Xfers send token OTP
                     xfersOTP();
              });
       </script>
{/literal}