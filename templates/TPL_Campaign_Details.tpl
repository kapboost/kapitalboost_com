{if !$member_id && $content.project_type != 'donation' && $content.campaign_id != 75}
<script>
{literal}

$(document).ready(function () {
   $("#close-box").hide();
   $("#lightbox").fadeIn(200);
   $("#lightbox").off("click");
});

{/literal}
</script>
{/if}
{if $member_id || $error_msg=="permit" || $content.project_type=='donation' || $content.campaign_id == 75}
<link rel="stylesheet" href="assets/css/new-kp-campaigns.css" />

<div id="fade"></div>
<div id="modal">
   <div class="content-loading">
      <h1>Your investment is being processed</h1>
      <!-- <h3>Please Wait..</h3> -->
      <img id="loader" src="/assets/gif/ellipsis.gif" />
   </div>
</div>

<input type="hidden" class="ind-total-amount" value="{$content.total_funding_amt}" />
<input type="hidden" class="ind-curr-amount" value="{$content.curr_funding_amt}" />
<div id="campaigns-detail">
   <div id="campaigns-detail-1" style="background-image:url('../assets/images/campaign/{$content.campaign_background}')">
      <div style="background-color: rgba(255,255,255,0.6);">
         <div id="campaigns-detail-1-inner">
            <div id="campaigns-detail-1-inner-left">
               {if $content.campaign_logo}
               <img id="campaign-logo" src="../assets/images/campaign/{$content.campaign_logo}" />
               {/if}
            </div>
            <div id="campaigns-detail-1-inner-right">
               <div class="campaigns-detail-1-para">
                  <h1 class="campaigns-detail-h1" id="campaign-name">
                     {if $content.acronim != ""}
                     {$content.acronim}
                     {else}
                     {$content.campaign_name}
                     {/if}
                  </h1>
               </div>
               <p class="campaigns-company">{if $la=='id'}{$content[outer].classification_id}{else}{$content.classification}{/if}</p>
               <div class="campaigns-detail-1-para detail-1-para-middle">
                  <div class="cam-country">
                     <img class="cam-marker" src="../assets/images/marker.png">
                     <p class="cam-country-name">{$content.country}</p>
                  </div>
                  <div class="cam-industry">
                     {if $content.project_type=='donation'}
                     <img class="cam-marker" src="../assets/images/donate-icon.png" style="width:20px" />
                     {else}
                     <img class="cam-marker" src="../assets/images/industry-icon.png">
                     {/if}
                     <p class="cam-industry-name">{$content.industry}</p>
                  </div>
               </div>
            </div>
            <div class="invest-now-btn-div">
               {if $content.days_left <= 0}
               <a  class="silverbtn invest-btn invest-now-btn" style="font-weight: none; color:#224f77">Closed</a>
               {else if $content.project_type=='private' && $closedPw != 'correct'}
               <a  class="silverbtn invest-btn invest-now-btn" style="font-weight: none; color:#224f77">{if $la=='id'}{else}Disallowed{/if}</a>

               {else}
               {if $content.project_type=='donation'}
               <div id="cndtext" class="comingsoon"><h2> Campaign launches in <br><span id="cndtime"></span></h2></div>
               <a data-toggle="modal" id="invbtn" class="greenbtn invest-btn investOpen invest-now-btn" style="font-weight: none;">{if $la=='id'}{else}donate now{/if}</a>
               {else}
               {if $member_status != 2}

               <a  class="silverbtn invest-btn invest-now-btn" style="font-weight: none; color:#224f77">{if $la=='id'}{else}Sorry, your account is still under review{/if}</a>
               {else}
               <div id="cndtext" class="comingsoon"><h2> Campaign launches in <br><span id="cndtime"></span></h2></div>
               <a data-toggle="modal" id="invbtn" class="bluebtn invest-btn investOpen invest-now-btn" style="font-weight: none;">{if $la=='id'}{else}invest now{/if}</a>
               {/if}
               {/if}

               {/if}
            </div>
         </div>
      </div>
   </div>

   <div id="campaigns-detail-2">
      <div id="campaigns-detail-2-inner">
         <div id="campaigns-detail-2-inner-left" class="leftRight textCenter">
            {if ($content.days_left <= 0 && $closedPw != 'correct' && $content.project_type != 'donation') || ($content.project_type=='private' && $closedPw != 'correct')}
            {if $la=='id'}{else}{$content.campaign_description|truncate}{/if}<br><br><br>
            {else}
            {if $la=='id' && $content.campaign_description_id != ''}{$content.campaign_description_id}{else}{$content.campaign_description}{/if}<br><br><br>
            {/if}
         </div>
         <div id="campaigns-detail-2-inner-right">
            <div class="stats-box">
               <div id="stats-box">
                  <h2 class="campaigns-detail-h2">{if $la=='id'}{else}Overview{/if} </h2>
                  <ul id="stats-list">
                     <li>
                        <div class="campaigns-bar">
                           {if $content.project_type == 'donation'}
                           <div class="campaigns-bar-inner-green campaigns-bar-inner "></div>
                           {else}
                           <div class="campaigns-bar-inner "></div>
                           {/if}
                        </div>
                     </li>
                     <li>
                        <p class="stats-title">{if $la=='id'}{else}Target funding{/if}</p>
                        <p class="stats-num total-amount"><span style="float:right">S$ {$content.total_funding_amt|number_format}</span>{if $current_location === 'INDONESIA'}<br><span style="font-size:12px">
                           <!-- (IDR {$content.total_funding_amt_id|number_format}) -->
                        </span>{/if}
                     </p>
                  </li>
                  <li>
                     <p class="stats-title">{if $la=='id'}{else}Funded{/if}</p>
                     <p class="stats-num"><span style="float:right">S$ {$content.curr_funding_amt|number_format}</span>{if $current_location === 'INDONESIA'}<br><span style="font-size:12px">
                        <!-- (IDR {$content.curr_funding_amt_id|number_format}) -->
                     </span>{/if}
                  </p>
               </li>
               <li>
                  <p class="stats-title">{if $la=='id'}{else}Min investment{/if}</p>
                  <p class="stats-num curr-amount">S$ {if $current_location === 'INDONESIA' && $content.minimum_investment != 0} {$content.minimum_investment|number_format} {else}{$content.minimum_investment|number_format} {/if}
                  </p>
               </li>
               <li>
                  <p class="stats-title">{if $la=='id'}{else}Returns{/if}</p>
                  <p class="stats-num">{$content.project_return}%</p>
               </li>
               {if $content.days_left <= 0}
               <li>
                  <p class="stats-title">{if $la=='id'}{else}Days left{/if}</p>
                  <p class="stats-num">{if $la=='id'}{else}0, Closed{/if}</p>
               </li>
               {else}
               <li>
                  <p class="stats-title">{if $la=='id'}{else}Days left{/if}</p>
                  <p class="stats-num">{$content.days_left}</p>
               </li>
               {/if}

               <li>
                  <p class="stats-title">{if $la=='id'}{else}Tenor{/if}</p>
                  <p class="stats-num">{$content.funding_summary}</p>
               </li>
            </ul>
         </div>
         <center>
            {if $content.days_left <= 0}
            <a class="silverbtn invest-btn" style="font-weight: none;color:#224f77">{if $la=='id'}{else}closed{/if}</a>
         </div>
         {else if $content.project_type=='private' && $closedPw != 'correct'}
         <a class="silverbtn invest-btn" style="font-weight: none;color:#224f77">{if $la=='id'}{else}Disallowed{/if}</a>
      </div>
      {else}


   </div>
   <center>
      {if $content.project_type=='donation'}

      <div id="cndtext2" class="comingsoon2"><h2> Campaign launches in <br><span id="cndtime2"></span></h2></div>

      <a id="secondInvest" data-toggle="modal" class="greenbtn invest-btn investOpen" style="width:200px;padding-left:0;padding-right:0">{if $la=='id'}{else}donate now{/if}</a>

      <a data-toggle="modal" class="greenbtn invest-btn enquireOpen" style="font-weight: none;margin-top: -15px;padding-right: 0;padding-left: 0;width:200px;margin-left: 4px">{if $la=='id'}{else}Enquire{/if}</a>

      {else}
      <div id="cndtext2" class="comingsoon2"><h2> Campaign launches in <br><span id="cndtime2"></span></h2></div>
      <a id="secondInvest" data-toggle="modal" class="bluebtn invest-btn investOpen" style="width:200px;padding-left:0;padding-right:0">{if $la=='id'}{else}invest now{/if}</a>

      <a data-toggle="modal" class="bluebtn invest-btn enquireOpen" style="margin-top: -15px;padding-right: 0;padding-left: 0;width:200px;margin-left:4px">{if $la=='id'}{else}Enquire{/if}</a>

      {/if}
      <br/>
      {/if}
   </center>
   <div id="">
      <div id="gallery-box">
         <h2 class="campaigns-detail-h2">{if $la=='id'}{else}Gallery{/if}</h2>
         {if ($content.days_left <= 0 && $closedPw != 'correct'  && $content.project_type != 'donation') || ($content.project_type == 'private' && $closedPw != 'correct')}
         {else}
         <ul id="gallery-list">
            {if $content.image_1!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_1}')" data-img-url="assets/images/campaign/{$content.image_1}" data-img-name="{$image_name[0]}"  id="481"></li>
            {/if}
            {if $content.image_2!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_2}')" data-img-url="assets/images/campaign/{$content.image_2}" data-img-name="{$image_name[1]}" id="482"></li>
            {/if}
            {if $content.image_3!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_3}')" data-img-url="assets/images/campaign/{$content.image_3}" data-img-name="{$image_name[2]}" id="483"></li>
            {/if}
            {if $content.image_4!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_4}')" data-img-url="assets/images/campaign/{$content.image_4}" data-img-name="{$image_name[3]}" id="484"></li>
            {/if}
            {if $content.image_5!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_5}')" data-img-url="assets/images/campaign/{$content.image_5}" data-img-name="{$image_name[4]}" id="485"></li>
            {/if}
            {if $content.image_6!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_6}')" data-img-url="assets/images/campaign/{$content.image_6}" data-img-name="{$image_name[5]}" id="486"></li>
            {/if}
            {if $content.image_7!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_7}')" data-img-url="assets/images/campaign/{$content.image_7}" data-img-name="{$image_name[6]}" id="487"></li>
            {/if}
            {if $content.image_8!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_8}')" data-img-url="assets/images/campaign/{$content.image_8}" data-img-name="{$image_name[7]}" id="488"></li>
            {/if}
            {if $content.image_9!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_9}')" data-img-url="assets/images/campaign/{$content.image_9}" data-img-name="{$image_name[8]}" id="489"></li>
            {/if}
            {if $content.image_10!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_10}')" data-img-url="assets/images/campaign/{$content.image_10}" data-img-name="{$image_name[9]}" id="490"></li>
            {/if}
            {if $content.image_11!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_11}')" data-img-url="assets/images/campaign/{$content.image_11}" data-img-name="{$image_name[10]}" id="491"></li>
            {/if}
            {if $content.image_12!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_12}')" data-img-url="assets/images/campaign/{$content.image_12}" data-img-name="{$image_name[11]}" id="492"></li>
            {/if}
            {if $content.image_13!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_13}')" data-img-url="assets/images/campaign/{$content.image_13}" data-img-name="{$image_name[12]}" id="493"></li>
            {/if}
            {if $content.image_14!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_14}')" data-img-url="assets/images/campaign/{$content.image_14}" data-img-name="{$image_name[13]}" id="494"></li>
            {/if}
            {if $content.image_15!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_15}')" data-img-url="assets/images/campaign/{$content.image_15}" data-img-name="{$image_name[14]}" id="495"></li>
            {/if}
            {if $content.image_16!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_16}')" data-img-url="assets/images/campaign/{$content.image_16}" data-img-name="{$image_name[15]}" id="496"></li>
            {/if}
            {if $content.image_17!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_17}')" data-img-url="assets/images/campaign/{$content.image_17}" data-img-name="{$image_name[16]}" id="497"></li>
            {/if}
            {if $content.image_18!=""}
            <li style="background-image:url('../assets/images/campaign/{$content.image_18}')" data-img-url="assets/images/campaign/{$content.image_18}" data-img-name="{$image_name[17]}" id="498"></li>
            {/if}
         </ul>
         {/if}
         <div class="galSticky"></div>
      </div>
   </div>

   <div>
      {if $pdfsArray[0] || $pdfsArray[1] || $pdfsArray[2] || $pdfsArray[3] || $pdfsArray[4] || $pdfsArray[5] || $pdfsArray[6] || $pdfsArray[7]}
      <h2 class="campaigns-detail-h2">{if $la=='id'}{else}Documents{/if}</h2>

      {/if}
      <ul>
         {if ($content.days_left <= 0 && $closedPw != 'correct' && $content.project_type != 'donation') || ($content.project_type == 'private' && $closedPw != 'correct')}
         {else}
         {section name=number loop=$pdfsArray}
         {if $pdfsArray[number]}
         <input type="hidden" value="{$pdfsDesArray[number]}" />
         <input type="hidden" value="{$pdfsArray[number]}"/>
         <li style="float:left"><a href="https://kapitalboost.com/assets/images/campaign-pdf/{$pdfsArray[number]}" target="_blank">{$pdfsDesArray[number]}</a></li>
         <br>
         {/if}
         {/section}
         {/if}
         <br/>
         <br/>
      </ul>
   </div>
</div>
</div>
</div>
<div id="campaigns-detail-3">
   <div id="campaigns-detail-3-inner">
      <h2 class="con-h2" id="OPSticky">{if $la=='id'}{else}Other Projects{/if}</h2>

      <ul id="campaigns-list">
         {section name=outer loop=$other1}
         {if $member_id}
         <li onclick="window.location.href = 'campaign/view/{$other1[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
            {else}
            <li id="eth-log" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
               {/if}
               <div class="campaigns-list-con">
                  <input type="hidden" class="total-amount" value="{$other1[outer].total_funding_amt}" />
                  <input type="hidden" class="curr-amount" value="{$other1[outer].curr_funding_amt}" />

                  <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$other1[outer].campaign_background}')"></div>
                  <div class="campaigns-list-middle">
                     <h4 class="campaigns-title">{$other1[outer].campaign_name}</h4>
                     <span class="campaigns-company">{if $la=='id'}{$other1[outer].classification_id}{else}{$other1[outer].classification}{/if}</span>
                     <div class="campaigns-desc">
                        {if $la=='id'}{$other1[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$other1[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}
                     </div>

                     {if $member_id}
                     <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$other1[outer].campaign_id}">{if $la=='id'}{else}read more{/if}</a>
                     </p>
                     {else}
                     <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">{if $la=='id'}{else}read more{/if}</a>
                     </p>
                     {/if}
                  </div>
                  <div class="campaigns-list-bottom">
                     <div class="campaigns-list-bottom-1">
                        <div class="campaigns-country">
                           <img class="campaigns-marker" src="../../assets/images/marker.png" />
                           <p class="campaigns-country-name">{$other1[outer].country}</p>
                        </div>
                        <div class="campaigns-industry">
                           <img class="campaigns-marker" src="../../assets/images/industry-icon.png" />
                           <p class="campaigns-industry-name">{$other1[outer].industry}</p>
                        </div>
                        <div style="clear:fix"></div>
                     </div>
                     <div class="campaigns-bar">
                        <div class="campaigns-bar-inner"></div>
                     </div>
                     <ul class="campaigns-data">
                        <li style="background: white;">
                           <p class="campaigns-data-bold">S$ {$other1[outer].total_funding_amt|number_format}</p>
                           <p>{if $la=='id'}{else}Target{/if}</p>
                        </li>
                        <li style="background: white;">
                           <p class="campaigns-data-bold">S$ {$other1[outer].curr_funding_amt|number_format}</p>
                           <p>{if $la=='id'}{else}Funded{/if}</p>
                        </li>
                        <li style="background: white;">
                           <p class="campaigns-data-bold">{$other1[outer].project_return} %</p>
                           <p>{if $la=='id'}{else}Return{/if}</p>
                        </li>

                        <li style="background: white;">
                           <p class="campaigns-data-bold">{$other1[outer].funding_summary}</p>
                           <p>{if $la=='id'}{else}Tenor{/if}</p>
                        </li>

                        {if $other[outer].days_left <= 0}
                        <li style="background: white;">
                           <p class="campaigns-data-bold">{if $la=='id'}{else}0, Closed{/if}</p>
                           <p>{if $la=='id'}{else}Days Left{/if}</p>
                        </li>
                        {else}
                        <li style="background: white;">
                           <p class="campaigns-data-bold">{$other1[outer].days_left}</p>
                           <p>{if $la=='id'}{else}Days Left{/if}</p>
                        </li>
                        {/if}
                        <li style="background: white;">
                           <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                              {if $other1[outer].risk=="C" || $other1[outer].risk=="C+" || $other1[outer].risk=="High"}
                              <label class="label label-danger" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other1[outer].risk}{/if}</label><br />
                              {elseif $other1[outer].risk=="B" || $other1[outer].risk=="B+" || $other1[outer].risk=="B-" || $other1[outer].risk=="Medium"}
                              <label class="label label-warning" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other1[outer].risk}{/if}</label><br />
                              {elseif $other1[outer].risk=="A" || $other1[outer].risk=="A-" || $other1[outer].risk=="Low"}
                              <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other1[outer].risk}{/if}</label><br />
                              {else}
                              <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">N/A</label><br />
                              {/if}
                              <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                           </div>
                        </li>
                     </ul>
                  </div>
               </div>
            </li>
            {/section}
            {section name=outer loop=$other2}
            {if $member_id}
            <li onclick="window.location.href = 'campaign/view/{$other2[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
               {else}
               <li id="eth-log" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                  {/if}
                  <div class="campaigns-list-con">
                     <input type="hidden" class="total-amount" value="{$other2[outer].total_funding_amt}" />
                     <input type="hidden" class="curr-amount" value="{$other2[outer].curr_funding_amt}" />

                     <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$other2[outer].campaign_background}')"></div>
                     <div class="campaigns-list-middle">
                        <h4 class="campaigns-title">{$other2[outer].campaign_name}</h4>
                        <span class="campaigns-company">{if $la=='id'}{$other2[outer].classification_id}{else}{$other2[outer].classification}{/if}</span>
                        <div class="campaigns-desc">
                           {if $la=='id'}{$other2[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$other2[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}
                        </div>

                        {if $member_id}
                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$other2[outer].campaign_id}">{if $la=='id'}{else}read more{/if}</a>
                        </p>
                        {else}
                        <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">{if $la=='id'}{else}read more{/if}</a>
                        </p>
                        {/if}
                     </div>
                     <div class="campaigns-list-bottom">
                        <div class="campaigns-list-bottom-1">
                           <div class="campaigns-country">
                              <img class="campaigns-marker" src="../../assets/images/marker.png" />
                              <p class="campaigns-country-name">{$other2[outer].country}</p>
                           </div>
                           <div class="campaigns-industry">
                              <img class="campaigns-marker" src="../../assets/images/industry-icon.png" />
                              <p class="campaigns-industry-name">{$other2[outer].industry}</p>
                           </div>
                           <div style="clear:fix"></div>
                        </div>
                        <div class="campaigns-bar">
                           <div class="campaigns-bar-inner"></div>
                        </div>
                        <ul class="campaigns-data">
                           <li style="background: white;">
                              <p class="campaigns-data-bold">S$ {$other2[outer].total_funding_amt|number_format}</p>
                              <p>{if $la=='id'}{else}Target{/if}</p>
                           </li>
                           <li style="background: white;">
                              <p class="campaigns-data-bold">S$ {$other2[outer].curr_funding_amt|number_format}</p>
                              <p>{if $la=='id'}{else}Funded{/if}</p>
                           </li>
                           <li style="background: white;">
                              <p class="campaigns-data-bold">{$other2[outer].project_return} %</p>
                              <p>{if $la=='id'}{else}Return{/if}</p>
                           </li>

                           <li style="background: white;">
                              <p class="campaigns-data-bold">{$other2[outer].funding_summary}</p>
                              <p>{if $la=='id'}{else}Tenor{/if}</p>
                           </li>

                           {if $other[outer].days_left <= 0}
                           <li style="background: white;">
                              <p class="campaigns-data-bold">{if $la=='id'}{else}0, Closed{/if}</p>
                              <p>{if $la=='id'}{else}Days Left{/if}</p>
                           </li>
                           {else}
                           <li style="background: white;">
                              <p class="campaigns-data-bold">{$other2[outer].days_left}</p>
                              <p>{if $la=='id'}{else}Days Left{/if}</p>
                           </li>
                           {/if}
                           <li style="background: white;">
                              <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                 {if $other2[outer].risk=="C" || $other2[outer].risk=="C+" || $other2[outer].risk=="High"}
                                 <label class="label label-danger" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other2[outer].risk}{/if}</label><br />
                                 {elseif $other2[outer].risk=="B" || $other2[outer].risk=="B+" || $other2[outer].risk=="B-" || $other2[outer].risk=="Medium"}
                                 <label class="label label-warning" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other2[outer].risk}{/if}</label><br />
                                 {elseif $other2[outer].risk=="A" || $other2[outer].risk=="A-" || $other2[outer].risk=="Low"}
                                 <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other2[outer].risk}{/if}</label><br />
                                 {else}
                                 <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">N/A</label><br />
                                 {/if}
                                 <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                              </div>
                           </li>
                        </ul>
                     </div>
                  </div>
               </li>
               {/section}
               {section name=outer loop=$other3}
               {if $member_id}
               <li onclick="window.location.href = 'campaign/view/{$other3[outer].campaign_id}'" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                  {else}
                  <li id="eth-log" style="cursor:pointer;" class="cam-li-each col-xs-12 col-sm-6 col-md-4 col-lg-4">
                     {/if}
                     <div class="campaigns-list-con">
                        <input type="hidden" class="total-amount" value="{$other3[outer].total_funding_amt}" />
                        <input type="hidden" class="curr-amount" value="{$other3[outer].curr_funding_amt}" />

                        <div class="campaigns-list-top" style="background-image:url('../../assets/images/campaign/{$other3[outer].campaign_background}')"></div>
                        <div class="campaigns-list-middle">
                           <h4 class="campaigns-title">{$other3[outer].campaign_name}</h4>
                           <span class="campaigns-company">{if $la=='id'}{$other3[outer].classification_id}{else}{$other3[outer].classification}{/if}</span>
                           <div class="campaigns-desc">
                              {if $la=='id'}{$other3[outer].campaign_snippet_id|strip_tags|strip|truncate:120}{else}{$other3[outer].campaign_snippet|strip_tags|strip|truncate:120}{/if}
                           </div>

                           {if $member_id}
                           <p class="campaigns-read-more"><a class="campaigns-read-more-underline" href="campaign/view/{$other3[outer].campaign_id}">{if $la=='id'}{else}read more{/if}</a>
                           </p>
                           {else}
                           <p class="campaigns-read-more"><a class="campaigns-read-more-underline" id="eth-log">{if $la=='id'}{else}read more{/if}</a>
                           </p>
                           {/if}
                        </div>
                        <div class="campaigns-list-bottom">
                           <div class="campaigns-list-bottom-1">
                              <div class="campaigns-country">
                                 <img class="campaigns-marker" src="../../assets/images/marker.png" />
                                 <p class="campaigns-country-name">{$other3[outer].country}</p>
                              </div>
                              <div class="campaigns-industry">
                                 <img class="campaigns-marker" src="../../assets/images/industry-icon.png" />
                                 <p class="campaigns-industry-name">{$other3[outer].industry}</p>
                              </div>
                              <div style="clear:fix"></div>
                           </div>
                           <div class="campaigns-bar">
                              <div class="campaigns-bar-inner"></div>
                           </div>
                           <ul class="campaigns-data">
                              <li style="background: white;">
                                 <p class="campaigns-data-bold">S$ {$other3[outer].total_funding_amt|number_format}</p>
                                 <p>{if $la=='id'}{else}Target{/if}</p>
                              </li>
                              <li style="background: white;">
                                 <p class="campaigns-data-bold">S$ {$other3[outer].curr_funding_amt|number_format}</p>
                                 <p>{if $la=='id'}{else}Funded{/if}</p>
                              </li>
                              <li style="background: white;">
                                 <p class="campaigns-data-bold">{$other3[outer].project_return} %</p>
                                 <p>{if $la=='id'}{else}Return{/if}</p>
                              </li>

                              <li style="background: white;">
                                 <p class="campaigns-data-bold">{$other3[outer].funding_summary}</p>
                                 <p>{if $la=='id'}{else}Tenor{/if}</p>
                              </li>

                              {if $other[outer].days_left <= 0}
                              <li style="background: white;">
                                 <p class="campaigns-data-bold">{if $la=='id'}{else}0, Closed{/if}</p>
                                 <p>{if $la=='id'}{else}Days Left{/if}</p>
                              </li>
                              {else}
                              <li style="background: white;">
                                 <p class="campaigns-data-bold">{$other3[outer].days_left}</p>
                                 <p>{if $la=='id'}{else}Days Left{/if}</p>
                              </li>
                              {/if}
                              <li style="background: white;">
                                 <div class="" style="font-size: 14px;text-align: center;margin-top: 2px;">
                                    {if $other3[outer].risk=="C" || $other3[outer].risk=="C+" || $other3[outer].risk=="High"}
                                    <label class="label label-danger" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other3[outer].risk}{/if}</label><br />
                                    {elseif $other3[outer].risk=="B" || $other3[outer].risk=="B+" || $other3[outer].risk=="B-" || $other3[outer].risk=="Medium"}
                                    <label class="label label-warning" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other3[outer].risk}{/if}</label><br />
                                    {elseif $other3[outer].risk=="A" || $other3[outer].risk=="A-" || $other3[outer].risk=="Low"}
                                    <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">{if $la=='id'}{else}{$other3[outer].risk}{/if}</label><br />
                                    {else}
                                    <label class="label label-success" rel="tooltip" title="Based on in-house credit assessment, each investment is assigned a risk rating from A (least risky) to C (most risky)">N/A</label><br />
                                    {/if}
                                    <div style="font-size: 13px;margin-top: 5px;">{if $la=='id'}{else}Risk rating{/if}</div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </li>
                  {/section}
               </ul>

            </div>
         </div>




         <!-- start popup -->
         <div id="invest-content">
            <div class="investWrap">
               <center>
                  <div class="investInner">
                     <div class="investClose">
                        <span class="glyphicon glyphicon-remove"></span>
                     </div>
                     <div class="investBody">

                        {if ($need_bank_detail) }
                           As part of our Know Your Customer (KYC) procedure,  please update your bank account details in Dashboard before proceeding to invest.
                           <br ><br>
                           <a href="https://kapitalboost.com/member/bank-account.php" class="btn btn-primary">Complete Bank Account</a>
                        {else}


                        {if ($member_mobile == NULL || $member_country == NULL || $nric == NULL || $current_location == NULL ||$filenric == NULL ||$filenricback == NULL || $addressProof == NULL  || $dob == NULL || $resarea == NULL  || $member_icname == NULL || $ic_option == NULL || $ic_country == NULL) &&  $content.project_type != 'donation'}

                        {if $la=='id'}{else}As part of our Know Your Customer (KYC) procedure, please complete your profile and upload identification documents before proceeding to invest.{/if}
                        <br ><br>
                        <a href="https://kapitalboost.com/member/profile.php" class="btn btn-primary">Complete Profile</a>
                        <!-- <form method="POST" action ="dashboard/profile">
                        <input type="hidden" name="link" value="https://kapitalboost.com/campaign/view/{$content.campaign_id}">
                        <input type="submit" value="Complete Profile" />
                     </form> -->
                     {else}



                     <p class="investText">
                        {if $la=='id'}{else}Thank you for your interest in the <br />
                        <strong>
                           {if $content.acronim != ""}
                           {$content.acronim}
                           {else}
                           {$content.campaign_name}
                           {/if}
                        </strong> campaign. <br />{/if}
                        {if $content.campaign_subtype != ''}
                        {if $la=='id'}{else}Please indicate your preferred funding option, amount and mode of payment.{/if}
                        {else}
                        {if $la=='id'}{else}Please indicate the amount you would like to {/if}{if $content.project_type=='donation'}{if $la=='id'}{else}donate{/if}{else}{if $la=='id'}{else}invest{/if}{/if}.
                        {/if}
                     </p>
                     <form method="post" enctype="multipart/form-data" action="/dashboard/kapitalboost-payment" class="formTest" data-member="{$member_id}" data-campaign="{$content.campaign_id}">
                        <!-- <form method="post" enctype="multipart/form-data" action="#" class="formTest"> -->
                        {if $content.project_type=="donation"}
                        <div class="invest-form-layer_c">
                           <label for="CAT_Custom_20043844_157746">{if $la=='id'}{else}Payment{/if}</label>
                           <select name="paymentID" id="paymentID" class="cat_dropdown"style="color:black;" required>
                              <option value=""> {if $la=='id'}{else}Select Payment{/if} </option>
                              <option value="Paypal">Paypal</option>
                              {if $current_location==="SINGAPORE" }
                              <!-- <option value="Xfers">Xfers (bank transfer)</option> -->
                              {else if $current_location === 'INDONESIA'}
                              <!-- <option value="Xfers">Xfers (dompet virtual)</option> -->
                              <option value="Bank Transfer">Bank Transfer</option>

                              {else}
                              <option value="Bank Transfer">Bank Transfer</option>
                              {/if}

                           </select>
                        </div>
                        {else if $content.project_type=="private"}
                        <div class="invest-form-layer_c">
                           <label for="CAT_Custom_20043844_157746">{if $la=='id'}{else}Payment{/if}</label>
                           <select name="paymentID" id="paymentID" class="cat_dropdown"style="color:black;" required>
                              <option value=""> {if $la=='id'}{else}Select Payment{/if} </option>
                              <option value="Bank Transfer">Bank Transfer</option>


                           </select>
                        </div>
                        {else}
                        <div class="invest-form-layer_c">
                           <label for="CAT_Custom_20043844_157746">{if $la=='id'}{else}Payment{/if}</label>
                           <select name="paymentID" id="paymentID" class="cat_dropdown"style="color:black;" required>
                              <option value=""> {if $la=='id'}{else}Select Payment{/if} </option>
                              <option value="Bank Transfer">Bank Transfer</option>
                              <!-- {if $current_location==="SINGAPORE" }
                              <option value="Xfers" selected>Xfers (bank transfer)</option>
                              {else if $current_location === 'INDONESIA'} -->
                              <!-- <option value="Xfers">Xfers (dompet virtual)</option> -->
                              <!-- <option value="Bank Transfer">Bank Transfer</option> -->

                              <!-- {else} -->
                              <!-- <option value="Bank Transfer" selected>Bank Transfer</option> -->
                              <!-- {/if} -->

                           </select>
                        </div>
                        {/if}

                        <div class="invest-form-layer_c">
                           <label for="CAT_Custom_19975613_157746">{if $la=='id'}{else}Minimum{/if} </label>
                           <br />
                           <div name="smin" id="CAT_Custom_19975613_157746" class="cat_textbox">S$ {if $current_location === 'INDONESIA' && $content.minimum_investment != 0} {$content.minimum_investment} {else}{$content.minimum_investment}{/if}</div>
                        </div>
                        <div class="invest-form-layer_c">
                           <label for="CAT_Custom_19975613_157746">{if $content.project_type=='donation'}{if $content.campaign_subtype == 'hybrid'}{if $la=='id'}{else}Funding{/if}{else}{if $la=='id'}{else}Donation{/if}{/if}{else}{if $la=='id'}{else}Investment{/if}{/if} {if $la=='id'}{else}amount{/if}</label>
                           <br /><div id="staticParent">
                              <input type="number"  min="{if $current_location === 'INDONESIA' && $content.minimum_investment != 0} {$content.minimum_investment} {else}{$content.minimum_investment}{/if}"  id="child" class="cat_textbox investAmount" value="" style="color:black;"
                              name="amount" placeholder="SGD" required />

                              <input type="hidden" name="minimInvest" value="{if $current_location === 'INDONESIA' && $content.minimum_investment != 0} {$content.minimum_investment} {else}{$content.minimum_investment}{/if}" />

                              <input type="hidden" name="kbmaxamount" value="{$content.total_funding_amt}" />

                              <input type="hidden" name="kbcuramount" value="{$content.curr_funding_amt}" />

                           </div>
                        </div>
                        <div class="alert alert-danger popup-alert black-alert"><ul></ul></div>
                        <div name="warnAmount" id="warnAmount" style="display:none;color: red; margin-bottom: 10px; margin-top: -10px;">Please only input amount to denominations of $10.</div>

                        <div name="warnAmountMinim" id="warnAmountMinim" style="display:none;color: red; margin-bottom: 10px; margin-top: -10px;">Oops, please input amount higher than minimum investment</div>

                        <div name="warnAmountExceed" id="warnAmountExceed" style="display:none;color: red; margin-bottom: 10px; margin-top: -10px;">Sorry, your investment amount has exceeded the target funding. Please re-enter a lower investment amount.</div>

                        <div name="warnAmountExceed30" id="warnAmountExceed30" style="display:none;color: red; margin-bottom: 10px; margin-top: -10px;">Sorry, your investment amount has exceeded 30% of total amount allowed. Please re-enter a lower investment amount.</div>

                        {if $current_location === 'INDONESIA'}
                        <!-- <div> -->
                        <!-- {if $la=='id'}{else}Your investment amount is equivalent to IDR {/if}<span id="idrAmount" style="font-weight:bold;">0</span> -->
                        <!-- </div> -->
                        {/if}

                        {if $content.days_left <= 0}
                        <div class="invest-form-submit-layer">
                           <center>                <br>
                              <input class="cat_button disabled" disabled="disabled" type="button" value="Submit" id="catwebformbutton" />
                           </center>
                        </div>
                        {else}

                        {if $content.campaign_subtype == 'hybrid'}
                        <div class="invest-form-submit-layer">
                           <br >
                           <center>
                              <input class="cat_button disabled" disabled="disabled" name="donateBtn" type="button" value="Donate" id="catwebformbutton1" />
                           </center>
                           <br>
                           <center>
                              <input class="cat_button disabled" disabled="disabled" name="rewardBtn" type="button" value="Donate with reward" id="donateWithRewardBtn" />
                           </center>
                           <br />
                           <center>
                              <input class="cat_button disabled" disabled="disabled" name="interestBtn" type="button" value="Interest-free Loan" id="interestFreeLoanBtn" />
                           </center>
                        </div>
                        {else}
                        <div class="invest-form-submit-layer">
                           <br />
                           <center>
                              <input class="cat_button disabled" disabled="disabled" name="minimVal" type="button" value="Submit" id="catwebformbutton1" data-member="{$member_id}" data-campaign="{$content.campaign_id}"/>
                           </center>
                        </div>
                        {/if}

                        {/if}
                        <input type="hidden" name="business" value="erly@kapitalboost.com">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="item_name" value="{$content.campaign_name}">
                        <input type="hidden" name="item_number" value="1">
                        <input type="hidden" name="credits" value="510">
                        <input type="hidden" name="tx" value="TransactionID">
                        <input type="hidden" name="at" value="ODJuzlFwX-WNtbrnJhd_EB33SXbFXYt7xkWb8AGPGQp-eqi5l0wKkTr8it0">
                        <input type="hidden" name="userid" value="1">
                        <input type="hidden" name="cpp_header_image" value="https://kapitalboost.com/assets/images/logo.png">
                        <input type="hidden" name="no_shipping" value="1">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="handling" value="0">
                        <input type="hidden" name="custom" value="{$content.campaign_name}|{$member_name}|{$member_email}|{$current_location}|{$content.campaign_owner}|{$token}|{$content.project_type}|{$content.expiry_date}"/>
                        <input type="hidden" name="cancel_return" value="https://kapitalboost.com/cancel.php">
                        <input type="hidden" name="return" value="https://kapitalboost.com/payment">

                        <input type="hidden" name="order_id" value="A{$orderid}">
                        <input type="hidden" name="item_name_1" value="{$content.campaign_name}">
                        <input type="hidden" name="item_item_id_1" value="12345">
                        <input type="hidden" name="item_description_1" value="Kapitalboost campaign">
                        <input type="hidden" name="item_quantity_1" value="1">
                        <input type="hidden" name="currency" value="SGD">
                        <input type="hidden" name="refundable" value="false">
                        <input type="hidden" name="username" value="{$member_name}">
                        <input type="hidden" name="lastname" value="{$member_surname}">
                        <input type="hidden" name="xfersphone" value="{$member_mobile}">
                        <input type="hidden" name="email" value="{$member_email}">
                        <input type="hidden" name="projecttype" value="{$content.project_type}">
                        <input type="hidden" name="sme_subtype" value="{$content.sme_subtype}">
                        <input type="hidden" name="campaign_owner" value="{$content.campaign_owner_email}">
                        <input type="hidden" name="clossing_date" value="{$content.expiry_date}">
                        <input type="hidden" name="campaign_token" value="{$token}">
                        <input type="hidden" name="country" value="{$current_location}">
                        <input type="hidden" name="current_location" value="{$current_location}">
                        <input type="hidden" name="campaign_id" value="{$campaign_id}">
                        <input type="hidden" name="campaign_kod" value="{$content.acronim}">

                     </form>
                     {/if}

                     {/if}
                  </div>
               </div>
            </center>
         </div>
      </div>
      <!-- end popup -->

      {if ($content.days_left <= 0 && $closedPw != 'correct' && $content.project_type != 'donation') || ($content.project_type=='private' && $closedPw != 'correct')}

      <div id="closed">
         <div id="login-box">
            <div id="login-top">
               <a href="/campaign/" >X</a>
            </div>
            <form target="" method="POST">
               <input id="redirect-eth-log-privates" type="hidden" name="redirect" value=""/>
               <div id="login-middle">
                  <img id="login-logo" alt="" src="assets/images/logo-baru.png" />
                  <h1 id="login-h1" class="hidden">{if $la=='id'}{else}Private Password{/if}</h1>
                  <h2 class="login-private">
                     {if $content.project_type=='private'}
                     {if $la=='id'}{else}A password is required to view this page. This can be obtained directly from the business owner or upon request to the Kapital Boost team (please email to <a href="mailto:hello@kapitalboost.com" target="_blank">hello@kapitalboost.com</a>).{/if}
                     {else}
                     {if $la=='id'}{else}This campaign is closed for investment. If you are an investor and want to access the campaign writeup, please request for the password from <a href="mailto:hello@kapitalboost.com">hello@kapitalboost.com</a>{/if}
                     {/if}
                  </h2>

                  <input type="password" id="close-pass" name="closedPw"  style="width:100%"/>
                  {if $closedPw == 'wrong'}
                  <h4 style='color:red;text-align:center;margin-top:20px'>{if $la=='id'}{else}The password you've entered is incorrect{/if}</h4>
                  {/if}

               </div>
               <div id="login-bottom">
                  <div id="log-bottom-1">
                     <input type="submit"id="close-submit"  />

                  </div>
               </div>

            </form>
         </div>

      </form>
   </div>
</div>


{/if}

<!-- start enquire -->
<div class="enquireWrap">
   <div class="enquireInner">
      <div class="enquireClose">
         <span class="glyphicon glyphicon-remove"></span>
      </div>
      <div class="enquireTitle">
         {if $la=='id'}{else}Enquire{/if}
      </div>
      <div class="enquireBody">
         <!--<form method="POST" action="index.php?class=Campaign&method=GetContent&campaign_id={$content.campaign_id}">-->
         <input type="text" id="eName"  value="{$member_name}" readonly />
         <input type="text" id="eEmail"  value="{$member_email}" readonly />
         <input type="text" id="eProject" value="{$content.campaign_name}" readonly />
         <input type="hidden" id="eToken" value="{$token}" />
         <textarea id="eMsg" placeholder="{if $la=='id'}{else}Type your enquiry and click Submit. We will respond within 1 working day.{/if}" required></textarea>
         <button class="btn btn-primary" id="enquiryBtn" type="button">{if $la=='id'}{else}Submit{/if}</button>
         <!--</form>-->
      </div>
   </div>
</div>
<!-- end enquire -->

<!-- start enquire -->

<div class="enquireWrapMsg">
   <div class="enquireInnerMsg">
      <div class="enquireCloseMsg">
         <span class="glyphicon glyphicon-remove"></span>
      </div>
      <div class="enquireTitleMsg">
         {if $la=='id'}{else}Enquire{/if}
      </div>
      <div class="enquireTextMsg">
         {if $la=='id'}{else}Thank you for your inquiry!<br>
         Our team is looking into it and will reply to you shortly.<br>
         If your enquiry is urgent, you may call us at +65 9865 9648 from Monday to Friday during our office hours.{/if}
      </div>
   </div>
</div>

<!-- end enquire -->


<div id="real-lightbox">
   <span class="glyphicon glyphicon-chevron-right galleryControl1" id="galleryControl1"></span>
   <span class="glyphicon glyphicon-chevron-left galleryControl2" id="galleryControl2"></span>
   <div id="real-lightbox-inner">
      <div id="real-lightbox-inner-top">
         <div id="real-close-box">
            <span class="glyphicon glyphicon-remove"></span>
         </div>
         <div id="real-lightbox-inner-Title">

         </div>
      </div>
      <div id="real-lightbox-inner-middle">
      </div>
   </div>
</div>

<!-- Modal -->
<div class="modal fade" id="invest-confirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> -->
            <h4 class="modal-title" id="myModalLabel">Invest Confirmation</h4>
         </div>
         <div class="modal-body">
            By clicking <b>Next</b>, you have confirmed to invest <strong id="amount-to-invest"></strong> in the crowdfunding campaign and have read and understood the <a href="/legal#risk" target="_blank">Risk Statement</a> related to making such investments.
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" id="investConfirmed">Next</button>
         </div>
      </div>
   </div>
</div>

<div id="info-member" data-id-member="{$member_id}" data-name-member="{$member_name}"></div>

{elseif $error_msg=='wrongpw'}

<div class="row" align="center">
   <div class="col-xs-12" style="text-align: center; color: red; margin-top:200px;">
      <div class="alert alert-warning">
         <div>{if $la=='id'}{else}The password you have entered is incorrect. The password can be obtained directly from the business owner or upon request to the Kapital Boost team {/if}
         </div>
         <div>{if $la=='id'}{else}(please email to hello@kapitalboost.com).{/if}
         </div>
      </div>
   </div>
</div>
{/if}

<script>

{if $content.project_type == "private" && $pass != $content.private_password}
{literal}
$(function () {
   $('#private-lightbox').fadeIn();
});
{/literal}
{/if}

{literal}
$(function () {

   var $ind_curr_amt = $('.ind-curr-amount').val();
   var $ind_total_amt = $('.ind-total-amount').val();
   var $ind_bar_width = ($ind_curr_amt / $ind_total_amt) * 100;
   //console.log($ind_curr_amt);
   //console.log($ind_total_amt);
   if ($ind_bar_width > 100)
   $ind_bar_width = 100;
   $('.campaigns-bar-inner').css({width: $ind_bar_width + '%'});


});


{/literal}
</script>
<script>
{literal}
// Set the date we're counting down to
var countDownDate = new Date("{/literal}{$release_time} {$content.release_time}{literal}:00 GMT+0800").getTime();
//console.log("countDownDate " + countDownDate);
//var countDownDate = new Date("March 8, 2018 05:56:01 GMT+0800").getTime();
document.getElementById("invbtn").style.display = "none";
document.getElementById("secondInvest").style.display = "none";
document.getElementById("cndtext").style.display = "none";
document.getElementById("cndtext2").style.display = "none";
var investContent = $("#invest-content").html();
var distance = 0;
// Update the count down every 1 second
var x = setInterval(function() {

   // Get todays date and time
   var now = new Date().getTime();

   // Find the distance between now an the count down date
   distance = countDownDate - now;
   //console.log("distance " + distance);

   // Time calculations for days, hours, minutes and seconds
   var days = Math.floor(distance / (1000 * 60 * 60 * 24));
   var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
   var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
   var seconds = Math.floor((distance % (1000 * 60)) / 1000);

   // Output the result in an element with id="demo"
   document.getElementById("cndtime").innerHTML =  ('0' + hours).slice(-2)+ ":" + ('0' + minutes).slice(-2) + ":" + ('0' + seconds).slice(-2);
   document.getElementById("cndtime2").innerHTML = ('0' + hours).slice(-2)+ ":" + ('0' + minutes).slice(-2) + ":" + ('0' + seconds).slice(-2);

   // If the count down is over, write some text
   if (distance < 0) {
      clearInterval(x);
      //document.getElementById("demo").innerHTML = "EXPIRED";
      document.getElementById("invbtn").style.display = "initial";
      document.getElementById("secondInvest").style.display = "inline-block";
      document.getElementById("cndtext").style.display = "none";
      document.getElementById("cndtext2").style.display = "none";
      $("#invest-content").html(investContent);
   } else {
      document.getElementById("invbtn").style.display = "none";
      document.getElementById("secondInvest").style.display = "none";
      document.getElementById("cndtext").style.display = "initial";
      document.getElementById("cndtext2").style.display = "initial";
      $("#invest-content").html('');
   }
}, 1000);
{/literal}
</script>
<script>
{literal}

function amountValidation() {
   $(".investAmount").on("keyup", function(e) {
      if (e.keyCode == 13) return false;

      var exRate = '{/literal}{$exRate}{literal}';
      var minimInvestAmount = parseFloat('{/literal}{$content.minimum_investment}{literal}');
      var totalAmount = parseFloat('{/literal}{$content.total_funding_amt}{literal}');
      var currentAmount = parseFloat('{/literal}{$content.curr_funding_amt}{literal}');
      var sisa = totalAmount - currentAmount;
      var $amountIdr = $("#amountIdr");
      var $amountIdrV = $("#amountIdrValue");

      var $theVal = parseFloat($(this).val());

      var errorReport = 0;

      $(".popup-alert ul").html("");

      if ($theVal == "") {
         $("#amountIdr").val("0");
      }

      if ($theVal < minimInvestAmount) {
         if (sisa >= minimInvestAmount) {
            // console.log("input lebih kecil dari minimum invest");
            $(".invest-form-submit-layer input.cat_button").attr("type", "button");
            $(".invest-form-submit-layer input.cat_button").attr("disabled", "disabled");
            $(".invest-form-submit-layer input.cat_button").addClass("disabled");
            $(".popup-alert ul").append("<li>*Please input amount higher than minimum investment</li>");
            errorReport += 1;
         }
      }

      thirtyPercent = totalAmount * (30/100);
      if ($theVal > thirtyPercent) {
         // console.log("input lebih dari 30% total funding");
         $(".invest-form-submit-layer input.cat_button").attr("type", "button");
         $(".invest-form-submit-layer input.cat_button").attr("disabled", "disabled");
         $(".invest-form-submit-layer input.cat_button").addClass("disabled");
         $(".popup-alert ul").append("<li>*Your investment amount has exceeded 30% of total amount allowed. Please re-enter a lower investment amount.</li>");
         errorReport += 1;
      }

      if ($theVal > sisa) {
         // console.log("input lebih besar dari sisa");
         $(".invest-form-submit-layer input.cat_button").attr("type", "button");
         $(".invest-form-submit-layer input.cat_button").attr("disabled", "disabled");
         $(".invest-form-submit-layer input.cat_button").addClass("disabled");
         $(".popup-alert ul").append("<li>*Your investment amount has exceeded the target funding. Please re-enter a lower investment amount.</li>");
         errorReport += 1;
      }

      if ($theVal % 10 != 0) {
         // console.log("input bukan kelipatan 10");
         $(".invest-form-submit-layer input.cat_button").attr("type", "button");
         $(".invest-form-submit-layer input.cat_button").attr("disabled", "disabled");
         $(".invest-form-submit-layer input.cat_button").addClass("disabled");
         $(".popup-alert ul").append("<li>*Please only input amount to denominations of $10.</li>");
         errorReport += 1;
      }

      if (errorReport == 0) {
         $(".invest-form-submit-layer input.cat_button").attr("type", "submit");
         $(".invest-form-submit-layer input.cat_button").removeAttr("disabled");
         $(".invest-form-submit-layer input.cat_button").removeClass("disabled");
         $(".popup-alert").removeClass("show");
      } else {
         $(".popup-alert").addClass("show");
      }

      return false;

   })
}

$(document).ready(function () {


   /* start invest now */
   $('.investOpen').click(function () {
      if (distance <= 0) {
         $('.investWrap').fadeIn(300);
      }else{
         console.log(distance);
      }

      amountValidation();
      $('.investClose').click(function () {
         $('.investWrap').fadeOut(300);
      });

      $('.formTest').submit(function (event) {
         // $('#catwebformbutton1').click(function (event) {
         var minimInvest = $('#child').val();
         var minimTarget = $('input[name="minimInvest"]').val();
         var maxAmount = parseInt($('input[name="kbmaxamount"]').val());
         var currAmount = $('input[name="kbcuramount"]').val();
         var totalInvestNow = parseInt(currAmount)+parseInt(minimInvest);
         var paymentMethod = $("#paymentID").val();
         var sisa = maxAmount - currAmount;
         var amount30 = maxAmount * (30/100);
         var paymentMethod = $("#paymentID").val();
         var campaign_id = $(this).data('campaign');
         var member_id = $(this).data('member');


         if (paymentMethod === "") {
            alert("Oops, please select the Payment Method");
            event.preventDefault();
         } else {
            $.ajax({
               method: "GET",
               url: "/index.php?class=Campaign&method=totalFundingUser&member_id="+member_id+"&campaign_id="+campaign_id+"&max_amount="+amount30+"&amount="+$("[name='amount']").val(),
               context: this,
               success: function(result) {
                  $result = JSON.parse(result);
                  if ($result.error == 1 || $result.error == "1") {
                     $(".popup-alert ul").html("");
                     $(".invest-form-submit-layer input.cat_button").attr("type", "button");
                     $(".invest-form-submit-layer input.cat_button").attr("disabled", "disabled");
                     $(".invest-form-submit-layer input.cat_button").addClass("disabled");
                     $(".popup-alert ul").append("<li style='color: #c0392b;'>*"+$result.message+"</li>");
                     $(".popup-alert").addClass("show");
                  }else{
                     var $this = this;
                     $('.investWrap').fadeOut(300);

                     setTimeout(function() {
                        $('#amount-to-invest').text("S$" + $("[name='amount']").val());
                        $('#invest-confirmation').modal('show');
                     }, 300);

                     $id = $("#info-member").data("id-member");
                     $name = $("#info-member").data("name-member");

                     mixpanel.track("User Invest", {
                        "User ID": $id,
                        "Username": $name,
                        "Campaign Name": $("#campaign-name").text(),
                     });

                     $("#investConfirmed").click(function() {
                        $('#invest-confirmation').modal('hide');
                        setTimeout(function() {
                           openModal();
                           $this.submit();
                        }, 300);
                     });
                  }
               }
            })

         }
         return false;

      });
   });

   /* end invest now */

   /* start invest now */
   $('.enquireOpen').click(function () {
      $('.enquireWrap').fadeIn(300);
   });

   $('.enquireClose').click(function () {
      $('.enquireWrap').fadeOut(300);
   });
   /* end invest now */

   $("#enquiryBtn").on('click', function () {
      $('.enquireWrap').fadeOut(100);
      var name = $("#eName").val();
      var email = $("#eEmail").val();
      var project = $("#eProject").val();
      var token = $("#eToken").val();
      var msg = $("#eMsg").val();

      $.post("index.php?class=Campaign&method=Enquiry", {name: name, email: email, project: project, msg: msg, token: token});

      $('.enquireWrapMsg').fadeIn(400);

   });

   $(document).on('click', '#gallery-list li', function () {
      var $img_url = $(this).attr('data-img-url');
      var $title = $(this).attr('title');
      var $Id = $(this).attr('id');
      var $cmname = $(this).attr('data-img-name');
      var galleryTotal = $('#gallery-list li').length;
      var galleryCount = 480 * 1 + galleryTotal - 1;
      if ($Id <= 481) {
         $('#galleryControl2').hide();
      }
      if ($Id => galleryCount) {
         $('#galleryControl1').hide();
      }
      if ($Id <= galleryCount) {
         $('#galleryControl1').show();
      }
      if ($Id > 481) {
         $('#galleryControl2').show();
      }
      $('#real-lightbox-inner-Title').html('<p><strong> ' + $cmname + '  </strong></p>');
      $('#real-lightbox-inner-middle').html('<img src=" ' + $img_url + ' " id="' + $Id + '" width="100%"/>');
      $('#real-lightbox').fadeIn();
   });




   $('#galleryControl1').click(function () {
      var test1 = $('#real-lightbox-inner-middle img').attr('id');
      var test2 = test1 * 1 + 1;
      var targetImg = $('#' + test2 + '').attr('data-img-url');
      var $cmname = $('#' + test2 + '').attr('data-img-name');
      var galleryTotal1 = $('#gallery-list li').length;
      var galleryCount1 = 480 * 1 + galleryTotal1 - 1;
      if (test2 <= 481) {
         $('#galleryControl2').hide();
      }
      if (test2 => galleryCount1) {
         $('#galleryControl1').hide();
      }
      if (test2 <= galleryCount1) {
         $('#galleryControl1').show();
      }
      if (test2 > 481) {
         $('#galleryControl2').show();
      }
      $('#real-lightbox-inner-Title').html('<p><strong> ' + $cmname + '  </strong></p>');
      $('#real-lightbox-inner-middle').html('<img src="' + targetImg + '" id="' + test2 + '" width="100%"/>');
   });
   $('#galleryControl2').click(function () {
      var test12 = $('#real-lightbox-inner-middle img').attr('id');
      var test22 = test12 * 1 - 1;
      var targetImg2 = $('#' + test22 + '').attr('data-img-url');
      var $cmname = $('#' + test22 + '').attr('data-img-name');
      var galleryTotal2 = $('#gallery-list li').length;
      var galleryCount2 = 480 * 1 + galleryTotal2 - 1;
      if (test22 <= 481) {
         $('#galleryControl2').hide();
      }
      if (test22 => galleryCount2) {
         $('#galleryControl1').hide();
      }
      if (test22 <= galleryCount2) {
         $('#galleryControl1').show();
      }
      if (test22 > 481) {
         $('#galleryControl2').show();
      }
      $('#real-lightbox-inner-Title').html('<p><strong> ' + $cmname + '  </strong></p>');
      $('#real-lightbox-inner-middle').html('<img src="' + targetImg2 + '" id="' + test22 + '" width="100%"/>');
   });
});

function openModal() {
   document.getElementById('modal').style.display = 'block';
   document.getElementById('fade').style.display = 'block';
}

/* start validasi */
$(document).ready(function () {


   /* start enquire msg */
   $('.enquireCloseMsg').click(function () {
      $('.enquireWrapMsg').fadeOut(800);
   });
   /* end enquire msg */



});
/* end validasi */



function validAngka(a)
{
   var exRate = '{/literal}{$exRate}{literal}';
   var minimInvestAmount = parseInt('{/literal}{$content.minimum_investment}{literal}');
   var totalAmount = parseInt('{/literal}{$content.total_funding_amt}{literal}');
   var currentAmount = parseInt('{/literal}{$content.curr_funding_amt}{literal}');
   var sisa = totalAmount - currentAmount;


   if (a.value == "") {
      // <!-- document.getElementById("idrAmount").innerHTML = "0"; -->
      document.getElementById("amountIdr").value = "0";
      document.getElementById("amountIdrValue").value = "0";
   }

   //cek jika jumlah yang diinputkan tidak kelipatan 10
   if (a.value % 10 != 0  ) {
      document.getElementById("warnAmount").style.display = 'block';
      $("input[type=submit]").attr("disabled", "disabled");
   } else {
      document.getElementById("warnAmount").style.display = 'none';
      $("input[type=submit]").removeAttr("disabled");
   }
   if (sisa >= minimInvestAmount) {
      //cek jika jumlah yang diinputkan dibawah batas minimum
      if (a.value < minimInvestAmount ) {
         document.getElementById("warnAmountMinim").style.display = 'block';
         $("input[type=submit]").attr("disabled", "disabled");
      } else {
         document.getElementById("warnAmountMinim").style.display = 'none';
         $("input[type=submit]").removeAttr("disabled");
      }
   }

   //cek jika jumlah yang diinputkan melebihi target funding
   var inputFunding = parseInt(a.value) + currentAmount;
   if (inputFunding > totalAmount ) {
      document.getElementById("warnAmountExceed").style.display = 'block';
      $("input[type=submit]").attr("disabled", "disabled");
   } else {
      document.getElementById("warnAmountExceed").style.display = 'none';
      $("input[type=submit]").removeAttr("disabled");
   }

   //cek jika jumlah yang diinputkan melebihi 30%
   var tigaPuluhPersen = 0.3 * totalAmount;
   var userTotalFunding = '{/literal}{$userTotalFunding}{literal}';
   var totalFundingNow = parseInt(a.value) + parseInt(userTotalFunding);
   //console.log("Total Funding" + totalFundingNow);
   if (totalFundingNow > tigaPuluhPersen ) {
      document.getElementById("warnAmountExceed30").style.display = 'block';
      $("input[type=submit]").attr("disabled", "disabled");
   } else if (a.value > tigaPuluhPersen ) {
      document.getElementById("warnAmountExceed30").style.display = 'block';
      $("input[type=submit]").attr("disabled", "disabled");
   } else {
      document.getElementById("warnAmountExceed30").style.display = 'none';
      $("input[type=submit]").removeAttr("disabled");
   }
}

Number.prototype.formatMoney = function (c, d, t) {
   var n = this,
   c = isNaN(c = Math.abs(c)) ? 2 : c,
   d = d == undefined ? "." : d,
   t = t == undefined ? "," : t,
   s = n < 0 ? "-" : "",
   i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
   j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

function onlyNumbers(evt) {
   // Mendapatkan key code
   var charCode = (evt.which) ? evt.which : event.keyCode;
   // Validasi hanya tombol angka
   if (charCode > 31 && (charCode < 48 || charCode > 57))
   return false;
   return true;
}
{/literal}
</script>
<script>
{literal}
fbq('track', 'ViewContent');
{/literal}
</script>
