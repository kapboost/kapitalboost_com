            
<!--------------------------------------- content wrapper --------------------------------------->
<div class="row main-content-wrapper-00">
       <!--------------------------------------- breadcrumbs --------------------------------------->
       <div class="col-xs-12">
              <!----------------------------- separator ----------------------------->
              <div class="separator"></div>
              <!----------------------------- //separator ----------------------------->
              <div>
                     <ol class="breadcrumb">
                            <li><a href="#"><img src="images/breadcrumb-home.png"></a></li>
                            {$breadcrumb}
                     </ol>
              </div>
       </div>
       <!--------------------------------------- //breadcrumbs --------------------------------------->
       <div class="clearfix"></div>


       <!--------------------------------------- main content wrapper --------------------------------------->
       <div class="member-login-wrapper">
              <h2 class="t-center">MEMBER SIGNUP</h2>

              {if count($error_msg) gt 0}
              <div class="alert alert-danger" role="alert">
                     <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                     <span class="sr-only">Error:</span>
                     {section name=outer loop=$error_msg}
                     {$error_msg[outer]}
                     {/section}
              </div>
              {/if}

              <form method="post" id="register_form" name="register_form" action="member/register" autocomplete="off">
                     <!--------------------------------------- login wrapper --------------------------------------->
                     <div>
                            <div class="member-login-container">
                                   <div class="inner-container">
                                          <!----------------------------- data ----------------------------->
                                          <div class="row">
                                                 <div class="col-xs-12">
                                                        <div class="t-center">
                                                               <a href="member/facebook" type="button" class="btn-login-with-facebook"><span>SIGNUP with FACEBOOK</span></a>
                                                               <br><br>
                                                               <h4 class="c-pink t-center" style="margin:0;">--- OR ---</h4>
                                                        </div>
                                                        <br>
                                                        <div class="member-login-form-container">
                                                               <div class="form-horizontal">
                                                                      <div class="form-group">
                                                                             <label for="email" class="col-sm-3 col-md-4  control-label" style="text-align:right;">Your Email Address</label>
                                                                             <div class="col-sm-9 col-md-8">
                                                                                    <input required="required" name="member_email" value="{$member_email}" type="email" class="form-control" id="given-name" placeholder="Email Address">
                                                                             </div>
                                                                      </div>
                                                               </div>
                                                        </div>
                                                 </div>
                                          </div>
                                          <!----------------------------- //data ----------------------------->
                                   </div>
                                   <div class="member-login-sub-container">
                                          <div class="inner-container t-center">
                                                 Already Member? <a href="member/login">LOGIN HERE</a>
                                          </div>
                                   </div>
                            </div>
                            <br>
                            <div class="checkout-btn-wrapper">
                                   <button type="submit" class="btn btn-main-shopping-cart btn-green" style="margin-left:0;">REGISTER</button>
                            </div>
                     </div>
                     <!--------------------------------------- //login wrapper --------------------------------------->
              </form>
       </div>
       <!--------------------------------------- //main content wrapper --------------------------------------->

       <div class="clearfix"></div>
       <br>
</div>
<!--------------------------------------- //content wrapper --------------------------------------->
<br>



<!--------------------------------------- content wrapper --------------------------------------->
<div class="row main-content-wrapper-01">
       {$social_media_plugins}
</div>
<!--------------------------------------- //content wrapper --------------------------------------->
