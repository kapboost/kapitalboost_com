			<div id="container">
<center>
                <div id="default">
                    <div id="default-1">
                        <div id="default-1-back">
                            <div id="default-1-inner" style="text-align: left;">
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h1>
                                    <strong>
                                        <h1><span style="font-size: 28px;" id="term">Kapital Boost - Terms of Use</span></h1>
                                        <div><span style="font-size: 28px;"><span style="font-size: 18px;"></span><br />
                                            </span>
                                        </div>
                                    </strong>
                                </h1>
                                <p style="text-align: left;">This website and the contents herein do not constitute as any financial advice, investment advice or solicitation for the purposes of making financial investments in Singapore or other territories. Kapital Boost Pte. Ltd. is a firm specialising in the matching of opportunities between our registered members and small to medium-sized enterprises. With regard to these opportunities, the need and onus to do due diligence lies squarely with our members as we do not profess to advise on the same. All dealings and transactions are directly with the businesses, project owners or authorized agents we refer to our members.</p>
                                <p style="text-align: left;">Kapital Boost Pte. Ltd. is not licensed and/or registered under the Securities &amp; Futures Act of Singapore or the Financial Advisor's Act under the Monetary Authority of Singapore and thus cannot offer, solicit, advice or recommend the buying and selling of investment securities or any other financial and banking products from the public.<br />
                                    <br />
                                    Further, we state that in preparing and procuring the information within this document, whilst reasonable care is taken to ensure reliability and accuracy, Kapital Boost Pte. Ltd. and its affiliates, directors, employees, and partners provides no guarantee with regard to its content and completeness and does not accept any liability for losses which might arise from making use of this information.<br />
                                    <br />
                                    The opinions expressed in this document are those of Kapital Boost Pte. Ltd. at the time of writing. If nothing is indicated to the contrary, all figures are unaudited. The recipient is, in particular, recommended to check that the information provided is in line with his/her own circumstances with regard to any legal, regulatory, tax or other consequences, if necessary with the help of a professional advisor.<br />
                                    <br />
                                    This document may not be reproduced either in part or in full without the written permission of Kapital Boost Pte. Ltd. It is expressly not intended for persons who, due to their nationality or place of residence, are not permitted access to such information under local law.<br />
                                    <br />
                                    Every investment involves risk, especially with regard to fluctuations in value and return. Investments in foreign currencies involve the additional risk that the foreign currency might lose value against the investor's reference currency. It should be noted that historical returns and scenarios are no guarantee of future performance. Thank you for reading these notices. If you require additional information or clarifications from us on this matter, please email support@kapitalboost.com.
                                </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;"><br />
                                    </span>
                                </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;"><br />
                                    </span>
                                </p>
                                <p style="text-align: left;">
                                    <span style="font-size: 18px; color: #3f3f3f;">
                                <h1><strong><span style="font-size: 28px;" id="privacy">Kapital Boost - Website Privacy Policy</span></strong></h1>
                                </span></p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;"><br />
                                    </span>
                                </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">Kapital Boost takes your privacy very seriously. We ask that you read this privacy policy (the &ldquo;Policy&rdquo;) carefully as it contains important information about what to expect when Kapital Boost collects personal information about you and how Kapital Boost will use your personal data.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">This Policy applies to information we collect on www.kapitalboost.com and its sub-domains about:</span></p>
                                <p> </p>
                                <p style="text-align: left; margin-left: 36pt;"><span style="font-size: 18px; color: #3f3f3f;">i) visitors to our website (the &ldquo;Site&rdquo;); and</span></p>
                                <p> </p>
                                <p style="text-align: left; margin-left: 36pt;"><span style="font-size: 18px; color: #3f3f3f;">ii) people who register their emails with us.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="font-size: 20px; color: #1f497d;">1. &nbsp;Information collected from all visitors to our website</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">We will obtain personal data about you when you visit us. When you visit us, we may monitor the use of this Site through the use of cookies and similar tracking devices. For example, we may monitor the number of times you visit our Site or which pages you go to. This information helps us to build a profile of our users. Some of this data will be aggregated or statistical, which means that we will not be able to identify you individually. Please see further the section on &ldquo;Our Use of Cookies&rdquo; below.</span></p>
                                <p> </p>
                                <p style="text-align: left; margin-left: 14.2pt; text-indent: -14.2pt;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">2. &nbsp;Information collected from users who register their emails with us</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">When you register your emails with us, we may use it for the following:</span></p>
                                <p> </p>
                                <p style="text-align: left; margin-left: 36pt;"><span style="font-size: 18px; color: #3f3f3f;">(a) will send you newsletters, contact you about products and services we think may be of interest to you.</span></p>
                                <p> </p>
                                <p style="text-align: left; margin-left: 36pt;"><span style="font-size: 18px; color: #3f3f3f;">(b) Your information may also be used by Club Ethis, a private network owned by Ethis Pte Ltd, our affiliated company, in contacting you about their products and services. </span></p>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">If you agree to us providing you with these information, you can always opt out at a later date. If you would rather not receive these information, please send an email message to support@kapitalboost.com with the subject UNSUBSCRIBE. </span></p>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">By submitting your email you consent to the use of that email as set out in this policy.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">3. &nbsp; Our use of cookies</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">Cookies are text files placed on your computer to collect standard Internet log information and visitor behaviour information. The information is used to track visitor use of the Site and to compile statistical reports on Site activity. For further information about cookies visit www.aboutcookies.org or www.allaboutcookies.org. You can set your browser not to accept cookies and the above websites tell you how to remove cookies from your browser. However, in a few cases some of our Site features may not function if you remove cookies from your browser.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">4. &nbsp; How we protect your information</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">We are committed to maintaining the security of personal information. We have put in place appropriate security procedures and technical and organisational measures to safeguard your personal information such as securing our websites with SSL.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">5. &nbsp; Access to your information and updating and correcting your information</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">Subject to the exceptions referred to in section 21(2) of the Personal Data Protection Act 2012 (No. 26 of 2012) of Singapore (&ldquo;PDPA&rdquo;), you have the right to request a copy of the information that we hold about you. If you would like a copy of some or all of your personal information, please send an email to support@kapitalboost.com. We may make a small charge for this service.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">We want to ensure that your personal information is accurate and up to date. If any of the information that you have provided to Kapital Boost changes, for example if you change your email address, name or payment details, or if you wish to cancel your registration, please let us know the correct details by sending an email to support@kapitalboost.com. You may ask us, or we may ask you, to correct information you or we think is inaccurate, and you may also ask us to remove information which is inaccurate.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">6. &nbsp; Non-disclosure</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">We do not sell, trade, or otherwise transfer to third parties your personally identifiable information.&nbsp; This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, as long as these parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours and others&rsquo; rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">7. &nbsp; Links to other websites</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">Our Site may contains links to other websites. This privacy policy only applies to this website so when you link to other websites you should read their own privacy policies.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">8. &nbsp; Sale of business</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">If Kapital Boost&rsquo;s business is sold or integrated with another business your details may be disclosed to our advisers and any prospective purchasers and their advisers and will be passed on to the new owners of the business.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong>
                                    </strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">9. &nbsp; Changes to privacy policy</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">We keep our privacy policy under regular review. If we change our privacy policy we will post the changes on this page, and place notices on other pages of the Site, so that you may be aware of the information we collect and how we use it at all times. This privacy policy was last updated on 27th day of August 2015.</span></p>
                                <p> </p>
                                <p style="text-align: left;"><strong><span style="color: #3f3f3f;">
                                    </span></strong>
                                </p>
                                <h3><strong><span style="color: #1f497d;">10. &nbsp;How to contact us</span></strong></h3>
                                <strong>
                                </strong>
                                <p> </p>
                                <p style="text-align: left;"><span style="font-size: 18px; color: #3f3f3f;">We welcome your views about our Site and our privacy policy. If you would like to contact us with any queries or comments please send an email to support@kapitalboost.com.</span></p>
                                <p><br />
                                </p-->
                            </div>
                        </div>
                    </div>
                </div>
</center>
            </div>