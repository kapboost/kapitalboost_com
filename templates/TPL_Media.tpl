<link rel="stylesheet" href="assets/css/kp-media.css" />
<div id="media" style="min-height: none;">
    <div id="media-1">
        <div id="media-1-back" style="background: white;padding: 25px 0;">
            <div id="media-1-inner">
<!--
                <h1 id="media-1-h1"><span style="font-size: 28px;">Media</span></h1>
-->
<h2 class="allNewTitle">Media</h2>
                <div><span style="font-size: 18px;"></span></div>
                
                <!--
                {section name=outer loop=$content}
                <div><span style="font-size: 18px;"><a href="http://{$content[outer].media_url}" target="_blank"><img alt="" src="../../assets/images/media/{$content[outer].image}" class="mediaImage" style="border: 0px solid; width: 30%; height: 30%;" /></a></span></div><br/>
                {/section} 
                -->
                <center>
                 <div class="mediaImageLoop">
                    <ul class="mediaImageUl">
                        {section name=outer loop=$content}
                        <li>
                            <a href="{$content[outer].media_url}" target="_blank">
                                <img alt="" src="../../assets/images/media/{$content[outer].image}" class="mediaImage" />
                            </a>
                        </li>
                        {/section}
                    </ul>
                 </div>
                 </center>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="assets/js/kp-media.js"></script>