<section class="new-get-funded">
	<div class="jumbotron" style="background-image: url('https://kapitalboost.com/staging/valuepenguin/images/hd-gambar.jpg');">
		<div class="overlay">
		  <div class="container">
		  	<img src="https://kapitalboost.com/staging/valuepenguin/images/kb-logo.png" class="get-funded-logo" alt="Kapital Boost Logo">
		  	<div class="row">
		  		<div class="col-md-6">
		  			<h1 class="color-white">Grow BIG Together</h1>
						<p class="lead color-white">Kapital Boost adalah platform peer to peer financing yang menghubungkan UKM dengan investor untuk mendapatkan bagi hasil yang adil dan transparan. Kami menggunakan skema keuangan syariah dengan persetujuan yang lebih mudah untuk membantu usahamu tumbuh besar.</p>
		  		</div>
		  		<div class="col-md-6 col-lg-5">
		  			<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="https://kapitalboost.com/state/st3/assets/img/why-us/3.png" alt="Fast and Simple">
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Cepat dan Mudah</h4>
						    <p>1 - 2 minggu pencairan setelah dokumen lengkap.</p>
						  </div>
						</div>
						<div class="media">
						  <div class="media-left">
						    <a href="#">
						      <img class="media-object" src="https://kapitalboost.com/state/st3/assets/img/why-us/2.png" alt="Competitive Fund">
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Biaya yang Kompetitif</h4>
						    <p>(1,17% hingga 1,8% per bulan) + 3% hingga 5% platform fee</p>
						  </div>
						</div>
						<div class="media">
						  <div class="media-left">
						    <a href="#">

						      <img class="media-object" src="assets/images/jaminan-asset.png" alt="Fast and Simple">

						      <!-- <img class="media-object" src="https://kapitalboost.com/state/st3/assets/img/why-us/1.png" alt="Fast and Simple"> -->
						    </a>
						  </div>
						  <div class="media-body">
						    <h4 class="media-heading">Tanpa Jaminan Aset</h4>
						    <p>Pembiayaan tanpa jaminan aset hanya dengan invoice.</p>
						  </div>
						</div>
		  		</div>
		  	</div>
		  </div>
		</div>
	</div>

	<section id="requirements">
		<div class="container">
			<div class="row">
				<div class="col-lg-12" align="center">
					<!-- <h2 class="section-title">Ajukan Pembiayaan</h2> -->
					<p></p>
				</div>
				<div class="col-md-6">
					<h3>Kriteria kelayakan:</h3>
					<ul>
						<li>Berasal dari Indonesia</li>
						<li>Telah beroperasi selama lebih dari 1 tahun</li>
						<li>Penjualan tahunan lebih dari Rp. 1 Miliar</li>
						<li>Arus kas yang positif dalam 12 bulan terakhir</li>
					</ul>
				</div>

				<div class="col-md-6">
					<h3>Tujuan pembiayaan:</h3>
					<ul>
						<li>Pembelian aset (bahan baku, peralatan, persedian, dan lain-lain) berdasarkan pembelian / pesanan kerja yang ada</li>
						<li>Pembiayaan modal kerja (invoice financing) dari perusahaan besar yang terpercaya</li>
						<li>Pembiayaan jangka pendek dengan durasi kurang dari enam bulan</li>
					</ul>
				</div>

				<div class="col-lg-12 contact">
					<p>Untuk informasi lebih lanjut hubungi kami di +62 812 8787 4136/+62 21 540 1817 atau email hello@kapitalboost.com.</p>
				</div>
			</div>
		</div>
	</section>

	<div id="form" class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="section-title">Ajukan Pembiayaan</h2>
				<br>
			</div>
			<div class="col-lg-12">
				<form id="getfundedform" style="width:100%" name="getfundedform" method="post" enctype="multipart/form-data" action="getfunded/register" onsubmit="return checkCheckBoxes(this);">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022884">Nama Lengkap <span class="req">*</span></label>
								{if $member_id}
									<input type="text" maxlength="4000" name="full_name" class="form-control" value="{$member_firstname} {$member_surname}" readonly/>
									<input type="hidden" name="owner" value="{$member_name}"/>
								{else}
									<input type="text" maxlength="4000" name="full_name" class="form-control" value="" required />
								{/if}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="EmailAddress">Alamat Email <span class="req">*</span></label>
								{if $member_id}
									<input type="email" name="email" id="email" class="form-control" maxlength="255" value="{$member_email}" readonly/>
								{else}
									<input type="email" name="email" id="email" class="form-control" maxlength="255" value="" required />
								{/if}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022885">Nomor Telepon <span class="req">*</span></label>
								<input type="number" maxlength="4000" name="mobile" id="mobile" class="form-control" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022886">Nama Perusahaan<span class="req">*</span></label>
								<input type="text" maxlength="4000" name="company" class="form-control" required />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022889">Tahun Berdiri <span class="req">*</span></label>
								<input type="number" maxlength="255" name="year" class="form-control" required/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-xs-4">
									<div class="form-group">
										<label for="CAT_Custom_20022890">Mata Uang <span class="req">*</span></label>
										<select name="currency" class="form-control" required>
											<option value="">-- Pilih --</option>
											<option value="IDR">IDR</option>
											<option value="MYR">MYR</option>
											<option value="SGD">SGD</option>
										</select>
									</div>
								</div>
								<div class="col-xs-8">
									<div class="form-group">
										<label for="CAT_Custom_20022891">Pendapatan Tahunan <span class="req">*</span></label>
										<input type="number" maxlength="4000" name="est" class="form-control" required />
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022889">Jumlah Dana Yang di Ajukan <span class="req">*</span></label>
								<input type="text" name="fund" class="form-control" required/>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022895">Periode Pendanaan <span class="req">*</span></label>
								<select name="period" class="form-control" required>
									<option value="">-- Pilih --</option>
									<option value="2 months">2 Bulan</option>
									<option value="3 months">3 Bulan</option>
									<option value="4 months">4 Bulan</option>
									<option value="5 months">5 Bulan</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="CAT_Custom_20022895">Solusi pembiayaan apa yang anda cari?<span class="req">*</span></label>
								<select name="fin_type" class="form-control" required>
									<option value="">-- Pilih --</option>
									<option value="Invoice Financing">Pembiayaan Modal Kerja</option>
									<option value="Asset Purchase Financing">Pembiayaan Pembelian Aset</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<!-- <label for="CAT_Custom_20022895"></label> -->
							<div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
							<p><span class="req">*</span> <i>Wajib</i></p>
						</div>
						<div class="col-md-6">
							<input type="checkbox" name="confirm" id="CAT_Custom_20022898_0" required/> <span class="req">*</span>Saya menyatakan bahwa informasi yang diberikan benar dan akurat.
							<br/>
							<input type="checkbox" name="termAndCondition" id="termAndCondition" required/> <span class="req">*</span>Saya telah membaca dan menyetujui <a href="https://kapitalboost.com/legal#term">Syarat Penggunaan</a> dan <a href="https://kapitalboost.com/legal#privacy">Kebijakan Privasi</a>.
						</div>

						<input type="hidden" name="source" value="/get-funded-new" />

						<div class="clear"></div>
						<div class="col-md-12" align="center">
							<button class="cat_button bluebtn" type="submit" id="getfunded-submit">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<section id="what-we-do">
		<div class="parallax-window" data-parallax="scroll" data-image-src="https://kapitalboost.com/staging/valuepenguin/images/bg-2.png"></div>

		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<iframe width="560" height="315" src="https://www.youtube.com/embed/PcIBqRsKRlc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="col-md-6">
					<h3>Tujuan Kami</h3>
					<p class="color-grey-light">Kapital Boost berusaha untuk mengatasi kurangnya pembiayaan untuk Usaha Kecil Menengah di Indonesia dan Singapura. Kami percaya UKM adalah pilar penting perekonomian negara, mereka harus terus dibantu untuk terus tumbuh.</p>
					<p class="color-grey-light">Kami memberikan alternatif pembiayaan untuk UKM yang lebih inklusif, adil dan transparan. Melalui struktur keuangan syariah, UKM yang potensial akan dihubungkan dengan ratusan investor dari berbagai negara di dunia untuk mendanai usahamu tumbuh besar. </p>
				</div>
			</div>
		</div>
	</section>

	<div id="featured-on" class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="section-title">Diliput Oleh</h2>
			</div>
			<div class="col-lg-12">
				<div class="features-on">
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/bh.png" alt="Logo Berita Harian">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/bloomberg.png" alt="Logo Bloomberg">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/crowdfund_insider.png" alt="Logo Crowdfunding">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/markets.png" alt="Logo The Edge Market">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/newbfm.png" alt="Logo BFM">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/salam.png" alt="Logo Global Islamic Economy Gateway">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/straits.png" alt="Logo The Straits">
					</div>
					<div class="item">
						<img src="https://kapitalboost.com/state/st3/assets/img/feature-on/techinasia.png" alt="Logo Techinasia">
					</div>
				</div>
			</div>
		</div>
	</div>

	<section id="testimonial">
		<div class="container">
			<div class="row">
				<div class="col-lg-12" align="center">
					<h2 class="section-title">Testimonial</h2>
				</div>
			</div>
			<div class="row list-testinomial">
				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Disamping persyaratannya yang tidak ribet, biaya pembiayaannya juga masih masuk akal. Bekerja sama dengan Kapital Boost juga memberikan kenyamanan karena sesuai syariah.</p>
						<h5>Abdul Rahman Hantiar, Pemilik Usaha</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_30.png" alt="" class="round"></span></center>
					</div>
				</div>

				<div class="item col-lg-12">
					<div class="testi">
						<p class="caption">Kapital Boost menyediakan alternatif pembiayaan bank yang sangat dibutuhkan oleh para pengusaha. Mereka memberikan pembiayaan yang cepat untuk perusahaan yang sedang berkembang cepat. Selain itu investor bisa berinvestasi di usaha yang menarik dan menjadi bagian dari kesuksesan mereka.</p>
						<h5>Djuan Onn Abdul Rahman, Pemilik Usaha</h5>
						<center><span><img src="https://kapitalboost.com/assets/images/testimonial/testimonial_25.jpg" alt="" class="round"></span></center>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="container" id="compliance">
		<div class="row">
			<div class="col-lg-12" align="center">
				<h2 class="section-title">Sertifikasi Syariah</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 item img">
				<img src="https://kapitalboost.com/assets/images/FSAC-logo.png" alt="FSAC logo">
			</div>
			<div class="col-md-7 item">
				<p>Struktur Crowdfunding Murabahah yang digunakan Kapital Boost telah disertifikasi sesuai oleh Financial Shariah Advisory & Consultancy (FSAC), sebuah unit konsultasi dari Pergas Investment Holdings (Singapura).</p>
			</div>
		</div>
	</div>

	<section id="funded-button">
		<div class="container">
			<div class="row">
				<div class="col-sm-6" align="center">
					<p class="lead"><b>Mulai ajukan pendanaan</b></p>
				</div>
				<div class="col-sm-6">
					<a href="/get-funded-new#form" class="btn btn-primary btn-block"><b>AJUKAN SEKARANG</b></a>
				</div>
			</div>
		</div>
	</section>
</section>

<script type="text/javascript">
	$("#menu").hide();

	$(document).ready(function() {
		$('#getfunded-submit' ).click(function() {
			fbq('track', 'GetFunded', {
        content_name: 'Get funded submit',
      });

			// $("#getfundedform").submit();
		});

		$('.parallax-window').parallax({
			speed: 0.7
		});

		$(".list-testinomial").slick({
			dots: true,
			autoplay: true,
			buttons: true
		});

		$('.features-on').slick({
			infinite: true,
		  slidesToShow: 5,
		  slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 2000,
			centerPadding: '30',
		  arrows: false,
			responsive: [
		    {
		      breakpoint: 640,
		      settings: {
		        centerMode: true,
		        centerPadding: '0',
		        slidesToShow: 3
		      }
		    },
		  ]
		});
	})
</script>
