<!DOCTYPE html>
<html lang="en" xmlns:og="http://ogp.me/ns#" prefix="og: http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
       <head>
              <style>.async-hide { opacity: 0 !important} </style>
              <script>{literal}(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
              h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
              (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
              })(window,document.documentElement,'async-hide','dataLayer',4000,
              {'GTM-NNCH57C':true});{/literal}</script>
              <base href="{if $smarty.server.HTTPS eq 'on'}{$smarty.const.SERVER_BASE_SECURE}{else}{$smarty.const.SERVER_BASE}{/if}">
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <script src='https://www.google.com/recaptcha/api.js'></script>
              <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">

              {if isset($content.blog_title)}
                <meta name="title" content="{$content.blog_title}"/>
                <meta name="description" content="{$content.meta_description|strip_tags|strip|truncate:240}"/>
                <meta name="keywords" content="{$content.meta_keywords}"/>
              {else}
                <meta name="title" content="{if $pg_title}{$pg_title}{else}{$page_title}{/if}"/>
                <meta name="description" content="{if $meta_descr}{$meta_descr|strip_tags|strip|truncate:240}{else}{$meta_desc|strip_tags|strip|truncate:240}{/if}"/>
                <meta name="keywords" content="{$meta_keyword}"/>
              {/if}
              <meta name="author" content="">
              <link rel="shortcut icon" href="ico/favicon.ico">

              <title>{if $pg_title}{$pg_title}{else}{$page_title}{/if}</title>

              <script src="https://use.fontawesome.com/a1f9958de3.js"></script>

              {if isset($content.blog_title)}
                <!-- FB -->
                <meta property='fb:app_id' content="1419559388071407"/>
                <meta property='og:type' content="website" />
                <meta property='og:image' content="{if $content.image}https://kapitalboost.com/assets/images/blog/{$content.image}{else}https://kapitalboost.com/assets/images/Slide1.jpg{/if}" />
                <meta property='og:title' content="{$content.blog_title}" />
                <meta property='og:site_name' content="Kapital Boost Crowdfunding" />
                <meta property='og:description' content="{$content.meta_description|strip_tags|strip|truncate:240}" />
                <meta property='og:url' content="https://kapitalboost.com/blog/{$content.slug}" />
                <meta property='og:image:width' content="336" />
                <meta property='og:image:height' content="201" />

                <!-- Twitter -->
                <meta name="twitter:card" content="summary_large_image" />
                <!-- <meta name="twitter:site" content="summary" /> -->
                <meta name="twitter:creator" content="@kapitalboost" />

              {else}
                <!-- FB -->
                <meta property='fb:app_id' content="1419559388071407"/>
                <meta property='og:type' content="website" />
                <meta property='og:image' content="https://kapitalboost.com/assets/images/kp-fav.png" />
                <meta property='og:title' content="{if $pg_title}{$pg_title}{else}{$page_title}{/if}" />
                <meta property='og:site_name' content="Kapital Boost Crowdfunding" />
                <meta name="description" property='og:description' content="{if $meta_descr}{$meta_descr|strip_tags|strip|truncate:240}{else}{$meta_desc|strip_tags|strip|truncate:240}{/if}" />
                <meta property='og:url' content="{if $og_url}{$og_url}{else}https://kapitalboost.com/{/if}" />
                <meta property='og:image:width' content="336" />
                <meta property='og:image:height' content="201" />

                <!-- Twitter -->
                <meta name="twitter:card" content="summary_large_image" />
                <!-- <meta name="twitter:site" content="summary" /> -->
                <meta name="twitter:creator" content="@kapitalboost" />
              {/if}


              <script type="text/javascript">var jslang = 'EN';</script>
              <link rel="shortcut icon" type="assets/image/png" href="assets/images/kp-fav.png" />
              <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
              <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap-theme.min.css" />
              <link rel="stylesheet" href="assets/css/new-kp-main.css" />
              <link rel="stylesheet" href="assets/css/new-kp-home.css" />
              <link rel="stylesheet" href="assets/css/kp-about.css" />
              <link rel="stylesheet" href="assets/css/kp-default.css" />
              <link rel="stylesheet" href="assets/css/kp-blog.css" />
              <link rel="stylesheet" href="assets/css/kp-event.css" />
              <link rel="stylesheet" href="assets/css/get-funded.css" />
              <!-- <link rel="stylesheet" href="http://localhost:3000/assets/css/get-funded.css" /> -->

              <link href="//cdn-images.mailchimp.com/embedcode/horizontal-slim-10_7.css" rel="stylesheet" type="text/css">
              <link rel="stylesheet" href="assets/css/Box-vs=b1967.r482151-phase1.css" type="text/css" media="screen" />
              <script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>
              <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
              <script type="text/javascript" src="assets/js/Java_Box.js?vs=b1967.r482151-phase1"></script>
              <link rel="stylesheet" href="assets/css/lightbox.min.css" />
              <link rel="stylesheet" type="text/css" href="assets/css/jssocials.css" />
              <link rel="stylesheet" type="text/css" href="assets/css/jssocials-theme-flat.css" />

              <style type="text/css">

                #mc_embed_signup{
                  background: rgba(52,140,247,1);
                  background: -moz-linear-gradient(-45deg, rgba(52,140,247,1) 0%, rgba(39,64,230,1) 100%);
                  background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(52,140,247,1)), color-stop(100%, rgba(39,64,230,1)));
                  background: -webkit-linear-gradient(-45deg, rgba(52,140,247,1) 0%, rgba(39,64,230,1) 100%);
                  background: -o-linear-gradient(-45deg, rgba(52,140,247,1) 0%, rgba(39,64,230,1) 100%);
                  background: -ms-linear-gradient(-45deg, rgba(52,140,247,1) 0%, rgba(39,64,230,1) 100%);
                  background: linear-gradient(135deg, rgba(52,140,247,1) 0%, rgba(39,64,230,1) 100%);
                  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#348cf7', endColorstr='#2740e6', GradientType=1 );

                  clear:left; font:14px Helvetica,Arial,sans-serif;
                  width:100%;
                  padding-top: 40px;
                  padding-bottom: 40px;
                }

                #mc_embed_signup h3 {
                  font-size: 24px;
                  font-weight: normal;
                  line-height: 28px;
                  margin-bottom: 20px;
                  text-align: center;
                  color: #fff;
                }

                #mc_embed_signup p {
                  font-size: 14px;
                  font-weight: normal;
                  line-height: 18px;
                  color: #fff;
                }

                #mc_embed_signup input.email {
                  box-shadow: none;
                  width: 100%;
                  display: block;
                  margin-bottom: 7px;
                  border-radius: 0;
                  padding: 25px 10px;
                  border-color: white;
                  background: white;
                }

                #mc_embed_signup .clear {
                  display: block;
                }

                #mc_embed_signup .button:hover {
                  background: #d35400;
                }

                #mc_embed_signup .button {
                  background: #e67e22;
                  width: 100%;
                  height: 45px;
                  border-radius: 0;
                }
                /* Add your own Mailchimp form style overrides in your site stylesheet or in this style block.
                We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
              </style>

			  <!-- crazyegg -->
			  <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0075/8729.js" async="async"></script>


		<!-- Global site tag (gtag.js) - Google Analytics -->
              <script async src="https://www.googletagmanager.com/gtag/js?id=UA-88456967-1"></script>
              <script>
                     {literal}
                             window.dataLayer = window.dataLayer || [];
                             function gtag(){dataLayer.push(arguments);}
                             gtag('js', new Date());

                             gtag('config', 'UA-88456967-1');
                     {/literal}
              </script>
              <!-- <script>
                     {literal}
                            (function (i, s, o, g, r, a, m) {
                                   i['GoogleAnalyticsObject'] = r;
                                   i[r] = i[r] || function () {
                                          (i[r].q = i[r].q || []).push(arguments)
                                   }, i[r].l = 1 * new Date();
                                   a = s.createElement(o),
                                           m = s.getElementsByTagName(o)[0];
                                   a.async = 1;
                                   a.src = g;
                                   m.parentNode.insertBefore(a, m)
                            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

                            ga('create', 'UA-88456967-1', 'auto');
                            ga('send', 'pageview');

                     {/literal}
              </script> -->

              <!-- Facebook Pixel Code -->
              <script>
                     {literal}
                            !function (f, b, e, v, n, t, s) {
                                   if (f.fbq)
                                          return;
                                   n = f.fbq = function () {
                                          n.callMethod ?
                                                  n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                                   };
                                   if (!f._fbq)
                                          f._fbq = n;
                                   n.push = n;
                                   n.loaded = !0;
                                   n.version = '2.0';
                                   n.queue = [];
                                   t = b.createElement(e);
                                   t.async = !0;
                                   t.src = v;
                                   s = b.getElementsByTagName(e)[0];
                                   s.parentNode.insertBefore(t, s)
                            }(window,
                                    document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
                            fbq('init', '203885370040865'); // Insert your pixel ID here.
                            fbq('track', 'PageView');
                     {/literal}
              </script>
              <noscript>
              {literal}
              <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=203885370040865&ev=PageView&noscript=1">
       {/literal}
       </noscript>

              <!-- Facebook Pixel Code -->
              <script>
                     {literal}
                           !function(f,b,e,v,n,t,s)
 {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
 n.callMethod.apply(n,arguments):n.queue.push(arguments)};
 if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
 n.queue=[];t=b.createElement(e);t.async=!0;
 t.src=v;s=b.getElementsByTagName(e)[0];
 s.parentNode.insertBefore(t,s)}(window, document,'script',
 'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1003903116332179');
 fbq('track', 'PageView');
                     {/literal}
              </script>
              <noscript>
              {literal}
              <img height="1" width="1" style="display:none"
 src="https://www.facebook.com/tr?id=1003903116332179&ev=PageView&noscript=1"
/>
       {/literal}
       </noscript>

       <!-- DO NOT MODIFY -->
       <!-- End Facebook Pixel Code -->

       <script>
              {literal}
                     window.fbAsyncInit = function () {
                            FB.init({
                                   appId: '261151691029080',
                                   cookie: true,
                                   xfbml: true,
                                   version: 'v2.8'
                            });
                            FB.AppEvents.logPageView();
                     };
                     function checkLoginState() {
                            $("#fbCover").fadeIn();
                            $("#lightbox").fadeOut(100);
                            $("#reg-lightbox").fadeOut(100);

                            mixpanel.track("User Login via Facebook");

                            FB.getLoginStatus(function (response) {
                                   if (response.status === 'connected') {
                                          var token = response.authResponse.accessToken;
                                          FB.api('/me?fields=id,name,gender,location', function (response) {
                                                 console.log(JSON.stringify(response));
                                                 FB.api('/me/permissions', function (response) {
                                                        var email = 1;
                                                        for (i = 0; i < response.data.length; i++) {
                                                               if (response.data[i].permission == 'email' && response.data[i].status == 'declined') {
                                                                      $("#fbCover").fadeOut();
                                                                      alert("Please provide us your email address via Facebook login");
                                                                      email = 0;
                                                               }
                                                        }
                                                        if (email == 1) {
                                                               $.post("index.php?class=Member&method=Fblogin", {token: token}, function (reply) {
                                                                      if (reply == "1") {
                                                                             location.reload();
																			  //window.location.href = "https://kapitalboost.com/member/";
                                                                      }
                                                               });
                                                        }

                                                 });

                                          });
                                   }
                            });
                     }

                     (function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) {

                                   return;
                            }
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js";
                            fjs.parentNode.insertBefore(js, fjs);
                     }(document, 'script', 'facebook-jssdk'));
              {/literal}
       </script>

       <script src="https://kapitalboost.com/staging/valuepenguin/js/parallax.min.js"></script>

       <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
       <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
       <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
       <!-- start Mixpanel --><script type="text/javascript">{literal}(function(c,a){if(!a.__SV){var b=window;try{var d,m,j,k=b.location,f=k.hash;d=function(a,b){return(m=a.match(RegExp(b+"=([^&]*)")))?m[1]:null};f&&d(f,"state")&&(j=JSON.parse(decodeURIComponent(d(f,"state"))),"mpeditor"===j.action&&(b.sessionStorage.setItem("_mpcehash",f),history.replaceState(j.desiredHash||"",c.title,k.pathname+k.search)))}catch(n){}var l,h;window.mixpanel=a;a._i=[];a.init=function(b,d,g){function c(b,i){var a=i.split(".");2==a.length&&(b=b[a[0]],i=a[1]);b[i]=function(){b.push([i].concat(Array.prototype.slice.call(arguments,
0)))}}var e=a;"undefined"!==typeof g?e=a[g]=[]:g="mixpanel";e.people=e.people||[];e.toString=function(b){var a="mixpanel";"mixpanel"!==g&&(a+="."+g);b||(a+=" (stub)");return a};e.people.toString=function(){return e.toString(1)+".people (stub)"};l="disable time_event track track_pageview track_links track_forms track_with_groups add_group set_group remove_group register register_once alias unregister identify name_tag set_config reset opt_in_tracking opt_out_tracking has_opted_in_tracking has_opted_out_tracking clear_opt_in_out_tracking people.set people.set_once people.unset people.increment people.append people.union people.track_charge people.clear_charges people.delete_user people.remove".split(" ");
for(h=0;h<l.length;h++)c(e,l[h]);var f="set set_once union unset remove delete".split(" ");e.get_group=function(){function a(c){b[c]=function(){call2_args=arguments;call2=[c].concat(Array.prototype.slice.call(call2_args,0));e.push([d,call2])}}for(var b={},d=["get_group"].concat(Array.prototype.slice.call(arguments,0)),c=0;c<f.length;c++)a(f[c]);return b};a._i.push([b,d,g])};a.__SV=1.2;b=c.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?
MIXPANEL_CUSTOM_LIB_URL:"file:"===c.location.protocol&&"//cdn4.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn4.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn4.mxpnl.com/libs/mixpanel-2-latest.min.js";d=c.getElementsByTagName("script")[0];d.parentNode.insertBefore(b,d)}})(document,window.mixpanel||[]);
mixpanel.init("732fb0e27a81ef50aebfa7f3e478dd20");{/literal}</script><!-- end Mixpanel -->
</head>

<body>

       <div id="main">
              {$Header}

              <div id="container1">

                     {$ContentMiddle}
              </div>

              {$Footer}
       </div>
       <div id="fbCover" style="display:none">
              <div style="margin-top:200px;text-align:center">
                     Logging in with Facebook account. Please wait.
              </div>
       </div>
       <div id="lightbox">
              <div id="login-box">
                     <div id="login-top">
                            <a id="close-box">X</a>
                     </div>
                     <form name="loginform" id="loginform" method="post" action="">
                            <input id="redirect-eth-logs" type="hidden" name="redirect" value=""/>


                            <div class="form">
                                   <div id="login-middle">
                                          <img id="login-logo" alt="" src="assets/images/logo-baru.png" />
                                          <h1 id="login-h1">{if $la=='id'}{else}Investor Login{/if}</h1>
                                          <input id="login-email" name="member_username" type="text" placeholder="Username or Email address" autocomplete="off" />
                                          <input id="login-pass" name="member_password" type="password" placeholder="Password" />
                                   </div>
                                   <div>
                                          <div id="log-bottom-1">
                                                 <input id="login-submit" value="Log In" type="submit" />
                                          </div>

                                          <div id="log-bottom-2"><a id="login-forgot" href="forgot" onclick="return forgotForm();" >{if $la=='id'}{else}Forgot your password?{/if}</a>
                                                 <br>{if $la=='id'}{else}Not registered?{/if} <a id="reg-log" href="reg-log" onclick="return logReg();"><b>{if $la=='id'}{else}Sign up now{/if}</b></a>
                                          </div>
                                   </div>

                            </div>
                            <hr id="loginHr">
                            <p style="text-align:center">OR</p>
                            <div id="loginFb" scope="public_profile,email,user_location" auth_type="rerequest" class="fb-login-button" onlogin="checkLoginState();" data-max-rows="1" data-width="100%" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" style="text-align:center;width:100%"></div>

                     </form>
              </div>
       </div>
       <div id="lightbox-campaign" style="display:none">
              <div id="login-box">
                     <div id="login-top">
                            <a id="close-box">X</a>
                     </div>
                     <form name="loginform" id="loginform" method="post" action="">
                            <input id="redirect-eth-logs" type="hidden" name="redirect" value=""/>


                            <div class="form">
                                   <div id="login-middle">
                                          <img id="login-logo" alt="" src="assets/images/logo-baru.png" />
                                          <h1 id="login-h1">{if $la=='id'}{else}<br /> The crowdfunding campaigns are only accessible to registered members. <br /> Please proceed to login or register an account with us.{/if}</h1>
                                          <input id="login-email" name="member_username" type="text" placeholder="Username or Email address" autocomplete="off" />
                                          <input id="login-pass" name="member_password" type="password" placeholder="Password" />
                                   </div>
                                   <div>
                                          <div id="log-bottom-1">
                                                 <input id="login-submit" value="Log In" type="submit" />
                                          </div>

                                          <div id="log-bottom-2"><a id="login-forgot" href="forgot" onclick="return forgotForm();" >{if $la=='id'}{else}Forgot your password?{/if}</a>
                                                 <br>{if $la=='id'}{else}Not registered?{/if} <a id="reg-log" href="reg-log" onclick="return logReg();"><b>{if $la=='id'}{else}Sign up now{/if}</b></a>
                                          </div>
                                   </div>

                            </div>
                            <hr id="loginHr">
                            <p style="text-align:center">OR</p>
                            <div id="loginFb" scope="public_profile,email,user_location" auth_type="rerequest" class="fb-login-button" onlogin="checkLoginState();" data-max-rows="1" data-width="100%" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" style="text-align:center;width:100%"></div>

                     </form>
              </div>
       </div>

       <div id="loginError" class="logpopupWrap">
              <div class="logpopupInner">
                     <div class="logpopupClose">
                            <span class="glyphicon glyphicon-remove"></span>
                     </div>
                     <div class="logpopupTitle" style="text-align:left">
                     {if $la=='id'}{else}You have not verified your account. To do so, please click the link on the Account Activation email
                            (it might be in your Spam folder) that was sent to you shortly after you created an account with us. <br>
                            If you'd like us to re-send the Account Activation email, please fill in your email address below and click submit
                     {/if}
              </div>
              <div><input type="text" placeholder="Email Address" id="loginErrorEmail"/></div>
              <div><button class="btn btn-primary" id="loginErrorSubmit">{if $la=='id'}{else}SUBMIT{/if}</button></div>

       </div>
</div>



<div id="reg-lightbox">
       <div id="reg-lightbox-inner" >

              <div id="reg-lightbox-main" >
                     <div id="reg-lightbox-close">
                            <span id="reg-close" class="glyphicon glyphicon-remove"></span>
                     </div>
                     <form name="registerform" id="registerform" method="post" action="index.php?class=Member&method=RegisterForm" >

                            <div class="regt" style="width:100%">{if $la=='id'}{else}Membership Registration{/if}</div>

                            <div  style="width:100%" data-width="100%" scope="public_profile,email,user_location" auth_type="rerequest" class="fb-login-button" onlogin="checkLoginState();" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false" ></div>

                            <hr style="width:100%;margin:10px auto 10px;border-color:#404040">
                            <p style="text-align:center;margin-bottom:0;margin-top:10px">OR</p>


                            <div id="usernameExist">
                                   <p>
                                   {if $la=='id'}{else}The Username you've chosen is taken. <br>
                                          Please use another one.{/if}
                                   </p>
                            </div>
                            <div id="emailexist">
                                   <p>
                                   {if $la=='id'}{else}You already have an account in Kapital Boost. <br />
                                          Please click the button below to retrieve your password.{/if}
                                   </p>

                                   <div class="column-full">
                                          <input type="button" href="/member/forgot-password" value="Forgot Password" id="gotoforgot"/>
                                   </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-row">
                                   <div class="column-one">
                                          <div class="reg-title" >
                                                 <select name="member_gender" id="member_gender" class="reg_dropdown" required>
                                                        <option value="">{if $la=='id'}{else}Title{/if}</option>
                                                        <option value="mr">{if $la=='id'}{else}Mr.{/if}</option>
                                                        <option value="mrs">{if $la=='id'}{else}Mrs.{/if}</option>
                                                        <option value="ms">{if $la=='id'}{else}Ms.{/if}</option>
                                                 </select>
                                          </div>
                                   </div>
                                   <div class="column-three">
                                          <input placeholder="First Name" type="text"  name="member_firstname" class=" reg_textbox" value="" required id="regis4" />
                                   </div>

                            </div>
                            <div class="form-row">
                                   <div class="column-full">
                                          <input placeholder="Last Name" type="text" name="member_surname" class=" reg_textbox" value="" id="regis0" required/>
                                   </div>
                            </div>
                            <div class="form-row">
                                   <div class="column-full">
                                          <select name="member_country" class=" member_country" id="member_kota" data-live-search="true" data-style="btn-default" required>
                                                 <option value="">Select your country</option>
                                                 <option value="SINGAPORE">SINGAPORE</option>
                                                 <option value="INDONESIA">INDONESIA</option>
                                                 {html_options options=$member_country_options selected=$member_country}
                                          </select>
                                   </div>
                            </div>

                            <div class="form-row">
                                   <div class="column-full">
                                          <input placeholder="Unique Username" type="text" name="member_username" class=" reg_textbox" value="" id="regis1" required/>
                                   </div>
                            </div>

                            <div class="form-row">
                                   <div class="column-full">
                                          <div class="reg-title"></div>
                                          <input placeholder="Email Address" type="text" name="member_email" class=" reg_textbox" value="" id="regis5" required>
                                   </div>
                            </div>

                            <div class="form-row">
                                   <div class="column-full">
                                          <div class="reg-title"></div>
                                          <input placeholder="Password - Minimum 8 Characters" type="password" name="member_password" class=" reg_textbox" value="" id="regis2" required/>
                                   </div>
                            </div>

                            <div class="form-row">
                                   <div class="column-full">
                                          <div class="reg-title"></div>
                                          <input placeholder="Confirm Password" type="password" name="retype_password" class=" reg_textbox" value="" id="regis3" required/>
                                   </div>
                            </div>
							<div class="form-row">
                                   <div class="column-full">
                                          <select name="how_you_know" class=" member_country" id="member_kota" data-live-search="true" data-style="btn-default" required>
                                                 <option value="">How did you know us?</option>
                                                  <option value="1">Search Engine</option>
												  <option value="2">Ads</option>
												  <option value="3">Social Media</option>
												  <option value="4">Friend/Family</option>
												  <option value="5">Community Event</option>
												  <option value="6">News/Blog/Magazine</option>
												  <option value="7">Other</option>

                                          </select>
                                   </div>
                            </div>
                            <div class="form-row">
                                   <div class="column-full">
                                          <div class="reg-title"></div>
                                          <input type="checkbox" name="confirm" id="CAT_Custom_20022898_0" required/> <span class="req">*</span>I agree to the <a href="https://kapitalboost.com/legal#term">Terms of Use</a> and <a href="https://kapitalboost.com/legal#privacy">Privacy Policy</a>
                                   </div>
                            </div>

                            <div class="form-row">
                                   <div class="column-full">
										  <br>
                                          <div class="reg-title"></div>
                                          <div class="g-recaptcha" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
										  <br>
                                   </div>
                            </div>

                            <div class="form-row">
                                   <br>
                                   <button
                                          id="register-submit"
                                          >
                                   {if $la=='id'}{else}Register{/if}

                            </button>
                            <br>


                     </div>


              </form>
       </div>


</div>
</div>
<div class="regThankWrap">
       <div class='regThankInner'>
              <div class='regThankClose'>
                     <span class='glyphicon glyphicon-remove'></span>
              </div>
              <div class='regThankTitle'>
              {if $la=='id'}{else}Thank you for registering!{/if}
       </div>
       <div class='regThankText'>
       {if $la=='id'}{else}A verification email has been sent to <font id="registeredEmail" style='color: #1eb2f6;'></font>. Please follow the instructions in the email to verify your account.   Remember to check the spam folder in case you don't see the email in your inbox.{/if}
</div>
</div>
</div>
<!-- Verification Successful -->
<div class="VerifySuccessfulWrap" style="display:none;">
       <div class='regThankInner'>
              <div class='regThankClose'>
                     <span class='glyphicon glyphicon-remove'></span>
              </div>
              <div class='regThankTitle'>
              {if $la=='id'}{else}Account Verified{/if}
       </div>
       <div class='regThankText'>
       <!--{if $la=='id'}{else}You may login either with your email address : {$email}. or your username : {$username} in the future.{/if}-->
	   Thank you for joining the community of investors at Kapital Boost. You can now view our SME campaigns that are open for investment on the website and Android App. To invest, you must first complete your profile by providing a few more information and documents. <a href="member/profile.php">Update your profile </a> now, or you may do so at a later time.
</div>
</div>
</div>
<!--Verification Failed -->
<div class="VerifyFailedWrap" style="display: none;">
       <div class='regThankInner'>
              <div class='regThankClose'>
                     <span class='glyphicon glyphicon-remove'></span>
              </div>
              <div class='regThankTitle'>
              {if $la=='id'}{else}Account Not Verified{/if}
       </div>
       <div class='regThankText'>
       {if $la=='id'}{else}There was an error while verifying your account. Please contact us.{/if}
</div>
</div>
</div>



<script type="text/javascript" src="assets/js/kp-main.js"></script>
<script type="text/javascript" src="assets/js/kp-default.js"></script>
<script type="text/javascript" src="assets/js/lightbox.min.js"></script>
{literal}
       <script type="text/javascript">
              function Check(str) {
                     var format = /[!$%=\[\]{};':"\\|<>]+/;
                     if (format.test(str)) {
                            return true;
                     } else {
                            return false;
                     }
              }



              $(function () {
                     var $doc = $(document);
                     var $rwd_shown = false;
                     var index = [];
                     $doc.on('tap click', '#rwd-menu', function () {
                            if (!$rwd_shown) {
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(0).fadeOut(100);
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(1).addClass('rwd-cross-1');
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(2).addClass('rwd-cross-2');
                                   $('#menu').addClass('menu-rwd-show');
                                   $rwd_shown = true;
                            } else {
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(0).fadeIn(100);
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(1).removeClass('rwd-cross-1');
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(2).removeClass('rwd-cross-2');
                                   $('#menu').removeClass('menu-rwd-show');
                                   $rwd_shown = false;
                            }
                     });

                     $('#login-submit').click(function (e) {
                            e.preventDefault();
                            $.post('index.php?class=Member&method=LoginForm', $('#loginform').serialize(), function (reply) {
                                   if (reply.status == 1) {
                                   		mixpanel.track("User Login", {
						                    "Email or Username": $("#login-email").val()
						                });

                                          if (reply.redirect == "https://kapitalboost.com/") {
                                                 location.reload();
												 //window.location.href = "https://kapitalboost.com/member/";
                                          } else {
                                                 window.location.href = reply.redirect;
												  //window.location.href = "https://kapitalboost.com/member/";
                                          }
                                   } else if (reply.status == 2) {
                                          $("#lightbox").hide();
                                          $("#loginError").fadeIn();
                                   } else {
                                          alert(reply.error);
                                   }
                            }, 'json');
                     });

                     //
                     $('#login-submit2').click(function (e) {
                            e.preventDefault();

                            $.post('index.php?class=Member&method=LoginForm', $('#loginform').serialize(), function (reply) {
                                   if (reply.status) {
                                          window.location.href = reply.redirect;
                                   } else {
                                          alert(reply.error);
                                   }
                            }, 'json');
                     });
                     //

                     $("#loginErrorSubmit").on("click", function (e) {
                            e.preventDefault();
                            var email = $("#loginErrorEmail").val();
                            if (email != "") {
                                   $.post("index.php?class=Member&method=Reactivate", {email: email}, function (reply) {
                                          var repli = JSON.parse(reply);
                                          console.log(repli.status);
                                          if (repli.status == "0") {
                                                 alert("This email address is not in our record");
                                          } else if (repli.status == "1") {
                                                 alert("This account has already been verified");
                                          } else {
                                                 $("loginError").fadeOut();
                                                 alert("An activation email has been sent to your email");
                                          }
                                   });
                            } else {
                                   alert("Please enter your email address");
                            }

                     });

                     $('#emailexist').hide();
                     $("#usernameExist").hide();





                     $doc.on('click', '#kap-header-arr', function () {
                            $("html, body").animate({
                                   scrollTop: $('#home-2').offset().top - 70
                            }, 600);
                     });


                     $doc.on('click', '#eth-log-2', function () {
                            $('#lightbox').fadeIn();
                     });

                     $doc.on('click', '.eth-logs', function () {
                            var index = $(this).index();
                            var parentIndex = $(this).parent().index();
                            if (parentIndex != 4) {
                                   if (campaigns[index] == null || campaigns[index] == "") {
                                          redirect = "https://kapitalboost.com/campaign/";
                                   } else {
                                          redirect = "https://kapitalboost.com/campaign/view/" + campaigns[index];
                                   }
                            } else {
                                   if (campaigns[index] == null || campaigns[index] == "") {
                                          redirect = "https://kapitalboost.com/campaign/";
                                   } else {
                                          redirect = "https://kapitalboost.com/campaign/view/" + campaigns[index + 3];
                                   }
                            }

                            document.getElementById('redirect-eth-logs').value = redirect;
                     });

                     $doc.on('click', '.eth-log-privates', function () {
                            var index = $(this).index();
                            if (campaigns[index] == null || campaigns[index] == "") {
                                   redirect = "https://kapitalboost.com/campaign/";
                            } else {
                                   redirect = "https://kapitalboost.com/campaign/view/" + campaigns[index];
                            }
                            document.getElementById('redirect-eth-log-privates').value = redirect;
                     });


                     $doc.on('click', '#eth-log', function () {
                            $('#lightbox').fadeIn();

                     });

                     $doc.on('click', '#eth-log-campaign', function () {
                            $('#lightbox-campaign').fadeIn();

                     });
                     $doc.on('click', '#reg', function () {
                            $('#reg-lightbox').fadeIn();

                     });

                     $doc.on('click', '#close-box', function () {
                            $('#lightbox').fadeOut();


                     });




                     $doc.on('click', '#real-close-box', function () {

                            $('#real-lightbox').fadeOut();

                     });

                     $doc.on('click', '#kap-header-button-1', function () {

                            $('#vid-lightbox').fadeIn();

                     });

                     $doc.on('click', '#reg-close', function () {

                            $('#reg-lightbox').fadeOut();

                     });

                     /*
                      $doc.on('click', '.invest-btn', function() {

                      $('#invest-lightbox').fadeIn();
                      $('#invest-con-1').show();

                      });

                      $doc.on('click', '#invest-close-box', function() {

                      $('#invest-lightbox').fadeOut();

                      });
                      */



                     var featuredlist = new Array('5880164', '5670036', '5821367');





              });

              $(document).ready(function() {
                $('.regThankClose').on('click', function () {
                   $('.regThankWrap').fadeOut(200);
                   $('.VerifySuccessfulWrap').fadeOut(200);
                   $('.VerifyFailedWrap').fadeOut(200);
                });
              })
       </script>
       <script type="text/javascript">
              $(document).ready(function () {
                     $('#close-box').click(function () {
                            $('#lightbox').fadeOut(800);
                     });
              });
       </script>
       <script>
              $(window).load(function () {

                     $('.campaignWrap').show();
                     $('#dasMenu2').hide();

                     verifiedEmail = QueryString.email;
                     verifiedUsername = QueryString.username;
                     if ("undefined" === typeof verifiedEmail) {
                     } else {
                            if (verifiedEmail != "failed") {
                                   // $(".VerifySuccessfulWrap").fadeIn(200).delay(5000).fadeOut(300);
                                   $(".VerifySuccessfulWrap").fadeIn(200);
                            } else {
                                   // $(".VerifyFailedWrap").fadeIn(200).delay(5000).fadeOut(300);
                                   $(".VerifyFailedWrap").fadeIn(200);
                            }
                            removeParam("email");
                            removeParam("username");
                     }

                     function moveDiv() {
                            var autoTop = $('#home-1-inner').height();
                            var objTop = $('.kapHeaderSliderHome').height();

                            $('.kapHeaderSliderHome').css('top', (autoTop / 2) - (objTop / 2));
                     }

                     moveDiv();

                     $(window).resize(function () {
                            moveDiv();
                     });


                     function moveDivEtc() {
                            var autoTopBottom = $('.allBanner').height();
                            var autoRightLeft = $('.allBanner').width();
                            var objTopBottom = $('.allTitle').height();
                            var objRightLeft = $('.allTitle').width();

                            $('.allTitle').css('top', (autoTopBottom / 2) - (objTopBottom / 2)).css('left', (autoRightLeft / 2) - (objRightLeft / 2));
                     }

                     moveDivEtc();

                     $(window).resize(function () {
                            moveDivEtc();
                     });

                     $(document).ready(function () {
                            $('#dasMenu1').click(function () {
                                   $('#dasMenu1').hide();
                                   $('#dasMenu2').show();
                                   $('.dasUl').slideDown();
                            });

                            $('#dasMenu2').click(function () {
                                   $('#dasMenu2').hide();
                                   $('#dasMenu1').show();
                                   $('.dasUl').slideUp();
                            });
                     });


                     var $win = $(window);
                     var $home_gallery = $('#home-gallery');
                     var $home_gallery_each = $('.home-gallery-each');
                     var $home_gallery_canvas = $('#home-gallery-canvas');
                     var $home = $('#home');
                     var $home_1 = $('#home-1-inner');
                     var $home_1_inner = $('#home-1');
                     var $home_2 = $('#home-2');
                     var $home_2_map = $('#home-2-map');
                     var $galstat = 1;
                     var $gallength = $home_gallery_each.length;


                     $('.home-gallery-each').each(function () {

                            var $src = $(this).find('.home-gallery-each-src').val();
                            $(this).css('background-image', 'url(' + $src + ')');

                     });

                     var gallery_loop = function () {

                            $home_gallery_each.fadeOut(800);
                            $home_gallery.find('.home-gallery-each:nth-child(' + $galstat + ')').fadeIn(800);

                            if ($galstat >= $gallength) {
                                   $galstat = 1;
                            } else {
                                   $galstat++;
                            }

                            setTimeout(gallery_loop, 8000);
                     };

                     gallery_loop();

                     var bannerTo = 1;
                     var bannerPrev;
                     var bannerNext;

                     $('.kapHeaderSlider:nth-child(1)').fadeIn(800);

                     setInterval(function () {
                            if (bannerTo > 2) {
                                   bannerTo = 1;
                                   bannerPrev = bannerTo - 1;
                                   bannerNext = bannerTo + 1;
                                   $('.kapHeaderSlider:nth-child(' + bannerTo + ')').fadeIn(800);
                                   $('.kapHeaderSlider:nth-child(' + bannerPrev + ')').fadeOut(800);
                                   $('.kapHeaderSlider:nth-child(3)').fadeOut(800);
                            } else {
                                   bannerTo++;
                                   bannerPrev = bannerTo - 1;
                                   bannerNext = bannerTo + 1;
                                   $('.kapHeaderSlider:nth-child(' + bannerTo + ')').fadeIn(800);
                                   $('.kapHeaderSlider:nth-child(' + bannerPrev + ')').fadeOut(800);
                                   $('.kapHeaderSlider:nth-child(' + bannerNext + ')').fadeOut(800);
                            }
                     }, 8000);
              });
       </script>

       <script>
              $(function () {

                     var today = new Date();
                     var firstDate = new Date(today.getFullYear(), today.getMonth() + 1, today.getDate());
                     var oneDay = 24 * 60 * 60 * 1000;

                     $('.campaigns-list-con').each(function () {
                            var $bar_width = 0;

                            var $curr_amt = $(this).find('.curr-amount').val();
                            var $total_amt = $(this).find('.total-amount').val();

                            var $bar_width = ($curr_amt / $total_amt) * 100;

                            if ($bar_width > 100)
                                   $bar_width = 100;

                            $(this).find('.campaigns-bar-inner').animate({
                                   'width': $bar_width + '%'
                            }, 500);

                     });


              });
       </script>

       <script type="text/javascript">
              $(function () {

                     var $win = $(window);
                     var $doc = $(document);
                     var $stat = 0;
                     var $doc = $(document);
                     var $rwd_shown = false;
                     $doc.on('tap click', '#rwd-menu', function () {

                            if (!$rwd_shown) {

                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(0).fadeOut(100);
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(1).addClass('rwd-cross-1');
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(2).addClass('rwd-cross-2');
                                   $('#menu').addClass('menu-rwd-show');
                                   $rwd_shown = true;
                            } else {

                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(0).fadeIn(100);
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(1).removeClass('rwd-cross-1');
                                   $('#rwd-menu-inner').children('.rwd-menu-each').eq(2).removeClass('rwd-cross-2');
                                   $('#menu').removeClass('menu-rwd-show');
                                   $rwd_shown = false;
                            }
                     });

              });
       </script>
       <script type="text/javascript">
              function logReg() {
                     $('#lightbox').fadeOut();
                     $('#reg-lightbox').fadeIn();
              }
              function forgotForm() {
                     window.location.href = 'member/forgot-password';
              }
       </script>

       <script type="text/javascript">
              $(document).ready(function () {
                     $('#close-box').click(function () {
                            $('#lightbox').fadeOut(800);
                     });
              });
       </script>

       <script type="text/javascript">
              $(document).ready(function () {
                     $('#image-popup').click(function () {
                            $('#image-popup-wadah').fadeIn(800);
                     });

                     $('#close-popup').click(function () {
                            $('#image-popup-wadah').fadeOut(800);
                     });

                     $('#image-popup-s').click(function () {
                            $('#image-popup-wadah-s').fadeIn(800);
                     });

                     $('#close-popup-s').click(function () {
                            $('#image-popup-wadah-s').fadeOut(800);
                     });
              });
              $(document).ready(function () {
                     $('#dropContent1').click(function () {
                            $('#dropContent1').hide();
                            $('#dropContent2').show();
                            $('#dropItem').slideDown();
                     });

                     $('#dropContent2').click(function () {
                            $('#dropContent2').hide();
                            $('#dropContent1').show();
                            $('#dropItem').slideUp();
                     });

                     $('.HIYDropMenuItem > li').click(function () {
                            $('#dropContent1').hide();
                            $('#dropContent2').show();
                            $('#dropItem').slideUp();
                     });
              });

              $(document).ready(function () {
                     $('a[href^="how-it-works#"]').on('click', function (e) {
                            e.preventDefault();

                            var target = this.hash,
                                    $target = $(target);

                            $('html, body').stop(true, true).animate({
                                   'scrollTop': $target.offset().top - 90
                            }, 900, 'swing');
                     });
              });



              /* start watch video */
              $(document).ready(function () {
                     $('#VWButton').click(function () {
                            $('.watchVideo').fadeIn(800);
                     });

                     $('.VWClose').click(function () {
                            $('.watchVideo').fadeOut(800);
                     });
              });
              /* end watch video */



              /* start new header */
              $(document).ready(function () {
                     $('#newOpen').click(function () {
                            $('#newOpen').hide();
                            $('#newClose').show();
                            $('.newMenu').slideDown();
                     });

                     $('#newClose').click(function () {
                            $('#newClose').hide();
                            $('#newOpen').show();
                            $('.newMenu').slideUp();
                     });

                     $('.newMenu').mouseup(function () {
                            return false;
                     });
                     $(document).mouseup(function () {
                            $('.newMenu').slideUp();
                     });
              });
              /* end new header */



              /* start contact us */
              $(document).ready(function () {
                     $(window).load(function () {
                            $('.contactWrap').fadeIn(200);
                            $('.contactClose').click(function () {
                                   $('.contactWrap').fadeOut(200);
                            });
                     });
              });
              /* end contact us */

              //start menu slide
              $(document).ready(function () {
                     $('#menu_slide').click(function () {
                            var $this = $(this);
                            var $target = $this.attr('class');

                            if ($target == 'plus') {
                                   $('#menu_slide').attr('class', 'minus').text('-');
                            }
                            if ($target == 'minus') {
                                   $('#menu_slide').attr('class', 'plus').text('+');
                            }

                            $('.howUl, .howUlFaq').slideToggle();
                     });
              });
			  $(document).ready(function () {
				  $("label").tooltip({
					'selector': '',
					'placement': 'top',
					'container':'body'
				  });
				});

				$('.auto-tooltip').tooltip();
              //end menu slide
       </script>

       <!-- getsitecontrol -->
       <script>
              (function (w, i, d, g, e, t, s) {
                     w[d] = w[d] || [];
                     t = i.createElement(g);
                     t.async = 1;
                     t.src = e;
                     s = i.getElementsByTagName(g)[0];
                     s.parentNode.insertBefore(t, s);
              })(window, document, '_gscq', 'script', '//widgets.getsitecontrol.com/66008/script.js');

              $(document).ready(function() {
                path = window.location.pathname;
                path = path.replace("/", '');
                path = path.replace(/-/g, ' ');
                mixpanel.track("go to " + path + " page");

                $("#registerform").submit(function() {
                  mixpanel.track("User Registration", {
                    "Full Name": $("[name='member_firstname']").val() + ' ' + $("[name='member_surname']").val(),
                    "Email Address": $("[name='member_email']").val(),
                    "username": $("#regis1").val(),
                    "country": $("[name='member_country']").val()
                  });
                });
              });
       </script>

       <!-- <script type="text/javascript">var subscribersSiteId='473323a3-66fa-4a19-9984-8898b112c5f3';</script><script type="text/javascript" src="https://cdn.subscribers.com/assets/subscribers.js"></script> -->

{/literal}

{if $member_id}
<div id="info-member" data-id-member="{$member_id}" data-name-member="{$member_name}"></div>
  {literal}
    <script type="text/javascript">
      $(document).ready(function() {
        $id = $("#info-member").data("id-member");
        $name = $("#info-member").data("name-member");
        mixpanel.identify($id);

        mixpanel.people.set({
           "$name": $name
        });
      })
    </script>
  {/literal}
{/if}
</body>
</html>
