<link rel="stylesheet" type="text/css" href="/xfers/assets/css/gaya.css">

<div class="partnersWrap" ng-app="xfers" ng-controller="xfersCtrl">
       <div class="partnersInnerDas" style="width: 100%; margin: 0px">
              <center>
                     <!-- start xfers transferinfo -->
                     <div class="xfersThankYou">
                            <div class="XTYInner">
                                   {if $error_uploading == 0}
                                          <p>There's an error while uploading. Please contact us.</p>
                                   {else}
                                          <div class="XTYText">


                                                 <p>
                                                        Thank you for providing the proof of payment for {if $campaign_tipe == "donation"}{if $campaign_subtype != ""}funding{else}donation{/if}{else}investment{/if} in the <b>{$campaign_name}</b> campaign.
                                                 </p>


                                          </div>
                                          <div class="XTYText2">
                                                 <p>
                                                        Our team will confirm and update your payment status shortly, which will be reflected on the <a href="https://kapitalboost.com/dashboard/payment-history">Portfolio </a> page of your Dashboard.

                                                 </p>

                                                 <p>
                                                        If you have any questions, please email us at
                                                        <span>
                                                               <a href="mailto:support@kapitalboost.com">support@kapitalboost.com</a>
                                                        </span>
                                                 </p>
                                          </div>

                                          <div class="XTYText4">
                                                 <p>
                                                        {if $campaign_tipe== "donation"}
                                                               Spread the goodness and let your friends and family know by sharing this project
                                                        {else}

                                                               Help support the project by letting others know
                                                        {/if}
                                                 </p>
                                          </div>
                                          <ul class="XTYLink">
                                                 <li>
                                                        <a href="https://plus.google.com/share?url=https://kapitalboost.com/campaign/view/{$campaign_id}">
                                                               <img src="/xfers/assets/img/linkGoogle.jpg" />
                                                        </a>
                                                 </li>
                                                 <li>
                                                        <a href="https://www.facebook.com/sharer/sharer.php?u=https://kapitalboost.com/campaign/view/{$campaign_id}">
                                                               <img src="/xfers/assets/img/linkFacebook.jpg" />
                                                        </a>
                                                 </li>
                                                 <li>
                                                        <a href="https://twitter.com/intent/tweet?text=&url=https://kapitalboost.com/campaign/view/{$campaign_id}">
                                                               <img src="/xfers/assets/img/linkTwitter.jpg" />
                                                        </a>
                                                 </li>
                                                 <li>
                                                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=https://kapitalboost.com/campaign/view/{$campaign_id}">
                                                               <img src="/xfers/assets/img/linkLinkedIn.jpg" />
                                                        </a>
                                                 </li>
                                          </ul>

                                   {/if}
                            </div>
                     </div>
                     <!-- end x fers transferinfo -->


              </center>
       </div>
</div>

