<!-------------------------------------------- main content area wrapper -------------------------------------------->
<div class="subpage-main-content-area-wrapper">
       <div class="subpage-content-wrapper subpage-content-wrapper-2">
              <div class="inner-container">

                     <div class="checkout-receipt-area-wrapper">
                            <!-------------------------------------------- main content left area wrapper -------------------------------------------->
                            <div class="main-content-left-area-wrapper">
                                   <!--------------------------------------- receipt section --------------------------------------->
                                   <div class="panel-wrapper checkout-receipt-wrapper">
                                          <div class="panel-header-wrapper top-panel-header">
                                                 <div class="panel-inner">Payment failed</div>
                                          </div>
                                          <br>
                                          <div class="panel-inner checkout-receipt-content-wrapper">
                                                 <p class="checkout-receipt-main-header" style="color:#ff0000">Your order payment is Failed</p><br>
                                                 <p><font color="black">Please fell free to contact  our Internet Team at <strong class="c-green">607-2243663</strong> for any assistance. Our bussiness operating hour are from Monday to Friday between 8.30am to 2pm.</font></p><br>
                                                 <p><font color="black">
                                                        Best regards,<br>
                                                        <span class="checkout-receipt-name">Temple Project Team.</span>
                                                        <br><br><br><br><br><br>
                                                        </font></p>
                                          </div>
                                   </div>
                                   <!--------------------------------------- //receipt section --------------------------------------->
                                   <br>

                            </div>
                            <!-------------------------------------------- //main content left area wrapper -------------------------------------------->



                            <!-------------------------------------------- main content right area wrapper -------------------------------------------->
                            <div class="main-content-right-area-wrapper">
                                   <div class="main-content-right-area-wrapper-inner">
                                          <!--------------------------------------- checkout sidebar area wrapper --------------------------------------->
                                          <div class="checkout-sidebar-banner-area-wrapper">
                                                 <!------------------------ banner ------------------------>
                                                 <div class="checkout-sidebar-banner-wrapper">
                                                        <a href="catalog/mothers-day"><img src="assets/images/banner-aux-left.jpg"></a>
                                                 </div>
                                                 <!------------------------ //banner ------------------------>
                                          </div>
                                          <!--------------------------------------- //checkout sidebar area wrapper --------------------------------------->
                                   </div>
                            </div>
                            <!-------------------------------------------- //main content right area wrapper -------------------------------------------->
                            <div class="clearfix"></div>


                     </div>

              </div>    
       </div>
       <div class="main-footer-area-wrapper-top"></div>
</div>
<!-------------------------------------------- //main content area wrapper -------------------------------------------->

<script>
       {
              literal
       }
       $(document).ready(function () {
              $('#cart_total_change').html('0');
       });
       {
              /literal}
</script>