{$pagetop}


<center>
       <div class="partnersWrap">
              <div class="partnersInner" style="text-align: left;">

                     <h2 class="allNewTitle">FAQs <span id="menu_slide" class="plus">+</span></h2>
                     <center>
                            <ul class="nav nav-tabs howUlFaq" id="investor-tabs">
                                   <li class="active"><a href="#for-small" data-toggle="tab">SME crowdfunding</a></li>
                                   <li><a href="#donation" data-toggle="tab">Donation</a></li>
                                   <!-- <li><a href="#private" data-toggle="tab">Private crowdfunding</a></li> -->
                                   <li><a href="#e-wallet" data-toggle="tab">e-Wallet</a></li>
                            </ul>
                     </center>
                     <div class="tab-content">
                            <div class="tab-pane active" id="for-small">
                                   <h3><strong><span class="allContentTitleFaq">For SMEs</span></strong></h3><br>
                                   {section name=sme loop=$faqSmeHeaderA}
                                          <div class="FAQAsk">
                                                 <h3><span class="allContentTextEtc">
                                                               <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq00{$smarty.section.sme.index}" aria-expanded="true">
                                                                      <em>{$faqSmeHeaderA[sme]}</em>
                                                               </a>
                                                        </span>
                                                 </h3>
                                          </div>

                                          <div class="panel-collapse collapse FAQAns" id="faq00{$smarty.section.sme.index}">
                                                 <div class="panel-body">
                                                        <span class="allContentText" style="color: #3f3f3f">
                                                               {$faqSmeContentA[sme]}
                                                        </span>
                                                 </div>
                                          </div>
                                          <br>
                                   {/section}
                                   <h3><strong><span class="allContentTitleFaq">For Financiers (Members)</span></strong></h3><br>
                                   {section name=member loop=$faqMemberHeaderA}
                                          <div class="FAQAsk">
                                                 <h3><span class="allContentTextEtc">
                                                               <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq11{$smarty.section.member.index}" aria-expanded="true">
                                                                      <em>{$faqMemberHeaderA[member]}</em>
                                                               </a>
                                                        </span>
                                                 </h3>
                                          </div>

                                          <div class="panel-collapse collapse FAQAns" id="faq11{$smarty.section.member.index}">
                                                 <div class="panel-body">
                                                        <span class="allContentText" style="color: #3f3f3f">
                                                               {$faqMemberContentA[member]}
                                                        </span>
                                                 </div>
                                          </div>
                                          <br>
                                   {/section}
                            </div>

                            <div class="tab-pane" id="donation">
                                   <!--<h3><strong><span class="allContentTitleFaq">For Donation Crowdfunding</span></strong></h3>--><br>
                                   {section name=don loop=$faqDonationHeaderA}
                                          <div class="FAQAsk">
                                                 <h3><span class="allContentTextEtc">
                                                               <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq22{$smarty.section.don.index}" aria-expanded="true">
                                                                      <em>{$faqDonationHeaderA[don]}</em>
                                                               </a>
                                                        </span>
                                                 </h3>
                                          </div>

                                          <div class="panel-collapse collapse FAQAns" id="faq22{$smarty.section.don.index}">
                                                 <div class="panel-body">
                                                        <span class="allContentText" style="color: #3f3f3f">
                                                               {$faqDonationContentA[don]}
                                                        </span>
                                                 </div>
                                          </div>
                                          <br>
                                   {/section}

                            </div>
                            <div class="tab-pane" id="private">
                                   <!--<h3><strong><span class="allContentTitleFaq">For Private Crowdfunding</span></strong></h3>--><br>
                                   {section name=private loop=$faqPrivateHeaderA}
                                          <div class="FAQAsk">
                                                 <h3><span class="allContentTextEtc">
                                                               <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq33{$smarty.section.private.index}" aria-expanded="true">
                                                                      <em>{$faqPrivateHeaderA[private]}</em>
                                                               </a>
                                                        </span>
                                                 </h3>
                                          </div>

                                          <div class="panel-collapse collapse FAQAns" id="faq33{$smarty.section.private.index}">
                                                 <div class="panel-body">
                                                        <span class="allContentText" style="color: #3f3f3f">
                                                               {$faqPrivateContentA[private]}
                                                        </span>
                                                 </div>
                                          </div>
                                          <br>
                                   {/section}
                            </div>
                            <div class="tab-pane" id="e-wallet">
                                   <h3><strong><span class="allContentTitleFaq">e-Wallet</span></strong></h3><br>
                                   {section name=wallet loop=$faqXfersHeaderA}
                                          <div class="FAQAsk">
                                                 <h3><span class="allContentTextEtc">
                                                               <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq44{$smarty.section.wallet.index}" aria-expanded="true">
                                                                      <em>{$faqXfersHeaderA[wallet]}</em>
                                                               </a>
                                                        </span>
                                                 </h3>
                                          </div>

                                          <div class="panel-collapse collapse FAQAns" id="faq44{$smarty.section.wallet.index}">
                                                 <div class="panel-body">
                                                        <span class="allContentText" style="color: #3f3f3f">
                                                               {$faqXfersContentA[wallet]}
                                                        </span>
                                                 </div>
                                          </div>
                                          <br>
                                   {/section}
                            </div>
                     </div>
              </div>

</center>
