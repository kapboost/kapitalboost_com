{$pagetop}

{literal}

       <script>
              $(document).ready(function () {

                     var hash = location.hash;

                     console.log(hash);
                     if(hash == '#term'){
                            openTerm();
                     }else if (hash == '#privacy'){
                            openPrivacy();
                     }else if(hash == '#risk'){
                            openRisk();
                     }


                     $("#legal-term").on('click', function () {
                            openTerm();
                     });
                     $("#legal-privacy").on('click', function () {
                            openPrivacy();
                     });
                     $("#legal-risk").on('click', function () {
                            openRisk();
                     });

              });

              function openTerm() {
                     
                     $("#term").addClass("active");
                     $("#termLink").addClass("active");
                     $("#privacy").removeClass("active");
                     $("#privacyLink").removeClass("active");
                     $("#risk").removeClass("active");
                     $("#riskLink").removeClass("active");
              }
              function openPrivacy() {
                     
                     $("#term").removeClass("active");
                     $("#termLink").removeClass("active");
                     $("#privacy").addClass("active");
                     $("#privacyLink").addClass("active");
                     $("#risk").removeClass("active");
                     $("#riskLink").removeClass("active");
              }
              function openRisk() {
                     
                     $("#term").removeClass("active");
                     $("#termLink").removeClass("active");
                     $("#privacy").removeClass("active");
                     $("#privacyLink").removeClass("active");
                     $("#risk").addClass("active");
                     $("#riskLink").addClass("active");
              }


       </script>

{/literal}

<div id="container">
       <center>
              <div class="partnersWrap">
                     <div class="partnersInner">

                            <h1 class="allNewTitle">Legal<span id="menu_slide" class="plus">+</span></h1>

                            <ul class="nav nav-tabs howUl" id="tabs">
                                   <li class="active" id="termLink"><a href="https://kapitalboost.com/legal#term" data-toggle="tab">Terms of Use</a></li>
                                   <li id="privacyLink" class=""><a href="https://kapitalboost.com/legal#privacy" data-toggle="tab">Privacy Policy</a></li>
                                   <li class="" id="riskLink"><a href="https://kapitalboost.com/legal#risk" data-toggle="tab">Risk Statement</a></li>
                            </ul>

                            <div class="tab-content" id="myTabContent">
                                   <div class="tab-pane active" id="term">
                                          <h1 style="margin:50px auto"><strong>Terms of Use</strong></h1>

                                          {$legalContent[0]}


                                   </div>

                                   <div class="tab-pane" id="privacy">
                                          <h1 style="margin:50px auto"><strong>Privacy Policy</strong></h1>

                                          {$legalContent[1]}

                                   </div>

                                   <div class="tab-pane" id="risk">
                                          <h1 style="margin:50px auto"><strong>Risk Statement</strong></h1>

                                          {$legalContent[2]}

                                   </div>
                            </div>
                     </div>
              </div>
       </center>

</div>
