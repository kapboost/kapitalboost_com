{if $member_id}

       <link rel="stylesheet" href="/assets/css/manage-profile.css" />
       <form id="getform" action="https://kapitalboost.com/dashboard/update-profile"  method="POST" class="dasCampaign" style="text-align: left;" enctype="multipart/form-data" autocomplete="off">
              <div id="about-1" style="min-height: 850px;height: auto; margin-top: 40px;padding-bottom: 0;">
                     <div style="text-align:center;background-color:#f2f2f2;padding-bottom:40px">
                            {if $query && $link2==NULL}
                                   <div class="alert alert-success" style="margin:80px auto auto auto; width: 30%;text-align: center">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                   {if $la == 'id'}{else}Your profile has been updated!{/if}
                            </div>
                     {/if}

                     <div id="about-1-inner" style="display: inline-block;padding-bottom: 20px;">

                            <h2 style="font-size: 40px;color: #646c6f;font-weight: bold;font-family: Lato;margin:30px">{if $la == 'id'}{else}Your Profile{/if} </h2>

                            <div class="col-xs-12 col-md-6" style="z-index:3000;text-align: left">

                                   <div class="form-group">
                                          <label>{if $la == 'id'}{else}Country of Residence : {/if}</label>
                                          <select name="current_location" id="country" class="cat_dropdown">
                                                 <option value="">{if $la == 'id'}{else}Select your country{/if}</option>
                                                 <option value='SINGAPORE'>SINGAPORE</option>
                                                 <option vlaue='INDONESIA'>INDONESIA</option>
                                                 {html_options options=$member_country_options selected=$current_location}
                                          </select>
                                   </div>

                                   <div class="form-group" >
                                          <label>{if $la == 'id'}{else}Username : {/if}</label>
                                          <input type="text"  name="uname" id="uname" value="{$member_name}" class="form-control" {if $member_name != ''}readonly{/if} placeholder="Unique User Name">
                                          <img id="unameload" src="/assets/images/loading1.gif" class="unameicons">
                                          <img id="unametick" src="/assets/images/tick.png" class="unameicons" >
                                          <img id="unamecross" src="/assets/images/cross.jpg" class="unameicons" >
                                          <p id="uname-error" style="padding:0;margin:0;color:red"></p>
                                   </div>

                                   <div class="form-group">
                                          <label>{if $la == 'id'}{else}First Name : {/if}</label>
                                          <input type="text" name="firstname" id="firstname" value="{$member_firstname}" class="form-control" required>
                                   </div>
                                   <div class="form-group">
                                          <label>{if $la == 'id'}{else}Last Name : {/if}</label>
                                          <input type="text" name="lastname" id="lastname" value="{$member_surname}" class="form-control"  required autocomplete="off">
                                   </div>

                                   <div class="form-group">
                                          <label>{if $la == 'id'}{else}Email : {/if}</label>
                                          <input id="xu_email" type="text" name="email" value="{$member_email}" class="form-control" readonly >
                                   </div>


                            </div>

                            <div class="col-xs-12 col-md-6" style="z-index:3000;text-align: left">
                                   <div class="form-group">
                                          <label>{if $la == 'id'}{else}Nationality : {/if}</label>
                                          <select name="nationality" id="nationality" class="cat_dropdown">
                                                 <option value="">{if $la == 'id'}{else}Select your nationality{/if}</option>
                                                 {html_options options=$member_nationality_options selected=$member_nationality}
                                          </select>
                                   </div>
                                   <div class="form-group" >
                                          <label>{if $la == 'id'}{else}Date of Birth (DD/MM/YYYY): {/if}</label>
                                          <input type="text" id="dob" name="dob" value="{$dob}" class="form-control" required>
                                   </div>
                                   <div class="form-group" >
                                          <label>{if $la == 'id'}{else}Residential Address  : {/if}</label>
                                          <input type="text" name="resarea" id="address" value="{$resarea}" class="form-control" required>
                                   </div>
                                   <div class="form-group" >
                                          <label>{if $la == 'id'}{else}Handphone No.  : {/if}</label>
                                          <input type="text" id="phone_current_user" name="nope" value="{$member_mobile}" class="form-control" required>
                                   </div>
                                   <div class="form-group">
                                          <label>{if $la == 'id'}{else}Password : {/if}</label>
                                          <input type="password" name="password"  class="form-control" placeholder="Change Your Password" autocomplete="off" />
                                          <input type="hidden" name="hiddenpassword" value="{$member_password}" />
                                   </div>

                            </div>
                     </div>

                     <div id="about-1-inner1">
                            <div class="row" style="text-align: left;">
                                   <hr>
                                   <div class="col-xs-12 col-md-6">
                                          <div class="form-group">
                                                 <label>{if $la == 'id'}{else}NRIC/Passport No : {/if}</label>
                                                 <input type="text" id="nric" name="nric" value="{$nric}" class="form-control" required>
                                          </div>
                                          <div class="form-group" >
                                                 <label>{if $la == 'id'}{else}Type of Identification Number :{/if}</label>
                                                 <select name="ic_option" id="ic_option" class="cat_dropdown">
                                                        <option value="">{if $la=='id'}{else}Select the type of identification number{/if}</option>
                                                        {html_options options=$ic_options selected=$ic_option}
                                                 </select>
                                          </div>
                                   </div>

                                   <div class="col-xs-12 col-md-6">
                                          <div class="form-group" >
                                                 <label>{if $la == 'id'}{else}Full name as shown in Passport / ID : {/if}</label>
                                                 <input type="text" id="ic_name" name="ic_name" value="{$member_icname}" class="form-control" placeholder="Require for the contract" required>
                                          </div>

                                          <div class="form-group" >
                                                 <label>{if $la == 'id'}{else}Issuing country : {/if}</label>
                                                 <select name="ic_country" id="ic_country" class="cat_dropdown">
                                                        <option value="">{if $la == 'id'}{else}Select your issuing country{/if}</option>
                                                        {html_options options=$member_country_options selected=$ic_country}
                                                 </select>
                                          </div>
                                   </div>
                            </div>
                     </div>
                     <div class="container">
                            <div class="row">
                                   <hr>
                                   {if $filenric}
                                          <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" style="padding:0">
                                                 <label  class="profileUploadHeaders">{if $la == 'id'}{else}Change NRIC (front)/passport <br> *please only use *.jpg or *.png file {/if}</label>
                                                 <br />
                                                 <input class="profileUploadFile" type="file" id="nric_file" name="nric_file" />
                                                 <br>
                                                 <img src="/assets/nric/datanric/{$filenric}"  width="85%"  />
												 {if $checkLastExtFront='.pdf'} <a href='/assets/nric/datanric/{$filenric}'> Download NRIC (front) </a>{/if}
                                          </div>
                                   {else}
                                          <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" style="padding:0">
                                                 <label class="profileUploadHeaders">{if $la == 'id'}{else}Upload NRIC  (front)/passport<br> *please only use *.jpg or *.png file{/if}</label>
                                                 <br />
                                                 <input class="profileUploadFile" type="file" id="nric_file" name="nric_file" required/>
                                                 <br>
                                          </div>
                                   {/if}
                                   {if $filenricback}
                                          <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" style="padding:0">
                                                 <label class="profileUploadHeaders">{if $la == 'id'}{else}Change NRIC (back) <br> *please only use *.jpg or *.png  file{/if}</label>
                                                 <div style="margin-top:-20px"><input type="checkbox" name="passport" id="passport" {if $filenricback == 'passport.png'}checked{/if} value="passport" /> {if $la == 'id'}{else}Not applicable for passport{/if}</div>
                                                 <br>
                                                 <input class="profileUploadFile" type="file" id="nric_file_back" name="nric_file_back" />
                                                 <br>
                                                 {if $filenricback != 'passport.png'}<img src="/assets/nric/datanricback/{$filenricback}"  width="85%"  />{/if}
												 {if $checkLastExtBack='.pdf'} <a href='/assets/nric/datanricback/{$filenricback}'> Download NRIC (back) </a>{/if}
                                          </div>
                                   {else}
                                          <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" style="padding:0">
                                                 <label class="profileUploadHeaders">{if $la == 'id'}{else}Upload NRIC (back)<br> *please only use *.jpg or *.png file {/if}</label>
                                                 <div style="margin-top:-20px"><input type="checkbox" name="passport" id="passport" value="passport"/> {if $la == 'id'}{else}Not applicable for passport{/if}</div>
                                                 <br>
                                                 <input class="profileUploadFile" type="file" id="nric_file_back" name="nric_file_back" />
                                                 <br>
                                          </div>
                                   {/if}

                                   {if $addressProof}
                                          <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" style="padding:0">
                                                 <label  class="profileUploadHeaders">
                                                 {if $la == 'id'}{else}Change proof of address <br> *please only use *.jpg, *.png or *.pdf file {/if}<span class="glyphicon glyphicon-info-sign addressproof" aria-hidden="true"></span>
                                                 <span class="addressprofftooltips">{if $la == 'id'}{else}Provide a picture of your utility bill, phone bill, or bank statement that is less than three months old. Make sure that the date is shown in the picture.{/if}</span>
                                          </label>
                                          <br />
                                          <input class="profileUploadFile" type="file" name="address_proof" id="address_proof" />
                                          <br>
                                          <img src="/assets/nric/addressproof/{$addressProof}"  width="85%" />
										  {if $checkLastExtBack='.pdf'} <a href='/assets/nric/addressproof/{$addressProof}'> Download Address Proof </a>{/if}
                                   </div>
                            {else}
                                   <div class="col-md-4 col-xs-12 col-lg-4 col-sm-4" style="padding:0">
                                          <label  class="profileUploadHeaders">
                                          {if $la == 'id'}{else}Upload proof of address <br> *please only use *.jpg, *.png or *.pdf file{/if}<span class="glyphicon glyphicon-info-sign addressproof" aria-hidden="true"></span>
                                          <span class="addressprofftooltips">{if $la == 'id'}{else}Provide a picture of your utility bill, phone bill, or bank statement that is less than three months old. Make sure that the date is shown in the picture.{/if}</span>
                                   </label>
                                   <br />
                                   <input class="profileUploadFile" type="file" name="address_proof" required id="address_proof"/>
                                   <br>
                            </div>
                     {/if}

              </div>
              <div class="row">
                     <hr><br>
                     <div class="col-md-6 col-xs-12 col-lg-6 col-sm-12" style="text-align: left;">

                            <input class="ver-inv" type="hidden" value="" />
                            <label class="labelIn2">{if $la == 'id'}{else}Which of these financial products have you invested in?{/if}</label>
                            <div class="checkIn"> 
                                   <br />

                                   <input type="checkbox" class="ticks" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks" {if $ceek1}checked{/if}/><span>{if $la == 'id'}{else}Stocks{/if}</span> <br>
                                   <input type="checkbox" class="ticks" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" {if $ceek2}checked{/if}/><span>{if $la == 'id'}{else}Bonds{/if}</span> <br>                                                        
                                   <input type="checkbox" class="ticks" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" {if $ceek3}checked{/if}/><span>{if $la == 'id'}{else}Mutual funds{/if}</span> <br>
                                   <input type="checkbox" class="ticks" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" {if $ceek4}checked{/if}/><span>{if $la == 'id'}{else}Alternative investments (specify){/if}</span> <br>
                                   <input type="checkbox" class="ticks" name="check_6" id="CAT_Custom_20047261_157746_4" value="None of the above" {if $ceek5}checked{/if}/><span>{if $la == 'id'}{else}None of the above{/if}</span> <br>

                            </div>
                     </div>
                     <div class="col-md-6 col-xs-12 col-lg-6 col-sm-12" style="text-align:left;">
                            <input class="ver-inv" type="hidden" value="" />
                            <label class="labelIn2">State your interest in investing through Kapital Boost</label>
                            <div class="checkIn">
                                   <br />

                                   <input type="checkbox" class="ticks" name="check_7" id="CAT_Custom_20043779_157746_0" value="Attractive returns" {if $ceek6}checked{/if}/><span>{if $la == 'id'}{else}Attractive returns{/if}</span><br>
                                   <input type="checkbox" class="ticks" name="check_8" id="CAT_Custom_20043779_157746_1" value="Short tenor" {if $ceek7}checked{/if}/><span>{if $la == 'id'}{else}Short tenor{/if}</span><br>
                                   <input type="checkbox" class="ticks" name="check_9" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" {if $ceek8}checked{/if} /><span>{if $la == 'id'}{else}Social/ethical reasons{/if}</span><br>                                   
                                   <input type="checkbox" class="ticks" name="check_10" id="CAT_Custom_20043779_157746_3" value="Investment diversification" {if $ceek9}checked{/if}/><span>{if $la == 'id'}{else}Investment diversification{/if}</span><br>
                                   <input type="checkbox" class="ticks" name="check_11" id="CAT_Custom_20043779_157746_4" value="Transparency" {if $ceek10}checked{/if}/><span>{if $la == 'id'}{else}Transparency{/if}</span><br>
                                   <input type="checkbox" class="ticks" name="check_12" id="CAT_Custom_20043779_157746_5" value="Others" {if $ceek11}checked{/if}/><span>{if $la == 'id'}{else}Others{/if}</span><br>

                            </div>
                     </div>

                     <div class="col-md-12" style="z-index:2000">
                            <hr>
                            <div class="invest-form-layer newLayer">
                                   <input style="float: left;" type="checkbox" name="check_13" id="CAT_Custom_20043781_157746_0" value="yes"{if $ceek12} checked{/if} required />
                                   <label style="float: left;max-width: 95%;margin: 0 0 0 5px;text-align: left" class="labelIn3">
                                   {if $la == 'id'}{else}I have read, understood and agreed to the <a href="https://kapitalboost.com/legal#term" target="_blank">Terms of Use</a>, <a href="https://kapitalboost.com/legal#privacy" target="_blank"> Privacy Policy </a> and <a href="https://kapitalboost.com/legal#risk" target="_blank">Risk Statement</a> set out by Kapital Boost.{/if}
                            </label>
                     </div>

                     <div class="invest-form-layer newLayer">
                            <input style="float: left;" type="checkbox" name="check_14" id="CAT_Custom_20043781_157746_1" value="yes"{if $ceek13} checked{/if} required />
                            <label style="float: left;max-width: 95%;margin: 0 0 0 5px;text-align: left;" class="labelIn3">
                            {if $la == 'id'}{else}I agree to share my personal information with Xfers, a third party payment gateway partner of Kapital Boost. For more information on Xfers, click <a target="_blank" href="https://xfers.io">here.</a>{/if}
                     </label>
              </div>

       </div>

       <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12"><input type="button" class="btn btn-primary" ng-click="XtoU(XupdateInfo)" name="button" id="button2" value="Update Profile" style="margin-top: 30px;"/></div>
</div>

</div>
</div>
</div>
</form>




{literal}
       <script type="text/javascript" language="JavaScript">

              var uniquename = '{/literal}{$member_name}{literal}';

              $(document).ready(function () {
                     var nricbackfile = 0;
                     var nricbackfileupload = 0;
                     var nricfrontfileupload = 0;
                     var addressfileupload = 0;
                     var passport = '{/literal}{$filenricback}{literal}';
                     var nric = '{/literal}{$filenric}{literal}';
                     var addressProof = '{/literal}{$addressProof}{literal}';

                     $("#nric_file_back").on("change", function () {
                            nricbackfile = 1;
                            nricbackfileupload = 1;
                            $("#nric_file_back").css("color", "");
                     });
                     $("#nric_file").on("change", function () {
                            nricfrontfileupload = 1;
                            $("#nric_file").css("color", "");
                     });
                     $("#address_proof").on("change", function () {
                            addressfileupload = 1;
                            $("#address_proof").css("color", "");
                     });

                     $("#button2").on("click", function (e) {
                            e.preventDefault();
                            if (passport != '') {
                                   nricbackfile = 1;
                                   if (passport == 'passport.png') {
                                          if (nricbackfileupload == 0) {
                                                 nricbackfileupload = 0;
                                          }
                                   } else {
                                          nricbackfileupload = 1;
                                   }
                            }

                            if ($("#passport").is(':checked')) {
                                   nricbackfile = 1;
                            } else {
                                   if (nricbackfileupload == 0) {
                                          nricbackfile = 0;
                                   } else {
                                          nricbackfile = 1;
                                   }
                            }
                            if ($("#uname").val() == "") {
                                   FieldEmpty($("#uname"), $("#uname").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#uname"));
                            }
                            if (uniquename == '') {
                                   alert("User Name must be unique");
                                   $(document).scrollTop(0);
                                   return;
                            }

                            if ($("#nric").val() == "") {
                                   FieldEmpty($("#nric"), $("#nric").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#nric"));
                            }

                            if ($("#firstname").val() == "") {
                                   FieldEmpty($("#firstname"), $("#firstname").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#firstname"));
                            }
                            if ($("#lastname").val() == "") {
                                   FieldEmpty($("#lastname"), $("#lastname").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#lastname"));
                            }
                            if ($("#dob").val() == "") {
                                   FieldEmpty($("#dob"), $("#dob").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#dob"));
                            }

                            if ($("#address").val() == "") {
                                   FieldEmpty($("#address"), $("#address").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#address"));
                            }
                            if ($("#phone_current_user").val() == "") {
                                   FieldEmpty($("#phone_current_user"), $("#phone_current_user").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#phone_current_user"));
                            }

                            if ($("#nationality").val() == "") {
                                   FieldEmpty($("#nationality"), $("#nationality").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#nationality"));
                            }

                            if ($("#ic_name").val() == "") {
                                   FieldEmpty($("#ic_name"), $("#ic_name").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#ic_name"));
                            }
                            
                            if ($("#ic_option").val() == "") {
                                   FieldEmpty($("#ic_option"), $("#ic_option").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#ic_option"));
                            }
                            
                            if ($("#ic_country").val() == "") {
                                   FieldEmpty($("#ic_country"), $("#ic_country").offset().top);
                                   return;
                            } else {
                                   FieldFilled($("#ic_country"));
                            }
                            
                            

                            if (nricbackfile == 0) {
                                   UploadEmpty($("#nric_file_back"), $("#nric_file_back").offset().top);
                                   alert("Please attach your NRIC (back) or check 'Not applicable for passport'");
                                   return;
                            }
                            if (nric == '' && nricfrontfileupload == 0) {
                                   UploadEmpty($("#nric_file"), $("#nric_file").offset().top);
                                   alert("Please attach NRIC picture");
                                   return;
                            }

                            if (addressProof == '' && addressfileupload == 0) {
                                   UploadEmpty($("#address_proof"), $("#address_proof").offset().top);
                                   alert("Please attach Proof of Address");
                                   return;
                            }

                            if ($("#CAT_Custom_20043781_157746_1").is(':checked')) {
                            } else {
                                   alert("Click on the checkbox to indicate that you have agreed to share information provided to Kapital Boost with Xfers.");
                                   return;
                            }
                            if ($("#CAT_Custom_20043781_157746_0").is(':checked')) {
                            } else {
                                   alert("Please check in the agreement checkbox");
                                   return;
                            }

                            if (nricbackfile == 1) {

                                   var allString = $("#nric").val() + $("#firstname").val() + $("#dob").val() + $("#lastname").val() + $("#address").val() + $("#phone_current_user").val();

                                   if (Check(allString)) {
                                          alert("Field names can't contain the following special characters: !$%_=\[\]{};':\"\|<>");
                                   } else {
                                          $("#getform").submit();

                                   }

                            }

                     });


                     $("#uname").on("input", function () {
                            var val = $(this).val();
                            if (val != "") {
                                   $("#unameload").show();
                                   $("#unametick").hide();
                                   $("#unamecross").hide();
                                   if (Check(val)) {
                                          $("#uname-error").text("No Special Characters Allowed");
                                          $("#unameload").hide();
                                          $("#unametick").hide();
                                          $("#unamecross").show();
                                   } else {
                                          $("#uname-error").text("");
                                          $.post("index.php?class=Member&method=CheckUName", {uname: val}, function (reply) {
                                                 if (reply == 0) {
                                                        $(this).css("border-color", "");
                                                        $("#uname-error").text("");
                                                        $("#unameload").hide();
                                                        $("#unametick").show();
                                                        $("#unamecross").hide();
                                                        uniquename = 1;
                                                 } else {
                                                        $("#uname-error").text("User Name is taken");
                                                        $("#unameload").hide();
                                                        $("#unametick").hide();
                                                        $("#unamecross").show();
                                                        uniquename = 0;
                                                 }
                                          });
                                   }
                            } else {
                                   $("#uname-error").text("");
                                   $("#unameload").hide();
                                   $("#unametick").hide();
                                   $("#unamecross").show();
                                   uniquename = 0;
                            }
                     });



              });

              function FieldEmpty(item, position) {
                     position = position - 150;
                     $("html,body").animate({scrollTop: position}, 500, function () {
                            item.css("border-color", "red");
                            item.attr("placeholder", "Please complete this field").val("").focus().blur();
                     });
              }
              function UploadEmpty(item, position) {
                     position = position - 150;
                     $("html,body").animate({scrollTop: position}, 500, function () {
                            item.css("color", "red");
                     });
              }
              function FieldFilled(item) {
                     item.css("border-color", "");
                     item.removeAttr('style');
              }


       </script>
{/literal}
{else}
       <div class="row" align="center">
              <div class="col-xs-12" style="text-align: center; color: red; margin-top:200px;">
                     <div class="alert alert-warning">
                            <div>You don't have permission!</div>
                     </div>
              </div>
       </div>
{/if}

{literal}
       <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.14/angular.min.js"></script>
       <script type="text/javascript" src="/assets/js/kpb.js"></script>
{/literal}