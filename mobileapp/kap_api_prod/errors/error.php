<?php 
	$status = $_SERVER["REDIRECT_STATUS"];
	$codes = array(
			403 => array("403 Forbidden", "The Server has refused to fullfill your Request."),
			404 => array("404 Not Found", "The document/file requested was not found on this server."),
			405 => array("405 Method Not Allowed", "The method specified in the Request-Line is not allowed for the specified resource."),
			408 => array("408 Request Timeout", "Your Browser failed to send a request in the time allowed by server."),
			500 => array("500 Internal Server Error", "The request was unsuccessful due to an unexpected condition encountered by the server."),
			502 => array("502 Bad Gateaway", "The Server received an invalid response from the upstream server while trying to fulfill the request."),
			504 => array("504 Gateaway Timeout", "The upstream server failed to send a request in the time allowd by server."),
		);

	$title = $codes[$status][0];
	$message = $codes[$status][1];

	if ($title == false || strlen($status) != 3) {
		$title = "Unrecognized Status Code";
		$message = "The website returned an unrecognized status code.";
	}
?>
<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		body {
			margin: 0;
			background-color: #F4F4F4;
		}
		#content {
			width: 68%;
			background-color: #FFF;
			margin: 5% auto 0;
			padding: 2%;
		}
	</style>
	<title><?php echo $title; ?></title>
</head>
<body>
	<div id="content">
		<h1><?php echo $title; ?></h1>
		<p><?php echo $message; ?></p>
	</div>
</body>
</html>