<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invest extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	function genRndString($length = 150, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
        if ($length > 0) {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars)) {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1})
                    $the_chars .= $r;
            }

            return $the_chars;
        }
    }
    function OrderId($length = 7, $chars = '1234567890') {
        if ($length > 0) {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars)) {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1})
                    $the_chars .= $r;
            }

            return $the_chars;
        }
    }

	public function index()
	{
		$req = array(
				"member_id" => $this->input->post("member_id"),
				"campaign_id" => $this->input->post("campaign_id"),
				"payment_type" => $this->input->post("payment_type"),
				"status" => "open",
				"invest_date" => date("Y-m-d"),
				"campaign_type" => $this->input->post("campaign_type"),
				"campaign_name" => $this->input->post("campaign_name"),
				"amount" => $this->input->post("amount"),
				"investor_name" => $this->input->post("investor_name"),
				"currency" => $this->input->post("currency"),
				"order_id" => $this->OrderId(),
				"campaign_owner" => $this->input->post("campaign_owner"),
				"token" => $this->genRndString(),
				"closing_date" => $this->input->post("closing_date"),
			);

		if ($this->db->insert("tpaymentpending", $req)) {
			echo "Success";
		} else {
			echo "error";
		}
	}


}

/* End of file Invest.php */
/* Location: ./application/controllers/Invest.php */