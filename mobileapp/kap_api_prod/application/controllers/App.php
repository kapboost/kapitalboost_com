<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller{
  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  function index()
  {
    $this->load->view('app');
  }
  function getMemberId($email){
    $query = $this->db->get_where('tmember', array('member_email' => $email));
    $data = $query->row_array();
    $id = $data["member_id"];
    return $id;
  }
  function countDate($date){
    $now = time();
    $c=floor( (strtotime($date) - $now ) / (60 * 60 * 24));if($c<1){$res = 0;}else{$res=$c;
    }
    return 
    $res;
  }
  function checkMail($email){
    $query = $this->db->get_where("tmember",array("member_email" => $email));
    if ($query->row_array() == null) {
      return false;
    } else {
      return true;
    }
  }
  public function myinfo(){
    $email = $this->input->post("email");
    $res = [];
    if (empty($email)) {
      $res["status"] = 0;
    } else {
      $data = $this->db->get_where('tmember', array('member_email' => $email));
      $res["status"] = 1;
      $res["data"] = $data->row_array();
    }
    echo json_encode($res);
  }
  public function manageprofile(){
    $req = json_decode(file_get_contents("php://input"));
    $this->db->where('member_id', $req->member_id);
    $this->db->update('tmember', $req);
    $check_nric = $this->input->post("change_nric");
    if ($check_nric == "ok") {
      $config['upload_path'] = '../getfundedassets/';
      $config["file_name"] = time().uniqid() + ".png";
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('file')){
        $tmp_file = $this->upload->data();
        $file_name = $tmp_file->file_name;
        $this->db->set('nric_file', $file_name);
        $this->db->where('member_email', $this->input->post("email"));
        $this->db->update('tmember');
      }
    }
    $res = array();
    $res["status"] = 1;
    $res["msg"] = "Success";
    echo json_encode($res);
  }
  public function campaigndetails($id = null){
    $res = array();
    if ($id == null) {
      $res["status"] = 0;
      $res["data"] = "No Campaign";
    } else {
      $query = $this->db->query("SELECT *,DATEDIFF(clossing_date,NOW()) AS days_left,DATEDIFF(clossing_date,release_date) AS duration FROM tcampaign WHERE campaign_id = '$id'");
        $campaign = array();
        foreach ($query->result() as $row)
        {
                $campaign = array(
                    "campaign_id" => $row->campaign_id,
                    "campaign_name" => $row->campaign_name,
                    "campaign_description" => $row->campaign_description,
                    "campaign_country" => $row->country,
                    "campaign_industry" => $row->industry,
                    "campaign_background" => "https://kapitalboost.com/assets/images/campaign/".$row->campaign_background,
                    "campaign_target" => $row->total_funding_amt,
                    "campaign_funded" => $row->curr_funding_amt,
                    "campaign_returns" => $row->project_return,
                    "campaign_tenor" => $row->funding_summary,
                    "campaign_days_left" => $row->days_left,
                    "campaign_risk" => $row->risk,
                    "minimum_investment" => $row->minimum_investment,
                    "campaign_type" => $row->project_type
                  );
        }
      $res["status"] = 1;
      $res["data"] = $campaign;
    }
    echo json_encode($res);
  }
  public function payment(){
    $email = $this->input->post("email");
    $member_id = $this->getMemberId($email);
    $data = $this->db->order_by("ppid DESC")->get_where('tpaymentpending',array('member_id' => $member_id));
    $res_data = array();
    foreach ($data->result_array() as $key) {
        $campaign_details = $this->db->get_where('tcampaign',array('campaign_id' => $key['campaign_id']));
        $tinv_pay = $this->db->get_where('tinv', array('token' => $key['token']));
        $tinv_pay_data = $tinv_pay->row_array();
        $tmp = $key;
        $tmp["campaign_details"] = $campaign_details->row_array();
        $tmp["payment_status_tinv"] = $tinv_pay_data["status"];
        $res_data[] = $tmp;
    }
    $res = array();
    if ($data->result_array() == null) {
      $res["status"] = 0;
    } else {
      $res["status"] = 1;
      $res["data"] = $res_data;
    }
    echo json_encode($res);
  }
  public function portofolio(){
    $email = $this->input->post("email");
    $data = $this->db->order_by("id DESC")->get_where('tinv',array('email' => $email, 'status' => 'Paid'));
    $tinv_data = $data->result_array();
    $res_data = array();
    foreach ($tinv_data as $key) {
        $campaign_details = $this->db->get_where('tcampaign',array('campaign_name' => $key["campaign"]));
        $tmp = $key;
        $tmp["campaign_details"] = $campaign_details->row_array();
        $res_data[] = $tmp;
    }
    // echo json_encode($res_data);
    // die();
    $res = array();
    if ($data->result_array() == null) {
      $res["status"] = 0;
    } else {
      $res["status"] = 1;
      $res["data"] = $res_data;
    }
    echo json_encode($res);
  }
  public function campaign($type = null){
    $res = array();
    if ($type == null) {
      exit;
    }
    else if ($type == "sme") {
        $query = $this->db->query("SELECT *,DATEDIFF(clossing_date,NOW()) AS days_left,DATEDIFF(clossing_date,release_date) AS duration FROM tcampaign WHERE project_type='sme' ORDER BY campaign_id DESC");
        // $query = $this->db->order_by("campaign_id","DESC")->get_where('tcampaign',array('project_type' => "sme"));
        $campaign = array();
        foreach ($query->result() as $row)
        {
                $campaign[] = array(
                    "campaign_id" => $row->campaign_id,
                    "campaign_name" => $row->campaign_name,
                    "campaign_description" => substr(strip_tags($row->campaign_description),0, 120),
                    "campaign_country" => $row->country,
                    "campaign_industry" => $row->industry,
                    "campaign_background" => "https://kapitalboost.com/assets/images/campaign/".$row->campaign_background,
                    "campaign_target" => $row->total_funding_amt,
                    "campaign_funded" => $row->curr_funding_amt,
                    "campaign_returns" => $row->project_return,
                    "campaign_tenor" => $row->funding_summary,
                    "campaign_days_left" => $row->days_left,
                    "campaign_risk" => $row->risk
                  );
        }
        $res["status"] = 1;
        $res["data"] = $campaign;
    }
    // End Campaign SME
    else if($type == "donation"){
          $query = $this->db->query("SELECT *,DATEDIFF(clossing_date,NOW()) AS days_left,DATEDIFF(clossing_date,release_date) AS duration FROM tcampaign WHERE project_type='donation' ORDER BY campaign_id DESC");
      // $query = $this->db->order_by("campaign_id","DESC")->get_where('tcampaign',array('project_type' => "donation"));
        $campaign = array();
        foreach ($query->result() as $row)
        {
                $campaign[] = array(
                    "campaign_id" => $row->campaign_id,
                    "campaign_name" => $row->campaign_name,
                    "campaign_description" => substr(strip_tags($row->campaign_description),0, 120),
                    "campaign_country" => $row->country,
                    "campaign_industry" => $row->industry,
                    "campaign_background" => "https://kapitalboost.com/assets/images/campaign/".$row->campaign_background,
                    "campaign_target" => $row->total_funding_amt,
                    "campaign_funded" => $row->curr_funding_amt,
                    "campaign_returns" => $row->project_return,
                    "campaign_tenor" => $row->funding_summary,
                    "campaign_days_left" => $row->days_left,
                    "campaign_risk" => $row->risk
                  );
        }
        $res["status"] = 1;
        $res["data"] = $campaign;
    }
    else if($type == "private"){
      $password = $this->input->post("password");
      $query = $this->db->query("SELECT *,DATEDIFF(clossing_date,NOW()) AS days_left,DATEDIFF(clossing_date,release_date) AS duration FROM tcampaign WHERE project_type='private' && private_password = '$password'");
      // $query = $this->db->get_where('tcampaign',array('project_type' => "private",'private_password'=>$password));
        $campaign = array();
        foreach ($query->result() as $row)
        {
                $campaign = array(
                    "campaign_id" => $row->campaign_id,
                    "campaign_name" => $row->campaign_name,
                    "campaign_description" => substr(strip_tags($row->campaign_description),0, 120),
                    "campaign_country" => $row->country,
                    "campaign_industry" => $row->industry,
                    "campaign_background" => "https://kapitalboost.com/assets/images/campaign/".$row->campaign_background,
                    "campaign_target" => $row->total_funding_amt,
                    "campaign_funded" => $row->curr_funding_amt,
                    "campaign_returns" => $row->project_return,
                    "campaign_tenor" => $row->funding_summary,
                    "campaign_days_left" => $this->countDate($row->expiry_date),
                    "campaign_risk" => $row->risk
                  );
        }
        $res["status"] = 1;
        $res["data"] = $campaign;
    } else {
      $res["status"] = 0;
    }
    echo json_encode($res);
  }
  public function login(){
    $email = $this->input->post("member_email");
    $password = $this->input->post("member_password");
    $query = $this->db->get_where('tmember', array("member_email"=>$email,"member_password" => $password));
    $res = array();
    if ($query->row_array() == null) {
      $res["status"] = 0;
      $res["msg"] = "Login Failed";
    } else {
      $res["status"] = 1;
      $res["data"] = $query->row_array();
    }
    echo json_encode($res);
  }
  public function register(){
    $username = $this->input->post("member_username");
    $email = $this->input->post("member_email");
    $password = $this->input->post("member_password");
    $lastname = $this->input->post("member_surname");
    $firstname = $this->input->post("member_firstname");
    $country = $this->input->post("member_country");
    $res = array();
    $registered = $this->checkMail($email);
    if ($registered) {
      $res["status"] = 0;
      $res["msg"] = "Email Registered !";
    } else {
      $temp = array(
        "member_username" => $username,
        "member_email" => $email,
        "member_password" => $password,
        "member_firstname" => $firstname,
        "member_surname" => $lastname,
        "member_country" => $country,
      );
      $query = $this->db->insert("tmember",$temp);
      if ($query) {
        $res["status"] = 1;
        $res["msg"] = "Successfully";
      } else {
        $res["status"] = 0;
        $res["msg"] = "Error Register !";
      }
    }
    echo json_encode($res);
  }
  public function forgot(){
    $email = $this->input->post("email");
    $res = array();
    $registered = $this->checkMail($email);
    if ($registered) {
      $res["status"] = 1;
      $res["msg"] = "Successfully forgot email !";
    } else {
      $res["status"] = 0;
      $res["msg"] = "Email Not Found !";
    }
    echo json_encode($res);
  }
  public function getfunded(){
    echo $this->input->get("text");
    // $req = array(
    //   "mobile" => $this->input->get("phone"),
    //   "country" => $this->input->get("country"),
    //   "company" => $this->input->get("company"),
    //   "industri" => $this->input->get("industry"),
    //   "year" => $this->input->get("year_established"),
    //   "currency" => $this->input->get("currency"),
    //   "project_type" => $this->input->get("project_type"),
    //   "est_an_rev" => $this->input->get("annual_revenue"),
    //   "funding_amount" => $this->input->get("amount"),
    //   "ass_tobe_pur" => $this->input->get("assets_to_purchase"),
    //   "funding_period" => $this->input->get("funding_period"),
    //   "have_purchased_order" => $this->input->get("have_purchase_order"),
    //   "attach_file" => $this->input->get("financial_report")
    // );
    // $this->db->insert('tgetfunded',$req);
    $res = array();
    $config['upload_path']   = '../getfunded_img/';
    $config['allowed_types'] = 'gif|jpg|png|pdf';
    $config['max_size']      = 100000000;
    $config['file_name']     = "img_".time();
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload('financial_image')){
        $error = array('error' => $this->upload->display_errors());
        $res["status"] = 0;
        $res["msg"] = $error["error"];
    } else {
        $res["status"] = 1;
        $res["msg"] = "success";
    }
    echo json_encode($res);
  }
}
