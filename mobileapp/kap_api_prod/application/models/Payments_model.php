<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payments_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
		
	}

	public function bank($email){
		
		$to = $email;
		$subject = 'Birthday Reminders for August';

		// Message
		$message = '
		<html>
		<head>
		  <title>Birthday Reminders for August</title>
		</head>
		<body>
		  <p>Here are the birthdays upcoming in August!</p>
		  <table>
		    <tr>
		      <th>Person</th><th>Day</th><th>Month</th><th>Year</th>
		    </tr>
		    <tr>
		      <td>Johny</td><td>10th</td><td>August</td><td>1970</td>
		    </tr>
		    <tr>
		      <td>Sally</td><td>17th</td><td>August</td><td>1973</td>
		    </tr>
		  </table>
		</body>
		</html>
		';
		$headers[] = 'MIME-Version: 1.0';
		$headers[] = 'Content-type: text/html; charset=iso-8859-1';
		$headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';

		mail($to, $subject, $message, implode("\r\n", $headers));

	}

}

/* End of file Payments_model.php */
/* Location: ./application/models/Payments_model.php */