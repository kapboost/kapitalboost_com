<?php

class Book_model extends CI_Model
{

  function __construct()
  {
    $this->load->database();
  }

  public function get_all_book($slug = FALSE){

    if ($slug === FALSE) {
      $query = $this->db->get('book');
      return $query->result_array();

    }

    $query = $this->db->get_where('book',array('slug' => $slug));
    return $query->row_array();

  }

  public function get_book($id = NULL){
    $query = $this->db->get_where('book',array('book_id' => $id));
    return $query->row_array();

  }

  public function insert_book()
  {

    $data = array(
      'name' => $this->input->post('name'),
      'price' => $this->input->post('price'),
      'stock' => $this->input->post('stock')
    );

    return $this->db->insert('book', $data);
  }


  public function update_book($id)
  {
    $data = array(
      'name' => $this->input->post('name'),
      'price' => $this->input->post('price'),
      'stock' => $this->input->post('stock')
    );

    $this->db->where('book_id', $id);
    return $this->db->update('book', $data);
  }

  public function delete_book($id)
  {
    return $this->db->delete('book', array('book_id' => $id));
  }


}

?>
