app.factory('LocalDatabaseService', [function(){
	return {
		set: function(data){
			window.localStorage.setItem("account", data);
		},
		get:function(data){
			return window.localStorage.getItem(data);
		}
	}
}]);