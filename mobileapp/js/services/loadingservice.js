app.service('LoadingService', ['$ionicLoading', function ($ionicLoading) {
	this.load = function(){
		$ionicLoading.show({
                template: 'Loading...'
        });
	}
	this.finish = function(){
		$ionicLoading.hide();
	}
	return this;
}]);