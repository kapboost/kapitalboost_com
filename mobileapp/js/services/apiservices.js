app.service('ApiServices', ['$http', function ($http) {

	var url = "https://kapitalboost.com/kapitalboost_api_dev/index.php/";

	// var url = "https://kapitalboost.com/kapitalboost_api_prod/index.php/";



    this.Post = function(url, data){

		return $http.post(url, data, {

			headers : {

				'Content-Type': 'application/x-www-form-urlencoded'

			},

			transformRequest: function(obj) {

                var str = [];

                for (var p in obj)

                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));

                return str.join("&");

            }

		});

	};

	this.login = function(data){

		return this.Post(url+"login",data);

	};

    this.register = function(data){

     	return this.Post(url+"register", data);

    };

    this.forgot = function(data){

    	return this.Post(url+"forgot", data);

    };

    this.campaignSme = function(){

    	return $http.get(url+'campaign/sme');

    };

    this.campaignDonation = function(){

    	return $http.get(url+'campaign/donation');

    };

    this.campaignPrivate = function(data){

    	return this.Post(url+'campaign/private',data);

    };

    this.campaignDetails = function(id){

        return $http.get(url+'campaigndetails/'+id);  

    };

    this.payment = function(data){

    	return this.Post(url+'payment', data);

        // return $http.get('json/payment.json');

    };

    this.portofolio = function(data){

    	return this.Post(url+'portofolio', data);

        // return $http.get('json/portofolio.json');

    };

    this.invest = function(data){

        return this.Post(url+'invest', data);

    }

    this.payBank = function(data){

    	return this.Post(url+'pay/bank',data);

    };

    this.payPaypal = function(data){

    	return this.Post(url+'pay/paypal',data);

    };

    this.payXfers = function(data){

    	return this.Post(url+'pay/xfers', data);

    };

    return this;

 }]);