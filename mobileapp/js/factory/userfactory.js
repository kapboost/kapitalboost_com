app.factory('UserFactory', ['$window', function($window){
	var user  = "kapitalboost_user";
	var data;
	this.get = function(){
		return JSON.parse($window.localStorage.getItem(user));
	};
	this.save = function(data){
		$window.localStorage.setItem(user,JSON.stringify(data));
	};
	this.getMail = function(){
		data = this.get();
	    return data.member_email;
	};
	this.getName = function(){
		data = this.get();
	    return data.member_firstname + ' ' + data.member_surname;
	};
	this.getCountry = function(){
		data = this.get();
	    return data.member_country;	
	};
	this.getMemberId = function(){
		data = this.get();
	    return data.member_id;		
	}

	return this;
}]);