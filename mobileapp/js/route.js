app.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  .state('login', {
  	url: '/login',
    cache : false,
  	templateUrl : 'templates/login.html',
    controller : 'loginCtrl'
  })
  .state('register', {
  	url: '/register',
    cache : false,
  	templateUrl : 'templates/register.html',
    controller : 'registerCtrl'
  })
  .state('forgot', {
  	url: '/forgot',
    cache : false,
  	templateUrl : 'templates/forgot.html',
    controller : 'forgotCtrl'
  })
  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'

  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    cache : false,
    views: {
      'tab-dash': {
        templateUrl: 'templates/dashboard.html',
        controller : 'dashboardCtrl'
      }
    }
  })
  .state('tab.campaign', {
    url: '/campaign',
    cache : false,
    views: {
      'tab-dash': {
        templateUrl: 'templates/campaign.html',
        controller : 'campaignCtrl'
      }
    }
  })
  .state('invest', {
      url : '/invest/:id',
      cache : false,
      templateUrl : 'templates/details.html',
      controller : 'detailsCtrl'
  })
  .state('tab.getfunded', {
    url: '/getfunded',
    cache : false,
    views: {
      'tab-dash': {
        templateUrl: 'templates/getfunded.html',
        controller : 'getfundedCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

});
