var app = angular.module('kapitalboost', ['ionic','ngSanitize'])
// .config(function($ionicConfigProvider){
//   $ionicConfigProvider.views.transition('none');
// })
.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
.constant('shopSettings',{
    payPalSandboxId :'AUhBe3ZooAWTDibYjEOP8SJo_Scnmk14EQ5oL3mxfKMqoas0I59I9gYTmc0B64pQtZ7kYCxZYQhFD8f3',
    payPalProductionId : 'production id here',
    payPalEnv: 'PayPalEnvironmentSandbox', // for testing production for production
    payPalShopName : 'Kapitalboost',
    payPalMerchantPrivacyPolicyURL : 'https://kapitalboost.com',
    payPalMerchantUserAgreementURL : 'https://kapitalboost.com'

})
