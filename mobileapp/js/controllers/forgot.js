app.controller('forgotCtrl', function($scope, ApiServices, LoadingService) {

	$scope.forgot = function(email){
		if (email === undefined) {
			alert("Please Enter Your Email");
		} else {
			LoadingService.load();
            var params = {
                member_email : email
            };

            ApiServices.forgot(params).success(function(res){
                LoadingService.finish();
                alert(res.msg);
            }).error(function(){
                alert("Please Check Your Internet.");
                LoadingService.finish();
            });
		}
	};
});