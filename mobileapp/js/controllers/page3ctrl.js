'use strict';
app.controller('page3Ctrl', function($scope, $http, LocalDatabaseService, $cordovaFileTransfer) {
    $scope.pts = {
        "name": "Pt",
        "value": "Project Type",
        "values": ["Project Type", "Donation", "SME", "Private"]
    };

    $scope.months = {
        "name": "Month",
        "value": "Funding Period",
        "values": ["Funding Period", "1 month", "2 months", "3 months", "4 months", "5 months", "6 months", "7 months", "8 months", "9 months", "10 months", "11 months", "12 months"]
    };
    $scope.currencys = {
        "name": "currency",
        "value": "Currency",
        "values": ["Currency", "IDR", "MYR", "SGD"]
    }
    $scope.hpos = {
        "name": "hpos",
        "value": "Have purchase orders",
        "values": ["Have purchase orders", "No", "Yes"]
    }

    /*
        Get Currest User
     */
    var data = LocalDatabaseService.get('account');
    var account = JSON.parse(data);
    /**
     * [get value from input]
     * @param  {[input]} data   [have all of basic needed]
     * @param  {[select]} cureencys [select.currency]
     * @param  {[select]} months [select.months]
     * @return {[select]} hpis   [have purchase orders]
     */
    $scope.gf = {

    };

    $scope.gfing = function(data, protypes, currencys, months, hpos) {
        /**
         * prepare the variable
         */
        
        var currency = currencys.value;
        var fp = months.value;
        var have_purchase_order = hpos.value;
        var project_type = protypes.value;

        var fullname = account.member_firstname + account.member_surname;
        var email = account.member_email;
        var mobile = data.phone;
        var country = data.country;
        var company = data.company;
        var industry = data.industry;
        var year = new Date().getFullYear();
        var currency = currency;
        var project_type = project_type;
        var est_return = data.ear;
        var funding_amount = data.fa;
        var ass_tobe_purchase = data.assets;
        var funding_period = fp;
        var have_purchase_order = have_purchase_order;
        // var attach_file = document.getElementById("attach_file_img").value;

        

        // var gfdata = {
        //     fullname: fullname,
        //     email: email,
        //     mobile: mobile,
        //     country: country,
        //     company: company,
        //     industri: industry,
        //     year: year,
        //     currency: currency,
        //     project_type: project_type,
        //     est_an_rev: est_return,
        //     funding_amount: funding_amount,
        //     ass_tobe_pur: ass_tobe_purchase,
        //     funding_period: funding_period,
        //     have_purchased_order: have_purchase_order,
        //     attach_file: attach_file
        // };

        /*
            Missing File
         */

        var url = "http://project-testserver.com/rest/v1/getFunded";
        // var url = "http://anipedia.xyz/img.php";
        // console.log('file is ' );
        // console.dir(file);
        // fileUpload.uploadFileToUrl(file, url);

        // $http({
        //     method: 'POST',
        //     url: url,
        //     headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
        //     transformRequest: function(obj) {
        //         var str = [];
        //         for (var p in obj)
        //             str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        //         return str.join("&");
        //     },
        //     data: gfdata
        // }).success(function(res) {
        //     console.log(res);
        // }).error(function(res) {
        //     alert("No Internet Connection !");
        // });

      
     //File for Upload
     var targetPath = document.getElementById("myphotos").value;
      
     // File name only
     var filename = targetPath.split("/").pop();
      
     var options = {
          fileKey: "attach_file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "image/jpg",
          params : {
            'directory':'upload', 
            'fileName':filename, 
            'fullname': fullname,
            'email': email,
            'mobile': mobile,
            'country': country,
            'company': company,
            'industri': industry,
            'year': year,
            'currency': currency,
            'project_type': project_type,
            'est_an_rev': est_return,
            'funding_amount': funding_amount,
            'ass_tobe_pur': ass_tobe_purchase,
            'funding_period': funding_period,
            'have_purchased_order': have_purchase_order
        }
      };
      console.log(options);
      $cordovaFileTransfer.upload(url, targetPath, options).then(function (result) {
          // console.log("SUCCESS: " + JSON.stringify(result.response));
          alert("Success"+ JSON.stringify(result.response))
      }, function (err) {
          // console.log("ERROR: " + JSON.stringify(err));
          alert("Success"+ JSON.stringify(err))
      });

    };
})
