app.controller('getfundedCtrl', ['$scope','$http','UserFactory','LoadingService', function ($scope, $http, UserFactory, LoadingService) {
	$scope.pts = {
        "name": "Pt",
        "value": "Project Type",
        "values": ["Project Type", "Donation", "SME", "Private"]
    };

    $scope.months = {
        "name": "Month",
        "value": "Funding Period",
        "values": ["Funding Period", "1 month", "2 months", "3 months", "4 months", "5 months", "6 months", "7 months", "8 months", "9 months", "10 months", "11 months", "12 months"]
    };
    $scope.currencys = {
        "name": "currency",
        "value": "Currency",
        "values": ["Currency", "IDR", "MYR", "SGD"]
    }
    $scope.hpos = {
        "name": "hpos",
        "value": "Have purchase orders",
        "values": ["Have purchase orders", "No", "Yes"]
    }

    $scope.send = function(gf){
        LoadingService.load();
        var params = {
            owner : UserFactory.getName(),
            fullname : UserFactory.getName(),
            email : UserFactory.getMail(),
            phone : gf.phone,
            country : gf.country,
            company : gf.company,
            industry : gf.industry,
            year_established : gf.your_established,
            currency : $scope.currencys.value,
            project_type : $scope.pts.value,
            annual_revenue : gf.estimated_annual_revenue,
            amount : gf.amount,
            assets_to_purchase : gf.assets,
            funding_period : $scope.months.value,
            have_purchase_order : $scope.hpos.value
        };

        // console.log(params);
        
        // var fd = new FormData();
        // angular.forEach($scope.financeFile, function(file){
        //     fd.append('financial_image', file);
        // });
        // console.log(fd);

        $http({
            method : 'post',
            url : 'https://kapitalboost.com/kapitalboost_api_prod/index.php/getfunded',
            // data : fd,
            params : params,
            transformRequest : angular.identity,
            headers : {'Content-type' : undefined}
        }).then(function(res){
            LoadingService.finish();
            if (res.data.status == 1) {
                alert("Success ");
            } else {
                alert("Fail ");
            }
        });
    }
}]);