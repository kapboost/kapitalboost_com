'use strict';

app.controller('berandaCtrl', function($scope, UserFactory, $http, $location) {
    var data = UserFactory.get('account');
    var user = JSON.parse(data);
    $scope.account = user.user_info;

    $scope.listCampaigns = [];

    function getCampaigns() {
        $scope.listCampaigns = [];
        $scope.isLoadCampaign = true;
        $http.get('http://project-testserver.com/rest/v1/campaign/' + $scope.account.member_email + '', { cache: true })
            .success(function(res) {
                $scope.listCampaigns = res.campaign_owner;

                $scope.isLoadCampaign = false;
                if ($scope.listCampaigns.length == 0) {
                    $scope.noCampaigns1 = true;
                } else {
                    $scope.noCampaigns1 = false;
                }
            });
    }

    getCampaigns();

    // Function GET Campaigns Page 1
    function successCampaign() {
        $scope.isLoadCampaign = true;
        $http.get('http://project-testserver.com/rest/v1/campaign_success/' + $scope.account.member_email + '', { cache: true })
            .success(function(res) {
                $scope.successCampaigns = res.campaign_success;
                $scope.isLoadCampaign = false;

                if ($scope.listCampaigns.length == 0) {
                    $scope.noCampaigns = true;
                } else {
                    $scope.noCampaigns = false;
                }
            });
    }

    function closeCampaigns() {
        $scope.isLoadCampaign = true;
        $http.get('http://project-testserver.com/rest/v1/campaign_closed/' + $scope.account.member_email + '', { cache: true })
            .success(function(res) {
                $scope.closeCampaigns = res.campaign_closed;
                $scope.isLoadCampaign = false;
                if ($scope.listCampaigns.length == 0) {
                    $scope.noCampaigns3 = true;
                } else {
                    $scope.noCampaigns3 = false;
                }
            });
    }

    $scope.activePage1 = true;
    $scope.activePage2 = false;
    // show page 2
    $scope.showPage = function(page) {
        if (page == 'page1') {
            $scope.activePage1 = true;
            $scope.activePage2 = false;
            $scope.activePage3 = false;
            getCampaigns();
        } else if (page == 'page2') {
            $scope.activePage1 = false;
            $scope.activePage2 = true;
            $scope.activePage3 = false;
        } else {
            $scope.activePage1 = false;
            $scope.activePage2 = false;
            $scope.activePage3 = true;
        }
    }

    // Page 1 Tabs Setting
    $scope.page1Tab1 = true;
    $scope.Page1 = function(page) {
        if (page == 'your-campaigns') {
            $scope.page1Tab1 = true;
            $scope.page1Tab2 = false;
            $scope.page1Tab3 = false;
            getCampaigns();
        } else if (page == 'success-campaigns') {
            $scope.page1Tab1 = false;
            $scope.page1Tab2 = true;
            $scope.page1Tab3 = false;
            successCampaign();
        } else if (page == 'closed-campigns') {
            $scope.page1Tab1 = false;
            $scope.page1Tab2 = false;
            $scope.page1Tab3 = true;
            closeCampaigns();
        }
    }

    // page 2 Settings
    $scope.pageSME = true;
    $scope.Page2 = function(page) {
        if (page == 'smefunding') {
            $scope.pageSME = true;
            $scope.pageDonation = false;
            $scope.pagePrivate = false;
            getCampaignSME();
        } else if (page == 'donation') {
            $scope.pageSME = false;
            $scope.pageDonation = true;
            $scope.pagePrivate = false;
            getCamapaignDonation();
        } else if (page == 'private') {
            $scope.pageSME = false;
            $scope.pageDonation = false;
            $scope.pagePrivate = true;
        }
    }

    function getCampaignSME() {
        $http.get('https://kapitalboost.com/apis/v1/campaign.php?campaign=sme')
            .success(function(res) {
                // $scope.FeedCampaigns = res.campaign_sme;
                console.log(res);
            });
    }
    getCampaignSME();

    function getCamapaignDonation() {
        $http.get('http://project-testserver.com/rest/v1/campaign_donation', { cache: false })
            .success(function(res) {
                $scope.CampaignDonation = res.campaign_donation;
            });

    }

    $scope.getPrivate = function(pwd) {
        if (pwd === undefined) {
            alert("please insert a Password !");
        } else {
            $http.get('http://project-testserver.com/rest/v1/campaign_private/' + pwd)
                .then(function(res) {
                    var data = res.data.campaign_private;
                    if (data.length === 0) {
                        console.log("kosong");
                    } else {
                        $scope.PrivateCampaign = data;
                    }
                });
        }
    };
})
