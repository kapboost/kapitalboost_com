app.controller('loginCtrl', function($scope, ApiServices, LoadingService, UserFactory) {

    // create object var data

    var data = {};


    // create function login

    $scope.login = function(user) {

        data = user;

        // cek object data tidak empty

        if (data != null) {

            LoadingService.load();

            var params = {

                member_email : data.email,

                member_password : data.password

            };



            ApiServices.login(params).success(function(res){

                console.log(res);

                LoadingService.finish();

                if (res.status == 1){

                    var c_u = res.data;

                    UserFactory.save(c_u);

                    window.location = "#/tab/dash";

                } else {

                    alert(res.msg);

                }

            }).error(function(){

                alert("Please Check Your Internet!");

                LoadingService.finish();

            });



        } else { // error object data is empty

            alert('Please Insert Your Username And Password!');

        }

    }

})

