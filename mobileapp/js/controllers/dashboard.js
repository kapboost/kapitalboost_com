app.controller('dashboardCtrl', ['$scope','ApiServices','UserFactory','$ionicPopup','LoadingService','PaypalService','$ionicModal', function ($scope, ApiServices, UserFactory, $ionicPopup, LoadingService, PaypalService, $ionicModal) {
	$scope.account = UserFactory.get();
	
	$scope.currentView = "payment";
	$scope.changeView = function(page){
		$scope.currentView = page;
		if (page == 'portofolio') {
			$scope.loadPortofolio();
		} else if(page == 'payment'){
			$scope.loadPayment();
		}
		
	};

	var params = {
		email : UserFactory.getMail()
	};

	$scope.isLoadPayment = false;
	$scope.loadPayment = function(){
		ApiServices.payment(params).then(function(res){
			$scope.payments = res.data;
			$scope.isLoadPayment = true;
		});
	};
	$scope.loadPayment();

	$scope.isLoadPortofolio = false;
	$scope.loadPortofolio = function(){
		ApiServices.portofolio(params).then(function(res){
			$scope.portofolios = res.data;
			$scope.isLoadPortofolio = true;
		});
	};
		// $scope.loadPortofolio();

	$scope.PayPaypal = function(data) {
		$scope.currentPayment = data;
	   	var confirmPopup = $ionicPopup.confirm({
	    	title: 'Pay NOW ?',
	    	template : '<div><table class="card-table paynow-table"><tr><td>Campaign Name</td><td>: <b>'+data.campaign_name+'</b></td></tr><tr><td>Invest Amount</td><td>: <b>S$ '+data.amount+'</b></td></tr><tr><td>Invest Date</td><td>: <b>'+data.invest_date+'</b></td></tr><tr><td>Clossing Date</td><td>: <b>'+data.closing_date+'</b></td></tr><tr><td>Payment Type</td><td>: <b>'+data.payment_type+'</b></td></tr></table></div>',
	     	buttons: [{
		    	text: 'Cancel',
		    	type: 'button-default'
		  	}, {
		    	text: 'Pay S$ '+data.amount,
		    	type: 'button-positive',
		    	onTap: function(){
		    		LoadingService.load();

		    		PaypalService.initPaymentUI().then(function () {
						PaypalService.makePayment(data.amount, "Total Amount").then(function (res) {
							LoadingService.finish();
							if (res.response.state == 'approved') {
								var tx_code = res.response.id;
								var params = {
						    		member_id: UserFactory.getMemberId(),
						    		member_email : UserFactory.getMail(),
						    		member_name : UserFactory.getName(),
									campaign_id: data.campaign_id,
									campaign_country : UserFactory.getCountry(),
									payment_type: data.payment_type,
									campaign_type: data.campaign_type,
									campaign_name: data.campaign_name,
									tx_code : tx_code,
									token_user : data.token,
									amount: data.amount,
									investor_name: UserFactory.getName(),
									currency: data.currency,
									campaign_owner: data.campaign_owner,
									close: data.closing_date
					    		};

					    		ApiServices.payPaypal(params).then(function(resp){
						    		$scope.loadPayment();
						    		LoadingService.finish();
						    		$scope.paypalInfo(data);
						    	});
							}

						}, function (error) {
							LoadingService.finish();
							alert("Transaction Canceled");
						});
					});
		    }
		  }]
	   });
	 };

	 $scope.PayBank = function(data) {
		$scope.currentPayment = data;
	   	var confirmPopup = $ionicPopup.confirm({
	    	title: 'Pay NOW ?',
	     	template : '<div><table class="card-table paynow-table"><tr><td>Campaign Name</td><td>: <b>'+data.campaign_name+'</b></td></tr><tr><td>Invest Amount</td><td>: <b> S$ '+data.amount+'</b></td></tr><tr><td>Invest Date</td><td>: <b>'+data.invest_date+'</b></td></tr><tr><td>Clossing Date</td><td>: <b>'+data.closing_date+'</b></td></tr><tr><td>Payment Type</td><td>: <b>'+data.payment_type+'</b></td></tr></table></div>',
	     	buttons: [{
		    	text: 'Cancel',
		    	type: 'button-default'
		  	}, {
		    	text: 'Pay S$ '+data.amount,
		    	type: 'kap-button-blue',
		    	onTap: function(){
		    		LoadingService.load();
			    	var params = {
			    		member_id: UserFactory.getMemberId(),
						campaign_id: data.campaign_id,
						payment_type: data.payment_type,
						campaign_type: data.campaign_type,
						campaign_name: data.campaign_name,
						project_type : data.campaign_type,
						token_user : data.token,
						amount: data.amount,
						campaign_country : UserFactory.getCountry(),
						member_email : UserFactory.getMail(),
						member_name: UserFactory.getName(),
						currency: data.currency,
						campaign_owner: data.campaign_owner,
						close: data.closing_date
			    	};
			    	ApiServices.payBank(params).then(function(res){
			    		$scope.loadPayment();
			    		LoadingService.finish();
			    		$scope.bankInfo();
			    	});
		    	}
		  	}]
	   });
	};

	$scope.bankInfo = function() {
	   var alertPopup = $ionicPopup.alert({
	     title: 'Thank You',
	     template: 'Please proceed to make payment to Kapital Boost. <br> by checking your email .',
	     buttons : [
	     	{
	     		text: 'OK',
		    	type: 'kap-button-primary',
		    	onTap: function(){

		    	}
	     	}

	     ]
	   });
	};

	$scope.paypalInfo = function(data) {
	   var alertPopup = $ionicPopup.alert({
	     title: 'Payment Success',
	     template: 'Thank you for your commitment of SGD <b>'+data.amount+'</b> in the <b>'+data.campaign_name+'</b> campaign.',
	     buttons : [
	     	{
	     		text: 'OK',
		    	type: 'kap-button-primary',
		    	onTap: function(){

		    	}
	     	}

	     ]
	   });
	};


	$ionicModal.fromTemplateUrl('templates/portofolio_detail.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.paymentDetailT = modal;
	});
	
	$scope.showDetailPay = function(data) {
		$scope.paymentDetailT.show();
		$scope.port_d = data;
	};

	$scope.hideDetailPay = function() {
		$scope.paymentDetailT.hide();
	};
	
}]);