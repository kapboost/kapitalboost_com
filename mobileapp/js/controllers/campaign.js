app.controller('campaignCtrl', ['$scope','ApiServices','$sce','LoadingService','$ionicModal', function ($scope, ApiServices, $sce, LoadingService, $ionicModal) {
	$scope.campaignPage = 'sme';
	$scope.campaignPrivate = {
		status : 0
	};
	$scope.changePage = function(page){
		$scope.campaignPage = page;
		$scope.campaignPrivateis = true;
		$scope.campaignPrivate.status = 0;
	};
	$scope.smeLoad = true;
	$scope.loadSME = function(){
		ApiServices.campaignSme().then(function(res){
			$scope.smeLoad = false;
			$scope.campaignSME = res.data.data;

		});
	};
	$scope.loadSME();

	$scope.donationLoad = true;
	$scope.loadDonation = function(){
		ApiServices.campaignDonation().then(function(res){
			$scope.donationLoad = false;
			$scope.campaignDonation = res.data.data;
		});
	};
	$scope.loadDonation();

	$scope.toHTML = function(data) {
       return $sce.trustAsHtml(data);
     };

     $scope.password = "";
    $scope.campaignPrivateis = true;
     $scope.reqPrivate = function(pass){
     	LoadingService.load();
     	if (pass !== "") {
     		var params = {
     			password : pass
     		};

     		ApiServices.campaignPrivate(params).then(function(res){
     			LoadingService.finish();
     			if (res.data.status == 0) {
     				alert("No Campaign With Your Password");
     			} else {
     				$scope.campaignPrivateis = false;
     				$scope.campaignPrivate = res.data;
     			}
     		});
     	} else {
     		alert("Please Insert Your Password");
     	}
     };

    $ionicModal.fromTemplateUrl('templates/details.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.modal = modal;
	});

	$scope.openSMEdetails = function(data) {
	    $scope.modal.show();
	    console.log(data);
	};
	$scope.closeSMEdetails = function(){
		$scope.modal.hide();	
	}
}]);