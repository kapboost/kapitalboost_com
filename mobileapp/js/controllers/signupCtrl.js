'use strict';
app.controller('signupCtrl', function($scope, $ionicLoading, $http) {
    var url = 'https://kapitalboost.com/apis/v1/register.php';

    // Loading Animation
    $scope.isLoad = function() {
        $ionicLoading.show({
            template: 'Loading...'
        });
    }
    $scope.isLoaded = function() {
        $ionicLoading.hide();
    }

    // submitting form event
    $scope.signup = function(user) {

        // get object from input #page daftar
        var data = user;
        $scope.isLoad();
        // posting to API server
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
                member_username: data.username,
                member_email: data.email,
                member_password: data.password,
                member_surname: data.lastname,
                member_firstname: data.firstname,
                member_country: data.nationality
            }
        }).success(function(res) {
            $scope.isLoaded();
            if (res.status == 2) {
                alert("Email is Already Exist !");
            }
            else if (res.status == 1) {
                alert('SignUp Success, You can Login Now !');
                window.location = '#/login';
            } else {
                alert("Error");
            }
        }).error(function(res) {
            $scope.isLoaded();
            alert('Sorry, ' + res + '');
        });
    }
});
