app.controller('detailsCtrl', function($stateParams, $scope, ApiServices, $ionicModal, UserFactory, LoadingService, $ionicPopup){

	var id = $stateParams.id;



	$scope.isLoad = false;

	ApiServices.campaignDetails(id).then(function(res){

		$scope.campaign = res.data;

		$scope.isLoad = true;

	});



	$scope.backViews = function(){

		window.history.go(-1);

	};



	$scope.investXfers = function(){

		alert("Invest Xfers");

	};



	$scope.userCountry = UserFactory.getCountry();



	$ionicModal.fromTemplateUrl('templates/invest.html', {

		scope: $scope,

		animation: 'slide-in-up'

	}).then(function(modal) {

		$scope.modal = modal;

	});

	$scope.amountINV = 0;

	$scope.openModal = function() {

		$scope.modal.show();

		$scope.cinv = $scope.campaign.data;

	};



	$scope.closeModal = function() {

		$scope.modal.hide();

	};



	$scope.paymentMethod = function(data){

		console.log(data);

	};



	$scope.amountinv = undefined;

	$scope.investNow = function(data, amount, pm){
		if (amount > data.minimum_investment) {
		LoadingService.load();

		var params = {

			member_id: UserFactory.getMemberId(),

			campaign_id:data.campaign_id,

			payment_type: pm,

			campaign_type: data.campaign_type,

			campaign_name: data.campaign_name,

			amount: amount,

			investor_name: UserFactory.getName(),

			currency: "SGD",

			campaign_owner:data.campaign_owner,

			closing_date: data.campaign_close_date,

		};

		// console.log(params);

		ApiServices.invest(params).then(function(res){

			LoadingService.finish();

			

			if (res.data.status == 0) {

				alert("Invest Error !");

				$scope.closeModal();

			} else if(res.data.status == 1){

				$scope.paymentSuccess();

				$scope.closeModal();

			}

		});

	} else {
		alert("You must invest not less than minimum");
	}

	}



	$scope.paymentSuccess = function() {

	   var alertPopup = $ionicPopup.alert({

	     title: 'Thank You',

	     template: 'Invest Successfully.',

	     buttons : [

	     	{

	     		text: 'OK',

		    	type: 'kap-button-primary',

		    	onTap: function(){

		    		window.location = "#/tab/dash";

		    	}

	     	}



	     ]

	   });

	};

});