app.controller('registerCtrl', ['$scope','ApiServices','LoadingService', function ($scope, ApiServices, LoadingService) {
	$scope.user = {
		username : undefined,
		email : undefined,
		password : undefined,
		lastname : undefined,
		firstname : undefined,
		nationality : undefined
	};
	$scope.signup = function(data){
		if (data.username !== undefined && data.firstname !== undefined && data.lastname !== undefined && data.nationality !== undefined && data.email !== undefined && data.password !== undefined) {
			LoadingService.load();
			var params = {
				member_username : data.username,
				member_email : data.email,
				member_password : data.password,
				member_surname : data.lastname,
				member_firstname : data.firstname,
				member_country : data.nationality
			};
			ApiServices.register(params).then(function(res){
				LoadingService.finish();
				if (res.data.status == 1) {
					alert('SignUp Success, You can Login Now !');
                	window.location = '#/login';
				} else {
					alert(res.data.msg);
				}
			});
		} else {
			alert("Please Insert All Field");
		}
	};
}]);