<?php

class mysql {

       var $dbhost = null;
       var $dbuser = null;
       var $dbpass = null;
       var $dbname = null;
       var $conn = null;
       var $result = null;

       function __construct() {
              $mysqlfile = parse_ini_file('../../.rDir/.MySQL.ini');
              $this->dbhost = $mysqlfile['dbhost'];
              $this->dbuser = $mysqlfile['dbuser'];
              $this->dbpass = $mysqlfile['dbpass'];
              $this->dbname = $mysqlfile['dbname'];
       }

       public function Connection() {
              $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
              if (mysqli_connect_error()) {
                     return false;
              }
              mysqli_set_charset($this->conn, 'utf8');
              return true;
       }

// Function connect to sql

       public function CloseConnection() {
              if ($this->conn != null) {
                     mysqli_close($this->conn);
              }
       }

// Function disconnect to sql

       public function CheckUsername($email) {
              $sql = "SELECT User_Email FROM UserData WHERE email = '$email'";
              $this->result = mysqli_query($this->conn, $sql);
              if (mysqli_num_rows($this->result) == 1) {
                     return true;
              }
              return false;
       }

       public function GetMemberIdFromEncrypt($encrypt) {
              $stmt = $this->conn->prepare("SELECT member_id FROM tmember where md5(1290*3+member_id)= ?");
              $stmt->bind_param("s", $encrypt);

              $stmt->execute();
              $this->result = $stmt->get_result();
              $row = $this->result->fetch_assoc();
              if (count($this->result) == 1) {

                     return $row['member_id'];
              } else {
                     return 0;
              }
       }
       public  function UpdatePassword($member_id, $member_password){
              $password = md5($member_password);
              $sql = "UPDATE tmember SET member_password = '$password' WHERE member_id='$member_id'";
              return mysqli_query($this->conn, $sql);
       }

}
