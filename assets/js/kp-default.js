$(function () {

       var $win = $(window);
       var $doc = $(document);
       var $default = $('#default');
       var $default_1 = $('#default-1');
       var $default_1_map = $('#default-1-back');


       var default_props = function () {

              $('#container').css({'min-height': $default_1_map.height()});
              $default.css({'min-height': $default_1_map.height()});
              $default_1.css({'min-height': $default_1_map.height()});

       };

       default_props();

       $win.resize(function () {

              default_props();

       });


       if ($('.cartInputText').length > 0) {

              $('.cartInputText').val('1');
              $('.cartInputText').change();

       }


       $("#pen-stat").change(function () {

              if ($(this).val() == 'yes') {

                     $('#CAT_Custom_20022896').fadeIn();

              } else {

                     $('#CAT_Custom_20022896').fadeOut();

              }

       });

       if ($(window).width() < 992) {
              var div1 = $("#campaigns-detail-2-inner-left");
              var div2 = $("#campaigns-detail-2-inner-right");

              var tdiv1 = div1.clone();
              var tdiv2 = div2.clone();

              div1.replaceWith(tdiv2);
              div2.replaceWith(tdiv1);
              
              $("#secondInvest").remove();
       }


});
function removeParam(parameter)
{
       var url = document.location.href;
       var urlparts = url.split('?');

       if (urlparts.length >= 2)
       {
              var urlBase = urlparts.shift();
              var queryString = urlparts.join("?");

              var prefix = encodeURIComponent(parameter) + '=';
              var pars = queryString.split(/[&;]/g);
              for (var i = pars.length; i-- > 0; )
                     if (pars[i].lastIndexOf(prefix, 0) !== -1)
                            pars.splice(i, 1);
              url = urlBase + '?' + pars.join('&');
              window.history.pushState('', document.title, url); // added this line to push the new url directly to url bar .

       }
       return url;
}
