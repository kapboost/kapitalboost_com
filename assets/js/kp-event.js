$(function(){
	
    var $win  		 = 	$(window);
    var $doc  		 = 	$(document);
    var $event 		 = 	$('#event');
    var $event_1	 =	$('#event-1');
    var $event_1_map =	$('#event-1-back');
    
    
    var event_props = function() {

        $event.css({'min-height':$win.height()});
        $event_1.css({'min-height':$event_1_map.height()});
    	//$event_1.height($event_1_map.height());
    
    };
    
    event_props();
    
    $win.resize(function(){
        
    	event_props();
    
    });


});
