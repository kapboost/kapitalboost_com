$(function() {

	var $win  		 	= 	$(window);
    var $doc  		 	= 	$(document);
    var $contact 		= 	$('#contact');
    var $contact_1	 	=	$('#contact-1');
    var $contact_1_map 	=	$('#contact-1-back');
    
    
    var contact_props = function() {

        $contact.css({'min-height':$win.height()});
    	$contact_1.height($contact_1_map.height());
    
    };
    
    contact_props();
    
    $win.resize(function(){
        
    	contact_props();
    
    });
    
    

});
