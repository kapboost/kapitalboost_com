$(function(){
	
    var $win  		 	= 	$(window);
    var $doc  		 	= 	$(document);
    var $media 		 	= 	$('#media');
    var $media_1	 	=	$('#media-1');
    var $media_1_map 	=	$('#media-1-back');
    var $view_more	 	=	$('.view-more');
    var $media_lighbox	=	$('#media-lightbox');
    
    
    var media_props = function() {

        $media.css({'min-height':$win.height()});
    	$media_1.css({'min-height':$media_1_map.height() + 50});
    
    };
    
    media_props();
    
    $win.resize(function(){
        
    	media_props();
    
    });


});
