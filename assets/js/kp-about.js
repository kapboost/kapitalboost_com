$(function() {

	var $win  		 	= $(window);
    var $doc  		 	= $(document);
    var $about 		 	= $('#about');
    var $about_1	 	= $('#about-1');
    var $about_1_map 	= $('#about-1-back');
    var $about_1_inner  = $('#about_1_inner');
    
    var about_props = function() {

        $about.css({'min-height':$win.height()});
        $about_1.css({'min-height':$about_1_map.height()});
        $about_1_inner.css({'min-height':$about_1_map.height()});
    
    };
    
    about_props();
    
    $win.resize(function(){
        
    	about_props();
    
    });
    
    

});
