$(function(){
	
    var $win  		 	= 	$(window);
    var $doc  		 	= 	$(document);
    var $register 		= 	$('#register');
    var $register_1		=	$('#register-1');
    var $register_1_map =	$('#register-1-back');
    
    
    var register_props = function() {

        $register.css({'min-height':$win.height()});
        $register_1.css({'min-height':$register_1_map.height()});
    
    };
    
    register_props();
    
    $win.resize(function(){
        
    	register_props();
    
    });


});