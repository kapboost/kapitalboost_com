<?php
	
	// include_once './../admin/mysql.php';

	// fungsi untuk cek pembayaran semua investor
	function check_payout_calculation($member_id, $campaign_id = null, $update = false) {
		$mSql = new mysql();

		if ($mSql->Connection()) {
			if (is_null($campaign_id)) {
				$query = "SELECT tinv.*, tcampaign.m_payout, tcampaign.project_return from tinv inner join tcampaign on tinv.campaign_id = tcampaign.campaign_id where member_id = $member_id and no_master_payout = '0'";
			}elseif (!is_null($campaign_id)) {
				$query = "SELECT tinv.*, tcampaign.m_payout, tcampaign.project_return from tinv inner join tcampaign on tinv.campaign_id = tcampaign.campaign_id where tinv.campaign_id = $campaign_id and no_master_payout = '0'";
			}
			$invs = $mSql->conn->prepare($query);
	    $invs->execute();

	    $mSql->result = $invs->get_result();

	    $objs = array();

      if ($mSql->result->num_rows >= 1) {
         while ($row = $mSql->result->fetch_assoc()) {
            $objs[] = $row;
         }
    	}

    	$update_result = array('datas' => []);

	    // print_r(count($objs));
	    foreach ($objs as $inv) {
	    	// echo "<br>";
	    	// echo $inv['nama'] . " (". $inv['campaign'] .")";
	    	// echo "<br>Type : ". $inv['tipe'] . " (". $inv['date'] .")";
	    	// echo "<br>";
	    	// echo "<br>";
	    	$exp = $inv['expected_payout'];
	    	// echo "expected payout amount = $exp";
	    	// echo "<br>";
	    	$each_month = payouts_amount($inv);
	    	$new_calculation = round(calculate_from_return($inv['total_funding'], $inv['project_return']), 2);
	    	// echo "<br>";
	    	// echo $new_calculation;
	    	// echo "<br>";
	    	
	    	$saved_amount = round($inv['expected_payout'], 2);
	    	if ($new_calculation != $each_month || $new_calculation != $saved_amount) {
	    		// echo "Hasil Perbandingan ==> [[Nominal Tidak Sesuai]]";
	    		$update_payout = update_payout_amount($mSql, $inv, $new_calculation, ($new_calculation == $each_month), $update);
	    		$update_result['datas'][$inv['id']] = array(
	    			'tinv_id' => $inv['id'], 
	    			'investor_name' => $inv['nama'], 
	    			'campaign_name' => $inv['campaign'], 
	    			'status' => ($update_payout['result']) ? 'success':'danger', 
	    			'total_payout' => $each_month,
	    			'expected_payout' => $saved_amount,
	    			'nominal' => $new_calculation, 
	    			'msg' => $update_payout['err']
	    		);
	    	}else{
	    		// echo "Hasil Perbandingan ==> [[Nominal Sesuai]]";
	    	}

	    	// echo "<br>";
	    	// echo "$saved_amount";
	    	// echo "<br>";
	    	
	    	// echo "<hr>";
	    	// echo "<br>";
	    	if (count($update_result['datas']) < 1) {
	    		$update_result['message'] = 'Semua Data Sesuai';
	    	}else{
	    		$update_result['message'] = 'Terdapat data yang tidak sesuai';
	    	}

	    	if ($update) {
	    		$update_result['updated'] = true;
	    	}
	    }
	    // $invs = mysqli_insert_id($this->conn);

	    return $update_result;
		}
	}

	function update_payout_amount($mSql, $inv, $new_calculation, $per_month = true, $update = true) {

		$sql = "";

		// $sql1 = "UPDATE tinv SET member_id = '$mid' WHERE id = '$id'";

		$params = array();

		if ($inv['expected_payout'] != $new_calculation) {
			$params['expected_payout'] = $new_calculation;
		}

		if (!$per_month) {
			$payoutA = explode("==", $inv['m_payout']);
			$amountA = $dateA = $statusA = array();

			for ($i = 1; $i < count($payoutA); $i++) {
		   $payouts = explode("~", $payoutA[$i-1]);
		   if ($payouts[0]) {
		   	$percentage_per_month = (float)$payouts[0]/100;
		   	$params["bulan_$i"] = round((float)$inv['expected_payout'] * $percentage_per_month, 2);
		   }
			}
		}

		$valueSets = array();

		foreach ($params as $key => $value) {
      $valueSets[] = $key . " = '" . $value . "'";
    }

    if ($update) {
    	$query = "UPDATE tinv SET ".join(", ",$valueSets)." WHERE id='".$inv['id']."'";
	    $result = $mSql->conn->query($query);
	    $err = $mSql->conn->error;

	    return array('result' => $result, 'err' => $err);
    }else{
    	return array('result' => 1, 'err' => null);
    }
    
  //   echo "=====";
  //   echo "<br>";
  //   print_r($query);
  //   echo "<br>";
  //   echo "Result : ";
		// print_r($result);
		// echo "<br>";
		// echo "Error : ";
		// print_r($err);
		// echo "<br>";

		// echo "lorem ipsum dolor sit amet ========================================================================";
    // return array('result' => $result, 'err' => $err);
    // return array('result' => 1, 'err' => null);
	}

	function calculate_from_return($amount, $return) {
		$result = $amount + ($amount * ($return/100));

		// echo "<br>";
		// echo "New Calculation = ".$result." (total funding + (total funding x (return / 100)))";
		// echo "<br>";
		// echo "<br>";

		if (round($result, 1) == 0) {
			$result = round($result, 1);
		}

		return $result;
	}

	function payouts_amount($payout) {
		$cal = 0;
		for ($i=1; $i <= 12; $i++) { 
			$per_mounth = "bulan_$i";
			// echo "<br>";
			// echo "$per_mounth";
			// echo " => $payout[$per_mounth]";
			$cal += $payout[$per_mounth];
		}

		$cal = round($cal, 2);

		// simulation
		// $a = 10.14;
		// $b = 21.82;
		// $c = 21.95;
		// $x = 11.05;

		// $d = $a - round($a, 1);
		// $e = $b - round($b, 1);
		// $f = $c - round($c, 1);
		// $z = $x - round($x, 1);
		// $d = round($d, 1);
		// echo "<hr>";
		// echo "input A : $a";
		// echo "<br>";
		// if ($d > 0 && $d < 0.1) {
		// 	$cs = $d > 0 && $d < 0.1;
		// 	$a = round($a);
		// 	echo "$d > 0 && $d < 0.1 : ".$cs;
		// 	echo "<br>";
		// 	echo "A = $a";
		// 	echo "   : $d";
		// }else{
		// 	echo "A = $a";
		// 	echo "   : $d";
		// }

		// echo "<hr>";
		// echo "input B : $b";
		// echo "<br>";
		// if ($e > 0 && $e < 0.1) {
		// 	$b = round($b);
		// 	echo "B = $b";
		// 	echo "   : $e";
		// }else{
		// 	echo "B = $b";
		// 	echo "   : $e";
		// }
		// echo "<hr>";

		// echo "input C : $c";
		// echo "<br>";
		// if ($f > 0 && $f < 0.1) {
		// 	$f = round($f);
		// 	echo "C = $c";
		// 	echo "   : $f";
		// }else{
		// 	echo "C = $c";
		// 	echo "   : $f";
		// }
		// echo "<hr>";
		// echo "input X : $x";
		// echo "<br>";
		// if ($z > 0 && $z < 0.1) {
		// 	$x = round($x);
		// 	echo "X = $x";
		// 	echo "   : $z";
		// }else{
		// 	echo "X = $x";
		// 	echo "   : $z";
		// }
		// echo "<hr>";
		// echo "<hr>";
		// echo "<hr>";
		// echo "<hr>";

		$c = $cal - round($cal, 2);
		$a = round($c, 1);
		$c = round($c, 2);
		$d = round($cal, 2);
		// echo "<br>a =  $a";
		// echo "<br>c =  $c";
		// echo "<br>";
		// echo "---->>  $cal - $d =  $c : " . ($c > 0 && round($cal, 2) < 0.1);
		// echo "<br>";

		if ($c > 0 && round($c, 1) < 0.1) {
			$cal = round($cal);
		}

		// echo "<br>";
		// echo "Total Penjumlahan Perbulan = $cal";
		// echo "<br>";

		return $cal;
	}


// $result = check_payout_calculation(5682);
// echo "<br>";
// echo "<br>";
// print_r($result);

?>