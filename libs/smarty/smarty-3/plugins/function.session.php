<?php 
/** 
 * Smarty plugin 
 * @package Smarty 
 * @subpackage plugins 
 */ 


/** 
 * Assigns a session variable within the template. 
 * 
 * Type:     function<br> 
 * Name:     session<br> 
 * Purpose:  Assigns a session variable within the template. 
 * @link http://www.smarty.net/manual/en/language.function.session.php 
 *          session (Smarty online manual) 
 * @author   Bimal Poudel <smarty at bimal dot org dot com dot np> 
 */ 
function smarty_function_session($params=array(), &$smarty) 
{ 
   $total = count($params); 
    
   if($total) 
   foreach($params as $index => $value) 
   { 
      $_SESSION[$index] = $value; 
   } 
    
   return(false); 
} 
?>