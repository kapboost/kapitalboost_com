<?php

/**
 * Kapitalboost OTP and Access Token
 */
class KapitalOTP {

       public function check($phone) {
             
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     printf("Connect failed: %s\n", mysqli_connect_error());
                     exit();
              }

              $otp = mysqli_real_escape_string($mysqli, $o);
              $at = mysqli_real_escape_string($mysqli, $a);
              $id = mysqli_real_escape_string($mysqli, $i);

              $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "' . $phone . '"';

              $query = mysqli_query($mysqli, $sql);
              $data = mysqli_fetch_array($query);
              return $data;
              
       }

}

$xfersphone = $_POST["xfersphone"];
$orderid = $_POST["order_id"];
$amount = $_POST['amount'];
$firstname = $_POST["username"];
$lastname = $_POST["lastname"];
$member_id = $_POST["member_id"];
$member_country = $_POST["member_country"];
$refundable = $_POST["refundable"];
$currency = $_POST["currency"];
$item_name = $_POST["item_name"];
$item_id = $_POST["item_item_id_1"];
$item_description = $_POST["item_description_1"];
$item_quantity = $_POST["item_quantity_1"];
$campaign_id = $_POST["campaign_id"];
$member_email = $_POST["member_email"];
$project_type = $_POST["project_type"];
$campaign_token = $_POST["campaign_token"];
$campaign = array(
    "campaign_type" => $project_type,
    "campaign_id" => $campaign_id
);
$cjson = json_encode($campaign);
echo '
    <script>
      localStorage.setItem("campaign", JSON.stringify(' . $cjson . '));
    </script>
  ';

$params = '{
    "amount": "' . $amount . '",
    "campaign_type" : "' . $project_type . '",
    "currency": "' . $currency . '",
    "redirect": "false",
    "notify_url": "https://kapitalboost.com",
    "return_url": "https://kapitalboost.com",
    "cancel_url": "https://kapitalboost.com",
    "order_id": "' . $orderid . '",
    "description": "' . $item_name . '",
    "shipping": "0",
    "tax": "0.0",
    "refundable" : "false",
    "debit" : "true",
    "member_email" : "' . $member_email . '",
    "member_name" : "' . $firstname . '",
    "token" : "' . $campaign_token . '",
    "items": [
      {
        "description": "' . $item_name . '",
        "price": "' . $amount . '",
        "quantity": 1,
        "name": "' . $item_name . '"
      }
    ],
    "meta_data": {
      "firstname": "' . $firstname . '",
      "lastname": "' . $lastname . '"
    }
  }';
$current_phone = array(
    "phone" => $xfersphone
);
$current_location = array("country" => $member_country);
$cl = json_encode($current_location);
$cp = json_encode($current_phone);

$access = KapitalOTP::check($xfersphone);
// if (!$access["0"] == "") {
//   echo "Success";
//   print_r(json_encode($access));
// } else {
//   echo "Not Success";
// }
// die();
if (!$access["0"] == "") {

       // True status
       // when OTP/Access_Token is available in database
       // let redirect to next step

       echo '
      <script type="text/javascript">
        localStorage.setItem("kapitaluser", JSON.stringify(' . $params . '));
        localStorage.setItem("user", JSON.stringify(' . $cp . '));
               localStorage.setItem("country", JSON.stringify(' . $cl . '));
        window.location = "/dashboard/payment/xfers-ballance?mid=' . $member_id . '";
      </script>
      ';

       // echo file_get_contents("partials/index_no_otp.php");
} else {

       // False status
       // when OTP/Access_Token is not available in database
       // let go to step 1 | request OTP

       echo '
      <script type="text/javascript">
        localStorage.setItem("kapitaluser", JSON.stringify(' . $params . '));
        localStorage.setItem("user", JSON.stringify(' . $cp . '));
               localStorage.setItem("country", JSON.stringify(' . $cl . '));
window.location = "/dashboard/payment/xfers-otp";
        
      </script>
      ';
}
/**/
?>
