<!DOCTYPE html>
<html>
<head>
  <title>Xfers User Bllance</title>
  <!-- start style css -->
<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap-theme.min.css" />
<link rel="stylesheet" href="/assets/css/new-kp-main.css" />
<link rel="stylesheet" href="/assets/css/new-kp-home.css" />
<link rel="stylesheet" href="/assets/css/kp-about.css" />
<link rel="stylesheet" href="/assets/css/kp-default.css" />
<link rel="stylesheet" href="/assets/css/kp-blog.css" />
<link rel="stylesheet" href="/assets/css/kp-event.css" />
<link rel="stylesheet" type="text/css" href="assets/css/gaya.css">
<!-- end style css -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body ng-app="xfers" ng-controller="xfersCtrl">
<!-- New UI -->
<center>
  <!-- start header -->
<div id="menu">
    <div id="menu-back"></div>
            <center>
                                <ul class="newMenu">
 <li><a href="https://kapitalboost.com/dashboard/payment-history"> Portfolio </a></li>
<li><a href="https://kapitalboost.com/dashboard/profile"> Manage Profile </a></li>
                      
                       
                    </ul>
            <ul class="dasUl">
                         <li>
                            <a href="dashboard/payment-history" style="cursor:pointer;">
                               Payment History
                            </a>
                         </li>
<!--
                         <li>
                            <a href="dashboard/invest-history" style="cursor:pointer;">
                              Portfolio
                            </a>
                         </li>
-->
                         <li>
                            <a href="dashboard/profile"style="cursor:pointer;">
                                Profile
                            </a>
                         </li>
                     </ul>
            </center>
    <div id="menu-front">
        <div id="menu-front-inner">
            <div id="menu-logo">
                <a href="">
                <img alt="Kapital Boost Logo" id="menu-logo-img" src="/assets/images/logo.png" />
                </a>
            </div>
            <ul id="kb-menu">
<li><a href="">home</a>
                </li>
                <li><a href="about">About</a>
                </li>
                <li><a href="campaign/">Campaigns</a>
                </li>
                <li><a id="eth-log">Get Funded</a>
                </li>
                <li><a href="how-it-works">How It Works</a>
                </li>  
                                <li><a id="eth-log" class="loginRegisMenu" style="color: #609edf;">Login</a> 
                </li>
                <li><a id="reg" class="loginRegisMenu" style="color: #609edf;">Register</a>
                </li>
                            </ul>
            <div id="rwd-menu">
                <div id="rwd-menu-inner">
                    <div class="rwd-menu-each"></div>
                    <div class="rwd-menu-each"></div>
                    <div class="rwd-menu-each"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="logpopupWrap">
 <div class="logpopupInner">
  <div class="logpopupClose">
   <span class="glyphicon glyphicon-remove"></span>
  </div>
  <div class="logpopupTitle">
   Are you sure want to logout?
  </div>
  <ul class="logpopupYesNo">
   <li><a href="logout">Yes</a></li>
   <li><a id="logpopupClose">No</a></li>
  </ul>
 </div>
</div>

 
    <script>
$(document).ready(function(){

 $('#testbro').click(function(){
  $('.logpopupWrap').fadeIn(500);
 });

 $('#logpopupClose').click(function(){
  $('.logpopupWrap').fadeOut(500);
 });

 $('.logpopupClose').click(function(){
  $('.logpopupWrap').fadeOut(500);
 });
});

/*
$("#dialog").dialog({
   autoOpen: false,
   modal: true,
   resizable: false,
   height: "auto",
   width: 400,
   buttons : {
        "Yes" : function() {
            window.location.href="https://kapitalboost.com/logout";           
        },
        "No" : function() {
          $(this).dialog("close");
        }
      }
    });

$("#testbro").on("click", function(e) {
    e.preventDefault();
    $("#dialog").dialog("open");
});
*/
    </script>
  <!-- end header -->



  <!-- start xfers balance -->
  <div class="xfersBalance">
    <div class="XBInner">
      <div class="XBLogo">
        <img src="/payment_design/filesNew/images/xfersLogo.png" />
      </div>
      <div class="XBTitle">
        <p>
          Checking your balance
        </p>
      </div>
      <div class="XBYourBalance">
        <div>
          <span>Your balance</span>
          <p>S$ {{xballance.available_balance}}</p>
        </div>
        <div>
          <span class="glyphicon glyphicon-refresh" ng-click="userBallance()"></span>
          <p>
            Refresh
          </p>
        </div>
      </div>
      <div class="XBButton">
        <div>
          <form>
            <button ng-click="xfersCheckout()">
              Pay Now - S$ {{params.amount}}
            </button>
          </form>
        </div>
        <div>
          <form action="transferinfo.php">
            <button style="margin-top:20px;">
              <a style="color:#FFFFFF" >Deposit</a>
            </button>
          </form>
        </div>
        {{status}}
      </div>
    </div>
  </div>
  <!-- end x fers balance -->
</center>



  <!-- start footer -->
    <div id="footer">
    <div id="footer-inner">
        <div id="footer-main">
            <div id="footer-main-1">
                <h3 class="foot-header">About</h3>
                <p><a href="about">About</a></p>
                <p><a href="how-it-works">How it works</a></p>
                <p><a href="media">Media</a></p>                            
            </div>
            <div id="footer-main-2">
                <h3 class="foot-header">General</h3>
                <p><a href="campaign/">Campaigns</a></p>
                <p><a href="blog">Blog</a></p>
                <p><a href="partners">Partners</a></p>
            </div>
            <div id="footer-main-3">
                <h3 class="foot-header">Support</h3>
                                <p><a id="eth-log" style="cursor:pointer;">Get Funded</a></p>
                                <p><a href="faq">FAQs</a></p>
                <p><a href="contact-us">Contact Us</a></p>
            </div>
            <div id="footer-main-5">
                <h3 class="foot-header">Legal</h3>
                <p><a href="https://kapitalboost.com/legal#term">Term of use</a></p>
                <p><a href="https://kapitalboost.com/legal#privacy">Privacy Policy</a></p>
                <p><a href="https://kapitalboost.com/legal#risk">Risk Statement</a></p>
            </div>
            <div id="footer-main-4">
                <h3 class="foot-header foot-header-2">Follow Us</h3>
                
                <p>
                    <a class="sc-fb-icon" href="https://www.facebook.com/kapitalboost" target="_blank"><img alt="" src="../assets/images/facebook.png" /></a>
                    <a class="sc-ln-icon" href="https://www.linkedin.com/company/6597611?trk=tyah&trkInfo=clickedVertical:company,clickedEntityId:6597611,idx:1-1-1,tarId:1440685694219,tas:kapital%20boost" target="_blank"><img alt="" src="../assets/images/linkedin.png" /></a>
                    <a class="sc-tw-icon" href="https://twitter.com/kapitalboost" target="_blank"><img alt="" src="../assets/images/twitter-white.png" /></a>
                </p>
                <div id="ssl-con"><img alt="" id="ssl-img" src="../assets/images/SSL-CERT.png" /></div>
            </div>
        </div>
        
        <div id="footer-copy">&copy; COPYRIGHT 2017 &bull; KAPITAL BOOST PTE LTD &bull; SINGAPORE 201525866W</div>
        <div id="footer-disclaimer">
            <div id="disclaimer-content">
                <p>This website and the contents herein do not constitute as any financial advice, investment advice or solicitation for the purposes of making financial investments in Singapore or other territories. Kapital Boost Pte. Ltd. is a firm specialising in the matching of opportunities between our registered members and small to medium-sized enterprises. With regard to these opportunities, the need and onus to do due diligence lies squarely with our members as we do not profess to advise on the same. All dealings and transactions are directly with the businesses, project owners or authorized agents we refer to our members.</p>
                <p>Kapital Boost Pte. Ltd. is not licensed and/or registered under the Securities & Futures Act of Singapore or the Financial Advisor's Act under the Monetary Authority of Singapore and thus cannot offer, solicit, advice or recommend the buying and selling of investment securities or any other financial and banking products from the public.</p>
            </div>
        </div>
  </div>
  </div>
  <!-- end footer -->
<!-- End UI -->

  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
   <script type="text/javascript">

    var app = angular.module("xfers", []);
    app.controller("xfersCtrl", function($scope, $http){
      $scope.xballance = {};
      $scope.xcheckout = {};
      $scope.params = {};

      var chargeparams = function(){
        return $scope.params = JSON.parse(localStorage.getItem('kapitaluser'));
      }; chargeparams();
      var getPhone = function(){
        return $scope.cphone = JSON.parse(localStorage.getItem('user'));
      };
      var getCampaign = function(){
        return $scope.cCampaign = JSON.parse(localStorage.getItem('campaign'));
      };
      
      $scope.userBallance = function(){
        getPhone();
        var url = "apis/ballance.php";
        
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
                "getuserballance": $scope.cphone.phone
            }
        }).success(function(res) {
            $scope.xballance = res;
        }).error(function(err) {
            alert("No Internet Connection");
        });
      };

      var isuserBallance = function(){
        getPhone();
        var url = "apis/ballance.php";
        
        $http({
            method: 'POST',
            url: url,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            transformRequest: function(obj) {
                var str = [];
                for (var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {
                "getuserballance": $scope.cphone.phone
            }
        }).success(function(res) {
            $scope.xballance = res;
        }).error(function(err) {
            alert("No Internet Connection");
        });
      };

      isuserBallance();
      
      $scope.xfersCheckout = function(){
        var url = "apis/charge.php";
        chargeparams();
        getPhone();
        getCampaign();
        
        var paramslha = {
                "amount": $scope.params.amount, 
                "campaign_type" : $scope.params.campaign_type,
                "currency": $scope.params.currency, 
                "redirect": "false", 
                "notify_url": "https://kapitalboost.com",
                "return_url": "https://kapitalboost.com",
                "cancel_url": "https://kapitalboost.com",
                "order_id": $scope.params.order_id,
                "description": $scope.params.description,
                "debit_only": true,
                "refundable" : false,
                "phone" : $scope.cphone.phone,
                "shipping": "0",
                "tax": "0",
                "member_email" : $scope.params.member_email,
                "a_name" : $scope.params.member_name,
                "token" : $scope.params.token,
                "items" : [
                  {
                    "description":$scope.params.description,
                    "price":$scope.params.amount,
                    "quantity":1,
                    "name": $scope.params.items[0].name
                  }
                ], 
                "meta_data": {
                  "firstname":$scope.params.firstname, 
                  "lastname":$scope.params.lastname
                }        
            };


        $http({
            method: 'POST',
            url: url,
            data: paramslha
        }).success(function(res) {
            localStorage.setItem('status', JSON.stringify(res));
            if (res.error == false) {
              window.location.href = "thankyou.php?campaign="+$scope.params.description+"&campaignid="+$scope.cCampaign.campaign_id+"&amount="+$scope.params.amount+"&type="+$scope.cCampaign.campaign_type;
            } else {
              $scope.status = res;
            }
        }).error(function(err) {
            alert("No Internet Connection");
        });

      };
      // Xfers User Ballance
    });
  </script>
</body>
</html>