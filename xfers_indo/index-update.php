<?php

  /**
  * Kapitalboost OTP and Access Token
  */
  class KapitalOTP
  {
    public function check($phone){
      /*
      * $o = OTP
      * $a = Access Token
      * $i = Id
      *
      * OTP is member_otp , Access Token is member_access_token and Id is member_id or the mean is unique value from database
      * i call Id but after in production i will use email :D
      * Created Bye Mochamad Imam Ghozalie
       */
      $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

      /* check connection */
      if (mysqli_connect_errno()) {
          printf("Connect failed: %s\n", mysqli_connect_error());
          exit();
      }

      $otp = mysqli_real_escape_string($mysqli,$o);
      $at = mysqli_real_escape_string($mysqli,$a);
      $id = mysqli_real_escape_string($mysqli,$i);

      $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "'.$phone.'"';
      
      $query = mysqli_query($mysqli, $sql);
      $data = mysqli_fetch_array($query);
      return $data;
      // if (!empty($query)) {
      //   return true;
      // } else {
      //   return false;
      // }
    }
  }


  $xfersphone = $_POST["xfersphone"];
  $orderid = $_POST["order_id"];
  $amount = $_POST['amount'];
  $firstname = $_POST["username"];
  $lastname = $_POST["lastname"];
  $refundable = $_POST["refundable"];
  $currency = $_POST["currency"];
  $item_name = $_POST["item_name"];
  $item_id = $_POST["item_item_id_1"];
  $item_description = $_POST["item_description_1"];
  $item_quantity = $_POST["item_quantity_1"];

  $params = '{
    "amount": "'.$amount.'",
    "currency": "'.$currency.'",
    "redirect": "false",
    "notify_url": "https://kapitalboost.com",
    "return_url": "https://kapitalboost.com",
    "cancel_url": "https://kapitalboost.com",
    "order_id": "'.$orderid.'",
    "description": "'.$item_description.'",
    "shipping": "0",
    "tax": "0.0",
    "refundable" => "false",
    "debit" => "true",
    "items": [
      {
        "description": "'.$item_description.'",
        "price": "'.$amount.'",
        "quantity": 1,
        "name": "'.$item_name.'"
      }
    ],
    "meta_data": {
      "firstname": "'.$firstname.'",
      "lastname": "'.$lastname.'"
    }
  }';


  $access = KapitalOTP::check($xfersphone);
  if ($access) {
    // True status
    // when OTP/Access_Token is available in database
    // let redirect to next step
    echo '
      <script type="text/javascript">
        localStorage.setItem("kapitaluser", JSON.stringify('.$params.'));
      </script>
      ';
      echo file_get_contents("partials/index_no_otp.php");
  } else {
    
    // False status
    // when OTP/Access_Token is not available in database
    // let go to step 1 | request OTP
    echo '
      <script type="text/javascript">
        localStorage.setItem("kapitaluser", JSON.stringify('.$params.'));
      </script>
      ';

    echo file_get_contents("partials/index_req_otp.php");
  }
  ?>
