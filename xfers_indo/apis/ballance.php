<?php

/**
 * Kapitalboost OTP and Access Token
 */
class KapitalOTP {

       public function getuserballance($phone) {

              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     return "Error";
              }

              $p = mysqli_real_escape_string($mysqli, $phone);

              $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "' . $p . '"';

              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     $data = mysqli_fetch_array($query);
                     return $data;
              } else {
                     return false;
              }
       }

}

if (isset($_POST["getuserballance"])) {
       $phone = $_POST["getuserballance"];
       $curl = curl_init();
       $a = KapitalOTP::getuserballance($phone);
       if ($a) {
              $access_token = $a[0];
              curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://id.xfers.com/api/v3/user",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "GET",
                  CURLOPT_HTTPHEADER => array(
                      "x-xfers-user-api-key: $access_token"
                  ),
              ));

              $response = curl_exec($curl);
              $err = curl_error($curl);

              curl_close($curl);

              if ($err) {
                     echo "cURL Error #:" . $err;
              } else {
                     echo $response;
              }
       } else {
              echo "Error";
       }
}
?>