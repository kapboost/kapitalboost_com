<?php
  /*

    Author       : Mochamad Imam Ghozalie
    name         : Kapitalboost email notification for xfers payments
    description  : create template email and return back.

  */
  function tplCrowdfunding($name, $cname, $amount){
    $member_name = $name;
    $campaign_name = $cname;
    $amount = $amount;
    $msg = "
    <html>
    <head>
      <title>Kapitalboost</title>
    </head>
    <style type='text/css'>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
    </style>
    <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
      <table height='100%' width='100%' bgcolor='#305e8e'>
        <tbody>
          <tr>
            <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
              <center>
                <div style='display: inline-block;width: 640px;background: #00b0fd;'>
                  <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                    <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
                    <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                     Dear ".$name.",
                    </p>
                    <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                      Thank you for your fund transfer of <b>SGD ".$amount."</b> for investment in the <b>".$campaign_name."</b> crowdfunding project.
                    </p>
                    <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                      To check on the status and repayment schedules of your investments,
                      click on Portfolio under the Dashboard.
                      If you have questions, please email <a href=\"mailto:support@kapitalboost.com\">support@kapitalboost.com</a>.
                    </p>
                    <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                      Best regards, <br>
                      Kapital Boost team
                    </p>
                  </div>
                  <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                    <p style='font-size: 16px;color: white;width:100%'>
                      &copy; COPYRIGHT 2017 - KAPITAL BOOST PTE LTD
                    </p>
                  </div>
                </div>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </body>
    </html>

    ";
    return $msg;
  }

  function tplDonation($name, $cname, $amount){
    $member_name = $name;
    $campaign_name = $cname;
    $amount = $amount;
    $msg = "
    <html>
    <head>
      <title>Kapitalboost</title>
    </head>
    <style type='text/css'>
    @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
    </style>
    <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
      <table height='100%' width='100%' bgcolor='#305e8e'>
        <tbody>
          <tr>
            <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
              <center>
                <div style='display: inline-block;width: 640px;background: #00b0fd;'>
                  <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                    <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
                    <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                     Dear ".$name.",
                    </p>
                    <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                      Thank you for your generous donation of SGD <b>".$amount."</b> for the <b>".$campaign_name."</b> project.
                    </p>
                    <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                      May your kindness be rewarded in multiplefold.
                    </p>
                    <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                      Best regards, <br>
                      Kapital Boost team
                    </p>
                  </div>
                  <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                    <p style='font-size: 16px;color: white;width:100%'>
                      &copy; COPYRIGHT 2017 - KAPITAL BOOST PTE LTD
                    </p>
                  </div>
                </div>
              </center>
            </td>
          </tr>
        </tbody>
      </table>
    </body>
    </html>

    ";
    return $msg;
  }


?>
