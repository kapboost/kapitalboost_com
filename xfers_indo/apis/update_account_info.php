<?php

$data = json_decode(file_get_contents('php://input'));
$curl = curl_init();

class KapitalOTP {

       public function getAccesstoken($phone) {
              /*
               * $phone = PHONE
               *
               * OTP is member_otp , Access Token is member_access_token and Id is member_id or the mean is unique value from database
               * i call Id but after in production i will use email :D
               * Created Bye Mochamad Imam Ghozalie
               */
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     return "Error";
              }

              $p = mysqli_real_escape_string($mysqli, $phone);

              $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "' . $p . '"';

              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     $data = mysqli_fetch_array($query);
                     return $data[0];
              } else {
                     return false;
              }
       }

       public function insertXfersInfo($phone, $xudata) {
              /*
               * $phone = PHONE
               *
               * OTP is member_otp , Access Token is member_access_token and Id is member_id or the mean is unique value from database
               * i call Id but after in production i will use email :D
               * Created Bye Mochamad Imam Ghozalie
               */
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     return "Error";
              }

              $p = mysqli_real_escape_string($mysqli, $phone);

              $sql = "UPDATE  `tmember` SET  `xfers_info` =  '" . $xudata . "' WHERE member_mobile_no =  '" . $phone . "'";
              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     return true;
              } else {
                     return false;
              }
       }

}

$phone = $data->phone;
$params = $data->data;
$access_token = KapitalOTP::getAccesstoken($phone);
$datas = json_encode($params);
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://id.xfers.com/api/v3/user",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "PUT",
    CURLOPT_POSTFIELDS => "$datas",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
        "x-xfers-user-api-key: $access_token"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);
if ($err) {
       echo "cURL Error #:" . $err;
} else {

       $s_db = KapitalOTP::insertXfersInfo($phone, $datas);

       header("Content-type : Application/Json");
       echo $response;
}