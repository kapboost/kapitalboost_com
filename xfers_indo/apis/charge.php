<?php

/**
 * Kapitalboost OTP and Access Token
 */
class KapitalOTP {

       public function insert($o, $a, $i) {
              
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              /* check connection */
              if (mysqli_connect_errno()) {
                     printf("Connect failed: %s\n", mysqli_connect_error());
                     exit();
              }

              $otp = mysqli_real_escape_string($mysqli, $o);
              $at = mysqli_real_escape_string($mysqli, $a);
              $id = mysqli_real_escape_string($mysqli, $i);

              $sql = 'UPDATE tmember SET member_otp= "' . $otp . '", member_access_token="' . $at . '" WHERE id = "' . $id . '"';

              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     return true;
              } else {
                     return false;
              }
       }

       public function getuserballance($phone) {
              
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              if (mysqli_connect_errno()) {
                     printf("Connect failed: %s\n", mysqli_connect_error());
                     exit();
              }

              $p = mysqli_real_escape_string($mysqli, $phone);

              $sql = 'SELECT member_access_token FROM tmember WHERE member_mobile_no = "' . $p . '"';

              $query = mysqli_query($mysqli, $sql);
              if ($query) {
                     $res = mysqli_fetch_array($query);
                     return $res;
              } else {
                     return false;
              }
       }

       public function updateXfers($member_name, $member_email, $campaign_name, $amount, $campaign_owner, $project_type, $campaign_end, $token) {
              $mysqli = new mysqli('localhost', 'kapitalboost', 'kapital2016', 'kapitalboost');

              $sql = "UPDATE tinv SET status = 'Paid' WHERE token_pp = '$token'";


              $itinv = mysqli_query($mysqli, $sql);
              if (!$itinv) {
                     return "Error !" . mysqli_error();
              }

       }

}

$data = json_decode(file_get_contents('php://input'));

$cphone = $data->phone;

$a = KapitalOTP::getuserballance($cphone);
if ($a) {
       $access_token = $a[0];
       $data->user_api_token = $access_token;
       $data->currency = "IDR";
       $datas = json_encode($data);
	   // echo "'<script>console.log(\"amount : $datas\")</script>'";
       
       //print_r($data);

       $curl = curl_init();

       curl_setopt_array($curl, array(
           CURLOPT_URL => "https://id.xfers.com/api/v3/charges.php",
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => "$datas",
           CURLOPT_HTTPHEADER => array(
               "cache-control: no-cache",
               "content-type: application/json",
               "x-xfers-user-api-key: EZCgmQ9E1KzNBsorZDJmwdDCdQ6AfggKUefohFCbQXQ"
           ),
       ));

       $result = curl_exec($curl);
	   // echo "'<script>console.log(\"amount : $result\")</script>'";
       $err = curl_error($curl);
// $err = false;
       curl_close($curl);

       if ($err) {
              echo "cURL Error #:" . $err;
       } else {

              $response = json_decode($result, true);
              if ($response['status'] == 'completed' or $response['status'] == 'paid') {
                     //   # Transaction successful
                     include_once("email/email.payments.php");

                     $user = $data->a_name;
                     $campaign_name = $data->description;
                     $amount = $data->amount;
                     $token = $data->token;
                     $email = $data->member_email;
                     $campaign_type = $data->campaign_type;
                     if ($campaign_type == 'sme' or $campaign_type == 'private') {
                            $xmsgsuccess = tplCrowdfunding($user, $campaign_name, $amount);
                     } else {
                            $xmsgsuccess = tplDonation($user, $campaign_name, $amount);
                     }
                     /*
                       @ Update tinv to Paid
                       @ params ($member_name, $member_email, $campaign_name, $amount, $campaign_owner, $project_type, $campaign_end, $token){
                      */

                     $uXfersPaid = KapitalOTP::updateXfers($user, $email, $campaign_name, $amount, "Kosong", $campaign_type, "Kosong", $token);

                     /*
                       Sending Email
                      */
                     include_once("phpmailer.php");
                     $mail = new PHPMailer;
                     // From email address and name
                     $mail->From = "info@kapitalboost.com";
                     $mail->FromName = "Kapital Boost";
                     $to = $data->member_email;
                     $mail->addAddress($to);
                     $mail->isHTML(true);
                     $mail->Subject = "Payment Completed";
                     $mail->Body = $xmsgsuccess;
                     if (!$mail->send()) {
                            echo "Mailer Error: " . $mail->ErrorInfo;
                     }
                     /*
                       End Sending Email
                      */
                     $resXfers = array(
                         "status" => "Success" . $uXfersPaid,
                         "error" => false
                     );
                     header('Content-Type: application/json');
                     echo json_encode($resXfers);
              } else {
                     # debit unsuccessful, can be insufficient funds or KYC error.
                     echo "Transaction Failed! - " . 'Error : ' . $response['error'];
                     # meta data would contain error message if anything
                     echo $response['meta_data']['error_code'];
              }
       }
       // Enf Of API Charges
} else {
       echo "Error";
}
?>
