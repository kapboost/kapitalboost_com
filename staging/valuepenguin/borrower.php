<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">

	<!-- Open Graph -->
	<meta property='fb:app_id' content="1419559388071407"/>
	<meta property='og:type' content="website" />
	<meta property='og:image' content="https://kapitalboost.com/assets/images/Slide1.jpg" />
	<meta property='og:title' content="Kapital Boost: Islamic crowdfunding - Business Finance" />
	<meta property='og:site_name' content="Kapital Boost Crowdfunding" />
	<meta property='og:description' content="Kapital Boost is Asia’s first Islamic P2P crowdfunding platform for SMEs. Learn more about getting funding or investing ethically with us today." />
	<meta property='og:url' content="https://kapitalboost.com" />
	<meta property='og:image:width' content="336" />
	<meta property='og:image:height' content="201" />
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- Twitter Theme -->
	<meta name="twitter:widgets:theme" content="light">
	
	<!-- Title &amp; Favicon -->
	<title>Kapital Boost: Islamic crowdfunding - Business Finance</title>
	<link rel="shortcut icon" type="image/x-icon" href="https://kapitalboost.com/assets/images/kp-fav.png">

	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7CHind+Madurai:400,500&amp;subset=latin-ext" rel="stylesheet">
	
	<!-- Css -->
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/core.min.css" />
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/skin.css" />
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/kap.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

	<!--[if lt IE 9]>
    	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="shop home-page">

	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">

			<!-- Header -->
			<header class="header header-absolute header-transparent header-fixed-on-mobile" data-helper-in-threshold="200" data-helper-out-threshold="500" data-sticky-threshold="200" data-bkg-threshold="100" data-compact-threshold="100">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
							<div class="row">
								<div class="column width-5 primary-nav-column">
									<nav class="navigation nav-block primary-navigation nav-right sub-menu-indicator">
										<ul>
											<li></li>
											<li></li>
											<li></li>
										</ul>
									</nav>
								</div>
								<div class="column width-2 logo-column">
									<div class="logo logo-center">
										<div class="logo-inner">
											<a href="https://kapitalboost.com/vp/borrower"><img src="https://kapitalboost.com/staging/valuepenguin/images/logo-full.png" alt="Kapital Boost Logo" /></a>
											<a href="https://kapitalboost.com/vp/borrower"><img src="https://kapitalboost.com/staging/valuepenguin/images/full-logo-white-kapital.png" alt="Kapital Boost Logo" /></a>
										</div>
									</div>
								</div>
								<div class="column width-5 secondary-nav-column">
									<nav class="navigation nav-block primary-navigation nav-left sub-menu-indicator">
										<ul>
											<li></li>
											<li></li>
											<li></li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->

			<!-- Content -->
			<div class="content clearfix">

				<!-- Fullscreen Slider Section -->
				<div class="section-block featured-media window-height tm-slider-parallax-container">
					<div class="tm-slider-container full-width-slider" data-featured-slider data-parallax data-parallax-fade-out data-animation="slide" data-speed="1000" data-scale-under="960" data-scale-min-height="450">
						<ul class="tms-slides">
							<li class="tms-slide" data-image data-as-bkg-image data-force-fit data-overlay-bkg-color="#454580" data-overlay-bkg-opacity="0.3" data-animation="slideTopBottom">
								<div class="tms-content">
									<div class="tms-content-inner center left-on-mobile v-align-middle">
										<div class="row">
											<div class="column width-12">
												<h1 class="tms-caption weight-light color-white mb-5" data-no-scale data-animate-in="preset:slideInUpShort;duration:800ms;delay:400ms;">
													Need funding for asset purchase? <br /> Looking to liquidate your invoices? 
												</h1>  <br />  <br /> 
												<div class="clear"></div>
												<h1 class="tms-caption weight-semi-bold color-white mb-40" data-no-scale data-animate-in="preset:slideInUpShort;duration:800ms;delay:100ms;">
													Let us help. <br /> We offer a fast and simple financing option for SMEs.
												</h1>
												<div class="clear"></div>
												<a href="#services" class="tms-caption scroll-link button rounded large text-small text-uppercase shadow bkg-blue color-white bkg-hover-theme color-hover-white"
													data-offset="-20"
													data-animate-in="preset:slideInUpShort;duration:800ms;delay:1000ms;"
													data-no-scale>
													Get Funded
												</a>
											</div>
										</div>
									</div>
								</div>
								<img data-src="https://kapitalboost.com/assets/images/landImage2.jpg" data-retina src="images/blank.png" alt=""/>
							</li>
						</ul>
					</div>
				</div>
				<!-- Fullscreen Slider Section End -->

				<!-- Logo Section 1 -->
				<div class="section-block replicable-content ">
					<div class="row">
						<div class="column width-10 offset-1 center">
							<h3 class="mb-50">Why Kapital Boost?</h3>
						</div>
					</div>
					<div class="row flex boxes">
						<div class="column width-4">
							<div class="feature-column box rounded large bkg-white center horizon" data-animate-in="preset:flipInY;duration:1000ms;" data-threshold="1">
								<i class="fas fa-rocket feature-icon color-gradient-purple-haze"></i>
								<div class="feature-text">
									<h4>Fast and simple approval process</h4>
									<p>Get approved within days. Raise financing in less than a week</p>
								</div>
							</div>
						</div>
						<div class="column width-4">
							<div class="feature-column box rounded large bkg-white center horizon" data-animate-in="preset:flipInY;duration:1000ms;delay:200ms;" data-threshold="1">
								<i class="fas fa-hand-holding-usd feature-icon color-gradient-purple-haze"></i>
								<div class="feature-text">
									<h4>Competitive funding cost</h4>
									<p>Our funding cost is favourable to most non-bank financing options</p>
								</div>
							</div>
						</div>
						<div class="column width-4">
							<div class="feature-column box rounded large bkg-white center horizon" data-animate-in="preset:flipInY;duration:1000ms;delay:400ms;" data-threshold="1">
								<i class="fas fa-chart-line feature-icon color-gradient-purple-haze"></i>
								<div class="feature-text">
									<h4>Increase business exposure</h4>
									<p>Be known in the market. Build network with our community</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Logo Section 1 End -->
				
				<div class="section-block hero-5 hero-5-2 clear-height show-media-column-on-mobile bkg-ash">
					<div class="media-column width-6">
						<div class="media-overlay bkg-black opacity-05"></div>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/PcIBqRsKRlc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
					<div class="row">
						<div class="column width-5 push-7">
							<div class="hero-content split-hero-content">
								<div class="hero-content-inner left horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;delay:200ms;" data-threshold="0.5">
									<p class="lead color-white font35">What We Do</p>
									<p class="color-grey-light">Kapital Boost aims to tackle the lack of financing available to small businesses in Southeast Asia. We help level the playing field by offering these businesses a crowdfunding platform to access temporary liquidity for goods and capital purchases.</p>
									<p class="color-grey-light">We also address the shortage of attractive Islamic-based investments. By funding small businesses, Kapital Boost members (funders) have the opportunity to earn attractive returns on short-term, Shariah-structured deals. In other words, our members get rewarded for doing good.  </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Hero 5 Section End -->

				<!-- Logo Slider -->
				<div class="section-block pt-80 pb-80 bkg-white">
					<div class="row">
						<div class="column width-12 center">
							<h3 class="mb-50">Featured On</h3>
						</div>
					</div>
					<div class="row">
						<div class="column width-12">
							<div class="tm-slider-container logo-slider pb-30" data-nav-arrows="false" data-nav-show-on-hover="false" data-nav-dark data-nav-keyboard="false" data-auto-advance data-auto-advance-interval="4000" data-progress-bar="false" data-pause-on-hover="false" data-carousel-visible-slides="5">
								<ul class="tms-slides">	
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/bloomberg1.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/salam.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/CrowdfundLogo.jpg" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/techinasia.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/straits.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/markets.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/newbfm.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/bh.png" src="images/blank.png" alt=""/>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<!-- Service Section -->
				<div id="services" class="section-block replicable-content bkg-grey-ultralight">
					<div class="row">
						<div class="column width-6">
							
							<div class="contact-form-container"> 
								<form class="getfund" action="prog/" method="POST">
									<div class="row">
										<div class="column width-8">
											<div class="field-wrapper">
												<input type="text" name="fname" class="form-fname form-element rounded medium" placeholder="Full Name" tabindex="1" value="" required>
											</div>
										</div>
										<div class="column width-8">
											<div class="field-wrapper">
												<input type="text" name="email" class="form-fname form-element rounded medium" placeholder="Email Address" tabindex="1" value="" required>
											</div>
										</div>
										<div class="column width-8">
											<div class="field-wrapper">
												<input type="text" name="mobile" class="form-fname form-element rounded medium" placeholder="Mobile Number" tabindex="1" value="" required>
											</div>
										</div>
										<div class="column width-8">
											<div class="field-wrapper">
												<input type="text" name="company" class="form-fname form-element rounded medium" placeholder="Company Name" tabindex="1" value="" required>
											</div>
										</div>
										<div class="column width-8">
											<div class="field-wrapper">
												<input type="number" name="year" class="form-fname form-element rounded medium" placeholder="Year established" tabindex="1" value="" required>
											</div>
										</div>
									
										
										<!-- <div class="column width-6"> -->
											<!-- <div class="field-wrapper"> -->
												<!-- <label>Attach copy of the purchase order</label> -->
												<!-- <input type="file" name="email" class="form-email form-element rounded medium" placeholder="Asset to be purchased" tabindex="3" required> -->
											<!-- </div> -->
										<!-- </div> -->
									</div>
									
									<div class="row">
										<div class="column width-3">
											<div class="form-select form-element rounded medium">
												<select name="currency" tabindex="6" class="form-aux" data-label="currency">
													<option value="SGD">SGD</option>
													<option value="IDR">IDR</option>
												</select>
											</div>
										</div>
										
										<div class="column width-5">
											<div class="field-wrapper">
												<input type="number" name="revenue" class="form-fname form-element rounded medium" placeholder="Annual Revenue" tabindex="1" value="" required>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="column width-8">
											<div class="form-select form-element rounded medium">
												<select name="financing_type" tabindex="6" class="form-aux" data-label="financing_type">
													<option value="">What financing solution are you seeking?</option>
													<option value="Invoice Financing">Invoice Financing</option>
													<option value="Asset Purchase Financing">Asset Purchase Financing</option>
												</select>
											</div>
										</div>
										<div class="column width-8">
											<div class="form-select form-element rounded medium">
												<select name="financing_period" tabindex="6" class="form-aux" data-label="financing_period">
													<option value="">Financing period</option>
													<option value="2">2 months</option>
													<option value="3">3 months</option>
													<option value="4">4 months</option>
													<option value="5">5 months</option>
												</select>
											</div>
										</div>
									</div>
									
									<div class="row">
										<div class="column width-8">	
											<div class="field-wrapper">
												<div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="column width-12">
											<input type="submit" value="Submit" class="form-submit button rounded medium bkg-theme bkg-hover-theme color-white color-hover-white">
										</div>
									</div>
								</form>
								
							</div>
						</div>
						<div class="column width-6">
							<h2 class="mb-50">Get Funded</h2>
							<h3 class="mb-50">Eligibility criteria:</h3>
							<p><ul>
								<li>Incorporated in Indonesia or Singapore</li>
								<li>Been in operations for more than 1 year</li>
								<li>Annual sales of more than IDR1bn or SGD100,000 </li>
								<li>Positive free cash flow in the past 12 months </li>
							</ul></p>
							<h3 class="mb-50">Suitable for:</h3>
							<p><ul>
								<li>Purchasing assets (raw materials, equipments, supplies etc) based on existing purchase/work order or </li>
								<li>Financing invoices issued to established companies such as MNCs, public-listed companies, or state-owned enterprises </li>
								<li>Short-term financing of less than six months </li>

							</ul></p>
							<p>Not clear on our funding requirements? Speak to us at +65 6342 5473/+62 812 8787 4136 or email hello@kapitalboost.com. </p>
						</div>
					</div>
				</div>
				<!-- Service Section End -->

				<!-- Faq Section End -->

				<!-- Call to Action Section -->
				
				<!-- Call to Action Section End -->

			</div>
			<!-- Content End -->

			<!-- Footer -->
			<footer class="footer footer-light with-border">
				<div class="footer-top">
					
				</div>
				<div class="footer-bottom">
					<div class="row">
						<div class="column width-6 offset-3">
							<div class="widget center left-on-mobile">
								<p class="mb-0">&copy; KapitalBoost 2018. All Rights Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- Footer End -->

		</div>
	</div>

	<!-- Js -->
	<script src="https://kapitalboost.com/staging/valuepenguin/js/jquery-3.2.1.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3JCAhNj6tVAO_LSb8M-AzMlidiT-RPAs"></script>
	<script src="https://kapitalboost.com/staging/valuepenguin/js/timber.master.min.js"></script>
</body>
</html>