<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">

	<!-- Open Graph -->
	<meta property='fb:app_id' content="1419559388071407"/>
	<meta property='og:type' content="website" />
	<meta property='og:image' content="https://kapitalboost.com/assets/images/Slide1.jpg" />
	<meta property='og:title' content="Kapital Boost: Islamic crowdfunding - Business Finance" />
	<meta property='og:site_name' content="Kapital Boost Crowdfunding" />
	<meta property='og:description' content="Kapital Boost is Asia’s first Islamic P2P crowdfunding platform for SMEs. Learn more about getting funding or investing ethically with us today." />
	<meta property='og:url' content="https://kapitalboost.com" />
	<meta property='og:image:width' content="336" />
	<meta property='og:image:height' content="201" />
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- Twitter Theme -->
	<meta name="twitter:widgets:theme" content="light">
	
	<!-- Title &amp; Favicon -->
	<title>Kapital Boost: Islamic crowdfunding - Business Finance</title>
	<link rel="shortcut icon" type="image/x-icon" href="https://kapitalboost.com/assets/images/kp-fav.png">

	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7CHind+Madurai:400,500&amp;subset=latin-ext" rel="stylesheet">
	
	<!-- Css -->
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/core.min.css" />
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/skin.css" />
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/investor.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

	<!--[if lt IE 9]>
    	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="shop home-page">

	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">

			<!-- Header -->
			<header class="header header-absolute header-transparent header-fixed-on-mobile" data-helper-in-threshold="200" data-helper-out-threshold="500" data-sticky-threshold="200" data-bkg-threshold="100">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
							<div class="logo">
								<div class="logo-inner">
									<a href="https://kapitalboost.com/vp/investor"><img src="https://kapitalboost.com/staging/valuepenguin/images/logo-full.png" alt="Kapital Boost Logo" /></a>
									<a href="https://kapitalboost.com/vp/investor"><img src="https://kapitalboost.com/staging/valuepenguin/images/full-logo-white-kapital.png" alt="Kapital Boost Logo" /></a>
								</div>
							</div>
							<!-- <nav class="navigation nav-block primary-navigation nav-left sub-menu-indicator"> -->
								<!-- <ul> -->
									<!-- <li class="current"> -->
										<!-- <a href="#home" class="scroll-link">Home</a> -->
									<!-- </li> -->
								<!-- </ul> -->
							<!-- </nav> -->
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->

			<!-- Content -->
			<div class="content clearfix">

				<!-- Intro Section -->
				<div id="home" class="section-block head-bg window-height clear-height-on-tablet">
					<div class="media-overlay bkg-black opacity-02"></div>
					<div class="row flex v-align-middle">
						<div class="column width-12">
							<div class="row flex one-column-on-tablet">
								<div class="column width-6 v-align-middle">
									<div>
										<h1 class="color-white">Get returns of up to 24% p.a.</h1>
										<p class="lead color-white">Kapital Boost offers a peer-to-peer crowdfunding platform for SMEs to raise financing from retail investors. We use Islamic finance structures to ensure ethical investments and lower credit risks for investors. Sign up now to find out more.</p>
										<!-- <a href="#about" class="scroll-link button rounded medium border-white bkg-hover-green color-white color-hover-white left mb-80"> -->
											<!-- Discover Our Product -->
										<!-- </a> -->
									</div>
								</div>
								<div class="column width-6">
									<div class="signup-box box rounded xlarge mb-0 bkg-white shadow">
										<h3>Register</h3>
										<p class="mb-10">Already registered? <a href="https://kapitalboost.com" class="fade-location">Click Here</a></p>
										<div class="register-form-container">
											<form class="register-form" action="https://kapitalboost.com/index.php?class=Member&method=RegisterForm" method="post">
												<div class="row merged-form-elements">
													<div class="column width-6">
														<div class="field-wrapper">
															<label class="color-charcoal">First Name:</label>
															<input type="text" name="member_firstname" class="form-firstname form-element rounded medium" placeholder="" value="" required>
														</div>
													</div>
													<div class="column width-6">
														<div class="field-wrapper">
															<label class="color-charcoal">Last Name:</label>
															<input type="text" name="member_surname" class="form-lastname form-element rounded medium" placeholder="" value="" required>
														</div>
													</div>
												</div>
												<div class="row merged-form-elements">
													<div class="column width-6">
														<div class="field-wrapper">
															<label class="color-charcoal">Email Address:</label>
															<input type="text" name="member_email" class="form-firstname form-element rounded medium" placeholder="" value="" required>
														</div>
													</div>
													<div class="column width-6">
														<div class="field-wrapper">
															<label class="color-charcoal">Username:</label>
															<input type="text" name="member_username" class="form-lastname form-element rounded medium" placeholder="" value="" required>
														</div>
													</div>
												</div>
												<!-- <div class="row"> -->
													<!-- <div class="column width-12"> -->
														<!-- <div class="field-wrapper"> -->
															<!-- <label class="color-charcoal">Email:</label> -->
															<!-- <input type="email" name="register[email]" class="form-email form-element rounded medium" placeholder="johndoe@gmail.com" required> -->
														<!-- </div> -->
													<!-- </div> -->
												<!-- </div> -->
												<div class="row merged-form-elements">
													<div class="column width-6">
														<div class="field-wrapper">
															<label class="color-charcoal">Password:</label>
															<input type="password" name="member_password" class="form-firstname form-element rounded medium" placeholder="" value="" required>
														</div>
													</div>
													<div class="column width-6">
														<div class="field-wrapper">
															<label class="color-charcoal">Confirm Password:</label>
															<input type="password" name="retype_password" class="form-lastname form-element rounded medium" placeholder="" value="" required>
															<input type="hidden" name="vpn" value="valuepgn" >
														</div>
													</div>
												</div>
												<div class="row merged-form-elements">
													<div class="column width-12">	
														<div class="field-wrapper">
															<div class="g-recaptcha form-element rounded medium" data-sitekey="6LeKSlAUAAAAAI41SwPXe3VrMGR16w6QCiA7tXFK"></div>
														</div>
													</div>
												</div>
												<div class="row merged-form-elements">
													
													<div class="column width-12 mt-10 center">
														<input type="submit" value="Create Account" class="form-submit button rounded medium bkg-blue bkg-hover-theme bkg-focus-green color-white color-hover-white no-margins">
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Intro Section End -->

				<div class="section-block replicable-content">
					<div class="row">
						<div class="column width-10 offset-1 center">
							<h3 class="mb-50">Why Invest via Kapital Boost?</h3>
						</div>
					</div>
					<div class="row flex boxes">
						<div class="column width-4">
							<div class="feature-column box rounded large bkg-white center horizon" data-animate-in="preset:flipInY;duration:1000ms;" data-threshold="1">
								<i class="fas fa-bullseye feature-icon color-gradient-purple-haze"></i>
								<div class="feature-text">
									<h4>Transparency. No hidden fees/charges</h4>
									<p>We focus on transparency. Fees are borne by the SMEs. Investing is free of charge</p>
								</div>
							</div>
						</div>
						<div class="column width-4">
							<div class="feature-column box rounded large bkg-white center horizon" data-animate-in="preset:flipInY;duration:1000ms;delay:200ms;" data-threshold="1">
								<i class="fas fa-donate feature-icon color-gradient-purple-haze"></i>
								<div class="feature-text">
									<h4>Short-term investments with attractive returns</h4>
									<p>Invest from 2-5 months on SME projects offering annualised returns from 16-24%</p>
								</div>
							</div>
						</div>
						<div class="column width-4">
							<div class="feature-column box rounded large bkg-white center horizon" data-animate-in="preset:flipInY;duration:1000ms;delay:400ms;" data-threshold="1">
								<i class="fas fa-check-circle feature-icon color-gradient-purple-haze"></i>
								<div class="feature-text">
									<h4>Thorough screening and due diligence proces</h4>
									<p>All investments are carefully screened to ensure only deserving SME projects are funded</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<!-- Hero 5 Section -->
				<div class="section-block hero-5 hero-5-2 clear-height show-media-column-on-mobile bkg-ash">
					<div class="media-column width-6">
						<div class="media-overlay bkg-black opacity-05"></div>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/PcIBqRsKRlc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					</div>
					<div class="row">
						<div class="column width-5 push-7">
							<div class="hero-content split-hero-content">
								<div class="hero-content-inner left horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;delay:200ms;" data-threshold="0.5">
									<p class="lead color-white font35">What We Do</p>
									<p class="color-grey-light">Kapital Boost aims to tackle the lack of financing available to SMEs in Southeast Asia. We help level the playing field by offering these businesses a crowdfunding platform to access temporary liquidity for goods and capital purchases.</p>
									<p class="color-grey-light">We also address the shortage of attractive Islamic-based investments. By funding SMEs, Kapital Boost members (funders) have the opportunity to earn attractive returns on short-term, Shariah-structured deals. In other words, our members get rewarded for doing good.  </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Hero 5 Section End -->

				<!-- Logo Slider -->
				<div class="section-block pt-80 pb-80 bkg-white">
					<div class="row">
						<div class="column width-12 center">
							<h3 class="mb-50">Featured On</h3>
							<br />
						</div>
					</div>
					<div class="row">
						<div class="column width-12">
							<div class="tm-slider-container logo-slider pb-30" data-nav-arrows="false" data-nav-show-on-hover="false" data-nav-dark data-nav-keyboard="false" data-auto-advance data-auto-advance-interval="4000" data-progress-bar="false" data-pause-on-hover="false" data-carousel-visible-slides="5">
								<ul class="tms-slides">	
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/bloomberg1.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/salam.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/CrowdfundLogo.jpg" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/techinasia.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/straits.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/markets.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/newbfm.png" src="images/blank.png" alt=""/>
										</div>
									</li>
									<li class="tms-slide">
										<div class="tms-content-scalable">
											<img data-src="https://kapitalboost.com/assets/images/bh.png" src="images/blank.png" alt=""/>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<!-- Logo Slider End -->

				<!-- Custom Call to Action Section -->
				<div class="section-block pt-60 pb-60 bkg-grey-ultralight">
					<div class="row flex two-columns-on-tablet">
						<div class="column width-5 offset-1 v-align-middle horizon" data-animate-in="preset:slideInLeftShort;duration:1000ms;delay:0;" data-threshold="1">
							<p class="lead">Ready to invest, sign up today!</p>
						</div>
						<div class="column width-5 center v-align-middle horizon" data-animate-in="preset:slideInRightShort;duration:1000ms;delay:300;" data-threshold="1">
							<div>
								<a href="#home" class="scroll-link button rounded medium full-width bkg-blue color-white bkg-hover-green color-hover-white no-marginsk">Create an Account</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Custom Call to Action Section End -->

			</div>
			<!-- Content End -->

			<!-- Footer -->
			<footer class="footer footer-light with-border">
				<div class="footer-top">
					
				</div>
				<div class="footer-bottom">
					<div class="row">
						<div class="column width-6 offset-3">
							<div class="widget center left-on-mobile">
								<p class="mb-0">&copy; KapitalBoost 2018. All Rights Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- Footer End -->

		</div>
	</div>

	<!-- Js -->
	<script src="https://kapitalboost.com/staging/valuepenguin/js/jquery-3.2.1.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3JCAhNj6tVAO_LSb8M-AzMlidiT-RPAs"></script>
	<script src="https://kapitalboost.com/staging/valuepenguin/js/timber.master.min.js"></script>
</body>
</html>