<?php
class mysql {

  var $dbhost = null;
  var $dbuser = null;
  var $dbpass = null;
  var $dbname = null;
  var $conn = null;
  var $result = null;

  function __construct() {
    $mysqlfile = parse_ini_file('../../.rDir/.MySQL.ini');
    $this->dbhost = $mysqlfile['dbhost'];
    $this->dbuser = $mysqlfile['dbuser'];
    $this->dbpass = $mysqlfile['dbpass'];
    $this->dbname = $mysqlfile['dbname'];
  }

  public function Connection() {
    $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
    if (mysqli_connect_error()) {
      return false;
    }
    mysqli_set_charset($this->conn, 'utf8');
    return true;
  }

  // Function connect to sql

  public function CloseConnection() {
    if ($this->conn != null) {
      mysqli_close($this->conn);
    }
  }

public function VpInsert($fname, $email, $mobile, $company, $year, $currency, $revenue, $financing_type, $financing_period) {
	$stmt = $this->conn->prepare("INSERT INTO tborrower (fullname, email, mobile, company, year_established, currency, revenue, financing_type, period, created) VALUES ('$fname', '$email', '$mobile', '$company', '$year', '$currency', '$revenue', '$financing_type', '$financing_period', NOW())");
	$stmt->execute();
	$mId = mysqli_insert_id($this->conn);
	return $mId;
}

}
?>
