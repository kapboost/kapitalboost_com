<!DOCTYPE html>
<html lang="zxx">
<head>
	<meta charset="UTF-8" />
	<meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0" name="viewport">

	<!-- Open Graph -->
	<meta property='fb:app_id' content="1419559388071407"/>
	<meta property='og:type' content="website" />
	<meta property='og:image' content="https://kapitalboost.com/assets/images/Slide1.jpg" />
	<meta property='og:title' content="Kapital Boost: Islamic crowdfunding - Business Finance" />
	<meta property='og:site_name' content="Kapital Boost Crowdfunding" />
	<meta property='og:description' content="Kapital Boost is Asia’s first Islamic P2P crowdfunding platform for SMEs. Learn more about getting funding or investing ethically with us today." />
	<meta property='og:url' content="https://kapitalboost.com" />
	<meta property='og:image:width' content="336" />
	<meta property='og:image:height' content="201" />
	<script src='https://www.google.com/recaptcha/api.js'></script>

	<!-- Twitter Theme -->
	<meta name="twitter:widgets:theme" content="light">
	
	<!-- Title &amp; Favicon -->
	<title>Kapital Boost: Islamic crowdfunding - Business Finance</title>
	<link rel="shortcut icon" type="image/x-icon" href="https://kapitalboost.com/assets/images/kp-fav.png">

	<!-- Font -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700%7CHind+Madurai:400,500&amp;subset=latin-ext" rel="stylesheet">
	
	<!-- Css -->
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/core.min.css" />
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/skin.css" />
	<link rel="stylesheet" href="https://kapitalboost.com/staging/valuepenguin/css/kap.css" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous">

	<!--[if lt IE 9]>
    	<script type="text/javascript" src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body class="shop checkout">

	

	<div class="wrapper reveal-side-navigation">
		<div class="wrapper-inner">

			<!-- Header -->
			<header class="header header-relative header-fixed-on-mobile nav-dark" data-bkg-threshold="100" data-sticky-threshold="0">
				<div class="header-inner">
					<div class="row nav-bar">
						<div class="column width-12 nav-bar-inner">
							<div class="logo">
								<div class="logo-inner">
									<a href="index.html"><img src="https://kapitalboost.com/staging/valuepenguin/images/logo-full.png" alt="Kapital Boost Logo" /></a>
									<a href="index.html"><img src="https://kapitalboost.com/staging/valuepenguin/images/full-logo-white-kapital.png" alt="Kapital Boost Logo" /></a>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</header>
			<!-- Header End -->

			<!-- Content -->
			<div class="content clearfix">

				<!-- Intro Title Section 1 -->
				<div class="section-block intro-title-1 small">
					<div class="row">
						<div class="column width-6 offset-3">
							<div class="title-container">
								<div class="title-container-inner center left-on-mobile">
									<br />
									<h1 class="mb-0">You have successfully submitted a funding request. </h1>
									<p class="lead mb-0 mb-mobile-20">Our team is currently reviewing it and will be in touch with you within the next working day.</p>
									<br />
									<br />
									<p><a href="https://kapitalboost.com" class="button medium rounded bkg-theme bkg-hover-theme color-white color-hover-white">Back to Home</a></p>
									<br />
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Intro Title Section 1 End -->

				
			</div>
			<!-- Content End -->

			<!-- Footer -->
			<footer class="footer footer-light with-border">
				<div class="footer-top">
					
				</div>
				<div class="footer-bottom">
					<div class="row">
						<div class="column width-6 offset-3">
							<div class="widget center left-on-mobile">
								<p class="mb-0">&copy; KapitalBoost 2018. All Rights Reserved.</p>
							</div>
						</div>
					</div>
				</div>
			</footer>
			<!-- Footer End -->

		</div>
	</div>

	<!-- Js -->
	<script src="https://kapitalboost.com/staging/valuepenguin/js/jquery-3.2.1.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3JCAhNj6tVAO_LSb8M-AzMlidiT-RPAs"></script>
	<script src="https://kapitalboost.com/staging/valuepenguin/js/timber.master.min.js"></script>
</body>
</html>