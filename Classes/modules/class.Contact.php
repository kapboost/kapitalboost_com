<?php

include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

class Contact extends Front {

	function SendContactUs($from, $to, $member_name, $subject, $message, $html = FALSE) {
		require(DOC_ROOT . 'Classes/sendgrid-php/sendgrid-php.php');
		$subject = $subject;
		$to = new SendGrid\Email($to, $to);
		$content_email = $message;
		$content = new SendGrid\Content("text/html", $content_email);
		$from = new SendGrid\Email($member_name, $from);
		$mail = new SendGrid\Mail($from, $subject, $to, $content);
		$apiKey = "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";
		$sg = new \SendGrid($apiKey);
		$response = $sg->client->mail()->send()->post($mail); 
	}

	function SendContact() {
		// echo "'<script>console.log(\"masuk contact\")</script>'";
		//verify recaptcha
		$response = $_POST["g-recaptcha-response"];
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret' => '6LeKSlAUAAAAAChAjmtKLimNg65KcFs-UhByJdot',
			'response' => $_POST["g-recaptcha-response"]
		);
		$options = array(
			'http' => array (
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context); 
		$captcha_success=json_decode($verify);
		// ----------------------------------
		
		// echo "'<script>console.log(\"dope : ".$captcha_success->success."\")</script>'";
		if ($captcha_success->success==false) {
			echo "<script>alert(\"Wrong Captcha\");</script>";
			echo "<script>window.history.back();</script>";
			
		} else if ($captcha_success->success==true) {
		
			$member_name = $_POST['nama_lengkap'];
			$email = $_POST['email'];
			$pesan = $_POST['pesan']; // Message
			$tanggal = date("Y-m-d h:i:s");

			if (empty($email) || empty($pesan) || empty($member_name)) {
				$error = "Fields not filled";
			} else {
				$error = "";
				$sukses = "Thank you for getting in touch!";
				$simpan = "INSERT INTO contact_us(nama, email,pesan,date) VALUES('$member_name', '$email','$pesan', '$tanggal')";
				$query = $this->db->exec($simpan);

				$subject = "Thank You for Contacting Us";
				$message = "
					
				<html>
					<head>
						<title>Kapital Boost's Enquiry</title>
					</head>
					<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
						<div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
							<div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
								<img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
								<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
									Hi " . $member_name . ",
								</p>
								<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
									Thank you for your inquiry!
								</p>
								<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
									Our team is looking into it and will reply to you shortly. If your enquiry is urgent, you may call us at +65 9865 9648 from Monday to Friday during our office hours.
								</p>

								<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
									Talk to you soon,<br><br>Kapital Boost
								</p>
							</div>
							<div style='width:100%;height:7px;background-color:#00b0fd'></div>
							<div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
								<p style='font-size: 12px;color: white;text-align:center'>
									&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
								</p>
							</div>
						</div>
					</body>
				</html>


				";


				$this->SendEmailSendGridOld(EMAIL_FROM, $email, "", $subject, $message, TRUE);
				
				$this->AddTrail("Send Contact Web", $this->member_id);
				if (strpos($email, 'yahoo') !== false) {
					$email = EMAIL_FROM;
				}
				$subject2 = "[INFO] From Contact Us";
				$message2 = "                                Contact Us Form<br /><br />
											Name       : {$member_name}<br />
											Email      : {$email}<br />
											Messages   : {$pesan}<br />
											<br />";

				$this->SendContactUs($email,"arief@kapitalboost.com", "$member_name", $subject2, $message2, TRUE);
			}

			$this->smarty->assign("sukses", $sukses);
			$this->smarty->assign("error", $error);
			$content = $this->smarty->fetch('TPL_Contact_Us.tpl');
			return $content;
		}
	}

}

?>
