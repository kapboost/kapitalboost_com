<?php

class StaticPage extends Front {

       function LoadContact() {
              $success = "";

              if ($_POST) {
                     // include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

                     // $feedback = nl2br($_POST['feedback']);


                     // $subject = "[INFO] Inquiry from templeproject.com";
                     // $message = "
                                                    // Name : {$_POST[name]}<br />
                                                    // Email : {$_POST[email]}<br />
                                                    // Phone : {$_POST[phone]}<br />
                                                    // Order No. : {$_POST[order_no]}<br /><br />
                                                    // Feedback<br />
                                                    // ------------------------------------------<br />
                                                    // {$feedback}<br />
                                                    // <br />
                                                    // <br />
                                                    // This is a computer-generated email, please do NOT reply to this email.<br />
                                                    // <hr /><br /><br />";

                     // $this->SendEmailSendGridOld(EMAIL_FROM, "jcsinaga@gmail.com", array('jcsinaga@gmail.com'), $subject, $message, TRUE);

                     // $success = "Thank you for contact us, we will process your inquiry as soon as possible.";
              }


              //Set Breadcrumb
              $this->AddBreadCrumb('Contact Us');
              $this->smarty->assign('breadcrumb', $this->LoadBreadCrumb());
              $this->smarty->assign('success', $success);

              list($contactus, ) = $this->db->singlearray("select * from tgeneral_page where page_id='12'");

              $contentleft = $contactus['page_content'];

              $this->smarty->assign('contentleft', $contentleft);

              $this->_setMeta('Contact for Crowdfunding Site | Kapital Boost', 'Contact Us', 'Contact and Address for Kapital Boost Pte Ltd, the Singapore based Shariah compliant crowdfunding platform.  Visit our website for details, for enquiries call +65 9865 9648', '', '');

			  $this->AddTrail("Visit Contact StaticPage Web", $this->member_id);

              $content = $this->smarty->fetch('TPL_Contact_Us.tpl');
              return $content;
       }

       function LoadAbout() {
              list($about, ) = $this->db->singlearray("select * from tgeneral_page where page_id='7'");
              list($team, ) = $this->db->singlearray("select * from tgeneral_page where page_id='8'");
              $pageTop = $about['page_top'];

              $content = $about['page_content'] . $team['page_content'];

              $this->smarty->assign('pagetop', $pageTop);
              $this->smarty->assign('content', $content);

              $this->_setMeta('Islamic Financing | Kapital Boost', 'About Us', 'Learn more about Kapital Boost, the Shariah compliant crowdfunding platform.  Visit our website for details, for enquiries call +65 9865 9648', '', '');
              $content = $this->smarty->fetch('TPL_StaticPage.tpl');
              return $content;
       }

       function LoadAboutUs() {
              list($about, ) = $this->db->singlearray("select * from tgeneral_page where page_id='14'");
              list($team, ) = $this->db->singlearray("select * from tgeneral_page where page_id='8'");
              $pageTop = $about['page_top'];
              //print_r($pageTop);

              $content = $about['page_content'] . $team['page_content'];
              $theTeam = $team['page_content'];
              if ($this->lang == 'id') {
                     $aboutHeader = $about["page_header_id"];
                     $aboutContent = $about['page_content_id'];
              } else {
                     $aboutHeader = $about["page_header"];
                     $aboutContent = $about['page_content'];
              }
              $aboutHeaderA = explode("~~~", $aboutHeader);
              $aboutContentA = explode("~~~", $aboutContent);

              //$this->smarty->assign('content', $content);
              $aboutA = [];
              for ($i = 0; $i < count($aboutContentA); $i++) {
                     $aboutA[$i]["header"] = $aboutHeaderA[$i];
                     $aboutA[$i]['content'] = $aboutContentA[$i];
              }

			  $this->AddTrail("Visit About Us Web", $this->member_id);

              $this->smarty->assign('pagetop', $pageTop);
              $this->smarty->assign('aboutA', $aboutA);
              $this->smarty->assign('theTeam', $theTeam);

              $this->_setMeta('Islamic Financing | Kapital Boost', 'About Us', 'Learn more about Kapital Boost, the Shariah compliant crowdfunding platform.  Visit our website for details, for enquiries call +65 9865 9648', '', '');
              $content = $this->smarty->fetch('aboutus.tpl');
              return $content;
       }

       function LoadHowItWorks() {
              list($howitworks, ) = $this->db->singlearray("select * from tgeneral_page where page_id='9'");
              if ($this->lang == 'id') {
                     $content = $howitworks['page_content_id'];
              } else {
                     $content = $howitworks['page_content'];
              }
              $this->smarty->assign('content', $content);

			  $this->AddTrail("Visit How It works Web", $this->member_id);

              $this->_setMeta('Crowdfunding Platform | Kapital Boost', 'How it works', 'Learn more about Islamic Crowdfunding Platform by Kapital Boost.  Get started in 3 simple steps.  Visit our website for details, for enquiries call +65 9865 9648', '', '');
              $content = $this->smarty->fetch('TPL_StaticPage.tpl');
              return $content;
       }

       function LoadCareers() {
              list($careers, ) = $this->db->singlearray("select * from tgeneral_page where page_id='17'");
			  $pageTop = $careers['page_top'];
              $content = $careers['page_content'];
			  // echo "'<script>alert(\"content : $content\")</script>'";
			  $this->smarty->assign('pagetop', $pageTop);
              $this->smarty->assign('content', $content);
              $this->smarty->assign('pg_title', 'Career - Kapital Boost');
              $this->smarty->assign('meta_descr', 'Join our journey to become the largest P2P Islamic financing platform!');
              $this->smarty->assign('meta_keyword', 'Career, Islamic Finance, Job Vacancy, Fintech, Hiring, Startup');
              $this->smarty->assign('og_url', 'https://kapitalboost.com/career');

			  $this->AddTrail("Visit Careers Web", $this->member_id);

              $this->_setMeta('Crowdfunding Platform | Kapital Boost', 'Careers', 'Lets join us in Kapital Boost Islamic Crowdfunding', '', '');

              $content = $this->smarty->fetch('TPL_StaticPage.tpl');
              return $content;
       }

       function LoadLegal() {
              list($legal, ) = $this->db->singlearray("select * from tgeneral_page where page_id='10'");

              $pageTop = $legal['page_top'];
              $content = $legal['page_content'];

              $this->smarty->assign('pagetop', $pageTop);
              $this->smarty->assign('content', $content);

              $this->_setMeta('Legal for Crowdfunding Site | Legal', 'Legal', 'Terms of use and privacy policy for Kapital Boost Islamic Crowdfunding', '', '');
              $content = $this->smarty->fetch('TPL_StaticPage.tpl');
              return $content;
       }

       function LoadLegals() {
              list($legal, ) = $this->db->singlearray("select * from tgeneral_page where page_id='16'");
              $pageTop = $legal['page_top'];
              if ($this->lang == 'id') {
                     $legalContent = $legal['page_content_id'];
              } else {
                     $legalContent = $legal['page_content'];
              }
              $legalContentA = explode("~~~^_^~~~", $legalContent);

              $this->smarty->assign('pagetop', $pageTop);
              $this->smarty->assign('legalContent', $legalContentA);

			  $this->AddTrail("Visit Legal Web", $this->member_id);

              $this->_setMeta('Legal for Crowdfunding Site | Legal', 'Legal', 'Terms of use and privacy policy for Kapital Boost Islamic Crowdfunding', '', '');
              $content = $this->smarty->fetch('legal.tpl');
              return $content;
       }

       function LoadFAQs() {
              list($faq, ) = $this->db->singlearray("select * from tgeneral_page where page_id='15'");
              $pageTop = $faq['page_top'];
              if ($this->lang == 'id') {
                     $faqHeader = $faq["page_header_id"];
                     $faqContent = $faq["page_content_id"];
              } else {
                     $faqHeader = $faq['page_header'];
                     $faqContent = $faq['page_content'];
              }

              $faqHeaderA = explode("+```+", $faqHeader);
              $faqSmeHeaderA = explode("~~~", $faqHeaderA[0]);
              $faqMemberHeaderA = explode("~~~", $faqHeaderA[1]);
              $faqDonationHeaderA = explode("~~~", $faqHeaderA[2]);
              $faqPrivateHeaderA = explode("~~~", $faqHeaderA[3]);
              $faqXfersHeaderA = explode("~~~", $faqHeaderA[4]);




              array_pop($faqSmeHeaderA);
              array_pop($faqMemberHeaderA);
              array_pop($faqDonationHeaderA);
              array_pop($faqPrivateHeaderA);
              array_pop($faqXfersHeaderA);

              $faqContentA = explode("+```+", $faqContent);
              $faqSmeContentA = explode("~~~", $faqContentA[0]);
              $faqMemberContentA = explode("~~~", $faqContentA[1]);
              $faqDonationContentA = explode("~~~", $faqContentA[2]);
              $faqPrivateContentA = explode("~~~", $faqContentA[3]);
              $faqXfersContentA = explode("~~~", $faqContentA[4]);

              $this->smarty->assign('pagetop', $pageTop);

              $this->smarty->assign('faqSmeHeaderA', $faqSmeHeaderA);
              $this->smarty->assign('faqMemberHeaderA', $faqMemberHeaderA);
              $this->smarty->assign('faqDonationHeaderA', $faqDonationHeaderA);
              $this->smarty->assign('faqPrivateHeaderA', $faqPrivateHeaderA);
              $this->smarty->assign('faqXfersHeaderA', $faqXfersHeaderA);

              $this->smarty->assign('faqSmeContentA', $faqSmeContentA);
              $this->smarty->assign('faqMemberContentA', $faqMemberContentA);
              $this->smarty->assign('faqDonationContentA', $faqDonationContentA);
              $this->smarty->assign('faqPrivateContentA', $faqPrivateContentA);
              $this->smarty->assign('faqXfersContentA', $faqXfersContentA);

			  $this->AddTrail("Visit FAQs Web", $this->member_id);

              $this->_setMeta(' Crowdfunding Questions & Answers | Kapital Boost', 'FAQ', 'Know more about Kapital Boost Shariah compliant Crowdfunding Platform.  Visit our website for details, for enquiries call +65 9865 9648', '', '');
              $content = $this->smarty->fetch('faqs.tpl');
              return $content;
       }

}

?>
