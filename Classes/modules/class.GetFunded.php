<?php

include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

class GetFunded extends Front {

       function LoadList() {
              list($content, ) = $this->db->multiarray("SELECT *,DATEDIFF(expiry_date,NOW()) AS days_left,DATEDIFF(expiry_date,release_date) AS duration
 FROM tgetfunded WHERE release_date <= NOW() AND  expiry_date >= NOW()");

              list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");

              foreach ($country as $row) {
                     $member_country_options[$row['name']] = $row['name'];
              }
              $member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
              $member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
              $member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
              $member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
              $member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
              $this->smarty->assign("member_id", $this->member_id);
              $this->smarty->assign("member_name", $member_name);
              $this->smarty->assign("member_surname", $member_surname);
              $this->smarty->assign("member_firstname", $member_firstname);
              $this->smarty->assign("member_country", $member_country);
              $this->smarty->assign("member_email", $member_email);
              $this->smarty->assign("member_country_options", $member_country_options);
              $this->smarty->assign("content", $content);
              $this->_setMeta('SME Financing | Kapital Boost', 'Get Funded', 'Find out more on Islamic Financing Options for SMEs in South-East Asia. Visit our website for details, for enquiries call +65 9865 9648', '', '');

			  $this->AddTrail("Visit Get Funded Web", $this->member_id);

              $content = $this->smarty->fetch('TPL_Get_Funded.tpl');
              return $content;
       }

       function LoadListNew() {
              list($content, ) = $this->db->multiarray("SELECT *,DATEDIFF(expiry_date,NOW()) AS days_left,DATEDIFF(expiry_date,release_date) AS duration
 FROM tgetfunded WHERE release_date <= NOW() AND  expiry_date >= NOW()");

              list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");

              foreach ($country as $row) {
                     $member_country_options[$row['name']] = $row['name'];
              }
              $member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
              $member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
              $member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
              $member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
              $member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
              $this->smarty->assign("member_id", $this->member_id);
              $this->smarty->assign("member_name", $member_name);
              $this->smarty->assign("member_surname", $member_surname);
              $this->smarty->assign("member_firstname", $member_firstname);
              $this->smarty->assign("member_country", $member_country);
              $this->smarty->assign("member_email", $member_email);
              $this->smarty->assign("member_country_options", $member_country_options);
              $this->smarty->assign("content", $content);
              $this->_setMeta('SME Financing | Kapital Boost', 'Get Funded', 'Find out more on Islamic Financing Options for SMEs in South-East Asia. Visit our website for details, for enquiries call +65 9865 9648', '', '');

			  $this->AddTrail("Visit Get Funded Web", $this->member_id);

              $content = $this->smarty->fetch('TPL_Get_Funded_New.tpl');
              return $content;
       }
       function LoadListIndo() {
              list($content, ) = $this->db->multiarray("SELECT *,DATEDIFF(expiry_date,NOW()) AS days_left,DATEDIFF(expiry_date,release_date) AS duration
 FROM tgetfunded WHERE release_date <= NOW() AND  expiry_date >= NOW()");

              list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");

              foreach ($country as $row) {
                     $member_country_options[$row['name']] = $row['name'];
              }
              $member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
              $member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
              $member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
              $member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
              $member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
              $this->smarty->assign("member_id", $this->member_id);
              $this->smarty->assign("member_name", $member_name);
              $this->smarty->assign("member_surname", $member_surname);
              $this->smarty->assign("member_firstname", $member_firstname);
              $this->smarty->assign("member_country", $member_country);
              $this->smarty->assign("member_email", $member_email);
              $this->smarty->assign("member_country_options", $member_country_options);
              $this->smarty->assign("content", $content);
              $this->_setMeta('SME Financing | Kapital Boost', 'Get Funded', 'Find out more on Islamic Financing Options for SMEs in South-East Asia. Visit our website for details, for enquiries call +65 9865 9648', '', '');

			  $this->AddTrail("Visit Get Funded Web", $this->member_id);

              $content = $this->smarty->fetch('TPL_Get_Funded_Indo.tpl');
              return $content;
       }

       function RegisterForm() {
              $error_msg = array();
              $success_msg = "";

			//verify recaptcha
			$response = $_POST["g-recaptcha-response"];
			$url = 'https://www.google.com/recaptcha/api/siteverify';
			$data = array(
				'secret' => '6LeKSlAUAAAAAChAjmtKLimNg65KcFs-UhByJdot',
				'response' => $_POST["g-recaptcha-response"]
			);
			$options = array(
				'http' => array (
					'method' => 'POST',
					'content' => http_build_query($data)
				)
			);
			$context  = stream_context_create($options);
			$verify = file_get_contents($url, false, $context);
			$captcha_success=json_decode($verify);
			// ----------------------------------

			if ($captcha_success->success==false) {
			echo "<script>alert(\"Wrong Captcha\");</script>";
			echo "<script>window.history.back();</script>";

			} else if ($captcha_success->success==true) {

              if ($_POST) {

                     if ($_POST['email'] == "") {
                            $error_msg = "Email cannot be empty.";
                     }

                     if ($_POST['mobile'] == '') {
                            $error_msg = "Mobile number cannot be blank";
                     }
                     // if ($_POST['country'] == '') {
                            // $error_msg = "Country cannot be blank";
                     // }
                     if ($_POST['company'] == '') {
                            $error_msg = "Company cannot be blank";
                     }
                     // if ($_POST['industri'] == '') {
                            // $error_msg = "Industry cannot be blank";
                     // }
                     if ($_POST['year'] == '') {
                            $error_msg = "Year cannot be blank";
                     }
                     if ($_POST['currency'] == '0') {
                            $error_msg = "You haven't select currency";
                     }
                     if ($_POST['period'] == '0') {
                            $error_msg = "You haven't select funding period";
                     }
                     // if ($_POST['currency'] == '') {
                            // $error_msg = "Currency cannot be blank";
                     // }

                     if ($_POST['est'] == '') {
                            $error_msg = "Estimate Annual Revenue cannot be blank";
                     }
                     if ($_POST['fund'] == '') {
                            $error_msg = "Funding Amount Req cannot be blank";
                     }
                     // if ($_POST['assets'] == '') {
                            // $error_msg = "Assets to be purchased cannot be blank";
                     // }
                     if ($_POST['period'] == '') {
                            $error_msg = "Funding period cannot be blank";
                     }

					 if ($_POST['fin_type'] == '') {
                            $error_msg = "Financing Type cannot be blank";
                     }

                     if ($_POST['confirm'] == false) {
                            $error_msg = "You haven't confirm yet, please fill the checkbox";
                     }

                     if (!$error_msg) {
                            $time = date("Y-m-d H:i:s");
                            $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
                            // $emailaddress = $_POST['email'];
							$emailaddress = isset($_POST['email']) ? $_POST['email'] : '';
							$full_name = isset($_POST['full_name']) ? $_POST['full_name'] : '';
							$owner = isset($_POST['owner']) ? $_POST['owner'] : '';
							$mobile = isset($_POST['mobile']) ? $_POST['mobile'] : '';
							$country = isset($_POST['country']) ? $_POST['country'] : '';
							$company = isset($_POST['company']) ? $_POST['company'] : '';
							$industri = isset($_POST['industri']) ? $_POST['industri'] : '';
							$year = isset($_POST['year']) ? $_POST['year'] : '';
							$currency = isset($_POST['currency']) ? $_POST['currency'] : '';
							$project = isset($_POST['project']) ? $_POST['project'] : '';
							$est = isset($_POST['est']) ? $_POST['est'] : '';
							$fund = isset($_POST['fund']) ? $_POST['fund'] : '';
							$assets = isset($_POST['assets']) ? $_POST['assets'] : '';
							$period = isset($_POST['period']) ? $_POST['period'] : '';
							$po = isset($_POST['po']) ? $_POST['po'] : '';
							$fin_type = isset($_POST['fin_type']) ? $_POST['fin_type'] : '';
							$purchased_order2 = isset($_POST['purchased_order2']) ? $_POST['purchased_order2'] : '';
							$fileName = isset($_FILES["attach_file"]["name"]) ? $_FILES["attach_file"]["name"] : '';
							$fileImagePo = isset($_FILES["attach_file_po"]["name"]) ? $_FILES["attach_file_po"]["name"] : '';
							$fileImage = isset($_FILES["attach_file"]["name"]) ? $_FILES["attach_file"]["name"] : '';
							$source = isset($_POST['source']) ? $_POST['source'] : '';
                                                 $how_you_know = isset($_POST['how_you_know']) ? $_POST['how_you_know'] : '0';

                            if (preg_match($pattern, $emailaddress) !== 1) {
                                   // emailaddress is not valid
                                   $error_msg = "Email address is not valid";
                            } else {

									// $fileImage = $_FILES['attach_file']['tmp_name'];
									// $fileImagePo = $_FILES['attach_file_po']['tmp_name'];
									// $fileName = $_FILES["attach_file"]["name"];
									// move_uploaded_file($fileImage, "/var/www/kapitalboost.com/public_html/assets/images/getfunded/" . $fileName);
									// if ($_POST["po"] == "Yes") {
										// $fileNamePo = $_FILES["attach_file_po"]["name"];
										// move_uploaded_file($fileImagePo, "/var/www/kapitalboost.com/public_html/assets/images/getfunded/" . $fileNamePo);
										// move_uploaded_file($_FILES["attach_file_po"]["tmp_name"],"/var/www/kapitalboost.com/public_html/assets/images/getfunded/" . $_FILES["attach_file_po"]["name"]);
									// }else{
										// $fileImagePo = "0";
									// }

									$conn = $this->db->GetConn();
									$sql = "INSERT INTO tgetfunded SET email='" . $emailaddress . "',
									fullname='" . $full_name. "',
									owner='" . $owner. "',
									mobile='" . $mobile . "',
									country='" . $country . "',
									company='" . $company . "',
									industri='" .$industri . "',
									year='" . $year . "',
									currency='" . $currency . "',
									project_type='" . $project . "',
									est_an_rev='" . $est . "',
									funding_amount='" . $fund . "',
									ass_tobe_pur='" . $assets . "',
									attach_file = '$fileName',attach_file_po = '$fileNamePo',
									funding_period='" . $period . "',
									have_purchased_order='" . $po . "',
									date='" . $time . "',
									fin_type='" . $fin_type . "',
									source='" . $source . "',
                                                               how_you_know='" . $how_you_know . "',
									purchased_order='" . $purchased_order2 . "'";

									$this->db->exec($sql);

									$subject = '[INFO] Submitted for Funding';
									$message = "
									<html>
									<head>
									<title>Kapitalboost</title>
									</head>
									<style type='text/css'>
									@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
									</style>
									<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;font-family: 'Open Sans';'>
									<table height='100%' width='100%' bgcolor='#305e8e'>
									<tbody>
									<tr>
									<td style='height: 100%;width: 100%;padding: 20px 10%;' bgcolor='#305e8e'>
									<center>
									<div style='display: inline-block;position: relative;height: auto;width: 640px;padding: 0 0 10px 0;margin: 0 0 0 0;background: #00b0fd;'>
									<div style='display: inline-block;position: relative;height: auto;width: 100%;padding: 0;margin: 0;background: white;text-align: left;'>
									<img src='https://kapitalboost.com/assets/images/logo-baru.png' style='margin: 35px 0 0 35px;  max-width: 200px;' />
									<p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
									You have a new requested  project/campaign<br />
									</p>

									<p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
									Name                			: " . $full_name . "<br>
									Email Address               	: " . $emailaddress . "<br>
									Mobile Number               	: " . $mobile . "<br>
									Company                     	: " . $company . "<br>
									Year established            	: " . $year . "<br>
									Currency                    	: " . $currency . "<br>
									Project Type                	: " . $project . "<br>
									Est annual revenue          	: " . $est . "<br>
									Funding period              	: " . $period . "<br>
									Financing Type              	: " . $fin_type . "<br>
									Term of Use and Privacy Policy	: Agree <br>
									At : $time <br>
									</p>
									</div>
									</div>
									<div style='display: inline-block;position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
									<p style='font-size: 12px;color: white;'>
									&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
									</p>
									</div>
									</center>
									</td>
									</tr>
									</tbody>
									</table>
									</body>
									</html>

									";

									$this->SendEmailSendGridOld(EMAIL_FROM, "arief@kapitalboost.com", '', $subject, $message, TRUE);

									$subject2 = 'Get Funded via Kapital Boost';
									$message2 = "

									<html>
									<head>
									  <title>Kapital Boost</title>
									</head>
									<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
									<div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
									<div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
									<img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
									<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
									Hi " . $full_name . ",
									</p>
									<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
									You have successfully submitted a funding request. Our team is currently reviewing it and will be in touch with you within the next working day.
									</p>
									<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>

									To hasten the approval process, you may forward to us a copy of the following documents :
									<p style='display: block;font-size: 16px;text-align: left;margin: 0 3% 1% 3%;'>(i) Company business registration</p>
									<p style='display: block;font-size: 16px;text-align: left;margin: 0 3% 1% 3%;'>(ii) Last 12 months financial or bank statements</p>
									<p style='display: block;font-size: 16px;text-align: left;margin: 0 3% 1% 3%;'>(iii) Existing purchase order / invoice</p>
									</p>
									<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
									If you have any questions or concerns, please feel free to contact us.
									</p>
									<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
									Best regards,<br><br>Kapital Boost
									</p>
									</div>
									<div style='width:100%;height:7px;background-color:#00b0fd'></div>
									<div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
									<p style='font-size: 12px;color: white;text-align:center'>
									&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
									</p>
									</div>
									</div>
									</body>
									</html>

									";

									$this->SendEmailSendGridOld("admin@kapitalboost.com", $emailaddress, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject2, $message2, TRUE);

									$pesan_sukses = "You have successfully submitted a funding request. A Kapital Boost representative shall contact you within 1 working day.";
									$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
									$member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
									$member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
									$member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
									$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
                            }
                     }
                     $this->smarty->assign("member_id", $this->member_id);
                     $this->smarty->assign("member_name", $member_name);
                     $this->smarty->assign("member_surname", $member_surname);
                     $this->smarty->assign("member_firstname", $member_firstname);
                     $this->smarty->assign("member_country", $member_country);
                     $this->smarty->assign("member_email", $member_email);
                     $this->smarty->assign($_POST);
              }
			}
              $reply['error'] = $error_msg;

              $this->smarty->assign("pesan_sukses", $pesan_sukses);
              //$content = $this->smarty->fetch('TPL_Member_Register_Part_1.tpl');
              $this->_setMeta('SME Financing | Kapital Boost', 'Get Funded', 'Find out more on Islamic Financing Options for SMEs in South-East Asia. Visit our website for details, for enquiries call +65 9865 9648', '', '');

			  $this->AddTrail("Post Get Funded Web", $this->member_id);

              $content = $this->smarty->fetch('TPL_Get_Funded.tpl');
              return $content;
       }

}

?>
