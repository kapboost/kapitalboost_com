<?php

class Blog extends Front {

	function LoadList() {
		if ($_GET['tag']) {
			$tag = str_replace(",", "", $_GET['tag']);
			$sql = "SELECT * FROM tblog WHERE tag LIKE  '%$tag%' and enabled = 1 and lang = 0";
			list($content, ) = $this->db->multiarray($sql);
			$this->smarty->assign("content", $content);
			$this->_setMeta('Ideas on Islamic Financing and Crowdfunding Campaigns | Kapital Boost', 'Blog, Kapital Boost', 'Ideas, tips and thoughts on SME funding and Crowd sourcing based on Shariah Principles.  Visit our Blog for more content, for enquiries call +65 9865 9648', '', '');
			$this->AddTrail("Visit Blog Web", $this->member_id);
			$content = $this->smarty->fetch('TPL_Blog.tpl');
			return $content;
		} else {
			list($content, ) = $this->db->multiarray("SELECT * FROM tblog WHERE enabled = 1 and lang = 0 ORDER BY blog_id DESC");
			$this->smarty->assign("content", $content);
			$this->_setMeta('Ideas on Islamic Financing and Crowdfunding Campaigns | Kapital Boost', 'Blog, Kapital Boost', 'Ideas, tips and thoughts on SME funding and Crowd sourcing based on Shariah Principles.  Visit our Blog for more content, for enquiries call +65 9865 9648', '', '');
			$this->AddTrail("Visit Blog Web", $this->member_id);
			$content = $this->smarty->fetch('TPL_Blog.tpl');
			return $content;
		}
	}

	function GetContent() {
		$id = $_GET['blog_id'];
		str_replace($id, "(", "-");
		str_replace($id, ")", "-");
		$sql = "SELECT * FROM tblog WHERE slug='" . $id . "'";
		list($content, ) = $this->db->singlearray($sql);
		$tags = explode("~",$content["tag"]);
		$this->smarty->assign("content", $content);
		$this->smarty->assign("tags",$tags);
		// $this->_setMeta($content["blog_title"], "blog, Kapital Boost, " , $content['blog_title'], 'https://kapitalboost.com/assets/images/blog/'.$content["image"], '');
		$this->AddTrail("Visit Blog Page Web $id", $this->member_id);
		$content = $this->smarty->fetch('TPL_Blog_Detail.tpl');
		return $content;
	}

}

?>