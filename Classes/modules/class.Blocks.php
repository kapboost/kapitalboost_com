<?php

class Blocks extends Front {
	
	function LoadFooter() { 
		
		$content = "";
		$cek_className = "Dashboard";
		$cek_methodName = "DashboardConfirmBank";

		if ($cek_className == ($className = trim($_GET['class'])) && $cek_methodName == ($methodName = trim($_GET['method']) )) {
			$content = $this->smarty->fetch('TPL_Footer_Payment.tpl');
		} else {
			$content = $this->smarty->fetch('TPL_Footer.tpl');
		}
		return $content;
	}

	function LoadHeader() {
		$content = "";
		$cek_className = "Dashboard";
		$cek_methodName = "DashboardConfirmBank";

		if ($cek_className == ($className = trim($_GET['class'])) && $cek_methodName == ($methodName = trim($_GET['method']) )) {
			$content = $this->smarty->fetch('TPL_Header_Payment.tpl');
		} else {
			if ($this->member_id > 0) {
				$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
				$firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
				$xfers_token = $this->db->field("select member_access_token from tmember where member_id='$this->member_id'");
				$phone = $this->db->field("select member_mobile_no from tmember where member_id='$this->member_id'");
				$country = $this->db->field("select current_location from tmember where member_id='$this->member_id'");
				$this->smarty->assign("xfers_token", $xfers_token);
				$this->smarty->assign("phone", $phone);
				$this->smarty->assign("country", $country);
				$this->smarty->assign("member_id", $this->member_id);
				$this->smarty->assign("member_email", $this->member_email);
				$this->smarty->assign("member_name", $member_name);
				$this->smarty->assign("member_firstname",$firstname);
			}
			$content = $this->smarty->fetch('TPL_Header.tpl');
		}
		return $content;
	}

	function LoadCampaignUname() {
		if ($this->member_id > 0) {
			$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
			$this->smarty->assign("member_id", $this->member_id);
			$this->smarty->assign("member_email", $this->member_email);
			$this->smarty->assign("member_firstname", $member_firstname);
			$this->smarty->assign("member_surname", $member_surname);
			$this->smarty->assign("member_country", $member_country);
			$this->smarty->assign("member_name", $member_name);
		}
		$content = $this->smarty->fetch('TPL_Campaign_Details.tpl');
		return $content;
	}

	function LoadContentMiddle() {
		$content = "";
		$className = "";
		$methodName = "";
		$this->smarty->assign("la", $this->lang);

		if (isset($_GET['class']) != '') {
			$className = trim($_GET['class']);
			$methodName = trim($_GET['method']);
			$class_file = DOC_ROOT . "Classes/modules/class.{$className}.php";

			if (file_exists($class_file) && is_file($class_file)) {
				include_once($class_file);
				eval("\$obj = new $className;");
				$pos = strpos($methodName, "_");
				$this->smarty->assign('CurrentPage', "{$className}-{$methodName}");

				if (method_exists($obj, $methodName) && $pos !== 0) {
					$content = $obj->$methodName();
				} else {
					$content = $this->LoadErrorPage("The page you are looking for is temporarily unavailable.");
				}
			} else {
				$content = $this->LoadErrorPage("The page you are looking for is temporarily unavailable.");
			}
		} else {
			$content = $this->LoadHomeContent();
		}

		return $content;
	}

	function LoadHomeContent() {
		$now = date("Y-m-d H-i-s");

		if ($this->lang == 'id') {			 
		} else {
			list($testi, ) = $this->db->multiarray("SELECT * FROM ttestimonial WHERE testimonial_id = 25 ");
			list($testi2, ) = $this->db->multiarray("SELECT * FROM ttestimonial WHERE testimonial_id ORDER BY testimonial_id DESC");
		}

		list($content, ) = $this->db->multiarray("SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration 
			FROM tcampaign WHERE enabled=1 && (project_type='sme' || project_type='private') ORDER BY release_date DESC LIMIT 3");

		list($donation, ) = $this->db->multiarray("SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration 
			FROM tcampaign WHERE enabled=1 && project_type='donation' ORDER BY release_date DESC LIMIT 3");

		
		$exRate = $this->ExRate();;
		
		$this->AddTrail("Visit Home Web", $this->member_id);

		for ($i = 0; $i < count($content); $i++) {
			$totalFundingAmt = $content[$i]["total_funding_amt"];
			$currentFundingAmt = $content[$i]["curr_funding_amt"];
			$totalDonationFundingAmt = $donation[$i]["total_funding_amt"];
			$currentDonationFundingAmt = $donation[$i]["curr_funding_amt"];

			if ($totalFundingAmt == $currentFundingAmt) {
				$content[$i]["curr_funding_amt_id"] = $content[$i]["total_funding_amt_id"];
			} else {
				$content[$i]["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
			}
			if ($totalDonationFundingAmt == $currentDonationFundingAmt) {
				$donation[$i]["curr_funding_amt_id"] = $donation[$i]["total_funding_amt_id"];
			} else {
				$donation[$i] ["curr_funding_amt_id"] = $currentDonationFundingAmt * $exRate;
			}
		}

		$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
		$member_country = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
		$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
		$this->smarty->assign("content", $content);
		$this->smarty->assign("member_id", $this->member_id);
		$this->smarty->assign("member_name", $member_name);
		$this->smarty->assign("testi", $testi);
		$this->smarty->assign('testi2', $testi2);
		$this->smarty->assign('donation', $donation);
		$this->smarty->assign('email', $member_email);
		$this->smarty->assign('member_country', $member_country);
		$this->smarty->assign('exRate', $exRate);

		$content1 = $this->smarty->fetch('TPL_Homepage.tpl');
		return $content1;
	}

}

?>
