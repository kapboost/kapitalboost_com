<?php

class Front {

	var $db;
	var $smarty;
	var $sys_msg;
	var $merchant_id;
	var $config;
	var $member_id;
	var $session;
	var $xajax;
	var $objResponse;
	var $cookie_id;
	var $lang;
	var $page_title;

	function Front() {
		global $db, $smarty, $merchant_id, $config, $sys_msg, $xajax, $lang;
		$this->db = &$db;
		$this->smarty = &$smarty;
		$this->merchant_id = $merchant_id;
		$this->config = &$config;
		$this->sys_msg = &$sys_msg;

		$this->session = &$_SESSION['cookie']['cookie'];
		$this->cookie_id = &$_SESSION['cookie']['cookie_id'];
		$this->member_id = &$_SESSION['cookie']['member_id'];

		if (isset($_GET["lang"])) {
			$setlang = $_GET["lang"];
			if ($setlang == "id") {
				$this->lang = "id";
				setcookie("la", "id", 1900000000, "/");
				if ($this->member_id > 0) {
					$sql = "UPDATE tmember SET lang = 'id' WHERE member_id = '$this->member_id'";
					$this->db->exec($sql);
				}
			} else if ($setlang == "en") {
				$this->lang = "en";
				setcookie("la", "en", 1900000000, "/");
				if ($this->member_id > 0) {
					$sql = "UPDATE tmember SET lang = 'en' WHERE member_id = '$this->member_id'";
					$this->db->exec($sql);
				}
			}
		} else {
			//$this->lang = $_COOKIE["la"];
			$this->lang = "en";
		}

		if ($this->lang == "") {
			if ($this->member_id > 0) {
				$this->lang = $this->db->field("select lang from tmember where member_id='" . $this->member_id . "'");
				setcookie("la", $this->lang, 1900000000, "/");
			} else {
				$this->lang = "en";
				setcookie("la", "en", 1900000000, "/");
			}
		}
	}

	function SetPager($sql, $url = '', $limit_per_page = 0) {
		if ($url == '') {
			$url = $_SERVER['REQUEST_URI'];
		}

		$url = preg_replace("/\d+/", "", $url);

		if (!$limit_per_page) {
			$limit_per_page = PAGER_PER_PAGE;
		}

		if (!isset($_GET['page'])) {
			$page = 1;
		} else {
			$page = (int) $_GET['page'];
		}

		list(, $total_records) = $this->db->multiarray($sql);

		$pager_url = $url . "";
		$kgPagerOBJ = new kgPager();
		$kgPagerOBJ->pager_set($pager_url, $total_records, PAGER_SCROLL_PAGE, $limit_per_page, $page, PAGER_INACTIVE_PAGE_TAG, PAGER_PREVIOUS_PAGE_TEXT, PAGER_NEXT_PAGE_TEXT, PAGER_FIRST_PAGE_TEXT, PAGER_LAST_PAGE_TEXT, '');
		$this->smarty->assign('pager_url', $pager_url . $page);

		$result_paging = "";
		if ($kgPagerOBJ->total_pages > 1) {
			$result_paging .= '<ul class="pagination">' . $kgPagerOBJ->first_page .
			$kgPagerOBJ->previous_page .
			$kgPagerOBJ->page_links .
			$kgPagerOBJ->next_page .
			$kgPagerOBJ->last_page .
			'</ul>';
		}

		$this->smarty->assign('pager', $result_paging);
		list($list, ) = $this->db->multiarray($sql . " LIMIT $kgPagerOBJ->start, $kgPagerOBJ->per_page");

		return $list;
	}

	function CalculatePrice($price, $disc_type, $disc_value) {

		//Percentage discount
		if ($disc_type == 1) {
			$price = ((100 - $disc_value) / 100) * $price;
		}

		//dollar discount
		if ($disc_type == 2) {
			$price = $price - $disc_value;
		}

		$price = number_format($price, 2, '.', '');

		return $price;
	}

	function SetWebInfor($key, $str) {
		$this->config['sys'][$key] = $str;
	}

	function SetSession($key, $str) {
		$_SESSION['sys'][$key] = $str;
	}

	function AddSession($key, $str) {
		if (count($_SESSION['sys'][$key]) > 2) {
			$ar = &$_SESSION['sys'][$key];
			$ar = array_shift($ar);
		}

		if (!is_array($_SESSION['sys'][$key])) {
			$_SESSION['sys'][$key] = array();
		}
		$_SESSION['sys'][$key][] = $str;
	}

	function GetSession($key) {
		return $_SESSION['sys'][$key];
	}

	function CallMethod($className, $methodName, $params = NULL) {
		$obj = $this->InvokeClass($className);

		$result = $this->InvokeMethod($obj, $methodName, $params);
		return $result;
	}

	function InvokeMethod($obj, $method, $params = NULL) {
		if ($params) {
			return $obj->$method($params);
		} else {
			return $obj->$method();
		}
	}

	function InvokeClass($class) {
		  include_once(DOC_ROOT . "Classes/modules/class.{$class}.php");
		  eval("\$obj = new $class;");

		  return $obj;
	}

	// check if email address is valid
	function validate_email($val) {
		if ($val != "") {
			$pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
			if (preg_match($pattern, $val)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function Redirect($url, $hidden = array()) {

		$result = "
		<html>
		<head><title>Redirect</title></head>
		<base href=\"" . SERVER_BASE . "\" />
		<body>
		<form method=\"POST\" action=\"$url\" name=\"redirectForm\">";

		if (count($hidden)) {
			 foreach ($hidden as $fname => $fval) {
					$result .= "<input type=\"hidden\" name=\"$fname\" value=\"$fval\">";
			 }
		}

		$result .= "</form>
		<script language=\"JavaScript\">
		<!--
		document.redirectForm.submit();
		//-->
		</script>
		</body>
		</html>
		";

		//print $result;
		exit(0);
	}

	function AddBreadCrumb($key, $link = NULL) {
		$this->config['breadcrumb'][$key] = $link;
	}
	
	function AddTrail($trail, $member_id) {
		if ($member_id == NULL or $member_id=="" or $member_id=="0"){$member_id= $this->getUserIP();}
		$sqls = "INSERT INTO trail(event,member_id,date) VALUES ('$trail','$member_id',now())"; 
		// echo "'<script>console.log(\"trail : $sqls\")</script>'";
		$this->db->exec($sqls);
	}
	
	function ExRate() {
		$url = "https://free.currencyconverterapi.com/api/v5/convert?q=SGD_IDR&compact=y";
		$ch = curl_init();
		$timeout = 0;

		curl_setopt ($ch, CURLOPT_URL, $url);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$rawdata = curl_exec($ch);

		curl_close($ch);
		
		$obj = json_decode($rawdata);
		$exRate= round($obj->SGD_IDR->val);

		if ($exRate != '0' and $exRate != 0 and is_numeric($exRate)) {
			$updateCurrency = "UPDATE setting SET value = '$exRate' WHERE id = '1'";
			$this->db->exec($updateCurrency);
		} else {
			$exRate = $this->db->field("select value from setting where id = '1'");
		}
		
		return $exRate;
	}
	
	function getUserIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];

		if(filter_var($client, FILTER_VALIDATE_IP))
		{
			$ip = $client;
		}
		elseif(filter_var($forward, FILTER_VALIDATE_IP))
		{
			$ip = $forward;
		}
		else
		{
			$ip = $remote;
		}

		return $ip;
	}



	function LoadBreadCrumb() {
		$result = array();
		foreach ($this->config['breadcrumb'] as $key => $link) {
			if ($link != '') {
				$result[] = "<li><a href=\"$link\">" . htmlspecialchars($key) . "</a></li>";
			} else {
				$result[] = "<li class=\"active\">" . htmlspecialchars($key) . "</li>";
			}
		}

		$display = implode(htmlspecialchars(""), $result);
		return $display;
	}

	function LoadErrorPage($msg = "") {
		$result = "";

		if ($msg != "") {
			$result = "<div class='row main-content-wrapper-00'>
						<div class='separator'></div>
						<div class='col-xs-12'>{$msg}</p>
						</div>
						</div>";
		}
		return $result;
	}

	function formatSQL($str) {
		return $str;
		  //return mysql_escape_string($str);
	}

	function SendEmail($from, $to, $bcc, $subject, $message, $html = FALSE) {
		include_once(DOC_ROOT . 'Classes/class.phpmailer.php');
		date_default_timezone_set('Asia/Singapore');
		$mail = new PHPMailer();

		// Edited
		//$mail->IsSMTP();
		//$mail->Host = "smtp.gmail.com";
		//$mail->SMTPDebug = 2;
		//$mail->DKIM_domain = '127.0.0.1';
		// $mail->Debugoutput = 'html';
		//$mail->SMTPSecure = 'ssl';
		//$mail->SMTPAuth = true;
		//$mail->Host = "smtpout.secureserver.net"; // sets the SMTP server
		//$mail->Port = 25;                    // set the SMTP port for the GMAIL server              
		//$mail->port = 465;
		// Edited

		$mail->From = $from;
		$mail->FromName = EMAIL_FROM_NAME;
		$mail->SingleTo = true; //will send mail to each email address individually

		if (is_array($to)) {
			foreach ($to as $to_email) {
				$mail->AddAddress($to_email);
			}
		} else {
			$mail->AddAddress($to);
		}

		if (is_array($bcc)) {
			foreach ($bcc as $to_email) {
				$mail->AddBCC($to_email);
			}
		} else {
			if ($bcc != '') {
				$mail->AddBCC($bcc);
				$bcc2="arief@kapitalboost.com";
				$mail->AddBCC($bcc2);
			}
		}

		$mail->IsHTML($html);

		$mail->Subject = $subject;
		$mail->Body = $message;
		$mail->Encoding = "base64";
		$mail->CharSet = "UTF-8";
		$mail->AddReplyTo("admin@kapitalboost.com", "Reply to Kapital Boost");
		$mail->addCustomHeader("X-AntiAbuse: This header was added to track abuse, please include it with any abuse report");
		$mail->addCustomHeader("X-AntiAbuse: Primary Hostname - kapitalboost.com");
		$mail->addCustomHeader("X-AntiAbuse: Sender Address Domain - kapitalboost.com");
		$mail->addCustomHeader("X-Authenticated-Sender: admin@kapitalboost.com");
		$mail->addCustomHeader("X-Source-Dir: :/kapitalboost.com/public_html");

		$mail->Send();
		$mail->ClearAddresses();
		$mail->ClearBCCs();
	}

	function SendEmailSendGrid($from, $to, $bcc, $subject, $message, $html = FALSE) {
		require(DOC_ROOT . 'Classes/sendgrid-php/sendgrid-php.php');
		$subject = $subject;
		$to = new SendGrid\Email($to, $to);
		$header = $this->db->field("select content from temail where name='header'");
		
		$footer = $this->db->field("select content from temail where name='footer'");
		
		$content_email = $header."".$message."".$footer;
		$content = new SendGrid\Content("text/html", $content_email);
		$from = new SendGrid\Email("Kapital Boost", "info@kapitalboost.com");
		$mail = new SendGrid\Mail($from, $subject, $to, $content);
		$to = new SendGrid\Email(null, "arief@kapitalboost.com");
		$mail->personalization[0]->addBcc($to);
		$to = new SendGrid\Email(null, "admin@kapitalboost.com");
		$mail->personalization[0]->addBcc($to);
		$apiKey = "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";
		$sg = new \SendGrid($apiKey);
		$response = $sg->client->mail()->send()->post($mail);
		// echo $response->statusCode();
		// print_r($response->headers());
		// echo $response->body();
		
	}
	
	function SendEmailSendGridOld($from, $to, $bcc, $subject, $message, $html = FALSE) {
		require(DOC_ROOT . 'Classes/sendgrid-php/sendgrid-php.php');
		$subject = $subject;
		$to = new SendGrid\Email($to, $to);
		$content_email = $message;
		$content = new SendGrid\Content("text/html", $content_email);
		$from = new SendGrid\Email("Kapital Boost", "info@kapitalboost.com");
		$mail = new SendGrid\Mail($from, $subject, $to, $content);
		$apiKey = "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";
		$sg = new \SendGrid($apiKey);
		$response = $sg->client->mail()->send()->post($mail); 
		
	}

	public function _setMeta($page_title, $meta_keyword, $meta_desc, $image_url = "", $site_campaign_url = "") {

		$_SESSION['page_title'] = $page_title;
		$_SESSION['meta_keyword'] = $meta_keyword;
		$_SESSION['meta_desc'] = $meta_desc;
		$_SESSION['image_url'] = $image_url;
		$_SESSION['site_campaign_url'] = $site_campaign_url;
	}

	public function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	public function MailChimp($email, $firstname, $lastname, $country) {
		// MailChimp Integration Add new member
		$mcArray = array(
			"email_address" => "$email",
			"status" => "subscribed",
			"merge_fields" => array("FNAME" => "$firstname", "LNAME" => "$lastname", "COUNTRY" => "$country")
		);
		$mcJson = json_encode($mcArray);
		$mcCurl = curl_init();
		curl_setopt_array($mcCurl, array(
			CURLOPT_URL => "https://us11.api.mailchimp.com/3.0/lists/c2bdf8bd59/members",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 50,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $mcJson,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"authorization: Basic b3dlbjoxOGQ0ZjNlNzAxNTQ3ZWUwYzljYzk4MmE0YjI1NDFhMC11czEx"
			),
		));
		$response1 = curl_exec($mcCurl);
		curl_close($mcCurl);
	}

}

?>
