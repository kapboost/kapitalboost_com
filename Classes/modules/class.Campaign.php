<?php

include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

class Campaign extends Front {

	function genRndString($length = 150, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
		if ($length > 0) {
			$len_chars = (strlen($chars) - 1);
			$the_chars = $chars{rand(0, $len_chars)};
			for ($i = 1; $i < $length; $i = strlen($the_chars)) {
				$r = $chars{rand(0, $len_chars)};
				if ($r != $the_chars{$i - 1}) {
					$the_chars .= $r;
				}
			}
			return $the_chars;
		}
	}

	function OrderId($length = 12, $chars = '1234567890') {
		if ($length > 0) {
			$len_chars = (strlen($chars) - 1);
			$the_chars = $chars{rand(0, $len_chars)};
			for ($i = 1; $i < $length; $i = strlen($the_chars)) {
				$r = $chars{rand(0, $len_chars)};
				if ($r != $the_chars{$i - 1})
				$the_chars .= $r;
			}
			return $the_chars;
		}
	}

	function get_campaign() {
		if ($_POST['project_type'] == 'all') {
			header('Location: https://kapitalboost.com/campaign/');
			exit;
		}
		if ($_POST['project_type'] == 'sme') {
			header('Location: https://kapitalboost.com/campaign/sme/1');
			exit;
		}
		if ($_POST['project_type'] == 'donation') {
			header('Location: https://kapitalboost.com/campaign/donation/1');
			exit;
		}
		if ($_POST['project_type'] == 'private') {
			header('Location: https://kapitalboost.com/campaign/private/1');
			exit;
		}
	}

	function LoadList() {
		if (!empty($_GET['campaign'])) {
			$now = date("Y-m-d H-i-s");
			if ($_GET['campaign'] == 'sme') {
				$content = $_GET['project_type'];
				$start = isset($_GET['page']) ? $_GET['page'] : 0;
				$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE enabled=1 && project_type='" . $_GET['campaign'] . "' ORDER BY release_date DESC";
				list($content, ) = $this->db->multiarray($sql);
				$content = $_GET['project_type'];
				$content = $this->setPager($sql);
				$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
				$member_country = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
				$member_status = $this->db->field("select member_status from tmember where member_id='" . $this->member_id . "'");
				$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
				$member_surname = $this->db->field("select member_surname from tmember where member_id='".$this->member_id."'");


				$exRate = $this->ExRate();

				for ($i = 0; $i < count($content); $i++) {
					$totalFundingAmt = $content[$i]["total_funding_amt"];
					$currentFundingAmt = $content[$i]["curr_funding_amt"];

					if ($totalFundingAmt == $currentFundingAmt) {
						$content[$i]["curr_funding_amt_id"] = $content[$i]["total_funding_amt_id"];
					} else {
						$content[$i]["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
					}
				}
				$this->smarty->assign("member_id", $this->member_id);
				$this->smarty->assign("member_name", $member_name);
				$this->smarty->assign("member_email", $member_email);
				$this->smarty->assign("campaign_name", $campaign_name);
				$this->smarty->assign("member_surname", $member_surname);
				$this->smarty->assign("member_country", $member_country);
				$this->smarty->assign("content", $content);

				$this->_setMeta('Kapital Boost SME Crowdfunding Campaigns', 'SME, Campaign', 'Find out more on Islamic Financing Options for SMEs, Donation, and Private crowdfunding platform in South-East Asia.', '', '');

				$content = $this->smarty->fetch('TPL_Campaign.tpl');
				return $content;
			} else if ($_GET['campaign'] == 'donation') {
				$donation = $_GET['campaign'] == 'donation';
				$start = isset($_GET['page']) ? $_GET['page'] : 0;

				$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE  enabled=1 &&  project_type='" . $_GET['campaign'] . "' ORDER BY campaign_id DESC";
				list($content, ) = $this->db->multiarray($sql);
				$content = $_GET['project_type'];
				$content = $this->setPager($sql);

				$this->smarty->assign("content", $content);
				$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
				$member_country = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
				$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");


				$exRate = $this->ExRate();

				for ($i = 0; $i < count($content); $i++) {
					$totalFundingAmt = $content[$i]["total_funding_amt"];
					$currentFundingAmt = $content[$i]["curr_funding_amt"];

					if ($totalFundingAmt == $currentFundingAmt) {
						$content[$i]["curr_funding_amt_id"] = $content[$i]["total_funding_amt_id"];
					} else {
						$content[$i]["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
					}
				}
				$this->smarty->assign("member_id", $this->member_id);
				$this->smarty->assign("member_name", $member_name);
				$this->smarty->assign("campaign_name", $campaign_name);
				$this->smarty->assign("member_country", $member_country);
				$this->_setMeta('Kapital Boost Donation Crowdfunding Campaigns', 'Donation, Campaign', 'Find out more on Islamic Financing Options for SMEs, Donation, and private crowdfunding platform in South-East Asia.', '', '');
				$content = $this->smarty->fetch('TPL_Campaign.tpl');

				return $content;
			} else {
				$content = $_POST['project_type'];
				$private_pass = $_POST['password'];
				$start = isset($_GET['page']) ? $_GET['page'] : 0;
				$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE enabled=1 && project_type='" . $_GET['campaign'] . "' ORDER BY release_date DESC";
				list($content, ) = $this->db->multiarray($sql);
				$content = $_GET['project_type'];
				$content = $this->setPager($sql);
				$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
				$member_country = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
				$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");


				$exRate = $this->ExRate();

				for ($i = 0; $i < count($content); $i++) {
					$totalFundingAmt = $content[$i]["total_funding_amt"];
					$currentFundingAmt = $content[$i]["curr_funding_amt"];

					if ($totalFundingAmt == $currentFundingAmt) {
						$content[$i]["curr_funding_amt_id"] = $content[$i]["total_funding_amt_id"];
					} else {
						$content[$i]["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
					}
				}
				$this->smarty->assign("member_id", $this->member_id);
				$this->smarty->assign("member_name", $member_name);
				$this->smarty->assign("campaign_name", $campaign_name);
				$this->smarty->assign("content", $content);
				$this->smarty->assign("member_country", $member_country);
				$this->_setMeta('Kapital Boost Private Campaigns', 'private campaign', 'Find out more on Islamic Financing Options for SMEs, Donation, and private crowdfunding platform in South-East Asia.', '', '');
				$content = $this->smarty->fetch('TPL_Campaign.tpl');
				return $content;
			}
		} else {
			$start = isset($_GET['page']) ? $_GET['page'] : 0;
			$now = date("Y-m-d H-i-s");
			$sql = "SELECT *,DATEDIFF(expiry_date, '$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration
			FROM tcampaign WHERE enabled=1 ORDER BY release_date DESC";
			$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
			$member_country = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
			$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
			$content = $this->setPager($sql);


			$exRate = $this->ExRate();

			for ($i = 0; $i < count($content); $i++) {
				$totalFundingAmt = $content[$i]["total_funding_amt"];
				$currentFundingAmt = $content[$i]["curr_funding_amt"];

				if ($totalFundingAmt == $currentFundingAmt) {
					$content[$i]["curr_funding_amt_id"] = $content[$i]["total_funding_amt_id"];
				} else {
					$content[$i]["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
				}
			}
			$this->smarty->assign("content", $content);
			$this->smarty->assign("member_id", $this->member_id);
			$this->smarty->assign("member_name", $member_name);
			$this->smarty->assign("campaign_name", $campaign_name);
			$this->smarty->assign("member_country", $member_country);
			$this->smarty->assign("project_type_options", $this->config['project_type_options']);
			$this->_setMeta('SME Financing, Muslim Donations, Private Crowdfunding | Kapital Boost', 'SME, Donation, Private, Campaign', 'Find out more on Islamic Financing Options for SMEs, Charity/Donation, and private crowdfunding platform in South-East Asia.', '', '');

			$this->AddTrail("Visit Campaign Web", $this->member_id);

			$content = $this->smarty->fetch('TPL_Campaign.tpl');
			return $content;
		}
	}

	function GetContent() {
		$now = date("Y-m-d H-i-s");



		if (isset($_POST['member_password']) && !empty($_POST['member_password'])) {
			$_SESSION['pass'] = $_POST['member_password'];
		}

		if (isset($_SESSION['pass']) && !empty($_SESSION['pass'])) {
			$this->smarty->assign('pass', $_SESSION['pass']);
		}

		$id = (int) $_GET['campaign_id'];
		$preview = $_GET["preview"];
		$privates = $_GET["private"];

		$total_funding_amt_check = $this->db->field("select total_funding_amt from tcampaign where campaign_id = '$id'");
		$curr_funding_amt_check = $this->db->field("select curr_funding_amt from tcampaign where campaign_id = '$id'");
		$release_date_check = $this->db->field("select release_date from tcampaign where campaign_id = '$id'");
		// echo "'<script>console.log(\"total_funding_amt_check : $total_funding_amt_check\")</script>'";
		// echo "'<script>console.log(\"curr_funding_amt_check : $curr_funding_amt_check\")</script>'";
		// echo "'<script>console.log(\"release_date_check : $release_date_check\")</script>'";

		if ($curr_funding_amt_check >= $total_funding_amt_check) {
			$updateAuto = "UPDATE tcampaign SET expiry_date = '$release_date_check' WHERE campaign_id = '$id'";
			echo "'<script>console.log(\"updateAuto : $updateAuto\")</script>'";
			$this->db->exec($updateAuto);
		}

		if ($preview == 1 || $id == 125 || $privates == 1) {
			$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration, DATEDIFF(release_date,'$now') AS coming_days FROM tcampaign WHERE campaign_id='" . $id . "'";
		} else {
			$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration, DATEDIFF(release_date,'$now') AS coming_days FROM tcampaign WHERE enabled=1 && campaign_id='" . $id . "'";
		}

		list($content, ) = $this->db->singlearray($sql);
		$campaigntype = $content["campaign_type"];
		$privateProjectType = $content["project_type"];

		if ($id == 75){ //khusus untuk private crowdfund satay
			header("Location: https://kapitalboost.com/campaign/private/view/". $id);
		}

		$this->smarty->assign("campaigntype", $campaigntype);

		$campaign_name = $content['campaign_name'];

		$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->member_id > 0) {
			$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
			$member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
			$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
			$member_password = $this->db->field("select member_password from tmember where member_id='" . $this->member_id . "'");
			$member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
			$member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
			$member_icname = $this->db->field("select member_icname from tmember where member_id='" . $this->member_id . "'");
			$member_icoption = $this->db->field("select ic_option from tmember where member_id='" . $this->member_id . "'");
			$member_iccountry = $this->db->field("select ic_country from tmember where member_id='" . $this->member_id . "'");
			$member_status = $this->db->field("select member_status from tmember where member_id='" . $this->member_id . "'");
			$member_mobile = $this->db->field("select member_mobile_no from tmember where member_id='" . $this->member_id . "'");
			$resarea = $this->db->field("select residental_area from tmember where member_id='" . $this->member_id . "'");
			$dob = $this->db->field("select dob from tmember where member_id='" . $this->member_id . "'");
			$nric = $this->db->field("select nric from tmember where member_id='" . $this->member_id . "'");
			$nric_file = $this->db->field("select nric_file from tmember where member_id='" . $this->member_id . "'");
			$nric_file_back = $this->db->field("select nric_file_back from tmember where member_id='" . $this->member_id . "'");
			$addressProof = $this->db->field("select address_proof from tmember where member_id='" . $this->member_id . "'");
			$totalFundingbyUser = $this->db->field("select SUM(total_funding) from tinv where member_id='" . $this->member_id . "' and campaign_id = '" . $id . "'");
			echo "'<script>console.log(\"totalFundingbyUser : $totalFundingbyUser\")</script>'";



			$token = $this->genRndString();
			$orderid = $this->OrderId();

			$current_location = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");

			$need_bank_detail = false;
			if($current_location == 'SINGAPORE') {
				list($banks, ) = $this->db->singlearray("SELECT * FROM bank_account WHERE member_id='$this->member_id'");
				if(count($banks) == 0) {
					$need_bank_detail = true;
				}else {
					if($banks['account_name'] == '' || $banks['account_number'] == '') {
						$need_bank_detail = true;
					}else {
						$need_bank_detail = false;
					}
				}
			}


			$campaign_id = $_GET["campaign_id"];
			$this->smarty->assign("campaign_id", $campaign_id);
			$this->smarty->assign("current_location", $current_location);
			$this->smarty->assign('need_bank_detail', $need_bank_detail);
			$this->smarty->assign("orderid", $orderid);
			$this->smarty->assign("token", $token);
			$this->smarty->assign("member_id", $this->member_id);
			$this->smarty->assign("member_name", $member_name);
			$this->smarty->assign("member_status", $member_status);
			$this->smarty->assign("member_surname", $member_surname);
			$this->smarty->assign("member_firstname", $member_firstname);
			$this->smarty->assign("member_icname", $member_icname);
			$this->smarty->assign("ic_option", $member_icoption);
			$this->smarty->assign("ic_country", $member_iccountry);
			$this->smarty->assign("member_mobile", $member_mobile);
			$this->smarty->assign("member_country", $member_country);
			$this->smarty->assign("member_email", $member_email);
			$this->smarty->assign("member_password", $member_password);
			$this->smarty->assign("nric", $nric);
			$this->smarty->assign("dob", $dob);
			$this->smarty->assign("resarea", $resarea);
			$this->smarty->assign("filenric", $nric_file);
			$this->smarty->assign("filenricback", $nric_file_back);
			$this->smarty->assign("addressProof", $addressProof);
			$this->smarty->assign("userTotalFunding", $totalFundingbyUser);
		}

		$campaign_id = $_GET["campaign_id"];
		$release_time = $this->db->field("select release_date from tcampaign where campaign_id='" . $campaign_id . "'");
		$release_time = date("M j, Y", strtotime($release_time));
		// META Fb
		$fb_campaign_name = $this->db->field("select campaign_name from tcampaign where campaign_id='" . $campaign_id . "'");
		$fb_campaign_snippet = $this->db->field("select campaign_snippet from tcampaign where campaign_id='" . $campaign_id . "'");
		$fb_campaign_bg = $this->db->field("select campaign_background from tcampaign where campaign_id='" . $campaign_id . "'");
		$fb_campaign_desc = $this->db->field("select campaign_description from tcampaign where campaign_id='" . $campaign_id . "'");
		// Set Meta Tag

		$image_names = $this->db->field("select Image_Name from tcampaign where campaign_id='" . $campaign_id . "'");
		$image_name = explode("~", $image_names);

		$page_title = "Kapital Boost - crowdfunding of " . $fb_campaign_name . " campaign";
		$meta_keyword = $fb_campaign_snippet;
		$meta_desc = $fb_campaign_desc;
		$image_url = "https://kapitalboost.com/assets/images/campaign/" . $fb_campaign_bg;
		$this->_setMeta($page_title, $meta_keyword, $meta_desc, $image_url, '');

		$relatedC1 = $this->db->field("SELECT related_campaign_1 FROM tcampaign WHERE campaign_id = $campaign_id ");
		$relatedC2 = $this->db->field("SELECT related_campaign_2 FROM tcampaign WHERE campaign_id = $campaign_id ");
		$relatedC3 = $this->db->field("SELECT related_campaign_3 FROM tcampaign WHERE campaign_id = $campaign_id ");

		$relatedC1Id = explode("~", $relatedC1)[1];
		$relatedC2Id = explode("~", $relatedC2)[1];
		$relatedC3Id = explode("~", $relatedC3)[1];

		list($other1, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE campaign_id = $relatedC1Id");
		list($other2, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE campaign_id = $relatedC2Id");
		list($other3, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE campaign_id = $relatedC3Id");

		$pdfs = $content["pdfs"];
		$pdfsDes = $content["pdfs_description"];
		$pdfsArray = explode("~", $pdfs);
		$pdfsDesArray = explode("~", $pdfsDes);



		if (isset($_POST["closedPw"])) {
			$closedPw = $_POST["closedPw"];
			$closedPwDb = $this->db->field("select private_password from tcampaign where campaign_id='$campaign_id'");

			if ($closedPw === $closedPwDb) {
				$this->smarty->assign("closedPw", "correct");
			} else {
				$this->smarty->assign("closedPw", "wrong");
			}
		}


		$exRate = $this->ExRate();

		$totalFundingAmt = $content["total_funding_amt"];
		$currentFundingAmt = $content["curr_funding_amt"];

		if ($totalFundingAmt == $currentFundingAmt) {
			$content["curr_funding_amt_id"] = $content["total_funding_amt_id"];
		} else {
			$content["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
		}

		$this->smarty->assign("exRate",$exRate);
		$this->smarty->assign("release_time",$release_time);
		$this->smarty->assign("content", $content);
		$this->smarty->assign("image_name", $image_name);
		$this->smarty->assign("other1", $other1);
		$this->smarty->assign("other2", $other2);
		$this->smarty->assign("other3", $other3);
		$this->smarty->assign("pdfsArray", $pdfsArray);
		$this->smarty->assign("pdfsDesArray", $pdfsDesArray);

		$this->AddTrail("Visit Campaign Details Web: $fb_campaign_name", $this->member_id);

		$content = $this->smarty->fetch('TPL_Campaign_Details.tpl');
		return $content;
	}

	function TestPrivate() {
		$this->smarty->assign("content", $content);
		$content = $this->smarty->fetch('TPL_Private.tpl');
		return $content;
	}

	function PrivateContent() {
		$now = date("Y-m-d H-i-s");
		$id = (int) $_GET['campaign_id'];
		$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE project_type='private' && campaign_id='" . $id . "'";

		list($content, ) = $this->db->singlearray($sql);
		$campaigntype = $content["campaign_type"];
		$privateProjectType = $content["project_type"];

		if ($privateProjectType != "private"){
			header("Location: https://kapitalboost.com/campaign/");
		}

		$this->smarty->assign("campaigntype", $campaigntype);
		$campaign_name = $content['campaign_name'];
		$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if ($this->member_id > 0) {
			$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
			$member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
			$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
			$member_password = $this->db->field("select member_password from tmember where member_id='" . $this->member_id . "'");
			$member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
			$member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
			$member_icname = $this->db->field("select member_icname from tmember where member_id='" . $this->member_id . "'");
			$member_icoption = $this->db->field("select ic_option from tmember where member_id='" . $this->member_id . "'");
			$member_iccountry = $this->db->field("select ic_country from tmember where member_id='" . $this->member_id . "'");
			$member_mobile = $this->db->field("select member_mobile_no from tmember where member_id='" . $this->member_id . "'");
			$resarea = $this->db->field("select residental_area from tmember where member_id='" . $this->member_id . "'");
			$dob = $this->db->field("select dob from tmember where member_id='" . $this->member_id . "'");
			$nric = $this->db->field("select nric from tmember where member_id='" . $this->member_id . "'");
			$nric_file = $this->db->field("select nric_file from tmember where member_id='" . $this->member_id . "'");
			$nric_file_back = $this->db->field("select nric_file_back from tmember where member_id='" . $this->member_id . "'");
			$addressProof = $this->db->field("select address_proof from tmember where member_id='" . $this->member_id . "'");

			$token = $this->genRndString();
			$orderid = $this->OrderId();

			$current_location = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
			$campaign_id = $_GET["campaign_id"];
			$this->smarty->assign("campaign_id", $campaign_id);
			$this->smarty->assign("current_location", $current_location);
			$this->smarty->assign("orderid", $orderid);
			$this->smarty->assign("token", $token);
			$this->smarty->assign("member_id", $this->member_id);
			$this->smarty->assign("member_name", $member_name);
			$this->smarty->assign("member_surname", $member_surname);
			$this->smarty->assign("member_firstname", $member_firstname);
			$this->smarty->assign("member_icname", $member_icname);
			$this->smarty->assign("ic_option", $member_icoption);
			$this->smarty->assign("ic_country", $member_iccountry);
			$this->smarty->assign("member_mobile", $member_mobile);
			$this->smarty->assign("member_country", $member_country);
			$this->smarty->assign("member_email", $member_email);
			$this->smarty->assign("member_password", $member_password);
			$this->smarty->assign("nric", $nric);
			$this->smarty->assign("dob", $dob);
			$this->smarty->assign("resarea", $resarea);
			$this->smarty->assign("filenric", $nric_file);
			$this->smarty->assign("filenricback", $nric_file_back);
			$this->smarty->assign("addressProof", $addressProof);
		}

		$campaign_id = $_GET["campaign_id"];
		// META Fb
		$fb_campaign_name = $this->db->field("select campaign_name from tcampaign where campaign_id='" . $campaign_id . "'");
		$fb_campaign_snippet = $this->db->field("select campaign_snippet from tcampaign where campaign_id='" . $campaign_id . "'");
		$fb_campaign_bg = $this->db->field("select campaign_background from tcampaign where campaign_id='" . $campaign_id . "'");
		$fb_campaign_desc = $this->db->field("select campaign_description from tcampaign where campaign_id='" . $campaign_id . "'");
		// Set Meta Tag

		$image_names = $this->db->field("select Image_Name from tcampaign where campaign_id='" . $campaign_id . "'");
		$image_name = explode("~", $image_names);

		$page_title = "Kapital Boost - crowdfunding of " . $fb_campaign_name . " campaign";
		$meta_keyword = $fb_campaign_snippet;
		$meta_desc = $fb_campaign_desc;
		$image_url = "https://kapitalboost.com/assets/images/campaign/" . $fb_campaign_bg;
		$this->_setMeta($page_title, $meta_keyword, $meta_desc, $image_url, '');

		$relatedC1 = $this->db->field("SELECT related_campaign_1 FROM tcampaign WHERE campaign_id = $campaign_id ");
		$relatedC2 = $this->db->field("SELECT related_campaign_2 FROM tcampaign WHERE campaign_id = $campaign_id ");
		$relatedC3 = $this->db->field("SELECT related_campaign_3 FROM tcampaign WHERE campaign_id = $campaign_id ");

		$relatedC1Id = explode("~", $relatedC1)[1];
		$relatedC2Id = explode("~", $relatedC2)[1];
		$relatedC3Id = explode("~", $relatedC3)[1];

		list($other1, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE campaign_id = $relatedC1Id");
		list($other2, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE campaign_id = $relatedC2Id");
		list($other3, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE campaign_id = $relatedC3Id");

		$pdfs = $content["pdfs"];
		$pdfsDes = $content["pdfs_description"];
		$pdfsArray = explode("~", $pdfs);
		$pdfsDesArray = explode("~", $pdfsDes);

		$this->smarty->assign("closedPw", "correct");
		$exRate = $this->ExRate();

		$totalFundingAmt = $content["total_funding_amt"];
		$content["total_funding_amt_id"] = $content["total_funding_amt_id"] * $exRate;
		$content["total_funding_amt"] = $content["total_funding_amt"] * $exRate;
		$currentFundingAmt = $content["curr_funding_amt"];

		if ($totalFundingAmt == $currentFundingAmt) {
			$content["curr_funding_amt_id"] = $content["total_funding_amt_id"];
		} else {
			$content["curr_funding_amt_id"] = $currentFundingAmt * $exRate;
		}

		$this->smarty->assign("exRate",$exRate);
		$this->smarty->assign("content", $content);
		$this->smarty->assign("image_name", $image_name);
		$this->smarty->assign("other1", $other1);
		$this->smarty->assign("other2", $other2);
		$this->smarty->assign("other3", $other3);
		$this->smarty->assign("pdfsArray", $pdfsArray);
		$this->smarty->assign("pdfsDesArray", $pdfsDesArray);

		$this->AddTrail("Visit Private Campaign Web", $this->member_id);

		// $content = $this->smarty->fetch('TPL_Campaign_Details.tpl');
		$content = $this->smarty->fetch('TPL_Private_Campaign_Details.tpl');
		return $content;
	}

	function Enquiry() {
		$name = $_POST['name'];
		$email = $_POST['email'];
		$project = $_POST['project'];
		$msg = $_POST['msg'];
		$token = $_POST['token'];
		$now = date("Y-m-d H-i-s");

		$simpan = "INSERT INTO tenquire(name, email, project, msg, token,date) VALUES ('$name', '$email', '$project', '$msg', '$token','$now')";
		$query = $this->db->exec($simpan);

		$subject = "Your enquiry was received";
		$message = "

		<html>
		<head>
		<title>Kapital Boost's Enquiry</title>
		</head>
		<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
		<div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
		<div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
		<!-- <img src='https://kapitalboost.com/email/logo.jpg'  style='margin: 3% 0 0 3%;width:30%;'/> -->
		<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
		Hi " . $name . ",
		</p>
		<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
		Thank you for your inquiry!
		</p>
		<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
		Our team is looking into it and will reply to you shortly. If your enquiry is urgent, you may call us at +65 9865 9648 from Monday to Friday during our office hours.
		</p>

		<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
		Talk to you soon,<br><br>Kapital Boost
		</p>
		</div>
		<div style='width:100%;height:7px;background-color:#00b0fd'></div>
		<div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
		<p style='font-size: 12px;color: white;text-align:center'>
		&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
		</p>
		</div>
		</div>
		</body>
		</html>

		";

		// $this->SendEmail(EMAIL_FROM, $email, "", $subject, $message, TRUE);
		$this->SendEmailSendGridOld(EMAIL_FROM, $email, "", $subject, $message, TRUE);

		$this->AddTrail("Equire Web", $this->member_id);

		$subject2 = "[INFO] Enquire";
		$message2 = "                           Enquire Campaign <br /><br />
		Name          : {$name}<br />
		Campaign Name : {$project}<br />
		Email         : {$email}<br />
		Messages      : {$msg}<br />
		<br />";

		// $this->SendEmail(EMAIL_FROM, "admin@kapitalboost.com", array(), $subject2, $message2, TRUE);
		$this->SendEmailSendGridOld(EMAIL_FROM, "arief@kapitalboost.com", array(), $subject2, $message2, TRUE);
	}

	function totalFundingUser() {
		$member_id = $_GET['member_id'];
		$campaign_id = $_GET['campaign_id'];
		$maxAmount = (float)$_GET['max_amount'];
		$amount = (float)$_GET['amount'];

		$sql = "SELECT SUM(`total_funding`) as 'total_funding', SUM(`total_funding_id`) as 'total_funding_id' FROM `tinv` WHERE member_id='$member_id' AND `campaign_id`='$campaign_id'";
		list($data, ) = $this->db->singlearray($sql);
		$remaining = $maxAmount - (float)$data['total_funding'];

		if ($remaining <= 0) {
			echo json_encode(array("status" => 200, "error" => 1, "message" => "Sorry, your investment amount has reached 30% of total amount allowed.", "total_fund" => $data['total_funding']));
		} else if ($remaining < $amount) {
			echo json_encode(array("status" => 200, "error" => 1, "message" => "Your maximum additional investment in this campaign is SGD {$remaining}. Each investor should invest no more than 30% of total amount.", "total_fund" => $data['total_funding']));
		}else{
			echo json_encode(array("status" => 200, "error" => 0, "message" => "", "total_fund" => $data['total_funding']));
		}

		exit();
	}

}

?>
