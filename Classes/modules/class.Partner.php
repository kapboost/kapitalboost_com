<?php

class Partner extends Front {

       function LoadList() {
              list($content, ) = $this->db->multiarray("SELECT * FROM tpartner WHERE enabled = 1 ORDER BY insert_dt DESC");

              $this->smarty->assign("content", $content);
			  
			  $this->AddTrail("Visit Partner Web", $this->member_id);

              $content = $this->smarty->fetch('TPL_Partners.tpl');
              return $content;
       }

}

?>