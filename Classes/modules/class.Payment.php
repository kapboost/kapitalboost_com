<?php

include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

class Payment extends Front {

       function genRndString($length = 20, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
              if ($length > 0) {
                     $len_chars = (strlen($chars) - 1);
                     $the_chars = $chars{rand(0, $len_chars)};
                     for ($i = 1; $i < $length; $i = strlen($the_chars)) {
                            $r = $chars{rand(0, $len_chars)};
                            if ($r != $the_chars{$i - 1})
                                   $the_chars .= $r;
                     }

                     return $the_chars;
              }
       }

       function Paypal() {


              $item_no = $_REQUEST['item_number'];
              $item_transaction = $_REQUEST['tx'];
              $item_price = $_REQUEST['amt'];
              $item_currency = $_REQUEST['cc'];
              $cm = urldecode($_REQUEST["cm"]);
              
              $ids = explode('|', $cm);
              $campaign_name = $ids[0];
              $campaign_name = str_replace("+", " ", $campaign_name);
              $member_name = $ids[1];
              $member_email = $ids[2];
              $member_country = $ids[3];
              $campaign_owner = $ids[4];
              $tokenuser = $ids[5];
              $project_type = $ids[6];
              $close = $ids[7];
              $firstname = $ids[9];
              $paypal_campaign_id = $_REQUEST['item_number'];

              $this->smarty->assign("paypal_campaign_id", $paypal_campaign_id);

              $this->smarty->assign("campaign_id", $paypal_campaign_id);
              // Rechecking the product price and currency details

              $paypal_msg = "Payment Success";
              //$simpan = "INSERT INTO tinv(nama, email, country, campaign, tipe, total_funding, date, status, owner, bukti, tx_code, token, bank, project_type, closing_date) VALUES ('$member_name', '$member_email', '$member_country', '$campaign_name', 'Paypal', '$item_price', NOW(), 'Paid', '$campaign_owner', '','$item_transaction', '$tokenuser', '', '$project_type','$close')";
              $simpan = "UPDATE tinv SET status = 'Paid' WHERE token_pp = '$tokenuser'";
              $this->db->exec($simpan);

              $subject = "Payment Completed";
              $message = "
<html>
       <head>
              <title>Kapital Boost</title>
       </head>
       <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
              <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
                     <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
                            <img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
                            <p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
                                   Dear " . $member_name . ",
                            </p>
                            <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                   Thank you for your generous donation of <b>SGD " . $item_price . "</b> for the <b>" . $campaign_name . "</b> campaign.
                            </p>
                            
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   May your kindness be rewarded in multiplefold.
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   Best Regards,<br><br>Kapital Boost
                            </p>
                     </div>
                     <div style='width:100%;height:7px;background-color:#00b0fd'></div>
                     <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                            <p style='font-size: 12px;color: white;text-align:center'>
                                   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
                            </p>
                     </div>
              </div>
       </body>
</html>                     
      ";

              $this->SendEmailSendGridOld(EMAIL_FROM, $member_email,  'admin@kapitalboost.com', $subject, $message, TRUE);


              //Email Here

              $campaign_subtype = $this->db->field("select campaign_subtype from tcampaign where campaign_id='" . $paypal_campaign_id . "'");
              $campaign_subtype_ship = $this->db->field("select campaign_subtype_ship from tcampaign where campaign_id='" . $paypal_campaign_id . "'");

              $this->smarty->assign("member_email", $member_email);
              $this->smarty->assign("project_type", $project_type);
              $this->smarty->assign("campaign_subtype", $campaign_subtype);
              $this->smarty->assign("campaign_subtype_ship", $campaign_subtype_ship);
              $this->smarty->assign("campaign_name", $campaign_name);
              $this->smarty->assign("item_price", $item_price);
              $this->smarty->assign("paypal_msg", $paypal_msg);
              $this->smarty->assign("error_msg", $error_msg);
			  
			  $this->AddTrail("Paypal Payment Web", $this->member_id);


              $content = $this->smarty->fetch('TPL_Investor.tpl');
              return $content;
       }

       function BankTransfer() {

              //date_default_timezone_set('Asia/Singapore');

              $funding = $_POST['amount'];
              $ids = explode('|', $_POST['custom']);
              $campaign_name = $ids[0];
              $member_name = $ids[1];
              $member_email = $ids[2];
              $member_country = $ids[3];
              $campaign_owner = $ids[4];
              $token_user = $ids[5];
              $project_type = $ids[6];
              $close = $ids[7];
              $firstname = $ids[9];

              $pp_token = $_POST["token"];
              $eks = "SELECT id FROM tinv WHERE tx_code='$token_user'";
              $query = $this->db->field($eks);
              $checktoken = $query;

              $e_campaign_id = $_POST["campaign_id"];

              $this->smarty->assign("e_campaign_id", $e_campaign_id);



              //Rechecking the product price and currency details
              if ($checktoken > 0) {
                     $error_msg = "Token Mismatch!";
              } else {

                     $bank_msg = 'Thank you for your interest of investment in the ' . $campaign_name . ' crowdfunding deal. We shall get back to you shortly with the financing agreement and payment instruction. If you have any questions, please email us at admin@kapitalboost.com';
                     $token = $this->genRndString();
                     $time = date("Y-m-d h:i:s");

                     $simpan = "INSERT INTO tinv(closing_date,date,nama, email, country, campaign, tipe, total_funding, status, owner, bukti, tx_code, token, bank, project_type, token_pp) VALUES ('$close','$time','$member_name', '$member_email', '$member_country', '$campaign_name', 'Bank Transfer', '$funding', 'Unpaid', '$campaign_owner', '','$token_user','$token', '', '$project_type', '$pp_token')";
                     $query = $this->db->exec($simpan);
                     if ($project_type == "donation") {
                            $seperate = "donation";
                     }else{
                            $seperate = "commitment";
                     }
                            $message = "

<html>
       <head>
              <title>Kapital Boost</title>
       </head>
       <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
              <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
                     <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
                            <img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
                            <p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
                                   Dear " . $member_name . ",
                            </p>
                            <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                   Thank you for your ".$seperate." of <b>SGD " . $funding . "</b> to the <b>" . $campaign_name . "</b> project.
                            </p>

                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   Please proceed to make payment to Kapital Boost according to the instructions below.
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   <b>Payment instructions</b>
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   (i) Please transfer to the following account:
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   Account name: <b>Kapital Boost Pte Ltd</b><br>
                                   Bank name: <b>Overseas Chinese Banking Corp. (OCBC)</b><br>
                                   Account number: <b>6232 3373 1001</b><br>
                                   SWIFT code: <b>OCBCSGSG</b><br>
                                   Bank code: <b>7339</b><br>
                            </p>

                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   (ii) Once transferred, please submit to us the proof of payment by clicking the button below.
                            </p>
                            <a href='https://kapitalboost.com/dashboard/confirm/bank?token=" . $token . "&campaign_name=" . $campaign_name . "&action=confirm?type=" . $project_type . "&amount=" . $funding . "&campaign_id=" . $e_campaign_id . "&campaign_name=" . $campaign_name . "' 
                               style='background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;'>
                                   Confirm Payment
                            </a>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   To check on the payment status of your ".$seperate.", click on <a href='https://kapitalboost.com/dashboard/payment-pending' style='text-decoration:none'>Payment</a> under the Dashboard.
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   If you have questions, please email <a href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
                            </p>
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   Best Regards,<br><br>Kapital Boost
                            </p>
                     </div>
                     <div style='width:100%;height:7px;background-color:#00b0fd'></div>
                     <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                            <p style='font-size: 12px;color: white;text-align:center'>
                                   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
                            </p>
                     </div>
              </div>
       </body>
</html>
            ";
                     
                     
                     $subject = "Payment Confirmation";


                     $this->SendEmailSendGridOld(EMAIL_FROM, $member_email,  'admin@kapitalboost.com', $subject, $message, TRUE);
              }
			  
			  $this->AddTrail("Bank Transfer Payment Web", $this->member_id);
			  
              $this->smarty->assign("member_email", $member_email);
              $this->smarty->assign("project_type", $project_type);
              $this->smarty->assign("funding", $funding);
              $this->smarty->assign("campaign_name", $campaign_name);
              $this->smarty->assign("bank_msg", $bank_msg);
              $this->smarty->assign("error_msg", $error_msg);
              $content = $this->smarty->fetch('TPL_Investor.tpl');
              return $content;
       }

}

?>
