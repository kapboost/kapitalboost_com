<?php

include_once(DOC_ROOT . 'Classes/class.phpmailer.php');
include_once(DOC_ROOT . 'php-graph-sdk-5.5/src/Facebook/autoload.php');

class Member extends Front {

   function LoginForm() {
      $reply['status'] = 0;

      if (isset($_POST['member_username'])) {

         $username = $_POST["member_username"];
         $password = $_POST['member_password'];
         $tanggal = date("Y-m-d H:i:s");
         $password = md5($password);
         $sPassword = $this->db->field("SELECT member_password FROM tmember WHERE member_email='$username' || member_username='$username'");

         if ($sPassword == "0") { // No account
            $error_msg = "Email or username is invalid.";
            $reply['status'] = 0;
         } else {
            $validated = $this->db->field("SELECT Validated FROM tmember WHERE  member_email='$username' || member_username='$username' ");
            if ($validated == 0) {
               $reply['status'] = 2;
            } else {
               if ($password !== $sPassword) {
                  $error_msg = "Password is incorrect.";
                  $reply['status'] = 0;
               } else {
                  $member_id = $this->db->field("SELECT member_id FROM tmember WHERE Validated = 1 && ( member_email='$username' || member_username='$username') && member_password='$password' ");
                  $firstname = $this->db->field("SELECT member_firstname FROM tmember WHERE Validated = 1 && ( member_email='$username' || member_username='$username') && member_password='$password' ");
                  $mobileno = $this->db->field("SELECT member_mobile_no FROM tmember WHERE Validated = 1 && ( member_email='$username' || member_username='$username') && member_password='$password' ");
                  $location = $this->db->field("SELECT current_location FROM tmember WHERE Validated = 1 && ( member_email='$username' || member_username='$username') && member_password='$password'");
                  $_SESSION['cookie']['member_id'] = $member_id;
                  $this->AddTrail("Login Web", $member_id);
                  $_SESSION["MemberEmail"] = $username;
                  $_SESSION["MemberName"] = $firstname;
                  $_SESSION["MemberId"] = $member_id;
                  $_SESSION["MemberPhone"] = $mobileno;
                  $_SESSION["MemberCountry"] = $location;

                  $reply['status'] = 1;

                  if ($_POST['redirect'] != '') {
                     $reply['redirect'] = $_POST['redirect'];
                  } else {
                     $reply['redirect'] = SERVER_BASE;
                  }
               }
            }
         }
      }

      $reply['error'] = $error_msg;

      echo json_encode($reply);
      exit;
   }

   function Fblogin() {

      if ($this->member_id != 0) {

      } else {
         $app_id = '261151691029080';
         $app_secret = '16e8e0eab10cbaaa20fccf2f7a0b2e00';

         $fb = new Facebook\Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => 'v2.9',
         ]);
         $helper = $fb->getJavaScriptHelper();
         $token = $_POST["token"];

         try {
            $accessToken = $helper->getAccessToken();
            $response = $fb->get('/me?fields=id,name,first_name,last_name,email,gender,location', $token);
         } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
         } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
         }
         if (!isset($accessToken)) {
            echo 'No cookie set or no OAuth data could be obtained from cookie.';
            exit;
         }

         $user = $response->getGraphUser();

         $status = 9;

         $email = $user["email"];
         $location = strtoupper($user["location"]["name"]);
         $fId = $user["id"];
         $firstname = $user["first_name"];
         $lastname = $user["last_name"];
         $gender = $user["gender"];
         if ($gender == "male") {
            $gender = "M";
         } else {
            $gender = "F";
         }
         $now = date("Y-m-d H-i-s");

         $member_id = $this->db->field("SELECT member_id FROM tmember WHERE  member_email='$email'");


         if ($member_id > 0) {
            $sql = "UPDATE tmember SET f_id = '$fId', Validated = 1 WHERE member_email = '$email'";
            $this->db->exec($sql);
            $_SESSION["cookie"]["member_id"] = $member_id;
            $firstname = $this->db->field("SELECT member_firstname FROM tmember WHERE Validated = 1 && ( member_email='$email' || member_username='$email') ");
            $mobileno = $this->db->field("SELECT member_mobile_no FROM tmember WHERE Validated = 1 && ( member_email='$email' || member_username='$email')");
            $location = $this->db->field("SELECT current_location FROM tmember WHERE Validated = 1 && ( member_email='$email' || member_username='$email')");
            $_SESSION["MemberEmail"] = $email;
            $_SESSION["MemberName"] = $firstname;
            $_SESSION["MemberId"] = $member_id;
            $_SESSION["MemberPhone"] = $mobileno;
            $_SESSION["MemberCountry"] = $location;
            $this->AddTrail("Login FB Web", $member_id);
            // echo "'<script>console.log(\"session email : ".$_SESSION["MemberEmail"]."\")</script>'";
            $status = 1;
         } else {
            $sql = "INSERT INTO tmember(f_id,Validation_Code,Validated,member_email,member_firstname,member_surname,member_gender,insert_dt,current_location) VALUES ('$fId','VerifiedByFB',1,'$email','$firstname','$lastname','$gender','$now','$location' )";
            $member_id = $this->db->exec1($sql);
            $_SESSION["cookie"]["member_id"] = $member_id;
            $firstname = $this->db->field("SELECT member_firstname FROM tmember WHERE Validated = 1 && ( member_email='$email' || member_username='$email') ");
            $mobileno = $this->db->field("SELECT member_mobile_no FROM tmember WHERE Validated = 1 && ( member_email='$email' || member_username='$email')");
            $location = $this->db->field("SELECT current_location FROM tmember WHERE Validated = 1 && ( member_email='$email' || member_username='$email')");
            $_SESSION["MemberEmail"] = $email;
            $_SESSION["MemberName"] = $firstname;
            $_SESSION["MemberId"] = $member_id;
            $_SESSION["MemberPhone"] = $mobileno;
            $_SESSION["MemberCountry"] = $location;
            // echo "'<script>console.log(\"session email2 : ".$_SESSION["MemberEmail"]."\")</script>'";
            $this->AddTrail("Login FB Web", $member_id);
            $status = 1;
            $this->MailChimp($email, $firstname, $lastname, $location);
         }



         echo json_encode($status);
      }
      exit;
   }

   function CheckUName(){
      $uname = $this->test_input($_POST["uname"]);

      $allUName = $this->db->field("SELECT member_username FROM tmember WHERE member_username = '$uname'");
      if($allUName == "0"){
         echo 0;
      }else{
         echo 1;
      }

      exit;
   }

   function Reactivate() {
      $email = $_POST["email"];
      $member_id = $this->db->field("SELECT member_id FROM tmember WHERE member_email = '$email'");
      if ($member_id == 0) {
         $reply["status"] = 0;
      } else {
         $validated = $this->db->field("SELECT Validated FROM tmember WHERE member_email = '$email'");
         if ($validated == 1) {
            $reply["status"] = 1;
         } else {
            $validationCode = $this->db->field("SELECT Validation_Code FROM tmember WHERE member_email = '$email'");
            $firstName = $this->db->field("SELECT member_firstname FROM tmember WHERE member_email = '$email'");

            $subject = "Account Activation";
            // $message = "

            // <html>
            // <head>
            // <title>Kapital Boost</title>
            // </head>
            // <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
            // <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
            // <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
            // <img src='https://kapitalboost.com/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
            // <p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
            // Hi " . $firstName . ",
            // </p>
            // <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
            // Your account is almost ready! To activate, please click below.
            // </p>
            // <a href='https://kapitalboost.com/verify/$validationCode' style='display:inline-block;background: #00b0fd;outline: none;border: none;padding: 1% 3%;margin: 0 0 1% 3%;font-size: 16px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;'>
            // ACTIVATE MY ACCOUNT
            // </a>
            // <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
            // If you have any questions, feel free to contact us at <a href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
            // </p>
            // <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
            // Best Regards,<br><br>Kapital Boost
            // </p>
            // </div>
            // <div style='width:100%;height:7px;background-color:#00b0fd'></div>
            // <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
            // <p style='font-size: 12px;color: white;text-align:center'>
            // &copy; COPYRIGHT 2017 - KAPITAL BOOST PTE LTD
            // </p>
            // </div>
            // </div>
            // </body>
            // </html>


            // ";

            $message2 = '<table align="center" bgcolor="#ecf0f1" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
            <td align="center">
            <!--SECTION TABLE-680-->

            <table align="center" border="0" bgcolor="#ffffff" cellpadding="0" cellspacing="0" class="display-width" width="680">
            <tbody>
            <tr>
            <td align="center" style="padding:0 30px;">
            <!--SECTION TABLE-600-->

            <table align="center" border="0" cellpadding="0" cellspacing="0" class="display-width" width="600">
            <tbody>
            <tr>
            <td height="60"></td>
            </tr>
            <tr>
            <td align="left">
            <table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%">
            <tbody>
            <tr>
            <td align="left" class="MsoNormal" style="color:#666666;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; line-height:24px;">
            <p>
            Hi, '. $name .' <br /><br />
            Your account is almost ready! To activate, please click below.
            </p>
            </td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            <tr>
            <td height="30"></td>
            </tr>
            <tr>
            <td align="center" valign="middle" class="button-width">
            <table align="center" bgcolor="#56a1d9" border="0" cellspacing="0" cellpadding="0" class="display-button" width="50%"> <!-- USING TABLE AS BUTTON -->
            <tr>
            <td align="center" valign="middle" class="MsoNormal" width="50%" style="color:#ffffff;font-family:\'Segoe UI\',sans-serif,Arial,Helvetica,Lato;font-size:14px;font-weight:bold;letter-spacing:1px;padding:16px 16px;text-transform:capitalize;">
            <a href="https://kapitalboost.com/verify/'. $validationCode .'" style="color:#ffffff;text-decoration:none;margin: 0 auto;">Activate My Account</a>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
            <td height="30"></td>
            </tr>
            <tr>
            <td align="left">
            <table align="left" border="0" cellpadding="0" cellspacing="0" class="display-width" width="100%">
            <tbody>
            <tr>
            <td align="left" class="MsoNormal" style="color:#666666;font-family:Segoe UI, Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size:14px; line-height:24px;">
            <p>
            If you have any questions, feel free to contact us at <a href="mailto:support@kapitalboost.com" style="text-decoration:none;">support@kapitalboost.com</a>.
            </p>
            <p>
            Your account is almost ready! To activate, please click below.
            </p>
            </td>
            </tr>
            </tbody>
            </table>
            </td>
            </tr>
            <tr>
            <td height="60"></td>
            </tr>
            </tbody>
            </table>

            <!--SECTION TABLE-600 ENDS-->
            </td>
            </tr>
            </tbody>
            </table>
            <!--SECTION TABLE-680 ENDS-->
            </td>
            </tr>
            </tbody>
            </table>';

            // $this->SendEmail(EMAIL_FROM, $email, 'admin@kapitalboost.com', $subject, $message, TRUE);
            $this->SendEmailSendGrid(EMAIL_FROM, $profile['member_email'], 'admin@kapitalboost.com', $subject, $message2, TRUE);
            $this->AddTrail("Activate Account Web", $member_id);

            $reply["status"] = 2;
         }
      }
      echo json_encode($reply);
      exit;
   }

   function MailList(){

      $email = $_POST["email"];

      $this->MailChimp($email, "", "", "");

      exit;
   }

   function LoginFormBank() {
      $reply['status'] = false;

      if ($_POST) {

         //Manual Sign In
         if ($_POST['member_username'] == "") {
            $error_msg = "Email cannot be empty.";
         }

         if ($_POST['member_password'] == "") {
            $error_msg = "Password cannot be empty.";
         }

         if (!$error_msg) {
            //check exist with password
            $member_id = $this->db->field("SELECT member_id FROM tmember WHERE member_email='" . $_POST['member_username'] . "' AND member_password='" . $_POST['member_password'] . "' ");

            $this->AddTrail("Login FormBank Function Web", $member_id);
            if ($member_id > 0) {
               $_SESSION['cookie']['member_id'] = $member_id;

               $reply['status'] = true;

               if ($_POST['redirect'] != '') {
                  $reply['redirect'] = $_POST['redirect'];
               } else {
                  $reply['redirect'] = SERVER_BASE;
               }
            } else {
               $error_msg = "Email/Username maybe wrong, please try again.";
            }
         }
      }

      $reply['error'] = $error_msg;

      echo json_encode($reply);

      exit;
   }

   function ForgotPasswordForm() {
      $error_msg = array();
      $success_msg = "";

      if ($_POST) {

         if ($_POST['member_email'] == "") {
            $error_msg[] = "Email cannot be empty.";
         }

         if (!$error_msg) {
            //check exist with password

            $member_email = $_POST["member_email"];
            $member_id = $this->db->field("SELECT member_id FROM tmember WHERE (Validated = 1 && member_email='$member_email') ");

            if ($member_id > 0) {

               list($profile, $c_profile) = $this->db->singlearray("SELECT * FROM tmember WHERE member_email='$member_email'");

               $encrypt = md5(1290 * 3 + $member_id);
               $link_activate = "https://kapitalboost.com/reset/addpass?encrypt=$encrypt&action=reset";
               $name = $profile['member_firstname'];

               $subject = $this->db->field("SELECT title FROM temail WHERE name = 'reset-password'");

               $message2 = $this->db->field("SELECT content FROM temail WHERE name = 'reset-password'");
               $message2 = str_replace('{user_name}', $name, $message2);
               $message2 = str_replace('{link_activate}', $link_activate, $message2);

               $this->SendEmailSendGrid(EMAIL_FROM, $profile['member_email'], 'admin@kapitalboost.com', $subject, $message2, TRUE);
               $this->AddTrail("Reset Password Success Web", $member_id);
               $success_msg = "Please check your email for instructions to change your password";
            } else {
               $error_msg[] = "We do not recognize that email address. Please enter the email address that was used to register with Kapital Boost. For further assistance, you may contact us at <a href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>";
               $this->AddTrail("Reset Password Fail Web", $member_id);
            }
         }
         $this->smarty->assign($_POST);
      }

      $this->AddBreadCrumb('Forgot Password');
      $this->smarty->assign('breadcrumb', $this->LoadBreadCrumb());

      $this->smarty->assign("success_msg", $success_msg);
      $this->smarty->assign("error_msg", $error_msg);
      $this->smarty->assign('redirect', $_REQUEST['redirect']);
      $content = $this->smarty->fetch('TPL_Member_ForgotPassword.tpl');
      return $content;
   }

   function RegisterForm() {
      $error_msg = array();
      $success_msg = "";

      if ($_POST) {
         if ($_POST['member_email'] == "") {
            $error_msg = "Email cannot be empty.";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         }

         if ($_POST['member_password'] == '') {
            $error_msg = "Password cannot be blank";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         }
         // if ($_POST['member_surname'] == '') {
         // $error_msg = "Last Name cannot be blank";
         // echo "<script>alert(\"".$error_msg."\");</script>";
         // echo "<script>window.history.back();</script>";
         // }
         if ($_POST['member_firstname'] == '') {
            $error_msg = "First Name cannot be blank";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         }
         if ($_POST['member_username'] == '') {
            $error_msg = "Username cannot be blank";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         }
         // if ($_POST['member_country'] == '') {
         // $error_msg = "Country cannot be blank";
         // echo "<script>alert(\"".$error_msg."\");</script>";
         // echo "<script>window.history.back();</script>";
         // }
         if ($_POST['member_password'] != $_POST['retype_password']) {
            $error_msg = "Your Password does not match";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         }
         $member_email = $_POST["member_email"];
         $member_username = $_POST["member_username"];
         //check exist with password
         $member_id = $this->db->field("SELECT COUNT(*) FROM tmember WHERE  member_email='$member_email'");
         $member_uname = $this->db->field("SELECT COUNT(*) FROM tmember WHERE  member_username='$member_username'");

         if ($member_id > 0) {
            $error_msg = "Your email already registered. Please login.";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         } else if ($member_uname > 0) {
            $error_msg = "Your username already taken!";
            echo "<script>alert(\"".$error_msg."\");</script>";
            echo "<script>window.history.back();</script>";
         }

         if (!$error_msg) {
            $response = $_POST["g-recaptcha-response"];
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $data = array(
               'secret' => '6LeKSlAUAAAAAChAjmtKLimNg65KcFs-UhByJdot',
               'response' => $_POST["g-recaptcha-response"]
            );
            $options = array(
               'http' => array (
                  'method' => 'POST',
                  'content' => http_build_query($data)
               )
            );
            $context  = stream_context_create($options);
            $verify = file_get_contents($url, false, $context);
            $captcha_success=json_decode($verify);

            if ($captcha_success->success==false) {

               echo "<script>alert(\"Wrong Captcha\");</script>";
               echo "<script>window.history.back();</script>";

            } else if ($captcha_success->success==true) {

               $member_username = $_POST["member_username"];
               $member_name = $this->db->field("SELECT COUNT(*) FROM tmember WHERE  member_username='$member_username'");

               if ($member_name > 0) {
                  $error_msg = "username";
                  echo "<script>alert(\"".$error_msg."\");</script>";
                  echo "<script>window.history.back();</script>";

               } else {
                  $pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';

                  $emailaddress = $_POST['member_email'];

                  if (preg_match($pattern, $emailaddress) !== 1) {
                     // emailaddress is not valid
                     $error_msg = "Email address is not valid";

                     echo "<script>alert(\"".$error_msg."\");</script>";
                     echo "<script>window.history.back();</script>";
                  } else {
                     $member_email = $_POST['member_email'];
                     $member_first_name = $_POST['member_firstname'];
                     $lastname = isset($_POST['member_surname']) ? $_POST['member_surname'] : '';
                     $password = $_POST['member_password'];
                     $password = md5($password);
                     $username = $_POST['member_username'];
                     $nationality = isset($_POST['nationality']) ? $_POST['nationality'] : '';
                     $country=isset($_POST['member_country']) ? $_POST['member_country'] : '';
                     $vpn=isset($_POST['vpn']) ? $_POST['vpn'] : '';
                     $title = isset($_POST['member_gender']) ? $_POST['member_gender'] : '';
                     $how_you_know = isset($_POST['how_you_know']) ? $_POST['how_you_know'] : '0';
                     if($title == 'mr'){
                        $gender = "M";
                     }else{
                        $gender = "F";
                     }


                     $validationCode =  $vpn . md5($member_email) . time();
                     $now = date("Y-m-d H-i-s");
                     $sql = "INSERT INTO tmember(member_username,Validation_Code,member_firstname,member_email,member_surname,member_password,current_location,member_title,insert_dt,member_gender,how_you_know) VALUES ('$username','$validationCode','$member_first_name', '$member_email','$lastname','$password','$country','$title','$now','$gender','$how_you_know')";

                     // echo "'<script>console.log(\"sql : ".$sql."\")</script>'";
                     $this->db->exec($sql);

                     // $subject = "Account Activation";
                     $link_activate = "https://kapitalboost.com/verify/$validationCode";


                     $subject = $this->db->field("SELECT title FROM temail WHERE name = 'register-activation'");
                     $message2 = $this->db->field("SELECT content FROM temail WHERE name = 'register-activation'");
                     $message2 = str_replace('{user_name}', $member_first_name, $message2);
                     $message2 = str_replace('{link_activate}', $link_activate, $message2);


                     $this->SendEmailSendGrid(EMAIL_FROM, $member_email, 'admin@kapitalboost.com', $subject, $message2, TRUE);

                     // $this->SendEmail(EMAIL_FROM, $member_email, 'admin@kapitalboost.com', $subject, $message, TRUE);
                     $this->AddTrail("Register Account Web", $member_id);

                     $error_msg = "You have successfully registered";

                     $this->MailChimp($member_email, $member_first_name, $lastname, $country);

                     $content = $this->smarty->fetch('TPL_Register_Success.tpl');


                  }
               }
            }

         }

         // $this->smarty->assign($_POST);
      }

      // $this->smarty->assign("error_msg", $error_msg);
      // $reply['error'] = $error_msg;

      // echo json_encode($reply);

      // exit;
      //$content = $this->smarty->fetch('TPL_Member_Register_Part_1.tpl');
      // $content = $this->smarty->fetch('TPL_Member_Login.tpl');
      return $content;
   }



   function Verification() {
      $code = $_GET["code"];
      $validated = $this->db->field("SELECT Validated FROM tmember WHERE Validation_Code = '$code'");
      if ($validated == '0') {
         $sql = "UPDATE tmember SET Validated = 1 WHERE Validation_Code = '$code'";
         $this->db->exec($sql);

         $member_id = $this->db->field("SELECT member_id FROM tmember WHERE Validation_Code = '$code'");
         $username = $this->db->field("SELECT member_username FROM tmember WHERE Validation_Code = '$code'");
         $email = $this->db->field("SELECT member_email FROM tmember WHERE Validation_Code = '$code'");
         $_SESSION['cookie']['member_id'] = $member_id;
         $this->AddTrail("Verification Web", $member_id);
         header("Location: https://kapitalboost.com?email=$email&username=$username");
         echo "<script>
         fbq('track', 'CompleteRegistration');
         </script>";
         exit;
      } else {
         header('Location: https://kapitalboost.com?email=failed&username=failed');
         exit;
      }
   }



   function LoadProfile() {
      $error_msg = array();

      if ($this->member_id == 0 || $this->member_id == '') {
         header("Location: " . SERVER_BASE . "member/login");
         exit;
      }

      $error_msg = array();
      $success_msg = "";

      if ($_POST) {


         if ($_POST['member_email'] == '') {
            $error_msg[] = "<b>Email</b> cannot be blank";
         }

         if ($_POST['member_password'] != '') {
            if ($_POST['member_password'] != $_POST['retype_password']) {
               $error_msg[] = "<b>Your Password</b> does not match";
            }
         }


         if ($_POST['member_givenname'] == '') {
            $error_msg[] = "<b>Full Name</b> cannot be blank";
         }

         if ($_POST['member_contact_no'] == '') {
            $error_msg[] = "<b>Contact</b> cannot be blank";
         }

         if ($_POST['member_street_1'] == '') {
            $error_msg[] = "<b>Address</b> cannot be blank";
         }

         if ($_POST['member_city'] == '') {
            $error_msg[] = "<b>City</b> cannot be blank";
         }

         if ($_POST['member_state'] == '') {
            $error_msg[] = "<b>State</b> cannot be blank";
         }

         if ($_POST['member_country'] == '') {
            $error_msg[] = "<b>Country</b> cannot be blank";
         }

         if ($_POST['member_postal_code'] == '') {
            $error_msg[] = "<b>Postal Code</b> cannot be blank";
         }

         if ($_POST['member_contact_no'] == '') {
            $error_msg[] = "<b>Contact No</b> cannot be blank";
         }

         if ($_POST['member_mobile_no'] == '') {
            $error_msg[] = "<b>Mobile No</b> cannot be blank";
         }

         if (!$error_msg) {

            $member_birthday_array = explode("/", $_POST['member_birthday']);
            $_POST['member_birthday'] = "$member_birthday_array[2]-$member_birthday_array[1]-$member_birthday_array[0]";
            $conn = $this->db->GetConn();

            $password = "";

            if (!empty($_POST['member_password'])) {
               $password = "member_password='" . mysqli_escape_string($conn, $_POST['member_password']) . "',";
            }

            $rs = $this->db->exec("UPDATE tmember SET
               member_email='" . mysqli_escape_string($conn, $_POST['member_email']) . "',
               {$password}
               member_givenname='" . mysqli_escape_string($conn, $_POST['member_givenname']) . "',
               member_birthday='" . mysqli_escape_string($conn, $_POST['member_birthday']) . "',
               member_street_1='" . mysqli_escape_string($conn, $_POST['member_street_1']) . "',
               member_city='" . mysqli_escape_string($conn, $_POST['member_city']) . "',
               member_state='" . mysqli_escape_string($conn, $_POST['member_state']) . "',
               member_country='" . mysqli_escape_string($conn, $_POST['member_country']) . "',
               member_postal_code='" . mysqli_escape_string($conn, $_POST['member_postal_code']) . "',
               member_contact_no='" . mysqli_escape_string($conn, $_POST['member_contact_no']) . "',
               member_mobile_no='" . mysqli_escape_string($conn, $_POST['member_contact_no']) . "',
               promotion='" . mysqli_escape_string($conn, $_POST['promotion']) . "',
               insert_dt=NOW()
               WHERE member_id='" . $this->member_id . "'
               ");

               if ($rs) {
                  $member_id = $this->member_id;
                  $this->AddTrail("Load Profil Save Web", $member_id);

                  $success_msg = "Your Profile Already Updated";
               } else {
                  $error_msg[] = "Your Profile Failed to Update";
               }

               //header("Location: /ohbabynme/member/profile");
               //exit;
            } else {
               $this->smarty->assign($_POST);
            }
         }

         list($detail, ) = $this->db->singlearray("SELECT * FROM tmember WHERE member_id='" . $this->member_id . "'");

         $this->smarty->assign($detail);

         if (isset($_SESSION['success_msg']) && !empty($_SESSION['success_msg'])) {
            $success_msg = $_SESSION['success_msg'];

            unset($_SESSION['success_msg']);
         }

         list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");
         list($nationality, ) = $this->db->multiarray("SELECT * FROM tnationality ORDER BY nationality ASC");
         $this->AddTrail("Load Profil Web", $member_id);

         $sidebar = $this->LoadSidebar(false, 'profile');
         $sidebar_mobile = $this->LoadSidebar(true, 'profile');

         $this->AddBreadCrumb('Member Profile');
         $this->smarty->assign('breadcrumb', $this->LoadBreadCrumb());

         $this->smarty->assign("sidebar", $sidebar);
         $this->smarty->assign("sidebar_mobile", $sidebar_mobile);
         $this->smarty->assign("country", $country);
         $this->smarty->assign("hello", "goodbye");
         $this->smarty->assign("nationality", $nationality);
         $this->smarty->assign('member_title_options', $this->config['member_title_options']);
         $this->smarty->assign("error_msg", $error_msg);
         $this->smarty->assign("success_msg", $success_msg);
         $content = $this->smarty->fetch('TPL_Member_Profile.tpl');
         return $content;
      }

      function LogoutForm() {
         $member_id = $this->member_id;
         $this->AddTrail("Logout Web", $member_id);
         unset($_SESSION['cookie']);
         session_destroy();

         echo '<script>
         window.location.href = "' . SERVER_BASE . '";
         </script>';

         exit;

         //session_destroy();
         //header("Refresh: 0; url=index.php?active=Home", true, 302);
         //$redirect = header(SERVER_BASE.'home');
         //return $redirect;
         //exit;
      }

   }

   ?>
