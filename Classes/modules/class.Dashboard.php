<?php

class Dashboard extends Front {

	function PaymentT() {
		$now = date("Y-m-d H-i-s");

		if ($this->member_id > 0) {
			$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
			$member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
			$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");

			$this->smarty->assign("member_id", $this->member_id);
			$this->smarty->assign("member_name", $member_name);
			$this->smarty->assign("member_country", $member_country);
			$this->smarty->assign("member_email", $member_email);
			$campaign = $this->db->field("select campaign from tinv WHERE id='59'");

			$sql = "SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expriy_date,release_date) AS duration FROM tcampaign WHERE enabled=1 && campaign_name='" . $campaign . "'";
			list($content, ) = $this->db->multiarray($sql);

			$this->smarty->assign("content", $content);
			list($list, ) = $this->db->multiarray("SELECT * FROM tinv WHERE email='$member_email'  ORDER BY date DESC");
			list($hancok, ) = $this->db->multiarray("SELECT * FROM tcampaign WHERE enabled=1 && campaign_name='Hikmatus Sholawat'");
		} else {
			$error_msg = "You dont have any campaign invest!";
		}

		$this->smarty->assign('list', $list);
		$this->smarty->assign('hancok', $hancok);
		$this->smarty->assign('error_msg', $error_msg);
		$content = $this->smarty->fetch('TPL_Dashboard.tpl');

		return $content;
	}

	function PaymentPending() {
		$id = $this->member_id;
		list($listPP, ) = $this->db->multiarray("SELECT * FROM tpaymentpending WHERE member_id='$id' ORDER BY ppid DESC");

		$username = $this->db->field("select member_username from tmember where member_id='" . $id . "'");
		$lastname = $this->db->field("select member_surname from tmember where member_id='" . $id . "'");
		$phone = $this->db->field("select member_mobile_no from tmember where member_id='" . $id . "'");
		$member_email = $this->db->field("select member_email from tmember where member_id='" . $id . "'");
		$member_country = $this->db->field("select member_country from tmember where member_id='" . $id . "'");

		$this->smarty->assign('username', $username);
		$this->smarty->assign('lastname', $lastname);
		$this->smarty->assign('phone', $phone);
		$this->smarty->assign('member_email', $member_email);
		$this->smarty->assign('listPP', $listPP);
		$content = $this->smarty->fetch('TPL_Dashboard_PaymentPending.tpl');

		return $content;
	}

	function KapitalboostPayment() {
		date_default_timezone_set('Asia/Singapore');
		$id = $this->member_id;
		$now = date("Y-m-d H-i-s"); // Show me


		function msg_email_paymanet($member_id, $member_name, $firstname, $lastname, $email, $phone, $amount, $campaign_id, $campaign_name, $campaign_type, $campaign_subtype_chosen, $campaign_subtype_ship, $payment_type, $ppid, $token, $member_country, $closing_date, $order_id, $sme_subtype, $amountIdrValue) {
			if ($campaign_subtype_chosen == '') {
				$donationText = "donation";
			} else {
				$donationText = "funding";
			}

			if ($sme_subtype == 'INVOICE  FINANCING') {
				$agreementText = "Invoice Financing Agreement";
			} else {
				$agreementText = "Murabaha Agreement";
			}

			$reward = "";
			if ($campaign_subtype_chosen == 'reward') {
				if ($campaign_subtype_ship == "1") {

				} else {

				}
				//$reward = "<p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>The owner of $campaign_name campaign will be in touch with you to request for information such as mailing address etc.</p>";
			}

			setlocale(LC_MONETARY, 'en_SG');
			$amount = money_format('%i', $amount);
			// $amount = number_format($amount,0,'.',',');
			if ($payment_type == "Bank Transfer") {
				if ($campaign_type != "donation") {
					if ($member_country === 'INDONESIA') {
						$content = '
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										Thank you for your commitment of
										<b>SGD ' . $amount . ' ( Rp '.$amountIdrValue.')</b> in the <b>' . $campaign_name . '</b> campaign.
									</p>
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										We are currently preparing your '. $agreementText .', which will be sent to your email within 1 working day.
									</p>
						';
					} else {
						$content = '
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										Thank you for your commitment of
										<b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
									</p>
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										We are currently preparing your '. $agreementText .', which will be sent to your email within 1 working day.
									</p>
						';
					}
				} else {
					if ($member_country === 'INDONESIA') {
						$content = '
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										Thank you for your ' . $donationText . ' of
										<b>SGD ' . $amount . ' ( Rp '.$amountIdrValue.')</b> in the <b>' . $campaign_name . '</b> campaign.
									</p>' . $reward . '
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										For payment, please proceed to make a bank transfer to the following account:
									</p>';
					} else {
						$content = '
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										Thank you for your ' . $donationText . ' of
										<b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
									</p>' . $reward . '
									<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
										For payment, please proceed to make a bank transfer to the following account:
									</p>';
					}
				}
				if ($member_country === 'INDONESIA') {
					$content .= '

								<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									For payment, please proceed to make a bank transfer to the following account:
								</p>
								<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									Account name: <b>PT Kapital Boost Indonesia</b><br>
									Bank name: <b>Bank Permata Syariah</b><br>
									Account number: <b>007 0178 9040</b><br>
								</p>';
				} else {
					// $content .= '
					// 			<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
					// 				For overseas fund transfer to Kapital Boost, you may do so via online payment gateways such as <b><a href="https://transferwise.com/">Transferwise</a></b> or <b><a href="https://www.xendpay.com/">Xendpay</a></b>
					// 			</p>
					// 			<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
					// 				If these are not available in your country, kindly email us so we can advise you accordingly.
					// 			</p>
					// 			<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
					// 				<b><u>Our account details:</u></b>
					// 			</p>
					// 			<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
					// 				Account name: <b>Kapital Boost Pte Ltd</b><br>
					// 				Bank name: <b>CIMB Bank Berhad, Singapore</b><br>
					// 				Account number: <b>2000 562 641</b><br>
					// 				SWIFT code: <b>CIBBSGSG</b><br>
					// 				Bank code: <b>7986</b><br>
					// 				Branch code: <b>001</b><br>
					// 			</p>
					// ';
					$content .= '
								<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									For payment, kindly transfer to our bank account as follows:
								</p>
								<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									<b><u>Our account details:</u></b>
								</p>
								<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									Account name: <b>Kapital Boost Pte Ltd</b><br>
									Bank name: <b>CIMB Bank Berhad, Singapore</b><br>
									Account number: <b>2000 562 641</b><br>
									SWIFT code: <b>CIBBSGSG</b><br>
									Bank code: <b>7986</b><br>
									Branch code: <b>001</b><br>
								</p>
								<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									If you are making an overseas fund transfer to Kapital Boost, you may do so using online payment gateways such as <b><a href="https://transferwise.com/">Transferwise</a></b> or <b><a href="https://www.instarem.com/en-sg/transfer-money-to-singapore">Instarem</a></b>. If these are not available in your country, kindly email us so we can advise you accordingly.
								</p>
					';
				}
				$content .= '
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								Once transferred, please submit the proof of payment via the button below.
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								<a href="https://kapitalboost.com/dashboard/confirm/bank?type=' . $campaign_type . '&campaign_id=' . $campaign_id . '&amount=' . $amount . '&campaign_name=' . $campaign_name . '&token=' . $token . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">Attach transfer slip</a>
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								To check on payment status, please visit the <a href="https://kapitalboost.com/member/investment.php" style="text-decoration:none">Portfolio</a> page on your dashboard.
							</p>
				';
			}


			$amounts = number_format($amount);
			if ($campaign_type != "donation" && ($payment_type == "Xfers" || $payment_type == "xfers")) {
				$content = '
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								Thank you for your commitment of
								<b>SGD ' . $amounts . '</b> in the <b>' . $campaign_name . '</b> campaign.
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								We are currently preparing your '. $agreementText .', which will be sent to your email within 1 working day.
							</p>

							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								For payment, please proceed via Xfers e-wallet by clicking on the link below.
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								<a href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '?payment_type=xfers&tokenpp=' . $token . '&member_id=' . $member_id . '&username=' . $member_name . '&lastname=' . $lastname . '&member_email=' . $email . '&phone=' . $phone . '&amount=' . $amount . '&campaign_id=' . $campaign_id . '&campaign_name=' . $campaign_name . '&campaign_type=' . $campaign_type . '&member_country=' . $member_country . '&closing_date=' . $closing_date . '&order_id=' . $order_id . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 10px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
								Pay Now
								</a>
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								To check on your payment status, please visit the <a href="https://kapitalboost.com/member/investment.php" style="text-decoration:none">Portfolio</a> page on your dashboard.
							</p>
				';
			}
			if ($campaign_type == "donation" && ($payment_type == "Xfers" || $payment_type == "xfers")) {
				$content = '
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								Thank you for your ' . $donationText . ' of
								<b>SGD ' . $amounts . '</b> in the <b>' . $campaign_name . '</b> campaign.
							</p>' . $reward . '
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								For payment, please proceed via Xfers e-wallet by clicking on the link below.
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								<a href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '?payment_type=xfers&tokenpp=' . $token . '&member_id=' . $member_id . '&username=' . $member_name . '&lastname=' . $lastname . '&member_email=' . $email . '&phone=' . $phone . '&amount=' . $amount . '&campaign_id=' . $campaign_id . '&campaign_name=' . $campaign_name . '&campaign_type=' . $campaign_type . '&member_country=' . $member_country . '&closing_date=' . $closing_date . '&order_id=' . $order_id . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 10px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
								Pay Now
								</a>
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								To check on your payment status, please visit the <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a> page on your dashboard.
							</p>
				';
			}

			if ($campaign_type != "donation" && $payment_type == "Paypal") {
			$content = '
						<p style="margin:20px 0;line-height:1.5em;">
							Thank you for your commitment of
							<b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
						</p>
						<p style="margin:20px 0;line-height:1.5em;">
							Please allow us one working day to prepare your Contract. Once ready, it will be sent to your registered email address. Do keep a lookout for it.
						</p>
							<a href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 10px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
								Pay Now
							</a>
						<p style="margin:20px 0;line-height:1.5em;">
							Please note that completion of the investment process is conditional upon successful payment transfer and electronic signing of the Contract.
						</p>
						<p style="margin:20px 0;line-height:1.5em;">
							To check on your payment status, please visit the <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a> page on your dashboard.
						</p>

			  ';
			}

			if ($campaign_type == "donation" && $payment_type == "Paypal") {
				$content = '
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								Thank you for your ' . $donationText . ' of
								<b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
							</p>' . $reward . '
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								For payment, please proceed to PayPal website via the button below
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								<a class="button-kpb-g" href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
								Pay Now
								</a>
							</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
								To check on your payment status, please visit the
								<a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a>
								page on your dashboard.
							</p>
				';
			}

			$msg = "

			<html>
				<head>
					<title>Kapital Boost's Enquiry</title>
				</head>
				<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
					<div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
						<div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
							<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
								Dear " . $firstname . ",
							</p>
							<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
								" . $content . "
							</p>

							<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
								Best regards,<br><br>Kapital Boost
							</p>
						</div>
						<div style='width:100%;height:7px;background-color:#00b0fd'></div>
						<div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
							<p style='font-size: 12px;color: white;text-align:center'>
								&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
							</p>
						</div>
					</div>
				</body>
			</html>
			";
			return $msg;
		}

		// Params From Campaign Details



		$exRate=$this->ExRate();

		$email = $_POST["email"];
		$country = $_POST["country"];
		$payment_type = $_POST["paymentID"]; // 1
		$campaign_type = $_POST["projecttype"]; // 2
		$sme_subtype = $_POST["sme_subtype"]; // 2
		$campaign_name = $_POST["item_name_1"]; // 3
		$amount = $_POST["amount"]; // 4
		$campaign_kod = $_POST["campaign_kod"];

		if (isset($_POST["amountIdr"])) {
			$amountIdr = $_POST["amountIdr"];
			$amountIdrValue = $_POST["amountIdrValue"];
		} else {
			$amountIdr = "0";
			$amountIdrValue = "0";
		}
		$amountIdrValue = $amount * $exRate;
		$username = $_POST["username"]; // 5
		$currency = "SGD";
		$campaign_id = $_POST["campaign_id"];
		$order_id = $_POST["order_id"]; // 7
		$token = $_POST["campaign_token"]; // 9
		$clossing_date = $_POST["clossing_date"]; // 10

		$pReturn = $this->db->field("select project_return from tcampaign where campaign_id = '$campaign_id'");
		$expectedPayout = $amount + ($amount * $pReturn / 100);

		if (isset($_POST["donateBtn"])) {
			$campaign_subtype_chosen = "donate";
		} else if (isset($_POST["rewardBtn"])) {
			$campaign_subtype_chosen = "reward";
		} else if (isset($_POST["interestBtn"])) {
			$campaign_subtype_chosen = "interest";
		}

		// round investment mount
		$amount = round($amount, 0);
		$amountIdrValue = round($amountIdrValue, 0);

		$member_firstname = $this->db->field("select member_firstname from tmember where member_id='  $id '");
		$member_surname = $this->db->field("select member_surname from tmember where member_id='$id'");
		$phone = $this->db->field("select member_mobile_no from tmember where member_id='" . $id . "'");
		$autoContractM = $this->db->field("select auto_contract from tmember where member_id='" . $id . "'");
		$autoContract = $this->db->field("select contract from tcampaign where campaign_id='" . $campaign_id . "'");
		$memberFullName = $member_firstname . " " . $member_surname;

		$campaign_subtype_ship = $this->db->field("select campaign_subtype_ship from tcampaign where campaign_id = '$campaign_id'");
		$total_funding_amt_check = $this->db->field("select total_funding_amt from tcampaign where campaign_id = '$campaign_id'");
		$curr_funding_amt_check = $this->db->field("select curr_funding_amt from tcampaign where campaign_id = '$campaign_id'");
		$release_date_check = $this->db->field("select release_date from tcampaign where campaign_id = '$campaign_id'");
		$fundNow = intval($curr_funding_amt_check)+$amount;



		if ($fundNow <= $total_funding_amt_check){

				if ($fundNow >= $total_funding_amt_check && $campaign_type != "donation") {
					$updateDate = "UPDATE tcampaign SET expiry_date = '$release_date_check' WHERE campaign_id = '$campaign_id'";
					$this->db->exec($updateDate);
				}

				$sql1 = "INSERT INTO tinv 	(id,member_id,nama,email,country,campaign,campaign_id,tipe,total_funding,total_funding_id,expected_payout,date,status,"
					  . "project_type,campaign_subtype_chosen,closing_date,token_pp,order_id) VALUES (NULL,'$id','$memberFullName','$email','$country','$campaign_name','$campaign_id',"
					  . "'$payment_type','$amount','$amountIdrValue','$expectedPayout','$now','Unpaid','$campaign_type','$campaign_subtype_chosen','$clossing_date','$token','$order_id')";

				$this->db->exec($sql1);

				$sql_ppid = "SELECT id FROM `tinv` ORDER BY `id` DESC LIMIT 1";
				$sql_e_ppid = "SELECT id FROM `tinv` ORDER BY `id` DESC LIMIT 1";

				$ppid = $this->db->field($sql_ppid);
				$e_ppid = $this->db->field($sql_e_ppid);

				$auto = $this->db->field("SELECT SUM(total_funding) AS Totals FROM tinv WHERE campaign_id='$campaign_id'");
				$updateAuto = "UPDATE tcampaign SET curr_funding_amt = '$auto' WHERE campaign_id = '$campaign_id'";
				$this->db->exec($updateAuto);



				$payouts = $this->db->field("select m_payout from tcampaign where campaign_id = '$campaign_id'");
				if ($payouts != "") {
					$payoutA = explode("==", $payouts);
					$amountA = $dateA = $statusA = array();
					for ($i = 0; $i < count($payoutA); $i++) {
						$payout = explode("~", $payoutA[$i]);
						$amountA[$i] = $payout[0];
						$dateA[$i] = $payout[1];
						$statusA[$i] = $payout[2];

						if ($amountA[$i] != "") {
							$singleAmount = $expectedPayout * ($amountA[$i] / 100);
							$this->UpdatePayout($ppid, $i + 1, round($singleAmount, 2), $dateA[$i], $statusA[$i]);
						}
					}
				}


				$this->smarty->assign("ppid", $ppid);
				if ($campaign_type == "donation") {
					$subject = "Intent of donation";
				} else {
					$subject = "Intent of investment";
				}
				if ($amountIdrValue !== "0") {
					$amountEmail = $amount . " (IDR $amountIdr) ";
				} else {
					$amountEmail = $amount;
				}
				$email_notif_payment = msg_email_paymanet($id, $username, $member_firstname, $member_surname, $email, $phone, $amountEmail, $campaign_id, $campaign_kod, $campaign_type, $campaign_subtype_chosen, $campaign_subtype_ship, $payment_type, $e_ppid, $token, $country, $clossing_date, $order_id, $sme_subtype, $amountIdrValue);

				if (isset($_POST["username"]) && $payment_type != "paypal") {
					$this->SendEmailSendGridOld(EMAIL_FROM, $email, 'admin@kapitalboost.com', $subject, $email_notif_payment, TRUE);
				}

				$curl = curl_init();
				curl_setopt_array($curl, array(
					CURLOPT_URL => "https://kapitalboost.com/admin/createpdf.php?c=$campaign_id&i=$ppid&token=$token",
					CURLOPT_SSL_VERIFYPEER => false,
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 50,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
				));
				curl_exec($curl);

				if ($autoContract == 1) {
					echo "'<script>console.log(\"masuk ke autocontract\")</script>'";
					// echo "'<script>console.log(\"https://kapitalboost.com/admin/sendpdf.php?c=$campaign_id&i=$ppid&token=$token\")</script>'";
					// if ($autoContractM == 1) {
					 $curl_send = curl_init();
					 curl_setopt_array($curl_send, array(
						 CURLOPT_URL => "https://kapitalboost.com/admin/sendpdf.php?c=$campaign_id&i=$ppid&token=$token",
						 CURLOPT_SSL_VERIFYPEER => false,
						 CURLOPT_MAXREDIRS => 10,
						 CURLOPT_TIMEOUT => 50,
						 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						 CURLOPT_CUSTOMREQUEST => "GET",
					 ));

					 $res = curl_exec($curl_send);
					// }
				}

				if (isset($_GET["member_email"])) {
					$member_wallet = $this->db->field("select wallet from tmember where member_id='" . $_GET["member_email"] . "'");
				}else if($this->member_id){
					$member_wallet = $this->db->field("select wallet from tmember where member_id='" . $this->member_id . "'");
				}else{
					$member_wallet = 0;
				}

				$this->smarty->assign('member_wallet', (float)$member_wallet);
				$this->smarty->assign('pay_with_wallet', (float)$member_wallet >= (float)$amount);

				$this->AddTrail("Payment Web: $campaign_name - $payment_type", $this->member_id);

				$this->smarty->assign("total_funding", $amount);
				$this->smarty->assign("total_funding_id", $amountIdrValue);
				$this->smarty->assign('username', $username);
				$this->smarty->assign('lastname', $member_surname);
				$this->smarty->assign('firstname', $member_firstname);
				$this->smarty->assign('phone', $phone);
				$this->smarty->assign('member_email', $email);
				$this->smarty->assign('member_id', $id);
				$this->smarty->assign('campaign_id', $campaign_id);
				$this->smarty->assign('member_country', $country);
				$this->smarty->assign('token_pp', $token);
				$this->smarty->assign('closing_date', $clossing_date);
				$this->smarty->assign('campaign', $campaign_name);
				$this->smarty->assign('project_type', $campaign_type);
				$this->smarty->assign('tipe', $payment_type);
				$this->smarty->assign('order_id', $order_id);
				$this->smarty->assign('campaign_subtype_chosen', $campaign_subtype_chosen);

				if ($campaign_type == "donation" && $payment_type == "Paypal") {
					$sql = "SELECT * FROM tinv WHERE id='" . $ppid . "'";
					// echo "'<script>console.log(\"ppid : $sql\")</script>'";
					list($detail, $detail_count) = $this->db->singlearray($sql);

					header("Location: https://kapitalboost.com/dashboard/payment-detail/$ppid");

				} else {
					$content = $this->smarty->fetch('TPL_Invest_Form.tpl');
				}

				return $content;
		}
	}

	function UpdatePayout($iId, $item, $amount, $date, $status) {
		$date = date("Y-m-d", strtotime($date));
		$sql = "UPDATE tinv SET bulan_$item = '$amount',tanggal_$item = '$date',status_$item = '$status' WHERE id = $iId";
		$this->db->field($sql);
	}

	function XferstOtp() {
		$error = 0;
		$this->smarty->assign("gagalotp", $error);

		$content = $this->smarty->fetch('TPL_Xfers_otp.tpl');
		return $content;
	}

	function XfersBallance() {
		if ($this->member_id) {
			$id = $this->member_id;
		} else {
			$id = $_GET["mid"];
			$this->member_id = $id;
		}
		$country = $this->db->field("select current_location from tmember where member_id = '$id'");
		$ballance = "{{xballance.available_balance}}";
		$amount = "{{params.amount}}";
		$status = "{{status}}";
		$otp = $this->db->field("select member_otp from tmember where member_id='" . $id . "'");

		if ($otp == NULL) {
			$this->smarty->assign("mid", $id);
			$content = $this->smarty->fetch('TPL_Xfers_otp_2.tpl');
			$error = "You dont have any campaign invest!";
			$this->smarty->assign("gagalotp", $error);

			return $content;
		} else {
			$this->smarty->assign("country", $country);
			$this->smarty->assign("ballance", $ballance);
			$this->smarty->assign("status", $status);
			$this->smarty->assign("amount", $amount);
			$this->smarty->assign("mid", $id);
			$content = $this->smarty->fetch('TPL_Xfers_ballance.tpl');

			return $content;
		}
	}

	function XfersTransferinfo() {
		$mid = $_GET["mid"];
		$this->smarty->assign("mid", $mid);
		$content = $this->smarty->fetch('TPL_Xfers_transferinfo.tpl');

		return $content;
	}

	function XfersThankyou() {
		$type = $_GET["type"];

		if ($type == "donation") {
			$campaign_type = "Donation";
		} else if ($type == "sme") {
			$campaign_type = "Fund";
		}
		$country = $_GET["country"];
		$amount = $_GET["amount"];
		if ($country === 'INDONESIA') {
			setlocale(LC_MONETARY, 'en_US.UTF-8');
			$amount = money_format('%.0n', $amount);
			$amount = substr($amount, 1);
		}

		$campaign_name = $_GET["campaign"];
		$campaign_id = $_GET["campaignid"];

		$this->smarty->assign("campaign_type", $campaign_type);
		$this->smarty->assign("amount", $amount);
		$this->smarty->assign("country", $country);
		$this->smarty->assign("campaign_name", $campaign_name);
		$this->smarty->assign("campaign_id", $campaign_id);
		$this->smarty->assign("type", $type);

		// Email here

		$content = $this->smarty->fetch('TPL_Xfers_Thankyou.tpl');

		return $content;
	}

	function ShowProfile() {
		list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");
		list($nationality, ) = $this->db->multiarray("SELECT * FROM tnationality ORDER BY nationality ASC");
		foreach ($country as $row) {
			$member_country_options[$row['name']] = $row['name'];
		}
		$ic_options["IC"] = "Identity Card";
		$ic_options["FIN"] = "FIN";
		$ic_options["Passport"] = "Passport";

		foreach ($nationality as $row2) {
			$member_nationality_options[$row2['nationality']] = $row2['nationality'];
		}

		$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
		$member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
		$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
		$member_password = $this->db->field("select member_password from tmember where member_id='" . $this->member_id . "'");
		$member_surname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
		$member_firstname = $this->db->field("select member_firstname from tmember where member_id='" . $this->member_id . "'");
		$member_icname = $this->db->field("select member_icname from tmember where member_id='" . $this->member_id . "'");
		$member_icoption = $this->db->field("select ic_option from tmember where member_id='" . $this->member_id . "'");
		$member_iccountry = $this->db->field("select ic_country from tmember where member_id='" . $this->member_id . "'");
		$member_mobile = $this->db->field("select member_mobile_no from tmember where member_id='" . $this->member_id . "'");
		$resarea = $this->db->field("select residental_area from tmember where member_id='" . $this->member_id . "'");
		$dob = $this->db->field("select dob from tmember where member_id='" . $this->member_id . "'");
		$nric = $this->db->field("select nric from tmember where member_id='" . $this->member_id . "'");
		$filenric = $this->db->field("select nric_file from tmember where member_id='" . $this->member_id . "'");
		$filenricback = $this->db->field("select nric_file_back from tmember where member_id='" . $this->member_id . "'");
		$addressProof = $this->db->field("select address_proof from tmember where member_id='" . $this->member_id . "'");
		$ceek1 = $this->db->field("select cek1 from tmember where member_id='" . $this->member_id . "'");
		$ceek2 = $this->db->field("select cek2 from tmember where member_id='" . $this->member_id . "'");
		$ceek3 = $this->db->field("select cek3 from tmember where member_id='" . $this->member_id . "'");
		$ceek4 = $this->db->field("select cek4 from tmember where member_id='" . $this->member_id . "'");
		$ceek5 = $this->db->field("select  cek5 from tmember where member_id='" . $this->member_id . "'");
		$ceek6 = $this->db->field("select cek6 from tmember where member_id='" . $this->member_id . "'");
		$ceek7 = $this->db->field("select cek7 from tmember where member_id='" . $this->member_id . "'");
		$ceek8 = $this->db->field("select cek8 from tmember where member_id='" . $this->member_id . "'");
		$ceek9 = $this->db->field("select cek9 from tmember where member_id='" . $this->member_id . "'");
		$ceek10 = $this->db->field("select cek10 from tmember where member_id='" . $this->member_id . "'");
		$ceek11 = $this->db->field("select cek11 from tmember where member_id='" . $this->member_id . "'");
		$ceek12 = $this->db->field("select cek12 from tmember where member_id='" . $this->member_id . "'");
		$ceek13 = $this->db->field("select cek13 from tmember where member_id='" . $this->member_id . "'");
		$current_location = $this->db->field("select current_location from tmember where member_id='" . $this->member_id . "'");
		$member_nationality = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
		$this->smarty->assign("current_location", $current_location);
		$this->smarty->assign("member_country_options", $member_country_options);
		$this->smarty->assign("ic_options", $ic_options);
		$this->smarty->assign("ic_option", $member_icoption);
		$this->smarty->assign("ic_country", $member_iccountry);
		$this->smarty->assign("member_nationality", $member_nationality);
		$this->smarty->assign("member_nationality_options", $member_nationality_options);
		$this->smarty->assign("member_id", $this->member_id);
		$this->smarty->assign("member_name", $member_name);
		$this->smarty->assign("member_surname", $member_surname);
		$this->smarty->assign("member_firstname", $member_firstname);
		$this->smarty->assign("member_icname", $member_icname);
		$this->smarty->assign("member_mobile", $member_mobile);
		$this->smarty->assign("member_country", $member_country);
		$this->smarty->assign("member_email", $member_email);
		$this->smarty->assign("member_password", $member_password);
		$this->smarty->assign("nric", $nric);
		$this->smarty->assign("dob", $dob);
		$this->smarty->assign("resarea", $resarea);
		$this->smarty->assign("filenric", $filenric);
		$this->smarty->assign("filenricback", $filenricback);
		$this->smarty->assign("addressProof", $addressProof);
		$checkLastExtFront = substr($filenric, -4);
		$checkLastExtBack = substr($filenricback, -4);
		$checkLastExtAddress = substr($addressProof, -4);
		$this->smarty->assign("checkLastExtFront", $checkLastExtFront);
		$this->smarty->assign("checkLastExtBack", $checkLastExtBack);
		$this->smarty->assign("checkLastExtAddress", $checkLastExtAddress);


		$this->smarty->assign("ceek1", $ceek1);
		$this->smarty->assign("ceek2", $ceek2);
		$this->smarty->assign("ceek3", $ceek3);
		$this->smarty->assign("ceek4", $ceek4);

		$this->smarty->assign("ceek5", $ceek5);
		$this->smarty->assign("ceek6", $ceek6);
		$this->smarty->assign("ceek7", $ceek7);
		$this->smarty->assign("ceek8", $ceek8);

		$this->smarty->assign("ceek9", $ceek9);
		$this->smarty->assign("ceek10", $ceek10);
		$this->smarty->assign("ceek11", $ceek11);
		$this->smarty->assign("ceek12", $ceek12);
		$this->smarty->assign("ceek13", $ceek13);

		$content = $this->smarty->fetch('Ghz_Dashboard_Manage_Profile.tpl');
		return $content;
	}

	function SaveProfile() {

		$uname = $this->test_input($_POST['uname']);
		$first = $this->test_input($_POST['firstname']);
		$last = $this->test_input($_POST['lastname']);
		if (isset($_POST['password']) && $this->test_input($_POST["password"]) != "") {
			$pass = md5($this->test_input($_POST['password']));
		} else {
			$pass = $this->test_input($_POST["hiddenpassword"]);
		}

		$nationality = $this->test_input($_POST['nationality']);
		$country = $this->test_input($_POST['current_location']);
		$nric = $this->test_input($_POST['nric']);
		$ic_option = $this->test_input($_POST["ic_option"]);
		$ic_name = $this->test_input($_POST["ic_name"]);
		$ic_country = $this->test_input($_POST["ic_country"]);
		$dob = $this->test_input($_POST['dob']);
		$resarea = $this->test_input($_POST['resarea']);
		$nope = $this->test_input($_POST['nope']);
		$cek1 = $this->test_input($_POST['check_1']);
		$cek2 = $this->test_input($_POST['check_2']);
		$cek3 = $this->test_input($_POST['check_3']);
		$cek4 = $this->test_input($_POST['check_4']);
		$cek5 = $this->test_input($_POST['check_6']);
		$cek6 = $this->test_input($_POST['check_7']);
		$cek7 = $this->test_input($_POST['check_8']);
		$cek8 = $this->test_input($_POST['check_9']);
		$cek9 = $this->test_input($_POST['check_10']);
		$cek10 = $this->test_input($_POST['check_11']);
		$cek11 = $this->test_input($_POST['check_12']);
		$cek12 = $this->test_input($_POST['check_13']);
		$cek13 = $this->test_input($_POST['check_14']);

		// NRIC Front
		$nric_imgname = $_FILES["nric_file"]["name"];
		$nric_pisah = explode(".", $nric_imgname);
		$nric_imgtype = end($nric_pisah);
		$nric_file_img = $_FILES["nric_file"]["tmp_name"];
		$nric_newname = time() . rand() . "." . $nric_imgtype;
		// NRIC Back
		if (isset($_POST["passport"])) {
			$nric_back_newname = 'passport.png';
		} else {
			$nric_imgname_back = $_FILES["nric_file_back"]["name"];
			$nric_pisah_back = explode(".", $nric_imgname_back);
			$nric_imgtype_back = end($nric_pisah_back);
			$nric_file_img_back = $_FILES["nric_file_back"]["tmp_name"];
			$nric_back_newname = time() . rand() . "." . $nric_imgtype_back;
		}
		// Address Proof
		$apName = $_FILES["address_proof"]["name"];
		$apExt = explode(".", $apName);
		$apType = end($apExt);
		$ap_file = $_FILES["address_proof"]["tmp_name"];
		$ap_newname = time() . rand() . "." . $apType;


		if (!empty($nric_file_img)) {
			$uploadnric = "UPDATE tmember SET nric_file = '$nric_newname' WHERE member_id='$this->member_id'";
			$this->db->exec($uploadnric);
			move_uploaded_file($nric_file_img, "assets/nric/datanric/" . $nric_newname);
		}
		if (!empty($nric_file_img_back) || $nric_back_newname == 'passport.png') {
			$uploadnricback = "UPDATE tmember SET nric_file_back = '$nric_back_newname' WHERE member_id='$this->member_id'";
			$this->db->exec($uploadnricback);
			if ($nric_back_newname != "passport.png") {
			move_uploaded_file($nric_file_img_back, "assets/nric/datanricback/" . $nric_back_newname);
			}
		}
		if (!empty($ap_file)) {
			$uploadap = "UPDATE tmember SET address_proof = '$ap_newname' WHERE member_id='$this->member_id'";
			$this->db->exec($uploadap);
			move_uploaded_file($ap_file, "assets/nric/addressproof/" . $ap_newname);
		}

		$ic_type = "$ic_country $ic_option No.";
		$update = "UPDATE tmember SET current_location = '$country',member_username = '$uname',
		member_password = '$pass',member_surname = '$last',member_firstname = '$first',
		member_country = '$nationality',nric= '$nric',ic_type='$ic_type',ic_option = '$ic_option',member_icname = '$ic_name',ic_country = '$ic_country',dob = '$dob',residental_area = '$resarea',
		cek1 = '$cek1',cek2 = '$cek2', cek3 = '$cek3',cek4 = '$cek4',cek5 = '$cek5',cek6 = '$cek6',cek7 = '$cek7',cek8 = '$cek8',cek9 = '$cek9',cek10 = '$cek10',cek11 = '$cek11',
		cek12 = '$cek12', cek13 = '$cek13',member_mobile_no = '$nope' WHERE member_id = '$this->member_id'";
		$query = $this->db->exec($update);


		if ($query) {
			$sukses = "Berhasil update";
		} else {
			$gagal = "gagal update";
		}

		$access_token_member = $this->db->field("select member_access_token from tmember where member_id = '$this->member_id'");

		$gender = "";
		if ($member_gender == "F") {
			$gender = "female";
		} else {
			$gender = "male";
		}

		$arr_xudata = array(
			"first_name" => $first,
			"last_name" => $last,
			"email" => $member_email,
			"date_of_birth" => $dob,
			"gender" => $gender,
			"address_line_1" => $resarea,
			"nationality" => $member_nationality,
			"identity_no" => $nric,
			"country" => $current_location,
			"id_front_url" => "https://kapitalboost.com/assets/nric/datanric/$filenric",
			"id_back_url" => "https://kapitalboost.com/assets/nric/datanricback/$filenricback",
			"proof_of_address_url" => "https://kapitalboost.com/assets/nric/addressproof/$addressProof"
		);

		$xudata = json_encode($arr_xudata);

		$curl = curl_init();
		if ($current_location == 'SINGAPORE') {
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://www.xfers.io/api/v3/user",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "PUT",
				CURLOPT_POSTFIELDS => $xudata,
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"content-type: application/json",
					"x-xfers-user-api-key: $access_token_member"
				),
			));
		} else if ($current_location == 'INDONESIA') {
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://id.xfers.com/api/v3/user",
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 20,
				CURLOPT_TIMEOUT => 60,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "PUT",
				CURLOPT_POSTFIELDS => "$xudata",
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",
					"content-type: application/json",
					"x-xfers-user-api-key: $access_token_member"
				),
			));
		}

		$response = curl_exec($curl);
		$sql = "UPDATE  tmember SET  `xfers_info` =  '$response' WHERE member_id = '$this->member_id'";

		// MailChimp Integration Edit member info
		$mcArray = array(
			"email_address" => "$member_email",
			"status" => "subscribed",
			"merge_fields" => array("FNAME" => "$member_firstname", "LNAME" => "$member_surname", "COUNTRY" => "$current_location", "MNUMBER" => "$nope")
		);
		$md5Email = md5($member_email);
		$mcJson = json_encode($mcArray);
		$mcCurl = curl_init();
		curl_setopt_array($mcCurl, array(
			CURLOPT_URL => "https://us11.api.mailchimp.com/3.0/lists/c2bdf8bd59/members/$md5Email",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 50,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => $mcJson,
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",
				"content-type: application/json",
				"authorization: Basic b3dlbjoxOGQ0ZjNlNzAxNTQ3ZWUwYzljYzk4MmE0YjI1NDFhMC11czEx"
			),
		));
		$response1 = curl_exec($mcCurl);
		curl_close($mcCurl);
		$this->db->exec($sql);
		$this->smarty->assign("query", $query);
		$this->smarty->assign("sukses", $sukses);
		$this->smarty->assign("gagal", $gagal);
		$this->ShowProfile();
		$content = $this->smarty->fetch('Ghz_Dashboard_Manage_Profile.tpl');

		return $content;
	}

	function InvestHistory() {
		$member_name = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
		$member_country = $this->db->field("select member_country from tmember where member_id='" . $this->member_id . "'");
		$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
		$this->smarty->assign("member_id", $this->member_id);
		$this->smarty->assign("member_name", $member_name);
		$this->smarty->assign("member_country", $member_country);
		$this->smarty->assign("member_email", $member_email);
		list($invest, ) = $this->db->multiarray("SELECT * FROM tinv WHERE owner='$member_name'  ORDER BY date DESC");
		$this->smarty->assign('invest', $invest);
		$this->smarty->assign('error_msg', $error_msg);
		$content = $this->smarty->fetch('TPL_Dashboard_Invest.tpl');

		return $content;
	}

	function test_input($data) {
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		return $data;
	}

	function LoadInvestDataForm($data = NULL) { // show me
		if (isset($_GET["campaign_id"]) && isset($_GET["member_email"]) && $this->member_id == "") {
			$username = $this->test_input($_GET["username"]);
			$lastname = $this->test_input($_GET["lastname"]);
			$phone = $this->test_input($_GET["phone"]);
			$total_funding = $this->test_input($_GET["amount"]);
			$member_email = $this->test_input($_GET["member_email"]);
			$campaign_id = $this->test_input($_GET["campaign_id"]);
			$member_country = $this->test_input($_GET["member_country"]);
			$tokenpp = $this->test_input($_GET["tokenpp"]);
			$closingDate = $this->test_input($_GET["closing_date"]);
			$campaign_name = $this->test_input($_GET["campaign_name"]);
			$cType = $this->test_input($_GET["campaign_type"]);
			$pType = $this->test_input($_GET["payment_type"]);
			$order_id = $this->test_input($_GET["order_id"]);
			$member_id = $this->test_input($_GET["member_id"]);
			$this->smarty->assign('total_funding', $total_funding);
			// echo "'<script>console.log(\"Masuk a\")</script>'";

		} else {
			$id = (int) $_GET['id'];
			if ($data) {
				$this->smarty->assign($data);
			}
			$username = $this->db->field("select member_username from tmember where member_id='" . $this->member_id . "'");
			$member_id = $this->member_id;
			$lastname = $this->db->field("select member_surname from tmember where member_id='" . $this->member_id . "'");
			$phone = $this->db->field("select member_mobile_no from tmember where member_id='" . $this->member_id . "'");
			$member_email = $this->db->field("select member_email from tmember where member_id='" . $this->member_id . "'");
			$member_country = $this->db->field("select current_location from tmember where member_id='$this->member_id'");
			$campaign_id = $this->db->field("select campaign_id from tinv where id='$id'");
			$campaign_name = $this->db->field("select campaign from tinv where id = '$id'");
			$tokenpp = $this->db->field("select token_pp from tinv where id = $id");
			$closingDate = $this->db->field("select expiry_date from tcampaign where id = $campaign_id");
			$cType = $this->db->field("select project_type from tinv where id = $id");
			$pType = $this->db->field("select tipe from tinv where id = $id");
			$order_id = $this->db->field("select order_id from tinv where id = $id");
			// echo "'<script>console.log(\"data : $data\")</script>'";

		}

		if (isset($_GET["member_email"])) {
			$member_wallet = $this->db->field("select wallet from tmember where member_id='" . $_GET["member_email"] . "'");
		}else if($this->member_id){
			$member_wallet = $this->db->field("select wallet from tmember where member_id='" . $this->member_id . "'");
		}else{
			$member_wallet = 0;
		}

		$invMId = $this->db->field("select member_id from tinv where token_pp = '$tokenpp'");
		$imgStr = $this->db->field("select images from tinv where id = '$id'");
		$total_funding = $this->db->field("select total_funding from tinv where id = '$id'");
		$total_funding_id = $this->db->field("select total_funding_id from tinv where id = '$id'");

		if (is_numeric($total_funding_id) and $total_funding_id != '0' and $total_funding_id != 0){
			$total_funding_id = number_format($total_funding_id, 2, '.', ',');
			// echo "'<script>console.log(\"masuk udah ada fund id\")</script>'";
		} else {
			$exRate = $this->ExRate();

			// echo "'<script>console.log(\"masuk cek exRate : $exRate\")</script>'";

			$total_funding_id = $total_funding * $exRate;

			$updateFundId = "UPDATE tinv SET total_funding_id = '$total_funding_id' WHERE id = '$id'";
			$this->db->exec($updateFundId);

			$total_funding_id = number_format($total_funding_id, 2, '.', ',');
		}

		$total_fundpaypal = number_format($total_funding, 2, '.', '');


		$images = explode("~", $imgStr);

		if ($member_id != $invMId) {
			$this->smarty->assign("mismatched", "1");
			$this->smarty->assign('member_id', $member_id);
		} else {
			$this->smarty->assign("mismatched", "0");
			$this->smarty->assign("images", $images);
			$this->smarty->assign('username', $username);
			$this->smarty->assign('lastname', $lastname);
			$this->smarty->assign('total_funding_id', $total_funding_id);
			$this->smarty->assign('total_fundpaypal', $total_fundpaypal);
			$this->smarty->assign('phone', $phone);
			$this->smarty->assign('member_email', $member_email);
			$this->smarty->assign('member_id', $member_id);
			$this->smarty->assign('campaign_id', $campaign_id);
			$this->smarty->assign('member_country', $member_country);
			$this->smarty->assign('token_pp', $tokenpp);
			$this->smarty->assign('closing_date', $closingDate);
			$this->smarty->assign('campaign', $campaign_name);
			$this->smarty->assign('project_type', $cType);
			$this->smarty->assign('tipe', $pType);
			$this->smarty->assign('order_id', $order_id);
			$this->smarty->assign('member_wallet', $member_wallet);
			$this->smarty->assign('pay_with_wallet', $member_wallet >= $total_funding);

		}
	// echo "'<script>console.log(\"$order_id|$campaign_name|$username|$member_email|$member_country|campaign_owner1|$tokenpp|$cType|$total_fundpaypal\")</script>'";

		$content = $this->smarty->fetch('TPL_Invest_Form.tpl');
		return $content;
	}

	function InvestData() {
		$id = (int) $_GET['id'];
		$sql = "SELECT * FROM tinv WHERE id='" . $id . "'";
		list($detail, $detail_count) = $this->db->singlearray($sql);
		$result = $this->LoadInvestDataForm($detail);

		return $result;
	}

	function BankConfirmationThankyou() {
		//verify recaptcha
		$response = $_POST["g-recaptcha-response"];
		$url = 'https://www.google.com/recaptcha/api/siteverify';
		$data = array(
			'secret' => '6LeKSlAUAAAAAChAjmtKLimNg65KcFs-UhByJdot',
			'response' => $_POST["g-recaptcha-response"]
		);
		$options = array(
			'http' => array (
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);
		$context  = stream_context_create($options);
		$verify = file_get_contents($url, false, $context);
		$captcha_success=json_decode($verify);
		// ----------------------------------

		if ($captcha_success->success==false) {
		echo "<script>alert(\"Wrong Captcha\");</script>";
		echo "<script>window.history.back();</script>";

		} else if ($captcha_success->success==true) {
			$type = $_POST["type"];
			$campaign_id = $_POST["campaign_id"];
			$amount = $_POST["amount"];
			$bank = $_POST["bank_name"];
			$account_name = $_POST["account_name"];
			$account_number = $_POST["account_number"];
			$fileName = time();
			$token = $_POST["action"];
			$error_uploading = 0;
			//$file_name = $_POST["file_name"];

			$query = "SELECT token_pp FROM tinv WHERE token_pp='$token'";
			$conn = $this->db->GetConn();
			$result = mysqli_query($conn, $query);
			$Results = mysqli_fetch_array($result);
			$cari = mysqli_num_rows($result);

			if ($cari >= 1) {
				$query = "UPDATE tinv SET bukti='$fileName', bank='$bank', bank_transfer_account_name='$account_name',bank_transfer_account_number='$account_number'  WHERE  token_pp='$token'";
				mysqli_query($conn, $query);
				move_uploaded_file($_FILES['bukti']['tmp_name'], "assets/images/bukti/$fileName");
				$error_uploading = 1;
			} else {
				$error_uploading = 0;
			}


			$this->smarty->assign("type", $type);
			$this->smarty->assign("campaign_id", $campaign_id);
			$nama_campaign = $this->db->field("select campaign_name from tcampaign where campaign_id='" . $campaign_id . "'");
			$tipe_campaign = $this->db->field("select project_type from tcampaign where campaign_id='" . $campaign_id . "'");
			$campaign_subtype = $this->db->field("select campaign_subtype from tcampaign where campaign_id = '" . $campaign_id . "'");

			$subject = 'Proof of payment';
			$message = "

			<html>
				<head>
					<title>Kapital Boost's Proof Submitted</title>
				</head>
				<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
					<div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
						<div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
							<img src='https://kapitalboost.com/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
							<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
								Dear Admin,
							</p>
							<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
								You have a new bank transfer confirmation.
							</p>
							<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
								Bank Name                : " . $bank . "<br>
								Account Holder's Name             : " . $account_name . "<br>
								Account Number           : " . $account_number . "<br>
								Receipt File : <a href='https://kapitalboost.com/assets/images/bukti/$fileName' target='_blank'>Here</a><br>
							</p>

							<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
								Best regards,<br><br>Tech Ninja
							</p>
						</div>
						<div style='width:100%;height:7px;background-color:#00b0fd'></div>
						<div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
							<p style='font-size: 12px;color: white;text-align:center'>
								&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
							</p>
						</div>
					</div>
				</body>
			</html>

			";

			$this->SendEmailSendGridOld(EMAIL_FROM, "admin@kapitalboost.com", '', $subject, $message, TRUE);
			//Email Here

			$this->smarty->assign("campaign_name", $nama_campaign);
			$this->smarty->assign("campaign_tipe", $tipe_campaign);
			$this->smarty->assign("campaign_subtype", $campaign_subtype);
			$this->smarty->assign("amount", $amount);
			$this->smarty->assign("error_uploading", $error_uploading);
			$content = $this->smarty->fetch('Ghz_Bank_Thankyou.tpl');

			return $content;
		}
	}

	function DashboardConfirmBank() {
		$id = $this->member_id;
		if ($id) {
			$login = "login";
		}

		$this->smarty->assign("login", $login);
		$bank_type = $_GET["type"];
		$bank_campaign_id = $_GET["campaign_id"];
		$bank_amount = $_GET["amount"];
		$bank_token = $_GET["token"];
		$bank_campaign_name = $_GET["campaign_name"];

		$this->smarty->assign("bank_type", $bank_type);
		$this->smarty->assign("bank_campaign_id", $bank_campaign_id);
		$this->smarty->assign("bank_amount", $bank_amount);
		$this->smarty->assign("bank_token", $bank_token);
		$this->smarty->assign("bank_campaign_name", $bank_campaign_name);
		$content = $this->smarty->fetch('Ghz_Bank_Confirm.tpl');

		return $content;
	}

	function DashboardPaymentApi() {
		$ppid = $_GET["ppid"];
		$login = $this->member_id;
		$id = $this->member_id;

		// Prepare Query
		$sql_member_id = "select member_id  from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_payment_type = "select payment_type from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_status = "select status from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_invest_date = "select invest_date from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_campaign_type = "select campaign_type from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_campaign_name = "select campaign_name from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_amount = "select amount from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_investor_name = "select investor_name from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_currency = "select currency from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_order_id = "select order_id from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_campaign_owner = "select campaign_owner from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_token = "select token from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_closing_date = "select closing_date from tpaymentpending where ppid = '" . $ppid . "'";
		$sql_campaign_id = "select campaign_id from tpaymentpending where ppid = '" . $ppid . "'";


		$member_id1 = $this->db->field($sql_member_id);
		$payment_type = $this->db->field($sql_payment_type);
		$status1 = $this->db->field($sql_status);
		$invest_date1 = $this->db->field($sql_invest_date);
		$campaign_type1 = $this->db->field($sql_campaign_type);
		$campaign_name1 = $this->db->field($sql_campaign_name);
		$amount1 = $this->db->field($sql_amount);
		$investor_name1 = $this->db->field($sql_investor_name);
		$currency1 = $this->db->field($sql_currency);
		$order_id1 = $this->db->field($sql_order_id);
		$campaign_owner1 = $this->db->field($sql_campaign_owner);
		$token1 = $this->db->field($sql_token);
		$closing_date1 = $this->db->field($sql_closing_date);
		$campaign_id1 = $this->db->field($sql_campaign_id);

		$sql_member_email1 = "select member_email from tmember where member_id = '" . $id . "'";
		$member_email1 = $this->db->field($sql_member_email1);
		$this->smarty->assign("member_email1", $member_email1);

		$sql_member_phone_1 = "select member_mobile_no from tmember where member_id = '" . $id . "'";
		$member_phone_1 = $this->db->field($sql_member_phone_1);
		$this->smarty->assign("member_phone_1", $member_phone_1);

		$sql_member_country_1 = "select member_country from tmember where member_id = '" . $id . "'";
		$member_country_1 = $this->db->field($sql_member_country_1);
		$this->smarty->assign("member_country_1", $member_country_1);

		$this->smarty->assign("member_id1", $member_id1);
		$this->smarty->assign("payment_type", $payment_type);
		$this->smarty->assign("status1", $status1);
		$this->smarty->assign("invest_date1", $invest_date1);
		$this->smarty->assign("campaign_type1", $campaign_type1);
		$this->smarty->assign("campaign_name1", $campaign_name1);
		$this->smarty->assign("amount1", $amount1);
		$this->smarty->assign("investor_name1", $investor_name1);
		$this->smarty->assign("currency1", $currency1);
		$this->smarty->assign("order_id1", $order_id1);
		$this->smarty->assign("campaign_owner1", $campaign_owner1);
		$this->smarty->assign("token1", $token1);
		$this->smarty->assign("closing_date1", $closing_date1);
		$this->smarty->assign("campaign_id1", $campaign_id1);

		$content = $this->smarty->fetch('Ghz_Payment.tpl');
		return $content;
	}

	function payWithWallet() {
		$order_id = $_GET['order_id'];
		$tinv = $this->db->singlearray("SELECT * FROM tinv WHERE order_id = '$order_id'");
		$investment_datas = $tinv[0];
		$message_error = "";
		$already_paid = '0';

		if ($investment_datas['status'] == "Unpaid") {
			$member_id = $investment_datas['member_id'];
			$member_wallet = $this->db->field("select wallet from tmember where member_id = '$member_id'");
			$invest_amount = (float)$investment_datas['total_funding'];

			$wallet = (float)$member_wallet - $invest_amount;
			$update_wallet_result = $this->db->exec("UPDATE tmember SET wallet = '$wallet' WHERE member_id = '$member_id'");

			if ($update_wallet_result) {
				$transDesc = $investment_datas['campaign'] . " - Investment";
				$query_history_wallet = $this->db->exec("INSERT INTO twallet_transaction (member_id,stats,amount,description,date) VALUES ('$member_id','Investment','$invest_amount','$transDesc',NOW())");
				$update_invest_status = $this->db->exec("UPDATE tinv SET status = 'Wallet' WHERE order_id = '$order_id'");
			}else{
				$message_error = "Ops, something when wrong. Please try again.";
			}

			// send email information to admin
		}else{
			$already_paid = '1';
			$this->sendPayWalletInfo($investment_datas);
		}

		$this->smarty->assign("tinv", $investment_datas);
		$this->smarty->assign("already_paid", $already_paid);
		$content = $this->smarty->fetch('wallet_payment.tpl');
		return $content;
	}

	function sendPayWalletInfo($investment) {
		$subject = "Campaign Payment With Wallet - ".$investment['campaign'];
		$message = "

		<html>
			<head>
				<title>Kapital Boost's Proof Submitted</title>
			</head>
			<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
				<div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
					<div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
						<img src='https://kapitalboost.com/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
						<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
							Dear Admin,
						</p>
						<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
							You have a new information of Payment with Wallet
						</p>
						<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
							Member Name                : " . $investment['nama'] . "<br>
							Member Email             : " . $investment['email'] . "<br>
							Campaign Name           : " . $investment['campaign'] . "<br>
							Investment Amount (SGD)           : " . $investment['total_funding'] . "<br>
							Investment Amount (IDR)           : " . number_format($investment['total_funding_id']) . "<br>
							Payment Method           : Pay With Wallet<br>
						</p>

						<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
							Best regards,<br><br>Tech Ninja
						</p>
					</div>
					<div style='width:100%;height:7px;background-color:#00b0fd'></div>
					<div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
						<p style='font-size: 12px;color: white;text-align:center'>
							&copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
						</p>
					</div>
				</div>
			</body>
		</html>

		";

		$this->SendEmailSendGridOld(EMAIL_FROM, "admin@kapitalboost.com", '', $subject, $message, TRUE);
	}
}

?>
