<?php

class Event extends Front {

	function LoadList() {
		if ($_GET['tag']) {
			$tag = str_replace(",", "", $_GET['tag']);
			$sql = "SELECT * FROM events WHERE tag LIKE  '%$tag%' ";
			list($content, ) = $this->db->multiarray($sql);
			$this->smarty->assign("content", $content);
			$this->_setMeta('Ideas on Islamic Financing and Crowdfunding Campaigns | Kapital Boost', 'Event, Kapital Boost', 'Ideas, tips and thoughts on SME funding and Crowd sourcing based on Shariah Principles.  Visit our Event for more content, for enquiries call +65 9865 9648', '', '');
			$content = $this->smarty->fetch('TPL_Event.tpl');
			
			return $content;
		} else {
			list($content, ) = $this->db->multiarray("SELECT * FROM events WHERE enabled = 1 ORDER BY event_id DESC");
			$this->smarty->assign("content", $content);
			$this->_setMeta('Ideas on Islamic Financing and Crowdfunding Campaigns | Kapital Boost', 'Event, Kapital Boost', 'Ideas, tips and thoughts on SME funding and Crowd sourcing based on Shariah Principles.  Visit our Event for more content, for enquiries call +65 9865 9648', '', '');
			
			$this->AddTrail("Visit Event Web", $this->member_id);
			
			$content = $this->smarty->fetch('TPL_Event.tpl');
			
			return $content;
		}
	}

	function GetContent() {
		$id = $_GET['event_id'];
		str_replace($id, "(", "-");
		str_replace($id, ")", "-");
		$sql = "SELECT * FROM events WHERE slug='" . $id . "'";
		list($content, ) = $this->db->singlearray($sql);

		$tags = explode("~",$content["tag"]);
		$this->smarty->assign("content", $content);
		$this->smarty->assign("tags",$tags);
		$this->_setMeta($content["event_title"], "event, Kapital Boost, " , $content['event_title'], 'https://kapitalboost.com/assets/images/event/'.$content["image"], '');
		
		$this->AddTrail("Visit Event Page Web: $id", $this->member_id);

		$content = $this->smarty->fetch('TPL_Event_Detail.tpl');
		return $content;
	}

}

?>