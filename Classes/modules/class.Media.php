<?php
class Media extends Front{

    function LoadList()
	{
		list($content,) = $this->db->multiarray("SELECT * FROM tmedia WHERE enabled = 1 ORDER BY sort ASC");
		
		$this->smarty->assign("content", $content);
		
		$this->AddTrail("Visit Media Web", $this->member_id);
		
		$content = $this->smarty->fetch('TPL_Media.tpl');        
		return $content;
    }
}
?>