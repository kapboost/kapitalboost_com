<?php

class Events extends Front {

	function LoadList() {
		$now = date("Y-m-d H-i-s");
		list($coming, ) = $this->db->singlearray("SELECT * FROM tevent WHERE event_date > '$now' && event_enabled = 1  ORDER BY event_date DESC ");
		list($past,) = $this->db->multiarray("SELECT * FROM tevent WHERE event_date <= '$now' && event_enabled = 1  ORDER BY event_date DESC ");
		$this->smarty->assign("coming", $coming);
		$this->smarty->assign("past",$past);
		$content = $this->smarty->fetch('events.tpl');
		
		return $content;
	}

	function GetContent() {
		$id = (int) $_GET['event_id'];
		$sql = "SELECT * FROM tevent WHERE event_id='" . $id . "'";
		list($content, ) = $this->db->multiarray($sql);
		$this->smarty->assign("content", $content);
		$content = $this->smarty->fetch('TPL_Events_Detail.tpl');
		
		return $content;
	}

}

?>