<?php

class DV_Session {

       var $sid = '';
       var $session_length = 7;

       //Initialize the class
       function DV_Session($mylength = 7) {
              $this->session_length = $mylength;
              $this->ResetSid();
       }

       function ResetSid() {
              $this->sid = md5(uniqid(rand(), 32));
       }

       function FindCookieId($mysession) {

              if ($_SESSION['cookie']['cookie'] == $mysession) {
                     return $_SESSION['cookie']['cookie_id'];
              } else {
                     $data = $this->FindSessionData($mysession);
                     return $data['cookie_id'];
              }
       }

       function FindSessionData($mysession) {
              global $db;

              list( $data, $datacount ) = $db->singlearray("select * from tcookie where cookie='$mysession'");

              $this->SetSessionData($data);

              return $data;
       }

       function SetSessionData($arData) {
              $member_id = isset($_SESSION['cookie']['member_id']) ? $_SESSION['cookie']['member_id'] : '';

              $_SESSION['cookie'] = $arData;

              if (!empty($member_id)) {
                     $_SESSION['cookie']['member_id'] = $member_id;
              }
       }

       function ValidSession($mysession) {

              global $db;

              $timenow = time();

              $cookieData = $this->FindSessionData($mysession);

              if ($DEBUG)
                     echo("SessionVerify cookie_id[{$cookieData[cookie_id]}] session [$mysession]<br>");

              if ($cookieData['cookie_id'] > 0) {
                     /*
                       if ($cookieData['affilate_id'] == '' || $cookieData['affilate_id'] < 1){
                       $gnutime = strtotime($cookieData[affilate_insert_datetime]);
                       $insert_dt = date("U",$gnutime);
                       $diff = $timenow - $insert_dt;
                       $hr = 24 * 60 * 60; // 1 day
                       if ($diff > $hr){
                       return 0;
                       }
                       }
                      */

                     /////////////////////////////////////////////////////////////////////////////////////////////////////////////
                     // check if member login? if so, timed out yet?
                     $memberdatetime = $cookieData['member_insert_datetime'];

                     $member_id = $cookieData['member_id'];
                     $cookie_id = $cookieData['cookie_id'];

                     if ($DEBUG)
                            echo("SessionVerify[$member_id]<br>");

                     if ($member_id > 0) {
                            // check if timed out
                            $gnutime = strtotime($memberdatetime);
                            $membersec = date("U", $gnutime);

                            $diff = $timenow - $membersec;

                            if ($DEBUG)
                                   echo("Member datetime[$memberdatetime] LoginTime[$membersec] TimeNow[$timenow] Diff[$diff] Member id[$member_id]<br>");
                            if ($DEBUG)
                                   echo("LoginTime[$membersec][$timenow][$diff]<br>");

                            if ($diff < $SESSION_EXPIRED) {
                                   // still within the time limit
                                   $db->exec("UPDATE tcookie SET member_insert_datetime=NOW() WHERE cookie='$mysession'");

                                   return 1;
                            } else {
                                   // oops, timeout!
                                   $db->ecec("UPDATE tcookie SET member_id='0', member_insert_datetime=NOW() WHERE cookie='$mysession'");
                                   return 0;
                            }
                     }

                     return 1;
              } else {
                     return 0;
              }
       }

       function MakeSession() {

              global $db;

              $ip = preg_split("/\./", $_SERVER["REMOTE_ADDR"]);

              // Generate a pure random seed
              $myseed = $this->MakeSeed();

              $newsession = $myseed . "-" . $this->sid;

              $fail = 1;

              // Make sure the seed does not already exist, find one that doesnt exist
              while ($mycount = $db->field("SELECT COUNT(*) FROM tcookie WHERE cookie='$newsession'")) {
                     $fail++;
                     $myseed = $this->MakeSeed();

                     $newsession = $myseed . "-" . $this->sid;
              }


              $rs = $db->exec("INSERT INTO tcookie SET
													cookie = '" . $newsession . "',
													insert_dt = NOW(),
													remote_ip = '" . $_SERVER['REMOTE_ADDR'] . "'");

              if ($rs) {

                     $data = $this->FindSessionData($newsession);

                     if ($data['cookie_id'] > 0) {
                            return 1;
                     } else {
                            return 0;
                     }
              } else {
                     return 0;
              }
       }

       function MakeSeed() {

              $genseed = substr(preg_replace("#[^A-Za-z1234567890]#", "", crypt(time())) .
                      preg_replace("#[^A-Za-z1234567890]#", "", crypt(time())) .
                      preg_replace("#[^A-Za-z1234567890]#", "", crypt(time())), 0, $this->session_length);

              return $genseed;
       }

}

?>
