<?php

//****************************************************************
//
// Benjamin Charles Tehan
// benjamin@dv9.com
//
// DV9 International Pte Ltd
// 21A Sembawang Road
// Singapore 779076
// Copyright (C) 2002, All Rights Reserved
//
// Class : upload.class.php
//
//****************************************************************

class UPLOAD {
    var $classname  = "UPLOAD";
    var $UPLOADDEBUG    = FALSE;

    function save($saveto, $file) {


        if ($file != "") {
            if (file_exists($saveto)) { @unlink($saveto); }

            @copy($file, $saveto) or $log = 0;
            if (file_exists($saveto)) {
                $log = 1;
                if ($this->UPLOADDEBUG) {
                    echo ("<!-- UPLOADED $saveto //-->\n");
                }
            }
        }

        return $log;
    }
}

?>

