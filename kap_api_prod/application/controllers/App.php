<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class App extends CI_Controller {

       public function __construct() {
              parent::__construct();

              $this->load->model('forgot_model');
              $this->load->model('register_model');
       }

       function index() {
              $this->load->view('app');
       }

       function getMemberId($email) {
              $query = $this->db->get_where('tmember', array('member_email' => $email));
              $data = $query->row_array();
              $id = $data["member_id"];
              return $id;
       }

       function countDate($date) {
              $now = time();
              $c = floor((strtotime($date) - $now ) / (60 * 60 * 24));
              if ($c < 1) {
                     $res = 0;
              } else {
                     $res = $c;
              }
              return
                      $res;
       }

       function checkMail($email) {
              $query = $this->db->get_where("tmember", array("member_email" => $email));
              if ($query->row_array() == null) {
                     return false;
              } else {
                     return true;
              }
       }

       function checkUsername($username) {
              $query = $this->db->get_where("tmember", array("member_username" => $username));
              if ($query->row_array() == null) {
                     return false;
              } else {
                     return true;
              }
       }
       
       function version(){
              echo json_encode(array("version"=>"1.3")); // only Double value
       }

       public function myinfo() {
              $email = $this->input->post("email");
              $res = [];
              if (empty($email)) {
                     $res["status"] = 0;
              } else {
                     $data = $this->db->get_where('tmember', array('member_email' => $email));
                     $res["status"] = 1;
                     $res["data"] = $data->row_array();
                     
              }
              echo json_encode($res);
       }

       public function manageprofile() {
              $req = json_decode(file_get_contents("php://input"));
              $res = array();
              $res["return"] = $req;
              $this->db->update('tmember', $req, array("member_id" => $req->member_id));
              $res["status"] = 1;
              $res["msg"] = "Success";

              $query = $this->db->get_where('tmember', array("member_id" => $req->member_id));
              $data = $query->row_array();
              $member_title = $data["member_title"];
              $first = $data["member_firstname"];
              $last = $data["member_surname"];
              $email = $data["member_email"];
              $dob = $data["dob"];
              $resarea = $data["residental_area"];
              $member_nationality = $data["member_country"];
              $nric = $data["nric"];
              $current_location = $data["current_location"];
              $xfers_token = $data["member_access_token"];
              $nric_newname = $data["nric_file"];
              $nric_back_newname = $data["nric_file_back"];
              $ap_newname = $data["address_proof"];
              $gender = "";
              if ($member_title == "mr") {
                     $gender = "male";
              } else if ($member_title == "ms" || $member_title == "mrs") {
                     $gender = "female";
              }

              $arr_xudata = array(
                  "first_name" => $first,
                  "last_name" => $last,
                  "email" => $email,
                  "date_of_birth" => $dob,
                  "gender" => $gender,
                  "address_line_1" => $resarea,
                  "nationality" => $member_nationality,
                  "identity_no" => $nric,
                  "country" => $current_location,
                  "id_front_url" => "https://kapitalboost.com/assets/nric/datanric/$nric_newname",
                  "id_back_url" => "https://kapitalboost.com/assets/nric/datanricback/$nric_back_newname",
                  "proof_of_address_url" => "https://kapitalboost.com/assets/nric/addressproof/$ap_newname"
              );
              $xudata = json_encode($arr_xudata);
              $curl = curl_init();
              curl_setopt_array($curl, array(
                  CURLOPT_URL => "https://www.xfers.io/api/v3/user",
                  CURLOPT_RETURNTRANSFER => true,
                  CURLOPT_SSL_VERIFYPEER => false,
                  CURLOPT_ENCODING => "",
                  CURLOPT_MAXREDIRS => 10,
                  CURLOPT_TIMEOUT => 30,
                  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                  CURLOPT_CUSTOMREQUEST => "PUT",
                  CURLOPT_POSTFIELDS => $xudata,
                  CURLOPT_HTTPHEADER => array(
                      "cache-control: no-cache",
                      "content-type: application/json",
                      "postman-token: e6a82e16-8aa8-fe93-0aa8-5f74f10ccb8d",
                      "x-xfers-user-api-key: $xfers_token"
                  ),
              ));

              curl_exec($curl);

              $this->db->update("tmember", array("xfers_info" => $xudata), array("member_email" => $email));

              echo json_encode($res);
       }

       public function manageprofileupload() {
              $val = $this->input->post("val");
              $name = $this->input->post("name");
              if ($val == "" || $val == NULL) {
                     $file_name = time() . rand(0, 100);
                     $config['allowed_types'] = 'gif|jpg|png|pdf|docx|jpeg';
                     $config['upload_path'] = '/var/www/kapitalboost.com/public_html/assets/nric/datanric/';
                     $config["file_name"] = $file_name;
                     $this->load->library('upload', $config);
                     if (!$this->upload->do_upload('file')) {
                            $error = array('error' => $this->upload->display_errors());
                            $res["error"] = $error;
                     } else {
                            $member_email = $this->input->post("email");
                            $tmp_file = $this->upload->data();
                            $file_name = $tmp_file["file_name"];
                            $this->db->update('tmember', array("nric_file" => $file_name), array("member_email" => $member_email));
                            $res["success"] = "success";
                     }
              }

//NRIC
              if ($val == "nric") {
                     $res["val"] = $val;
                     $config_nric['allowed_types'] = 'gif|jpg|png|pdf|docx|jpeg';
                     $config_nric['upload_path'] = '/var/www/kapitalboost.com/public_html/assets/nric/datanric/';
                     $config_nric["file_name"] = $name;
                     $this->load->library('upload', $config_nric);
                     if (!$this->upload->do_upload('nric')) {
                            $error = array('error' => $this->upload->display_errors());
                            $res["error"] = $error;
                     } else {
                            $member_email = $this->input->post("email");
                            
                            $this->db->update('tmember', array("nric_file" => $name), array("member_email" => $member_email));
                            $res["success"] = "success";
                     }
              }

//NRIC Back
              if ($val == "nricback") {
                     $config_nric_back['allowed_types'] = 'gif|jpg|png|pdf|docx|jpeg';
                     $config_nric_back['upload_path'] = '/var/www/kapitalboost.com/public_html/assets/nric/datanricback/';
                     $config_nric_back["file_name"] = $name;
                     $this->load->library('upload', $config_nric_back);
                     if (!$this->upload->do_upload('nricback')) {
                            $error = array('error' => $this->upload->display_errors());
                            $res["error"] = $error;
                     } else {
                            $member_email = $this->input->post("email");
                            $this->db->update('tmember', array("nric_file_back" => $name), array("member_email" => $member_email));
                            $res["success"] = "success";
                     }
              }
              if ($val == "passport.png") {
                     $member_email = $this->input->post("email");
                     $this->db->update('tmember', array("nric_file_back" => 'passport.png'), array("member_email" => $member_email));
                     $res["success"] = "success";
              }

//Address Proof
              if ($val == "addressProof") {
                     $config_ap['allowed_types'] = 'gif|jpg|png|pdf|docx|jpeg';
                     $config_ap['upload_path'] = '/var/www/kapitalboost.com/public_html/assets/nric/addressproof/';
                     $config_ap["file_name"] = $name;
                     $this->load->library('upload', $config_ap);
                     if (!$this->upload->do_upload('addressProof')) {
                            $error = array('error' => $this->upload->display_errors());
                            $res["error"] = $error;
                     } else {

                            $member_email = $this->input->post("email");
                            $this->db->update('tmember', array("address_proof" => $name), array("member_email" => $member_email));
                            $res["success"] = "success";
                     }
              }

              echo json_encode($res);
       }

       public function updatepassword() {
              $member_id = $this->input->post("member_id");
              $newPassword = $this->input->post("new_password");
              $password = md5($newPassword);
              $this->db->update("tmember", array("member_password" => $password), array("member_id" => $member_id));

//echo json_encode($res);
       }

       public function campaigndetails($id = null) {
              $res = array();
              if ($id == null) {
                     $res["status"] = 0;
                     $res["data"] = "No Campaign";
              } else {
                     $now = date("Y-m-d");
                     $query = $this->db->query("SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE enabled=1 && campaign_id = '$id'");
                     $campaign = array();
                     foreach ($query->result() as $row) {
                            $campaign = array(
                                "campaign_id" => $row->campaign_id,
                                "campaign_name" => $row->campaign_name,
                                "campaign_description" => $row->campaign_description,
                                "campaign_country" => $row->country,
                                "campaign_industry" => $row->industry,
                                "campaign_background" => "https://kapitalboost.com/assets/images/campaign/" . $row->campaign_background,
                                "campaign_target" => $row->total_funding_amt,
                                "campaign_funded" => $row->curr_funding_amt,
                                "campaign_returns" => $row->project_return,
                                "campaign_tenor" => $row->funding_summary,
                                "campaign_days_left" => $row->days_left,
                                "campaign_risk" => $row->risk,
                                "minimum_investment" => $row->minimum_investment,
                                "campaign_type" => $row->project_type,
                                "m_payout" => $row->m_payout
                            );
                     }
                     $res["status"] = 1;
                     $res["data"] = $campaign;
              }
              echo json_encode($res);
       }

       public function payment() {
              $email = $this->input->post("email");
              $member_id = $this->getMemberId($email);
              $data = $this->db->order_by("ppid DESC")->get_where('tpaymentpending', array('member_id' => $member_id));
              $res_data = array();
              foreach ($data->result_array() as $key) {
                     $campaign_details = $this->db->get_where('tcampaign', array('campaign_id' => $key['campaign_id']));
                     $tinv_pay = $this->db->get_where('tinv', array('token' => $key['token']));
                     $tinv_pay_data = $tinv_pay->row_array();
                     $tmp = $key;
                     $data_campaign = $campaign_details->row_array();
                     $tmp["invest_date"] = date("d-m-Y", strtotime($key['invest_date']));
                     $tmp["campaign_details"] = $data_campaign;
                     $tmp["clossing_date"] = date("d-m-Y", strtotime($data_campaign['expiry_date']));
                     $tmp["payment_status_tinv"] = $tinv_pay_data["status"];
                     $res_data[] = $tmp;
              }
              $res = array();
              if ($data->result_array() == null) {
                     $res["status"] = 0;
              } else {
                     $res["status"] = 1;
                     $res["data"] = $res_data;
              }
              echo json_encode($res);
       }

       public function portfolio() {
              $email = $this->input->post("email");
              $data = $this->db->order_by("id DESC")->get_where('tinv', array('email' => $email));
              $tinv_data = $data->result_array();
              $res_data = array();

              foreach ($tinv_data as $key) {
                     $query = $this->db->query("SELECT project_return, industry,curr_funding_amt,total_funding_amt,campaign_background FROM tcampaign WHERE campaign_id = " . $key["campaign_id"] . " && enabled  = 1 ORDER BY campaign_id DESC");
                     $tmp = $key;
                     $tmp["campaign_details"] = $query->row_array();
                     $res_data[] = $tmp;
              }

              $res = array();
              if ($data->result_array() == null) {
                     $res["status"] = 0;
              } else {
                     $res["status"] = 1;
                     $res["data"] = $res_data;
              }
              echo json_encode($res);
       }
	   
		function AddTrail($trail, $member_id) {
			if ($member_id == NULL or $member_id=="" or $member_id=="0"){$member_id= $this->getUserIP();}
			$sqls = $this->db->query("INSERT INTO trail(event,member_id,date) VALUES ('$trail','$member_id',now())"); 
			// echo "'<script>console.log(\"trail : $sqls\")</script>'";
			$this->db->exec($sqls);
		}
		
		function getUserIP() {
			$client  = @$_SERVER['HTTP_CLIENT_IP'];
			$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
			$remote  = $_SERVER['REMOTE_ADDR'];

			if(filter_var($client, FILTER_VALIDATE_IP))
			{
				$ip = $client;
			}
			elseif(filter_var($forward, FILTER_VALIDATE_IP))
			{
				$ip = $forward;
			}
			else
			{
				$ip = $remote;
			}

			return $ip;
		}

       public function campaign($type = null) {
              $res = array();
              $now = date("Y-m-d");
              if ($type == null) {
                     exit;
              } else if ($type == "sme") {
                     $query = $this->db->query("SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE project_type='sme' && enabled  = 1 ORDER BY campaign_id DESC");
// $query = $this->db->order_by("campaign_id","DESC")->get_where('tcampaign',array('project_type' => "sme"));
                     $campaign = array();
                     foreach ($query->result() as $row) {
                            $campaign[] = array(
                                "campaign_id" => $row->campaign_id,
                                "campaign_name" => $row->campaign_name,
                                "campaign_description" => substr(strip_tags($row->campaign_snippet), 0, 120),
                                "campaign_country" => $row->country,
                                "campaign_industry" => $row->industry,
                                "campaign_background" => "https://kapitalboost.com/assets/images/campaign/" . $row->campaign_background,
                                "campaign_target" => $row->total_funding_amt,
                                "campaign_funded" => $row->curr_funding_amt,
                                "campaign_returns" => $row->project_return,
                                "campaign_tenor" => $row->funding_summary,
                                "campaign_days_left" => $row->days_left,
                                "campaign_risk" => $row->risk
                            );
                     }
                     $res["status"] = 1;
                     $res["data"] = $campaign;
              }
// End Campaign SME
              else if ($type == "donation") {
                     $query = $this->db->query("SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE project_type='donation' && enabled  = 1 ORDER BY campaign_id DESC");
// $query = $this->db->order_by("campaign_id","DESC")->get_where('tcampaign',array('project_type' => "donation"));
                     $campaign = array();
                     foreach ($query->result() as $row) {
                            $campaign[] = array(
                                "campaign_id" => $row->campaign_id,
                                "campaign_name" => $row->campaign_name,
                                "campaign_description" => substr(strip_tags($row->campaign_snippet), 0, 120),
                                "campaign_country" => $row->country,
                                "campaign_industry" => $row->industry,
                                "campaign_background" => "https://kapitalboost.com/assets/images/campaign/" . $row->campaign_background,
                                "campaign_target" => $row->total_funding_amt,
                                "campaign_funded" => $row->curr_funding_amt,
                                "campaign_returns" => $row->project_return,
                                "campaign_tenor" => $row->funding_summary,
                                "campaign_days_left" => $row->days_left,
                                "campaign_risk" => $row->risk
                            );
                     }
                     $res["status"] = 1;
                     $res["data"] = $campaign;
              } else if ($type == "private") {
                     $password = $this->input->post("password");
                     $query = $this->db->query("SELECT *,DATEDIFF(expiry_date,'$now') AS days_left,DATEDIFF(expiry_date,release_date) AS duration FROM tcampaign WHERE project_type='private' && enabled  = 1 && private_password = '$password'");
// $query = $this->db->get_where('tcampaign',array('project_type' => "private",'private_password'=>$password));
                     $campaign = array();
                     foreach ($query->result() as $row) {
                            $campaign = array(
                                "campaign_id" => $row->campaign_id,
                                "campaign_name" => $row->campaign_name,
                                "campaign_description" => substr(strip_tags($row->campaign_snippet), 0, 120),
                                "campaign_country" => $row->country,
                                "campaign_industry" => $row->industry,
                                "campaign_background" => "https://kapitalboost.com/assets/images/campaign/" . $row->campaign_background,
                                "campaign_target" => $row->total_funding_amt,
                                "campaign_funded" => $row->curr_funding_amt,
                                "campaign_returns" => $row->project_return,
                                "campaign_tenor" => $row->funding_summary,
                                "campaign_days_left" => $this->countDate($row->expiry_date),
                                "campaign_risk" => $row->risk
                            );
                     }
                     $res["status"] = 1;
                     $res["data"] = $campaign;
              } else {
                     $res["status"] = 0;
              }
              echo json_encode($res);
       }

       public function login() {
              $email = $this->input->post("member_email");
              $password = $this->input->post("member_password");
              $password = md5($password);
              $query = $this->db->get_where('tmember', array("member_email" => $email, "member_password" => $password, "Validated" => "1"));
              $res = array();
              if ($query->row_array() == null) {
                     $query1 = $this->db->get_where('tmember', array("member_username" => $email, "member_password" => $password, "Validated" => "1"));
                     if ($query1->row_array() == null) {
                            $res["status"] = 0;
                            $res["msg"] = "We do not recognize that email address or password. Please try again.";
                     } else {
                            $res["status"] = 1;
                            $res["data"] = $query1->row_array();
                     }
              } else {
                     $res["status"] = 1;
                     $res["data"] = $query->row_array();
              }
              echo json_encode($res);
       }

       public function register() {
              $title = $this->input->post("member_title");
              $username = $this->input->post("member_username");
              $email = $this->input->post("member_email");
              $password = $this->input->post("member_password");
              $password = md5($password);
              $lastname = $this->input->post("member_surname");
              $firstname = $this->input->post("member_firstname");
              $country = $this->input->post("member_country");
              $now = date("Y-m-d H-i-s");
              $code = substr(str_shuffle(str_repeat("0123456789", 6)), 0, 6);
              $res = array();
              $registered = $this->checkMail($email);
              $usernameDuplicated = $this->checkUsername($username);
              if ($usernameDuplicated) {
                     $res["status"] = 2;
                     $res["msg"] = "Username Exist!";
              } else {
                     if ($registered) {
                            $res["status"] = 0;
                            $res["msg"] = "Email Registered !";
                     } else {
                            $temp = array(
                                "member_title" => $title,
                                "member_username" => $username,
                                "member_email" => $email,
                                "member_password" => $password,
                                "member_firstname" => $firstname,
                                "member_surname" => $lastname,
                                "member_country" => $country,
                                "Validation_Code" => $code,
                                "insert_dt" => $now
                            );
                            $query = $this->db->insert("tmember", $temp);
                            if ($query) {
                                   $res["status"] = 1;
                                   $res["msg"] = "Successfully";
                                   $this->register_model->send_email($email, $firstname, $code);
                            } else {
                                   $res["status"] = 0;
                                   $res["msg"] = "Error Register !";
                            }
                     }
              }
              echo json_encode($res);
       }

       public function countries() {
              $query = $this->db->get("tcountry");
              $u_data = $query->result_array();
              for ($i = 0; $i < count($u_data); $i++) {
                     $countries[$i] = $u_data[$i]["name"];
              }
              $res["data"] = $countries;
              echo json_encode($res);
       }

       public function nationalities() {
              $query = $this->db->get("tnationality");
              $u_data = $query->result_array();
              for ($i = 0; $i < count($u_data); $i++) {
                     $nationalities[$i] = $u_data[$i]["nationality"];
              }
              $res["data"] = $nationalities;
              echo json_encode($res);
       }

       public function verify() {
              $email = $this->input->post("verify_email");
              $code = $this->input->post("verify_code");
              $res = array();

              $query = $this->db->get_where("tmember", array("member_email" => $email));
              $u_data = $query->row_array();
              if ($code == $u_data["Validation_Code"]) {
                     $this->db->update("tmember", array("Validated" => "1"), array("member_email" => $email));
                     $query1 = $this->db->get_where('tmember', array("member_email" => $email));
                     $res["data"] = $query1->row_array();
                     $res["status"] = "1";
                     $res["email"] = $email;
              } else {
                     $res["status"] = "0";
                     $res["email"] = "failed";
              }
              echo json_encode($res);
       }

       public function forgot() {
              $email = $this->input->post("member_email");
              $res = array();
              $registered = $this->checkMail($email);
              if ($registered) {
                     $res["status"] = 1;
                     $res["msg"] = "Please check your email for instructions to reset your password !";
                     $data = $this->db->get_where('tmember', array("member_email" => $email));
                     $u_data = $data->row_array();
                     $name = $u_data["member_firstname"];
                     $member_id = $u_data["member_id"];
                     $this->forgot_model->send_mail($email, $name, $member_id);
              } else {
                     $res["status"] = 0;
                     $res["msg"] = "Email Not Found !";
              }
              echo json_encode($res);
       }

       public function getfunded() {
              $res = array();
              $attachFile = time();
              $req = array(
                  "fullname" => $this->input->post('fullname'),
                  "email" => $this->input->post('email'),
                  "mobile" => $this->input->post("phone"),
                  "country" => $this->input->post("country"),
                  "company" => $this->input->post("company"),
                  "industri" => $this->input->post("industry"),
                  "year" => $this->input->post("year_established"),
                  "currency" => $this->input->post("currency"),
                  "project_type" => $this->input->post("project_type"),
                  "est_an_rev" => $this->input->post("annual_revenue"),
                  "funding_amount" => $this->input->post("amount"),
                  "ass_tobe_pur" => $this->input->post("assets_to_purchase"),
                  "funding_period" => $this->input->post("funding_period"),
                  "have_purchased_order" => $this->input->post("have_purchase_order"),
                  "attach_file" => $attachFile
              );
              $this->db->insert('tgetfunded', $req);
              $insert_id = $this->db->insert_id();
              if ($insert_id != 0 && $insert_id != NULL) {
                     $res["id"] = $insert_id;
                     $res["success"] = "You have successfully submitted a funding request. We shall contact you within the next 24 hours.";
              } else {
                     $res["success"] = "Error Occured. Please contact us";
              }
              echo json_encode($res);
       }

       public function getfundedupload() {
              $file_name = time() . rand(0, 100);
              $config['allowed_types'] = '*';
              $config['upload_path'] = '/var/www/kapitalboost.com/public_html/assets/images/getfunded/';
              $config["file_name"] = $file_name;
              $this->load->library('upload', $config);
              if (!$this->upload->do_upload('file')) {
                     $error = array('error' => $this->upload->display_errors());
                     $res["error"] = $error;
              } else {
                     $id = $this->input->post("id");
                     $tmp_file = $this->upload->data();
                     $file_name = $tmp_file["file_name"];
                     $this->db->update('tgetfunded', array("attach_file" => $file_name), array("getfunded_id" => $id));
                     $res["error"] = "no error";
              }
              echo json_encode($res);
       }

       public function contactus() {
              $res["office"] = "150 Changi Rd #04-07 Singapore 419973";
              $res["phone"] = "+65 6342 5473";
              $res["email"] = "hello@kapitalboost.com";

              echo json_encode($res);
       }

}
