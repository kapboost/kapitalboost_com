<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pay extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model("payment_model");
	}

	function genRndString($length = 20, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz')
    {
        if($length > 0)
        {
            $len_chars = (strlen($chars) - 1);
            $the_chars = $chars{rand(0, $len_chars)};
            for ($i = 1; $i < $length; $i = strlen($the_chars))
            {
                $r = $chars{rand(0, $len_chars)};
                if ($r != $the_chars{$i - 1}) $the_chars .=  $r;
            }
            return $the_chars;
        }
    }
	public function index()
	{
	}
	public function bank(){
		$email = $this->input->post("member_email");
		$name = $this->input->post("member_name");
		$amount = $this->input->post("amount");
		$campaign_name = $this->input->post("campaign_name");
		$project_type = $this->input->post("project_type");
		$token = $this->input->post("token_user");
		$campaign_id = $this->input->post("campaign_id");
		$req = array(
				"member_name" => $name,
				"member_email" => $email,
				"campaign_name" => $this->input->post("campaign_name"),
				"campaign_country" => $this->input->post("campaign_country"),
				"campaign_owner" => $this->input->post("campaign_owner"),
				"token_user" => $this->input->post("token_user"),
				"project_type" => $this->input->post("project_type"),
				"close" => $this->input->post("close"),
			);
		$id = $this->db->get_where("tinv", array("tx_code" => $this->input->post("token_user")));
		if ($id->row_array() !== null) {
			echo "Already";
			exit;
		} 
		$this->db->set('status', 'waiting');
		$this->db->where('token', $this->input->post("token_user"));
		$this->db->update('tpaymentpending');
		$temp_tinv = array(
				"closing_date" => $this->input->post("close"),
				"date" => date("Y-m-d h:i:s"),
				"nama" => $name,
				"email" => $email,
				"country" => $this->input->post("campaign_country"),
				"campaign" => $campaign_name,
				"tipe" => "Bank Transfer",
				"total_funding" => $amount,
				"status" => "Unpaid",
				"owner" => $this->input->post("campaign_owner"),
				"bukti" => "",
				"tx_code" => $this->input->post("token_user"),
				"token" => $token,
				"bank" => "",
				"project_type" => $project_type,
				"token_pp" => $this->input->post("token_user")
			);
		if ($this->db->insert("tinv", $temp_tinv)) {
			echo "SUccess";
			$this->payment_model->send_mail_bank($email, $name, $amount, $campaign_name, $project_type, $token, $campaign_id);
		}
	}
	public function paypal(){
		$check = $this->db->get_where("tinv", array("token" => $this->input->post("token_user")));
		if ($check->row_array() !== null) {
			echo "Payment Failed";
			exit;
		}
		$email = $this->input->post("member_email");
		$campaign_name = $this->input->post("campaign_name");
		$name = $this->input->post("member_name");
		$amount = $this->input->post("amount");
		$temp_paypal = array(
				"nama" => $name,
				"email" => $email,
				"campaign" => $campaign_name,
				"country" => $this->input->post("campaign_country"),
				"owner" => $this->input->post("campaign_owner"),
				"tipe" => "Paypal",
				"total_funding" => $amount,
				"date" => date("Y-m-d h:i:s"),
				"status" => "Paid",
				"bukti" => "",
				"tx_code" => $this->input->post("tx_code"),
				"token" => $this->input->post("token_user"),
				"bank" => "",
				"project_type" => $this->input->post("project_type"),
				"closing_date" => $this->input->post("close")
			);
		if ($this->db->insert("tinv", $temp_paypal)) {
			$this->payment_model->send_mail_paypal($email, $name, $amount, $campaign_name);
			echo "Success";
		}
		$this->db->set('status', 'close');
		$this->db->where('token', $this->input->post("token_user"));
		$this->db->update('tpaymentpending');
	}
	public function xfers(){
		echo "xfers";
	}
}
/* End of file Pay.php */
/* Location: ./application/controllers/Pay.php */