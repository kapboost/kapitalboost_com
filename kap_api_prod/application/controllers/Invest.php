<?php

if (!defined('BASEPATH'))
       exit('No direct script access allowed');

class Invest extends CI_Controller {

       public function __construct() {
              parent::__construct();
              $this->load->model("invest_model");
       }

       function genRndString($length = 150, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
              if ($length > 0) {
                     $len_chars = (strlen($chars) - 1);
                     $the_chars = $chars{rand(0, $len_chars)};
                     for ($i = 1; $i < $length; $i = strlen($the_chars)) {
                            $r = $chars{rand(0, $len_chars)};
                            if ($r != $the_chars{$i - 1})
                                   $the_chars .= $r;
                     }
                     return $the_chars;
              }
       }

       function OrderId($length = 7, $chars = '1234567890') {
              if ($length > 0) {
                     $len_chars = (strlen($chars) - 1);
                     $the_chars = $chars{rand(0, $len_chars)};
                     for ($i = 1; $i < $length; $i = strlen($the_chars)) {
                            $r = $chars{rand(0, $len_chars)};
                            if ($r != $the_chars{$i - 1})
                                   $the_chars .= $r;
                     }
                     return $the_chars;
              }
       }

       public function index() {
              $campaign_name = $this->input->post("campaign_name");
              $campaign_type = $this->input->post("campaign_type");
              $payment_type = $this->input->post("payment_type");
              $amount = $this->input->post("amount");
              $member_name = $this->input->post("investor_name");
              $lastname = $this->input->post("lastname");
              $member_id = $this->input->post("member_id");
              $email = $this->input->post("member_email");
              $campaign_id = $this->input->post("campaign_id");
              $phone = $this->input->post("phone");
              $campaign_subtype_chosen = $this->input->post("cSubType");
              $campaign_subtype_ship = $this->input->post("cSubTypeShip");
              $member_country = $this->input->post("member_country");
              $token = $this->genRndString();
              $order_id = "P" . $this->OrderId();

              $query = $this->db->get_where('tcampaign', array("campaign_id" => $campaign_id));
              $data = $query->row_array();
              $campaign_country = $data["country"];
              $total_funding_amt = $data["total_funding_amt"];
              $curr_funding_amt = $data["curr_funding_amt"];
			  $funding_amt = intval($curr_funding_amt) + $amount;
              $pReturn = $data["project_return"];
              $payouts = $data["m_payout"];
              $closing_date = $data["expiry_date"];
              $expectedPayout = $amount + ($amount * $pReturn / 100);



	
              $req = array(
                  "member_id" => $member_id,
                  "email" => $email,
                  "campaign_id" => $campaign_id,
                  "tipe" => $payment_type,
                  "date" => date("Y-m-d H:i:s"),
                  "project_type" => $campaign_type,
                  "campaign" => $campaign_name,
                  "total_funding" => $amount,
                  "nama" => $member_name,
                  "expected_payout" => $expectedPayout,
                  "order_id" => $order_id,
                  "token_pp" => $token,
                  "closing_date" => $closing_date,
                  "country" => $campaign_country,
                  "status" => "Unpaid",
              );
				
			// ==============================================================
			// Block for a while
			
			date_default_timezone_set("Asia/Bangkok");
			if (date('H') >= 19 || $campaign_type=="donation") {
			   // echo date('H');
			
			  if ($funding_amt > $total_funding_amt) {
				  $res["status"] = "Your funding exceed our maximum investment";
                  echo json_encode($res);
			  } else if ($this->db->insert("tinv", $req)) {
                     $query2 = $this->db->get_where('tinv', array("token_pp" => $token));
                     $data2 = $query2->row_array();
                     $iId = $data2["id"];
                     if ($payouts != "") {
                            $payoutA = explode("==", $payouts);
                            $amountA = $dateA = $statusA = array();
                            for ($i = 0; $i < count($payoutA); $i++) {
                                   $payout = explode("~", $payoutA[$i]);
                                   $amountA[$i] = isset($payout[0]) ? $payout[0] : null;
                                   $dateA[$i] = isset($payout[1]) ? $payout[1] : null;
                                   $statusA[$i] = isset($payout[2]) ? $payout[2] : null;
                                   if ($amountA[$i] != "") {
                                          $singleAmount = $expectedPayout[$i] * ($amountA[$i] / 100);
                                          $this->UpdatePayout($iId, $i + 1, $singleAmount, $dateA[$i], $statusA[$i]);
                                   }
                            }
                     }
					 $totalFund = $this->countFundAmount($campaign_id);
					$updateFundAmount = $this->db->update("tcampaign", array("curr_funding_amt" => $totalFund), array("campaign_id" => $campaign_id));
					 
					
                     $res["email"] = $this->invest_model->send_email($member_id, $member_name, $lastname, $email, $phone, $amount, $campaign_id, $campaign_name, $campaign_type, $campaign_subtype_chosen, $campaign_subtype_ship, $payment_type, $iId, $token, $member_country, $closing_date, $order_id);
                     
                     $res["status"] = "Success";
                     echo json_encode($res);
              } else {
                     $res["status"] = "error";
                     echo json_encode($res);
			  }
			}
			  // ============================================ End Block for a while

              /* $req = array(
                "member_id" => $member_id,
                "campaign_id" => $this->input->post("campaign_id"),
                "payment_type" => $payment_type,
                "status" => "open",
                "invest_date" => date("Y-m-d"),
                "campaign_type" => $campaign_type,
                "campaign_name" => $campaign_name,
                "amount" => $amount,
                "investor_name" => $member_name,
                "currency" => $this->input->post("currency"),
                "order_id" => $this->OrderId(),
                "campaign_owner" => $this->input->post("campaign_owner"),
                "token" => $token,
                "closing_date" => $this->input->post("closing_date"),
                );

                if ($this->db->insert("tpaymentpending", $req)) {
                $query = $this->db->get_where('tmember', array("member_id" => $member_id));
                $data = $query->row_array();
                $email = $data["member_email"];
                $query2 = $this->db->get_where('tpaymentpending', array("token" => $token));
                $data2 = $query2->row_array();
                $ppid = $data2["ppid"];
                $this->invest_model->send_email($email, $member_name, $amount, $campaign_name, $campaign_type, $payment_type, $ppid);
                echo "Success";
                } else {
                echo "error";
                } */
       }
	   
	    function countFundAmount($campaign_id) {

			 $this->db->select('SUM(total_funding) as total_funding')
			  ->from('tinv')
			  ->where('campaign_id', $campaign_id);

			 $query = $this->db->get();

			 if ($query->num_rows() > 0) {
				 return $query->row()->total_funding;
			 } else {
				return false; 
			 }
			 
		}
       
       function UpdatePayout($iId, $item, $amount, $date, $status) {
              $date = date("Y-m-d", strtotime($date));
            //   $sql = "UPDATE tinv SET bulan_$item = '$amount',tanggal_$item = '$date',status_$item = '$status' WHERE id = $iId";
            //   $this->db->field($sql);
              $this->db->update("tinv", array("bulan_$item" => $amount), array("id" => $iId));
              $this->db->update("tinv", array("tanggal_$item" => $date), array("id" => $iId));
              $this->db->update("tinv", array("status_$item" => $status), array("id" => $iId));
       }

}

/* End of file Invest.php */
/* Location: ./application/controllers/Invest.php */