<?php

if (!defined('BASEPATH'))
       exit('No direct script access allowed');

class Getfunded_model extends CI_Model {

       public function __construct() {
              parent::__construct();
       }

       public function send_mail($email, $fullname, $mobile, $country, $company, $industri, $year, $currency, $project_type, $est_an_rev, $funding_amount, $ass_tobe_pur, $funding_period, $have_purchased_order) {
              $to = $email;
              $subject = '[INFO] Submitted for Funding';
              $message = "
		    			<html>
						<head>
						  <title>Kapitalboost</title>
						</head>
						<style type='text/css'>
						@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
						</style>
						<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;font-family: 'Open Sans';'>
						<table height='100%' width='100%' bgcolor='#305e8e'>
						<tbody>
						<tr>
						<td style='height: 100%;width: 100%;padding: 20px 10%;' bgcolor='#305e8e'>
						<center>
						  <div style='display: inline-block;position: relative;height: auto;width: 640px;padding: 0 0 10px 0;margin: 0 0 0 0;background: #00b0fd;'>
						    <div style='display: inline-block;position: relative;height: auto;width: 100%;padding: 0;margin: 0;background: white;text-align: left;'>
						      <img src='https://kapitalboost.com/assets/images/logo-baru.png' style='margin: 35px 0 0 35px; max-width: 200px;' />
						       <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
						        You have a new requested  project/campaign<br />
						      </p>
						      
						      <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
						        Fullname                    : " . $fullname . "<br>
						        Email Address               : " . $email . "<br>
						        Mobile Number               : " . $mobile . "<br>
						        Country                     : " . $country . "<br>
						        Company                     : " . $company . "<br>
						        Industri                    : " . $industri . "<br>
						        Year established            : " . $year . "<br>
						        Currency                    : " . $currency . "<br>     
						        Project Type                : " . $project_type . "<br>
						        Est annual revenue          : " . $est_an_rev . "<br>
						        Funding amount required     : " . $funding_amount . "<br>
						        Assets to be purchased      : " . $ass_tobe_pur . "<br>
						        Funding period              : " . $funding_period . "<br>
						        Have purchase orders        : " . $have_purchased_order . "<br>
						           
						      </p>
						    </div>
						  </div>
						  <div style='display: inline-block;position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
						    <p style='font-size: 12px;color: white;'>
						      &copy; COPYRIGHT 2015 - KAPITAL BOOST PTE LTD
						    </p>
						  </div>
						</center>
						</td>
						</tr>
						</tbody>
						</table>
						</body>
						</html>
		    ";
              $headers[] = 'MIME-Version: 1.0';
              $headers[] = 'Content-type: text/html; charset=iso-8859-1';
              $headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';

              include_once 'Admin_model.php';
              $se = new Admin_model();
              $se->SendEmailSendGridOld("admin@kapitalboost.com",$to, 'admin@kapitalboost.com', $subject, $message, TRUE);
              
       }

}

/* End of file Getfunded_model.php */
/* Location: ./application/models/Getfunded_model.php */