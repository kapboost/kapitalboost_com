<?php

if (!defined('BASEPATH')) {
       exit('No direct script access allowed');
}

class Admin_model extends CI_Model {

       public function __construct() {
              parent::__construct();
       }

       public function SendEmail($from, $to, $bcc, $subject, $message, $html = FALSE) {
              include_once('class.phpmailer.php');
              date_default_timezone_set('Asia/Singapore');
              $mail = new PHPMailer();
              $mail->SetLanguage("en","language/");
              $mail->From = $from;
              $mail->FromName = "Kapital Boost";
              if (is_array($to)) {
                     foreach ($to as $to_email) {
                            $mail->AddAddress($to_email);
                     }
              } else {
                     $mail->AddAddress($to);
              }

              if (is_array($bcc)) {
                     foreach ($bcc as $to_email) {
                            $mail->AddBCC($to_email);
                     }
              } else {
                     if ($bcc != '') {
                            $mail->AddBCC($bcc);
                     }
              }
              $mail->IsHTML($html);
              $mail->Subject = $subject;
              $mail->Body = $message;
              $mail->Encoding = "base64";
              //$mail->CharSet = "UTF-8";
              $mail->AddReplyTo("support@kapitalboost.com", "Reply to Kapital Boost");
              $mail->addCustomHeader("X-AntiAbuse: This header was added to track abuse, please include it with any abuse report");
              $mail->addCustomHeader("X-AntiAbuse: Primary Hostname - kapitalboost.com");
              $mail->addCustomHeader("X-AntiAbuse: Sender Address Domain - kapitalboost.com");
              $mail->addCustomHeader("X-Authenticated-Sender: info@kapitalboost.com");
              $mail->addCustomHeader("X-Source-Dir: :/kapitalboost.com/public_html");
              
              $mail->Send();
       }
	   
	   function SendEmailSendGridOld($from, $to, $bcc, $subject, $message, $html = FALSE) {
			include_once('sendgrid-php/sendgrid-php.php');
			date_default_timezone_set('Asia/Singapore');
			$subject = $subject;
			$to = new SendGrid\Email($to, $to);
			$content_email = $message;
			$content = new SendGrid\Content("text/html", $content_email);
			$from = new SendGrid\Email("Kapital Boost", "info@kapitalboost.com");
			$mail = new SendGrid\Mail($from, $subject, $to, $content);
			$apiKey = "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";
			$sg = new \SendGrid($apiKey);
			$response = $sg->client->mail()->send()->post($mail);
			
		}

}
