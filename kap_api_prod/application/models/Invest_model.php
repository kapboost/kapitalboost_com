<?php

if (!defined('BASEPATH'))
       exit('No direct script access allowed');

class Invest_model extends CI_Model {

       public function __construct() {
              parent::__construct();
       }

       /* function msg_email_paymanet($member_name, $amount, $campaign_name, $campaign_type, $payment_type, $ppid) {
         if ($campaign_type != "donation" && $payment_type == "Bank transfer") {
         $content = '

         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Thank you for your commitment of <br>
         <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Please allow us one working day to prepare your contract. Once ready, it will be sent to your registered email address. Do keep a lookout for it.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Meanwhile, you may proceed to make payment to Kapital Boost according to the instructions below. Please note that completion of the investment process is conditional upon successful payment transfer and electronic signing of the Contract.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         <b>Payment instructions</b> <br>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         (i) Please transfer to the following account:
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Account name: <b>Kapital Boost Pte Ltd</b><br>
         Bank name: <b>Overseas Chinese Banking Corp. (OCBC)</b><br>
         Account number: <b>6232 3373 1001</b><br>
         SWIFT code: <b>OCBCSGSG</b><br>
         Bank code: <b>7339</b><br>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         For fund transfer from Hong Kong, Indonesia or Malaysia, we recommend using www.transwap.com to save on transfer cost. For fund transfer from other parts of the world, we recommend using www.transferwise.com.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         (ii) Once transferred, please submit the proof of payment by clicking the button below.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         <a href="https://kapitalboost.com/dashboard/payment-pending" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">Confirm Payment</a>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         To view your list of payments, please visit the <a href="https://kapitalboost.com/dashboard/payment-pending" style="text-decoration:none">Payment</a> page on your dashboard.
         </p>
         ';
         }
         if ($campaign_type == "donation" && $payment_type == "Bank transfer") {
         $content = '
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Thank you for your donation of <br>
         <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Please proceed to make payment to Kapital Boost according to the instructions below.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         <b>Payment instructions</b>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         (i) Please transfer funds to the following account:
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Account name: <b>Kapital Boost Pte Ltd</b><br>
         Bank name: <b>Overseas Chinese Banking Corp. (OCBC)</b><br>
         Account number: <b>6232 3373 1001</b><br>
         SWIFT code: <b>OCBCSGSG</b><br>
         Bank code: <b>7339</b><br>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         For fund transfer from Hong Kong, Indonesia or Malaysia, we recommend using www.transwap.com to save on transfer cost. For fund transfer from other parts of the world, we recommend using www.transferwise.com.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         (ii) Once transferred, please submit the proof of payment by clicking the button below.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         <a href="https://kapitalboost.com/dashboard/payment-pending" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">Confirm Payment</a>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         To view your list of payments, please visit the <a href="https://kapitalboost.com/dashboard/payment-pending" style="text-decoration:none">Payment</a> page on your dashboard.
         </p>
         ';
         }
         if ($campaign_type == "donation" && $payment_type == "Paypal" OR $payment_type == "paypal") {
         $content = '
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         Thank you for your donation of
         <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         You may proceed to make payment using Paypal by clicking on the link below.
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         <a class="button-kpb-g" href="https://kapitalboost.com/dashboard/payment-api?ppid=' . $ppid . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
         Pay Now
         </a>
         </p>
         <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
         To view your list of payments,
         please visit the
         <a href="https://kapitalboost.com/dashboard/payment-pending" style="text-decoration:none">Payment</a>
         page on your dashboard.
         </p>
         ';
         }

         $msg = "
         <html>
         <head>
         <title>Kapitalboost</title>
         </head>
         <style type='text/css'>
         @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
         </style>
         <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
         <table height='100%' width='100%' bgcolor='#305e8e'>
         <tbody>
         <tr>
         <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
         <center>
         <div style='display: inline-block;width: 640px;background: #00b0fd;'>
         <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
         <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
         <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
         Dear " . $member_name . ",
         </p>
         <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
         " . $content . "
         </p>
         <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
         Best regards, <br>
         Kapital Boost
         </p>
         </div>
         <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
         <p style='font-size: 16px;color: white;width:100%'>
         &copy; COPYRIGHT 2017 - KAPITAL BOOST PTE LTD
         </p>
         </div>
         </div>
         </center>
         </td>
         </tr>
         </tbody>
         </table>
         </body>
         </html>
         ";
         return $msg;
         } */

       function msg_email_paymanet($member_id, $member_name, $lastname, $email, $phone, $amount, $campaign_id, $campaign_name, $campaign_type, $campaign_subtype_chosen, $campaign_subtype_ship, $payment_type, $ppid, $token, $member_country, $closing_date, $order_id) {
              if ($campaign_subtype_chosen == '') {
                     $donationText = "donation";
              } else {
                     $donationText = "funding";
              }
              $reward = "";
              if ($campaign_subtype_chosen == 'reward') {
                     if ($campaign_subtype_ship == "1") {
                            
                     } else {
                            
                     }
                     //$reward = "<p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>The owner of $campaign_name campaign will be in touch with you to request for information such as mailing address etc.</p>";
              }
              $content = "";
              if ($campaign_type != "donation" && $payment_type == "Bank Transfer") {
                     $content = '<p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              Thank you for your commitment of 
                              <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              We are currently preparing your Investment Agreement, which will be sent to your email within 1 working day.
</p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              For payment, please transfer to the following account:
                            </p>
                            
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              Account name: <b>Kapital Boost Pte Ltd</b><br>
							Bank name: <b>CIMB Bank Berhad, Singapore</b><br>
							Account number: <b>2000 562 641</b><br>
							SWIFT code: <b>CIBBSGSG</b><br> 
							Bank code: <b>7986</b><br>
							Branch code: <b>001</b><br>
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                           If you are transferring from overseas, we accept the following payment methods:
                            </p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									(1) <b><a href="https://transwap.com/">Transwap</a></b> - for fund transfer from Hong Kong, Indonesia or Malaysia<br>
									(2) <b><a href="https://transferwise.com/">Transferwise</a></b> or <b><a href="https://www.xendpay.com/">Xendpay</a></b>  - for fund transfer from other parts of the world<br>
								</p>
							<p style="margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;">
									If the above methods are not available in your country, kindly email us so we can advise you accordingly. 
								</p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              Once transferred, please submit the proof of payment via the button below. 
                            </p>
 <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                             <a href="https://kapitalboost.com/dashboard/confirm/bank?type=' . $campaign_type . '&campaign_id=' . $campaign_id . '&amount=' . $amount . '&campaign_name=' . $campaign_name . '&token=' . $token . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">Attach transfer slip</a> 
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              To check on your payment status, please visit the <a href="https://kapitalboost.com/member/investment.php" style="text-decoration:none">Portfolio</a> page on your Investor Dashboard. .
                            </p>
              ';
              }
              if ($campaign_type == "donation" && $payment_type == "Bank Transfer") {
                     $content = '
                                 <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                                  Thank you for your ' . $donationText . ' of 
                                  <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
                                </p>' . $reward . '
                                <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                                  For payment, please proceed to make a bank transfer to the following account:
                                </p>
                                
                                <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                                  Account name: <b>Kapital Boost Pte Ltd</b><br>
									Bank name: <b>CIMB Bank Berhad, Singapore</b><br>
									Account number: <b>2000 562 641</b><br>
									SWIFT code: <b>CIBBSGSG</b><br> 
									Bank code: <b>7986</b><br>
									Branch code: <b>001</b><br>
                                </p>
<p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                            For fund transfer from Hong Kong, Indonesia or Malaysia, we recommend using www.transwap.com to save on transfer cost. For fund transfer from other parts of the world, we recommend using www.transferwise.com. 
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              Once transferred, please submit the proof of payment by clicking the button below. 
                            </p>
 <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                             <a href="https://kapitalboost.com/dashboard/confirm/bank?type=' . $campaign_type . '&campaign_id=' . $campaign_id . '&amount=' . $amount . '&campaign_name=' . $campaign_name . '&token=' . $token . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">Attach transfer slip</a> 
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              To check on payment status, please visit the <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a> page on your dashboard.
                            </p>
              ';
              }
              if ($campaign_type != "donation" && $payment_type == "Xfers" OR $payment_type == "xfers") {
                     $content = '
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              Thank you for your commitment of 
                              <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
                            </p>
                                                        
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              You have selected to make payment via Xfers. Kindly note that only those with Internet banking enabled Singapore bank accounts are eligible to use Xfers. To proceed with payment, please click on the link below.
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              <a href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '?payment_type=xfers&tokenpp=' . $token . '&member_id=' . $member_id . '&username=' . $member_name . '&lastname=' . $lastname . '&member_email=' . $email . '&phone=' . $phone . '&amount=' . $amount . '&campaign_id=' . $campaign_id . '&campaign_name=' . $campaign_name . '&campaign_type=' . $campaign_type . '&member_country=' . $member_country . '&closing_date=' . $closing_date . '&order_id=' . $order_id . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 10px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
                                Pay Now
                              </a>
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              To check on your payment status, please visit the <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a> page on your dashboard.
                            </p>
              ';
              }
              if ($campaign_type == "donation" && $payment_type == "Xfers" OR $payment_type == "xfers") {
                     $content = '
                          <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                            Thank you for your ' . $donationText . ' of 
                            <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.

                          </p>' . $reward . '
                          <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                            You have selected to make payment via Xfers.
                            Kindly note that only those with Internet banking enabled Singapore bank accounts are eligible to use Xfers.
                            To proceed with payment,
                            please click on the link below.
                          </p>
                          <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              <a href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '?payment_type=xfers&tokenpp=' . $token . '&member_id=' . $member_id . '&username=' . $member_name . '&lastname=' . $lastname . '&member_email=' . $email . '&phone=' . $phone . '&amount=' . $amount . '&campaign_id=' . $campaign_id . '&campaign_name=' . $campaign_name . '&campaign_type=' . $campaign_type . '&member_country=' . $member_country . '&closing_date=' . $closing_date . '&order_id=' . $order_id . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 10px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
                              Pay Now
                              </a>
                          </p>
                          <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                            To check on your payment status, please visit the <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a> page on your dashboard.
                          </p>
              ';
              }

              if ($campaign_type != "donation" && $payment_type == "Paypal") {
                     $content = '
                            <p style="margin:20px 0;line-height:1.5em;">
                              Thank you for your donation of 
                              <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.

                            </p>
                            <p style="margin:20px 0;line-height:1.5em;">
                              Please allow us one working day to prepare your Contract. Once ready, it will be sent to your registered email address. Do keep a lookout for it.
                            </p>
                            <a href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 10px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
                              Pay Now
                            </a>
                            <p style="margin:20px 0;line-height:1.5em;">
                              Please note that completion of the investment process is conditional upon successful payment transfer and electronic signing of the Contract.
                            </p>
                            <p style="margin:20px 0;line-height:1.5em;">
                              To check on your payment status, please visit the <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a> page on your dashboard.
                            </p>

              ';
              }

              if ($campaign_type == "donation" && $payment_type == "Paypal") {
                     $content = '
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              Thank you for your ' . $donationText . ' of 
                              <b>SGD ' . $amount . '</b> in the <b>' . $campaign_name . '</b> campaign.
                            </p>' . $reward . '
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                            For payment, please proceed to PayPal website via the button below
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              <a class="button-kpb-g" href="https://kapitalboost.com/dashboard/payment-detail/' . $ppid . '" style="background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;">
                                Pay Now
                              </a>
                            </p>
                            <p style="display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;">
                              To check on your payment status, please visit the 
                              
                              <a href="https://kapitalboost.com/dashboard/payment-history" style="text-decoration:none">Portfolio</a>
                              page on your dashboard.
                            </p>
              ';
              }

              $msg = "
            <html>
            <head>
              <title>Kapitalboost</title>
            </head>
            <style type='text/css'>
            @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
            </style>
            <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
              <table height='100%' width='100%' bgcolor='#305e8e'>
                <tbody>
                  <tr>
                    <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
                      <center>
                        <div style='display: inline-block;width: 640px;background: #00b0fd;'>
                          <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                            <img src='https://kapitalboost.com/assets/images/logo-baru.png' style='margin: 35px 0 0 35px; max-width: 200px; height: auto;' />
                            <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                             Dear " . $member_name . ",
                            </p>
                            <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;color: white'>
                             " . $content . "
                            </p>
                            <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                              Best regards, <br><br>
                              Kapital Boost 
                            </p>
                          </div>
                          <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                            <p style='font-size: 16px;color: white;width:100%'>
                              &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
                            </p>
                          </div>
                        </div>
                      </center>
                    </td>
                  </tr>
                </tbody>
              </table>
            </body>
            </html>

            ";
              return $msg;
       }

       public function send_email($member_id, $member_name, $lastname, $email, $phone, $amount, $campaign_id, $campaign_name, $campaign_type, $campaign_subtype_chosen, $campaign_subtype_ship, $payment_type, $ppid, $token, $member_country, $closing_date, $order_id) {
              $to = $email;
              if ($campaign_type == 'donation') {
                     $subject = "Intent of donation";
              } else{
                     $subject = 'Intent of investment';
              }

              $message = $this->msg_email_paymanet($member_id, $member_name, $lastname, $email, $phone, $amount, $campaign_id, $campaign_name, $campaign_type, $campaign_subtype_chosen, $campaign_subtype_ship, $payment_type, $ppid, $token, $member_country, $closing_date, $order_id);
              $headers[] = 'MIME-Version: 1.0';
              $headers[] = 'Content-type: text/html; charset=iso-8859-1';
              $headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';
              
              include_once 'Admin_model.php';
              $se = new Admin_model();
              $se->SendEmailSendGridOld("admin@kapitalboost.com",$email, 'admin@kapitalboost.com', $subject, $message, TRUE);
              
              return $email;
       }
       
       
}

/* End of file Pay_model.php */
/* Location: ./application/models/Pay_model.php */