<?php

if (!defined('BASEPATH'))
       exit('No direct script access allowed');

class Forgot_model extends CI_Model {

       public function __construct() {
              parent::__construct();
       }

       public function send_mail($email, $name, $member_id) {
              $encrypt = md5(1290 * 3 + $member_id);
              $to = $email;
              $subject = 'Reset Password';

              $message = "
					<html>
                      <head>
                        <title>Kapital Boost</title>
                      </head>
                      <style type='text/css'>
                      @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
                      a:hover {
                          cursor: pointer;
                          text-decoration: none;
                      }
                      </style>
                      <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;font-family: 'Open Sans';'>
                      <table height='100%' width='100%' bgcolor='#305e8e'>
                      <tbody>
                      <tr>
                      <td style='height: 100%;width: 100%;padding: 20px 10%;' bgcolor='#305e8e'>
                      <center>
                        <div style='display: inline-block;position: relative;height: auto;width: 640px;padding: 0 0 10px 0;margin: 0 0 0 0;background: #00b0fd;'>
                          <div style='display: inline-block;position: relative;height: auto;width: 100%;padding: 0;margin: 0;background: white;text-align: left;'>
                            <img src='https://kapitalboost.com/assets/images/logo-baru.png' style='margin: 35px 0 0 35px;max-width: 200px;' />
                            <p style='display: block;font-size: 24px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                              Hi " . $name . ",
                            </p>
                            <p style='display: block;font-size: 18px;text-align: left;margin: 0 30px 30px 35px;'>
                              Click below to reset your password:
                            </p>
                            <a href='https://kapitalboost.com/reset/addpass?encrypt=" . $encrypt . "&action=reset' style='position: relative;background: #00b0fd;outline: none;border: none;padding: 15px 30px;margin: 0 35px 10px 35px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;'>
                              Reset Password
                            </a>
                            <p style='display: block;font-size: 18px;text-align: left;margin: 30px 35px 35px 35px;'>
                              If you have any questions or concerns, please feel free to contact support@kapitalboost.com.
                            </p>
                                  <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
                                    Best regards,<br />
                                    Kapital Boost team
                                  </p>
                          </div>
                        </div>
                        <div style='display: inline-block;position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                          <p style='font-size: 12px;color: white;'>
                            &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
                          </p>
                        </div>
                      </center>
                      </td>
                      </tr>
                      </tbody>
                      </table>
                      </body>
                     </html>
		";
              $headers[] = 'MIME-Version: 1.0';
              $headers[] = 'Content-type: text/html; charset=iso-8859-1';
              $headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';

              include_once 'Admin_model.php';
              $se = new Admin_model();
              $se->SendEmailSendGridOld("admin@kapitalboost.com",$to,"admin@kapitalboost.com", $subject, $message, TRUE);
              
       }

}

/* End of file Forgot_model.php */
/* Location: ./application/models/Forgot_model.php */