<?php

if (!defined('BASEPATH'))
       exit('No direct script access allowed');

class Payment_model extends CI_Model {

       public function __construct() {
              parent::__construct();
       }

       public function send_mail_paypal($email, $name, $amount, $campaign_name) {
              $to = $email;
              $subject = 'Payment Completed';

              $message = "
		    			<html>
	                      <head>
	                        <title>Kapitalboost</title>
	                      </head>
	                      <style type='text/css'>
	                      @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
	                      </style>
	                      <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
	                        <table height='100%' width='100%' bgcolor='#305e8e'>
	                          <tbody>
	                            <tr>
	                              <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
	                                <center>
	                                  <div style='display: inline-block;width: 640px;background: #00b0fd;'>
	                                    <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
	                                      <img src='https://kapitalboost.com/assets/images/logo-baru.png' style='margin: 35px 0 0 35px;max-width: 200px;' />
	                                      <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
	                                       Dear " . $name . ",
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        Thank you for your generous donation of <b>SGD " . $amount . "</b> for the <b>" . $campaign_name . "</b> project.
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        May your kindness be rewarded in multiplefold.
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        Best regards, <br>
	                                        Kapital Boost team
	                                      </p>
	                                    </div>
	                                    <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
	                                      <p style='font-size: 16px;color: white;width:100%'>
	                                        &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
	                                      </p>
	                                    </div>
	                                  </div>
	                                </center>
	                              </td>
	                            </tr>
	                          </tbody>
	                        </table>
	                      </body>
	                      </html>
		    ";
              $headers[] = 'MIME-Version: 1.0';
              $headers[] = 'Content-type: text/html; charset=iso-8859-1';
              $headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';

              mail($to, $subject, $message, implode("\r\n", $headers));
       }

       public function send_mail_bank($email, $name, $amount, $campaign_name, $project_type, $token, $campaign_id) {
              $to = $email;
              $subject = 'Payment Confirmation';

              if ($project_type == "donation") {
                     $message = "
	                      <html>
	                      <head>
	                        <title>Kapitalboost</title>
	                      </head>
	                      <style type='text/css'>
	                      @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
	                      </style>
	                      <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
	                        <table height='100%' width='100%' bgcolor='#305e8e'>
	                          <tbody>
	                            <tr>
	                              <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
	                                <center>
	                                  <div style='display: inline-block;width: 640px;background: #00b0fd;'>
	                                    <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
	                                      <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
	                                      <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
	                                          Dear " . $name . ",
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        Thank you for your donation of <b>SGD " . $amount . "</b> to the <b>" . $campaign_name . "</b> project.
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        Please proceed to make payment to Kapital Boost according to the instructions below.
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        <b>Payment instructions</b>
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        (i) Please transfer to the following account:
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        Account name: <b>Kapital Boost Pte Ltd</b><br>
	                                        Bank name: <b>Overseas Chinese Banking Corp. (OCBC)</b><br>
	                                        Account number: <b>6232 3373 1001</b><br>
	                                        SWIFT code: <b>OCBCSGSG</b><br>
	                                        Bank code: <b>7339</b><br>
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        (ii) Once transferred, please submit to us the proof of payment by clicking the button below.
	                                      </p>
	                                      <br>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        <a href='https://kapitalboost.com/dashboard/confirm/bank?token=" . $token . "&campaign_name=" . $campaign_name . "&action=confirm?type=" . $project_type . "&amount=" . $amount . "&campaign_id=" . $campaign_id . "' 
	                                        style='background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;'>
	                                          Confirm Payment
	                                        </a>
	                                      <p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        <br>
	                                        To check on the payment status of your donation, click on <a href='https://kapitalboost.com/dashboard/payment-pending' style='text-decoration:none'>Payment</a> under the Dashboard.
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                          If you have questions, please email <a href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
	                                      </p>
	                                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
	                                        Best regards, <br>
	                                        Kapital Boost 
	                                      </p>
	                                    </div>
	                                    <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
	                                      <p style='font-size: 16px;color: white;width:100%'>
	                                        &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
	                                      </p>
	                                    </div>
	                                  </div>
	                                </center>
	                              </td>
	                            </tr>
	                          </tbody>
	                        </table>
	                      </body>
	                      </html>
			    ";
              } else {
                     $message = "
			        <html>
			        <head>
			          <title>Kapitalboost</title>
			        </head>
			        <style type='text/css'>
			        @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
			        </style>
			        <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
			          <table height='100%' width='100%' bgcolor='#305e8e'>
			            <tbody>
			              <tr>
			                <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
			                  <center>
			                    <div style='display: inline-block;width: 640px;background: #00b0fd;'>
			                      <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
			                        <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
			                        <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
			                          Dear " . $name . ",
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          Thank you for your committment of <b>SGD " . $amount . "</b> to the <b>" . $campaign_name . "</b> crowdfunding deal.
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          Please proceed to make payment to Kapital Boost according to the instructions below.
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          <b>Payment instructions</b>
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          (i) Please transfer to the following account:
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          Account name: <b>Kapital Boost Pte Ltd</b><br>
			                          Bank name: <b>Overseas Chinese Banking Corp. (OCBC)</b><br>
			                          Account number: <b>6232 3373 1001</b><br>
			                          SWIFT code: <b>OCBCSGSG</b><br>
			                          Bank code: <b>7339</b><br>
			                        </p>

			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          (ii) Once transferred, please submit to us the proof of payment by clicking the button below.
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          <a href='https://kapitalboost.com/dashboard/confirm/bank?token=" . $token . "&campaign_name=" . $campaign_name . "&action=confirm?type=" . $project_type . "&amount=" . $amount . "&campaign_id=" . $campaign_id . "' 
			                           style='background-color: #00b0fd;outline: none;border-radius: 5px;padding: 15px 30px;font-size: 18px;color: white;font-weight: bold;cursor: pointer;text-decoration: none;'>
			                            Confirm Payment
			                          </a>
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          To check on the status and repayment schedules of your investments, click on Portfolio under the Dashboard.
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                            If you have questions, please email <a style=\"text-decoration:none\" href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
			                        </p>
			                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
			                          Best regards, <br>
			                          Kapital Boost 
			                        </p>
			                      </div>
			                      <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
			                        <p style='font-size: 16px;color: white;width:100%'>
			                          &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
			                        </p>
			                      </div>
			                    </div>
			                  </center>
			                </td>
			              </tr>
			            </tbody>
			          </table>
			        </body>
			        </html>
	        	";
              }
              $headers[] = 'MIME-Version: 1.0';
              $headers[] = 'Content-type: text/html; charset=iso-8859-1';
              $headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';

              mail($to, $subject, $message, implode("\r\n", $headers));
       }

}

/* End of file Portofolio_model.php */
/* Location: ./application/models/Portofolio_model.php */