<?php

if (!defined('BASEPATH'))
       exit('No direct script access allowed');

class Register_model extends CI_Model {

       public function __construct() {
              parent::__construct();
       }

       public function send_email($email, $name,$code) {
              $to = $email;
              $subject = "Account Verification Code - $code";

              $message = "
					<html>
                        <head>
                          <title>Kapital Boost</title>
                        </head>
                        <style type='text/css'>
                        @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
                        a {
                          text-decoration: none;
                        }
                        a:hover {
                          text-decoration: none;
                          cursor: pointer;
                        }
                        button {
                          cursor: pointer;
                        }
                        button:hover {
                          cursor: pointer;
                        }
                        </style>
                        <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;font-family: 'Open Sans';'>
                        <table height='100%' width='100%' bgcolor='#305e8e'>
                        <tbody>
                        <tr>
                        <td style='height: 100%;width: 100%;padding: 20px 10%;' bgcolor='#305e8e'>
                        <center>
                          <div style='display: inline-block;position: relative;height: auto;width: 640px;padding: 0 0 10px 0;margin: 0 0 0 0;background: #00b0fd;'>
                            <div style='display: inline-block;position: relative;height: auto;width: 100%;padding: 0;margin: 0;background: white;text-align: left;'>
                              <img src='https://kapitalboost.com/assets/images/logo-baru.png' style='margin: 35px 35px 0 35px; max-width:150px !important;' />
                              <p style='display: block;font-size: 24px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                                Hi " . $name . ",
                              </p>
                              <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 30px 35px;'>
                                Your account is almost ready! To activate, please use the 6-digit code below to verify in our app.
                              </p>
                              <p style='display: block;font-size: 40px;text-align: left;margin: 0 35px 30px 35px;letter-spacing:20px'>
                                                 $code
                                          </p>
                              <p style='display: block;font-size: 18px;text-align: left;margin: 30px 35px 35px 35px;'>
                                If you have any questions, feel free to contact us at <a href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
                              </p>
                              <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
                                Best regards,<br />
                                Kapital Boost team
                              </p>
                            </div>
                          </div>
                          <div style='display: inline-block;position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                            <p style='font-size: 12px;color: white;'>
                              &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
                            </p>
                          </div>
                        </center>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        </body>
                    </html>
		";
              $headers[] = 'MIME-Version: 1.0';
              $headers[] = 'Content-type: text/html; charset=iso-8859-1';
              $headers[] = 'From: Kapital Boost <admin@kapitalboost.com>';

              include_once 'Admin_model.php';
              $se = new Admin_model();
              $se->SendEmailSendGridOld("admin@kapitalboost.com",$to, 'admin@kapitalboost.com', $subject, $message, TRUE);
              
              //mail($to, $subject, $message, implode("\r\n", $headers));
       }

}

/* End of file Register_model.php */
/* Location: ./application/models/Register_model.php */