<?php
defined("BASEPATH") OR exit("No direct script access allowed");

$route["default_controller"] = "app";
$route["payment"] = "app/payment";
$route["portfolio"] = "app/portfolio";
$route["login"] = "app/login";
$route["version"] = "app/version";
$route["register"] = "app/register";
$route["verify"] = "app/verify";
$route["contactus"] = "app/contactus";
$route["countries"] = "app/countries";
$route["nationalities"] = "app/nationalities";
$route["forgot"] = "app/forgot";
$route["campaign/(:any)"] = "app/campaign/$1";
$route["campaigndetails/(:any)"] = "app/campaigndetails/$1";
$route["getfunded"] = "app/getfunded";
$route["getfundedupload"] = "app/getfundedupload";
$route["invest"] = "invest";
$route["myinfo"] = "app/myinfo";
$route["manageprofile"] = "app/manageprofile";
$route["manageprofileupload"] = "app/manageprofileupload";
$route["updatepassword"] = "app/updatepassword";
$route["pay/bank"] = "pay/bank";
$route["pay/paypal"] = "pay/paypal";
$route["pay/xfers"] = "pay/xfers";
$route["xfers/otp"] = "xfers/otp";
$route["404_override"] = "";
$route["translate_uri_dashes"] = FALSE;
