<?php

class Campaign extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDataForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");
              foreach ($country as $row) {
                     $member_country_options[$row['name']] = $row['name'];
              }
              $sql = "SELECT * FROM tgetfunded";
              $result = mysqli_query($sql);

              while ($row = mysqli_fetch_assoc($result)) {
                     $json[] = $row;
                     echo json_encode($json);
              }
              $this->smarty->assign("getfunded", $getfunded);
              $this->smarty->assign("json", $json);
              $this->smarty->assign("member_country_options", $member_country_options);
              $this->smarty->assign("project_type_options", $this->config['project_type_options']);

              $this->smarty->assign('status_options', $this->config['banner_status_options']);


              $content = $this->smarty->fetch('CSTPL_Campaign_Form.php');
              return $content;
       }

       function LoadDataList() {
              if (isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteData($delkey);
                     }
              }

              //Search
              if (isset($_POST['keyword']) && !empty($_POST['keyword'])) {
                     $keyword = $_POST['keyword'];
                     $filter .= "AND (slug LIKE '%" . $keyword . "%' OR campaign_name LIKE '%" . $keyword . "%')";

                     $this->smarty->assign('keyword', $keyword);
              }
              $sql = "SELECT * FROM tcampaign WHERE campaign_id > 0 {$filter} ORDER BY campaign_id DESC";
              list($list, $list_count) = $this->db->multiarray($sql);
              $this->smarty->assign('list', $list);

              /* sme,funding,private */

              $content = $this->smarty->fetch('CSTPL_Campaign_List.php');
              return $content;
       }

       function EditData() {
              $id = (int) $_GET['campaign_id'];

              $sql = "SELECT * FROM tcampaign WHERE campaign_id='" . $id . "'";
              list($detail, $detail_count) = $this->db->singlearray($sql);

              $result = $this->LoadDataForm($detail);
              return $result;
       }

       function ValidateDataForm() {
              $result = "";
              $error = 0;

              if ($_POST['slug'] != '') {
                     $alias = $this->_getUniqueCampaignSlug($_POST['campaign_id'], $_POST['slug']);
                     $check_exist = $this->db->field("SELECT count(*) FROM tcampaign WHERE slug ='" . $alias . "' AND campaign_id != '" . $_POST['campaign_id'] . "'");

                     if ($check_exist > 0) {
                            $this->sys_msg['error'][] = "The url alias already exist in the DB. Please make sure the URL alias is unique.";
                            $error = 1;
                     } else {
                            $_POST['slug'] = $alias;
                     }
              } else {
                     $_POST['slug'] = $this->_getUniqueCampaignSlug($_POST['campaign_id'], $_POST['campaign_name']);
              }
              if ($_POST['campaign_name'] == '') {
                     $this->sys_msg['error'][] = "Campaign Name cannot be blank.";
                     $error = 1;
              }
              if ($_POST['release_date'] == '') {
                     $this->sys_msg['error'][] = "Release Date cannot be blank.";
                     $error = 1;
              }
              /* if($_POST['expiry_date'] == ''){
                $this->sys_msg['error'][] = "Expiry Date cannot be blank.";
                $error = 1;
                }
                if($_POST['address'] == ''){
                $this->sys_msg['error'][] = "Address cannot be blank.";
                $error = 1;
                }
                if($_POST['na'] == ''){
                $this->sys_msg['error'][] = "NA cannot be blank.";
                $error = 1;
                }
                if($_POST['city'] == ''){
                $this->sys_msg['error'][] = "City cannot be blank.";
                $error = 1;
                }
                if($_POST['state'] == ''){
                $this->sys_msg['error'][] = "State cannot be blank.";
                $error = 1;
                }
                if($_POST['zipcode'] == ''){
                $this->sys_msg['error'][] = "Zipcode cannot be blank.";
                $error = 1;
                } */
              if ($_POST['member_country'] == '') {
                     $this->sys_msg['error'][] = "Country cannot be blank.";
                     $error = 1;
              }
              if ($_POST['company_name'] == '') {
                     $this->sys_msg['error'][] = "Company Name cannot be blank.";
                     $error = 1;
              }
              if ($_POST['total_funding_amt'] == '') {
                     $this->sys_msg['error'][] = "Total Funding AMT cannot be blank.";
                     $error = 1;
              }
              if ($_POST['industry'] == '') {
                     $this->sys_msg['error'][] = "Industry cannot be blank.";
                     $error = 1;
              }
              if ($_POST['minimum_investment'] == '') {
                     $this->sys_msg['error'][] = "Minimum Investment cannot be blank.";
                     $error = 1;
              }
              if ($_POST['project'] == '0') {
                     $this->sys_msg['error'][] = "Please select the project type";
                     $error = 1;
              }
              /* if($_POST['industry_country'] == ''){
                $this->sys_msg['error'][] = "Industry Country cannot be blank.";
                $error = 1;
                } */
              if ($_POST['funding_summary'] == '') {
                     $this->sys_msg['error'][] = "Funding Summary cannot be blank.";
                     $error = 1;
              }
              if ($_POST['curr_funding_amt'] == '') {
                     $this->sys_msg['error'][] = "Curr Funding AMT cannot be blank.";
                     $error = 1;
              }
              if ($_POST['project_return'] == '') {
                     $this->sys_msg['error'][] = "Project Return cannot be blank.";
                     $error = 1;
              }
              if ($_POST['clossing_date'] == '') {
                     $this->sys_msg['error'][] = "Clossing Date cannot be blank.";
                     $error = 1;
              }
              /* if($_POST['related_campaign_1'] == ''){
                $this->sys_msg['error'][] = "Related Campaign 1 cannot be blank.";
                $error = 1;
                }
                if($_POST['related_campaign_2'] == ''){
                $this->sys_msg['error'][] = "Related Campaign 2 cannot be blank.";
                $error = 1;
                }
                if($_POST['related_campaign_3'] == ''){
                $this->sys_msg['error'][] = "Related Campaign 3 cannot be blank.";
                $error = 1;
                }
                /*
                if($_POST['image_1'] == ''){
                $this->sys_msg['error'][] = "Image 1 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_2'] == ''){
                $this->sys_msg['error'][] = "Image 2 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_3'] == ''){
                $this->sys_msg['error'][] = "Image 3 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_4'] == ''){
                $this->sys_msg['error'][] = "Image 4 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_5'] == ''){
                $this->sys_msg['error'][] = "Image 5 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_6'] == ''){
                $this->sys_msg['error'][] = "Image 6 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_7'] == ''){
                $this->sys_msg['error'][] = "Image 7 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_8'] == ''){
                $this->sys_msg['error'][] = "Image 8 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_9'] == ''){
                $this->sys_msg['error'][] = "Image 9 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_10'] == ''){
                $this->sys_msg['error'][] = "Image 10 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_11'] == ''){
                $this->sys_msg['error'][] = "Image 11 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_12'] == ''){
                $this->sys_msg['error'][] = "Image 12 cannot be blank.";
                $error = 1;
                }if($_POST['image_13'] == ''){
                $this->sys_msg['error'][] = "Image 13 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_14'] == ''){
                $this->sys_msg['error'][] = "Image 14 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_15'] == ''){
                $this->sys_msg['error'][] = "Image 15 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_16'] == ''){
                $this->sys_msg['error'][] = "Image 16 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_17'] == ''){
                $this->sys_msg['error'][] = "Image 17 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_18'] == ''){
                $this->sys_msg['error'][] = "Image 18 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_19'] == ''){
                $this->sys_msg['error'][] = "Image 19 cannot be blank.";
                $error = 1;
                }
                if($_POST['image_20'] == ''){
                $this->sys_msg['error'][] = "Image 20 cannot be blank.";
                $error = 1;
                }
               */

              if (!$error) {
                     $result = $this->_UpdateData($_POST);
              } else {
                     $result = $this->LoadDataForm($_POST);
              }

              return $result;
       }

       function _getUniqueCampaignSlug($product_id = 0, $alias = '') {
              $alias = trim($alias);
              $alias = preg_replace("/\s+/", '-', $alias);
              $alias = preg_replace("/[^A-Za-z0-9\-\_]/", '', $alias);

              $alias = preg_replace("/-+/", '-', $alias);
              $alias = preg_replace("/^-/", '', $alias);
              $alias = preg_replace("/-$/", '', $alias);

              $sql_alias = $alias;

              $index = 1;

              while ($check = $this->db->field("SELECT count(*) FROM tcampaign WHERE slug='$sql_alias' AND campaign_id != '$product_id'")) {
                     $sql_alias = $alias . '-' . $index;
                     $index++;
              }

              return strtolower($sql_alias);
       }

       function _UpdateData($data) {
              $uploaddir = "";
              $remote_ip = $_SERVER['REMOTE_ADDR'];

              $release_date = $data['release_date'];
              $expiry_date = $data['expiry_date'];
              $clossing_date = $data['clossing_date'];

              $date1 = explode("/", $release_date);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1 . " 00:00:00";

              $date2 = explode("/", $expiry_date);
              $tahun2 = $date2[2];
              $bulan2 = $date2[1];
              $tanggal2 = $date2[0];
              $expiry_date = $tahun2 . "-" . $bulan2 . "-" . $tanggal2 . " 00:00:00";

              $date3 = explode("/", $clossing_date);
              $tahun3 = $date3[2];
              $bulan3 = $date3[1];
              $tanggal3 = $date3[0];
              $clossing_date = $tahun3 . "-" . $bulan3 . "-" . $tanggal3 . " 00:00:00";

              if ($data['campaign_id'] > 0) {
                     $sql = "UPDATE tcampaign SET
        slug = '" . mysql_real_escape_string($data['slug']) . "',
                               enabled = '" . mysql_real_escape_string($data['enabled']) . "',

        campaign_name = '" . mysql_real_escape_string($data['campaign_name']) . "',
        campaign_owner = '" . mysql_real_escape_string($data['campaign_owner']) . "',
        campaign_owner_email = '" . mysql_real_escape_string($data['campaign_owner_email']) . "',
        campaign_description = '" . mysql_real_escape_string($data['campaign_description']) . "',
        release_date = '" . mysql_real_escape_string($release_date) . "',
        expiry_date = '" . mysql_real_escape_string($clossing_date) . "',
        classification = '" . mysql_real_escape_string($data['classification']) . "',
        project_type = '" . mysql_real_escape_string($data['project_type']) . "',
        private_password = '" . mysql_real_escape_string($data['private_password']) . "',
risk = '" . mysql_real_escape_string($data['risk']) . "',
        country = '" . mysql_real_escape_string($data['member_country']) . "',
        company_name = '" . mysql_real_escape_string($data['company_name']) . "',
        total_funding_amt = '" . mysql_real_escape_string($data['total_funding_amt']) . "',
        industry = '" . mysql_real_escape_string($data['industry']) . "',
        minimum_investment = '" . mysql_real_escape_string($data['minimum_investment']) . "',
        industry_country = '" . mysql_real_escape_string($data['industry_country']) . "',
        funding_summary = '" . mysql_real_escape_string($data['funding_summary']) . "',
        curr_funding_amt = '" . mysql_real_escape_string($data['curr_funding_amt']) . "',
        project_return = '" . mysql_real_escape_string($data['project_return']) . "',
        clossing_date = '" . mysql_real_escape_string($clossing_date) . "',
        remote_ip = '" . $remote_ip . "'
        WHERE campaign_id = '" . $data['campaign_id'] . "'
      ";

                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     $this->save_image($insert_id, 'campaign_logo', 'campaign_logo');
                     $this->save_image($insert_id, 'campaign_background', 'campaign_background');
                     $this->save_image($insert_id, 'image_1', 'image_1');
                     $this->save_image($insert_id, 'image_2', 'image_2');
                     $this->save_image($insert_id, 'image_3', 'image_3');
                     $this->save_image($insert_id, 'image_4', 'image_4');
                     $this->save_image($insert_id, 'image_5', 'image_5');
                     $this->save_image($insert_id, 'image_6', 'image_6');
                     $this->save_image($insert_id, 'image_7', 'image_7');
                     $this->save_image($insert_id, 'image_8', 'image_8');
                     $this->save_image($insert_id, 'image_9', 'image_9');
                     $this->save_image($insert_id, 'image_10', 'image_10');
                     $this->save_image($insert_id, 'image_11', 'image_11');
                     $this->save_image($insert_id, 'image_12', 'image_12');
                     $this->save_image($insert_id, 'image_13', 'image_13');
                     $this->save_image($insert_id, 'image_14', 'image_14');
                     $this->save_image($insert_id, 'image_15', 'image_15');
                     $this->save_image($insert_id, 'image_16', 'image_16');
                     $this->save_image($insert_id, 'image_17', 'image_17');
                     $this->save_image($insert_id, 'image_18', 'image_18');

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[campaign_id]] has been updated.";

                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "INSERT INTO tcampaign SET
        slug = '" . mysql_real_escape_string($data['slug']) . "',
                               enabled = '" . mysql_real_escape_string($data['enabled']) . "',

        campaign_name = '" . mysql_real_escape_string($data['campaign_name']) . "',
        campaign_owner = '" . mysql_real_escape_string($data['campaign_owner']) . "',
        campaign_owner_email = '" . mysql_real_escape_string($data['campaign_owner_email']) . "',
        campaign_description = '" . mysql_real_escape_string($data['campaign_description']) . "',
        release_date = '" . mysql_real_escape_string($release_date) . "',
        expiry_date = '" . mysql_real_escape_string($clossing_date) . "',
        classification = '" . mysql_real_escape_string($data['classification']) . "',
        project_type = '" . mysql_real_escape_string($data['project_type']) . "',
        private_password = '" . mysql_real_escape_string($data['private_password']) . "',
risk = '" . mysql_real_escape_string($data['risk']) . "',
        country = '" . mysql_real_escape_string($data['member_country']) . "',
        company_name = '" . mysql_real_escape_string($data['company_name']) . "',
        total_funding_amt = '" . mysql_real_escape_string($data['total_funding_amt']) . "',
        industry = '" . mysql_real_escape_string($data['industry']) . "',
        minimum_investment = '" . mysql_real_escape_string($data['minimum_investment']) . "',
        industry_country = '" . mysql_real_escape_string($data['industry_country']) . "',
        funding_summary = '" . mysql_real_escape_string($data['funding_summary']) . "',
        curr_funding_amt = '" . mysql_real_escape_string($data['curr_funding_amt']) . "',
        project_return = '" . mysql_real_escape_string($data['project_return']) . "',
        clossing_date = '" . mysql_real_escape_string($clossing_date) . "',
        remote_ip = '" . $remote_ip . "'
      ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     $this->save_image($insert_id, 'campaign_logo', 'campaign_logo');
                     $this->save_image($insert_id, 'campaign_background', 'campaign_background');
                     $this->save_image($insert_id, 'image_1', 'image_1');
                     $this->save_image($insert_id, 'image_2', 'image_2');
                     $this->save_image($insert_id, 'image_3', 'image_3');
                     $this->save_image($insert_id, 'image_4', 'image_4');
                     $this->save_image($insert_id, 'image_5', 'image_5');
                     $this->save_image($insert_id, 'image_6', 'image_6');
                     $this->save_image($insert_id, 'image_7', 'image_7');
                     $this->save_image($insert_id, 'image_8', 'image_8');
                     $this->save_image($insert_id, 'image_9', 'image_9');
                     $this->save_image($insert_id, 'image_10', 'image_10');
                     $this->save_image($insert_id, 'image_11', 'image_11');
                     $this->save_image($insert_id, 'image_12', 'image_12');
                     $this->save_image($insert_id, 'image_13', 'image_13');
                     $this->save_image($insert_id, 'image_14', 'image_14');
                     $this->save_image($insert_id, 'image_15', 'image_15');
                     $this->save_image($insert_id, 'image_16', 'image_16');
                     $this->save_image($insert_id, 'image_17', 'image_17');
                     $this->save_image($insert_id, 'image_18', 'image_18');

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[campaign_name]] has been created.";
                            $this->sys_msg['warning'][] = "To prevent campaign duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new Campaign, please try again.";
                     }
              }
       }

       function save_image($insert_id, $file_name, $field) {
              $img_file = $_FILES[$file_name];

              if ($img_file['tmp_name']) {
                     $fileinfo = image_get_info($img_file['tmp_name']);
                     $ext = strtolower($fileinfo['extension']);

                     $uploaddir .= "../assets/images/campaign";

                     if (!is_dir($uploaddir)) {
                            mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                     }

                     //rand (10,10000);
                     $image_name = rand(10, 10000) . $ext;

                     @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                     $sql = "UPDATE tcampaign SET {$field}='" . $image_name . "' WHERE campaign_id = '" . $insert_id . "'";
                     $rs = $this->db->exec($sql);
              }
       }

       function DeleteData($id) {
              $sql = "SELECT campaign_logo FROM tcampaign WHERE campaign_id='" . $id . "'";
              $image = $this->db->field($sql);
              $file = '../images/campaign/' . $image;
              unlink($file);

              $sql = "DELETE from tcampaign WHERE campaign_id='" . $id . "'";
              $rs = $this->db->exec($sql);
              if ($rs) {
                     $this->sys_msg['info'][] = "Campaign [$id] has been deleted.";
                     writeSysLog("Campaign", "Delete Campaign", "Campaign_id[{$id}] deleted");

                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete Campaign, please try again.";

                     return false;
              }
       }

       function LoadgetFundedList() {
              if (isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteGetFundedData($delkey);
                     }
              }

              //Search
              /* if (isset($_POST['keyword']) && !empty($_POST['keyword']))
                {
                $keyword = $_POST['keyword'];
                $filter .= "AND (slug LIKE '%".$keyword."%' OR campaign_name LIKE '%".$keyword."%')";

                $this->smarty->assign('keyword', $keyword);
                } */
              $sql = "SELECT * FROM tgetfunded ORDER BY getfunded_id DESC";
              list($list, $list_count) = $this->db->multiarray($sql);

              $this->smarty->assign('list', $list);

              $content = $this->smarty->fetch('CSTPL_GetFunded_List.php');
              return $content;
       }

       function LoadGetFundedDataForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              $content = $this->smarty->fetch('CSTPL_GetFunded_Form.php');
              return $content;
       }

       function EditGetFundedData() {
              $id = (int) $_GET['getfunded_id'];

              $sql = "SELECT * FROM tgetfunded WHERE getfunded_id='" . $id . "'";
              list($detail, $detail_count) = $this->db->singlearray($sql);

              $result = $this->LoadGetFundedDataForm($detail);
              return $result;
       }

       function DeleteGetFundedData($id) {

              $sql = "DELETE from tgetfunded WHERE getfunded_id='" . $id . "'";
              $rs = $this->db->exec($sql);
              if ($rs) {
                     $this->sys_msg['info'][] = "Get Funded [$id] has been deleted.";
                     writeSysLog("Get Funded", "Delete Get Funded", "getfunded_id[{$id}] deleted");

                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete Get Funded, please try again.";

                     return false;
              }
       }

}

?>