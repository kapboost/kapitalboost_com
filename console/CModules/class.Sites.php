<?php

class Sites {

       var $db;
       var $smarty;
       var $config;
       var $merchant_id;
       var $classPath;
       var $login_id;
       var $usergroup;

       function Sites() {
              global $db, $smarty, $config, $merchant_id, $sys_msg, $root, $login_id, $usergroup;

              $this->classPath = $root . 'console/CModules/';
              $this->db = &$db;
              $this->smarty = &$smarty;
              $this->config = $config;
              $this->merchant_id = $merchant_id;
              $this->sys_msg = &$sys_msg;
              $this->login_id = $login_id;
              $this->usergroup = $usergroup;
       }

       function SetPager222($current_url, $total_records) {

              $pager_url = $current_url . "&page=";
              $kgPagerOBJ = new kgPager();
              $kgPagerOBJ->pager_set($pager_url, $total_records, PAGER_SCROLL_PAGE, PAGER_PER_PAGE, $_GET['page'], PAGER_INACTIVE_PAGE_TAG, PAGER_PREVIOUS_PAGE_TEXT, PAGER_NEXT_PAGE_TEXT, PAGER_FIRST_PAGE_TEXT, PAGER_LAST_PAGE_TEXT, '');
              $this->smarty->assign('pager_url', $pager_url . $_GET['page']);
              $this->smarty->assign('pager', getPager($kgPagerOBJ));

              return $kgPagerOBJ;
       }

       function SendEmail($from, $to, $bcc, $subject, $message, $html = FALSE) {
              include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

              $mail = new PHPMailer();

              //$mail->IsSMTP();
              /* $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->Host       = SMTP_HOST; // sets the SMTP server
                $mail->Port       = 25;                    // set the SMTP port for the GMAIL server
                $mail->Username   = SMTP_USERNAME; // SMTP account username
                $mail->Password   = SMTP_PASSWORD;        // SMTP account password */

              $mail->From = $from;
              $mail->FromName = EMAIL_FROM_NAME;

              if (is_array($to)) {
                     foreach ($to as $to_email) {
                            $mail->AddAddress($to_email);
                     }
              } else {
                     $mail->AddAddress($to);
              }

              if (is_array($bcc)) {
                     foreach ($bcc as $to_email) {
                            $mail->AddBCC($to_email);
                     }
              } else {
                     if ($bcc != '')
                            $mail->AddBCC($bcc);
              }

              $mail->IsHTML($html);

              $mail->Subject = $subject;
              $mail->Body = $message;
              $mail->CharSet = "UTF-8";

              $mail->Send();
       }

       function SetPager($sql, $url = '', $limit_per_page = 0) {
              if ($url == '') {
                     $url = $_SERVER['REQUEST_URI'];
              }

              $url = preg_replace("/&page=\d+/", "", $url);

              if (!$limit_per_page) {
                     $limit_per_page = PAGER_PER_PAGE;
              }

              if (!isset($_GET['page'])) {
                     $page = 1;
              } else {
                     $page = (int) $_GET['page'];
              }

              list(, $total_records) = $this->db->multiarray($sql);

              //$total_record_sql = "SELECT COUNT(*)".substr($sql, strpos($sql, " FROM "), strlen($sql));
              //$total_records = $this->db->field($total_record_sql);

              $pager_url = $url . "&page=";
              $kgPagerOBJ = new kgPager();
              $kgPagerOBJ->pager_set($pager_url, $total_records, PAGER_SCROLL_PAGE, PAGER_PER_PAGE_CONSOLE, $page, PAGER_INACTIVE_PAGE_TAG, PAGER_PREVIOUS_PAGE_TEXT, PAGER_NEXT_PAGE_TEXT, PAGER_FIRST_PAGE_TEXT, PAGER_LAST_PAGE_TEXT, '');
              $this->smarty->assign('pager_url', $pager_url . $page);


              $result_paging = "";
              if ($kgPagerOBJ->total_pages > 1) {
                     $result_paging .= '<ul class="pagination">' . $kgPagerOBJ->first_page .
                             $kgPagerOBJ->previous_page .
                             $kgPagerOBJ->page_links .
                             $kgPagerOBJ->next_page .
                             $kgPagerOBJ->last_page .
                             '</ul>';
              }

              $this->smarty->assign('pager', $result_paging);

              list($list, $listcount) = $this->db->multiarray($sql . " LIMIT $kgPagerOBJ->start, $kgPagerOBJ->per_page");

              return $list;
       }

       function msg_tpl($msg, $type) {
              $result = '';

              if (count($msg)) {
                     $msg = array_unique($msg);
                     $result = "\n<div class=\"message-{$type}\">\n <ul>\n";

                     foreach ($msg as $m) {
                            $result .= "   <li>$m</li>\n";
                     }
                     $result .= "\n </ul>\n</div>\n";
              }

              return $result;
       }

}

?>
