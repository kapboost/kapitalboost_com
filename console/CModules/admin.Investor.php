<?php

include_once(DOC_ROOT . 'Classes/class.phpmailer.php');

class Investor extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
                     
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListSystem();
       }

       function InvestorList() {

              if (isset($_GET["keyword"])) {
                     $keyword = $_GET["keyword"];
                     $list = $this->SetPager("SELECT * FROM tinv WHERE ( nama LIKE '%$keyword%' || email LIKE '%$keyword%' || country LIKE '%$keyword%' || campaign LIKE '%$keyword%' ) && tipe='Paypal' ORDER BY date DESC");
              } else {
                     $list = $this->setPager("SELECT * FROM tinv WHERE id AND tipe='Paypal' ORDER BY date DESC");
              }
              $this->smarty->assign("list", $list);
              $this->smarty->assign("content", $content);
              $result = $this->smarty->fetch("CSTPL_Investor_List.php");

              return $result;
       }

       function All() {
              if (isset($_GET["keyword"])) {
                     $keyword = $_GET["keyword"];
                     $list = $this->SetPager("SELECT * FROM tinv WHERE nama LIKE '%$keyword%' || email LIKE '%$keyword%' || country LIKE '%$keyword%' || campaign LIKE '%$keyword%'  ORDER BY date DESC");
              } else {
                     $list = $this->setPager("SELECT * FROM tinv ORDER BY date DESC");
              }
              $this->smarty->assign("list", $list);
              $this->smarty->assign("content", $content);
              $result = $this->smarty->fetch("CSTPL_Investor_List.php");

              return $result;
       }

       function BankList() {
              if (isset($_GET["keyword"])) {
                     $keyword = $_GET["keyword"];
                     $list = $this->SetPager("SELECT * FROM tinv WHERE ( nama LIKE '%$keyword%' || email LIKE '%$keyword%' || country LIKE '%$keyword%' || campaign LIKE '%$keyword%' ) && tipe='Bank Transfer' ORDER BY date DESC");
              } else {
                     $list = $this->setPager("SELECT * FROM tinv WHERE id AND tipe='Bank Transfer' ORDER BY id DESC");
              }
              $this->smarty->assign("list", $list);
              $result = $this->smarty->fetch("CSTPL_Investor_List.php");

              return $result;
       }

       function XfersList() {
              if (isset($_GET["keyword"])) {
                     $keyword = $_GET["keyword"];
                     $list = $this->SetPager("SELECT * FROM tinv WHERE ( nama LIKE '%$keyword%' || email LIKE '%$keyword%' || country LIKE '%$keyword%' || campaign LIKE '%$keyword%' ) && tipe='Xfers' ORDER BY date DESC");
              } else {
                     $list = $this->setPager("SELECT * FROM tinv WHERE id AND tipe='Xfers' ORDER BY id DESC");
              }
              $this->smarty->assign("list", $list);
              $result = $this->smarty->fetch("CSTPL_Investor_List.php");

              return $result;
       }

       function LoadBankListForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              $content = $this->smarty->fetch('CSTPL_BankList_Form.php');
              return $content;
       }

       function EditBankListData() {
              $id = (int) $_GET['id'];

              $project_type = $_POST["project_type"];
              $servername = "localhost";
              $username = "kapitalboost";
              $password = "kapital2016";
              $dbname = "kapitalboost";
              $conn = new mysqli($servername, $username, $password, $dbname);
              $cek = "SELECT status, token_pp, bukti FROM tinv WHERE id='" . $id . "'";

              $result_cek = $conn->query($cek);

              if ($result_cek->num_rows > 0) {
                     // output data of each row
                     while ($row = $result_cek->fetch_assoc()) {
                            $hasil = $row["status"];
                            $cek_token = $row["token_pp"];
                            $cek_bukti = $row["bukti"];
                     }
              }

              if ($cek_bukti != '') {
                     $update = "UPDATE tpaymentpending SET  tpaymentpending.status = 'close' WHERE token ='$cek_token'";
                     $result_update = $conn->query($update);
              }

              $sql = "SELECT * FROM tinv WHERE id='" . $id . "'";
              list($detail, $detail_count) = $this->db->singlearray($sql);

              $result = $this->LoadBankListForm($detail);
              return $result;
       }

       public function GenerateEmailTemplate($nama, $campaign, $amount) {
              if ($campaign == "Donation" OR $campaign == "donation") {
                     $content = "
                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                       Thank you for your generous donation of <br>
                       <b>SGD " . $amount . "</b> for the <b>" . $campaign . "</b> campaign.
                      </p>
                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                        May your kindness be rewarded in multiplefold.
                      </p>
        ";
              } else if ($campaign == "payout") {
                     $content = "
                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                       A payment of <b>SGD " . $amount . "</b> from <b>" . $campaign . "</b> crowdfunding deal has been made to your Xfers/bank account.
                      </p>
					  
                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                        To view the updated payment status from the <b>" . $campaign . "</b>, <br>
						visit the <a href=\"https://kapitalboost.com/dashboard/payment-history\">Portfolio</a> page of your Dashboard.<br>
						
                        If you have any questions, please email <a style=\"text-decoration:none\" href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
                      </p>
        ";
              } else {
                     $content = "
                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                       A payment of <b>SGD " . $amount . "</b> from <b>" . $campaign . "</b> crowdfunding deal has been made to your Xfers/bank account.
                      </p>
					  
                      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                        To view the updated payment status from the <b>" . $campaign . "</b>, <br>
						visit the <a href=\"https://kapitalboost.com/dashboard/payment-history\">Portfolio</a> page of your Dashboard.<br>
						
                        If you have any questions, please email <a style=\"text-decoration:none\" href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>.
                      </p>
        ";
              }
              $message = "
                        <html>
                        <head>
                          <title>Kapitalboost</title>
                        </head>
                        <style type='text/css'>
                        @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
                        </style>
                        <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
                          <table height='100%' width='100%' bgcolor='#305e8e'>
                            <tbody>
                              <tr>
                                <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
                                  <center>
                                    <div style='display: inline-block;width: 640px;background: #00b0fd;'>
                                      <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                                        <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
                                        <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                                         Dear " . $nama . ",
                                        </p>
                                        " . $content . "
                                        <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                          Best regards, <br>
                                          Kapital Boost team
                                        </p>
                                      </div>
                                      <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                                        <p style='font-size: 16px;color: white;width:100%'>
                                          &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
                                        </p>
                                      </div>
                                    </div>
                                  </center>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </body>
                        </html>
                ";
              return $message;
       }

       function Update1() {
              $month = $_POST['Month_1'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_1='" . mysql_real_escape_string($month) . "',
                    tanggal_1='" . $release_date . "',
                    status_1='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update2() {
              $month = $_POST['Month_2'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_2='" . mysql_real_escape_string($month) . "',
                    tanggal_2='" . $release_date . "',
                    status_2='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = "payout";
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update3() {
              $month = $_POST['Month_3'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_3='" . mysql_real_escape_string($month) . "',
                    tanggal_3='" . $release_date . "',
                    status_3='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update4() {
              $month = $_POST['Month_4'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_4='" . mysql_real_escape_string($month) . "',
                    tanggal_4='" . $release_date . "',
                    status_4='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update5() {
              $month = $_POST['Month_5'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_5='" . mysql_real_escape_string($month) . "',
                    tanggal_5='" . $release_date . "',
                    status_5='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update6() {
              $month = $_POST['Month_6'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_6='" . mysql_real_escape_string($month) . "',
                    tanggal_6='" . $release_date . "',
                    status_6='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update7() {
              $month = $_POST['Month_7'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_7='" . mysql_real_escape_string($month) . "',
                    tanggal_7='" . $release_date . "',
                    status_7='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update8() {
              $month = $_POST['Month_8'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_8='" . mysql_real_escape_string($month) . "',
                    tanggal_8='" . $release_date . "',
                    status_8='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update9() {
              $month = $_POST['Month_9'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_9='" . mysql_real_escape_string($month) . "',
                    tanggal_9='" . $release_date . "',
                    status_9='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update10() {
              $month = $_POST['Month_10'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_10='" . mysql_real_escape_string($month) . "',
                    tanggal_10='" . $release_date . "',
                    status_10='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update11() {
              $month = $_POST['Month_11'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_11='" . mysql_real_escape_string($month) . "',
                    tanggal_11='" . $release_date . "',
                    status_11='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update12() {
              $month = $_POST['Month_12'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_12='" . mysql_real_escape_string($month) . "',
                    tanggal_12='" . $release_date . "',
                    status_12='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update13() {
              $month = $_POST['Month_13'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_13='" . mysql_real_escape_string($month) . "',
                    tanggal_13='" . $release_date . "',
                    status_13='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update14() {
              $month = $_POST['Month_14'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_14='" . mysql_real_escape_string($month) . "',
                    tanggal_14='" . $release_date . "',
                    status_14='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update15() {
              $month = $_POST['Month_15'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_15='" . mysql_real_escape_string($month) . "',
                    tanggal_15='" . $release_date . "',
                    status_15='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update16() {
              $month = $_POST['Month_16'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_16='" . mysql_real_escape_string($month) . "',
                    tanggal_16='" . $release_date . "',
                    status_16='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update17() {
              $month = $_POST['Month_17'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_17='" . mysql_real_escape_string($month) . "',
                    tanggal_17='" . $release_date . "',
                    status_17='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update18() {
              $month = $_POST['Month_18'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_18='" . mysql_real_escape_string($month) . "',
                    tanggal_18='" . $release_date . "',
                    status_18='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update19() {
              $month = $_POST['Month_19'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_19='" . mysql_real_escape_string($month) . "',
                    tanggal_19='" . $release_date . "',
                    status_19='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update20() {
              $month = $_POST['Month_20'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_20='" . mysql_real_escape_string($month) . "',
                    tanggal_20='" . $release_date . "',
                    status_20='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update21() {
              $month = $_POST['Month_21'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_21='" . mysql_real_escape_string($month) . "',
                    tanggal_21='" . $release_date . "',
                    status_21='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update22() {
              $month = $_POST['Month_22'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_22='" . mysql_real_escape_string($month) . "',
                    tanggal_22='" . $release_date . "',
                    status_22='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update23() {
              $month = $_POST['Month_23'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_23='" . mysql_real_escape_string($month) . "',
                    tanggal_23='" . $release_date . "',
                    status_23='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function Update24() {
              $month = $_POST['Month_24'];
              $tggl = $_POST['release_date'];

              $date1 = explode("/", $tggl);
              $tahun1 = $date1[2];
              $bulan1 = $date1[1];
              $tanggal1 = $date1[0];
              $release_date = $tahun1 . "-" . $bulan1 . "-" . $tanggal1;
              $status = $_POST['status'];
              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
        ";
                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];

                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";
                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                    bulan_24='" . mysql_real_escape_string($month) . "',
                    tanggal_24='" . $release_date . "',
                    status_24='" . mysql_real_escape_string($status) . "'
                    WHERE id='$id'
          ";

                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($status == 'Paid') {

                            $subject = "Payment Successful";
                            $member_email = $_POST["member_email"];
                            $nama = $_POST['member_name'];
                            $campaign = $_POST['campaign'];
                            $Class_Investor = new Investor();
                            $message = $Class_Investor->GenerateEmailTemplate($nama, $campaign, $month);
                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

       function _UpdateData($data) {
              $month1 = $_POST['Month_1'];
              $month2 = $_POST['Month_2'];
              $month3 = $_POST['Month_3'];
              $month4 = $_POST['Month_4'];
              $month5 = $_POST['Month_5'];
              $month6 = $_POST['Month_6'];
              $month7 = $_POST['Month_7'];
              $month8 = $_POST['Month_8'];
              $month9 = $_POST['Month_9'];
              $month10 = $_POST['Month_10'];
              $month11 = $_POST['Month_11'];
              $month12 = $_POST['Month_12'];
              $month13 = $_POST['Month_13'];
              $month14 = $_POST['Month_14'];
              $month15 = $_POST['Month_15'];
              $month16 = $_POST['Month_16'];
              $month17 = $_POST['Month_17'];
              $month18 = $_POST['Month_18'];
              $month19 = $_POST['Month_19'];
              $month20 = $_POST['Month_20'];
              $month21 = $_POST['Month_21'];
              $month22 = $_POST['Month_22'];
              $month23 = $_POST['Month_23'];
              $month24 = $_POST['Month_24'];
              $status = $_POST['status'];
              $tipe = $_POST['tipe'];
              $project_type = $_POST['project'];
              $member_email = $_POST['mail'];
              $nama = $_POST['nama'];
              $campaign = $_POST['campaign'];
              $funding = $_POST['funding'];


              $id = $_POST['id'];

              if ($data['id'] > 0) {
                     $sql = "UPDATE tinv SET
                status = '" . mysql_real_escape_string($status) . "',
            ";

                     $rs = $this->db->exec($sql);
                     $insert_id = $data['campaign_id'];


                     if ($rs) {
                            $this->sys_msg['info'][] = "Campaign [$data[id]] has been updated.";

                            writeSysLog("Campaign", "Edit Campaign", "Campaign ID[{$data['parent_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Campaign, please try again.";
                     }
              } else {
                     $sql = "  UPDATE tinv SET
                      bulan_1='" . mysql_real_escape_string($month1) . "',
                      bulan_2='" . mysql_real_escape_string($month2) . "',
                      bulan_3='" . mysql_real_escape_string($month3) . "',
                      bulan_4='" . mysql_real_escape_string($month4) . "',
                      bulan_5='" . mysql_real_escape_string($month5) . "',
                      bulan_6='" . mysql_real_escape_string($month6) . "',
                      bulan_7='" . mysql_real_escape_string($month7) . "',
                      bulan_8='" . mysql_real_escape_string($month8) . "',
                      bulan_9='" . mysql_real_escape_string($month9) . "',
                      bulan_10='" . mysql_real_escape_string($month10) . "',
                      bulan_11='" . mysql_real_escape_string($month11) . "',
                      bulan_12='" . mysql_real_escape_string($month12) . "',
                      bulan_13='" . mysql_real_escape_string($month13) . "',
                      bulan_14='" . mysql_real_escape_string($month14) . "',
                      bulan_15='" . mysql_real_escape_string($month15) . "',
                      bulan_16='" . mysql_real_escape_string($month16) . "',
                      bulan_17='" . mysql_real_escape_string($month17) . "',
                      bulan_18='" . mysql_real_escape_string($month18) . "',
                      bulan_19='" . mysql_real_escape_string($month19) . "',
                      bulan_20='" . mysql_real_escape_string($month20) . "',
                      bulan_21='" . mysql_real_escape_string($month21) . "',
                      bulan_22='" . mysql_real_escape_string($month22) . "',
                      bulan_23='" . mysql_real_escape_string($month23) . "',
                      bulan_24='" . mysql_real_escape_string($month24) . "',
                status= '" . mysql_real_escape_string($status) . "' WHERE id='$id'
            ";
                     $member_name = $_POST["fullname"];
                     $rs = $this->db->exec($sql);
                     $insert_id = mysql_insert_id();

                     if ($project_type != 'donation' && $status == 'Paid') {
                            $subject = "Payment Completed";
                            $message = "
              <html>
              <head>
                <title>Kapitalboost</title>
              </head>
              <style type='text/css'>
              @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
              </style>
              <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
                <table height='100%' width='100%' bgcolor='#305e8e'>
                  <tbody>
                    <tr>
                      <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
                        <center>
                          <div style='display: inline-block;width: 640px;background: #00b0fd;'>
                            <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                              <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
                              <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                               Dear " . $member_name . ",
                              </p>

                              <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                               Thank you for your fund transfer of <b>SGD " . $funding . "</b> for investment in the <b>" . $campaign . "</b> campaign.
                              </p>
                              <p style='display: block;font-size: 16px;text-align: left;margin: 0 10px 10px 35px;'>
                                To check on the status and repayment schedules of your investments, click on Portfolio under the Dashboard.
                              </p>
                              <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                 If you have questions, please email <a style=\"text-decoration:none\" href='mailto:support@kapitalboost.com'>support@kapitalboost.com</a>..
                              </p>
                              <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                Best regards, <br>
                                Kapital Boost team
                              </p>
                            </div>
                            <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                              <p style='font-size: 16px;color: white;width:100%'>
                                &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
                              </p>
                            </div>
                          </div>
                        </center>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </body>
              </html>


              ";


                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else if ($project_type == 'donation' && $status == 'Paid') {
                            $subject = "Payment Completed";
                            $message = "
                <html>
                <head>
                  <title>Kapitalboost</title>
                </head>
                <style type='text/css'>
                @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
                </style>
                <body style='background: #305e8e;height: auto;width: 750px;padding: 0;margin: 0 auto;font-family: \"Open Sans\";'>
                  <table height='100%' width='100%' bgcolor='#305e8e'>
                    <tbody>
                      <tr>
                        <td style='width: 750px;padding: 20px 10px;' bgcolor='#305e8e'>
                          <center>
                            <div style='display: inline-block;width: 640px;background: #00b0fd;'>
                              <div style='display: inline-block;width: 100%;background: white;text-align: left;border-bottom:10px solid #00AAEA'>
                                <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
                                <p style='display: block;font-size: 16px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
                                 Dear " . $member_name . ",
                                </p>

                                <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                 Thank you for your generous donation of <b>SGD " . $funding . "</b> for the <b>" . $campaign . "</b> campaign.
                                </p>
                                <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                  May your kindness be rewarded in multiplefold.
                                </p>
                                <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                  If you have any questions or concerns, please feel free to contact support@kapitalboost.com.
                                </p>
                                <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
                                  Best regards, <br>
                                  Kapital Boost team
                                </p>
                              </div>
                              <div style='display: inline-block;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                                <p style='font-size: 16px;color: white;width:100%'>
                                  &copy; COPYRIGHT 2016 - KAPITAL BOOST PTE LTD
                                </p>
                              </div>
                            </div>
                          </center>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </body>
                </html>


                ";

                            $this->SendEmail(EMAIL_FROM, $member_email, array('admin@kapitalboost.com', 'admin@kapitalboost.com'), $subject, $message, TRUE);
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Campaign", "Add Campaign", "Campaign_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['info'][] = "Payment [$id] has been update.";
                            $this->sys_msg['warning'][] = "To prevent Payment duplicate new record entry, please do not refresh the browser.";
                     }
              }
       }

}

#SYSTEM FUNCTION END
?>
