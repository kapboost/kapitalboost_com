<?php
class Media extends Sites{
	public function __construct(){
		global $module_id, $action_id, $db;
		parent::__construct();
		
		if ($module_id > 0 && $this->usergroup && $this->login_id){
			if ( $action_id > 0 ){
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
				if (!$check){
					die("Invalid operation access spotted, please check with administrator");
				}
			}
			else{
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
				if (!$check)
				{
					die("Invalid operation access spotted, please check with administrator");
				}
			}
		}
		else{
			die("You don't have permission to access this page, please check with administrator");
		}
	}
	
	function LoadDataForm($data=NULL){
		if($data){
			$this->smarty->assign($data);
		}
		
		$this->smarty->assign('status_options', $this->config['banner_status_options']);
		$content = $this->smarty->fetch('CSTPL_Media_Form.php');
		return $content;
	}
	
	function LoadDataList(){
		if(isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])){
			foreach($_POST['list_id'] as $delkey){
				$this->DeleteData($delkey);
			}
		}
		
		//Search
		if (isset($_POST['keyword']) && !empty($_POST['keyword']))
		{
			$keyword = $_POST['keyword'];
			$filter .= "AND media_name LIKE '%".$keyword."%'";
			
			$this->smarty->assign('keyword', $keyword);
		}
		$sql = "SELECT media_id, media_name, media_description, classification, media_url, image, release_date, expiry_date FROM tmedia WHERE media_id > 0 {$filter} ORDER BY media_id DESC";
		list($list, $list_count) = $this->db->multiarray($sql);
		
		$this->smarty->assign('list', $list);
		
		$content = $this->smarty->fetch('CSTPL_Media_List.php');
		return $content;
	}
	
	function EditData(){
		$id = (int)$_GET['media_id'];
		
		$sql = "SELECT * FROM tmedia WHERE media_id='".$id."'";
		list($detail, $detail_count) = $this->db->singlearray($sql);
		
		$result = $this->LoadDataForm($detail);
		return $result;
	}
	
	function ValidateDataForm(){
		$result = "";
		$error = 0;
		
		if($_POST['media_name'] == ''){
			$this->sys_msg['error'][] = "Media Name cannot be blank.";
			$error = 1;
		}
		if($_POST['media_description'] == ''){
			$this->sys_msg['error'][] = "Media Description cannot be blank.";
			$error = 1;
		}
		if($_POST['classification'] == ''){
			$this->sys_msg['error'][] = "Classification cannot be blank.";
			$error = 1;
		}
		if($_POST['release_date'] == ''){
			$this->sys_msg['error'][] = "Release Date cannot be blank.";
			$error = 1;
		}
		if($_POST['expiry_date'] == ''){
			$this->sys_msg['error'][] = "Expiry Date cannot be blank.";
			$error = 1;
		}
		
		$media_id = $_POST['media_id'];
		$sql = "SELECT image FROM tmedia WHERE media_id='".$media_id."'";
		$image = $this->db->field($sql);
		
		if(!$error){
			$result = $this->_UpdateData($_POST);
		}
		else{
			$this->smarty->assign('image', $image);
			$result = $this->LoadDataForm($_POST);
		}
		
		return $result;
	}
	
	function _UpdateData($data){
		$uploaddir = "";
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		
		$release_date = $data['release_date'];
		$expiry_date = $data['expiry_date'];
		
		$date1 = explode("/", $release_date);
		$tahun1=$date1[2];
		$bulan1=$date1[1];
		$tanggal1=$date1[0];
		$release_date=$tahun1."-".$bulan1."-".$tanggal1;
					
		$date2 = explode("/", $expiry_date);
		$tahun2=$date2[2];
		$bulan2=$date2[1];
		$tanggal2=$date2[0];
		$expiry_date=$tahun2."-".$bulan2."-".$tanggal2;
		
		if($data['media_id'] > 0){
			$sql = "UPDATE tmedia SET
				media_name = '".mysql_real_escape_string($data['media_name'])."',
				media_description = '".mysql_real_escape_string($data['media_description'])."',
				release_date = '".mysql_real_escape_string($release_date)."',
				expiry_date = '".mysql_real_escape_string($expiry_date)."',
				classification = '".mysql_real_escape_string($data['classification'])."',
				media_url = '".mysql_real_escape_string($data['media_url'])."',
				enabled = '".mysql_real_escape_string($data['enabled'])."',
				update_dt = NOW(),
				remote_ip = '".$remote_ip."'
				WHERE media_id = '".$data['media_id']."'
			";
			
			$rs = $this->db->exec($sql);
			
			$img_file = $_FILES['img'];
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/media";
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "media_".$data['media_id'].".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				//if($img_file != ""){
					$sql = "UPDATE tmedia SET image = '".$image_name."' WHERE media_id = '".$_data['media_id']."'";
					echo $sql."<br>";
					$rs = $this->db->exec($sql);
				//}
			}
			
			if($rs){
				$this->sys_msg['info'][] = "Media [$data[media_id]] has been updated.";
				
				writeSysLog("Media", "Edit Media", "Media ID[{$data['parent_id']}] updated");
			}
			else{
				$this->sys_msg['error'][] = "Failed to update Media, please try again.";
			}
		}
		else{
			$sql = "INSERT INTO tmedia SET
				media_name = '".mysql_real_escape_string($data['media_name'])."',
				media_description = '".mysql_real_escape_string($data['media_description'])."',
				release_date = '".mysql_real_escape_string($release_date)."',
				expiry_date = '".mysql_real_escape_string($expiry_date)."',
				classification = '".mysql_real_escape_string($data['classification'])."',
				media_url = '".mysql_real_escape_string($data['media_url'])."',
				image = '".mysql_real_escape_string($data['image'])."',
				enabled = '".mysql_real_escape_string($data['enabled'])."',
				insert_dt = NOW(),
				remote_ip = '".$remote_ip."'
			";
			
			$rs = $this->db->exec($sql);
			$insert_id = mysql_insert_id();
			
			$img_file = $_FILES['img'];
			
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/media";
				echo $uploaddir."<br>";
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "media_".$insert_id.".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tmedia SET image='".$image_name."' WHERE media_id = '".$insert_id."'";
				$rs = $this->db->exec($sql);
			}
			
			if($rs){
				$this->sys_msg['info'][] = "Media [$data[media_name]] has been created.";
				$this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
				writeSysLog("Media", "Add Media", "Media_id[{$insert_id}] added");
			}
			else{
				$this->sys_msg['error'][] = "Failed to add new Media, please try again.";
			}
		}
	}
	
	function DeleteData($id){
		$sql = "SELECT image FROM tmedia WHERE media_id='".$id."'";
		$image = $this->db->field($sql);
		$file = '../images/media/'.$image;
		unlink($file);
		
		$sql = "DELETE from tmedia WHERE media_id='".$id."'";
		
		$rs = $this->db->exec($sql);
		if($rs){
			$this->sys_msg['info'][] = "Media [$id] has been deleted.";
			writeSysLog("Media", "Delete Media", "Media_id[{$id}] deleted");
			
			return true;
		}
		else{
			$this->sys_msg['error'][] = "Failed to delete Media, please try again.";
			
			return false;
		}
	}
}
?>