<?php

class Log extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListSystem();
       }

       function ListLog() {

              $list = $this->setPager("SELECT tsystemlog.*, tmerchantlogin.console_user FROM tsystemlog, tmerchantlogin WHERE tsystemlog.login_id = tmerchantlogin.login_id ORDER BY tsystemlog.insert_dt DESC");

              $this->smarty->assign("list", $list);
              $result = $this->smarty->fetch("CSTPL_Log_List.php");

              return $result;
       }

       #SYSTEM FUNCTION END
}

?>