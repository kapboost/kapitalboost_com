<?php
class Testimonial extends Sites{
    public function __construct(){
        global $module_id, $action_id, $db;
        parent::__construct();
        
        if ($module_id > 0 && $this->usergroup && $this->login_id){
            if ( $action_id > 0 ){
                $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                if (!$check){
                    die("Invalid operation access spotted, please check with administrator");
                }
            }
            else{
                $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                if (!$check)
                {
                    die("Invalid operation access spotted, please check with administrator");
                }
            }
        }
        else{
            die("You don't have permission to access this page, please check with administrator");
        }
    }
    
    function LoadDataForm($data=NULL){
        if($data){
            $this->smarty->assign($data);
        }
        
        $this->smarty->assign('testimonial_status_options', $this->config['testimonial_status_options']);
        $content = $this->smarty->fetch('CSTPL_Testimonial_Form.php');
        return $content;
    }
    
    function LoadDataList(){
        if(isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])){
            foreach($_POST['list_id'] as $delkey){
                $this->DeleteData($delkey);
            }
        }
        
        //Search
        if (isset($_POST['keyword']) && !empty($_POST['keyword']))
        {
            $keyword = $_POST['keyword'];
            $filter .= "AND testimonial_subject LIKE '%".$keyword."%'";
            
            $this->smarty->assign('keyword', $keyword);
        }
        $sql = "SELECT * FROM ttestimonial WHERE testimonial_id > 0 {$filter} ORDER BY testimonial_id ASC";
        list($list, $list_count) = $this->db->multiarray($sql);
        
        $this->smarty->assign('list', $list);
        
        $content = $this->smarty->fetch('CSTPL_Testimonial_List.php');
        return $content;
    }
    
    function EditData(){
        $id = (int)$_GET['testimonial_id'];
        
        $sql = "SELECT * FROM ttestimonial WHERE testimonial_id='".$id."'";
        list($detail, $detail_count) = $this->db->singlearray($sql);
        
        $result = $this->LoadDataForm($detail);
        return $result;
    }
    
    function ValidateDataForm(){
        $result = "";
        $error = 0;
        
        if($_POST['testimonial_subject'] == ''){
            $this->sys_msg['error'][] = "Testimonial Subject cannot be blank.";
            $error = 1;
        }
        if($_POST['testimonial_content'] == ''){
            $this->sys_msg['error'][] = "Testimonial Content cannot be blank.";
            $error = 1;
        }
        
        $testimonial_id = $_POST['testimonial_id'];
        $sql = "SELECT testimonial_image FROM ttestimonial WHERE testimonial_id='".$testimonial_id."'";
        $testimonial_image = $this->db->field($sql);
        
        if(!$error){
            $result = $this->_UpdateData($_POST);
        }
        else{
            $this->smarty->assign('testimonial_image', $testimonial_image);
            $result = $this->LoadDataForm($_POST);
        }
        
        return $result;
    }
    
    function _UpdateData($data){
        $uploaddir = "";
        $remote_ip = $_SERVER['REMOTE_ADDR'];
        
        if($data['testimonial_id'] > 0){
            $sql = "UPDATE ttestimonial SET
                testimonial_subject = '".mysql_real_escape_string($data['testimonial_subject'])."',
                testimonial_content = '".mysql_real_escape_string($data['testimonial_content'])."',
                testimonial_status='".mysql_real_escape_string($data['testimonial_status'])."',
                update_dt = NOW(),
                remote_ip = '".$remote_ip."'
                WHERE testimonial_id = '".$data['testimonial_id']."'
            ";
            
            $rs = $this->db->exec($sql);
            
            $img_file = $_FILES['img'];
            if($img_file['tmp_name']){
                $fileinfo = image_get_info($img_file['tmp_name']);
                $ext = strtolower($fileinfo['extension']);
                
                $uploaddir .= "../assets/images/testimonial";
                if(!is_dir($uploaddir)){
                    mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                }
                
                $image_name = "testimonial_".$data['testimonial_id'].".".$ext;
                
                @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
                
                //if($img_file != ""){
                    $sql = "UPDATE ttestimonial SET testimonial_image = '".$image_name."' WHERE testimonial_id = '".$_data['testimonial_id']."'";
                    echo $sql."<br>";
                    $rs = $this->db->exec($sql);
                //}
            }
            
            if($rs){
                $this->sys_msg['info'][] = "Testimonial [$data[testimonial_id]] has been updated.";
                
                writeSysLog("Testimonial", "Edit Testimonial", "Testimonial ID[{$data['parent_id']}] updated");
            }
            else{
                $this->sys_msg['error'][] = "Failed to update Testimonial, please try again.";
            }
        }
        else{
            $div_atas ="</div></div><div class='item'><div class='home-5-testimonial'><p class='testi-content'>
";
            $atas = mysql_real_escape_string($div_atas);
            $div_person ="<p class='testi-person'>";
            $person = mysql_real_escape_string($div_person);
            $div_bawah = "<center>
<div style='width:100px;height:100px;border:2px solid white;border-radius: 100%;'></div>
</center>
";
$bawah = mysql_real_escape_string($div_bawah);
             $sql = "INSERT INTO ttestimonial SET
                testimonial_subject = '".mysql_real_escape_string($data['testimonial_subject'])."',
                testimonial_content = '".mysql_real_escape_string($data['testimonial_content'])."',
                testimonial_status='".mysql_real_escape_string($data['testimonial_status'])."',
                insert_dt = NOW(),
                remote_ip = '".$remote_ip."',
                div_atas = '".$atas."',
                person = '".$person."',
                div_bawah = '".$bawah."'

            ";
            
            $rs = $this->db->exec($sql);
            $insert_id = mysql_insert_id();
            
            $img_file = $_FILES['img'];
            if($img_file['tmp_name']){
                $fileinfo = image_get_info($img_file['tmp_name']);
                $ext = strtolower($fileinfo['extension']);
                
                $uploaddir .= "../assets/images/testimonial";
                echo $uploaddir."<br>";
                if(!is_dir($uploaddir)){
                    mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                }
                
                $image_name = "testimonial_".$insert_id.".".$ext;
                
                @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
                
                $sql = "UPDATE ttestimonial SET testimonial_image='".$image_name."' WHERE testimonial_id = '".$insert_id."'";
                $rs = $this->db->exec($sql);
            }
            
            if($rs){
                $this->sys_msg['info'][] = "Testimonial [$data[testimonial_subject]] has been created.";
                $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                writeSysLog("Testimonial", "Add Testimonial", "Testimonial_id[{$insert_id}] added");
            }
            else{
                $this->sys_msg['error'][] = "Failed to add new Testimonial, please try again.";
            }
        }
    }
    
    function DeleteData($id){
        $sql = "SELECT testimonial_image FROM ttestimonial WHERE testimonial_id='".$id."'";
        $image = $this->db->field($sql);
        $file = '../images/testimonial/'.$image;
        unlink($file);
        
        $sql = "DELETE from ttestimonial WHERE testimonial_id='".$id."'";
        // echo $sql."<br>";
        $rs = $this->db->exec($sql);
        // exit();
        if($rs){
            $this->sys_msg['info'][] = "Testimonial [$id] has been deleted.";
            writeSysLog("Testimonial", "Delete Testimonial", "Testimonial_id[{$id}] deleted");
            
            return true;
        }
        else{
            $this->sys_msg['error'][] = "Failed to delete Testimonial, please try again.";
            
            return false;
        }
    }
}
?>