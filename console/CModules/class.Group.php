<?php

class Group extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListGroup();
       }

       #Group FUNCTION BEGIN

       function LoadGroupForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              $listgroup = LoadCategoryGroupOptions(0, "", $data['group_parent_id']);

              $this->smarty->assign('group_status_options', $this->config['group_status_options']);
              $this->smarty->assign('listgroup', $listgroup);
              $this->smarty->assign('action', "ValidateGroupForm");
              $result = $this->smarty->fetch("CSTPL_Group_Form.php");
              return $result;
       }

       function ValidateGroupForm() {
              $error = 0;
              $result = "";

              if ($_POST['group_name'] == '') {
                     $this->sys_msg['error'][] = "<b>Group Name</b> cannot be blank";
                     $error = 1;
              }

              if ($_POST['group_alias'] != '') {
                     $alias = $this->_getUniqueGroupAlias($_POST['group_id'], $_POST['group_alias']);

                     $check_exist = $this->db->field("SELECT count(*) FROM tgroup WHERE group_alias='" . $alias . "' AND group_id != '" . $_POST['group_id'] . "'");

                     if ($check_exist > 0) {
                            $this->sys_msg['error'][] = "The url alias already exist in the DB. Please make sure the URL alias is unique.";
                            $error = 1;
                     } else {
                            $_POST['group_alias'] = $alias;
                     }
              } else {
                     $_POST['group_alias'] = $this->_getUniqueGroupAlias($_POST['group_id'], $_POST['group_name']);
              }

              if (isset($_POST['group_status']) == '') {
                     $this->sys_msg['error'][] = "<b>Status</b> must be choosen";
                     $error = 1;
              }

              if (!$error) {
                     $result .= $this->_UpdateGroup($_POST);
              } else {
                     $result .= $this->LoadGroupForm($_POST);
              }

              return $result;
       }

       function _UpdateGroup($data) {
              $uploaddir = "";
              $remote_ip = $_SERVER['REMOTE_ADDR'];


              if ($data['group_id'] > 0) {

                     $rs = $this->db->exec("UPDATE tgroup SET 
															group_name='" . formatSQL($data['group_name']) . "',
															group_parent_id='" . formatSQL($data['group_parent_id']) . "', 
															group_alias='" . formatSQL($data['group_alias']) . "', 
															group_description='" . formatSQL($data['group_description']) . "', 
															group_keyword='" . formatSQL($data['group_keyword']) . "', 
															group_status='" . formatSQL($data['group_status']) . "',   
															group_position='" . formatSQL($data['group_position']) . "',
															sort='" . formatSQL($data['sort']) . "',   
															update_dt=NOW(), 
															remote_ip='" . $remote_ip . "',
															login_id='" . $this->login_id . "'
															WHERE group_id = '" . $data['group_id'] . "'
															");

                     $img_file = $_FILES['img'];
                     if ($img_file['tmp_name']) {
                            $fileinfo = image_get_info($img_file['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/group/";

                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "gid_" . $data['group_id'] . "." . $ext;

                            @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                            $rs = $this->db->exec("UPDATE tgroup SET group_pathimages='" . formatSQL($image_name) . "'WHERE group_id = '" . $data['group_id'] . "'");
                     }

                     $img_file1 = $_FILES['img1'];
                     if ($img_file1['tmp_name']) {
                            $fileinfo = image_get_info($img_file1['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/group/";

                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "gid1_" . $data['group_id'] . "." . $ext;

                            @copy($img_file1['tmp_name'], "{$uploaddir}/{$image_name}");

                            $rs = $this->db->exec("UPDATE tgroup SET group_pathimages1='" . formatSQL($image_name) . "'WHERE group_id = '" . $data['group_id'] . "'");
                     }

                     $img_file2 = $_FILES['img2'];
                     if ($img_file2['tmp_name']) {
                            $fileinfo = image_get_info($img_file2['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/group/";

                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "gid2_" . $data['group_id'] . "." . $ext;

                            @copy($img_file2['tmp_name'], "{$uploaddir}/{$image_name}");

                            $rs = $this->db->exec("UPDATE tgroup SET group_pathimages2='" . formatSQL($image_name) . "'WHERE group_id = '" . $data['group_id'] . "'");
                     }

                     if ($rs) {
                            $this->sys_msg['info'][] = "Group [$data[group_name]] has been updated";
                            writeSysLog("Group", "Edit Group", "Group_id[{$data['group_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Group, please try again.";
                     }
              } else {
                     $rs = $this->db->exec("INSERT INTO tgroup SET 
															group_name='" . formatSQL($data['group_name']) . "',
															group_parent_id='" . formatSQL($data['group_parent_id']) . "', 
															group_alias='" . formatSQL($data['group_alias']) . "', 
															group_description='" . formatSQL($data['group_description']) . "', 
															group_keyword='" . formatSQL($data['group_keyword']) . "', 
															group_status='" . formatSQL($data['group_status']) . "', 
															group_position='" . formatSQL($data['group_position']) . "',     
															sort='" . formatSQL($data['sort']) . "', 
															insert_dt=NOW(), 
															remote_ip='" . $remote_ip . "',
															login_id='" . $this->login_id . "' 
															");

                     $insert_group_id = mysql_insert_id();

                     $img_file = $_FILES['img'];
                     if ($img_file['tmp_name']) {
                            $fileinfo = image_get_info($img_file['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/group/";

                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "gid_" . $insert_group_id . "." . $ext;

                            @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                            $rs = $this->db->exec("UPDATE tgroup SET group_pathimages='" . formatSQL($image_name) . "'WHERE group_id = '" . $insert_group_id . "'");
                     }

                     $img_file1 = $_FILES['img1'];
                     if ($img_file1['tmp_name']) {
                            $fileinfo = image_get_info($img_file1['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/group/";

                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "gid1_" . $insert_group_id . "." . $ext;

                            @copy($img_file1['tmp_name'], "{$uploaddir}/{$image_name}");

                            $rs = $this->db->exec("UPDATE tgroup SET group_pathimages1='" . formatSQL($image_name) . "'WHERE group_id = '" . $insert_group_id . "'");
                     }

                     $img_file2 = $_FILES['img2'];
                     if ($img_file2['tmp_name']) {
                            $fileinfo = image_get_info($img_file2['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/group/";

                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "gid2_" . $insert_group_id . "." . $ext;

                            @copy($img_file2['tmp_name'], "{$uploaddir}/{$image_name}");

                            $rs = $this->db->exec("UPDATE tgroup SET group_pathimages2='" . formatSQL($image_name) . "'WHERE group_id = '" . $insert_group_id . "'");
                     }

                     if ($rs) {
                            $this->sys_msg['info'][] = "Groups [$data[group_name]] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Group", "Add Group", "Group_id[{$insert_group_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new Group, please try again.";
                     }
              }
       }

       function EditGroup() {
              $id = (int) $_GET['group_id'];
              list($detail, $dcount) = $this->db->singlearray("select * FROM tgroup WHERE group_id=$id");

              $result = $this->LoadGroupForm($detail);

              return $result;
       }

       function ListGroup() {
              global $mod_link;

              if (isset($_POST['subaction']) == 'DeleteGroup' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteGroup($delkey);
                     }
              }

              $group_parent_id = 0;
              $condition = "";

              if (isset($_GET['group_id']) > 0) {
                     $group_parent_id = (int) $_GET['group_id'];
              }

              $condition = " AND group_parent_id='" . $group_parent_id . "'";

              list($list, $list_count) = $this->db->multiarray("SELECT * FROM tgroup WHERE group_id > 0 $condition ORDER BY group_name ASC");

              $breadcumb_list = array();
              $temp_group_parent_id = $group_parent_id;

              $i = 0;
              while ($temp_group_parent_id != 0) {
                     $link_go = "";
                     list($select_group, $select_group_count) = $this->db->singlearray("SELECT group_name, group_parent_id FROM tgroup WHERE group_id = '" . $temp_group_parent_id . "'");

                     $group_name = $select_group['group_name'];
                     $link_go = $mod_link . "&action=ListGroup&group_id={$temp_group_parent_id}";

                     if ($temp_group_parent_id == $group_parent_id) {
                            $breadcumb_list[$i] = "<li class='active'>{$group_name}</li>";
                     } else {
                            $breadcumb_list[$i] = "<li><a href='{$link_go}'>{$group_name}</a></li>";
                     }

                     $temp_group_parent_id = $select_group['group_parent_id'];
                     $i++;
              }

              $this->smarty->assign("breadcumb_list", $breadcumb_list);
              $this->smarty->assign("list", $list);
              $this->smarty->assign("action", "ListGroup");
              $result = $this->smarty->fetch("CSTPL_Group_List.php");

              return $result;
       }

       function DeleteGroup($id) {
              $rs = $this->db->exec("DELETE FROM tgroup WHERE group_id='$id'");

              if ($rs) {
                     $this->sys_msg['info'][] = "Group [$id] has been deleted";
                     writeSysLog("Group", "Delete Group", "Group_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete Group, please try again.";
                     return false;
              }
       }

       #Group FUNCTION END

       function _getUniqueGroupAlias($group_id = 0, $alias = '') {
              $alias = trim($alias);
              $alias = preg_replace("/\s+/", '-', $alias);
              $alias = preg_replace("/[^A-Za-z0-9\-\_]/", '', $alias);

              $alias = preg_replace("/-+/", '-', $alias);
              $alias = preg_replace("/^-/", '', $alias);
              $alias = preg_replace("/-$/", '', $alias);

              $sql_alias = $alias;

              $index = 1;

              while ($check = $this->db->field("SELECT count(*) FROM tgroup WHERE group_alias='$sql_alias' AND group_id != '$group_id'")) {
                     $sql_alias = $alias . '-' . $index;
                     $index++;
              }

              return strtolower($sql_alias);
       }

       function LoadGroupSearch() {
              $this->smarty->assign('action', "ValidateGroupSearchForm");
              $result = $this->smarty->fetch("CSTPL_Group_Search_Form.php");
              return $result;
       }

       function ValidateGroupSearchForm() {
              global $mod_link;

              $error = 0;

              if ($_POST['group_id'] == '') {
                     $this->sys_msg['error'][] = "<b>Group ID</b> cannot be blank.";
                     $error = 1;
              } else {
                     $check = $this->db->field("SELECT count(*) FROM tgroup WHERE group_id = '" . formatSQL($_POST['group_id']) . "'");
                     if ($check == 0) {
                            $this->sys_msg['error'][] = "<b>Group ID</b> not exist on database, please try again.";
                            $error = 1;
                     }
              }

              if (!$error) {
                     $group_id = (int) $_POST['group_id'];
                     $link_go = $mod_link . "&action=EditGroup&group_id={$group_id}";
                     header("Location: {$link_go}");
                     exit;
              } else {
                     $result .= $this->LoadGroupSearch($_POST);
              }

              return $result;
       }

}

?>