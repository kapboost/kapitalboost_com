<?php
class Blog extends Sites{
    public function __construct(){
        global $module_id, $action_id, $db;
        parent::__construct();
        
        if ($module_id > 0 && $this->usergroup && $this->login_id){
            if ( $action_id > 0 ){
                $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                if (!$check){
                    die("Invalid operation access spotted, please check with administrator");
                }
            }
            else{
                $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                if (!$check)
                {
                    die("Invalid operation access spotted, please check with administrator");
                }
            }
        }
        else{
            die("You don't have permission to access this page, please check with administrator");
        }
    }
    
    function LoadDataForm($data=NULL){
        if($data){
            $this->smarty->assign($data);
        }
        $this->smarty->assign('status_options', $this->config['banner_status_options']);
        $content = $this->smarty->fetch('CSTPL_Blog_Form.php');
        return $content;
    }
    
    function LoadDataList(){
        if(isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])){
            foreach($_POST['list_id'] as $delkey){
                $this->DeleteData($delkey);
            }
        }
        
        //Search
        if (isset($_POST['keyword']) && !empty($_POST['keyword']))
        {
            $keyword = $_POST['keyword'];
            $filter .= "AND (slug LIKE '%".$keyword."%' OR blog_title LIKE '%".$keyword."%')";
            
            $this->smarty->assign('keyword', $keyword);
        }
        $sql = "SELECT * FROM tblog WHERE blog_id > 0 {$filter} ORDER BY blog_id DESC";
        list($list, $list_count) = $this->db->multiarray($sql);
        
        $this->smarty->assign('list', $list);
        
        $content = $this->smarty->fetch('CSTPL_Blog_List.php');
        return $content;
    }
    
    function EditData(){
        $id = (int)$_GET['blog_id'];
        
        $sql = "SELECT * FROM tblog WHERE blog_id='".$id."'";
        list($detail, $detail_count) = $this->db->singlearray($sql);
        
        $release_datetime = explode(" ", $detail['release_date']);
        $release_time=$release_datetime[1];
        $release_date=$release_datetime[0];
        
        $date = explode("-", $release_date);
        $tahun=$date[0];
        $bulan=$date[1];
        $tanggal=$date[2];
        $detail['release_date']=$tanggal."/".$bulan."/".$tahun;
        
        $result = $this->LoadDataForm($detail);
        return $result;
    }
    
    function ValidateDataForm(){
        $result = "";
        $error = 0;
        
        /*if($_POST['slug'] == ''){
            $this->sys_msg['error'][] = "Slug cannot be blank.";
            $error = 1;
        }*/
        if($_POST['blog_title'] == ''){
            $this->sys_msg['error'][] = "Blog Title cannot be blank.";
            $error = 1;
        }
        if($_POST['blog_content'] == ''){
            $this->sys_msg['error'][] = "Blog Content cannot be blank.";
            $error = 1;
        }
        if($_POST['release_date'] == ''){
            $this->sys_msg['error'][] = "Release Date cannot be blank.";
            $error = 1;
        }
        
        $blog_id = $_POST['blog_id'];
        $sql = "SELECT image FROM tblog WHERE blog_id='".$blog_id."'";
        $image = $this->db->field($sql);
        
        if(!$error){
            $result = $this->_UpdateData($_POST);
        }
        else{
            $this->smarty->assign('image', $image);
            $result = $this->LoadDataForm($_POST);
        }
        
        return $result;
    }
    
    /*format URL*/
    function _format_uri( $string, $separator = '-' )
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array( '&' => 'and', "'" => '');
        $string = mb_strtolower( trim( $string ), 'UTF-8' );
        $string = str_replace( array_keys($special_cases), array_values( $special_cases), $string );
        $string = preg_replace( $accents_regex, '$1', htmlentities( $string, ENT_QUOTES, 'UTF-8' ) );
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        return $string;
    }
    
    function _UpdateData($data){
        $uploaddir = "";
        $remote_ip = $_SERVER['REMOTE_ADDR'];
        
        $release_date = $data['release_date'];
        
        $date1 = explode("/", $release_date);
        $tahun1=$date1[2];
        $bulan1=$date1[1];
        $tanggal1=$date1[0];
        $release_date=$tahun1."-".$bulan1."-".$tanggal1;
        
        $slug = $this->_format_uri($data['blog_title']);
        
        if($data['blog_id'] > 0){
            $sql = "UPDATE tblog SET
                slug = '".mysql_real_escape_string($slug)."',
                tag = '".$_POST['hashtags']."',
                blog_title = '".mysql_real_escape_string($data['blog_title'])."',
                blog_content = '".mysql_real_escape_string($data['blog_content'])."',
                enabled = '".mysql_real_escape_string($data['enabled'])."',
                release_date = '".mysql_real_escape_string($release_date)."',
                update_dt = NOW(),
                remote_ip = '".$remote_ip."'
                WHERE blog_id = '".$data['blog_id']."'
            ";
            
            $rs = $this->db->exec($sql);        
            $img_file = $_FILES['img'];
            
            if($img_file['tmp_name']){
                $fileinfo = image_get_info($img_file['tmp_name']);
                $ext = strtolower($fileinfo['extension']);
                
                $uploaddir .= "../assets/images/blog";
                echo $uploaddir."<br>";
                if(!is_dir($uploaddir)){
                    mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                }
                
                $image_name = "blog_".$data['blog_id'].".".$ext;
                
                @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
                
                $sql = "UPDATE tblog SET image='".$image_name."' WHERE blog_id = '".$data['blog_id']."'";
                $rs = $this->db->exec($sql);
            }
            
            if($rs){
                $this->sys_msg['info'][] = "Blog [$data[blog_id]] has been updated.";
                
                writeSysLog("Blog", "Edit Blog", "Blog ID[{$data['parent_id']}] updated");
            }
            else{
                $this->sys_msg['error'][] = "Failed to update Blog, please try again.";
            }
        }
        else{
                           
            $sql = "INSERT INTO tblog SET
                slug = '".mysql_real_escape_string($slug)."',
                tag = '".$_POST['hashtags']."',
                blog_title = '".mysql_real_escape_string($data['blog_title'])."',
                blog_content = '".mysql_real_escape_string($data['blog_content'])."',
                enabled = '".mysql_real_escape_string($data['enabled'])."',
                release_date = '".mysql_real_escape_string($release_date)."',
                image = '".mysql_real_escape_string($data['image'])."',
                insert_dt = NOW(),
                remote_ip = '".$remote_ip."'
            ";
            
            $rs = $this->db->exec($sql);
            $insert_id = mysql_insert_id();
            
            $img_file = $_FILES['img'];
            
            if($img_file['tmp_name']){
                $fileinfo = image_get_info($img_file['tmp_name']);
                $ext = strtolower($fileinfo['extension']);
                
                $uploaddir .= "../assets/images/blog";
                echo $uploaddir."<br>";
                if(!is_dir($uploaddir)){
                    mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                }
                
                $image_name = "blog_".$insert_id.".".$ext;
                
                @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
                
                $sql = "UPDATE tblog SET image='".$image_name."' WHERE blog_id = '".$insert_id."'";
                $rs = $this->db->exec($sql);
            }
            
            if($rs){
                $this->sys_msg['info'][] = "Blog [$data[blog_title]] has been created.";
                $this->sys_msg['warning'][] = "To prblog duplicate new record entry, please do not refresh the browser.";
                writeSysLog("Blog", "Add Blog", "Blog_id[{$insert_id}] added");
            }
            else{
                $this->sys_msg['error'][] = "Failed to add new Blog, please try again.";
            }
        }
    }
    
    function DeleteData($id){
        /*$sql = "SELECT image FROM tblog WHERE blog_id='".$id."'";
        $image = $this->db->field($sql);
        $file = '../images/blog/'.$image;
        unlink($file);*/
        
        $sql = "DELETE from tblog WHERE blog_id='".$id."'";
        $rs = $this->db->exec($sql);
        if($rs){
            $this->sys_msg['info'][] = "Blog [$id] has been deleted.";
            writeSysLog("Blog", "Delete Blog", "Blog_id[{$id}] deleted");
            
            return true;
        }
        else{
            $this->sys_msg['error'][] = "Failed to delete Blog, please try again.";
            
            return false;
        }
    }
}
?>