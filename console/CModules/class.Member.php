<?php

class Member extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListMember();
       }

       #Member FUNCTION BEGIN

       function LoadMemberForm($data = NULL) {
              list($country, ) = $this->db->multiarray("SELECT * FROM tcountry ORDER BY name ASC");
              list($nationality, ) = $this->db->multiarray("SELECT * FROM tnationality ORDER BY nationality ASC");

              if ($data) {
                     $this->smarty->assign($data);
              }

              foreach ($country as $row) {
                     $member_country_options[$row['name']] = $row['name'];
              }

              foreach ($nationality as $row2) {
                     $nationality_options[$row2['nationality']] = $row2['nationality'];
              }

              $this->smarty->assign("nationality_options", $nationality_options);
              $this->smarty->assign("member_country_options", $member_country_options);

              $this->smarty->assign('member_type_options', $this->config['member_type_options']);
              $this->smarty->assign('member_title_options', $this->config['member_title_options']);
              $this->smarty->assign('member_gender_options', $this->config['member_gender_options']);
              $this->smarty->assign('member_marital_status_options', $this->config['member_marital_status_options']);
              $this->smarty->assign('member_religion_options', $this->config['member_religion_options']);
              $this->smarty->assign('member_industry_options', $this->config['member_industry_options']);
              $this->smarty->assign('action', "ValidateMemberForm");
              $result = $this->smarty->fetch("CSTPL_Member_Form.php");
              return $result;
       }

       function ValidateMemberForm() {
              $error = 0;


              if ($_POST['member_email'] == '') {
                     $this->sys_msg['error'][] = "<b>Email</b> cannot be blank";
                     $error = 1;
              }

              if ($_POST['member_id'] > 0) {
                     $total = $this->db->field("SELECT COUNT(member_email) as total FROM tmember WHERE member_id!='" . $_POST['member_id'] . "' AND member_email = '" . $_POST['member_email'] . "'");

                     if ($total > 0) {
                            $this->sys_msg['error'][] = "<b>Email</b> already registered before";
                            $error = 1;
                     }
              } else {
                     $total = $this->db->field("SELECT COUNT(member_email) as total FROM tmember WHERE member_email = '" . $_POST['member_email'] . "'");

                     if ($total > 0) {
                            $this->sys_msg['error'][] = "<b>Email</b> already registered before";
                            $error = 1;
                     }
              }

              if ($_POST['member_password'] == '') {
                     $this->sys_msg['error'][] = "<b>Password</b> cannot be blank";
                     $error = 1;
              }


              if (!$error) {
                     $result .= $this->_UpdateMember($_POST);
              } else {
                     $result .= $this->LoadMemberForm($_POST);
              }

              return $result;
       }

       function _UpdateMember($data) {
              $remote_ip = $_SERVER['REMOTE_ADDR'];

              if ($data['member_id'] > 0) {
                     $member_email = $data['member_email'];
                     $username = $data['member_username'];
                     $member_first_name = $data['member_surname'];
                     $lastname = $data['member_givenname'];
                     $password = $data['member_password'];
                     $country = $data['member_country'];
                     $title = "";
                     $nric = $data['nric'];
                     $dob = $data['dob'];
                     $resedential_address = $data['resarea'];
                     $phone = $data['member_mobile'];
                     $nationality = $data['current_location'];

                     $nric_imgname = $_FILES["img"]["name"];
                     $nric_pisah = explode(".", $nric_imgname);
                     $nric_imgtype = end($nric_pisah);
                     $nric_file_img = $_FILES["img"]["tmp_name"];
                     $nric_newname = time() . rand() . "." . $nric_imgtype;

                     move_uploaded_file($nric_file_img, "assets/nric/datanric/" . $nric_newname);

                     $check1 = $data['check_1'];
                     $check2 = $data['check_2'];
                     $check3 = $data['check_3'];
                     $check4 = $data['check_4'];
                     $check5 = $data['check_5'];

                     $check6 = $data['check_6'];
                     $check7 = $data['check_7'];
                     $check8 = $data['check_8'];
                     $check9 = $data['check_9'];
                     $check10 = $data['check_10'];
                     $check11 = $data['check_11'];

                     $check12 = $data['check_13'];
                     $id_member = $data['member_id'];
                     $update = "UPDATE tmember SET
                        current_location = '" . $country . "',
                        member_username = '" . $username . "',
                        member_password = '" . $password . "',
                        member_surname = '" . $lastname . "',
                        member_firstname = '" . $member_first_name . "',
                        member_email = '" . $member_email . "',
                        member_country = '" . $nationality . "',
                        nric= '" . $nric . "',
                        dob = '" . $dob . "',
                        residental_area = '" . $resedential_address . "',
                        cek1 = '" . $check1 . "',
                        cek2 = '" . $check2 . "',
                        cek3 = '" . $check3 . "',
                        cek4 = '" . $check4 . "',
                        cek5 = '" . $check5 . "',
                        cek6 = '" . $check6 . "',
                        cek7 = '" . $check7 . "',
                        cek8 = '" . $check8 . "',
                        cek9 = '" . $check9 . "',
                        cek10 = '" . $check10 . "',
                        cek11 = '" . $check11 . "',
                        cek12 = '" . $check12 . "',
                        nric_file='" . $nric_newname . "',
                        member_mobile_no = '" . $phone . "' WHERE member_id = '$id_member'";

                     $rs = $this->db->exec($update);


                     if ($rs) {
                            $this->sys_msg['info'][] = "Member [$data[member_email]] has been updated";
                            writeSysLog("Member", "Edit Member", "Member_id[{$data['member_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Member, please try again.";
                     }
              } else {
                     $member_email = $data['member_email'];
                     $username = $data['member_username'];
                     $member_first_name = $data['member_surname'];
                     $lastname = $data['member_givenname'];
                     $password = $data['member_password'];
                     $country = $data['current_location'];
                     $title = "";
                     $nric = $data['nric'];
                     $dob = $data['dob'];
                     $resedential_address = $data['resarea'];
                     $phone = $data['member_mobile'];
                     $nationality = $data['member_country'];

                     $nric_imgname = $_FILES["img"]["name"];
                     $nric_pisah = explode(".", $nric_imgname);
                     $nric_imgtype = end($nric_pisah);
                     $nric_file_img = $_FILES["img"]["tmp_name"];
                     $nric_newname = time() . rand() . "." . $nric_imgtype;

                     move_uploaded_file($nric_file_img, "assets/nric/datanric/" . $nric_newname);

                     $check1 = $data['check_1'];
                     $check2 = $data['check_2'];
                     $check3 = $data['check_3'];
                     $check4 = $data['check_4'];
                     $check5 = $data['check_5'];

                     $check6 = $data['check_6'];
                     $check7 = $data['check_7'];
                     $check8 = $data['check_8'];
                     $check9 = $data['check_9'];
                     $check10 = $data['check_10'];
                     $check11 = $data['check_11'];

                     $check12 = $data['check_13'];

                     $sql = "INSERT INTO tmember(member_username,member_firstname,member_email,member_surname,member_password,current_location,member_title,nric,dob,residental_area,member_mobile_no,cek1,cek2,cek3,cek4,cek5,cek6,cek7,cek8,cek9,cek10,cek11,cek12,nric_file,member_country) VALUES ('$username','$member_first_name', '$member_email','$lastname','$password','$nationality','$title','$nric','$dob','$resedential_address','$phone','$check1','$check2','$check3','$check4','$check5','$check6','$check7','$check8','$check9','$check10','$check11','$check12','$nric_newname','$country')";

                     $rs = $this->db->exec($sql);

                     if ($rs) {
                            $this->sys_msg['info'][] = "member [$data[partner_name]] has been created.";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Partner", "Add Partner", "Partner_id[{$insert_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new Partner, please try again.";
                     }
              }
       }

       function EditMember() {
              $id = (int) $_GET['member_id'];

              list($detail, $dcount) = $this->db->singlearray("select * FROM tmember WHERE member_id=$id");

              if ($detail['member_birthday'] != '' && $detail['member_birthday'] != '0000-00-00') {
                     $detail['member_birthday'] = date("d/m/Y", strtotime($detail['member_birthday']));
              }
              $result = $this->LoadMemberForm($detail);

              return $result;
       }

       function ListMember() {
              if ($_POST['subaction'] == 'DeleteMember' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteMember($delkey);
                     }
              }
              //Search
              if (isset($_POST['keyword']) && !empty($_POST['keyword'])) {
                     $keyword = $_POST['keyword'];
                     $filter .= "WHERE member_email LIKE '%" . $keyword . "%' OR member_username LIKE '%" . $keyword . "%'";

                     $this->smarty->assign('keyword', $keyword);
              }
              //list($list, $list_count) = $this->db->multiarray("SELECT * FROM tmember ORDER BY member_id DESC");

              $sql = "SELECT * FROM tmember {$filter} ORDER BY member_email ASC";
              list($list, $list_count) = $this->db->multiarray($sql);


              $this->smarty->assign("list", $list);
              $this->smarty->assign("action", "ListMember");
              $result = $this->smarty->fetch("CSTPL_Member_List.php");

              return $result;
       }

       function DeleteMember($id) {
              $rs = $this->db->exec("DELETE FROM tmember WHERE member_id='$id'");

              if ($rs) {
                     $this->sys_msg['info'][] = "Member [$id] has been deleted";
                     writeSysLog("Member", "Delete Member", "Member_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete Member, please try again.";
                     return false;
              }
       }

       #Member FUNCTION END

       function LoadMemberSearch() {
              $this->smarty->assign('action', "ValidateMemberSearchForm");
              $result = $this->smarty->fetch("CSTPL_Member_Search_Form.php");
              return $result;
       }

}

?>