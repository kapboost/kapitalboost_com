<?php

class User extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListUsersGroup();
       }

       function LoadUserForm($data = NULL) {

              global $system_id;

              $this->smarty->assign('Title', 'Add New Users');

              if ($data) {
                     $this->smarty->assign($data);
              }

              list($list, $list_count) = $this->db->multiarray("SELECT * FROM tconsoleusergroup ORDER BY name ASC");

              $this->smarty->assign('action', "ValidateUserForm");
              $this->smarty->assign('usergrouplist', $list);
              $result = $this->smarty->fetch("CSTPL_User_Form.php");
              return $result;
       }

       function ValidateUserForm() {

              $error = 0;

              if ($_POST['console_user'] == '') {
                     $this->sys_msg['error'][] = "<b>User Name</b> cannot be blank";
                     $error = 1;
              }

              if ($_POST['console_password'] == '') {
                     $this->sys_msg['error'][] = "<b>Password</b> cannot be blank";
                     $error = 1;
              }

              if ($_POST['select_usergroup_id'] == '') {
                     $this->sys_msg['error'][] = "<b>User Group</b> must be choosen";
                     $error = 1;
              }

              if ($_POST['email'] != "" && !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                     $this->sys_msg['error'][] = "<b>Email</b> must be valid";
                     $error = 1;
              }

              if (!$error) {
                     $result .= $this->_UpdateUser($_POST);
              } else {
                     $result .= $this->LoadUserForm($_POST);
              }

              return $result;
       }

       function _UpdateUser($data) {


              if ($data['id'] > 0) {
                     $rs = $this->db->exec("UPDATE tmerchantlogin SET 
															usergroup_id='" . formatSQL($data['select_usergroup_id']) . "',
															console_user='" . formatSQL($data['console_user']) . "', 
															console_password=ENCODE('" . $data['console_password'] . "', 'admin')
															WHERE login_id = '" . $data['id'] . "'
															");


                     $rs2 = $this->db->exec("UPDATE tmerchantcontact SET
															 name='" . formatSQL($data['name']) . "',
															 address='" . formatSQL($data['address']) . "',
															 city='" . formatSQL($data['city']) . "',
															 zip='" . formatSQL($data['zip']) . "',
															 state='" . formatSQL($data['state']) . "',
															 country='" . formatSQL($data['country']) . "',
															 work_telephone='" . formatSQL($data['work_telephone']) . "',
															 fax_telephone='" . formatSQL($data['fax_telephone']) . "',
															 mobile='" . formatSQL($data['mobile']) . "',
															 pager='" . formatSQL($data['pager']) . "',
															 email='" . formatSQL($data['email']) . "'
															 WHERE login_id = '" . $data['id'] . "'
															 ");


                     if ($rs && $rs2) {
                            $this->sys_msg['info'][] = "User [{$data['console_user']}] has been updated";
                            writeSysLog("User", "Update User", "login_id[{$data['id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update user, please try again.";
                     }
              } else {

                     $rs = $this->db->exec("INSERT INTO tmerchantlogin SET 
															usergroup_id='" . formatSQL($data['select_usergroup_id']) . "',
															console_user='" . formatSQL($data['console_user']) . "', 
															console_password=ENCODE('" . $data['console_password'] . "', 'admin'),
															datetime=NOW()
															");

                     $insert_user_id = mysql_insert_id();

                     $rs2 = $this->db->exec("INSERT INTO tmerchantcontact SET
															 login_id='" . $insert_user_id . "',
															 name='" . formatSQL($data['name']) . "',
															 address='" . formatSQL($data['address']) . "',
															 city='" . formatSQL($data['city']) . "',
															 zip='" . formatSQL($data['zip']) . "',
															 state='" . formatSQL($data['state']) . "',
															 country='" . formatSQL($data['country']) . "',
															 work_telephone='" . formatSQL($data['work_telephone']) . "',
															 fax_telephone='" . formatSQL($data['fax_telephone']) . "',
															 mobile='" . formatSQL($data['mobile']) . "',
															 pager='" . formatSQL($data['pager']) . "',
															 email='" . formatSQL($data['email']) . "'
															 ");


                     if ($rs && $rs2) {
                            $this->sys_msg['info'][] = "User [{$data['console_user']}] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("User", "Insert User", "login_id[{$insert_user_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new user, please try again.";
                     }
              }
       }

       function EditUser() {
              #!!IMPORTANT : Get using $id instead $login_id to prevent miscommunication.
              $id = (int) $_GET['id'];
              list($detail, $dcount) = $this->db->singlearray("select DECODE(console_password, 'admin') as console_password, console_user, usergroup_id, tmerchantcontact.* FROM tmerchantlogin, tmerchantcontact WHERE tmerchantlogin.login_id = tmerchantcontact.login_id AND tmerchantlogin.login_id=$id");

              $detail['id'] = $detail['login_id'];

              $detail['select_usergroup_id'] = $this->db->field("select usergroup_id FROM tmerchantlogin WHERE login_id=$id");

              unset($detail['login_id']);

              $result = $this->LoadUserForm($detail);

              return $result;
       }

       function ListUser() {

              if ($_POST['subaction'] == 'DeleteUser' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteUser($delkey);
                     }
              }

              list($list, $list_count) = $this->db->multiarray("SELECT tmerchantlogin.console_user, tmerchantlogin.login_id, tconsoleusergroup.name FROM tmerchantlogin, tconsoleusergroup WHERE tconsoleusergroup.usergroup_id = tmerchantlogin.usergroup_id ORDER BY name ASC, console_user ASC");

              $this->smarty->assign("login_id", $this->login_id);
              $this->smarty->assign("list", $list);
              $result = $this->smarty->fetch("CSTPL_User_List.php");

              return $result;
       }

       function DeleteUser($id) {
              $rs = $this->db->exec("DELETE FROM tmerchantlogin WHERE login_id='" . $id . "'");
              $rs2 = $this->db->exec("DELETE FROM tmerchantcontact WHERE login_id='" . $id . "'");

              if ($rs && $rs2) {
                     $this->sys_msg['info'][] = "Users [$id] has been deleted";
                     writeSysLog("User", "Delete User", "login_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete user, please try again.";
                     return false;
              }
       }

       function LoadUserGroupForm($data = NULL) {

              global $system_id;

              $this->smarty->assign('Title', 'Add New Users');

              if ($data) {
                     $this->smarty->assign($data);
              }

              $string = "";

              list($systemlist, $systemlist_count) = $this->db->multiarray("SELECT * FROM tconsolesystem");



              foreach ($systemlist as $key) {
                     $string .= "<p>";
                     $string .= "<fieldset>";
                     $string .= "<legend>System : {$key['name']}</legend>";


                     $systemid = $key['system_id'];

                     list($modulelist, $modulelist_count) = $this->db->multiarray("SELECT DISTINCT(module_id) as module_id FROM tconsolesma where system_id = '$systemid' and action_id!=0");

                     foreach ($modulelist as $key2) {
                            $moduleid = $key2['module_id'];
                            $modulename = $this->db->field("SELECT name FROM tconsolemodule where module_id='$moduleid'");

                            $string .= "<p>";
                            $string .= "<fieldset>";
                            $string .= "<legend>Module : {$modulename}</legend>";
                            $string .= "<table border='0' cellpadding='0' cellspacing='0' class='grid_table wf'>";
                            $string .= "<thead>";
                            $string .= "<th width='20%'>Deny</th>";
                            $string .= "<th width='20%'>Allow</th>";
                            $string .= "<th>Action</th>";
                            $string .= "</thead>";
                            $string .= "<tbody>";

                            $moduleid = $key2['module_id'];
                            list($actionlist, $actionlist_count) = $this->db->multiarray("SELECT * FROM tconsolesma where module_id = '$moduleid' and action_id!=0");


                            foreach ($actionlist as $key3) {
                                   $actionid = $key3['action_id'];
                                   $checked1 = "";
                                   $checked2 = "";
                                   $actionlist_name = $this->db->field("SELECT name FROM tconsoleaction where action_id='$actionid'");

                                   if (isset($data['Action'][$actionid])) {
                                          if ($data['Action'][$actionid] == 1) {
                                                 $checked2 = "checked";
                                          } else {
                                                 $checked1 = "checked";
                                          }
                                   } else {
                                          $checked1 = "checked";
                                   }


                                   $string .= "<tr>";
                                   $string .= "<td><input type='radio' name='Action[$actionid]' value='0' $checked1></td>";
                                   $string .= "<td><input type='radio' name='Action[$actionid]' value='1' $checked2></td>";
                                   $string .= "<td>{$actionlist_name}</td>";
                                   $string .= "</tr>";
                            }

                            $string .= "</table>";
                            $string .= "</fieldset>";
                            $string .= "</p>";
                     }

                     $string .= "</fieldset>";
                     $string .= "</p>";
              }


              $this->smarty->assign('action', "ValidateUserGroupForm");
              $this->smarty->assign('list', $string);
              $result = $this->smarty->fetch("CSTPL_User_Group_Form.php");
              return $result;
       }

       function ValidateUserGroupForm() {

              $error = 0;

              if ($_POST['name'] == '') {
                     $this->sys_msg['error'][] = "<b>User Group Name</b> cannot be blank";
                     $error = 1;
              }


              if (!$error) {
                     $result .= $this->_UpdateUserGroup($_POST);
              } else {
                     $result .= $this->LoadUserGroupForm($_POST);
              }

              return $result;
       }

       function _UpdateUserGroup($data) {

              $remote_ip = $_SERVER['REMOTE_ADDR'];

              if ($data['id'] > 0) {
                     $rs = $this->db->exec("UPDATE tconsoleusergroup SET 
															name='" . formatSQL($data['name']) . "',
															description='" . formatSQL($data['description']) . "',
															update_dt=NOW(), 
															remote_ip='" . $remote_ip . "'
															WHERE usergroup_id = '" . $data['id'] . "'
															");

                     $rs2 = $this->db->exec("DELETE FROM tconsoleusergroup_map WHERE usergroup_id = '" . $data['id'] . "'");

                     foreach ($data['Action'] as $key => $val) {
                            list($detail, $detail_count) = $this->db->singlearray("SELECT system_id, module_id FROM tconsolesma WHERE action_id='" . $key . "'");

                            $rs3 = $this->db->exec("INSERT INTO tconsoleusergroup_map SET 
																usergroup_id='" . $data['id'] . "',
																action_id='" . $key . "',
																module_id='" . $detail['module_id'] . "',
																system_id='" . $detail['system_id'] . "',
																access='" . $val . "'
																");
                     }

                     if ($rs) {
                            $this->sys_msg['info'][] = "User Group [{$data['name']}] has been updated";
                            writeSysLog("User", "Update User Group", "usergroup_id[{$data['id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update group, please try again.";
                     }
              } else {
                     $rs = $this->db->exec("INSERT INTO tconsoleusergroup SET 
															name='" . formatSQL($data['name']) . "',
															description='" . formatSQL($data['description']) . "',
															insert_dt=NOW(), 
															remote_ip='" . $remote_ip . "'
															");

                     $insert_user_group_id = mysql_insert_id();

                     foreach ($data['Action'] as $key => $val) {
                            list($detail, $detail_count) = $this->db->singlearray("SELECT system_id, module_id FROM tconsolesma WHERE action_id='" . $key . "'");

                            $rs2 = $this->db->exec("INSERT INTO tconsoleusergroup_map SET 
																usergroup_id='" . $insert_user_group_id . "',
																action_id='" . $key . "',
																module_id='" . $detail['module_id'] . "',
																system_id='" . $detail['system_id'] . "',
																access='" . $val . "'
																");
                     }


                     if ($rs) {
                            $this->sys_msg['info'][] = "User Group [{$data['name']}] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("User", "Add User Group", "usergroup_id[{$insert_user_group_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new user group, please try again.";
                     }
              }
       }

       function EditUserGroup() {
              #!!IMPORTANT : Get using $id instead $usergroup_id to prevent miscommunication.
              $id = (int) $_GET['id'];
              list($detail, $dcount) = $this->db->singlearray("SELECT * FROM tconsoleusergroup WHERE usergroup_id ='" . $id . "'");

              list($list, $listcount) = $this->db->multiarray("SELECT action_id, access FROM tconsoleusergroup_map WHERE usergroup_id ='" . $id . "'");

              foreach ($list as $key) {
                     $action_id_from_list = $key['action_id'];
                     $access_from_list = $key['access'];
                     $detail['Action'][$action_id_from_list] = $access_from_list;
              }

              $detail['id'] = $detail['usergroup_id'];

              unset($detail['usergroup_id']);

              $result = $this->LoadUserGroupForm($detail);

              return $result;
       }

       function ListUserGroup() {

              if ($_POST['subaction'] == 'DeleteUserGroup' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteUserGroup($delkey);
                     }
              }

              list($list, $list_count) = $this->db->multiarray("SELECT * FROM tconsoleusergroup");

              $this->smarty->assign("list", $list);
              $result = $this->smarty->fetch("CSTPL_User_Group_List.php");

              return $result;
       }

       function DeleteUserGroup($id) {
              $rs = $this->db->exec("DELETE FROM tconsoleusergroup WHERE usergroup_id='" . $id . "'");
              $rs2 = $this->db->exec("DELETE FROM tconsoleusergroup_map WHERE usergroup_id='" . $id . "'");

              if ($rs && $rs2) {
                     $this->sys_msg['info'][] = "User Group [$gid] has been deleted";
                     writeSysLog("User", "Delete User Group", "usergroup_id[{$id}] added");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete user group, please try again.";
                     return false;
              }
       }

       function ListUserStatus() {
              global $CS_LOGIN_TIMEOUT, $login_id;

              list($list, $list_count) = $this->db->multiarray("SELECT * FROM tmerchantlogin");

              $i = 0;
              $currdatetime = date("Y-m-d H:i:s");

              foreach ($list as $key) {
                     if ($key['session'] != "") {
                            //last date time
                            $dtime = strtotime($key["datetime"]);

                            $ddiff = $time - $dtime;

                            //already expired, timeout
                            if ($ddiff > $CS_LOGIN_TIMEOUT) {
                                   $list[$i]['status_login'] = "Timeout";
                            } else {
                                   //now the user still online
                                   $list[$i]['status_login'] = "Online";
                            }
                     }
                     //not found any session ? logged out, the session already removed
                     else {
                            $list[$i]['status_login'] = "Offline";
                     }
                     $i++;
              }


              $this->smarty->assign("login_id", $this->login_id);
              $this->smarty->assign("list", $list);
              $result = $this->smarty->fetch("CSTPL_User_Status_List.php");

              return $result;
       }

       function KickUser() {
              $id = (int) $_GET['id'];

              $rs = $this->db->exec("UPDATE tmerchantlogin SET session='', datetime='' WHERE login_id='$id'");

              if ($rs) {
                     $this->sys_msg['info'][] = "User[$id] has been kicked out";
                     writeSysLog("User", "Kick User", "login_id[{$id}] kicked out");
              } else {
                     $this->sys_msg['info'][] = "User has been not been kicked out. Please try again.";
              }

              return $this->ListUserStatus();
       }

}

?>