<?php
class GeneralPage extends Sites{
	public function __construct(){
		global $module_id, $action_id, $db;
		parent::__construct();
		
		if ($module_id > 0 && $this->usergroup && $this->login_id){
			if ( $action_id > 0 ){
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
				if (!$check){
					die("Invalid operation access spotted, please check with administrator");
				}
			}
			else{
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
				if (!$check)
				{
					die("Invalid operation access spotted, please check with administrator");
				}
			}
		}
		else{
			die("You don't have permission to access this page, please check with administrator");
		}
	}
	
	function LoadDataForm($data=NULL){
		if($data){
			$this->smarty->assign($data);
		}
		
		$content = $this->smarty->fetch('CSTPL_GeneralPage_Form.php');
		return $content;
	}
	
	function LoadDataList(){
		if(isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])){
			foreach($_POST['list_id'] as $delkey){
				$this->DeleteData($delkey);
			}
		}
		
		//Search
		if (isset($_POST['keyword']) && !empty($_POST['keyword']))
		{
			$keyword = $_POST['keyword'];
			$filter .= "AND page_name LIKE '%".$keyword."%'";
			
			$this->smarty->assign('keyword', $keyword);
		}
		$sql = "SELECT * FROM tgeneral_page WHERE page_id > 0 {$filter} ORDER BY page_name ASC";
		list($list, $list_count) = $this->db->multiarray($sql);
		
		$this->smarty->assign('list', $list);
		
		$content = $this->smarty->fetch('CSTPL_GeneralPage_List.php');
		return $content;
	}
	
	function EditData(){
		$id = (int)$_GET['page_id'];
		
		$sql = "SELECT * FROM tgeneral_page WHERE page_id='".$id."'";
		list($detail, $detail_count) = $this->db->singlearray($sql);
		
		$result = $this->LoadDataForm($detail);
		return $result;
	}
	
	function ValidateDataForm(){
		$result = "";
		$error = 0;
		
		if($_POST['page_name'] == ''){
			$this->sys_msg['error'][] = "Page Name cannot be blank.";
			$error = 1;
		}
		if($_POST['page_content'] == ''){
			$this->sys_msg['error'][] = "Page Content cannot be blank.";
			$error = 1;
		}
		if($_POST['release_date'] == ''){
			$this->sys_msg['error'][] = "Release Date cannot be blank.";
			$error = 1;
		}	
		
		if(!$error){
			$result = $this->_UpdateData($_POST);
		}
		else{
			$result = $this->LoadDataForm($_POST);
		}
		
		return $result;
	}
	
	function _UpdateData($data){
		$uploaddir = "";
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		
		$release_date = $data['release_date'];
		
		$date1 = explode("/", $release_date);
		$tahun1=$date1[2];
		$bulan1=$date1[1];
		$tanggal1=$date1[0];
		$release_date=$tahun1."-".$bulan1."-".$tanggal1;
		
		if($data['page_id'] > 0){
			$sql = "UPDATE tgeneral_page SET
				page_name = '".mysql_real_escape_string($data['page_name'])."',
				page_content = '".mysql_real_escape_string($data['page_content'])."',
				release_date = '".mysql_real_escape_string($release_date)."',
				update_dt = NOW(),
				remote_ip = '".$remote_ip."'
				WHERE page_id = '".$data['page_id']."'
			";
			
			$rs = $this->db->exec($sql);
			
			/*
			$img_file = $_FILES['img'];
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/page";
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "page_".$data['page_id'].".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tgeneral_page SET page_image = '".$image_name."' WHERE page_id = '".$_data['page_id']."'";
				echo $sql."<br>";
				$rs = $this->db->exec($sql);
			}
			*/
			
			if($rs){
				$this->sys_msg['info'][] = "General Page [$data[page_id]] has been updated.";
				
				writeSysLog("General Page", "Edit General Page", "General Page ID[{$data['parent_id']}] updated");
			}
			else{
				$this->sys_msg['error'][] = "Failed to update General Page, please try again.";
			}
		}
		else{
			$sql = "INSERT INTO tgeneral_page SET
				page_name = '".mysql_real_escape_string($data['page_name'])."',
				page_content = '".mysql_real_escape_string($data['page_content'])."',
				release_date = '".mysql_real_escape_string($release_date)."',
				insert_dt = NOW(),
				remote_ip = '".$remote_ip."'
			";
			
			$rs = $this->db->exec($sql);
			$insert_id = mysql_insert_id();
			
			/*
			$img_file = $_FILES['img'];
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/page";
				echo $uploaddir."<br>";
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "page_".$insert_id.".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tgeneral_page SET page_image='".$image_name."' WHERE page_id = '".$insert_id."'";
				$rs = $this->db->exec($sql);
			}
			*/
			
			if($rs){
				$this->sys_msg['info'][] = "General Page [$data[page_name]] has been created.";
				$this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
				writeSysLog("General Page", "Add General Page", "Page_id[{$insert_id}] added");
			}
			else{
				$this->sys_msg['error'][] = "Failed to add new General Page, please try again.";
			}
		}
	}
	
	function DeleteData($id){
		/*
		$sql = "SELECT image FROM tgeneral_page WHERE page_id='".$id."'";
		$image = $this->db->field($sql);
		$file = '../images/page/'.$image;
		unlink($file);
		*/
		
		$sql = "DELETE from tgeneral_page WHERE page_id='".$id."'";
		$rs = $this->db->exec($sql);
		if($rs){
			$this->sys_msg['info'][] = "General Page [$id] has been deleted.";
			writeSysLog("General Page", "Delete General Page", "Page_id[{$id}] deleted");
			
			return true;
		}
		else{
			$this->sys_msg['error'][] = "Failed to delete General Page, please try again.";
			
			return false;
		}
	}
}
?>