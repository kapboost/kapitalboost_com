<?php

class Setting extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListThemes();
       }

       #SYSTEM FUNCTION BEGIN

       function LoadThemesForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              list($themeslist, $themeslist_count) = $this->db->multiarray("SELECT themes_id, themes_name, themes_primary FROM core_themes ORDER BY themes_name");

              $this->smarty->assign('themeslist', $themeslist);
              $this->smarty->assign('action', "ValidateThemesForm");
              $result = $this->smarty->fetch("CSTPL_Setting_Themes_Form.php");
              return $result;
       }

       function ValidateThemesForm() {
              $error = 0;

              if ($_POST['themes_id'] == '') {
                     $this->sys_msg['error'][] = "<b>Themes</b> must be selected";
                     $error = 1;
              }

              if (!$error) {
                     $result .= $this->_UpdateThemes($_POST);
              } else {
                     $result .= $this->LoadThemesForm($_POST);
              }

              return $result;
       }

       function _UpdateThemes($data) {
              $rs = $this->db->exec("UPDATE core_themes SET 
														themes_primary=0
														WHERE themes_primary=1
														");

              $rs1 = $this->db->exec("UPDATE core_themes SET 
														themes_primary=1
														WHERE themes_id='" . $data['themes_id'] . "'
														");
              if ($rs && $rs1) {
                     $this->sys_msg['info'][] = "Themes has been updated";
                     writeSysLog("Setting", "Select Themes", "themes_id[{$data['themes_id']}] selected");
              } else {
                     $this->sys_msg['error'][] = "Failed to select themes, please try again.";
              }
       }

}

?>