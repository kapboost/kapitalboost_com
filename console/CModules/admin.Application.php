<?php

class Application extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListSystem();
       }

       #SYSTEM FUNCTION BEGIN

       function LoadSystemForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              $this->smarty->assign('action', "ValidateSystemForm");
              $result = $this->smarty->fetch("CSTPL_Application_System_Form.php");
              return $result;
       }

       function ValidateSystemForm() {
              $error = 0;

              if ($_POST['name'] == '') {
                     $this->sys_msg['error'][] = "<b>System Name</b> cannot be blank";
                     $error = 1;
              }
              if ($_POST['ordering'] != '') {
                     if (!is_numeric($_POST['ordering'])) {
                            $this->sys_msg['error'][] = "<b>Ordering</b> must be numeric";
                            $error = 1;
                     }
              }

              if (!$error) {
                     $result .= $this->_UpdateSystem($_POST);
              } else {
                     $result .= $this->LoadSystemForm($_POST);
              }

              return $result;
       }

       function _UpdateSystem($data) {
              $remote_ip = $_SERVER['REMOTE_ADDR'];

              if ($data['id'] > 0) {
                     $rs = $this->db->exec("UPDATE tconsolesystem SET 
															name='" . formatSQL($data['name']) . "', 
															description='" . formatSQL($data['description']) . "', 
															ordering='" . formatSQL($data['ordering']) . "', 
															update_dt=NOW(), 
															remote_ip='" . $remote_ip . "'
															WHERE system_id = '" . $data['id'] . "'
															");
                     if ($rs) {
                            $this->sys_msg['info'][] = "System [$data[name]] has been updated";
                            writeSysLog("Application", "Edit System", "system_id[{$data['id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update system, please try again.";
                     }
              } else {
                     $rs = $this->db->exec("INSERT INTO tconsolesystem SET 
															name='" . formatSQL($data['name']) . "', 
															description='" . formatSQL($data['description']) . "', 
															ordering='" . formatSQL($data['ordering']) . "', 
															insert_dt=NOW(), 
															remote_ip='" . $remote_ip . "' 
															");

                     $insert_system_id = mysql_insert_id();

                     if ($rs) {
                            $this->sys_msg['info'][] = "Systems [$data[name]] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Application", "Add System", "system_id[{$insert_system_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new system, please try again.";
                     }
              }
       }

       function EditSystem() {
              #!!IMPORTANT : Get using $id instead $system_id to prevent miscommunication system.
              $id = (int) $_GET['id'];
              list($detail, $dcount) = $this->db->singlearray("select * FROM tconsolesystem WHERE system_id=$id");

              $detail['id'] = $detail['system_id'];
              unset($detail['system_id']);

              $result = $this->LoadSystemForm($detail);

              return $result;
       }

       function ListSystem() {
              if ($_POST['subaction'] == 'DeleteSystem' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteSystem($delkey);
                     }
              }

              list($list, $list_count) = $this->db->multiarray("SELECT * FROM tconsolesystem ORDER BY ordering ASC");

              $i = 0;
              foreach ($list as $key) {
                     $list[$i]['total_modules'] = $this->db->field("SELECT count(*) FROM tconsolesma WHERE system_id = '" . $key['system_id'] . "' AND action_id = 0"); //action_id = 0 => first module created, no action
                     $i++;
              }

              $this->smarty->assign("list", $list);
              $this->smarty->assign("action", "ListSystem");
              $result = $this->smarty->fetch("CSTPL_Application_System_List.php");

              return $result;
       }

       function DeleteSystem($id) {
              $rs = $this->db->exec("DELETE FROM tconsolesystem WHERE system_id='$id'");

              if ($rs) {
                     $this->sys_msg['info'][] = "System [$id] has been deleted";
                     writeSysLog("Application", "Delete System", "system_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete system, please try again.";
                     return false;
              }
       }

       #SYSTEM FUNCTION END
       #MODULE FUNCTION BEGIN

       function LoadModuleForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              list($systemlist, $systemlist_count) = $this->db->multiarray("SELECT system_id, name FROM tconsolesystem ORDER BY name");

              $this->smarty->assign('systemlist', $systemlist);
              $this->smarty->assign('action', "ValidateModuleForm");
              $result = $this->smarty->fetch("CSTPL_Application_Module_Form.php");
              return $result;
       }

       function ValidateModuleForm() {
              $error = 0;

              if ($_POST['name'] == '') {
                     $this->sys_msg['error'][] = "<b>Module Name</b> cannot be blank";
                     $error = 1;
              }
              if ($_POST['select_system_id'] == '') {
                     $this->sys_msg['error'][] = "<b>System</b> must be choosen";
                     $error = 1;
              }
              if ($_POST['filename'] == '') {
                     $this->sys_msg['error'][] = "<b>File Name</b> cannot be blank";
                     $error = 1;
              }
              if ($_POST['classname'] == '') {
                     $this->sys_msg['error'][] = "<b>Class Name</b> cannot be blank";
                     $error = 1;
              }
              if ($_POST['ordering'] != '') {
                     if (!is_numeric($_POST['ordering'])) {
                            $this->sys_msg['error'][] = "<b>Ordering</b> must be numeric";
                            $error = 1;
                     }
              }

              if (!$error) {
                     $result .= $this->_UpdateModule($_POST);
              } else {
                     $result .= $this->LoadModuleForm($_POST);
              }

              return $result;
       }

       function _UpdateModule($data) {
              $remote_ip = $_SERVER['REMOTE_ADDR'];

              if ($data['id'] > 0) {
                     $rs = $this->db->exec("UPDATE tconsolemodule SET 
															name='" . formatSQL($data['name']) . "',
															description='" . formatSQL($data['description']) . "', 
															filename='" . formatSQL($data['filename']) . "',
															classname='" . formatSQL($data['classname']) . "',
															ordering='" . formatSQL($data['ordering']) . "', 
															update_dt=NOW(), 
															remote_ip='" . $remote_ip . "'
															WHERE module_id = '" . $data['id'] . "'
															");

                     $rs2 = $this->db->exec("UPDATE tconsolesma 
									SET system_id = '" . formatSQL($data['select_system_id']) . "' WHERE module_id = '" . $data['id'] . "'");

                     if ($rs && $rs2) {
                            $this->sys_msg['info'][] = "Module [$data[name]] has been updated";
                            writeSysLog("Application", "Update Module", "module_id[{$data['id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update module, please try again.";
                     }
              } else {
                     $rs = $this->db->exec("INSERT INTO tconsolemodule SET 
															name='" . formatSQL($data['name']) . "',
															description='" . formatSQL($data['description']) . "', 
															filename='" . formatSQL($data['filename']) . "',
															classname='" . formatSQL($data['classname']) . "',
															ordering='" . formatSQL($data['ordering']) . "', 
															insert_dt=NOW(), 
															remote_ip='" . $remote_ip . "' 
															");

                     $insert_module_id = mysql_insert_id();

                     $rs2 = $this->db->exec("INSERT INTO tconsolesma SET
															 system_id='" . formatSQL($data['select_system_id']) . "',
															 module_id='" . $insert_module_id . "'
															 ");


                     if ($rs && $rs2) {
                            $this->sys_msg['info'][] = "Modules [$data[name]] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Application", "Add Module", "module_id[{$insert_module_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new module, please try again.";
                     }
              }
       }

       function EditModule() {
              #!!IMPORTANT : Get using $id instead $Module_id to prevent miscommunication Module.
              $id = (int) $_GET['id'];
              list($detail, $dcount) = $this->db->singlearray("select * FROM tconsolemodule WHERE module_id=$id");

              $detail['id'] = $detail['module_id'];

              $detail['select_system_id'] = $this->db->field("select system_id FROM tconsolesma WHERE module_id=$id LIMIT 1");

              unset($detail['module_id']);

              $result = $this->LoadModuleForm($detail);

              return $result;
       }

       function ListModule() {
              if ($_POST['subaction'] == 'DeleteModule' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteModule($delkey);
                     }
              }

              list($list, $list_count) = $this->db->multiarray("SELECT DISTINCT(tconsolemodule.module_id), tconsolemodule.name as module_name, tconsolemodule.protected, tconsolesystem.name as system_name FROM tconsolemodule, tconsolesma, tconsolesystem WHERE tconsolesma.module_id = tconsolemodule.module_id AND tconsolesma.system_id = tconsolesystem.system_id ORDER BY system_name, module_name");

              $i = 0;
              foreach ($list as $key) {
                     $list[$i]['total_action'] = $this->db->field("SELECT count(*) FROM tconsolesma WHERE module_id = '" . $key['module_id'] . "' AND action_id != 0"); //action_id = 0 => first module created, no action
                     $i++;
              }

              $this->smarty->assign("list", $list);
              $this->smarty->assign("action", "ListModule");
              $result = $this->smarty->fetch("CSTPL_Application_Module_List.php");

              return $result;
       }

       function DeleteModule($id) {
              $rs = $this->db->exec("DELETE FROM tconsolemodule WHERE module_id='$id'");
              $rs2 = $this->db->exec("DELETE FROM tconsolesma WHERE module_id='$id'");

              if ($rs && $rs2) {
                     $this->sys_msg['info'][] = "Module [$id] has been deleted";
                     writeSysLog("Application", "Delete Module", "module_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete module, please try again.";
                     return false;
              }
       }

       #MODULE FUNCTION END
       #ACTION FUNCTION BEGIN

       function LoadActionForm($data = NULL) {
              if ($data) {
                     $this->smarty->assign($data);
              }

              list($modulelist, $modulelist_count) = $this->db->multiarray("SELECT DISTINCT(tconsolemodule.module_id), tconsolemodule.name as module_name, tconsolesystem.name as system_name FROM tconsolemodule, tconsolesma, tconsolesystem WHERE tconsolesma.module_id = tconsolemodule.module_id AND tconsolesma.system_id = tconsolesystem.system_id ORDER BY system_name, module_name");

              $this->smarty->assign('modulelist', $modulelist);
              $this->smarty->assign('action', "ValidateActionForm");
              $result = $this->smarty->fetch("CSTPL_Application_Action_Form.php");
              return $result;
       }

       function ValidateActionForm() {
              $error = 0;

              if ($_POST['name'] == '') {
                     $this->sys_msg['error'][] = "<b>Action Name</b> cannot be blank";
                     $error = 1;
              }
              if ($_POST['select_module_id'] == '') {
                     $this->sys_msg['error'][] = "<b>System -> Module</b> must be choosen";
                     $error = 1;
              }
              if ($_POST['argument'] == '') {
                     $this->sys_msg['error'][] = "<b>Function Name</b> cannot be blank";
                     $error = 1;
              }
              if ($_POST['ordering'] != '') {
                     if (!is_numeric($_POST['ordering'])) {
                            $this->sys_msg['error'][] = "<b>Ordering</b> must be numeric";
                            $error = 1;
                     }
              }

              if (!$error) {
                     $result .= $this->_UpdateAction($_POST);
              } else {
                     $result .= $this->LoadActionForm($_POST);
              }

              return $result;
       }

       function _UpdateAction($data) {
              $remote_ip = $_SERVER['REMOTE_ADDR'];

              $get_system_id = $this->db->field("SELECT system_id FROM tconsolesma WHERE module_id = '" . formatSQL($data['select_module_id']) . "' LIMIT 1");

              if ($data['id'] > 0) {
                     $rs = $this->db->exec("UPDATE tconsoleaction SET 
															name='" . formatSQL($data['name']) . "',
															description='" . formatSQL($data['description']) . "', 
															argument='" . formatSQL($data['argument']) . "',
															update_dt=NOW(), 
															remote_ip='" . $remote_ip . "'
															WHERE action_id = '" . $data['id'] . "'
															");

                     $rs2 = $this->db->exec("UPDATE tconsolesma SET
															 system_id='" . $get_system_id . "',
															 module_id='" . formatSQL($data['select_module_id']) . "',
															 ordering='" . formatSQL($data['ordering']) . "'
															 WHERE action_id = '" . $data['id'] . "'
															 ");

                     if ($rs && $rs2) {
                            $this->sys_msg['info'][] = "Action [$data[name]] has been updated";
                            writeSysLog("Application", "Update Action", "action_id[{$data['id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update action, please try again.";
                     }
              } else {
                     $rs = $this->db->exec("INSERT INTO tconsoleaction SET 
															name='" . formatSQL($data['name']) . "',
															description='" . formatSQL($data['description']) . "', 
															argument='" . formatSQL($data['argument']) . "',
															insert_dt=NOW(), 
															remote_ip='" . $remote_ip . "'
															");

                     $insert_action_id = mysql_insert_id();

                     $rs2 = $this->db->exec("INSERT INTO tconsolesma SET
															 system_id='" . $get_system_id . "',
															 module_id='" . formatSQL($data['select_module_id']) . "',
															 action_id='" . $insert_action_id . "',
															 ordering='" . formatSQL($data['ordering']) . "'
															 ");


                     if ($rs && $rs2) {
                            $this->sys_msg['info'][] = "Actions [$data[name]] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Application", "Add Action", "action_id[{$insert_action_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new action, please try again.";
                     }
              }
       }

       function EditAction() {
              #!!IMPORTANT : Get using $id instead $action_id to prevent miscommunication Action.
              $id = (int) $_GET['id'];
              list($detail, $dcount) = $this->db->singlearray("select * FROM tconsoleaction WHERE action_id=$id");

              $detail['id'] = $detail['action_id'];

              $detail['select_module_id'] = $this->db->field("select module_id FROM tconsolesma WHERE action_id=$id LIMIT 1");
              $detail['ordering'] = $this->db->field("select ordering FROM tconsolesma WHERE action_id=$id LIMIT 1");

              unset($detail['action_id']);

              $result = $this->LoadActionForm($detail);

              return $result;
       }

       function ListAction() {
              if ($_POST['subaction'] == 'DeleteAction' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteAction($delkey);
                     }
              }

              list($list, $list_count) = $this->db->multiarray("SELECT tconsoleaction.action_id, tconsoleaction.protected, tconsoleaction.name as action_name, tconsolemodule.name as module_name, tconsolesystem.name AS system_name FROM tconsoleaction, tconsolesystem, tconsolemodule, tconsolesma WHERE tconsoleaction.action_id = tconsolesma.action_id AND tconsolemodule.module_id = tconsolesma.module_id AND tconsolesystem.system_id = tconsolesma.system_id ORDER BY system_name, module_name, action_name");


              $this->smarty->assign("list", $list);
              $this->smarty->assign("action", "ListAction");
              $result = $this->smarty->fetch("CSTPL_Application_Action_List.php");

              return $result;
       }

       function DeleteAction($id) {
              $rs = $this->db->exec("DELETE FROM tconsoleaction WHERE action_id='$id'");
              $rs2 = $this->db->exec("DELETE FROM tconsolesma WHERE action_id='$id'");
              $rs3 = $this->db->exec("DELETE FROM tconsoleusergroup_map WHERE action_id = '$id'");

              if ($rs && $rs2 && $rs3) {
                     $this->sys_msg['info'][] = "Action [$id] has been deleted";
                     writeSysLog("Application", "Delete Action", "action_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete action, please try again.";
                     return false;
              }
       }

       #ACTION FUNCTION END
}

?>