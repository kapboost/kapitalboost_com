<?php
class Partner extends Sites{
	public function __construct(){
		global $module_id, $action_id, $db;
		parent::__construct();
		
		if ($module_id > 0 && $this->usergroup && $this->login_id){
			if ( $action_id > 0 ){
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
				if (!$check){
					die("Invalid operation access spotted, please check with administrator");
				}
			}
			else{
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
				if (!$check)
				{
					die("Invalid operation access spotted, please check with administrator");
				}
			}
		}
		else{
			die("You don't have permission to access this page, please check with administrator");
		}
	}
	
	function LoadDataForm($data=NULL){
		if($data){
			$this->smarty->assign($data);
		}
		// echo "<br><br><br><br><br>";
		// print"<pre>";
		// print_r($data);
		// print"</pre>";
		$this->smarty->assign('status_options', $this->config['banner_status_options']);
		$content = $this->smarty->fetch('CSTPL_Partner_Form.php');
		return $content;
	}
	
	function LoadDataList(){
		// echo "<br><br><br><br><br>";
		if(isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])){
			// echo "true<br>";
			foreach($_POST['list_id'] as $delkey){
				$this->DeleteData($delkey);
			}
		}
		
		// print"<pre>";
		// print_r($_POST);
		// print"</pre>";
		
		//Search
		if (isset($_POST['keyword']) && !empty($_POST['keyword']))
		{
			$keyword = $_POST['keyword'];
			$filter .= "AND partner_name LIKE '%".$keyword."%'";
			
			$this->smarty->assign('keyword', $keyword);
		}
		$sql = "SELECT partner_id, partner_name, partner_image, partner_url FROM tpartner WHERE partner_id > 0 {$filter} ORDER BY partner_id DESC";
		// list($list, $list_count) = $this->db->multiarray($sql);
		list($list, $list_count) = $this->db->multiarray($sql);
		
		$this->smarty->assign('list', $list);
		
		$content = $this->smarty->fetch('CSTPL_Partner_List.php');
		return $content;
	}
	
	function EditData(){
		$id = (int)$_GET['partner_id'];
		
		$sql = "SELECT * FROM tpartner WHERE partner_id='".$id."'";
		list($detail, $detail_count) = $this->db->singlearray($sql);
		
		$result = $this->LoadDataForm($detail);
		return $result;
	}
	
	function ValidateDataForm(){
		$result = "";
		$error = 0;
		
		if($_POST['partner_name'] == ''){
			$this->sys_msg['error'][] = "Partner Name cannot be blank.";
			$error = 1;
		}
		if($_POST['partner_url'] == ''){
			$this->sys_msg['error'][] = "Partner URL cannot be blank.";
			$error = 1;
		}
		
		$partner_id = $_POST['partner_id'];
		$sql = "SELECT partner_image FROM tpartner WHERE partner_id='".$partner_id."'";
		$partner_image = $this->db->field($sql);
		
		if(!$error){
			$result = $this->_UpdateData($_POST);
		}
		else{
			$this->smarty->assign('partner_image', $partner_image);
			$result = $this->LoadDataForm($_POST);
		}
		
		return $result;
	}
	
	function _UpdateData($data){
		$uploaddir = "";
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		
		if($data['partner_id'] > 0){
			$sql = "UPDATE tpartner SET
				partner_name = '".mysql_real_escape_string($data['partner_name'])."',
				partner_description = '".mysql_real_escape_string($data['partner_description'])."',
				partner_url = '".mysql_real_escape_string($data['partner_url'])."',
				enabled = '".mysql_real_escape_string($data['enabled'])."',
				update_dt = NOW(),
				remote_ip = '".$remote_ip."'
				WHERE partner_id = '".$data['partner_id']."'
			";
			// echo "<br><br><br><br><br>";
			// echo $sql."<br>";
			
			$rs = $this->db->exec($sql);
			
			$img_file = $_FILES['img'];
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/partner";
				// echo $uploaddir."<br>";
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "partner_".$data['partner_id'].".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tpartner SET partner_image = '".$image_name."' WHERE partner_id = '".$_data['partner_id']."'";
				echo $sql."<br>";
				$rs = $this->db->exec($sql);
			}
			// print"<pre>";
			// print_r($_FILES['img']);
			// print_r($fileinfo);
			// print"</pre>";
			
			if($rs){
				$this->sys_msg['info'][] = "Partner [$data[partner_id]] has been updated.";
				
				writeSysLog("Partner", "Edit Partner", "Partner ID[{$data['parent_id']}] updated");
			}
			else{
				$this->sys_msg['error'][] = "Failed to update Partner, please try again.";
			}
		}
		else{
			$sql = "INSERT INTO tpartner SET
				partner_name = '".mysql_real_escape_string($data['partner_name'])."',
				partner_description = '".mysql_real_escape_string($data['partner_description'])."',
				partner_url = '".mysql_real_escape_string($data['partner_url'])."',
				enabled = '".mysql_real_escape_string($data['enabled'])."',
				insert_dt = NOW(),
				remote_ip = '".$remote_ip."'
			";
			// echo "<br><br><br><br><br>";
			// echo $sql."<br>";
			
			$rs = $this->db->exec($sql);
			$insert_id = mysql_insert_id();
			
			$img_file = $_FILES['img'];
			
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/partner";
				echo $uploaddir."<br>";
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "partner_".$insert_id.".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tpartner SET partner_image='".$image_name."' WHERE partner_id = '".$insert_id."'";
				// echo $sql."<br>";
				$rs = $this->db->exec($sql);
			}
			// print"<pre>";
			// print_r($_FILES['img']);
			// print_r($fileinfo);
			// print"</pre>";
			
			if($rs){
				$this->sys_msg['info'][] = "Partner [$data[partner_name]] has been created.";
				$this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
				writeSysLog("Partner", "Add Partner", "Partner_id[{$insert_id}] added");
			}
			else{
				$this->sys_msg['error'][] = "Failed to add new Partner, please try again.";
			}
		}
	}
	
	function DeleteData($id){
		$sql = "SELECT partner_image FROM tpartner WHERE partner_id='".$id."'";
		$image = $this->db->field($sql);
		$file = '../images/partner/'.$image;
		unlink($file);
		
		$sql = "DELETE from tpartner WHERE partner_id='".$id."'";
		// echo $sql."<br>";
		$rs = $this->db->exec($sql);
		// exit();
		if($rs){
			$this->sys_msg['info'][] = "Partner [$id] has been deleted.";
			writeSysLog("Partner", "Delete Partner", "Partner_id[{$id}] deleted");
			
			return true;
		}
		else{
			$this->sys_msg['error'][] = "Failed to delete Partner, please try again.";
			
			return false;
		}
	}
}
?>