<?php

/**
 *
 *
 *
 *
 */
class Audit extends Sites {

       private $mod_link;
       private $color = array('#b60404', '#d13632', '#e2571e', '#ec883a', '#e69333', '#d6a525',
           '#cdb924', '#d1d22c', '#ebec02', '#96bf33', '#479e1b',
           '#1da64c', '#11b7a5', '#016c9e', '#0e42b6', '#503fa9',
           '#4d1295', '#8119c9', '#a8225f', '#c83964', '#e45fa4');

       public function __construct() {
              global $mod_link;
              parent::__construct();
       }

       public function LoadDefault() {
              
       }

       function LoadOrderStatus($w, $h) {
              $sql = "SELECT count(*) as total, status 
				FROM ttransaction 
				GROUP BY status";

              list($list, $listcount) = $this->db->multiarray($sql);

              if ($listcount > 0) {
                     foreach ($list as $l) {
                            if ($l['status'] == 0) {
                                   $legend = "New";
                            }
                            if ($l['status'] == 3) {
                                   $legend = "Shipped";
                            }
                            if ($l['status'] == 9) {
                                   $legend = "Cancel";
                            }

                            $data[] = '{"Status":"' . $legend . '","Total":' . $l['total'] . '}';
                     }
              }


              if (count($data) > 0) {

                     $this->smarty->assign('graph_id', 'order-status');
                     $this->smarty->assign('title', 'Order Status');
                     $this->smarty->assign('vTitle', 'Status');
                     $this->smarty->assign('hTitle', 'Total');
                     $this->smarty->assign('w', $w);
                     $this->smarty->assign('h', $h);
                     $this->smarty->assign('data', '[' . implode(",", $data) . ']');

                     $this->smarty->assign('link', 'LoadOrderStatus');
                     $this->smarty->assign('order_status_options', $this->config['order_status']);
                     $GRAPH_SECTION = $this->smarty->fetch('chart/CSTPL_Pie2.php');
              } else {
                     $GRAPH_SECTION = 'No data available';
              }

              return $GRAPH_SECTION;
       }

       function LoadDailySales($w, $h) {
              $to = date('Y-m-d');
              $from = date('Y-m-d', strtotime($to . "-14 days"));

              $sql = "SELECT DATE(insert_dt) as dt, count(*) as total
				FROM  ttransaction 
				WHERE insert_dt >= '{$from} 00:00:00' AND insert_dt <= '{$to} 23:59:59' AND status != '9' 
				GROUP BY DATE(insert_dt)";

              list($list, $listcount) = $this->db->multiarray($sql);


              $total = 0;

              if ($listcount > 0) {
                     foreach ($list as $l) {
                            $data[] = '{"Date":"' . date('d M', strtotime($l['dt'])) . '","Total":' . $l['total'] . '}';
                     }
              }

              if (count($data) > 0) {

                     $this->smarty->assign('graph_id', 'daily-sales');
                     $this->smarty->assign('title', 'Daily Sales');
                     $this->smarty->assign('vTitle', 'Transaction');
                     $this->smarty->assign('hTitle', 'Date');
                     $this->smarty->assign('w', $w);
                     $this->smarty->assign('h', $h);
                     $this->smarty->assign('data', '[' . implode(",", $data) . ']');

                     $this->smarty->assign('link', 'LoadDailySales');
                     $this->smarty->assign('range_start', date('d-m-Y', strtotime($from)));
                     $this->smarty->assign('range_end', date('d-m-Y', strtotime($to)));

                     $GRAPH_SECTION = $this->smarty->fetch('chart/CSTPL_Column2.php');
              } else {
                     $GRAPH_SECTION = 'No data available';
              }

              return $GRAPH_SECTION;
       }

       function LoadDailyTotal($w, $h) {
              $to = date('Y-m-d');
              $from = date('Y-m-d', strtotime($to . "-14 days"));

              $sql = "SELECT DATE(insert_dt) as dt, SUM(grand_total) as gt
				FROM  ttransaction 
				WHERE insert_dt >= '{$from} 00:00:00' AND insert_dt <= '{$to} 23:59:59'
				AND status != '9'
				GROUP BY DATE(insert_dt)";

              list($list, $listcount) = $this->db->multiarray($sql);


              $total = 0;

              if ($listcount > 0) {
                     foreach ($list as $l) {
                            $data[] = '{"Total (SGD)":' . $l['gt'] . ',"Date":"' . date('d M', strtotime($l['dt'])) . '"}';
                     }
              }

              if (count($data) > 0) {

                     $this->smarty->assign('graph_id', 'daily-total');
                     $this->smarty->assign('title', 'Daily Total');
                     $this->smarty->assign('vTitle', 'Transaction');
                     $this->smarty->assign('hTitle', 'Grand');
                     $this->smarty->assign('w', $w);
                     $this->smarty->assign('h', $h);
                     $this->smarty->assign('data', '[' . implode(",", $data) . ']');

                     $this->smarty->assign('link', 'LoadDailyTotal');
                     $this->smarty->assign('range_start', date('d-m-Y', strtotime($from)));
                     $this->smarty->assign('range_end', date('d-m-Y', strtotime($to)));

                     $GRAPH_SECTION = $this->smarty->fetch('chart/CSTPL_Column2.php');
              } else {
                     $GRAPH_SECTION = 'No data available';
              }

              return $GRAPH_SECTION;
       }

       function LoadMonthlySales($w, $h) {
              $year = date('Y');

              $sql = "SELECT MONTH(insert_dt) as m,count(*) as total
				FROM  ttransaction 
				WHERE YEAR(insert_dt) = '{$year}'
				GROUP BY MONTH(insert_dt)
				ORDER BY MONTH(insert_dt) ASC";

              list($list, $listcount) = $this->db->multiarray($sql);

              $total = 0;

              $month = date('m');

              if ($listcount > 0) {
                     for ($i = 1; $i <= $month; $i++) {
                            $found = false;

                            foreach ($list as $l) {
                                   if ($l['m'] == $i) {
                                          $found = true;

                                          $data[] = '{"Month":"' . $l['m'] . ' / ' . $year . '","Total":' . $l['total'] . '}';
                                   }
                            }

                            if (!$found) {
                                   $data[] = '{"Month":"' . $i . ' / ' . $year . '","Total":0}';
                            }
                     }
              }



              if (count($data) > 0) {
                     $this->smarty->assign('graph_id', 'monthly-sales');
                     $this->smarty->assign('title', 'Monthly Sales');
                     $this->smarty->assign('vTitle', 'Transaction');
                     $this->smarty->assign('hTitle', 'Month');
                     $this->smarty->assign('w', $w);
                     $this->smarty->assign('h', $h);
                     $this->smarty->assign('data', '[' . implode(",", $data) . ']');

                     $this->smarty->assign('link', 'LoadMonthlySales');
                     $this->smarty->assign('range_start', date('d-m-Y', strtotime($from)));
                     $this->smarty->assign('range_end', date('d-m-Y', strtotime($to)));

                     $GRAPH_SECTION = $this->smarty->fetch('chart/CSTPL_Line2.php');
              } else {
                     $GRAPH_SECTION = 'No data available';
              }

              return $GRAPH_SECTION;
       }

       function week_range($month, $year) {
              $start = 1;
              $end = date('t', strtotime($year . '-' . $month . '-01'));

              $week = 0;
              $prev_week = "";

              for ($i = $start; $i <= $end; $i++) {
                     $day = sprintf("%02s", $i);
                     $date = $year . '-' . $month . '-' . $day;
                     $current_week = date('W', strtotime($date));

                     if ($current_week != $prev_week) {
                            $week++;
                     }

                     $arr[$date] = $week;

                     $prev_week = $current_week;
              }
              return $arr;
       }

       function dateRange($first, $last, $min = false, $step = '+1 day', $format = 'Y-m-d') {

              $dates = array();
              $current = strtotime($first);
              $last = strtotime($last);

              if ($min) {
                     $i = 0;
                     while ($current <= $last || $i <= $min) {

                            $dates[] = date($format, $current);
                            $current = strtotime($step, $current);
                            $i++;
                     }
              } else {
                     while ($current <= $last) {

                            $dates[] = date($format, $current);
                            $current = strtotime($step, $current);
                     }
              }

              return $dates;
       }

       function getStartAndEndDate($week, $year) {

              $time = strtotime("1 January $year", time());
              $day = date('w', $time);
              $time += ((7 * $week) + 1 - $day) * 24 * 3600;
              $return[0] = date('Y-m-j', $time);
              $time += 6 * 24 * 3600;
              $return[1] = date('Y-m-j', $time);
              return $return;
       }

       function LoadWeeklyProductSales($w, $h) {
              $to = date('Y-m-d');
              $from = date('Y-m-d', strtotime($to . "-7 days"));

              $sql = "SELECT product_name as product, SUM(product_quantity) as total
				FROM  tshoppingcart_detail 
				WHERE insert_dt >= '{$from} 00:00:00' AND insert_dt <= '{$to} 23:59:59'
				GROUP BY product_name 
				ORDER BY total ASC";
              // echo $sql;		
              list($list, $listcount) = $this->db->multiarray($sql);




              if ($listcount > 0) {
                     foreach ($list as $l) {
                            $data[] = '{"Product":"' . $l['product'] . '","Total":' . $l['total'] . '}';
                     }
              }

              if (count($data) > 0) {

                     $this->smarty->assign('graph_id', 'weekly-product-sales');
                     $this->smarty->assign('title', 'Weekly Product Total Sales');
                     $this->smarty->assign('hTitle', 'Total Sales');
                     $this->smarty->assign('vTitle', 'Product Name');
                     $this->smarty->assign('w', $w);
                     $this->smarty->assign('h', $h);
                     $this->smarty->assign('data', '[' . implode(",", $data) . ']');

                     $this->smarty->assign('link', 'LoadWeeklyProductSales');
                     $this->smarty->assign('range_start', date('d-m-Y', strtotime($from)));
                     $this->smarty->assign('range_end', date('d-m-Y', strtotime($to)));

                     $GRAPH_SECTION = $this->smarty->fetch('chart/CSTPL_Gantt.php');
              } else {
                     $GRAPH_SECTION = 'No data available';
              }

              return $GRAPH_SECTION;
       }

}
