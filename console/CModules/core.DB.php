<?php

class DB {

       var $classname = "DB";
       var $DBDEBUG = true;
       var $DEBUGINFO = "";
       var $replace_pairs = array("\\\"" => "\"", "\\'" => "''"); // to translate the \" to " and \' to ''
       var $sqllink = null;

       function connect($server, $username, $password,$database) {
              $this->sqllink = new mysqli($server, $username, $password, $database);
              if (mysqli_connect_error()) {
                     return false;
              }
              mysqli_set_charset($this->sqllink, 'utf8');
              return true;
       }

       function exec($QUERY) {
              $QUERY = trim($QUERY);
              if (mysqli_query($this->sqllink, $QUERY)) {
                     return 1;
              } else {
                     return 0;
              }
       }

       function field($QUERY) {
              $result = mysqli_query($this->sqllink, $QUERY);
              if (mysqli_num_rows($result) == 1) {
                     $sqldata = mysqli_fetch_array($result);
                     $myfield = $sqldata[0];
                     return $myfield;
              } else {
                     return 0;
              }
       }

       function singlearray($QUERY) {
              $myarray = array();
              $QUERY = strtr($QUERY, $this->replace_pairs);

              $result = mysqli_query($this->sqllink, $QUERY);
              if (mysqli_errno()) {
                     return array($myarray, -1);
              }

              $fields = mysqli_num_fields($result);

              while ($sqldata = mysqli_fetch_array($result)) {
                     for ($i = 0; $i < $fields; $i++) {
                            $fieldname = $this->mysqli_field_name($result, $i);
                            $myarray[$fieldname] = "$sqldata[$i]";
                     }
              }
              return array($myarray, mysqli_num_rows($result));
       }

       function multiarray($QUERY) {
              $QUERY = strtr($QUERY, $this->replace_pairs);
              $myarray = array();
              $count = 0;
              $result = mysqli_query($this->sqllink, $QUERY);

              $fields = mysqli_num_fields($result);
              while ($sqldata = mysqli_fetch_array($result)) {
                     for ($i = 0; $i < $fields; $i++) {
                            $fieldname = $this->mysqli_field_name($result, $i);
                            $myarray[$count][$fieldname] = "$sqldata[$i]";
                     }
                     $count++;
              }

              return array($myarray, mysqli_num_rows($result));
       }
       function mysqli_field_name($result, $field_offset) {
              $properties = mysqli_fetch_field_direct($result, $field_offset);
              return is_object($properties) ? $properties->name : null;
       }

       function GetConn(){
              return $this->sqllink;
       }

}

?>
