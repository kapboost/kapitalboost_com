<?php
include_once(DOC_ROOT.'Classes/class.phpmailer.php');
class Contact extends Sites{
    
   public function __construct(){
        global $module_id, $action_id, $db;
        parent::__construct();
        
        //Check user Access
            if ($module_id > 0 && $this->usergroup && $this->login_id)
            {
                if ( $action_id > 0 )
                {
                    $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                    if (!$check)
                    {
                            die("Invalid operation access spotted, please check with administrator");
                    }
                }
                else
                {
                    $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                    if (!$check)
                    {
                            die("Invalid operation access spotted, please check with administrator");
                    }
                }
            }
            else
            {
                    die("You don't have permission to access this page, please check with administrator");
            }
            
    }

   function LoadContactUsReply($data=NULL){
        if($data){
            $this->smarty->assign($data);
        }
        
        $content = $this->smarty->fetch('CSTPL_Contact_Form.php');
        return $content;
    }
    
    function ReplyMessage(){
        $id = (int)$_GET['id'];
        
        $sql = "SELECT * FROM contact_us WHERE id='".$id."'";
        list($detail, $detail_count) = $this->db->singlearray($sql);
        
        $result = $this->LoadContactUsReply($detail);
        return $result;
    }
    function SendMessage(){

$name =$_POST['nama_lengkap'];
$email =$_POST['email'];
$subjek = $_POST['subject'];
$pesan =$_POST['pesan'];

if($pesan==NULL){

$error = "Tidak bisa mengirim pesan!";
}
else{
      $subject = '[KapitalBoost-Reply] '.$subjek.'';
       $message = "
<html>
<head>
  <title>Kapitalboost</title>
</head>
<style type='text/css'>
@import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700);
</style>
<body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;font-family: 'Open Sans';'>
<table height='100%' width='100%' bgcolor='#305e8e'>
<tbody>
<tr>
<td style='height: 100%;width: 100%;padding: 20px 10%;' bgcolor='#305e8e'>
<center>
  <div style='display: inline-block;position: relative;height: auto;width: 640px;padding: 0 0 10px 0;margin: 0 0 0 0;background: #00b0fd;'>
    <div style='display: inline-block;position: relative;height: auto;width: 100%;padding: 0;margin: 0;background: white;text-align: left;'>
      <img src='https://kapitalboost.com/email/logo.jpg' style='margin: 35px 0 0 35px;' />
      <p style='display: block;font-size: 24px;color: #00adfd;text-align: left;margin: 25px 35px 25px 35px;'>
       Dear ".$name.",
      </p>
      <p style='display: block;font-size: 16px;text-align: left;margin: 0 35px 35px 35px;'>
       ".$pesan."
      </p>
      <p style='display: block;font-size: 18px;text-align: left;margin: 0 35px 35px 35px;'>
        Best regards,<br />
        Kapitalboost.com - Kapital Boost
      </p>
    </div>
  </div>
  <div style='display: inline-block;position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
    <p style='font-size: 12px;color: white;'>
      &copy; COPYRIGHT 2015 - KAPITAL BOOST PTE LTD
    </p>
  </div>
</center>
</td>
</tr>
</tbody>
</table>
</body>
</html>
";
                    
                    $this->SendEmail(EMAIL_FROM, $email, array('admin@kapitalboost.com','admin@kapitalboost.com'), $subject, $message, TRUE);
$this->sys_msg['info'][] = "Message  has been send.";
                $this->sys_msg['warning'][] = "To prevent duplicate message, please do not refresh the browser.";
}
}

    function LoadDefault()
        {
        //return $this->ListSystem();
    }
    
    
    function ContactList()
        {
            
                $list = $this->setPager("SELECT * FROM contact_us WHERE id ORDER BY id DESC");

                $this->smarty->assign("list",$list);    
        $result = $this->smarty->fetch("CSTPL_Contact.php");
        
        return $result;                                                     
    }
    
  
        #SYSTEM FUNCTION END
}

?>