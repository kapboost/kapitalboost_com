<?php

class spine {

       function buildsystem() {
              global $db;

              global $login_id, $usergroup, $language, $session;
              global $system_id, $time;

              //check user group
              $usergroup = $db->field("SELECT usergroup_id FROM tmerchantlogin WHERE login_id = '" . formatSQL($login_id) . "'");

              list($validarray, $validarray_count) = $db->multiarray("SELECT DISTINCT(tconsolesystem.system_id) FROM tconsoleusergroup_map, tconsolesystem where tconsolesystem.system_id = tconsoleusergroup_map.system_id AND usergroup_id = '$usergroup' AND access = 1 ORDER BY ordering");

              $system_group = array();

              $i = 0;
              foreach ($validarray as $key) {
                     $system_group[$i]["systemid"] = $key['system_id'];

                     $system_group[$i]["name"] = $db->field("SELECT name FROM tconsolesystem where system_id = '" . $key['system_id'] . "'");

                     $i++;
              }

              return $system_group;
       }

       function buildmodule($system_id) {

              global $db;

              global $login_id, $usergroup, $language, $session;
              global $module_id, $time;

              $usergroup = $db->field("SELECT usergroup_id FROM tmerchantlogin WHERE login_id = '$login_id'");

              list($validarray, $validarray_count) = $db->multiarray("SELECT DISTINCT(tconsoleusergroup_map.module_id) FROM tconsoleusergroup_map, tconsolemodule, tconsolesma where tconsolesma.system_id = '$system_id' AND tconsolesma.module_id = tconsoleusergroup_map.module_id AND tconsoleusergroup_map.module_id = tconsolemodule.module_id AND usergroup_id = '$usergroup' AND access = 1 order by tconsolemodule.ordering");

              $module_group = array();

              $i = 0;
              foreach ($validarray as $key) {

                     $module_group[$i]["moduleid"] = $key['module_id'];

                     $module_group[$i]["name"] = $db->field("SELECT name FROM tconsolemodule where module_id = '" . $key['module_id'] . "'");

                     $i++;
              }

              return $module_group;
       }

       function buildapp($system_id, $module_id) {

              global $db, $time;

              global $login_id, $language, $session;

              $myapps = "";

              $usergroup = $db->field("SELECT usergroup_id FROM tmerchantlogin where login_id='$login_id'");


              //echo ("Build App -- module:[$mymodule]");

              $app_group = array();

              $count = 0;

              list($myappsarray, $myappsarray_count) = $db->multiarray("SELECT * FROM tconsoleaction as console_action, tconsolesma as sma, tconsoleusergroup_map WHERE console_action.action_id = sma.action_id AND tconsoleusergroup_map.action_id = console_action.action_id AND tconsoleusergroup_map.access = 1 AND tconsoleusergroup_map.usergroup_id = $usergroup AND sma.module_id = '$module_id' AND sma.system_id = $system_id ORDER BY ordering");

              $count = 0;
              while (isset($myappsarray[$count])) {

                     $conaid = $myappsarray[$count]['action_id'];

                     if ($db->field("SELECT count(*) AS count FROM tconsoleusergroup_map where usergroup_id='$usergroup' AND action_id='$conaid'")) {

                            $app_group[$count]['name'] = $db->field("SELECT name FROM tconsoleaction WHERE action_id='{$myappsarray[$count]['action_id']}'");

                            $app_group[$count]['link'] = "?t=$time&session=$session&system_id=$system_id&module_id=$module_id&action_id=" . $myappsarray[$count]['action_id'] . "&action=" . $myappsarray[$count]['argument'];

                            $app_group[$count]['actionid'] = $myappsarray[$count]['action_id'];
                     }

                     $count++;
              }

              return $app_group;
       }

       function verifylogin($myuser, $mypwd, $mysession) {
              global $sqllink, $root, $db, $time, $config;
              global $login_id, $usergroup, $CS_LOGIN_TIMEOUT, $language;

              $access = 4;
              $currdatetime = date("Y-m-d H:i:s");
              // 22/08/02 les
              // Set the login id and merchant id

              list($mainarray, $mainarray_count) = $db->singlearray("SELECT * FROM tmerchantlogin WHERE console_user='$myuser' AND decode(console_password, 'admin')='$mypwd'");
              #list($mainarray, $mainarray_count) = $db->singlearray("SELECT * FROM tmerchantlogin WHERE console_user='".$myuser."' AND decode(console_password, 'admin')='".$mypwd."'");

              if ($mainarray_count > 0) {
                     $login_id = $mainarray['login_id'];
                     $usergroup = $mainarray['usergroup_id'];
                     $currsession = $mainarray['session'];
                     $ddatetime = $mainarray['datetime'];

                     if ($currsession && !OVERWRITE_LOGIN_SESSION) {
                            // check if timeout already
                            // check whether got timeout
                            $dtime = strtotime($ddatetime);
                            $smartyime = strtotime($currdatetime);
                            $ddiff = $smartyime - $dtime;

                            if ($ddiff > $CS_LOGIN_TIMEOUT) {
                                   // ok to relogin
                                   #writeSysLog("Access", "Login", "login_id[{$login_id}] online");
                                   writeSysLog("Access", "Login", "login [{$myuser}] online");
                                   $success = $db->exec("UPDATE tmerchantlogin SET session='$mysession', datetime=NOW() WHERE console_user='$myuser'");
                                   $access = 1;
                            } else {
                                   //deny access unless is the same session
                                   if ($currsession == $mysession) {
                                          #writeSysLog("Access", "Login", "login_id[{$login_id}] online");
                                          writeSysLog("Access", "Login", "login [{$myuser}] online");
                                          $success = $db->exec("UPDATE tmerchantlogin SET datetime='$currdatetime' WHERE session='$mysession'");
                                          $access = 1;
                                   } else {
                                          $access = 3;
                                   }
                            }
                     } else {
                            if ($login_id) {
                                   #writeSysLog("Access", "Login", "login_id[{$login_id}] online");
                                   writeSysLog("Access", "Login", "login [{$myuser}] online");
                                   $success = $db->exec("UPDATE tmerchantlogin SET session='$mysession', datetime=NOW() WHERE console_user='$myuser'");
                                   // echo ("Verify Login -- update success :[$success]<br>");
                                   $access = 1;
                            } else {
                                   $access = 2;
                            }
                     }
              }

              return $access;
       }

       function verifysession($mysession) {



              global $sqllink, $root, $db, $time;

              global $login_id, $usergroup, $login, $CS_LOGIN_TIMEOUT;

              $access = 0;


              $currdatetime = date("Y-m-d H:i:s");


              list($mainarray, $mainarray_count) = $db->singlearray("SELECT * FROM tmerchantlogin WHERE session='$mysession'");

              // need to set the globals for merchant_id and login_id

              if ($mainarray_count) {

                     $login_id = $mainarray['login_id'];

                     $usergroup = $mainarray['usergroup_id'];

                     $currsession = $mainarray['session'];



                     $ddatetime = $mainarray['datetime'];


                     $dtime = strtotime($ddatetime);

                     $smartyime = strtotime($currdatetime);


                     $ddiff = $smartyime - $dtime;

                     if ($ddiff > $CS_LOGIN_TIMEOUT) {
                            // timeout already
                            // update null cookie and datetime for the tMerhcantLogin
                            writeSysLog("Access", "Timeout", "login_id[{$login_id}] timeout");
                            $success = $db->exec("UPDATE tmerchantlogin SET datetime='', session='' WHERE session='$mysession'");

                            // echo("Timeout : [$success]<br>");

                            $access = 2;

                            // cs_Error("Your login have timeout. Please login using your userid and password!");
                     } else {

                            // within time

                            $access = 1;

                            // still within the time limit
                            // update datetime for the tMerhcantLogin

                            $success = $db->exec("UPDATE tmerchantlogin SET datetime='$currdatetime' WHERE session='$mysession'");
                     }
              }

              return $access;
       }

       function logout() {
              global $modules, $db;
              global $login_id, $usergroup, $login;

              writeSysLog("Access", "Logout", "login_id[{$login_id}] offline");
              $success = $db->exec("UPDATE tmerchantlogin SET datetime='', session='' WHERE login_id='$login_id'");

              session_destroy();

              return $success;
       }

}
?>

