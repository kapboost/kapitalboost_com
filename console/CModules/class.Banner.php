<?php

class Banner extends Sites {

       public function __construct() {
              global $module_id, $action_id, $db;
              parent::__construct();

              //Check user Access
              if ($module_id > 0 && $this->usergroup && $this->login_id) {
                     if ($action_id > 0) {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     } else {
                            $check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
                            if (!$check) {
                                   die("Invalid operation access spotted, please check with administrator");
                            }
                     }
              } else {
                     die("You don't have permission to access this page, please check with administrator");
              }
       }

       function LoadDefault() {
              //return $this->ListBanner();
       }

       function DeleteBanner($id) {
              $image = $this->db->field("SELECT banner_pathimages FROM tbanner WHERE banner_id=" . $id);
              $file = '../images/banner/' . $image;

              unlink($file);

              $rs = $this->db->exec("DELETE from tbanner WHERE banner_id='$id'");

              if ($rs) {
                     $this->sys_msg['info'][] = "Banner [$id] has been deleted";
                     writeSysLog("Banner", "Delete Banner", "Banner_id[{$id}] deleted");
                     return true;
              } else {
                     $this->sys_msg['error'][] = "Failed to delete Banner, please try again.";
                     return false;
              }
       }

       function Editbanner() {
              $id = (int) $_GET['banner_id'];
              list($detail, $listcount) = $this->db->singlearray("SELECT * from tbanner WHERE banner_id=" . $id);

              $result = $this->LoadBannerForm($detail);

              return $result;
       }

       function LoadBannerForm($data = NULL) {
              $this->smarty->assign('Title', 'Add New Entity');

              if ($data) {
                     $this->smarty->assign($data);
              }

              list($banner_group, ) = $this->db->multiarray("SELECT * FROM tbanner_group ORDER BY banner_group_name ASC");

              $this->smarty->assign('banner_group', $banner_group);
              $this->smarty->assign('banner_status_options', $this->config['banner_status_options']);
              $this->smarty->assign('formtitle', "Banner Form");
              $result = $this->smarty->fetch("CSTPL_Banner_Form.php");
              return $result;
       }

       function ListBanner() {
              list($banner_group, ) = $this->db->multiarray("SELECT * FROM tbanner_group ORDER BY banner_group_name ASC");

              if (isset($_POST['subaction']) == 'DeleteBanner' && is_array($_POST['list_id'])) {
                     foreach ($_POST['list_id'] as $delkey) {
                            $this->DeleteBanner($delkey);
                     }
              }
              //filter
              if (isset($_POST['subaction']) == 'FilterGroup') {
                     $filter = $_POST['banner_group_id'];
                     //$this->FilterGroup($filter);
                     $list = $this->SetPager("SELECT * from tbanner WHERE banner_group_id='" . $filter . "' ORDER BY insert_dt DESC");
                     //print_r ($list);
                     //echo("SELECT * from tbanner WHERE banner_group_id='".$filter."' ORDER BY insert_dt DESC");
              } else {
                     $list = $this->SetPager("SELECT * from tbanner ORDER BY insert_dt DESC");
              }

              if (isset($_POST['subaction']) == 'FilterGroup' && $_POST['banner_group_id'] == 'all_banner') {
                     $list = $this->SetPager("SELECT * from tbanner ORDER BY insert_dt DESC");
              }

              //$list = $this->SetPager("SELECT * from tbanner ORDER BY insert_dt DESC");

              $this->smarty->assign('banner_group_id', $_POST['banner_group_id']);
              $this->smarty->assign('banner_group', $banner_group);
              $this->smarty->assign("action", "ListBanner");
              $this->smarty->assign('list', $list);
              $result = $this->smarty->fetch("CSTPL_Banner_List.php");
              return $result;
       }

       function ValidateBannerForm() {
              $result = "";
              $error = 0;

              if ($_POST['banner_title'] == '') {
                     $this->sys_msg['error'][] = "Banner title cannot be blank";
                     $error = 1;
              }
              if (isset($_POST['banner_status']) == '') {
                     $this->sys_msg['error'][] = "Banner status must be choosen";
                     $error = 1;
              }
              /*
                if ($_POST['banner_url'] == '')
                {
                $this->sys_msg['error'][] = "Banner url cannot be blank";
                $error = 1;
                }
               */

              if ($_FILES['img']['tmp_name'] == '' && $_POST['banner_id'] == '') {
                     $this->sys_msg['error'][] = "Banner image cannot be blank";
                     $error = 1;
              }

              $banner_id = $_POST['banner_id'];
              $sql = "SELECT banner_pathimages FROM tbanner WHERE banner_id='" . $banner_id . "'";
              $banner_pathimages = $this->db->field($sql);

              if (!$error) {
                     $result .= $this->_UpdateBanner($_POST);
              } else {
                     $this->smarty->assign('banner_pathimages', $banner_pathimages);
                     $result .= $this->LoadBannerForm($_POST);
              }

              return $result;
       }

       function _UpdateBanner($data) {
              $uploaddir = "";
              $remote_ip = $_SERVER['REMOTE_ADDR'];

              if ($data['banner_id'] > 0) {
                     $query = "UPDATE tbanner set 
									banner_group_id = '" . formatSQL($data['banner_group_id']) . "',
									banner_title='" . formatSQL($data['banner_title']) . "',
									banner_title2='" . formatSQL($data['banner_title2']) . "',
									banner_url='" . formatSQL($data['banner_url']) . "',
									banner_sort='" . formatSQL($data['banner_sort']) . "',
									banner_status='" . formatSQL($data['banner_status']) . "',
									update_dt=NOW(), 
									remote_ip='" . $remote_ip . "',
									login_id='" . $this->login_id . "'
									WHERE banner_id=" . formatSQL($data['banner_id']);

                     $rs = $this->db->exec($query);

                     $img_file = $_FILES['img'];
                     if ($img_file['tmp_name']) {
                            $fileinfo = image_get_info($img_file['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/banner";
                            echo $uploaddir . "<br>";
                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "banner_" . $insert_id . "." . $ext;

                            @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                            $sql = "UPDATE tbanner SET banner_pathimages='" . formatSQL($image_name) . "' WHERE banner_id = '" . $data['banner_id'] . "'";
                            $rs = $this->db->exec($sql);
                     }

                     /* $img_file = $_FILES['img'];
                       if ($img_file['tmp_name'])
                       {
                       $fileinfo = image_get_info($img_file['tmp_name']);
                       $ext = strtolower($fileinfo['extension']);

                       $uploaddir .= "../assets/images/banner";

                       if (!is_dir($uploaddir))
                       {
                       mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                       }

                       $image_name = "banner_".$data['banner_id'].".".$ext;

                       @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                       $rs = $this->db->exec("UPDATE tbanner SET banner_pathimages='".formatSQL($image_name)."' WHERE banner_id = '".$data['banner_id']."'");
                       }

                       $img_file1 = $_FILES['img1'];
                       if ($img_file1['tmp_name'])
                       {
                       $fileinfo = image_get_info($img_file1['tmp_name']);
                       $ext = strtolower($fileinfo['extension']);

                       $uploaddir .= "../assets/images/banner";

                       if (!is_dir($uploaddir))
                       {
                       mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                       }

                       $image_name = "banner1_".$data['banner_id'].".".$ext;

                       @copy($img_file1['tmp_name'], "{$uploaddir}/{$image_name}");

                       $rs = $this->db->exec("UPDATE tbanner SET banner_pathimages1='".formatSQL($image_name)."' WHERE banner_id = '".$data['banner_id']."'");
                       }

                       $img_file2 = $_FILES['img2'];
                       if ($img_file2['tmp_name'])
                       {
                       $fileinfo = image_get_info($img_file2['tmp_name']);
                       $ext = strtolower($fileinfo['extension']);

                       $uploaddir .= "../assets/images/banner";

                       if (!is_dir($uploaddir))
                       {
                       mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                       }

                       $image_name = "banner2_".$data['banner_id'].".".$ext;

                       @copy($img_file2['tmp_name'], "{$uploaddir}/{$image_name}");

                       $rs = $this->db->exec("UPDATE tbanner SET banner_pathimages2='".formatSQL($image_name)."' WHERE banner_id = '".$data['banner_id']."'");
                       } */

                     if ($rs) {
                            $this->sys_msg['info'][] = "Banner [$data[banner_title]] has been updated";
                            writeSysLog("Banner", "Edit Banner", "Banner ID[{$data['banner_id']}] updated");
                     } else {
                            $this->sys_msg['error'][] = "Failed to update Banner, please try again.";
                     }
              } else {
                     $query = "INSERT INTO tbanner set 
									banner_group_id = '" . formatSQL($data['banner_group_id']) . "',
									banner_title='" . formatSQL($data['banner_title']) . "',
									banner_title2='" . formatSQL($data['banner_title2']) . "',
									banner_url='" . formatSQL($data['banner_url']) . "',
									banner_sort='" . formatSQL($data['banner_sort']) . "',
									banner_status='" . formatSQL($data['banner_status']) . "',
									insert_dt=NOW(), 
									remote_ip='" . $remote_ip . "',
									login_id='" . $this->login_id . "'
									";

                     $rs = $this->db->exec($query);
                     $insert_banner_id = mysql_insert_id();

                     $img_file = $_FILES['img'];
                     if ($img_file['tmp_name']) {
                            $fileinfo = image_get_info($img_file['tmp_name']);
                            $ext = strtolower($fileinfo['extension']);

                            $uploaddir .= "../assets/images/banner";
                            echo $uploaddir . "<br>";
                            if (!is_dir($uploaddir)) {
                                   mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
                            }

                            $image_name = "banner_" . $insert_banner_id . "." . $ext;

                            @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                            $sql = "UPDATE tbanner SET banner_pathimages='" . $image_name . "' WHERE banner_id = '" . $insert_banner_id . "'";
                            $rs = $this->db->exec($sql);
                     }

                     /* $img_file = $_FILES['img'];
                       if ($img_file['tmp_name'])
                       {
                       $fileinfo = image_get_info($img_file['tmp_name']);
                       $ext = strtolower($fileinfo['extension']);

                       $uploaddir .= "../assets/images/banner";

                       if (!is_dir($uploaddir))
                       {
                       mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                       }

                       $image_name = "banner_".$insert_banner_id.".".$ext;

                       @copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");

                       $rs = $this->db->exec("UPDATE tbanner SET banner_pathimages='".formatSQL($image_name)."' WHERE banner_id = '".$insert_banner_id."'");
                       }

                       $img_file1 = $_FILES['img1'];
                       if ($img_file1['tmp_name'])
                       {
                       $fileinfo = image_get_info($img_file1['tmp_name']);
                       $ext = strtolower($fileinfo['extension']);

                       $uploaddir .= "../assets/images/banner";

                       if (!is_dir($uploaddir))
                       {
                       mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                       }

                       $image_name = "banner1_".$insert_banner_id.".".$ext;

                       @copy($img_file1['tmp_name'], "{$uploaddir}/{$image_name}");

                       $rs = $this->db->exec("UPDATE tbanner SET banner_pathimages1='".formatSQL($image_name)."' WHERE banner_id = '".$insert_banner_id."'");
                       }

                       $img_file2 = $_FILES['img'];
                       if ($img_file2['tmp_name'])
                       {
                       $fileinfo = image_get_info($img_file2['tmp_name']);
                       $ext = strtolower($fileinfo['extension']);

                       $uploaddir .= "../assets/images/banner";

                       if (!is_dir($uploaddir))
                       {
                       mkdir($uploaddir, 0755) or die("<b>Error:</b> could not make directory [$uploaddir].");
                       }

                       $image_name = "banner2_".$insert_banner_id.".".$ext;

                       @copy($img_file2['tmp_name'], "{$uploaddir}/{$image_name}");

                       $rs = $this->db->exec("UPDATE tbanner SET banner_pathimages2='".formatSQL($image_name)."' WHERE banner_id = '".$insert_banner_id."'");
                       } */

                     if ($rs) {
                            $this->sys_msg['info'][] = "Banners [$data[banner_title]] has been created";
                            $this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
                            writeSysLog("Banner", "Add Banner", "Banner_id[{$insert_banner_id}] added");
                     } else {
                            $this->sys_msg['error'][] = "Failed to add new Banner, please try again.";
                     }
              }
       }

}

?>
