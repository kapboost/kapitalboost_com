<?php

include_once(DOC_ROOT . 'console/CModules/class.Audit.php');

class Dashboard extends Sites {

       public function __construct() {
              parent::__construct();

              global $db;

              $this->db = $db;
       }

       /* Pop Up */

       function LoadDefault() {
              $now = date('Y-m-d');

              //$to = date('Y-m-d');
              //$from = date('Y-m-d');

              $where = "";

              if ($order_status !== false) {
                     $where.= " WHERE t.status = '0' AND insert_dt >= '{$now} 00:00:00' AND insert_dt <= '{$now} 23:59:59' ";
              }

              list($list, ) = $this->db->multiarray("SELECT t.transaction_id,t.grand_total,t.payment_status,t.payment_method,d.*,s.shopping_cart_id
									FROM ttransaction AS t 
									JOIN tdelivery AS d ON d.cookie_id = t.cookie_id 
									JOIN tshoppingcart AS s ON t.cookie_id = s.cookie_id
									WHERE d.delivery_date_from = '$now'
									AND t.status != '3'
									AND t.status != '9'
									ORDER BY d.delivery_time ASC");

              if ($list) {
                     foreach ($list as $idx => $row) {
                            list($items, ) = $this->db->multiarray("SELECT product_name,product_description,product_quantity FROM tshoppingcart_detail WHERE shopping_cart_id = '$row[shopping_cart_id]'");
                            // echo "SELECT product_name,product_description,product_quantity FROM tshoppingcart_detail WHERE shopping_cart_id = '$row[shopping_cart_id]'";
                            // print_r($items);
                            $list[$idx]['items'] = $items;
                            $list[$idx]['order_status_name'] = $this->config['order_status'][$row['status']];
                            $list[$idx]['payment_status_name'] = $this->config['payment_status'][$row['payment_status']];
                     }
              }
              //print_r($list);die();
              $this->smarty->assign('list', $list);

              list($shiplist, ) = $this->db->multiarray("SELECT t.transaction_id,t.grand_total,t.payment_status,t.payment_method,s.shopping_cart_id,d.*  
									FROM ttransaction AS t 
									JOIN tdelivery AS d ON d.cookie_id = t.cookie_id 
									JOIN tshoppingcart AS s ON t.cookie_id = s.cookie_id
									WHERE d.delivery_date_from = '$now'
									AND t.status = '3'
									ORDER BY d.delivery_time ASC");

              if ($shiplist) {
                     foreach ($shiplist as $idx => $row) {
                            list($items, ) = $this->db->multiarray("SELECT product_name,product_description,product_quantity FROM tshoppingcart_detail WHERE shopping_cart_id = '$row[shopping_cart_id]'");
                            // echo "SELECT product_name,product_description,product_quantity FROM tshoppingcart_detail WHERE shopping_cart_id = '$row[shopping_cart_id]'";
                            $shiplist[$idx]['items'] = $items;
                            $shiplist[$idx]['order_status_name'] = $this->config['order_status'][$row['status']];
                            $shiplist[$idx]['payment_status_name'] = $this->config['payment_status'][$row['payment_status']];
                     }
              }
              //print_r($shiplist);
              //$this->smarty->assign("action",  "ListOrder");
              $this->smarty->assign('shiplist', $shiplist);



              $content = $this->smarty->fetch('CSTPL_Dashboard_Block.php');
              //$content .= $this->LoadLastAccess();
              //$content .= $this->LoadLastLogout();

              return $content;
       }

       /* function ValidateDelivery($data)
         {
         $error_msg = array;

         if ($data['delivery_givenname'] == '')
         {
         $error_msg[] = "Full name must be filled.";
         }

         if ($data['delivery_charge_id'] == '')
         {
         $error_msg[] = "Invalid Address, Please choose address from our autocomplete list";
         }

         if ($data['delivery_street_1'] == '')
         {
         $error_msg[] = "Address must be filled.";
         }

         if ($data['delivery_city'] == '')
         {
         $error_msg[] = "City must be filled.";
         }

         if ($data['delivery_state'] == '')
         {
         $error_msg[] = "State must be filled.";
         }

         if ($data['delivery_country'] == '')
         {
         $error_msg[] = "Country must be filled.";
         }

         if ($data['delivery_postal_code'] == '')
         {
         $error_msg[] = "Postal Code must be filled.";
         }

         /*if ($data['delivery_contact_no'] == '')
         {
         $error_msg[] = "Contact No must be filled.";
         //}

         if ($data['delivery_mobile_no'] == '')
         {
         $error_msg[] = "Mobile No must be filled.";
         }

         /*if ($data['delivery_email'] == '')
         {
         $error_msg[] = "Email must be filled.";
         }

         if ($data['delivery_msg_to_1'] == '')
         {
         $error_msg[] = "Message To must be filled.";
         }

         if ($data['delivery_msg_1'] == '')
         {
         $error_msg[] = "Message must be filled.";
         }

         if ($data['show_msg_from'] != 1 && $data['delivery_msg_from_1'] == '')
         {
         $error_msg[] = "Who is it from must be filled.";
         }

         if ($data['delivery_date_from'] == '')
         {
         $error_msg[] = "Please select your preferred delivery date.";
         }

         if ($data['delivery_time'] == '')
         {
         $error_msg[] = "Please select your preferred delivery time.";
         }

         if ($data['delivery_location'] == '')
         {
         $error_msg[] = "Please select your type of location.";
         }

         if ($data['delivery_date_from'] != '10/05/2015' && $data['delivery_time'] == '3')
         {
         $error_msg[] = "The night time delivery 6pm - 8pm only available for 10th May 2015";
         }

         if (!$error)
         {
         $result .= $this->UpdateDelivery($_POST);
         }else
         {
         $result .= $this->DeliveryDetail($_POST);
         }

         } */

       /* function UpdateDelivery()
         {
         if($data['delivery_id'>0])
         {
         $sql = "UPDATE tdelivery SET
         delivery_addr_id='".mysql_escape_string($data['delivery_addr_id'])."',
         delivery_type='".mysql_escape_string($data['delivery_type'])."',
         delivery_email='".mysql_escape_string($data['billing_email'])."',
         delivery_title='".mysql_escape_string($data['billing_title'])."',
         delivery_givenname='".mysql_escape_string($data['billing_givenname'])."',
         delivery_unit='".mysql_escape_string($data['billing_unit'])."',
         delivery_street_1='".mysql_escape_string($data['billing_street_1'])."',
         delivery_city='".mysql_escape_string($data['billing_city'])."',
         delivery_state='".mysql_escape_string($data['billing_state'])."',
         delivery_country='".mysql_escape_string($data['billing_country'])."',
         delivery_postal_code='".mysql_escape_string($data['billing_postal_code'])."',
         delivery_contact_no='".mysql_escape_string($data['billing_contact_no'])."',
         delivery_mobile_no='".mysql_escape_string($data['billing_mobile_no'])."',
         show_msg_from='".mysql_escape_string($data['show_msg_from'])."',
         delivery_msg_from_1='".mysql_escape_string($data['delivery_msg_from_1'])."',
         delivery_msg_to_1='".mysql_escape_string($data['delivery_msg_to_1'])."',
         delivery_msg_1='".mysql_escape_string($data['delivery_msg_1'])."',
         delivery_date_from='".mysql_escape_string($data['delivery_date_from'])."',
         delivery_time='".mysql_escape_string($data['delivery_time'])."',
         delivery_location='".mysql_escape_string($data['delivery_location'])."',
         delivery_company='".mysql_escape_string($data['delivery_company'])."',
         delivery_express='".mysql_escape_string($data['delivery_express'])."',
         delivery_charge_id='".mysql_escape_string($data['delivery_charge_id'])."',
         self_collect='".mysql_escape_string($data['self_collect'])."',
         insert_dt=NOW(),
         remote_ip='".$remote_ip."'
         WHERE
         delivery_id='".$data['delivery_id']."' AND
         cookie_id='".mysql_escape_string($this->cookie_id)."'
         ";
         }

         if ($rs)
         {
         $this->sys_msg['info'][] = "Delivery [$data[delivery_id]] has been updated";
         writeSysLog("Delivery", "Edit Delivery", "Delivery_id[{$data['delivery_id']}] updated");
         }
         else
         {
         $this->sys_msg['error'][] = "Failed to update Delivery, please try again.";
         }


         $result = $this->db->exec($sql);
         }

         /*function DeliveryStatus($data)
         {
         if($data)
         {
         $this->smarty->assign($data);
         }

         //$this->smarty->assign('deliver_detail', $deliver_detail);
         //$this->smarty->assign('action', "ValidateDeliveryStatus");
         $result = $this->smarty->fetch("CSTPL_Dashboard_Status.php");
         return $result;
         }

         /*function ValidateDeliveryStatus($data)
         {
         $error_msg = array;

         if ($data['delivery_street_1'] == '')
         {
         $error_msg[] = "Address must be filled.";
         }

         if ($data['delivery_date_from'] == '')
         {
         $error_msg[] = "Please select your preferred delivery date.";
         }

         if ($data['delivery_time'] == '')
         {
         $error_msg[] = "Please select your preferred delivery time.";
         }

         if ($data['delivery_date_from'] != '10/05/2015' && $data['delivery_time'] == '3')
         {
         $error_msg[] = "The night time delivery 6pm - 8pm only available for 10th May 2015";
         }

         if ($data['status'] == '')
         {
         $error_msg[] = "Please select status.";
         }

         if (!$error)
         {
         $result .= $this->UpdateDeliveryStatus($_POST);
         }else
         {
         $result .= $this->DeliveryStatus($_POST);
         }
         }


         /*Pop Up */

       function LoadDailySales() {
              $now = date('Y-m-d');

              $Audit = new Audit();

              $graph = $Audit->LoadDailySales(550, 325);

              $reply['status'] = true;
              $reply['graph'] = $graph;

              echo json_encode($reply);

              exit(0);
       }

       function LoadDailyTotal() {

              $now = date('Y-m-d');

              $Audit = new Audit();

              $graph = $Audit->LoadDailyTotal(550, 325);

              $reply['status'] = true;
              $reply['graph'] = $graph;

              echo json_encode($reply);

              exit(0);
       }

       function LoadMonthlySales() {

              $now = date('Y-m-d');

              $Audit = new Audit();

              $graph = $Audit->LoadMonthlySales(550, 325);

              $reply['status'] = true;
              $reply['graph'] = $graph;

              echo json_encode($reply);

              exit(0);
       }

       function LoadOrderStatus() {

              $now = date('Y-m-d');

              $Audit = new Audit();

              $graph = $Audit->LoadOrderStatus(550, 325);

              $reply['status'] = true;
              $reply['graph'] = $graph;

              echo json_encode($reply);

              exit(0);
       }

       function LoadWeeklyProductSales() {

              $now = date('Y-m-d');

              $Audit = new Audit();

              $graph = $Audit->LoadWeeklyProductSales(550, 325);

              $reply['status'] = true;
              $reply['graph'] = $graph;

              echo json_encode($reply);

              exit(0);
       }

       function LoadLastAccess($ajax = 0) {
              global $login_id;
              list($list, $listcount) = $this->db->multiarray("select * FROM tsystemlog WHERE login_id='$login_id' and log_action='Login' ORDER BY insert_dt DESC LIMIT 5");

              if ($listcount > 0) {
                     while (list($x) = each($list)) {
                            $list[$x]['display'] = $list[$x]['insert_dt'];
                     }
              }

              $this->smarty->assign('list', $list);
              $block_inner .= $this->smarty->fetch('CSTPL_Dashboard_Block_Inner.php');
              if ($ajax)
                     return $block_inner;

              $content = $this->_buildBlockContent('Last Access History', 'LoadLastAccess', $block_inner);


              return $content;
       }

       function LoadLastLogout($ajax = 0) {
              global $login_id;
              list($list, $listcount) = $this->db->multiarray("select * FROM tsystemlog WHERE login_id='$login_id' and log_action='Logout' ORDER BY insert_dt DESC LIMIT 5");

              if ($listcount > 0) {
                     while (list($x) = each($list)) {
                            $list[$x]['display'] = $list[$x]['insert_dt'];
                     }
              }
              $this->smarty->assign('list', $list);
              $block_inner .= $this->smarty->fetch('CSTPL_Dashboard_Block_Inner.php');
              if ($ajax)
                     return $block_inner;

              $content = $this->_buildBlockContent('Last Logout History', 'LoadLastLogout', $block_inner);


              return $content;
       }

       function _buildBlockContent($title, $appName, $block_inner, $time = 0) {
              $this->smarty->assign('block_inner', $block_inner);
              $this->smarty->assign('Title', $title);
              $this->smarty->assign('BlockID', $appName);

              $content = $this->smarty->fetch('CSTPL_Dashboard_Block.php');
              if ($time > 0)
                     $content .= $this->_loadAjax($appName, $time);

              return $content;
       }

       function _loadAjax($app, $time) {

              global $app_link;
              $result = "       
<script language=\"JavaScript\">
setTimeout('{$app}_Reload()', '{$time}');
function {$app}_Reload(){
   
    $('#{$app}-inner').html('<img src=\"images/loadingAnimation.gif\" />');
    $('#{$app}-inner').load(\"{$app_link}&ajax=1&ajaxMod=Dashboard&ajaxApp={$app}\");
    
    setTimeout('{$app}_Reload()', '{$time}');
   
}
</script>
";

              return $result;
       }

       function DeliveryToday() {
              $get_delivery_id = $_GET['delivery_id'];

              if ($_POST) {
                     $sql2 = "UPDATE tdelivery SET
			self_collect = '" . $_POST['self_collect'] . "',
			delivery_givenname = '" . $_POST['delivery_givenname'] . "',
			delivery_company = '" . $_POST['delivery_company'] . "',
			delivery_location = '" . $_POST['delivery_location'] . "',
			delivery_unit = '" . $_POST['delivery_unit'] . "',
			delivery_street_1 = '" . $_POST['delivery_street_1'] . "',
			delivery_city = '" . $_POST['delivery_city'] . "',
			delivery_state = '" . $_POST['delivery_state'] . "',
			delivery_country = '" . $_POST['delivery_country'] . "',
			delivery_postal_code = '" . $_POST['delivery_postal_code'] . "',
			delivery_email = '" . $_POST['delivery_email'] . "',
			delivery_mobile_no = '" . $_POST['delivery_mobile_no'] . "',
			delivery_contact_no = '" . $_POST['delivery_contact_no'] . "',
			delivery_time = '" . $_POST['delivery_time'] . "',
			delivery_express = '" . $_POST['delivery_express'] . "'
			WHERE delivery_id = '" . $get_delivery_id . "'
			";
                     // print_r($sql2);
                     $update = $this->db->exec($sql2);
              }

              $sql = "SELECT * FROM tdelivery WHERE delivery_id='" . $get_delivery_id . "'";
              list($deliverytoday) = $this->db->singlearray($sql);
              $this->smarty->assign("deliverytoday", $deliverytoday);
              $content = $this->smarty->fetch("CSTPL_Dashboard_Pop_Up.php");
              print $content;
              // print_r($sql);
              exit;
       }

       function DeliveryTodayStatus() {
              $transaction_id = $_GET['transaction_id'];
              $sql2 = "SELECT * FROM ttransaction WHERE transaction_id='" . $transaction_id . "'";
              list($deliverytodaypoint) = $this->db->singlearray($sql2);
              $get_member_id = $deliverytodaypoint[member_id];
              $get_subtotal = $deliverytodaypoint[subtotal];
              // print_r ($get_subtotal);
              if ($_POST) {
                     $status = "UPDATE ttransaction SET status='" . $_POST['status'] . "' WHERE transaction_id='" . $transaction_id . "'";
                     $this->db->exec($status);
              }

              if ($deliveryodaypoint['status'] != $_POST['status']) {

                     if ($_POST['status'] == "3") {
                            $point = "INSERT INTO tpoint SET
						transaction_id='" . $transaction_id . "',
						member_id='" . $get_member_id . "',
						point='" . $get_subtotal . "',
						description='Shipped',
						insert_dt=NOW()";

                            $this->db->exec($point);
                            // print_r ($point);
                     } else {
                            if ($deliverytodaypoint['status'] == "3") {
                                   if ($_POST['status'] == 0) {
                                          $order_status = 'New';
                                   } else if ($_POST['status'] == 1) {
                                          $order_status = 'In Process';
                                   } else if ($_POST['status'] == 3) {
                                          $order_status = 'Shipped';
                                   } else if ($_POST['status'] == 9) {
                                          $order_status = 'Cancel';
                                   }
                                   $point = "INSERT INTO tpoint SET
							transaction_id='" . $transaction_id . "',
							member_id='" . $get_member_id . "',
							point=-'" . $get_subtotal . "',
							description='" . $order_status . "',
							insert_dt=NOW()";

                                   $this->db->exec($point);
                                   // print_r ($point);
                            }
                     }
              }



              $sql = "SELECT * FROM ttransaction WHERE transaction_id='" . $transaction_id . "'";
              list($deliverytodaystatus) = $this->db->singlearray($sql);
              $this->smarty->assign("deliverytodaystatus", $deliverytodaystatus);
              $this->smarty->assign("deliverytodaypoint", $deliverytodaypoint);
              $content = $this->smarty->fetch("CSTPL_Dashboard_Pop_Up_Update.php");
              print $content;
              // print_r($sql);
              exit;
       }

}

?>
