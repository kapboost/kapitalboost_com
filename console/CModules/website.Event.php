<?php
class Event extends Sites{
	public function __construct(){
		global $module_id, $action_id, $db;
		parent::__construct();
		
		if ($module_id > 0 && $this->usergroup && $this->login_id){
			if ( $action_id > 0 ){
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id' AND action_id ='$action_id'");
				if (!$check){
					die("Invalid operation access spotted, please check with administrator");
				}
			}
			else{
				$check = $this->db->field("select count(*) FROM tconsoleusergroup_map WHERE usergroup_id='{$this->usergroup}' AND module_id='$module_id'");
				if (!$check)
				{
					die("Invalid operation access spotted, please check with administrator");
				}
			}
		}
		else{
			die("You don't have permission to access this page, please check with administrator");
		}
	}
	
	function LoadDataForm($data=NULL){
		if($data){
			$this->smarty->assign($data);
		}
		
		$content = $this->smarty->fetch('CSTPL_Event_Form.php');
		return $content;
	}
	
	function LoadDataList(){
		if(isset($_POST['subaction']) == 'DeleteData' && is_array($_POST['list_id'])){
			foreach($_POST['list_id'] as $delkey){
				$this->DeleteData($delkey);
			}
		}
		
		//Search
		if (isset($_POST['keyword']) && !empty($_POST['keyword']))
		{
			$keyword = $_POST['keyword'];
			$filter .= "AND event_name LIKE '%".$keyword."%'";
			
			$this->smarty->assign('keyword', $keyword);
		}
		$sql = "SELECT event_id, event_name, event_description, release_date, expiry_date, location, speakers, fee, ticket_info, event_image, insert_dt, update_dt FROM tevent WHERE event_id > 0 {$filter} ORDER BY event_id DESC";
		list($list, $list_count) = $this->db->multiarray($sql);
		
		$this->smarty->assign('list', $list);
		
		$content = $this->smarty->fetch('CSTPL_Event_List.php');
		return $content;
	}
	
	function EditData(){
		$id = (int)$_GET['event_id'];
		
		$sql = "SELECT * FROM tevent WHERE event_id='".$id."'";
		list($detail, $detail_count) = $this->db->singlearray($sql);
		
		$result = $this->LoadDataForm($detail);
		return $result;
	}
	
	function ValidateDataForm(){
		$result = "";
		$error = 0;
		
		if($_POST['event_name'] == ''){
			$this->sys_msg['error'][] = "Event Name cannot be blank.";
			$error = 1;
		}
		if($_POST['event_description'] == ''){
			$this->sys_msg['error'][] = "Event Description cannot be blank.";
			$error = 1;
		}
		if($_POST['release_date'] == ''){
			$this->sys_msg['error'][] = "Release Date cannot be blank.";
			$error = 1;
		}
		if($_POST['expiry_date'] == ''){
			$this->sys_msg['error'][] = "Expiry Date cannot be blank.";
			$error = 1;
		}
		if($_POST['location'] == ''){
			$this->sys_msg['error'][] = "Location cannot be blank.";
			$error = 1;
		}
		if($_POST['speakers'] == ''){
			$this->sys_msg['error'][] = "Speakers cannot be blank.";
			$error = 1;
		}
		if($_POST['fee'] == ''){
			$this->sys_msg['error'][] = "Fee cannot be blank.";
			$error = 1;
		}
		if($_POST['ticket_info'] == ''){
			$this->sys_msg['ticket_info'][] = "Ticket Info cannot be blank.";
			$error = 1;
		}
		
		if(!$error){		
			$result = $this->_UpdateData($_POST);
		}
		else{
		    $event_id = $_POST['event_id'];
            $sql = "SELECT event_image FROM tevent WHERE event_id='".$event_id."'";
            $event_image = $this->db->field($sql);
		
			$this->smarty->assign('event_image', $event_image);
			$result = $this->LoadDataForm($_POST);
		}
		
		return $result;
	}
	
	function _UpdateData($data){
		$uploaddir = "";
		$remote_ip = $_SERVER['REMOTE_ADDR'];
		
		$release_date = $data['release_date'];
		$expiry_date = $data['expiry_date'];

		$date1 = explode("/", $release_date);
		$tahun1=$date1[2];
		$bulan1=$date1[1];
		$tanggal1=$date1[0];
		$release_date=$tahun1."-".$bulan1."-".$tanggal1;
					
		$date2 = explode("/", $expiry_date);
		$tahun2=$date2[2];
		$bulan2=$date2[1];
		$tanggal2=$date2[0];
		$expiry_date=$tahun2."-".$bulan2."-".$tanggal2;
		
		if($data['event_id'] > 0){
			$sql = "UPDATE tevent SET
				event_name = '".mysql_real_escape_string($data['event_name'])."',
				event_description = '".mysql_real_escape_string($data['event_description'])."',
				release_date = '".mysql_real_escape_string($release_date)."',
				expiry_date = '".mysql_real_escape_string($expiry_date)."',
				location = '".mysql_real_escape_string($data['location'])."',
				speakers = '".mysql_real_escape_string($data['speakers'])."',
				fee = '".mysql_real_escape_string($data['fee'])."',
				ticket_info = '".mysql_real_escape_string($data['ticket_info'])."',
				event_image = '".mysql_real_escape_string($data['event_image'])."',
				update_dt = NOW(),
				remote_ip = '".$remote_ip."'
				WHERE event_id = '".$data['event_id']."'
			";
			
			$rs = $this->db->exec($sql);
			
			$img_file = $_FILES['img'];
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/event";
				
				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "event_".$data['event_id'].".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tevent SET event_image = '".$image_name."' WHERE event_id = '".$data['event_id']."'";

				$rs = $this->db->exec($sql);
			}
			
			if($rs){
				$this->sys_msg['info'][] = "event [$data[event_id]] has been updated.";
				
				writeSysLog("Event", "Edit Event", "Event ID[{$data['parent_id']}] updated");
			}
			else{
				$this->sys_msg['error'][] = "Failed to update Event, please try again.";
			}
		}
		else{
			$sql = "INSERT INTO tevent SET
				event_name = '".mysql_real_escape_string($data['event_name'])."',
				event_description = '".mysql_real_escape_string($data['event_description'])."',
				release_date = '".mysql_real_escape_string($release_date)."',
				expiry_date = '".mysql_real_escape_string($expiry_date)."',
				location = '".mysql_real_escape_string($data['location'])."',
				speakers = '".mysql_real_escape_string($data['speakers'])."',
				fee = '".mysql_real_escape_string($data['fee'])."',
				ticket_info = '".mysql_real_escape_string($data['ticket_info'])."',
				event_image = '".mysql_real_escape_string($data['event_image'])."',
				insert_dt = NOW(),
				remote_ip = '".$remote_ip."'
			";
			
			$rs = $this->db->exec($sql);
			$insert_id = mysql_insert_id();
			
			$img_file = $_FILES['img'];
			
			if($img_file['tmp_name']){
				$fileinfo = image_get_info($img_file['tmp_name']);
				$ext = strtolower($fileinfo['extension']);
				
				$uploaddir .= "../assets/images/event";

				if(!is_dir($uploaddir)){
					mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
				}
				
				$image_name = "event_".$insert_id.".".$ext;
				
				@copy($img_file['tmp_name'], "{$uploaddir}/{$image_name}");
				
				$sql = "UPDATE tevent SET event_image='".$image_name."' WHERE event_id = '".$insert_id."'";
				$rs = $this->db->exec($sql);
			}
			
			if($rs){
				$this->sys_msg['info'][] = "Event [$data[event_name]] has been created.";
				$this->sys_msg['warning'][] = "To prevent duplicate new record entry, please do not refresh the browser.";
				writeSysLog("Event", "Add Event", "Event_id[{$insert_id}] added");
			}
			else{
				$this->sys_msg['error'][] = "Failed to add new Event, please try again.";
			}
		}
	}
	
	function DeleteData($id){
		$sql = "SELECT event_image FROM tevent WHERE event_id='".$id."'";
		$image = $this->db->field($sql);
		$file = '../images/event/'.$image;
		unlink($file);
		
		$sql = "DELETE from tevent WHERE event_id='".$id."'";
		$rs = $this->db->exec($sql);
		if($rs){
			$this->sys_msg['info'][] = "Event [$id] has been deleted.";
			writeSysLog("Event", "Delete Event", "Event_id[{$id}] deleted");
			
			return true;
		}
		else{
			$this->sys_msg['error'][] = "Failed to delete Event, please try again.";
			
			return false;
		}
	}
}
?>