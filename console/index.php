<?php

date_default_timezone_set('Asia/Jakarta'); // set timezone in php
session_start();
$root = realpath(dirname(__FILE__) . '/../') . '/';
define('DOC_ROOT', $root);
include(DOC_ROOT . 'console/incs/common.php');

include($root . "console/CModules/class.Sites.php");

//DEFINE Variable first, decrease notice
$system_id = 0;
$module_id = 0;
$action_id = 0;
$action = "";
$session = "";
$ajax = 0;
$cf1_usergroup = "";
$mod_content = "";

echo 'hello';

$Sites = new Sites;

$variable_list = get_variables();   // get list of variables from POST and GET; same name, return POST value
while (list($key, $value) = each($variable_list)) {
       $$key = $value;
}

if (!isset($variable_list['session'])) {
       // create a random 32 char long session value
       $sessionvalue = md5(uniqid(rand()));
       //echo "<meta http-equiv=\"refresh\" content=\"0;URL=index.php?session=$sessionvalue&t=$time\">"; // Redirect browser
       $variable_list['session'] = $sessionvalue;
}

$login_tpl = 0; //0 = not used login template, 1 = use login template
$login_error = 0; //0 = not used error template, 1 = use error template


if (isset($variable_list['session'])) {
       $session = $variable_list['session'];

       if (isset($variable_list['cf1_name'])) {
              $loginok = $spine->verifylogin($variable_list['cf1_name'], $variable_list['cf2_password'], $variable_list['session']);
       } else {
              if ($loginok = $spine->verifysession($variable_list['session'])) {
                     $cf1_name = $db->field("SELECT console_user FROM tmerchantlogin WHERE session='" . $variable_list['session'] . "'");
                     $cf1_usergroup = $db->field("SELECT tconsoleusergroup.name FROM tmerchantlogin, tconsoleusergroup WHERE tmerchantlogin.usergroup_id =  tconsoleusergroup.usergroup_id AND session='" . $variable_list['session'] . "'");
              }
       }

       if (isset($variable_list['logout'])) {
              $success = $spine->logout();
              if ($success) {
                     $login_err_msg = "You have logout successfully!";
              } else {
                     $login_err_msg = "You are not logout.";
              }

              $login_error = 1;
       } else if ($loginok == 1) {
              #build system
              $systems = $spine->buildsystem();

              /*
                foreach( $systems as $val=>$key )
                {
                #each system grab all modules
                $systems[$val]['list_modules'] = $spine->buildmodule($key['systemid']);
                }
               */


              $modules = array();
              if ($system_id > 0) {
                     $modules = $spine->buildmodule($system_id);
                     foreach ($modules as $val => $key) {
                            $modules[$val]['list_apps'] = $spine->buildapp($system_id, $modules[$val]['moduleid']);  //this retrieves ALL apps for ALL modules...
                     }
              }

              if ($module_id > 0) {
                     $module_name = $db->field("SELECT name FROM tconsolemodule WHERE module_id = " . $module_id);
              }

              $smarty->assign("Title", PROJECT_NAME);
              $smarty->assign("session", $session);
              $smarty->assign("login", $cf1_name);
              $smarty->assign("system_id", $system_id);
              $smarty->assign("module_id", $module_id);
              $smarty->assign("action_id", $action_id);
              $smarty->assign("action", $action);
              $smarty->assign("list_menu", $systems);
              $smarty->assign("list_modules", $modules);
              //$smarty->assign("list_application",$apps);

              if (isset($action_id)) {
                     $smarty->assign("application_selected", "?session=$session&system_id=$system_id&module_id=$module_id&action_id=$action_id&action=$action");
              } else {
                     $smarty->assign("application_selected", "#");
              }



              $app_link = "index.php?session=$session&system_id=$system_id&module_id=$module_id&action=$action";
              $mod_link = "index.php?session=$session&system_id=$system_id&module_id=$module_id";
              $config['app_link'] = $app_link;
              $config['mod_link'] = $mod_link;
              $smarty->assign("mod_link", $mod_link);
              $smarty->assign("app_link", $app_link);
              $module_id = (int) $module_id;

              if ($ajax) {
                     $ajaxMod = $DVSecurity->sanitize_filename($ajaxMod);
                     $mod_file = $root . "console/CModules/class.{$ajaxMod}.php";
                     if (file_exists($mod_file) && is_file($mod_file)) {

                            require ($mod_file);

                            $obj = new $ajaxMod;
                            $mod_content = $obj->$ajaxApp($ajax);

                            print $mod_content;
                            return;
                     }
              }

              if ($system_id > 0 && $module_id > 0) {

                     $mod_file = $root . "console/CModules/" . $db->field("SELECT filename FROM tconsolemodule WHERE module_id = " . $module_id);
                     if (file_exists($mod_file) && is_file($mod_file)) {
                            require ($mod_file);

                            $module_name = $db->field("SELECT name FROM tconsolemodule WHERE module_id = " . $module_id);

                            $selected_module = $db->field("SELECT classname FROM tconsolemodule WHERE module_id = " . $module_id);

                            eval("\$obj = new $selected_module;");

                            $pos = strpos($action, "_");
                            if ($action == '')
                                   $action = 'LoadDefault';

                            if (method_exists($obj, $action)) {
                                   $mod_content .= $obj->$action();
                            } else {
                                   $mod_content .= "Method does not exists <strong>$selected_module -> $action</strong>";
                            }

                            if ($ajax) {
                                   print $mod_content;
                                   exit;
                            }
                     } else {
                            $mod_content = "<strong>$mod_file</strong> class file not found";
                     }
              } else {
                     //===============================================================
                     //Load Dashboard Class Object
                     //===============================================================
                     $module_name = "Dashboard";
                     $mod_file = $root . "console/CModules/class.Dashboard.php";
                     if (file_exists($mod_file) && is_file($mod_file)) {

                            require ($mod_file);

                            $board = new Dashboard;
                            if (empty($action)) {
                                   $mod_content = $board->LoadDefault();
                            } elseif ($action == "login") {
                                   $mod_content = $board->LoadDefault();
                            } else {
                                   $mode_content = $board->$action();
                                   if ($ajax) {
                                          echo $mod_content;
                                   }
                            }
                            //$board = new Dashboard;
                            //$mod_content = $board->LoadDefault();
                            //$mod_content = "";
                     }
                     //===============================================================
                     //END Load Dashboard Class Object
                     //===============================================================
              }
       } else if ($loginok == 2) {
              $login_err_msg = "Your login have timeout. Please login using your userid and password!";
              $login_error = 1;
       } else if ($loginok == 3) {
              //$login_err_msg = "Someone is using your account or you have not logged out properly. Please wait for your login to timeout.";
              //$login_error = 1;
              $login_tpl = 1;
       } else if ($loginok == 4) {
              $login_err_msg = "Your login <b>User ID</b> or <b>Password</b> is not correct, Please try again.";
              $login_error = 1;
       } else if ($loginok == 5) {
              $login_err_msg = "Your account has been disabled. Please email the administator <a href=\"mailto:helpdesk@dv9.com\">helpdesk@dv9.com</a>";
              $login_error = 1;
       } else {
              $login_tpl = 1;
       }
}

if (!$ajax) {
       $smarty->assign("Title", PROJECT_NAME);
       $smarty->assign("session", $session);

       if ($login_tpl == 1) {
              $smarty->display("CSTPL_Login.php");
       } else if ($login_error == 1) {
              $smarty->assign('message_error', $login_err_msg);
              $smarty->display("CSTPL_Error.php");
       } else {
              $sql = "SELECT * FROM tmember ORDER BY member_id";
              list($list) = $db->multiarray($sql);

              $sql2 = "SELECT * FROM tinv ORDER BY id";
              list($inv) = $db->multiarray($sql2);

              $sql3 = "SELECT * FROM tcampaign ORDER BY campaign_id";
              list($cmp) = $db->multiarray($sql3);

              $sql4 = "SELECT DATEDIFF(clossing_date,NOW()) AS days_left,DATEDIFF(clossing_date,release_date) AS duration FROM tcampaign WHERE campaign_id";
              list($ongoing) = $db->multiarray($sql4);

              $smarty->assign('list', $list);
              $smarty->assign('inv', $inv);
              $smarty->assign('cmp', $cmp);
              $smarty->assign('ongoing', $ongoing);
              $smarty->assign("login", $cf1_name);
              $smarty->assign("jumlah", $jumlah);
              $smarty->assign("usergroup_name", $cf1_usergroup);
              $smarty->assign("system_id", $system_id);
              $smarty->assign("module_id", $module_id);
              $smarty->assign("action_id", $action_id);
              $smarty->assign("action", $action);
              $smarty->assign("list_menu", $systems);
              //$smarty->assign("list_application",$apps);
              $smarty->assign('sys_msg', processSysMsg());
              $smarty->assign('mod_content', $mod_content);
              $smarty->assign('module_name', $module_name);
              $smarty->display("CSTPL_Index.php");
       }
}

?>

