<?php

require_once(DOC_ROOT."Classes/ftp_class.php");


function ftp_connect_server($ip, $login, $password){
    global $ftp, $config;
    $error = TRUE;
    if(!$ftp->SetServer($ip)) {
        $ftp->quit();
        $error = FALSE;
    }

    if (!$ftp->connect()) {
        $error = FALSE;
    }
    if (!$ftp->login($login, $password)) {
        $ftp->quit();
        $error = FALSE;
    }

    if(!$ftp->SetType(FTP_BINARY)) $error = FALSE;
    #if(!$ftp->Passive(FALSE)) echo "Passive FAILS!\n";
    return $error;
}

function ftp_putFile($localfile, $remotefile){
    global $ftp;

    if(FALSE !== $ftp->put($localfile, $remotefile))
        $error = TRUE;
    else {
        $ftp->quit();
        $error = FALSE;
    }
    return $error;
}

function ftp_getFile($remotefile, $localfile){
    global $ftp;
    $error = FALSE;
    if(FALSE !== $ftp->get($remotefile, $localfile))
        $error = TRUE;
    else {
        $ftp->quit();
        $error = FALSE;
    }

    return $error;
}

function ftp_quit_server(){
    global $ftp;
    $ftp->quit();
}

function ftp_check_filesize($localfile, $remotefile){
    global $ftp;
    $error = FALSE;

    $remote_filesize = $ftp->filesize($remotefile);
    $local_filesize  = filesize($localfile);
    settype($remote_filesize, "integer");
    settype($local_filesize, "integer");

    if ($remote_filesize == 0 || $local_filesize == 0){
        return $error;
    }

    if ($remote_filesize == $local_filesize){
        $error = TRUE;
    }

    return $error;
}

?>
