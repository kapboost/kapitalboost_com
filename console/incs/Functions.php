<?php

function get_variables() {

       $variable_array = array();

       while (list($key, $value) = each($_GET)) {

              $variable_array[$key] = $value;
       }

       while (list($key, $value) = each($_POST)) {

              $variable_array[$key] = $value;
       }

       return $variable_array;
}

function get_upload_variables() {

       $variable_array = array();


       while (list($file, $array) = each($_FILES)) {

              $variable_array["$file"] = $array;
       }


       return $variable_array;
}

// check if email address is valid
function validate_email($val) {
       if ($val != "") {
              $pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
              if (preg_match($pattern, $val)) {
                     return true;
              } else {
                     return false;
              }
       } else {
              return false;
       }
}

function DateAdd($interval, $number, $date) {

       $date_time_array = getdate($date);

       $hours = $date_time_array["hours"];
       $minutes = $date_time_array["minutes"];
       $seconds = $date_time_array["seconds"];
       $month = $date_time_array["mon"];
       $day = $date_time_array["mday"];
       $year = $date_time_array["year"];

       switch ($interval) {

              case "yyyy":
                     $year +=$number;
                     break;
              case "q":
                     $year +=($number * 3);
                     break;
              case "m":
                     $month +=$number;
                     break;
              case "y":
              case "d":
              case "w":
                     $day+=$number;
                     break;
              case "ww":
                     $day+=($number * 7);
                     break;
              case "h":
                     $hours+=$number;
                     break;
              case "n":
                     $minutes+=$number;
                     break;
              case "s":
                     $seconds+=$number;
                     break;
       }
       $timestamp = mktime($hours, $minutes, $seconds, $month, $day, $year);
       return $timestamp;
}

function DateDifferent($interval, $date1, $date2) {

       // get the number of seconds between the two dates
       $timedifference = $date2 - $date1;

       switch ($interval) {
              case "w":
                     $retval = bcdiv($timedifference, 604800);
                     break;
              case "d":
                     $retval = bcdiv($timedifference, 86400);
                     break;
              case "h":
                     $retval = bcdiv($timedifference, 3600);
                     break;
              case "n":
                     $retval = bcdiv($timedifference, 60);
                     break;
              case "s":
                     $retval = $timedifference;
                     break;
       }
       return $retval;
}

function cs_processtext($inputvar) {
       $inputvar = trim($inputvar);

       $finish = str_replace("'", "\'", $inputvar);
       $finish = str_replace("<", "&#60", $finish);
       $finish = str_replace(">", "&#62", $finish);

       // mysql_escape_string() does not escape % and _.
//  $finish = mysql_escape_string($inputvar);
       return $finish;
}

function upload_file($uploaddir, $UploadFile, $Filename) {
       /*
         WRITTEN BY  : FOO MING CHERN
         $uploaddir  : FULL PATH NAME
         $UploadFile : NAME OF THE FORM ELEMENT
         $Filename   : PHYSICAL NAME OF THE FILE STORED IN THE DIRECTORY
        */

       echo("this is entered !!!!");

       require_once("../Classes/upload.class.php");

       // ============== UPLOAD PDF  ============== \\
       $upload = new UPLOAD();

       if ($upload->save($uploaddir . "/" . $Filename, $UploadFile)) {
              return 1;
              echo("uploaded");
       } else {
              return 0;
              echo("NOT uploaded");
       }//end else
}

//end upload function

function sql_format($str) {
       $str = preg_replace("/'/", "\'", $str);
       return $str;
}

function get_content($file) {
       $fd = fopen($file, "r");
       $contents = fread($fd, filesize($file));
       fclose($fd);
       return $contents;
}

function printArray($ar) {

       print "\n\n<pre>\n" . print_r($ar, true) . "\n</pre>\n\n";
}

function formatPOST($post) {
       foreach ((array) $post as $key => $val) {
              if (!is_array($val)) {
                     $post[$key] = stripslashes($val);
              }
       }

       return $post;
}

function formatSQL($str) {
       #return $str;
       //return mysql_escape_string($str);
       return $str;
}

function getOptions($tbname, $pkid, $pkval) {
       global $db;

       list($list, $listcount) = $db->multiarray("select $pkid, $pkval FROM $tbname ORDER BY sort_order ASC, $pkval ASC");

       $result = array();

       $result[0] = '--- Select ---';
       foreach ($list as $row) {
              $result[$row[$pkid]] = $row[$pkval];
       }

       return $result;
}

function read_csv($csv_file) {
       $fp = fopen("$csv_file", "r");

       $rownum = 0;

       while ($data = fgetcsv($fp, 2000, ",")) {
              $num = count($data);

              if ($num < 2) {
                     continue;
              }
              for ($c = 0; $c < $num; $c++) {
                     $myarray[$rownum][$c] = $data[$c];
              }
              $rownum++;
       }

       return $myarray;
}

function sendEmail($data) {
       global $root, $db;

       include_once($root . 'Classes/class.phpmailer.php');

       $from_name = ($data['from_name']) ? $data['from_name'] : EMAIL_FROM_NAME;
       $from_email = ($data['from_email']) ? $data['from_email'] : EMAIL_FROM;

       $mailer = new PHPMailer();
       $mailer->IsSMTP();
       $mailer->Host = SMTP_HOST;
       $mailer->SMTPAuth = true;
       $mailer->IsHTML($data['html']);

       $mailer->Username = SMTP_USERNAME;
       $mailer->Password = SMTP_PASSWORD;
       $mailer->FromName = $from_name;
       $mailer->From = $from_email;

       $mailer->Sender = "enewsletter@kimsflorist.com.my";

       $mailer->AddAddress($data['to_email'], $data['to_name']);
       #$mailer->AddBCC($orderEmail,'customer');
       $mailer->Subject = $data['subject'];

       if (count($data['attachment']) > 0 && is_array($data['attachment'])) {
              foreach ($data['attachment'] as $attach) {
                     $mailer->AddAttachment($attach);
              }
       }

       $mailer->Body = $data['body'];

       if (!$mailer->Send()) {
              $db->exec("update tuser SET maillist=0, note='invalid' WHERE email='" . $data['to_email'] . "'");
              #echo "Message was not sent";
              #echo "Mailer Error: " . $mailer->ErrorInfo;
              return false;
       } else {
              return true;
       }
}

function writeCSV($header, $list, $fpath, $download = 1) {
       global $root;
       foreach ($header as $key => $dis) {
              $lines .= '"' . $dis . '",';
       }

       $lines = preg_replace("/,$/", '', $lines);
       $lines .= "\r\n";

       foreach ($list as $row) {
              $line = '';
              foreach ($header as $key => $dis) {
                     $in = preg_replace("/[\'\"]/", "", $row[$key]);
                     $in = preg_replace("/\<br\s+\/\>/", "\n", $in);
                     $line .= '"' . $in . '",';
              }

              $line = preg_replace("/,$/", '', $line);
              $line .= "\r\n";

              $lines .= $line;
       }


       $fhandle = fopen($fpath, "w+");
       fwrite($fhandle, $lines);
       fclose($fhandle);

       if ($download) {
              header("Pragma: public");
              header("Expires: 0");
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              header("Cache-Control: public");
              header("Content-Description: File Transfer");

              //Use the switch-generated Content-Type
              header("Content-Type: application/force-download");

              //Force the download
              $header = "Content-Disposition: attachment; filename=" . basename($fpath) . ";";
              header($header);
              header("Content-Transfer-Encoding: binary");
              header("Content-Length: " . filesize($fpath));
              echo $lines;
              exit;
       } else {
              return $fpath;
       }
}

function copyFileTo($source, $dest) {
       if (!copy($source, $dest)) {
              return false;
       } else {
              return true;
       }
}

function processSysMsg() {
       global $sys_msg;

       $result = "";

       if (count($sys_msg)) {
              foreach ($sys_msg as $msg_type => $msg) {
                     $result .= "<div class='alert alert-{$msg_type}'>\n";
                     foreach ($msg as $m) {
                            $result .= "<div>$m</div>\n";
                     }
                     $result .= "</div>\n";
              }
       }

       unset($sys_msg);

       return $result;
}

function _fix_gpc_magic(&$item) {
       if (is_array($item)) {
              array_walk($item, '_fix_gpc_magic');
       } else {
              $item = stripslashes($item);
       }
}

/**
 * Helper function to strip slashes from $_FILES skipping over the tmp_name keys
 * since PHP generates single backslashes for file paths on Windows systems.
 *
 * tmp_name does not have backslashes added see
 * http://php.net/manual/en/features.file-upload.php#42280
 */
function _fix_gpc_magic_files(&$item, $key) {
       if ($key != 'tmp_name') {
              if (is_array($item)) {
                     array_walk($item, '_fix_gpc_magic_files');
              } else {
                     $item = stripslashes($item);
              }
       }
}

/**
 * Fix double-escaping problems caused by "magic quotes" in some PHP installations.
 */
function fix_gpc_magic() {
       static $fixed = FALSE;
       if (!$fixed && ini_get('magic_quotes_gpc')) {
              array_walk($_GET, '_fix_gpc_magic');
              array_walk($_POST, '_fix_gpc_magic');
              array_walk($_COOKIE, '_fix_gpc_magic');
              array_walk($_REQUEST, '_fix_gpc_magic');
              array_walk($_FILES, '_fix_gpc_magic_files');
              $fixed = TRUE;
       }
}

function writeSysLog($log_tag = "", $log_action = "", $log_msg = "") {
       global $db, $login_id;

       $remote_ip = $_SERVER['REMOTE_ADDR'];
       $db->exec("INSERT INTO tsystemlog SET login_id='" . formatSQL($login_id) . "', log_tag='" . formatSQL($log_tag) . "', log_action='" . formatSQL($log_action) . "', log_msg='" . formatSQL($log_msg) . "', insert_dt=NOW(), remote_ip='" . $remote_ip . "'");
}

################################

function encode($str) {
       $crypted = md5_encrypt($str, 'DV929VD');
       $crypted = urlencode($crypted);

       return $crypted;
}

function decode($str) {
       $str = urldecode($str);
       $decrypted = md5_decrypt($str, 'DV929VD');

       return $decrypted;
}

function get_rnd_iv($iv_len) {
       $iv = '';
       while ($iv_len-- > 0) {
              $iv .= chr(mt_rand() & 0xff);
       }
       return $iv;
}

function md5_encrypt($plain_text, $password, $iv_len = 9) {
       $plain_text .= "\x13";
       $n = strlen($plain_text);
       if ($n % 9)
              $plain_text .= str_repeat("\0", 9 - ($n % 9));
       $i = 0;
       $enc_text = get_rnd_iv($iv_len);
       $iv = substr($password ^ $enc_text, 0, 512);
       while ($i < $n) {
              $block = substr($plain_text, $i, 9) ^ pack('H*', md5($iv));
              $enc_text .= $block;
              $iv = substr($block . $iv, 0, 512) ^ $password;
              $i += 9;
       }
       return base64_encode($enc_text);
}

function md5_decrypt($enc_text, $password, $iv_len = 9) {
       $enc_text = base64_decode($enc_text);
       $n = strlen($enc_text);
       $i = $iv_len;
       $plain_text = '';
       $iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
       while ($i < $n) {
              $block = substr($enc_text, $i, 9);
              $plain_text .= $block ^ pack('H*', md5($iv));
              $iv = substr($block . $iv, 0, 512) ^ $password;
              $i += 9;
       }
       return preg_replace('/\\x13\\x00*$/', '', $plain_text);
}

function getUnsubscribedLink($str, $html) {

       global $fc;

       $key = 'asa7&^%&*';
       $string = $s['email'];

       $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));

       $base = SERVER_BASE;


       if ($html) {

              $result = <<<EOF
    <br/>
    <br/>
    <center>
      <FONT SIZE="1" FACE="Verdana, Arial">
      To unsubscribe this eNewsletter, please click <a href="{$base}/unsubscribe/{$encrypted}">here</a>
      </FONT>
    </center>
EOF;
       } else {
              $result = "\n\rTo unsubscribe this eNewsletter, please visit the following URL:\n\r{$base}/unsubscribe/{$encrypted}";
       }

       return $result;
}

function getViewLink($str, $html) {

       $code = encode($str);
       if ($html) {
              $base = SERVER_BASE;
              return ""; #"<img src=\"{$base}console/broadcast.php?action=tracklink&subaction=view&code=$code\" width=\"0\" height=\"0\" border=\"0\" onerror=\"this.style.display='none'\"  />";
       }
}

function subscribed($data) {
       global $db;

       $uid = $db->field("select uid FROM tuser WHERE email='" . formatSQL($data['email']) . "'");

       if (!$uid) {
              $db->exec("INSERT INTO tuser SET
                    first_name='" . formatSQL($data['first_name']) . "',
                    last_name='" . formatSQL($data['last_name']) . "',
                    email='" . formatSQL($data['email']) . "',
                    insert_dt=now(),
                    remote_ip='" . formatSQL($_SERVER['REMOTE_ADDR']) . "'");

              //$uid = mysql_insert_id();
       }

       if ($data['gid'] > 0 && $uid > 0) {
              subscribed_group($uid, $data['gid']);
       }

       return $uid;
}

function subscribed_group($uid, $gid) {
       global $db;

       $exist = $db->field("select count(*) FROM tuser_group_map WHERE uid='$uid' AND gid='$gid'");

       if (!$exist) {
              $db->exec("INSERT INTO tuser_group_map SET uid='$uid', gid='$gid'");
       }
}

function array_sort_by_column(&$arr, $col, $dir = SORT_ASC, $type = SORT_NUMERIC) {
       $sort_col = array();
       foreach ($arr as $key => $row) {
              $sort_col[$key] = $row[$col];
       }

       array_multisort($sort_col, $type, $dir, $arr);
}

function LoadCategoryGroupOptions($group_id = 0, $strips = "", $selected_id = 0) {
       global $db;

       $strips_temp = $strips;
       $content = "";
       $selected = "";

       list($list, $listcount) = $db->multiarray("SELECT group_id, group_name FROM tgroup WHERE group_parent_id='" . $group_id . "' ORDER BY group_name ASC");

       while (list($x) = each($list)) {
              if ($selected_id == $list[$x]['group_id']) {
                     $selected = "selected";
              } else {
                     $selected = "";
              }

              $content .= "<option {$selected} value=" . $list[$x]['group_id'] . ">" . $strips . $list[$x]['group_name'] . "</option>\r\n";

              $check_child = $db->field("SELECT count(*) FROM tgroup WHERE group_parent_id='" . $list[$x]['group_id'] . "'");
              if ($check_child > 0) {
                     $strips .= $list[$x]['group_name'] . " -> ";
                     $content .= LoadCategoryGroupOptions($list[$x]['group_id'], $strips, $selected_id);
              }
              $strips = $strips_temp;
       }

       return $content;
}

function LoadCategoryGroupCheckbox($group_id = 0, $selected_id) {
       global $db;

       $content = "";
       $checked = "";

       list($list, $listcount) = $db->multiarray("SELECT group_id, group_name FROM tgroup WHERE group_parent_id='" . $group_id . "' ORDER BY group_name ASC");

       if ($group_id == 0) {
              $content .= "<ul class='mktree' id='tree1'>\r\n";
       } else {
              $content .= "<ul>\r\n";
       }

       while (list($x) = each($list)) {
              if (count($selected_id) > 0) {
                     if (in_array($list[$x]['group_id'], $selected_id)) {
                            $checked = "checked";
                     } else {
                            $checked = "";
                     }
              }

              $check_child = $db->field("SELECT count(*) FROM tgroup WHERE group_parent_id='" . $list[$x]['group_id'] . "'");
              if ($check_child > 0) {
                     //liClosed
                     //$strips .= $list[$x]['group_name']."->";
                     $content .= "<li class='liClosed'>\r\n";
                     $content .= $list[$x]['group_name'] . "\r\n";
                     $content .= LoadCategoryGroupCheckbox($list[$x]['group_id'], $selected_id);
                     $content .= "</li>\r\n";
              } else {
                     //liBullets
                     //$strips .= $list[$x]['group_name']."->";
                     $content .= "<li class='liBullet'>\r\n";
                     $content .= "<input {$checked} type='checkbox' name=group_id[]' value='{$list[$x]['group_id']}'>&nbsp;";
                     $content .= $list[$x]['group_name'] . "\r\n";
                     $content .= "</li>\r\n";
              }
              //$strips = $strips_temp;
       }

       $content .= "</ul>";

       return $content;
}

?>