<?php

$servername = "localhost";
$username = "kapitalboost";
$password = "kapital2016";
$dbname = "kapitalboost";


$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
       die("Connection failed: " . $conn->connect_error);
}

$campaignNames = [];
$campaignOptions = "";

$sql = "SELECT `campaign_name` FROM tcampaign WHERE DATEDIFF(`expiry_date`,NOW()) > 0 ORDER BY 'expiry_date' ASC";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
       // output data of each row
       while ($row = $result->fetch_assoc()) {
              array_push($campaignNames, $row["campaign_name"] );
       }
}
for($i=0;$i<count($campaignNames);$i++){
       $campaignOptions .= htmlspecialchars("<option value='").$campaignNames[$i]. htmlspecialchars("'>") . $campaignNames[$i] . htmlspecialchars("</option>");
}

echo json_encode($campaignOptions);
