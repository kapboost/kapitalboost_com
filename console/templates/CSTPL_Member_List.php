{literal}
<script type="text/javascript">

function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected member?")) 
	{
		/*runloading();*/
		mainform.subaction.value = "DeleteMember";
		mainform.submit();
	}
}

</script>
{/literal}


<form method="POST" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<legend>Member List</legend>
<!---------------Search------------->
<div class="form-group form-inline">
	<input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Name/Email">
	<input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
</div>
<!---------------Search------------->
<div class="table-responsive" >
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="20%">Username</th>
      <th width="20%">Member Email</th>
      <th width="20%">Full Name</th>
      <th width="15%">Nationality</th>
      <th width="*">Created Date</th>
    </tr>
  </thead>
  <tbody>
    {section name=outer loop=$list}
		<tr class="even">
		  <td><input type="checkbox" name="list_id[]" value="{$list[outer].member_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
		  <td><a href="index.php?t=&session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditMember&member_id={$list[outer].member_id}">{$list[outer].member_username}</a></td>
		  <td>{$list[outer].member_email}</td>
		  <td>{$list[outer].member_title} {$list[outer].member_firstname} {$list[outer].member_surname}</td>
		  <td>{$list[outer].member_country}</td>
		  <td>{$list[outer].insert_dt|date_format:$smarty.const.SMARTY_DATETIME_FORMAT}</td>
		</tr>
    {sectionelse}
		<tr class="even">
			<td colspan="5" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
  </tbody>
  
</table>
</div>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>

{$pager}