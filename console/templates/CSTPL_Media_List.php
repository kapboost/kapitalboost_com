{literal}
<script type="text/javascript">
function confirmDeletion(mainform)
{
	if(confirm("Are you sure you want to delete the selected media?")){
		/*runloading();*/
		mainform.subaction.value = "DeleteData";
		mainform.submit();
	}
}
</script>
{/literal}

<form method="POST" class="form-inline" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<legend>Media List</legend>
<!---------------Search------------->
<div class="form-group form-inline">
	<input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Name">
	<input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
</div>
<!---------------Search------------->
<br/><br/>
<div class="table-responsive" style="overflow-x: scroll">
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">No</th>
			<th width="25%">Media Name</th>
			<th width="25%">Release Date</th>
			<th width="25%">Expiry Date</th>
			<th width="40%">Media Image</th>
		</tr>
	</thead>
	<tbody>
    {section name=outer loop=$list}
		<tr class="even">
			<td><input type="checkbox" name="list_id[]" value="{$list[outer].media_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
			<td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditData&media_id={$list[outer].media_id}">{$list[outer].media_name}</a></td>
			<td>{$list[outer].release_date|date_format:"%d/%m/%Y"}</td>
			<td>{$list[outer].expiry_date|date_format:"%d/%m/%Y"}</td>
			<td>
			{if $list[outer].image != ''}
				<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/media/{$list[outer].image}" class="image-link" title="{$media_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/media/{$list[outer].image}&w=150" alt=""></a><br>
			{/if}
			</td>
		</tr>
	{sectionelse}
		<tr class="even">
			<td colspan="9" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
	</tbody>
</table>
</div>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>

</form>

{$pager}

