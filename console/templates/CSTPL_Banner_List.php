{literal}
<script type="text/javascript">

function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected product?")) 
	{
		/*runloading();*/
		mainform.subaction.value = "DeleteBanner";
		mainform.submit();
	}
}
function filterGroup(mainform) 
{
		/*runloading();*/
		mainform.subaction.value = "FilterGroup";
		mainform.submit();	
}

</script>
{/literal}


<form method="POST" class="form-inline" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">

<label>Banner Filter : </label>
	<select name="banner_group_id" class="selectpicker">
		<option value="all_banner" {if $banner_group[outer].banner_group_id == $banner_group_id}selected="selected"{/if}>All Banner</option>
		{section name=outer loop=$banner_group}
		<option value="{$banner_group[outer].banner_group_id}" {if $banner_group[outer].banner_group_id == $banner_group_id}selected="selected"{/if}>{$banner_group[outer].banner_group_name}</option>
		{/section}
	</select>
	<input name="Filter" type="button" class="btn btn-danger" id="button2" value="Filter" onclick="filterGroup(this.form);" />

<legend>Banner List</legend>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="10%">Banner Title</th>
      <th width="25%">Banner Image</th>
      <th width="25%">Banner URL</th>
      <th width="8%">Status</th>
      <th width="5%">Sort</th>
      <th width="*">Created Date</th>
    </tr>
  </thead>
  <tbody>
    {section name=outer loop=$list}
		<tr class="even">
		  <td><input type="checkbox" name="list_id[]" value="{$list[outer].banner_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
		  <td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditBanner&banner_id={$list[outer].banner_id}">{$list[outer].banner_title}</a></td>
		  <td>
			{if $list[outer].banner_pathimages != ''}
				<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/banner/{$list[outer].banner_pathimages}" class="image-link" title="{$banner_title}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/banner/{$list[outer].banner_pathimages}&w=150" alt=""></a><br>
			{/if}
		  </td>
		  <td>{$list[outer].banner_url}</td>
		  <td>
			{if $list[outer].banner_status == '1'}
				Online
			{else}
				Offline
			{/if}
		  </td>
		  <td>{$list[outer].banner_sort}</td>
		  <td>{$list[outer].insert_dt|date_format:$smarty.const.SMARTY_DATETIME_FORMAT}</td>
		</tr>
    {sectionelse}
		<tr class="even">
			<td colspan="7" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
  </tbody>
  
</table>
</div>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>

{$pager}
