{literal}
<script src="js/jquery-ui.min.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script type="text/javascript">
       $(document).ready(function () {
              $("#insert_dt").datepicker({
                     dateFormat: "{/literal}{$smarty.const.DIS_DATEPICKER_FORMAT}{literal}",
                     changeMonth: true,
                     numberOfMonths: 1
              });
       });

</script>

{/literal}

<form class="form-inline" role="form">
       <div>
              <label>Search with | Investors' Names | Emails | Countries | Campaign Name :</label>
              <input type="text" id="keyword" name="keyword" placeholder="Default : Search ALL"  max-width="500px" style="border:1px solid black;border-radius:10px;margin:5px 20px;height:35px;width:300px;padding-left: 10px" />
              <input type="button" class="btn btn-primary go" name="go" value="Go" onclick="Search()" style="width: 80px" /> 
       </div>
       <br />
</form>

<script>
       {
              literal
       }
       function Search() {
              var keyword = document.getElementById('keyword').value;
              AddOrReplaceGetParameter("keyword", keyword);

       }
       function AddOrReplaceGetParameter(parameterName, parameterValue) {
              var result = "", tmp = [], exist = false;
              var items = location.search.substr(1).split("&");
              for (var i = 0; i < items.length; i++) {
                     tmp = items[i].split("=");
                     if (tmp[0] === parameterName) {
                            items[i] = parameterName + "=" + parameterValue;
                            exist = true;
                     }
                     if (tmp[0] === "page") {
                            items[i] = "page=1";
                     }
                     result = result + items[i] + "&";
              }
              if (exist === false) {
                     result = result + parameterName + "=" + parameterValue + "&";
              }
              result = result.substring(0, result.length - 1); // Remove the last "&"
              result = "https://kapitalboost.com/console/index.php?" + result;
              window.location.href = result;
       }
       {
              /literal}
</script>

<div class="table-responsive">
       <table class="table table-striped table-bordered table-hover">
              <thead>
                     <tr>
                            <th width="3%">No</th>
                            <th width="13%">Investor Name</th>
                            <th width="10%">Investor Email</th>
                            <th width="12%">Campaign Name</th>
                            <th width="10%"> Payment Type</th>
                            <th width="10%">Total Funding</th>
                            <th width="15%">Date Of Payment </th>
                            <th width="5%"> Status</th>

                     </tr>
              </thead>
              <tbody>
                     {section name=outer loop=$list}
                     <tr class="even">
                            <td>{$smarty.section.outer.index+1}</td>
                            <td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditBankListData&id={$list[outer].id}">{$list[outer].nama}</a></td>
                            <td>{$list[outer].email}</td>
                            <td>{$list[outer].campaign|default:'-'}</td>
                            <td>{$list[outer].tipe|default:'-'}</td>
                            <td>{$list[outer].total_funding|default:'-'}</td>
                            <td>{$list[outer].date|date_format:"%d-%m-%Y %H:%M"}</td>
                            {if $list[outer].status=='Paid'}
                            <td><label class="label label-success"> Paid </label></td>
                            {else}
                            <td><label class="label label-warning"> Unpaid </label></td>
                            {/if}

                     </tr>
                     {sectionelse}
                     <tr class="even">
                            <td colspan=7>
              <center><i>No Record Found ...</i></center>
              </td>
              </tr>
              {/section}
              </tbody>
       </table>
</div>
{$pager}

{literal}<style>
       #iName{
              float: left
       }
</style>{/literal}
<div id="addI">
       <input id="iName" type="text" />

       <select name="campaign" id="campaign" class="cat_dropdown">
              <option value="">Select Campaign</option>
              <script>
                            {literal}
                            $.post("GetChampaignNames.php",function(data){
                                   console.log(data);
                            });
       {/literal}
                            </script>
       </select>
       
       <div>{$campaign_name_options}</div>

</div>



{literal}
<script>


              $("#iName").autocomplete({
                     source: "GetInvestorNames.php",
                     minLength: 2,
                     autoFocus: true,
                     delay: 100,
                     select: function (event, ui) {
                            $("#iName").css("color", "green");
              }



       });




</script>

{/literal}