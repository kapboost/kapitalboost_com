<!--=========Users Box=========-->
<div class="small_box">
  <div class="header">
    <img src="{$THEME_PATH}/images/users_icon.png" alt="History" width="24" height="24" />Online Users
  </div>
  <div class="body">
    <ul class="user_list">
    	{section name=count loop=$list}
      <li><img src="{$THEME_PATH}/images/user_placeholder.gif" width="54" height="54" alt="Username" /><b>{$list[count].console_user}</b><small>{$list[count].name}</small></li>
      {/section}
    </ul>
  </div>
</div>
<!--End Users Box-->