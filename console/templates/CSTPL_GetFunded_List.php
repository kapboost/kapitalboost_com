{literal}
<script type="text/javascript">
function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected campaign?")){
		/*runloading();*/
		mainform.subaction.value = "DeleteData";
		mainform.submit();
	}
}
</script>
{/literal}

<form method="POST" class="form-inline" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="campaign_id" value="{$campaign_id}">
<input type="hidden" name="subaction" value="">
<legend>Get Funded List</legend>
<!---------------Search------------->
<!--<div class="form-group form-inline">
	<input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Slug/Name">
	<input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
</div><br/><br/>-->
<!---------------Search------------->

<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">No</th>
			<th width="10%">Full Name</th>
			<th width="10%">Email</th>
			<th width="10%">Mobile</th>
			<th width="10%">Project Type</th>
			<th width="10%">Funding Amount</th>
			<th width="10%">Est Rev</th>
			<th width="10%">Funding Period</th>
		</tr>
	</thead>
	<tbody>
    {section name=outer loop=$list}
		<tr class="even">
			<td><input type="checkbox" name="list_id[]" value="{$list[outer].getfunded_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
			<td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditGetFundedData&getfunded_id={$list[outer].getfunded_id}">{$list[outer].fullname}</a></td>
			<td>{$list[outer].email}</td>
			<td>{$list[outer].mobile}</td>
			<td>{$list[outer].project_type}</td>
			<td>{$list[outer].funding_amount}</td>
			<td>{$list[outer].est_an_rev}</td>
			<td>{$list[outer].funding_period}</td>
		</tr>
	{sectionelse}
		<tr class="even">
			<td colspan="8" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
	</tbody>
</table>
</div>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>

{$pager}