<?php
mysql_connect("localhost", "elgibors_gst", "ZmvSw_40");
mysql_select_db("elgibors_temple");

$get_cookie_id = $_GET['cookie_id'];
//echo $get_cookie_id;

$sql = " SELECT t.*,c.*,d.*,d.*,b.*
			FROM ttransaction AS t 
			JOIN tdelivery AS d ON d.cookie_id = t.cookie_id
			JOIN tshoppingcart AS s ON t.cookie_id = s.cookie_id
			JOIN tshoppingcart_detail as c ON s.shopping_cart_id = c.shopping_cart_id
			JOIN tbilling as b ON t.cookie_id = b.cookie_id
			WHERE t.cookie_id = $get_cookie_id";
//echo $sql;

$invoice = mysql_query($sql);

$data = mysql_fetch_array($invoice);

// print_r($data);

$product_query = " SELECT DISTINCT c.product_name, c.product_description, c.product_quantity, c.product_price
						FROM tshoppingcart_detail AS c 
						JOIN tshoppingcart as s ON s.shopping_cart_id = c.shopping_cart_id
						WHERE s.cookie_id = $get_cookie_id";

$go = mysql_query($product_query);
$product = mysql_fetch_assoc($go);
?>
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="UTF-8" />
<title>Invoice</title>
<link href="../assets/print/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="../assets/print/bootstrap.min.js"></script>
<link href="../assets/print/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="../assets/print/stylesheet.css" rel="stylesheet" media="all" />
</head>
<body>
<div class="container">
    <div style="page-break-after: always;">
    <h1>Invoice #<?php echo $data[transaction_id]; ?></h1>
    <table class="table table-bordered">
		<thead>
			<tr>
				<td colspan="2">Order Details</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width: 50%;">
					<address>
						<strong>Temple Project</strong><br />
						No. 1, Jalan Sungai Chat, Komplek Mawar, 80100<br />
						Johor Bahru, Johor, 80100<br />
						Malaysia
					</address>
					<b>Telephone:</b> +607-2243663<br />
					<b>E-Mail:</b> enquiry@templeproject.com<br />
					<b>Web Site:</b> <a href="http://www.templeproject.com">http://www.templeproject.com</a>
				</td>
				<td style="width: 50%;">
					<b>Date Added:</b> <?php echo $data[insert_dt]; ?><br />
					<b>Order ID:</b> <?php echo $data[transaction_id]; ?><br />
					<b>Payment Method:</b> <?php echo $data[payment_method]; ?><br />
					<b>Shipping Method:</b> Delivery <br />
					<b>Delivery Date</b> <?php echo $data[delivery_date_from]; ?><br />
					<b>Delivery Time</b>
					<?php 
					if ($data[delivery_time]==1)
					{
						echo "9am - 2pm";
					}
					else if($data[delivery_time]==2)
					{
						echo "2pm - 6pm";
					}
					?>
					<br />
				</td>
			</tr>
		</tbody>
    </table>
	
    <table class="table table-bordered">
		<thead>
			<tr>
				<td style="width: 50%;"><b>Billing Address</b></td>
				<td style="width: 50%;"><b>Delivery Address</b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<address>
						<?php echo $data[billing_givenname]; ?><br />
						<?php
						echo $data[billing_street_1]; echo " ";
						echo $data[billing_unit]; echo " ";
						echo $data[billing_city]; 
						echo $data[billing_contact_no];
						?><br />
						<?php
						echo $data[billing_state]; echo " ";
						echo $data[billing_postal_code];
						?>
						<br />
						<?php $data[billing_country];?>
					</address>
				</td>
				<td>
					<address>
						<?php echo $data[delivery_givenname]; ?><br />
						<?php
						echo $data[delivery_street_1]; echo " ";
						echo $data[delivery_unit]; echo " ";
						echo $data[delivery_city]; 
						?><br />
						<?php
						echo $data[delivery_state]; echo " ";
						echo $data[delivery_postal_code];
						echo $data[billing_contact_no];
						?>
						<br />
						<?php $data[delivery_country];?>
					</address>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<td><b>Product Name</b></td>
				<td><b>Product Description</b></td>
				<td class="text-right"><b>Quantity</b></td>
				<td class="text-right"><b>Packet</b></td>
				<td class="text-right"><b>Unit Price</b></td>
				<td class="text-right"><b>Total</b></td>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($product as $p) { ?>
			<?php print_r ($p)?>
			<tr>
				<td><?php echo $p[product_name]; ?></td>
				<td><?php echo $p[product_description]; ?></td>
				<td class="text-right"><?php echo $p[product_quantity]; ?></td>
				<td class="text-right"><?php echo $p[product_name]; ?></td>
				<td class="text-right">RM <?php echo $p[product_price]; ?></td>
				<td class="text-right">RM <?php echo $p[subtotal]; ?></td>
			</tr>
			<?php } ?>
			
			<tr>
				<td class="text-right" colspan="5"><b>Sub-Total</b></td>
				<td class="text-right">RM <?php echo $data[subtotal]; ?></td>
			</tr>
			<tr>
				<td class="text-right" colspan="5"><b>Flat Shipping Rate</b></td>
				<td class="text-right">RM <?php echo $data[total_shipping_charge]; ?></td>
			</tr>
			<tr>
				<td class="text-right" colspan="5"><b>GST</b></td>
				<td class="text-right">RM <?php echo $data[total_gst]; ?></td>
			</tr>
			<tr>
				<td class="text-right" colspan="5"><b>Total</b></td>
				<td class="text-right">RM <?php echo $data[grand_total]; ?></td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
</body>
</html>
<?php

?>