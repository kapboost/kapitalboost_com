 

<!DOCTYPE html>
<html lang="en">
       <head>
              <meta charset="utf-8">
              <meta http-equiv="X-UA-Compatible" content="IE=edge">
              <meta name="viewport" content="width=device-width, initial-scale=1">
              <meta name="description" content="">
              <meta name="author" content="">
              <link rel="shortcut icon" href="ico/favicon.ico">

              <title>Kapitalboost's Admin</title>

              <!-- Bootstrap core CSS -->
              <link href="css/bootstrap.css" rel="stylesheet">
              <!-- Bootstrap theme -->
              <link href="css/bootstrap-theme.css" rel="stylesheet">

              <link href="css/font-import.css" rel="stylesheet">
              <link href="css/admin-panel.css" rel="stylesheet">

              <!-- DATE PICKER !-->    
              <link rel="stylesheet" href="js/pickadate/themes/default.css" id="theme_base">
              <link rel="stylesheet" href="js/pickadate/themes/default.date.css" id="theme_date">
              <link rel="stylesheet" href="js/pickadate/themes/default.time.css" id="theme_time">

              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


              <link href="css/custom.css" rel="stylesheet">

              <script src="js/jquery-1.10.2.js"></script>

              <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
              <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
              <![endif]-->
       </head>

       <body role="document">
              <!-------------------------------------------- top area -------------------------------------------->
              <div class="top-area-wrapper navbar-fixed-top">
                     <!-------------------------------------- top panel -------------------------------------->
                     <div class="top-panel-wrapper">
                            <div class="top-panel-inner-wrapper">
                                   <!------------------------------ logo ------------------------------>
                                   <div class="top-logo-wrapper"><img src="https://kapitalboost.com/assets/images/logo/kapitalboost-logo.png" width="200px" height="20px" style="margin-top:10px;"></div>
                                   <!------------------------------ //logo ------------------------------>
                                   <!------------------------------ top nav btn ------------------------------>
                                   <div class="main-top-nav-wrapper btn-group">
                                          <button type="button" class="main-top-nav-btn btn btn-default" id="menuWrap">
                                                 <span class="main-top-nav-icon glyphicon glyphicon-align-justify"></span>
                                          </button>
                                          <ul class="main-top-nav-drop dropdown-menu" role="menu" id="menuItem">
                                                 <li>
                                                        <div id="subMenuBuka_1">Application</div>
                                                        <div id="subMenuTutup_1">Application</div>
                                                        <div class="subDrop" id="subMenuContent_1">
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=1&action_id=11&action=LoadSystemForm">System - Add</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=1&action_id=10&action=ListSystem">System - List</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=1&action_id=7&action=LoadModuleForm">Module - Add</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=1&action_id=6&action=ListModule">Module - List</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=1&action_id=1&action=LoadActionForm">Action - Add</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=1&action_id=2&action=ListAction">Action - List</a></p>
                                                        </div>
                                                 </li>
                                                 <li>
                                                        <div id="subMenuBuka_2">Users</div>
                                                        <div id="subMenuTutup_2">Users</div>
                                                        <div class="subDrop" id="subMenuContent_2">
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=2&action_id=9&action=LoadUserGroupForm">User Group - Add</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=2&action_id=8&action=ListUserGroup">User Group - List</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=2&action_id=5&action=LoadUserForm">User - Add</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=2&action_id=4&action=ListUser">User - List</a></p>
                                                               <p><a href="index.php?t=&session={$session}&system_id=1&module_id=2&action_id=3&action=ListUserStatus">User - Status</a></p>
                                                        </div>
                                                 </li>
                                                 <li>
                                                        <a href="index.php?t=&session={$session}&system_id=1&module_id=96&action_id=16&action=ListLog">System Log</a>
                                                 </li>
                                                 <li>
                                                        <a href="index.php?logout=1&session={$session}">Logout </a>
                                                 </li>
                                          </ul>
                                   </div>

                                   <!------------------------------ //top nav btn ------------------------------>
                                   <!------------------------------ top user info ------------------------------>
                                   <div class="top-user-wrapper">
                                          <div class="top-user-info-wrapper btn-group">
                                                 <button type="button" class="main-top-user-btn btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                        <div class="hidden-xs">

                                                               <div class="top-user-info-content-wrapper">
                                                                      <span class="top-user-info-name">{$login}</span>
                                                                      <span class="top-user-info-designation">{$usergroup_name}</span>
                                                               </div>
                                                               <div class="clearfix"></div>
                                                        </div>
                                                        <div class="visible-xs"><span class="main-top-user-icon glyphicon glyphicon-align-justify"></span></div>
                                                 </button>

                                          </div>
                                          <div class="top-user-pic-wrapper hidden-xs"></div>
                                   </div>
                                   <!------------------------------ //top user info ------------------------------>
                                   <!------------------------------ top title ------------------------------>
                                   <div class="top-title-wrapper"></div>
                                   <!------------------------------ //top title ------------------------------>
                                   <div class="clear"></div>
                            </div>
                     </div>
                     <!-------------------------------------- //top panel -------------------------------------->
                     <!-------------------------------------- nav bar -------------------------------------->
                     <div>
                            <div class="main-menu navbar navbar-inverse" role="navigation">
                                   <div class="container-fluid">
                                          <div class="navbar-header">
                                                 <button type="button" class="main-menu navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                                        <span class="sr-only">Toggle navigation</span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                        <span class="icon-bar"></span>
                                                 </button>
                                          </div>
                                          <div class="navbar-collapse collapse">
                                                 <ul class="main-menu nav navbar-nav">


                                                        <li><a href="index.php?t=&session={$session}&system_id=33&module_id=114&action_id=72&action=LoadDataList">Campaign - List</a></li>
                                                        <li><a href="index.php?t=&session={$session}&system_id=33&module_id=114&action_id=73&action=LoadDataForm">Campaign - Add</a></li>

                                                        <li><a href="index.php?t=&session={$session}&system_id=33&module_id=114&action_id=78&action=LoadGetFundedList">Get Funded List</a></li>

                                                        <!-- <li><a href='index.php?t=&session={$session}&system_id=33&module_id=114&action_id=78&action=LoadGetFundedList'>Add Investment</a></li>-->

                                                        <li class="dropdown">
                                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">Investments<b class="caret"></b></a>
                                                               <ul class="dropdown-menu">
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=119&action_id=85&action=all">All</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=119&action_id=81&action=InvestorList">Paypal</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=119&action_id=83&action=BankList">Bank</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=119&action_id=84&action=XfersList">Xfers</a></li>
                                                               </ul>
                                                        </li>
                                                        <li class="dropdown">
                                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">Partner <b class="caret"></b></a>
                                                               <ul class="dropdown-menu">
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=111&action_id=66&action=LoadDataList">Partner - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=111&action_id=67&action=LoadDataForm">Partner - Add</a></li>
                                                               </ul>
                                                        </li>

                                                        <li><a href="index.php?t=&session={$session}&system_id=33&module_id=121&action_id=86&action=EnquireList">Enquire List</a></li>
                                                        <li class="dropdown">
                                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
                                                               <ul class="dropdown-menu">
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=113&action_id=74&action=LoadDataList">Blog - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=113&action_id=75&action=LoadDataForm">Blog - Add</a></li>
                                                               </ul>
                                                        </li>

                                                        <li><a href="index.php?t=&session={$session}&system_id=33&module_id=116&action_id=77&action=LoadDataList">General Page</a></li>
                                                        <li class="dropdown">
                                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">Member <b class="caret"></b></a>
                                                               <ul class="dropdown-menu">
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=101&action_id=30&action=LoadMemberForm">Member - Add</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=101&action_id=31&action=ListMember">Member - List</a></li>
                                                               </ul>
                                                        </li>
                                                        <li class="dropdown">
                                                               <a href="#" class="dropdown-toggle" data-toggle="dropdown">Others<b class="caret"></b></a>
                                                               <ul class="dropdown-menu">
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=112&action_id=70&action=LoadDataList">Event - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=112&action_id=71&action=LoadDataForm">Event - Add</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=118&action_id=80&action=MemberTLog">Member Tracking - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=110&action_id=68&action=LoadDataList">Media - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=110&action_id=69&action=LoadDataForm">Media - Add</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=120&action_id=85&action=ContactList">Messages - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=115&action_id=64&action=LoadDataList">Testimonials - List</a></li>
                                                                      <li><a href="index.php?t=&session={$session}&system_id=33&module_id=115&action_id=65&action=LoadDataForm">Testimonials - Add</a></li>
                                                               </ul>
                                                        </li>
                                                 </ul>
                                          </div>
                                   </div>
                            </div>
                     </div>
                     <!-------------------------------------- //nav bar -------------------------------------->
              </div>
              <!-------------------------------------------- //top area -------------------------------------------->

              <!-------------------------------------------- content area -------------------------------------------->
              <div class="main-content-area-wrapper container-fluid"  style="min-height: 665px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">{$module_name}</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">
                                                 {if $sys_msg ne ""}
                                                 <div id="sys_msg">
                                                        {$sys_msg}
                                                 </div>
                                                 {/if}
                                                 <center>
                                                        {if $mod_content ==NULL} 
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Member
                                                               </p>
                                                               <p></p>
                                                               <p class="dasCount">
                                                                      {$list|@count}

                                                               </p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Investor
                                                               </p>
                                                               <p class="dasCount">
                                                                      {$inv|@count}
                                                               </p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Total Campaign
                                                               </p>
                                                               <p class="dasCount">
                                                                      {$cmp|@count}
                                                               </p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      On Going Campaign
                                                               </p>
                                                               <p class="dasCount">
                                                                      10
                                                               </p>
                                                               </p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Closed Campaign
                                                               </p>
                                                               <p class="dasCount">
                                                                      1
                                                               </p>
                                                        </div>
                                                        {else}
                                                        {$mod_content}{/if}
                                                 </center>

                                          </div>
                                   </div>
                            </div>

                     </div>

              </div>
              <!-------------------------------------------- //content area -------------------------------------------->


              <!-- Start footer -->
              <div class="footerWrap">
                     <p class="FTitle">
                            Copyright &copy; PT.Globe Teledigital Indonesia 
                     </p>
              </div>
              <!-- End footer -->









              <!-- Bootstrap core JavaScript
              ================================================== -->
              <!-- Placed at the end of the document so the pages load faster -->

              <script src="js/bootstrap.min.js"></script>
              <script src="js/jquery-ui-1.10.4.min.js"></script>
              <script src="js/bootstrap-select.js" type="text/javascript"></script>
              <link href="css/bootstrap-select.css" rel="stylesheet" >

              <!-- Magnific Popup core CSS file -->
              <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css"> 
              <!-- Magnific Popup core JS file -->
              <script src="js/magnific-popup/jquery.magnific-popup.js"></script>

              <!-- DATEPICKER -->
              <script src="js/pickadate/picker.js"></script>
              <script src="js/pickadate/picker.date.js"></script>
              <script src="js/pickadate/picker.time.js"></script>
              <script src="js/pickadate/legacy.js"></script>


              {literal}
              <script type="text/javascript">
                     $('.selectpicker').selectpicker();


                     $('#datepicker,#datepicker2').pickadate({
                            format: 'dd/mm/yyyy'
                     });

                     $('.image-link').magnificPopup({type: 'image'});




                     // ADD SLIDEDOWN ANIMATION TO DROPDOWN //
                     $('.dropdown-toggle').on('show.bs.dropdown', function (e) {
                            $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
                     });

                     // ADD SLIDEUP ANIMATION TO DROPDOWN //
                     $('.dropdown-toggle').on('hide.bs.dropdown', function (e) {
                            $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
                     });



                     /* start menu */
                     $(document).ready(function () {
                            $('#menuWrap').mouseenter(function () {
                                   $('#menuItem').show();
                            });

                            $('#menuItem').mouseleave(function () {
                                   $('#menuItem').hide();
                            });
                     });
                     /* start menu */



                     /* start sub menu */
                     $(document).ready(function () {
                            $('#subMenuTutup_1').click(function () {
                                   $('#subMenuBuka_1').show();
                                   $('#subMenuTutup_1').hide();
                                   $('#subMenuContent_1').slideUp();
                            });
                            $('#subMenuBuka_1').click(function () {
                                   $('#subMenuTutup_1').show();
                                   $('#subMenuBuka_1').hide();
                                   $('#subMenuContent_1').slideDown();
                            });

                            $('#subMenuTutup_2').click(function () {
                                   $('#subMenuBuka_2').show();
                                   $('#subMenuTutup_2').hide();
                                   $('#subMenuContent_2').slideUp();
                            });
                            $('#subMenuBuka_2').click(function () {
                                   $('#subMenuTutup_2').show();
                                   $('#subMenuBuka_2').hide();
                                   $('#subMenuContent_2').slideDown();
                            });

                            /*
                             $('#subMenuTutup_3').click(function(){
                             $('#subMenuBuka_3').show();
                             $('#subMenuTutup_3').hide();
                             $('#subMenuContent_3').slideUp();
                             });
                             $('#subMenuBuka_3').click(function(){
                             $('#subMenuTutup_3').show();
                             $('#subMenuBuka_3').hide();
                             $('#subMenuContent_3').slideDown();
                             });
                             
                             $('#subMenuTutup_4').click(function(){
                             $('#subMenuBuka_4').show();
                             $('#subMenuTutup_4').hide();
                             $('#subMenuContent_4').slideUp();
                             });
                             $('#subMenuBuka_4').click(function(){
                             $('#subMenuTutup_4').show();
                             $('#subMenuBuka_4').hide();
                             $('#subMenuContent_4').slideDown();
                             });
                             */
                     });
                     /* end sub menu */


              </script>
              {/literal}


       </body>
</html>
