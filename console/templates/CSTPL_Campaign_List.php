{literal}
<script type="text/javascript">
       function confirmDeletion(mainform)
       {
              if (confirm("Are you sure you want to delete the selected campaign?")) {
                     /*runloading();*/
                     mainform.subaction.value = "DeleteData";
                     mainform.submit();
              }
       }
</script>
{/literal}

<form method="POST" class="form-inline" action="index.php">
       <input type="hidden" name="action" value="{$action}">
       <input type="hidden" name="action_id" value="{$action_id}">
       <input type="hidden" name="module_id" value="{$module_id}">
       <input type="hidden" name="session" value="{$session}">
       <input type="hidden" name="system_id" value="{$system_id}">
       <input type="hidden" name="campaign_id" value="{$campaign_id}">
       <input type="hidden" name="subaction" value="">
       <legend>Campaign List</legend>
       <!---------------Search------------->
       <div class="form-group form-inline">
              <input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Slug/Name">
              <input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
       </div>
       <!---------------Search------------->
       <br/><br/>
       <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover">
                     <thead>
                            <tr>
                                   <th width="5%">No</th>
                                   <th width="25%">Campaign Name</th>
                                   <!--<th width="25%">Slug / URL</th>-->
                                   <th width="15%">Project Type</th>
                                   <th width="10%">Release Date</th>
                                   <!--<th width="10%">Expiry Date</th>-->
                                   <th width="10%">Closing Date</th>
                                   <th width="15%">Total Funding</th>
                                   <th width="15%">Min. Investment</th>
                                   <th width="15%">Funding Summary</th>
                                   <th width="15%">Project Return</th>
                                   <th width="15%">Curr Funding AMT</th>
                                   <!--th>Image 1</th>
                                   <th>Image 2</th>
                                   <th>Image 3</th>
                                   <th>Image 4</th>
                                   <th>Image 5</th>
                                   <th>Image 6</th>
                                   <th>Image 7</th>
                                   <th>Image 8</th>
                                   <th>Image 9</th>
                                   <th>Image 10</th>
                                   <th>Image 11</th>
                                   <th>Image 12</th>
                                   <th>Image 13</th>
                                   <th>Image 14</th>
                                   <th>Image 15</th>
                                   <th>Image 16</th>
                                   <th>Image 17</th>
                                   <th>Image 18</th>
                                   <th>Image 19</th>
                                   <th>Image 20</th-->
                            </tr>
                     </thead>
                     <tbody>
                            {section name=outer loop=$list}
                            <tr class="even">
                                   <td><input type="checkbox" name="list_id[]" value="{$list[outer].campaign_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
                                   <td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditData&campaign_id={$list[outer].campaign_id}">{$list[outer].campaign_name}</a></td>
                                   <!--<td>{$list[outer].slug}</td>-->
                                   <td>{$list[outer].project_type}</td>
                                   <td>{$list[outer].release_date|date_format:"%d/%m/%Y"}</td>
                                   <!--<td>{$list[outer].expiry_date|date_format:"%d/%m/%Y"}</td>-->
                                   <td>{$list[outer].clossing_date|date_format:"%d/%m/%Y"}</td>
                                   <td>{$list[outer].total_funding_amt}</td>
                                   <td>{$list[outer].minimum_investment}</td>
                                   <td>{$list[outer].funding_summary}</td>
                                   <td>{$list[outer].project_return}</td>
                                   <td>{$list[outer].curr_funding_amt}</td>


                                   <!--td>
                                   {if $list[outer].campaign_logo != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].campaign_logo}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].campaign_logo}&w=150" alt=""></a><br>
                                   {/if}
                                   </td-->
                                   <!--td>
                                   {if $list[outer].image_1 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_1}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_1}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_2 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_2}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_2}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_3 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_3}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_3}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_4 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_4}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_4}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_5 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_5}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_5}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_6 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_6}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_6}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_7 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_7}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_7}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_8 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_8}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_8}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_9 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_9}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_9}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_10 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_10}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_10}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_11 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_11}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_11}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_12 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_12}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_12}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_13 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_13}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_13}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_14 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_14}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_14}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_15 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_15}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_15}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_16 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_16}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_16}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_17 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_17}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_17}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_18 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_18}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_18}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_19 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_19}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_19}&w=150" alt=""></a><br>
                                   {/if}
                                   </td>
                                   <td>
                                   {if $list[outer].image_20 != ''}
                                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_20}" class="image-link" title="{$campaign_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$list[outer].image_20}&w=150" alt=""></a><br>
                                   {/if}
                                   </td-->
                            </tr>
                            {sectionelse}
                            <tr class="even">
                                   <td colspan="25" align="center"><i>Data not found...<i></td>
                                                        </tr>
                                                        {/section}
                                                        </tbody>
                                                        </table>
                                                        </div>

                                                        <p>
                                                               <input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
                                                        </p>
                                                        </form>

                                                        {$pager}