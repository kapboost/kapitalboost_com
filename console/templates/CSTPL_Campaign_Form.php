<script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
<!--=========Forms=========-->
<style>

       {literal}
       @media screen and (max-width: 300px) {

              .form-input input[type="text"], .form-input input[type="email"], .form-input input[type="phone"], .form-input input[type="number"], .form-input input[type="tel"], .form-input-wide input[type="text"], .form-input-wide input[type="email"], .form-input-wide input[type="phone"], .form-input-wide input[type="number"], .form-input-wide input[type="tel"] {

                     width: 180px !important;

              }
              {/literal}
       </style>

       <form action="index.php" method="POST" enctype="multipart/form-data">
              <input type="hidden" name="action" value="{$action}">
              <input type="hidden" name="action_id" value="{$action_id}">
              <input type="hidden" name="module_id" value="{$module_id}">
              <input type="hidden" name="session" value="{$session}">
              <input type="hidden" name="system_id" value="{$system_id}">
              <input type="hidden" name="action" value="ValidateDataForm" />
              <input type="hidden" name="campaign_id" value="{$campaign_id}">

              <legend>Campaign Form</legend>
              {$json|@json_encode}
              <div class="row">
                     <div class="col-xs-12 col-md-6">
                            <!--<div class="form-group">
                                <label>Slug : </label>
                                <p class="bg-info context-text-panel">Leave it blank to let system generate it</p>
                                <input type="text" name="slug" value="{$slug}" class="form-control">
                            </div>-->

                            <div class="form-group">
                                   <label>Campaign Name : </label>
                                   <input type="text" name="campaign_name" value="{$campaign_name}" class="form-control">
                            </div>
                            <div class="form-group">
                                   <label>Campaign Owner : </label>
                                   <input type="text" name="campaign_owner" value="{$campaign_owner}" class="form-control">
                            </div>
                            <div class="form-group">
                                   <label>Campaign Owner Email : </label>
                                   <input type="text" name="campaign_owner_email" value="{$campaign_owner_email}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Company Name : </label>
                                   <input type="text" name="company_name" value="{$company_name}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Classification : </label>
                                   <input type="text" name="classification" value="{$classification}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Project Type : </label><br/>
                                   <select name="project_type" id="project_type" class="form-control" data-live-search="true" data-style="btn-default">
                                          {html_options options=$project_type_options selected=$project_type}
                                   </select>
                            </div>

                            <div class="form-group" id="private_password" style="display:none;">
                                   <label>Private Password : </label><br/>
                                   <input type="text" name="private_password" value="{$private_password}" class="form-control">
                            </div>



                            <div class="form-group">
                                   <label>Industry : </label>
                                   <input type="text" name="industry" value="{$industry}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Risk : </label>
                                   <select name="risk" value="Medium" class="form-control">
                                          {if $risk == "N/A"}
                                          <option value="N/A" selected="selected" >N/A</option>
                                          <option value="Low">Low</option>
                                          <option value="Medium">Medium</option>
                                          <option value="High">High</option>
                                          {elseif $risk == "Low"}
                                          <option value="N/A" >N/A</option>
                                          <option value="Low" selected="selected">Low</option>
                                          <option value="Medium">Medium</option>
                                          <option value="High">High</option>
                                          {elseif $risk == "Medium"}
                                          <option value="N/A" >N/A</option>
                                          <option value="Low">Low</option>
                                          <option value="Medium" selected="selected">Medium</option>
                                          <option value="High">High</option>
                                          {elseif $risk == "High"}
                                          <option value="N/A" >N/A</option>
                                          <option value="Low" >Low</option>
                                          <option value="Medium">Medium</option>
                                          <option value="High" selected="selected">High</option>
                                          {else}
                                          <option value="N/A" selected="selected" >N/A</option>
                                          <option value="Low" selected="selected">Low</option>
                                          <option value="Medium" selected="selected">Medium</option>
                                          <option value="High" selected="selected">High</option>
                                          {/if}
                                   </select>
                            </div>

                            <div class="form-group">
                                   <label>Country : </label>
                                   <select name="member_country" class="form-control" data-live-search="true" data-style="btn-default">
                                          <option value="">-- Please Select --</option>
                                          {html_options options=$member_country_options selected=$country}
                                   </select>
                                   <!--<input type="text" name="country" value="{$country}" class="form-control">-->
                            </div>

                            <div class="form-group">
                                   <label>Release Date : </label>
                                   <input type="text" name="release_date" id="datepicker" value="{$release_date|date_format:"%d/%m/%Y"}" class="form-control">
                            </div>

                            <!--<div class="form-group">
                                <label>Expiry Date : </label>
                                <input type="text" name="expiry_date" id="datepicker" value="{$expiry_date|date_format:"%d/%m/%Y"}" class="form-control">
                            </div>-->

                            <div class="form-group">
                                   <label>Closing Date : </label>
                                   <input type="text" name="clossing_date" id="datepicker" value="{$clossing_date|date_format:"%d/%m/%Y"}" class="form-control">
                            </div>

                            <!--<div class="form-group">
                                <label>Address : </label>
                                <input type="text" name="address" value="{$address}" class="form-control">
                            </div>
                        
                            <div class="form-group">
                                <label>NA : </label>
                                <input type="text" name="na" value="{$na}" class="form-control">
                            </div>
                        
                            <div class="form-group">
                                <label>City : </label>
                                <input type="text" name="city" value="{$city}" class="form-control">
                            </div>
                        
                            <div class="form-group">
                                <label>State : </label>
                                <input type="text" name="state" value="{$state}" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Risk : </label>
                                <input type="text" name="state" value="{$state}" class="form-control">
                            </div>
                        
                            <div class="form-group">
                                <label>Zipcode : </label>
                                <input type="text" name="zipcode" value="{$zipcode}" class="form-control">
                            </div>-->

                     </div>

                     <div class="col-xs-12 col-md-6">
                            <div class="form-group">
                                   <label>Total Funding AMT : </label>
                                   <input type="number" name="total_funding_amt" value="{$total_funding_amt}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Minimum Investment : </label>
                                   <input type="number" name="minimum_investment" value="{$minimum_investment}" class="form-control">
                            </div>

                            <!--<div class="form-group">
                                <label>Industry Country : </label>
                                <input type="text" name="industry_country" value="{$industry_country}" class="form-control">
                            </div>-->

                            <div class="form-group">
                                   <label>Tenor : </label>
                                   <input type="text" name="funding_summary" value="{$funding_summary}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Curr Funding AMT : </label>
                                   <input type="number" name="curr_funding_amt" value="{$curr_funding_amt}" class="form-control">
                            </div>

                            <div class="form-group">
                                   <label>Project Return : </label>
                                   <input type="text" name="project_return" value="{$project_return}" class="form-control">
                            </div>

                            <div style="min-height: 180px;">

                                   <div class="form-group">
                                          <label>Campaign Logo : </label>
                                          <input type="file" name="campaign_logo" >
                                          {if $campaign_logo != ''}
                                          <br />
                                          <div class="graph-wrapper" style="margin-left:-40px;">
                                                 <ul class="image_wrapper">
                                                        <li>
                                                               <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$campaign_logo}" class="image-link" title="{$campaign_logo}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$campaign_logo}&w=150" alt=""></a><br>
                                                        </li>
                                                 </ul>
                                          </div>
                                          <div class="clear"></div>
                                          {/if}
                                   </div>

                                   <div class="form-group">
                                          <label>Campaign Background : </label>
                                          <input type="file" name="campaign_background" >
                                          {if $campaign_background != ''}
                                          <br />
                                          <div class="graph-wrapper" style="margin-left:-40px;">
                                                 <ul class="image_wrapper">
                                                        <li>
                                                               <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$campaign_background}" class="image-link" title="{$campaign_background}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$campaign_background}&w=150" alt=""></a><br>
                                                        </li>
                                                 </ul>
                                          </div>
                                          <div class="clear"></div>
                                          {/if}
                                   </div>
                            </div>
                     </div>

              </div>

              <div class="row-fluid">
                     <div class="col-xs-12 col-md-12">
                            <div class="form-group">
                                   <label>Campaign Description : </label>
                                   <textarea name="campaign_description" class="form-control">{$campaign_description}</textarea>
                            </div>
                     </div>
              </div>

              <div class="row-fluid">
                     <div class="col-lg-6">
                            <div class="form-group">
                                   <label>Image 1 : </label>
                                   <input type="file" name="image_1" >
                                   {if $image_1 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_1}" class="image-link" title="{$image_1}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_1}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 2 : </label>
                                   <input type="file" name="image_2" >
                                   {if $image_2 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_2}" class="image-link" title="{$image_2}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_2}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 3 : </label>
                                   <input type="file" name="image_3" >
                                   {if $image_3 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_3}" class="image-link" title="{$image_3}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_3}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 4 : </label>
                                   <input type="file" name="image_4" >
                                   {if $image_4 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_4}" class="image-link" title="{$image_4}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_4}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 5 : </label>
                                   <input type="file" name="image_5" >
                                   {if $image_5 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_5}" class="image-link" title="{$image_5}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_5}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 6 : </label>
                                   <input type="file" name="image_6" >
                                   {if $image_6 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_6}" class="image-link" title="{$image_6}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_6}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 7 : </label>
                                   <input type="file" name="image_7" >
                                   {if $image_7 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_7}" class="image-link" title="{$image_7}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_7}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 8 : </label>
                                   <input type="file" name="image_8" >
                                   {if $image_8 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_8}" class="image-link" title="{$image_8}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_8}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 9 : </label>
                                   <input type="file" name="image_9" >
                                   {if $image_9 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_9}" class="image-link" title="{$image_9}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_9}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                     </div>

                     <div class="col-lg-6">

                            <div class="form-group">
                                   <label>Image 10 : </label>
                                   <input type="file" name="image_10" >
                                   {if $image_10 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_10}" class="image-link" title="{$image_10}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_10}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 11 : </label>
                                   <input type="file" name="image_11" >
                                   {if $image_11 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_11}" class="image-link" title="{$image_11}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_11}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 12 : </label>
                                   <input type="file" name="image_12" >
                                   {if $image_12 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_12}" class="image-link" title="{$image_12}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_12}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 13 : </label>
                                   <input type="file" name="image_13" >
                                   {if $image_13 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_13}" class="image-link" title="{$image_13}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_13}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 14 : </label>
                                   <input type="file" name="image_14" >
                                   {if $image_14 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_14}" class="image-link" title="{$image_14}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_14}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 15 : </label>
                                   <input type="file" name="image_15" >
                                   {if $image_15 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_15}" class="image-link" title="{$image_15}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_15}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 16 : </label>
                                   <input type="file" name="image_16" >
                                   {if $image_16 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_16}" class="image-link" title="{$image_16}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_16}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 17 : </label>
                                   <input type="file" name="image_17" >
                                   {if $image_17 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_17}" class="image-link" title="{$image_17}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_17}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                            <div class="form-group">
                                   <label>Image 18 : </label>
                                   <input type="file" name="image_18" >
                                   {if $image_18 != ''}
                                   <br />
                                   <div class="graph-wrapper">
                                          <ul class="image_wrapper">
                                                 <li>
                                                        <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_18}" class="image-link" title="{$image_18}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/{$image_18}&w=150" alt=""></a><br>
                                                 </li>
                                          </ul>
                                   </div>
                                   <div class="clear"></div>
                                   {/if}
                            </div>

                     </div>
              </div>



              <div class="row-fluid">

                     <div class="col-lg-12">
                            <div class="form-group">
                                   <label>Status : </label>
                                   <br />
                                   {html_radios labels='0' name='enabled' options=$status_options selected=$enabled separator='<br />'}
                            </div>
                            <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" style="margin-left:15px;margin-top:10px;"/>
                            <input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" style="margin-left:5px;margin-top:10px;"/>
                     </div>
              </div>
       </form>

       {literal}
       <script>CKEDITOR.replace('campaign_description');</script>
       <script type="text/javascript">
              function sent_onshow(value)
              {
                     $(".close_all").hide();
                     $("#show_this_" + value).show();
              }

              $(document).ready(function () {
                     $('#project_type').change(function () {
                            if ($(this).val() == "private") {
                                   $('#private_password').show();
                            } else {
                                   $('#private_password').hide();
                            }
                     }).change();
              });
       </script>
       {/literal}
