<!--=========Forms=========-->
<form action="index.php" method="POST">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
  <fieldset>
    <legend>Themes Information</legend>
    <p>
      <label>Choose Themes:</label>
      <select name="themes_id" id="select">
      	{section name=a loop=$themeslist}
        <option value="{$themeslist[a].themes_id}" {if $themeslist[a].themes_primary == 1} selected="selected" {/if} >{$themeslist[a].themes_name}</option>
        {/section}
      </select>
      <br />
      <span class="form_hint">themes will be automaticly changes after change page.</span>
    </p>
    <p>
      <input name="button2" type="submit" class="button2" id="button2" value="Submit" />
      <input name="button" type="reset" class="button" id="button" value="Reset" />
    </p>
    </fieldset>
</form>