{literal}
<script type="text/javascript">

function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected group?")) 
	{
		/*runloading();*/
		mainform.subaction.value = "DeleteSystem";
		mainform.submit();
	}
}

</script>
{/literal}


<form method="POST" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="20%">System Name</th>
      <th width="45%">Description</th>
      <th width="30%">Delete</th>
    </tr>
  </thead>
  <tbody>
    {section name=outer loop=$list}
    <tr class="even">
      <td>{$smarty.section.outer.index+1}</td>
      <td><a href="index.php?t=&session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditSystem&id={$list[outer].system_id}" title="Edit System [{$list[outer].name}]">{$list[outer].name}</a></td>
      <td>{$list[outer].description|nl2br}</td>
      <td>
      	{if $list[outer].protected == 1}
        <b>Protected</b>
        {elseif $list[outer].total_modules > 0}
        <b>{$list[outer].total_modules} module(s)</b>
        {else}
        <input type="checkbox" name="list_id[]" value="{$list[outer].system_id}" />
        {/if}
      </td>
    </tr>
    {/section}
  </tbody>
  
</table>
</div>

<p>
<span class="form_hint">System can be deleted when <b>no module</b> and <b>unprotected</b>.</span>
</p>
<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>

{$pager}