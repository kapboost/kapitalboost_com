<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateDataForm" />
<input type="hidden" name="media_id" value="{$media_id}">

<legend>Media Form</legend>
<div class="form-group">
	<label>Media Name : </label>
	<input type="text" name="media_name" value="{$media_name}" class="form-control">
</div>

<div class="form-group">
	<label>Media Description : </label>
	<textarea name="media_description" class="form-control">{$media_description}</textarea>
</div>

<div class="form-group">
	<label>Release Date : </label>
	<input type="text" name="release_date" id="datepicker" value="{$release_date|date_format:"%d/%m/%Y"}" class="form-control">
</div>

<div class="form-group">
	<label>Expiry Date : </label>
	<input type="text" name="expiry_date" id="datepicker" value="{$expiry_date|date_format:"%d/%m/%Y"}" class="form-control">
</div>

<div class="form-group">
	<label>Classification : </label>
	<input type="text" name="classification" value="{$classification}" class="form-control">
</div>

<div class="form-group">
	<label>Media Image : </label>
	<input type="file" name="img" >
	{if $image != ''}
	<br />
	<div class="graph-wrapper" style="margin-left:-40px;">
		<ul class="image_wrapper">
			<li>
				<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/media/{$image}" class="image-link" title="{$media_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/media/{$image}&w=150" alt=""></a><br>
			</li>
		</ul>
	</div>
	<div class="clear"></div>
	{/if}
</div>

<div class="form-group">
	<label>Media URL : </label>
	<input type="text" name="media_url" value="{$media_url}" class="form-control">
</div>

<div class="form-group">
	<label>Status : </label>
	<br />
	{html_radios labels='0' name='enabled' options=$status_options selected=$enabled separator='<br />'}
</div>


<br />
<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
</form>