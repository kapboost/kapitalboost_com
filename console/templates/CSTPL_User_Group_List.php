{literal}
<script type="text/javascript">

function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected group?")) 
	{
		/*runloading();*/
		mainform.subaction.value = "DeleteUserGroup";
		mainform.submit();
	}
}

</script>
{/literal}


<form method="POST" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th>User group</th>
				<th>Description</th>
				<th>Delete</th>
			</tr>
		</thead>
		<tbody>
			{section name=outer loop=$list}
			<tr>
				<td><a href="index.php?t=&session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditUserGroup&id={$list[outer].usergroup_id}" title="Edit User Group [{$list[outer].name}]">{$list[outer].name}</a></td>
				<td>{$list[outer].description}</td>
				<td>
					{if $list[outer].protected == 1}
					<b>Protected</b>
					{else}
					<input type="checkbox" name="list_id[]" value="{$list[outer].usergroup_id}" />
					{/if}
				</td>
			</tr>
			{/section}
		</tbody>
	</table>
</div>

<p>
<span class="form_hint">usergroup can be deleted when <b>unprotected</b>.</span>
</p>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>
{$pager}


