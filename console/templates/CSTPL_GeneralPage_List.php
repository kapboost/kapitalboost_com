{literal}
<script type="text/javascript">
function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected page?")){
		/*runloading();*/
		mainform.subaction.value = "DeleteData";
		mainform.submit();
	}
}
</script>
{/literal}

<form method="POST" class="form-inline" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<legend>Page List</legend>
<!---------------Search------------->
<!--div class="form-group form-inline">
	<input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Name">
	<input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
</div-->
<!---------------Search------------->
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="25%">Page Name</th>
			<th width="25%">Release Date</th>
		</tr>
	</thead>
	<tbody>
    {section name=outer loop=$list}
		<tr class="even">
			<td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditData&page_id={$list[outer].page_id}">{$list[outer].page_name}</a></td>
			<td>{$list[outer].release_date|date_format:"%d/%m/%Y"}</td>
		</tr>
	{sectionelse}
		<tr class="even">
			<td colspan="4" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
	</tbody>
</table>
</div>

</form>

{$pager}