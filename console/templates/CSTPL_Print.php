<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta charset="UTF-8" />
<title>Order</title>
<link href="assets/print/bootstrap.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="../assets/print/bootstrap.min.js"></script>
<link href="assets/print/font-awesome.min.css" type="text/css" rel="stylesheet" />
<link type="text/css" href="../assets/print/stylesheet.css" rel="stylesheet" media="all" />
</head>

<body>
<div class="container">
    <div style="page-break-after: always;">
    <h1>Order #{$order.transaction_id}</h1>
    <table class="table table-bordered">
		<thead>
			<tr>
				<td colspan="2"><strong>Order Details</strong></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="width: 50%;">
					<address>
						<strong>Kim'S Florist & Floral Supplies</strong><br />
						No. 1, Jalan Sungai Chat, Komplek Mawar, 80100<br />
						Johor Bahru, Johor, 80100<br />
						Malaysia
					</address>
					<b>Telephone:</b> +607-2243663<br />
					<b>E-Mail:</b> enquiry@kimsflorist.com.my<br />
					<b>Web Site:</b> <a href="http://www.kimsflorist.com.my">http://www.kimsflorist.com.my</a>
				</td>
				<td style="width: 50%;">
					<b>Date Added:</b> {$order.insert_dt|date_format:$smarty.const.SMARTY_DATETIME_FORMAT}<br />
					<b>Order ID:</b> {$order.transaction_id}<br />
					<b>Payment Method:</b> {$order.payment_method}<br />
					<b>Shipping Method:</b> 
					{if $delivery.self_collect == 1}
					Self Collect
					{else}
					Delivery
					{/if}
					<br />
					<b>Delivery Date</b> {$delivery.delivery_date_from|date_format:$smarty.const.SMARTY_DATE_FORMAT}<br />
					<b>Delivery Time</b>
					{if $delivery.delivery_time == 1}
					9am - 2pm
					{elseif $delivery.delivery_time == 2}
					2pm - 6pm
					{/if}
					<br />
				</td>
			</tr>
		</tbody>
    </table>
	
    <table class="table table-bordered">
		<thead>
			<tr>
				<td style="width: 50%;"><b>Billing Address</b></td>
				<td style="width: 50%;"><b>Delivery Address</b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<address>
						{$billing.billing_givenname}<br />
						{if $billing.billing_unit ne ""}{$billing.billing_unit},{/if} {$billing.billing_street_1} {$billing.billing_postal_code}<br>
						{if $billing.billing_city ne ""}{$billing.billing_city},{/if} {$billing.billing_state}<br>
						{$billing.billing_country}<br/> 
						Contact. {$billing.billing_contact_no}<br />
						Mobile. {$billing.billing_mobile_no}<br />
						E-mail. {$billing.billing_email}<br />
					</address>
				</td>
				<td>
					<address>
						{$delivery.delivery_givenname}<br />
						{if $delivery.delivery_unit ne ""}{$delivery.delivery_unit},{/if} {$delivery.delivery_street_1} {$delivery.delivery_postal_code}<br>
						{if $delivery.delivery_city ne ""}{$delivery.delivery_city},{/if} {$delivery.delivery_state} <br />
						{$delivery.delivery_country} <br />
						Contact. {$delivery.delivery_contact_no}<br>
						Mobile. {$delivery.delivery_mobile_no}<br>
						E-mail. {$delivery.delivery_email}<br>
					</address>
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<td><b>Product Name</b></td>
				<td><b>Product Description</b></td>
				<td class="text-right"><b>Quantity</b></td>
				<td class="text-right"><b>Unit Price</b></td>
				<td class="text-right"><b>Total</b></td>
			</tr>
		</thead>
		<tbody>
			{section name=a loop=$cart}
			<tr>
				<td>{$cart[a].product_name}</td>
				<td>{$cart[a].product_description}</td>
				<td class="text-right">{$cart[a].product_quantity}</td>
				<td class="text-right">RM {$cart[a].product_final_price}</td>
				<td class="text-right">RM {$cart[a].subtotal|number_format:2}</td>
			</tr>
			{/section}
			
			<tr>
				<td class="text-right" colspan="4"><b>Sub-Total</b></td>
				<td class="text-right">RM {$order.subtotal}</td>
			</tr>
			<tr>
				<td class="text-right" colspan="4"><b>Shipping Charge</b></td>
				<td class="text-right">RM {$order.total_shipping_charge}</td>
			</tr>
			<tr>
				<td class="text-right" colspan="4"><b>GST</b></td>
				<td class="text-right">RM {$order.total_gst}</td>
			</tr>
			<tr>
				<td class="text-right" colspan="4"><b>Total</b></td>
				<td class="text-right">RM {$order.grand_total}</td>
			</tr>
		</tbody>
	</table>
	</div>
</div>
</body>
</html>