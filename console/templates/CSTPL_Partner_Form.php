<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data" style="text-align: left;margin: 0 0 25px 0;">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateDataForm" />
<input type="hidden" name="partner_id" value="{$partner_id}">

<legend>Partner Form</legend>
<div class="form-group">
	<label>Partner Name : </label>
	<input type="text" name="partner_name" value="{$partner_name}" class="form-control">
</div>

<div class="form-group">
	<label>Partner Description : </label>
	<textarea name="partner_description" class="form-control">{$partner_description}</textarea>
</div>

<div class="form-group">
	<label>Image : </label>
	<input type="file" name="img" >
	{if $partner_image != ''}
	<br />
	<div class="graph-wrapper" style="margin-left:-40px;">
		<ul class="image_wrapper">
			<li>
				<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/partner/{$partner_image}" class="image-link" title="{$partner_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/partner/{$partner_image}&w=150" alt=""></a><br>
     	<!--

        <a href="" title="Delete Image" del-image="1" class="del-btn">[x] Delete</a><br>

        -->
			</li>
		</ul>
	</div>
	<div class="clear"></div>
	{/if}
</div>

<div class="form-group">
	<label>Partner url : </label>
	<input type="text" name="partner_url" value="{$partner_url}" class="form-control">
</div>

<div class="form-group">
	<label>Status : </label>
	<br />
	{html_radios labels='0' name='enabled' options=$status_options selected=$enabled separator='<br />'}
</div>

<br />
<div style="text-align: center;">
<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
</div>
</form>