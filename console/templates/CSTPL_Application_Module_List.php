{literal}
<script type="text/javascript">

function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected group?")) 
	{
		/*runloading();*/
		mainform.subaction.value = "DeleteModule";
		mainform.submit();
	}
}

</script>
{/literal}


<form method="POST" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="20%">System Name</th>
      <th width="45%">Module Name</th>
      <th width="30%">Delete</th>
    </tr>
  </thead>
  <tbody>
    {section name=outer loop=$list}
    <tr class="even">
      <td>{$smarty.section.outer.index+1}</td>
      <td>{$list[outer].system_name}</td>
      <td><a href="index.php?t=&session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditModule&id={$list[outer].module_id}" title="Edit Module [{$list[outer].module_name}]">{$list[outer].module_name}</td>
      <td>
      	{if $list[outer].protected == 1}
        <b>Protected</b>
        {elseif $list[outer].total_action > 0}
        <b>{$list[outer].total_action} action(s)</b>
        {else}
        <input type="checkbox" name="list_id[]" value="{$list[outer].module_id}" />
        {/if}
      </td>
    </tr>
    {/section}
  </tbody>
  
</table>
</div>
<p>
<span class="form_hint">Module can be deleted when <b>no action</b> and <b>unprotected</b>.</span>
</p>
<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>

{$pager}