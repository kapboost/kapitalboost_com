{literal}
<script type="text/javascript">
$(document).ready(function(){
	$("#insert_dt").datepicker({
		dateFormat: "{/literal}{$smarty.const.DIS_DATEPICKER_FORMAT}{literal}",
		changeMonth: true,
		numberOfMonths: 1
	});
});
function getFiltered(){
	var insert_dt = '&insert_dt='+$('#insert_dt').val();
	var log_tag = '&log_tag='+$('#log_tag').val();
	var log_action = '&log_action='+$('#log_action').val();
	var log_msg = '&log_msg='+$('#log_msg').val();
	var login_id = '&login_id='+$('select[name="login_id"]').val();
	var remote_ip = '&remote_ip='+$('#remote_ip').val();
    
	var ref_link = '{/literal}{$mod_link}{literal}'+'&action=ListLog'+insert_dt+log_tag+log_action+log_msg+login_id+remote_ip;
    window.location.replace(ref_link);
}
</script>
{/literal}

<form class="form-inline" role="form">
<div>
  <label>Search Criterial :</label>
  <input class="form-control" type="text" id="insert_dt" placeholder="Date" name="insert_dt" value="{$insert_dt}" size="12" />
  <input class="form-control" type="text" id="log_tag" placeholder="Tag Module" name="log_tag" value="{$log_tag}" />
  <input class="form-control" type="text" id="log_action" placeholder="Tag Action" name="log_action" value="{$log_action}" />
  <input class="form-control" type="text" id="log_msg" placeholder="Msg Info" name="log_msg" value="{$log_msg}" />
  <select name="login_id" class="selectpicker" id="banner-of-vo">
	<option value="">--User Login--</option>
  {section name=x loop=$id_list}
	<option {if $login_id eq $id_list[x].login_id }selected{/if} value="{$id_list[x].login_id}">{$id_list[x].console_user}</option>
  {/section}
  </select>
  <input class="form-control" type="text" id="remote_ip" placeholder="Remote IP" name="remote_ip" value="{$remote_ip}" />
  <input type="button" class="btn btn-primary go" name="go" value="Go" onclick="getFiltered()" /> 
</div>
<br />
</form>

<div class="table-responsive">
	<table class="table table-striped table-bordered table-hover">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th width="20%">Member Name</th>
				<th width="10%">Member Email</th>
				<th width="15%">Project Name</th>
				<th width="20%">Message</th>
				
			</tr>
		</thead>
		<tbody>
		{section name=outer loop=$list}
			<tr class="even">
				<td>{$smarty.section.outer.index+1}</td>
				<td>{$list[outer].name}</td>
				<td>{$list[outer].email}</td>

<td>{$list[outer].project}</td>
<td>{$list[outer].msg}</td>
				
			</tr>
		{sectionelse}
			<tr class="even">
				<td colspan=7>
					<center><i>No Record Found ...</i></center>
				</td>
			</tr>
		{/section}
		</tbody>
	</table>
</div>

{$pager}