{literal}
<script type="text/javascript">

function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected group?")) 
	{
		/*runloading();*/
		mainform.subaction.value = "DeleteUser";
		mainform.submit();
	}
}

</script>
{/literal}


<form method="POST" action="index.php" role="form" class="form-inline">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="30%">User Group</th>
      <th width="35%">User Name</th>
      <th width="30%">Delete</th>
    </tr>
  </thead>
  <tbody>
    {section name=outer loop=$list}
    <tr class="even">
      <td>{$smarty.section.outer.index+1}</td>
      <td>{$list[outer].name}</td>
      <td><a href="index.php?t=&session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditUser&id={$list[outer].login_id}" title="Edit User [{$list[outer].console_user}]">{$list[outer].console_user}</a></td>
      <td>
      	{if $login_id == $list[outer].login_id}
        <b>Protected</b>
        {else}
        <input type="checkbox" name="list_id[]" value="{$list[outer].login_id}" />
        {/if}
      </td>
    </tr>
    {/section}
  </tbody>
  
</table>
</div>
<p>
<span class="form_hint">Login user cannot delete own login ID, user can be deleted when <b>unprotected</b>.</span>
</p>
<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>
</form>

{$pager}