<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="ico/favicon.ico">
    
    <title>Login</title>
    
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    
    <link href="css/font-import.css" rel="stylesheet">
    <link href="css/admin-panel.css" rel="stylesheet">
    <link href="css/login.css" rel="stylesheet" media="screen">
    <link href="http://checkinjakarta.id/backend/css/animate.css" rel="stylesheet" media="screen">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
      
    {literal}
    <script type="text/javascript">
    
    $(function(){
        $('#forgotpassword_button').click(function(){
    
            var email=prompt("Please input your registered account email","");
            
            if (email!=null)
            {
              forgot_email(email);
            }
            
        });
    });
    
    function forgot_email( email ){
        str = 'email='+email;
        
        $.ajax({
            url: "forgotpassword.php",
            type: "POST",
            dataType: "html",
            async: false,
            data: str,
            success: handleResponseSendMail
        });
    
    }
    
    
    function handleResponseSendMail(html){
        alert(html);
    }
  
    
    </script>
    {/literal}
</head>

<body role="document" style="background: #e9e5dc;overflow: hidden;">
    <!-------------------------------------------- content area -------------------------------------------->
    <div class="main-content-area-login-wrapper container-fluid">

<!-- Start login new -->             
    <div id="box" class="animated bounceIn">
        <div class="top" style="padding-bottom: 10px; line-height: 15px;">
            <img src="http://checkinjakarta.id/images/GTI-White.png" class="logo" width="40" style="float: left">
            <a class="right_a btn-forgot-password" href="#" style="color: #eec104; font-family: Helvetica;"><i>Forgot Password?</i></a>
            <div style="padding-top: 5px">
                <div><span class="texttop" style="margin-top: 10px">Globe Teledigital Indonesia</span></div>
                <div><span class="texttop" style="color:#EEC104 ">ver 0.1</span></div>
            </div>
        </div>
        <div id="top_header" style="padding-bottom: 40px">
            <a href="#">
                <img class="logo" src="https://kapitalboost.com/assets/images/logo/kapitalboost-logo.png" width="230" alt="logo">
            </a>
            <h5>
                <i>Welcome to kapitalboost console!</i>
            </h5>
        </div>    
        <div id="inputs">
            <div id="bottom">
                                            <p style="text-align: center;">
                                              {$message_error}
                                            </p>
<br />
                       <p style="text-align: center;">
                                                Click the button below to go back to login page.
                                                <br /><br />
                                                <a href="index.php?t={$time}&session={$session}"><input name="button2" type="submit" class="btn  btn-primary btn-lg" value="Back to login" />
                       </p>
            </div>
        </div>
    </div>
</form>
<!-- End login new -->


    <!--<img src="images/page-login-bg.jpg" class="bg-cover-page">-->
    	<!--<div class="bg-cover-page"></div>-->
<!--
        <div class="login-panel-wrapper center-block">
        	<h1 style="color:#fff;"></h1>
        	<div class="login-panel-container">
-->
            	
            	<!-------------------------------------- left-panel -------------------------------------->
                <div class="login-left-panel-wrapper">
                	<!------------------------------- logo small device ------------------------------->
                    <div class="login-logo-small-dev-wrapper"><img src="../assets/images/kapitalboost-logo.png"></div>
                    <!------------------------------- //logo small device ------------------------------->
                    <!------------------------------- login form ------------------------------->
<!--
                	<div class="login-left-panel-container">
                    	<div class="panel-inner">
                        	<h3 style="margin-top:0;"></h3>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="panel-inner"> 
                                            <p>
                                              {$message_error}
                                            </p>
                                            <p>
                                                Click the button below to go back to login page.
                                                <br />
                                                <a href="index.php?t={$time}&session={$session}"><input name="button2" type="submit" class="btn btn-primary btn-lg" value="Back to login" /></p>
                                            </p>
                                    </div>
                                </div>
                            </div>
                        </div>
            		</div>
-->
                    <!------------------------------- //login form ------------------------------->
                    <!------------------------------- login btns small device ------------------------------->
<!--
                    <div class="login-btn-small-dev-wrapper">
                    	<span><a href="" class="btn-forgot-password">FORGOT PASSWORD?</a></span>
                            <span><a href="" class="btn-need-help">NEED HELP?</a></span>
                    </div>
-->
                    <!------------------------------- //login btns small device ------------------------------->
            	</div>
            	<!-------------------------------------- //left-panel -------------------------------------->
                <!-------------------------------------- right-panel -------------------------------------->
<!--
                <div class="login-right-panel-wrapper">
                	<div class="login-right-panel-container">
                    	<div class="login-logo-wrapper"><img src="../assets/images/kapitalboost-logo.png"></div>
                        <div class="login-btns-wrapper">

                        </div>
            		</div>
            	</div>
-->
            	<!-------------------------------------- //right-panel -------------------------------------->
                <div class="clearfix"></div>
            </div>
        </div>
        
    </div>
    <!-------------------------------------------- //content area -------------------------------------------->









	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-ui-1.10.4.min.js"></script>
    <script src="js/bootstrap-select.js" type="text/javascript"></script>
    <link href="css/bootstrap-select.css" rel="stylesheet" >
    
    <script>
		$('.selectpicker').selectpicker();
	</script>
</body>
</html>