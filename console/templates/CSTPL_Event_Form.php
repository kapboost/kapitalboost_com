<script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateDataForm" />
<input type="hidden" name="event_id" value="{$event_id}">

<legend>Event Form</legend>
<div class="form-group">
	<label>Event Name : </label>
	<input type="text" name="event_name" value="{$event_name}" class="form-control">
</div>

<div class="form-group">
	<label>Event Description : </label>
	<textarea name="event_description" class="form-control">{$event_description}</textarea>
</div>

<div class="form-group">
	<label>Release Date : </label>
	<input type="text" name="release_date" id="datepicker" value="{$release_date|date_format:"%d/%m/%Y"}" class="form-control">
</div>

<div class="form-group">
	<label>Expiry Date : </label>
	<input type="text" name="expiry_date" id="datepicker" value="{$expiry_date|date_format:"%d/%m/%Y"}" class="form-control">
</div>

<div class="form-group">
	<label>Location : </label>
	<input type="text" name="location" value="{$location}" class="form-control">
</div>

<div class="form-group">
	<label>Speakers : </label>
	<input type="text" name="speakers" value="{$speakers}" class="form-control">
</div>

<div class="form-group">
	<label>Fee : </label>
	<input type="text" name="fee" value="{$fee}" class="form-control">
</div>

<div class="form-group">
	<label>Ticket Info : </label>
	<input type="text" name="ticket_info" value="{$ticket_info}" class="form-control">
</div>

<div class="form-group">
	<label>Event Image : </label>
	<input type="file" name="img" >
	{if $event_image != ''}
	<br />
	<div class="graph-wrapper" style="margin-left:-40px;">
		<ul class="image_wrapper">
			<li>
				<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/event/{$event_image}" class="image-link" title="{$event_image}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/event/{$event_image}&w=150" alt=""></a><br>
			</li>
		</ul>
	</div>
	<div class="clear"></div>
	{/if}
</div>

<br />
<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />

</form>

{literal}
<script>CKEDITOR.replace('event_description');</script>
<script type="text/javascript">
function sent_onshow(value)
{
	$(".close_all").hide();
	$("#show_this_"+value).show();
}
</script>
{/literal}