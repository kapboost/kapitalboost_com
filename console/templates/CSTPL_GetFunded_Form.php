<script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateDataForm" />
<input type="hidden" name="campaign_id" value="{$campaign_id}">

<legend>Get Funded Form</legend>

<div class="col-xs-12 col-md-6">

<div class="form-group">
		<label>Campaign Owner : </label>
		<input type="text" name="fullname" value="{$owner}" class="form-control" readonly>
	</div>
	<div class="form-group">
		<label>Full Name : </label>
		<input type="text" name="fullname" value="{$fullname}" class="form-control" readonly>
	</div>

	<div class="form-group">
		<label>Email Address : </label>
		<input type="text" name="email" value="{$email}" class="form-control" readonly>
	</div>

	<div class="form-group">
		<label>Mobile Number : </label>
		<input type="text" name="mobile" id="mobile" value="{$mobile}" class="form-control" readonly>
	</div>

	<div class="form-group">
		<label>Country : </label>
		<input type="text" name="country" value="{$country}" class="form-control" readonly>
	</div>

	<div class="form-group">
		<label>Company : </label>
		<input type="text" name="company" value="{$company}" class="form-control"readonly>
	</div>

	<div class="form-group">
		<label>Industry : </label>
		<input type="text" name="industri" value="{$industri}" class="form-control" readonly>
	</div>

	<div class="form-group">
		<label>Year Established : </label>
		<input type="text" name="year" value="{$year}" class="form-control" readonly>
	</div>
	
</div>

<div class="col-xs-12 col-md-6">
	<div class="form-group">
		<label>Currency : </label>
		<input type="text" name="currency" value="{$currency}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Project Type : </label>
		<input type="text" name="project_type" value="{$project_type}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Est Annual Revenue  : </label>
		<input type="text" name="est_an_rev" value="{$est_an_rev}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Funding Amount Required : </label>
		<input type="text" name="funding_amount" value="{$funding_amount}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Assets To Be Purchased : </label>
		<input type="text" name="ass_tobe_pur" value="{$ass_tobe_pur}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Tenor : </label>
		<input type="text" name="funding_period" value="{$funding_period}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Have purchase orders  : </label>
		<input type="text" name="have_purchased_order" value="{$have_purchased_order}" class="form-control" readonly>
	</div>
        <div class="form-group">
		<label>Purchase Orders  : </label>
		<input type="text" name="have_purchased_order" value="{$purchased_order}" class="form-control" readonly>
	</div>
	
	<div class="form-group">
		<label>Attachment : </label>
		<a href="../{$attach_file}"target="_blank">Download</a>
	</div>
</div>

<br />
<!--<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" style="margin-left:15px;margin-top:10px;"/>
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" style="margin-left:5px;margin-top:10px;"/>-->
</form>
