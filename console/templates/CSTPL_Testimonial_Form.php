<script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateDataForm" />
<input type="hidden" name="testimonial_id" value="{$testimonial_id}">

<legend>Testimonial Form</legend>
<div class="form-group">
    <label>Testimonial Author : </label>
    <input type="text" name="testimonial_subject" value="{$testimonial_subject}" class="form-control">
</div>
<input type="hidden" name="div_atas" value="<div class='home-5-testimonial item'>
        <p class='testi-content'>" />
        <input type="hidden" name="tutup_p" value="</p>" />
        <input type="hidden" name="person" value="<p class='testi-person'>" />
        <input type="hidden" name="div_bawah" value="<center>
<div style='width:100px;height:100px;border:2px solid white;border-radius: 100%;'></div>
</center>" />

<div class="form-group">
    <label>Testimonial Content : </label>
    <textarea name="testimonial_content" class="form-control">{$testimonial_content}</textarea>
</div>

<div class="form-group">
    <label>Testimonial Image : </label>
    <input type="file" name="img" >
    {if $testimonial_image != ''}
    <br />
    <div class="graph-wrapper" style="margin-left:-40px;">
        <ul class="image_wrapper">
            <li>
                <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/testimonial/{$testimonial_image}" class="image-link" title="{$testimonial_image}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/testimonial/{$testimonial_image}&w=150" alt=""></a><br>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
    {/if}
</div>

<div class="form-group">
    <label>Status : </label>
    <br />
    {html_radios labels='0' name='testimonial_status' options=$testimonial_status_options selected=$testimonial_status separator='<br />'}
</div>

<br />
<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
</form>

{literal}
<script>CKEDITOR.replace('testimonial_content');</script>
<script type="text/javascript">
function sent_onshow(value)
{
    $(".close_all").hide();
    $("#show_this_"+value).show();
}
</script>
{/literal}