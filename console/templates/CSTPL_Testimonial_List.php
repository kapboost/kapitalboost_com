{literal}
<script type="text/javascript">
function confirmDeletion(mainform)
{
	if(confirm("Are you sure you want to delete the selected testimonial?")){
		/*runloading();*/
		mainform.subaction.value = "DeleteData";
		mainform.submit();
	}
}
</script>
{/literal}

<form method="POST" class="form-inline" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<legend>Testimonial List</legend>
<!---------------Search------------->
<!--div class="form-group form-inline">
	<input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Subject">
	<input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
</div-->
<!---------------Search------------->
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">No</th>
			<th width="25%">Testimonial Subject</th>
			<th width="25%">Testimonial Content</th>
			<th width="25%">Testimonial Image</th>
			<th width="8%">Status</th>
			<th width="*">Created Date</th>
		</tr>
	</thead>
	<tbody>
    {section name=outer loop=$list}
		<tr class="even">
			<td><input type="checkbox" name="list_id[]" value="{$list[outer].testimonial_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
			<td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditData&testimonial_id={$list[outer].testimonial_id}">{$list[outer].testimonial_subject}</a></td>
			<td>{$list[outer].testimonial_content}</td>
			<td>
				{if $list[outer].testimonial_image != ''}
					<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/testimonial/{$list[outer].testimonial_image}" class="image-link" title="{$testimonial_subject}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/testimonial/{$list[outer].testimonial_image}&w=150" alt=""></a><br>
				{/if}
			</td>
			<td>
				{if $list[outer].testimonial_status == '1'}
					Online
				{else}
					Offline
				{/if}
			</td>
			<td>{$list[outer].insert_dt|date_format:$smarty.const.SMARTY_DATETIME_FORMAT}</td>
		</tr>
	{sectionelse}
		<tr class="even">
			<td colspan="9" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
	</tbody>
</table>
</div>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>

</form>

{$pager}

