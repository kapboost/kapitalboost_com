{literal}
<script type="text/javascript">
function confirmDeletion(mainform) 
{
	if(confirm("Are you sure you want to delete the selected event?")){
		/*runloading();*/
		mainform.subaction.value = "DeleteData";
		mainform.submit();
	}
}
</script>
{/literal}

<form method="POST" class="form-inline" action="index.php">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="subaction" value="">
<legend>Event List</legend>
<!---------------Search------------->
<!--div class="form-group form-inline">
	<input type="text" name="keyword" class="form-control" value="{$keyword}"  placeholder="Name">
	<input name="Search" type="submit" class="btn btn-primary" id="button" value="Search">
</div-->
<!---------------Search------------->
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th width="5%">No</th>
			<th width="25%">Event Name</th>
			<th width="10%">Release Date</th>
			<th width="10%">Expiry Date</th>
			<th width="10%">Location</th>
			<th width="25%">Speakers</th>
			<th width="25%">Fee</th>
			<th width="25%">Ticket Info</th>
			<th width="40%">Event Image</th>
		</tr>
	</thead>
	<tbody>
    {section name=outer loop=$list}
		<tr class="even">
			<td><input type="checkbox" name="list_id[]" value="{$list[outer].event_id}" />&nbsp;{$smarty.section.outer.index+1}</td>
			<td><a href="index.php?session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=EditData&event_id={$list[outer].event_id}">{$list[outer].event_name}</a></td>
			<td>{$list[outer].release_date}</td>
			<td>{$list[outer].expiry_date}</td>
			<td>{$list[outer].location}</td>
			<td>{$list[outer].speakers}</td>
			<td>{$list[outer].fee}</td>
			<td>{$list[outer].ticket_info}</td>
			<td>
			{if $list[outer].event_image != ''}
				<a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/event/{$list[outer].event_image}" class="image-link" title="{$event_name}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/event/{$list[outer].event_image}&w=150" alt=""></a><br>
			{/if}
			</td>
			<!--td>{$list[outer].event_url}</td-->
		</tr>
	{sectionelse}
		<tr class="even">
			<td colspan="11" align="center"><i>Data not found...<i></td>
		</tr>
    {/section}
	</tbody>
</table>
</div>

<p>
	<input name="Delete" type="button" class="btn btn-danger" id="button2" value="Delete" onclick="confirmDeletion(this.form);" />
</p>

</form>

{$pager}

