<!--=========Forms=========-->
<form action="index.php" method="POST">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="id" value="{$id}">
<legend>System Information</legend>
<div class="form-group">
	<label>System Name : </label>
	<input type="text" name="name" value="{$name}" class="form-control">
</div>
<div class="form-group">
	<label>Description : </label>
	<textarea name="description" rows="5" class="form-control">{$description}</textarea>
</div>
<div class="form-group">
	<label>Ordering : </label>
	<input type="text" name="ordering" value="{$ordering}" class="form-control">
	<span class="form_hint">value : numeric, sorting : low to high.</span>
</div>

      <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
      <input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />

</form>