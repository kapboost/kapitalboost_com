<script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateDataForm" />
<input type="hidden" name="page_id" value="{$page_id}">

<legend>Page Form</legend>
<div class="form-group">
	<label>Page Name : </label>
	<input type="text" name="page_name" value="{$page_name}" class="form-control">
</div>

<div class="form-group">
	<label>Page Content : </label>
	<textarea name="page_content" id="page_content" class="form-control" rows="30">{$page_content}</textarea>
</div>

<div class="form-group">
	<label>Release Date : </label>
	<input type="text" name="release_date" id="datepicker" value="{$release_date|date_format:"%d/%m/%Y"}" class="form-control">
</div>

<br />
<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
</form>

{literal}
<script>CKEDITOR.replace('page_content2');</script>
<script type="text/javascript">
function sent_onshow(value)
{
	$(".close_all").hide();
	$("#show_this_"+value).show();
}
</script>
{/literal}