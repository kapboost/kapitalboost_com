<!--=========Forms=========-->
<form action="index.php" method="POST">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="id" value="{$id}">
<legend>User Information</legend>
<div class="form-group">
	<label>Name : </label>
	<input type="text" name="name" value="{$name}"class="form-control">
</div>
<div class="form-group">
	<label>Email : </label>
	<input type="text" name="email" value="{$email}"class="form-control">
</div>
<div class="form-group">
	<label>Address : </label>
	<textarea name="address" rows="5" class="form-control">{$address}</textarea>
</div>
<div class="form-group">
	<label>City : </label>
	<input type="text" name="city" value="{$city}"class="form-control">
</div>
<div class="form-group">
	<label>State : </label>
	<input type="text" name="state" value="{$state}"class="form-control">
</div>
<div class="form-group">
	<label>Country : </label>
	<input type="text" name="country" value="{$country}"class="form-control">
</div>
<div class="form-group">
	<label>Postal Code : </label>
	<input type="text" name="zip" value="{$zip}"class="form-control">
</div>
<div class="form-group">
	<label>Work Telephone : </label>
	<input type="text" name="work_telephone" value="{$work_telephone}"class="form-control">
</div>
<div class="form-group">
	<label>Fax Telephone : </label>
	<input type="text" name="fax_telephone" value="{$fax_telephone}"class="form-control">
</div>
<div class="form-group">
	<label>Mobile : </label>
	<input type="text" name="mobile" value="{$mobile}"class="form-control">
</div>
<div class="form-group">
	<label>Pager : </label>
	<input type="text" name="pager" value="{$pager}"class="form-control">
</div>
<legend>Login Information</legend>
<div class="form-group">
	<label>Login Name : </label>
	<input type="text" name="console_user" value="{$console_user}" class="form-control">
</div>
<div class="form-group">
	<label>Login Password : </label>
	<input type="password" name="console_password" value="{$console_password}" class="form-control">
</div>
<div class="form-group">
	<label>User Group : </label>
	<select name="select_usergroup_id" id="select" class="form-control">
      	{section name=a loop=$usergrouplist}
        <option value="{$usergrouplist[a].usergroup_id}" {if $select_usergroup_id == $usergrouplist[a].usergroup_id} selected="selected" {/if} >{$usergrouplist[a].name}</option>
        {/section}
    </select>
</div>

<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
<input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />

</form>