<link rel="stylesheet" href="https://kapitalboost.com/assets/css/kp-default.css" />
<link rel="stylesheet" href="https://kapitalboost.com/assets/css/input.css" />
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="_UpdateData" />
<input type="hidden" name="id" value="{$id}">


<legend> Account & Campaign Detail </legend>
<div class="col-xs-12 col-md-6">

    <div class="form-group">
        <label>Name : </label>
        <input type="text" name="fullname" value="{$nama}" class="form-control" readonly>
<input type="hidden" name="nama" value="{$nama}">
    </div>

    <div class="form-group">
        <label>Email Address : </label>
        <input type="text" name="email" value="{$email}" class="form-control" readonly>
                <input type="hidden" name="mail" value="{$email}">
    </div>

    <div class="form-group">
        <label>Country : </label>
        <input type="text" name="country" value="{$country}" class="form-control" readonly>
    </div>

    <div class="form-group">
        <label>Campaign Name : </label>
        <input type="text" name="campaign_name" value="{$campaign}" class="form-control" readonly>
                <input type="hidden" name="campaign" value="{$campaign}">
    </div>

    <div class="form-group">
        <label>Tipe : </label>
        <input type="text" name="tipe" value="{$tipe}" class="form-control" readonly>
    </div>

    <div class="form-group">
        <label>Total Funding : </label>
        <input type="text" name="t_funding" value="{$total_funding}" class="form-control" readonly>
<input type="hidden" name="funding" value="{$total_funding}">
    </div>

     <div class="form-group">
        <label>Bank Name : </label>
        <input type="text" name="bank" value="{$bank}" class="form-control" readonly>
                <input type="hidden" name="tipe" value="{$bank}">
    </div>


</div>
<div class="col-xs-12 col-md-6">
        <div class="form-group">
        <label>Project Type : </label>
        <input type="text" name="project_type" value="{$project_type}" class="form-control" readonly>
                <input type="hidden" name="project" value="{$project_type}">
    </div>
    <div class="form-group">
        <label>Date </label>
        <input type="text" name="date" value="{$date}" class="form-control" readonly>
    </div>
    <div class="form-group">
        <label>Account name</label>
        <input type="text" name="date" value="{$bank_transfer_account_name}" class="form-control" readonly>
    </div>
    <div class="form-group">
        <label>Account number</label>
        <input type="text" name="date" value="{$bank_transfer_account_number}" class="form-control" readonly>
    </div>
<div class="form-group">
        <label>Status Payment :<br /> </label>
{if $status=='Paid'}
        <label class="label label-success"> Paid </label>
{else}
<label class="label label-warning"> Unpaid </label>
{/if}
    </div>
{if $status !='Paid' && $tipe =='Bank Transfer'}
<div class="form-group">
        <label>Update Payment Status  : </label>
        <select name="status">
                <option> Select Status </option>
                <option value="Paid" >Paid </option>
                <option value="Unpaid" selected>Unpaid</option>
</select>
    </div>
{/if}
{if $tipe=='Bank Transfer'}
    <div class="form-group">
        <label>Attachment : </label>
        <a href="/assets/images/bukti/{$bukti}" target="_blank">Download</a>
    </div>
{/if}
{if $bukti  && $tipe=='Bank Transfer'}
   <div class="form-group">
     <label class="label label-success"> New Image Uploaded </label>
</div>
{/if}



<br />
<input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" style="margin-left:15px;margin-top:10px;"/></form>

</div>
</div>
<legend>Payout Schedule </legend>
<div class="month">
<div class="form-group">
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update1" />
<input type="hidden" name="id" value="{$id}">

        <label>Month 1 : </label>
                <div class="monContent">
                     <div class="monItem">
                        <div class="montFormWrap">
                   <input type="text" value="{$bulan_1}" name="Month_1" class="monthForm" placeholder="month 1">

                        </div>
                     </div>
                     {if $tanggal_1 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_1|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_1 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
                     <div class="monItem2">
                         <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
                     </div>
                </div>
            </form>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update2" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 2 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_2}" name="Month_2" class="monthForm" placeholder="month 2" >

                </div>
             </div>
             {if $tanggal_2 > 0 }
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_2|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_2 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update3" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 3 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_3}" name="Month_3" class="monthForm" placeholder="month 3">

                </div>
             </div>
             {if $tanggal_3 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_3|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_3 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update4" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 4 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_4}" name="Month_4" class="monthForm" placeholder="month 4">

                </div>
             </div>
{if $tanggal_4 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_1|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_4 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update5" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 5 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_5}" name="Month_5" class="monthForm" placeholder="month 5" >

                </div>
             </div>
            {if $tanggal_5 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_5|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_5 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update6" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 6 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_6}" name="Month_6" class="monthForm" placeholder="month 6">

                </div>
             </div>
             {if $tanggal_6 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_6|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_6 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update7" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 7 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_7}" name="Month_7" class="monthForm" placeholder="month 7">

                </div>
             </div>
             {if $tanggal_7 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_7|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_7 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update8" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 8 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_8}" name="Month_8" class="monthForm" placeholder="month 8">

                </div>
             </div>
             {if $tanggal_8 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_8|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_8 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update9" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 9 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_9}" name="Month_9" class="monthForm" placeholder="month 9" >

                </div>
             </div>
             {if $tanggal_9 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_9|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_9 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update10" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 10 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_10}" name="Month_10" class="monthForm" placeholder="month 10">

                </div>
             </div>
             {if $tanggal_10 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_10|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_10 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update11" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 11 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_11}"  name="Month_11" class="monthForm" placeholder="month 11">

                </div>
             </div>
             {if $tanggal_11 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_11|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_11 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update12" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 12 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_12}" name="Month_12" class="monthForm" placeholder="month 12" >

                </div>
             </div>
             {if $tanggal_12 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_12|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_12 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update13" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 13 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_13}" name="Month_13" class="monthForm" placeholder="month 13">

                </div>
             </div>
             {if $tanggal_13 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_13|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_13 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update14" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 14 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_14}" name="Month_14" class="monthForm" placeholder="month 14">

                </div>
             </div>
             {if $tanggal_14 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_14|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_14 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update15" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 15 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_15}" name="Month_15" class="monthForm" placeholder="month 15">

                </div>
             </div>
             {if $tanggal_15 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_15|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_15 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update16" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 16 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_16}" name="Month_16" class="monthForm" placeholder="month 16" >

                </div>
             </div>
             {if $tanggal_16 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_16|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_16 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update17" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 17 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_17}" name="Month_17" class="monthForm" placeholder="month 17" >

                </div>
             </div>
             {if $tanggal_17 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_17|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_17 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update18" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 18 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_18}" name="Month_18" class="monthForm" placeholder="month 18" >

                </div>
             </div>
             {if $tanggal_18 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_18|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_18 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update19" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 19 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_19}" name="Month_19" class="monthForm" placeholder="month 19">

                </div>
             </div>
             {if $tanggal_19 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_19|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_19 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update20" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 20 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_20}" name="Month_20" class="monthForm" placeholder="month 20">

                </div>
             </div>
             {if $tanggal_20 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_20|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_20 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update21" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 21 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_21}" name="Month_21" class="monthForm" placeholder="month 21">

                </div>
             </div>
             {if $tanggal_21 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_21|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_21 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>

<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update22" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 22 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_22}" name="Month_22" class="monthForm" placeholder="month 22">

                </div>
             </div>
            {if $tanggal_22 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_22|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_22 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"   class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update23" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 23 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_23}" name="Month_23" class="monthForm" placeholder="month 23">

                </div>
             </div>
             {if $tanggal_23 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_23|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_23 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>
<div class="col-xs-12 col-md-6">
    <div class="form-group">
    <form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="Update24" />
<input type="hidden" name="id" value="{$id}">
<input type="hidden" name="member_email" value="{$email}">
<input type="hidden" name="member_name" value="{$nama}">
<input type="hidden" name="campaign" value="{$campaign}">
        <label>Month 24 : </label>
        <div class="monContent">
             <div class="monItem">
                <div class="montFormWrap">
                    <input type="text" value="{$bulan_24}"  name="Month_24" class="monthForm" placeholder="month 24">

                </div>
             </div>
             {if $tanggal_24 > 0}
                     <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" value="{$tanggal_24|date_format:"%d/%m/%Y"}" class="monthForm">

                     </div>
                     {else}
                      <div class="monItem2">
                         <input type="text" name="release_date" id="datepicker" placeholder="date" class="monthForm">

                     </div>
                     {/if}
                     {if $status_24 == 'Paid'}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid" selected>Paid </option>
                <option value="Unpaid">On Going</option>
</select>
                     </div>
                     {else}
                     <div class="monItem2">
                        <select name="status">
                <option> Select Status </option>
                <option value="Paid">Paid </option>
                <option value="Unpaid" selected>On Going</option>
</select>
                     </div>
                     {/if}
             <div class="monItem2">
                 <input name="button" type="submit"  class="btn btn-primary" id="button" value="Update" style="margin-left:5px;margin-top:6px;"/>
             </div>
        </div>
    </form>
    </div>
</div>
</div>

<script>
{literal}
$(function () {
     $('#row_dim').hide();
     $('#type').change(function () {
         $('#row_dim').hide();
         if (this.options[this.selectedIndex].value == 'month') {
             $('#row_dim').show();
         }
     });
 });
{/literal}
</script>
