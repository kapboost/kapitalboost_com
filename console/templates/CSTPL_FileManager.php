{literal}
<link href="../libs/ckfinder/sample.css" rel="stylesheet" type="text/css" />
<style type="text/css">

/* By defining CKFinderFrame, you are able to customize the CKFinder frame style */
.CKFinderFrame
{
	border: solid 2px #e3e3c7;
	background-color: #f1f1e3;
}

</style>
<script type="text/javascript">

// This is a sample function which is called when a file is selected in CKFinder.
function ShowFileInfo( fileUrl, data )
{
	alert( 'The selected file URL is "' + fileUrl + '"' ) ;

	// Display additional information available in the "data" object.
	// For example, the size of a file (in KB) is available in the data["fileSize"] variable.
	for ( _info in data )
		alert( 'data["' + _info + '"]' + ' = ' + data[_info] ) ;
}

	</script>
{/literal}

{$FileManager}
	
