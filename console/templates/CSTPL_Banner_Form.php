<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="action" value="ValidateBannerForm" />
<input type="hidden" name="banner_id" value="{$banner_id}">
<legend>Banner Form</legend>

<div class="form-group">
	<label>Banner Group : </label>
	<select name="banner_group_id" class="selectpicker">
		{section name=outer loop=$banner_group}
		<option value="{$banner_group[outer].banner_group_id}" {if $banner_group[outer].banner_group_id == $banner_group_id}selected="selected"{/if}>{$banner_group[outer].banner_group_name}</option>
		{/section}
	</select>
</div>

<div class="form-group">
	<label>Banner Title 1 : </label>
	<input type="text" name="banner_title" value="{$banner_title}" class="form-control">
</div>
<div class="form-group">
	<label>Banner Title 2 : </label>
	<input type="text" name="banner_title2" value="{$banner_title2}" class="form-control">
</div>
<div class="form-group">
	<label>Banner URL : </label>
	<input type="text" name="banner_url" value="{$banner_url}" class="form-control">
</div>

<div class="form-group">
	<label>Ordering : </label>
	<input type="text" name="banner_sort" value="{$banner_sort}" class="form-control">
</div>

<div class="form-group">
	<label>Image : </label>
	<input type="file" name="img" >

    {if $banner_pathimages != ''}
    <br />
    <div class="graph-wrapper" style="margin-left:-40px;">
      <ul class="image_wrapper">
        <li>
          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/banner/{$banner_pathimages}" class="image-link" title="{$banner_title}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/banner/{$banner_pathimages}&w=150" alt=""></a><br>
        </li>
      </ul>
    </div>
    <div class="clear"></div>
    {/if}
</div>

<div class="form-group">
	<label>Status : </label>
	<br />
	{html_radios labels='0' name='banner_status' options=$banner_status_options selected=$banner_status separator='<br />'}
</div>

<br />
      <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
      <input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
</form>
