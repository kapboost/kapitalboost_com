<!--=========Forms=========-->
<form action="index.php" method="POST">
<input type="hidden" name="action" value="{$action}">
<input type="hidden" name="action_id" value="{$action_id}">
<input type="hidden" name="module_id" value="{$module_id}">
<input type="hidden" name="session" value="{$session}">
<input type="hidden" name="system_id" value="{$system_id}">
<input type="hidden" name="id" value="{$id}">
<legend>Module Information</legend>
<div class="form-group">
	<label>Module Name : </label>
	<input type="text" name="name" value="{$name}" class="form-control">
	<span class="form_hint">module name for display name in website.</span>
</div>
<div class="form-group">
	<label>System Name : </label>
	<select name="select_system_id" id="select" class="form-control">
      	{section name=a loop=$systemlist}
        <option value="{$systemlist[a].system_id}" {if $select_system_id == $systemlist[a].system_id} selected="selected" {/if} >{$systemlist[a].name}</option>
        {/section}
      </select>
</div>
<div class="form-group">
	<label>Description : </label>
	<textarea name="description" rows="5" class="form-control">{$description}</textarea>
</div>
<div class="form-group">
	<label>File Name : </label>
	<input type="text" name="filename" value="{$filename}" class="form-control">
	<span class="form_hint">filename = class file, must be exist. eg: class.User.php</span>
</div>
<div class="form-group">
	<label>Class Name : </label>
	<input type="text" name="classname" value="{$classname}" class="form-control">
	<span class="form_hint">classname = call class name, based on file class above. eg : $obj = new <b>User</b></span>
</div>
<div class="form-group">
	<label>Ordering : </label>
	<input type="text" name="ordering" value="{$ordering}" class="form-control">
	<span class="form_hint">value : numeric, sorting : low to high.</span>
</div>

    <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
      <input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
    </fieldset>
</form>