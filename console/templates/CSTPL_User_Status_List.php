<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="5%">No</th>
      <th width="30%">User Name</th>
      <th width="35%">Status</th>
      <th width="30%">Action</th>
    </tr>
  </thead>
  <tbody>
    {section name=outer loop=$list}
    <tr class="even">
      <td>{$smarty.section.outer.index+1}</td>
      <td>{$list[outer].console_user}</td>
      <td><b>{$list[outer].status_login}</b></td>
      <td>
        {if $list[outer].status_login == "Online" && $login_id != $list[outer].login_id}
        <a href="index.php?t=&session={$session}&system_id={$system_id}&module_id={$module_id}&action_id={$action_id}&action=KickUser&id={$list[outer].login_id}" title="Kick User [{$list[outer].console_user}]">Kick User</a>
        {else}
        -
        {/if}
      </td>
    </tr>
    {/section}
  </tbody>
  
</table>
</div>

<p>
<span class="form_hint">Login user cannot kick own login ID, only online user can be kick out.</span>
</p>
