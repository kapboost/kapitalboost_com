<script src="//cdn.ckeditor.com/4.5.8/full/ckeditor.js"></script>
<link rel="stylesheet"  href="https://kapitalboost.com/assets/css/tags-input.css">
<script src="https://kapitalboost.com/assets/js/tags-input.js"></script>

<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
       <input type="hidden" name="action" value="{$action}">
       <input type="hidden" name="action_id" value="{$action_id}">
       <input type="hidden" name="module_id" value="{$module_id}">
       <input type="hidden" name="session" value="{$session}">
       <input type="hidden" name="system_id" value="{$system_id}">
       <input type="hidden" name="action" value="ValidateDataForm" />
       <input type="hidden" name="blog_id" value="{$blog_id}">

       <legend>Blog Form</legend>
       <!--<div class="form-group">
           <label>Slug : </label>
           <input type="text" name="slug" value="{$slug}" class="form-control">
       </div>-->

       <div class="form-group">
              <label>Blog Title : </label>
              <textarea name="blog_title" class="form-control">{$blog_title}</textarea>
       </div>

       <div class="form-group">
              <label>Blog Content : </label>
              <textarea name="blog_content" id="blog_content" class="form-control" wrap="hard">{$blog_content}</textarea>
       </div>

       <div class="form-group">
              <label>Release Date : </label>
              <input type="text" name="release_date" id="datepicker" value="{$release_date}" class="form-control">
       </div>
       <div class="form-group">
              <label>Category : </label>
              <input name="hashtags"  class="form-control" placeholder="Category">
       </div>
       <div class="form-group">
              <label>Blog Image : </label>
              <input type="file" name="img" >
              {if $image != ''}
              <br />
              <div class="graph-wrapper" style="margin-left:-40px;">
                     <ul class="image_wrapper">
                            <li>
                                   <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/blog/{$image}" class="image-link" title="{$image}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/blog/{$image}&w=150" alt=""></a><br>
                            </li>
                     </ul>
              </div>
              <div class="clear"></div>
              {/if}
       </div>

       <div class="form-group">
              <label>Status : </label>
              <br />
              {html_radios labels='0' name='enabled' options=$status_options selected=$enabled separator='<br />'}
       </div>

       <br />
       <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" />
       <input name="button" type="reset" class="btn btn-danger" id="button" value="Reset" />
</form>

{literal}
<script>CKEDITOR.replace('blog_content');</script>
<script type="text/javascript">
       function sent_onshow(value)
       {
              $(".close_all").hide();
              $("#show_this_" + value).show();
       }
</script>
{/literal}