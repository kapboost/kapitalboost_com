<script src="js/mktree/mktree.js" language="javascript"></script>
<link rel="stylesheet" href="js/mktree/mktree.css">
{literal}
<style type="text/css">
    .invest-form-layer {
        width: 50%;
    }
    .invest-form-layer > .checkIn > label {
        float: left;
        width: 100%;
    }
    .invest-form-layer > .checkIn > input[type="checkbox"] {
        float: left;
        width: 5%;
    }
    .invest-form-layer > .checkIn > p {
        float: left;
        width: 94%;
        text-align: left;
    }
    .checkIn {
        margin-top: -10px;
    }
</style>
{/literal}
<!--=========Forms=========-->
<form action="index.php" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="action" value="{$action}">
    <input type="hidden" name="action_id" value="{$action_id}">
    <input type="hidden" name="module_id" value="{$module_id}">
    <input type="hidden" name="session" value="{$session}">
    <input type="hidden" name="system_id" value="{$system_id}">
    <input type="hidden" name="action" value="ValidateMemberForm" />
    <input type="hidden" name="member_id" value="{$member_id}">

    <legend>Member Form</legend>

    <div class="form-group">
        <label>User Name : </label>
        <input type="text" name="member_username" value="{$member_username}" class="form-control" required>
    </div>

    <div class="form-group">
        <label>First Name : </label>
        <input type="text" name="member_surname" value="{$member_firstname}" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Last Name : </label>
        <input type="text" name="member_givenname" value="{$member_surname}" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Email : </label>
        <input type="text" name="member_email" value="{$member_email}" class="form-control" required>
    </div>

    <div class="form-group">
        <label>Password : </label>
        <input type="password" name="member_password" value="{$member_password}" class="form-control" required>
    </div>


    <div class="form-group">
        <label>NRIC/Pasport No : </label>
        <input type="text" name="nric" value="{$nric}" class="form-control">


    </div>
    <div class="form-group">
        <label>DOB: </label>
        <input type="text" name="dob" value="{$dob}" class="form-control">
    </div>

    <div class="form-group">
        <label>Residential Address  : </label>
        <input type="text" name="resarea" value="{$residental_area}" class="form-control" >
    </div>
    <div class="form-group">
        <label>Handphone Number  : </label>
        <input type="text" name="member_mobile" value="{$member_mobile_no}" class="form-control" >
    </div>
    <div class="form-group">
        <label>Country : </label>
        <select name="member_country" id="country" class="cat_dropdown" class="form-control">
            {html_options options=$member_country_options selected=$current_location}
        </select>
    </div>
    <div class="form-group">
        <label>Nationality : </label>
        <select name="current_location" id="nationality" class="cat_dropdown" class="form-control">
            {html_options options=$nationality_options selected=$member_country}
        </select>
    </div>
    <div class="form-group">

        <label for="CAT_Custom_20043780_157746">Upload copy of NRIC</label>
        <br />
        <input type="file" name="img" />
        {if $nric_file}
        <div class="uploadImg">
            <p>
                Current NRIC File
            </p>           
            <img src="/assets/nric/datanric/{$nric_file}" alt="NRIC File" width="200px" height="200px" />
        </div>
        {/if}
    </div>
    <div class="invest-form-layer" style="margin-bottom: 30px;margin-top: 25px;">

        <input class="ver-inv" type="hidden" value="" />
        <label class="labelIn2">Which of these financial products have you invested in?</label>
        <div class="checkIn">
            <br />
            {if $cek1}
            <input type="checkbox" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks" checked/><p>Stocks</p>
            {else}
            <input type="checkbox" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks"/><p>Stocks</p>
            <br />
            {/if}
            {if $cek2}
            <input type="checkbox" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" checked/><p>Bonds</p>
            <br />
            {else}
            <input type="checkbox" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" /><p>Bonds</p>
            {/if}
            {if $cek3}
            <input type="checkbox" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" checked/><p>Mutual funds</p>
            {else}
            <input type="checkbox" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" /><p>Mutual funds</p>
            <br />
            {/if}
            {if $cek4}
            <input type="checkbox" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" checked/><p>Alternative investments (specify)</p>
            {else}
            <input type="checkbox" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" /><p>Alternative investments (specify)</p>
            <br />
            {/if}
            {if $cek5}
            <input type="checkbox" name="check_5" id="CAT_Custom_20047261_157746_4" value="None of the above" checked/><p>None of the above</p>
            {else}
            <input type="checkbox" name="check_5" id="CAT_Custom_20047261_157746_4" value="None of the above" /><p>None of the above</p>
            {/if}

        </div>
    </div>
    <div class="" style="padding-right: 0;padding-left: 0;width: 97%;margin-top: 25px;">

        <div class="invest-form-layer" style="margin-bottom: 30px;padding-right: 0;padding-left: 0;width: 51.5%;margin-top: 25px;">
            <input class="ver-inv" type="hidden" value="" />
            <label class="labelIn2">State your interest in investing through Kapital Boost</label>
            <div class="checkIn">
                <br />
                {if $cek6}
                <input type="checkbox" name="check_6" id="CAT_Custom_20043779_157746_0" value="Attractive returns" checked/><p>Attractive returns</p>
                {else}
                <input type="checkbox" name="check_6" id="CAT_Custom_20043779_157746_0" value="Attractive returns" /><p>Attractive returns</p>
                {/if}
                <br />
                {if $cek7}
                <input type="checkbox" name="check_7" id="CAT_Custom_20043779_157746_1" value="Short tenor" checked/><p>Short tenor</p>
                {else}
                <input type="checkbox" name="check_7" id="CAT_Custom_20043779_157746_1" value="Short tenor" /><p>Short tenor</p>
                {/if}
                <br />
                {if $cek8}
                <input type="checkbox" name="check_8" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" checked /><p>Social/ethical reasons</p>
                {else}
                <br />
                <input type="checkbox" name="check_8" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" /><p>Social/ethical reasons</p>
                {/if}
                {if $cek9}
                <input type="checkbox" name="check_9" id="CAT_Custom_20043779_157746_3" value="Investment diversification" checked/><p>Investment diversification</p>
                <br />
                {else}
                <input type="checkbox" name="check_9" id="CAT_Custom_20043779_157746_3" value="Investment diversification"/><p>Investment diversification</p>
                {/if}
                {if $cek10}
                <input type="checkbox" name="check_10" id="CAT_Custom_20043779_157746_4" value="Transparency" checked/><p>Transparency</p>
                {else}
                <input type="checkbox" name="check_10" id="CAT_Custom_20043779_157746_4" value="Transparency"/><p>Transparency</p>
                {/if}
                <br />
                {if $cek11}
                <input type="checkbox" name="check_11" id="CAT_Custom_20043779_157746_5" value="Others" checked/><p>Others</p>
                {else}
                <input type="checkbox" name="check_11" id="CAT_Custom_20043779_157746_5" value="Others" /><p>Others</p>
                {/if}
                <br /><br />
                {if $cek12}
                <div class="invest-form-layer" style="margin-bottom: 30px;width: 97.5%;">
                    <input style="float: left;" type="checkbox" name="check_13" id="CAT_Custom_20043781_157746_0" value="yes" checked />
                    <label style="margin-left: 10px;float: left;max-width: 95%;text-align: left;" class="labelIn3">I agree to Kapital Boost’s <font style="color: #609edf;">Terms of Use</font> and <font style="color: #609edf;"> Privacy Policy </font>, and have read understand the <font style="color: #609edf;">Risk Statement</font>.</label>
                </div>
                {else}
                <div class="invest-form-layer" style="margin-bottom: 30px;width: 97.5%;">
                    <input style="float: left;" type="checkbox" name="check_13" id="CAT_Custom_20043781_157746_0" value="yes" />
                    <label style="margin-left: 10px;float: left;max-width: 95%;text-align: left;" class="labelIn3">I agree to Kapital Boost’s <font style="color: #609edf;">Terms of Use</font> and <font style="color: #609edf;"> Privacy Policy </font>, and have read understand the <font style="color: #609edf;">Risk Statement</font>.</label>
                </div>
                {/if}
            </div>

        </div>
    </div>

</div>



<br />
<center>
    <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" style="margin-bottom: 25px;" />
    <input name="button" type="reset" class="btn btn-default" id="button" value="Reset" style="margin-bottom: 25px;" />
</center>
</form>
