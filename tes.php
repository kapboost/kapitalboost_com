<?php

$number = 1288655444.56;

// let's print the international format for the en_US locale
setlocale(LC_MONETARY, 'en_SG.ISO-8559-1');
echo money_format('%i', $number) . "\n";
echo number_format($number) . "\n";

?>