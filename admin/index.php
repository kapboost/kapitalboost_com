<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       $members = $mysql->GetTotalNumberOfMembers();
       $members1 = $mysql->GetTotalNumberOfMembers1();
       $members7 = $mysql->GetTotalNumberOfMembers7();
       $members30 = $mysql->GetTotalNumberOfMembers30();
       $investments = $mysql->GetTotalNumberOfInvestments();
       $investments1 = $mysql->GetTotalNumberOfInvestments1();
       $investments7 = $mysql->GetTotalNumberOfInvestments7();
       $investments30 = $mysql->GetTotalNumberOfInvestments30();
       $getFunded = $mysql->GetTotalNumberOfGetFunded3();
       $contactUs = $mysql->GetTotalNumberOfContactUs3();
       $enquiry = $mysql->GetTotalNumberOfEnquiry3();
       $smeCampaigns = $mysql->GetTotalNumberOfOnGoingCampaigns("sme");
       $donationCampaigns = $mysql->GetTotalNumberOfOnGoingCampaigns("donation");
       $privateCampaigns = $mysql->GetTotalNumberOfOnGoingCampaigns("private");
       $smeTotalCampaigns = $mysql->GetTotalNumberOfCampaignsType("sme");
       $donationTotalCampaigns = $mysql->GetTotalNumberOfCampaignsType("donation");
       $privateTotalCampaigns = $mysql->GetTotalNumberOfCampaignsType("private");
       $totalInvestment = $mysql->GetTotalInvestmentAmount();
       $paidInvestment = $mysql->GetTotalInvestmentAmountPaid();
       $kbWallet = $mysql->GetTotalKbWallet();
       $WithdrawalRequest = $mysql->GetWithdrawalRequest();
       $HowYouKnow1 = $mysql->GetTotalNumberOfHowDidMemberKnow(1);
       $HowYouKnow2 = $mysql->GetTotalNumberOfHowDidMemberKnow(2);
       $HowYouKnow3 = $mysql->GetTotalNumberOfHowDidMemberKnow(3);
       $HowYouKnow4 = $mysql->GetTotalNumberOfHowDidMemberKnow(4);
       $HowYouKnow5 = $mysql->GetTotalNumberOfHowDidMemberKnow(5);
       $HowYouKnow6 = $mysql->GetTotalNumberOfHowDidMemberKnow(6);
       $HowYouKnow7 = $mysql->GetTotalNumberOfHowDidMemberKnow(7);
}



?>

<!DOCTYPE html>
<html lang="en">
       <head>
<?php include_once 'initialize.php'; ?>

              <title>KapitalBoost Admin</title>

<?php include_once 'include.php'; ?>


       </head>
       <body>
              <?php include_once 'header.php'; ?>

              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Overview</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">

                                                 <center>
														<?php if ($WithdrawalRequest>0) { ?>
                                                        <div style="width:86%;background-color: #ff7675; color: #f5f6fa; font-size:18px;padding:5px;" >
                                                                      You have <?php echo $WithdrawalRequest; ?> new withdrawal request... 
                                                        </div>
														<?php } ?>
														
														<div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Members
                                                               </p>
                                                               
                                                               <p class="dasCount">Today : <?=$members1?></p>
                                                               <p class="dasCount">Last 7 Days : <?=$members7?></p>
                                                               <p class="dasCount">Last 30 Days : <?=$members30?></p>
                                                               <p class="dasCount">Total : <?= $members ?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Investments
                                                               </p>
                                                               
                                                               <p class="dasCount">Today : <?= $investments1 ?></p>
                                                               <p class="dasCount">Last 7 Days : <?= $investments7 ?></p>
                                                               <p class="dasCount">Last 30 Days : <?= $investments30 ?></p>
                                                               <p class="dasCount">Total : <?= $investments ?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Last 3 Days
                                                               </p>
                                                               <p class="dasCount">Get Funded : <?=$getFunded?></p>
                                                               <p class="dasCount">Contact Us : <?=$contactUs?></p>
                                                               <p class="dasCount">Enquiry : <?=$enquiry?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Amount invested
                                                               </p>
                                                               <p class="dasCount">Total : SGD<?= number_format($totalInvestment, 2, '.', ',') ?></p>
                                                               <p class="dasCount">Paid : SGD<?=number_format($paidInvestment, 2, '.', ',')?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               
                                                               <p class="dasTitle">
                                                                      SME Campaigns
                                                               </p>
                                                               <p class="dasCount">Total : <?=$smeTotalCampaigns?></p>
                                                               <p class="dasCount">On Going : <?= $smeCampaigns ?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Donation Campaigns
                                                               </p>
                                                               <p class="dasCount">Total : <?=$donationTotalCampaigns?></p>
                                                               <p class="dasCount">On Going : <?= $donationCampaigns ?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      Private Campaigns
                                                               </p>
                                                               <p class="dasCount">Total : <?=$privateTotalCampaigns?></p>
                                                               <p class="dasCount">On Going : <?= $privateCampaigns ?></p>
                                                        </div>
                                                        <div class="dasStat">
                                                               <p class="dasTitle">
                                                                      KB Wallet
                                                               </p>
                                                               <p class="dasCount"><a href="wallet-member.php" style="color: #fff;">Total : $<?=number_format($kbWallet, 2, '.', ',')?></a></p>
                                                        </div>
														
                                                        <div class="dasStat">
                                                               
                                                               <p class="dasTitle">
                                                                      How Did Member Know About Us
                                                               </p>
															   <div style="width:50%; padding: 10px;float:left;">
																   <p class="dasCount">Search Engine : <?= $HowYouKnow1 ?></p>
																   <p class="dasCount">Ads : <?= $HowYouKnow2 ?></p>
																   <p class="dasCount">Social Media : <?= $HowYouKnow3 ?></p>
																   <p class="dasCount">Friend/Family : <?= $HowYouKnow4 ?></p>
															   </div>
															   <div style="width:50%; padding: 10px;float:left;">
															   <p class="dasCount">Community Event : <?= $HowYouKnow5 ?></p>
                                                               <p class="dasCount">News/Blog/Magazine : <?= $HowYouKnow6 ?></p>
                                                               <p class="dasCount">Other : <?= $HowYouKnow7 ?></p>
															   </div>
                                                 </center>

                                          </div>
                                   </div>
                            </div>

                     </div>

              </div>



       </body>


</html>