<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$bId = test_input($_GET["b"]);

if (isset($_POST)) {
       $titlef = test_input($_POST["title"]);
       $desf = $_POST["content"];
       $tagf = test_input($_POST["tag"]);

       $enabledf = test_input($_POST["enabled"]);
       $datef = test_input($_POST["date"]);

       $title1 = strtolower(str_replace(" ", "-", $titlef));
       $title1 = str_replace("(", "-", $title1);
       $title1 = str_replace(")", "-", $title1);
       

       $tagff = str_replace(",", ",~", $tagf);

       if ($mysql->Connection()) {
              $mysql->EditEvents($bId, $titlef, $desf, $enabledf, $datef, $title1, $tagff);
       }

       if (isset($_FILES["image"]) && $_FILES["image"]["name"] != "") {
              $file = $_FILES["image"]["tmp_name"];
              $path = $_FILES["image"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/event/";
              $image_name = $title1 . "." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name);
              if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgEvent($bId, $image_name);
              }
       }
}



if ($mysql->Connection()) {
       list($title,$des,$images,$slug,$tags,$enabled, $date) = $mysql->GetSingleEvents($bId);
}

$tags = str_replace("~", "", $tags);

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}

if ($date=="" or $date == null){$date=date("Y-m-d");}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Edit Event - <?= $title ?></title>

              <?php include_once 'include.php'; ?>

              <script>


                     $(document).ready(function () {

                     });
              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Event Detail - <?= $title ?></span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body" style="text-align: center">


                                                 <form action="" method="POST" enctype="multipart/form-data">

                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Event Title : </label>
                                                                      <input  type="text" name="title" value="<?= $title ?>" class="form-control" required>
                                                               </div>





                                                        </div>
                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Released Date : </label>
                                                                      <input  type="text" name="date" id="date" value="<?= $date ?>" class="form-control" required>
                                                               </div>
                                                        </div>


                                                        <div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Event Image</label>
                                                               <input type='file' name='image'  /><br>

                                                               <?php if ($images != "") { ?>
                                                                      <img width="90%" src='../assets/images/event/<?= $images ?>' alt='<?=$images?>'><br>

                                                               <?php } ?>

                                                        </div>

                                                        <div class="row">
                                                               <div class="col-xs-12 col-md-12">
                                                                      <div class="form-group">
                                                                             <label>Event Content : </label>
                                                                             <textarea name="content" class="form-control"><?= $des ?></textarea>
                                                                      </div>
                                                               </div>
                                                        </div>

                                                        <label>Status : </label>
                                                        <select name="enabled">
                                                               <?php if ($enabled == 0) { ?>
                                                                      <option value="1" >Enabled</option>
                                                                      <option value ="0" selected>Disabled</option>
                                                               <?php } else { ?>
                                                                      <option value="1" selected="" >Enabled</option>
                                                                      <option value ="0" >Disabled</option>                                            
                                                               <?php } ?>
                                                        </select><br>

                                                        <div>
                                                               <input type="text" style="width:80%;margin:10px;padding:10px;" value="<?=$tags?>" name='tag' placeholder="Tags | seperate with comma, no space before and after comma" />
                                                        </div>

                                                        <input type="submit" class="btn btn-primary" value="Update"/>
													</form>
                                          </div>
                                   </div>
                            </div>


                            <script src="js/pickadate/picker.js"></script>
                            <script src="js/pickadate/picker.date.js"></script>
                            <script src="js/pickadate/legacy.js"></script>
                            <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css"> 
                            <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
                            <script>

                     $('#date').pickadate({
                            format: 'yyyy-mm-dd'
                     });

                     $(document).ready(function () {
                            $('.image-link').magnificPopup({type: 'image'});


                     });


                            </script>
                            <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>
                            <script>CKEDITOR.replace('content', {

                     });
                            </script>


                            </body>


                            </html>