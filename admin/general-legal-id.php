<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($pageContents, $releaseDate) = $mysql->GetGeneralLegalId();
       $pageContentsA = explode("~~~^_^~~~", $pageContents);
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin General - Legal</title>

              <?php include_once 'include.php'; ?>

              <script src="/admin/lib/ckeditor/ckeditor.js"></script>

              <script>
                     $(document).ready(function () {
                            $("#termDivBtn").on('click', function () {
                                   $("#termDiv").fadeIn();
                                   $("#privacyDiv").hide();
                                   $("#riskDiv").hide();
                            });
                            $("#privacyDivBtn").on('click', function () {
                                   $("#termDiv").hide();
                                   $("#privacyDiv").fadeIn();
                                   $("#riskDiv").hide();
                            });
                            $("#riskDivBtn").on('click', function () {
                                   $("#termDiv").hide();
                                   $("#privacyDiv").hide();
                                   $("#riskDiv").fadeIn();
                            });
                            
                            $(".UpdateBtns").on('click', function () {
                                   var contents1 = CKEDITOR.instances.contents1.getData();
                                   var contents2 = CKEDITOR.instances.contents2.getData();
                                   var contents3 = CKEDITOR.instances.contents3.getData();
                                   var contents = contents1 + "~~~^_^~~~" + contents2 + "~~~^_^~~~"+contents3;
                                   $.post("posts.php", {job: "updategenerallegalid", contents: contents}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(2000).fadeOut(100);
                                          $("#popupText").html(reply);
                                   });

                            });
                            
                     });
              </script>
              <style>
                     .headers1,.headers2,.headers3{
                            width: 95%;margin: 15px;padding:10px;border-radius: 10px;font-weight: bolder;font-size: 1.5em
                     }
                     .contents1,.contents2,.contents3{
                            width:  95%;
                            height: 80%;margin:15px;padding:10px;border-radius: 10px;font-size: 1.1em;background-color: #d7e6f4
                     }
                     #AddBtn1,#AddBtn2,#AddBtn3{
                            background-color: white;border: none;display: inline-block
                     }
                     h1{
                            margin-top:10px
                     }

              </style>
       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Legal - </span>
                                                 <button class="btn btn-info" id="termDivBtn">Terms of Use</button>
                                                 <button class="btn btn-info" id="privacyDivBtn">Privacy Policy</button>
                                                 <button class="btn btn-info" id="riskDivBtn">Risk Statement</button>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">

                                                 <div id="termDiv"  style="text-align:center">
                                                        <h1>Terms of Use</h1>
                                                        <div id="contentArea1">
                                                               <div><textarea name="contents1" id="contents1" class="contents1" ><?= $pageContentsA[0] ?></textarea></div>
                                                        </div>
                                                        <button class="btn btn-primary UpdateBtns"  style="margin-bottom: 20px">Update</button>
                                                 </div>

                                                 <div id="privacyDiv"  style="text-align:center;display:none;">
                                                        <h1>Privacy Policy</h1>
                                                        <div id="contentArea2">
                                                               <div><textarea name="contents2" id="contents2" class="contents2" ><?= $pageContentsA[1] ?></textarea></div>
                                                        </div>
                                                        <button class="btn btn-primary UpdateBtns"   style="margin-bottom: 20px">Update</button>
                                                 </div>

                                                 <div id="riskDiv"  style="text-align:center;display:none;">
                                                        <h1>Risk Statement</h1>
                                                        <div id="contentArea3">
                                                               <div><textarea name="contents3" id="contents3" class="contents3" ><?= $pageContentsA[2] ?></textarea></div>
                                                        </div>
                                                        <button class="btn btn-primary UpdateBtns" style="margin-bottom: 20px">Update</button>
                                                 </div>


                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


              <script>CKEDITOR.replace('contents1', {});</script>
              <script>CKEDITOR.replace('contents2', {});</script>
              <script>CKEDITOR.replace('contents3', {});</script>

       </body>


</html>