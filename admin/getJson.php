<?php

	include_once 'security.php';
	include_once 'mysql.php';

	$mysql = new mysql();
	
	$mysql->Connection();

	if ($_GET['table'] == 'member') {
		$result = $mysql->GetAllMembersJson();
	}else if ($_GET['table'] == 'investment') {
		$result = $mysql->GetAllInvestmentsListJson($_GET['cat']);
	}

	$the_columns = array(
		"draw"=> 1,
		"recordsTotal"=> count($result),
		"recordsFiltered"=> count($result),
		"data"=> $result
	);

	echo json_encode($the_columns);

?>