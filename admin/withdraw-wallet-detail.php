<?php
include_once 'security.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$c = test_input($_GET["c"]);

include_once 'model/withdraw-wallet.php';
$WithdrawWallet = new WithdrawWallet();

if ($WithdrawWallet->Connection()) {
   list($mId, $amount, $date, $status, $proof_payment, $date_transfer, $mName, $bankName, $bankAccName, $bankAccNumber, $bankSwift) = $WithdrawWallet->GetWithdrawRequestDetail($c);
}

function test_input($data) {
   $data = trim($data);  
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}


if (isset($_POST)) {
	if (isset($_FILES["image"]) && $_FILES["image"]["name"] != "") {
		  $file = $_FILES["image"]["tmp_name"];
		  $path = $_FILES["image"]["name"];
		  $ext = pathinfo($path, PATHINFO_EXTENSION);
		  
		  $nameFile=md5($bankAccNumber[0]);
		  $uploaddir = "../assets/images/blog/";
		  $image_name = $mId[0] . "" . $c . "" . $amount[0] . "" . $nameFile . "." . $ext;
		  move_uploaded_file($file, $uploaddir . $image_name);
		  if ($WithdrawWallet->Connection()) {
				 $WithdrawWallet->UpdateUploadedImgTransferWithdraw($c, $image_name, $mId[0], $amount[0]);
		  }

		  
		  echo '<script type="text/javascript">
			window.location = "withdraw-wallet.php?c='.md5("SuccessKBAdminTransferWithdraw").'"
		  </script>';
   }
	
}

?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KB Admin Withdraw Request Detail</title>

      <?php include_once 'include.php'; ?>


   </head>
   <body>
      <?php include_once 'header.php'; ?>
      <?php include_once 'popup.php'; ?>


      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Withdraw Request Detail</span>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body">




                     <link rel="stylesheet" href="https://kapitalboost.com/assets/css/kp-default.css" />
                     <link rel="stylesheet" href="https://kapitalboost.com/assets/css/input.css" />

                     <form action="" method="post"  enctype="multipart/form-data">
                        
                        <div class="col-xs-12 col-md-6">

                           <div class="form-group ">
                              <label>Name : </label>
                              <input type="text" name="fullname" value="<?= $mName[0] ?>" class="form-control" readonly=""/>
                           </div>
							
							<div class="form-group ">
                              <label>Bank Name : </label>
                              <input type="text" name="fullname" value="<?= $bankName[0] ?>" class="form-control" readonly=""/>
                           </div>
						   
							<div class="form-group ">
                              <label>Account Name : </label>
                              <input type="text" name="fullname" value="<?= $bankAccName[0] ?>" class="form-control" readonly=""/>
                           </div>
						   
							<div class="form-group ">
								<label>Proof of Transfer :</label><br /><br />
                              <input class="files" id="image"  type="file" name="image" style="display:inline-block"/>
                           </div>
						   
						   <div class="form-group ">
								<div class="item">
                                    <input id="update" class="pUpdates" name="button" type="submit"  class="btn btn-primary pUpdate" value="Update" style="margin-left:5px;margin-top:6px;"/>
                                </div>
                           </div>
                           
                        </div>
						<div class="col-xs-12 col-md-6">

                           <div class="form-group ">
                              <label>Amount : </label>
                              <input type="text" name="fullname" value="SGD <?= $amount[0] ?>" class="form-control" readonly=""/>
                           </div>
							
							<div class="form-group ">
                              <label>Account Number: </label>
                              <input type="text" name="fullname" value="<?= $bankAccNumber[0] ?>" class="form-control" readonly=""/>
                           </div>
						   
							<div class="form-group ">
                              <label>Swift Code : </label>
                              <input type="text" name="fullname" value="<?= $bankSwift[0] ?>" class="form-control" readonly=""/>
                           </div>
						   
						   <?php 
								if ($proof_payment[0]=="" or empty($proof_payment[0])){
									echo "";
								} else {
						   ?>
						   <div class="form-group ">
								<label>Proof of Transfer File :</label><br /><br />
                              <div class="imageLink" style="display:inline-block"><a target="_blank" href="../assets/images/blog/<?= $proof_payment[0] ?>">Proof of Transfer File </a></div>
                                    <div class="deleteLink" id="" style="display: inline-block;color:red;margin-left:20px">Delete</div>
                           </div>
							<?php } ?>
                           
                        </div>
                        
                     </form>

                  </div>


               </div>
            </div>
         </div>
      </div>

   <script src="js/pickadate/picker.js"></script>
   <script src="js/pickadate/picker.date.js"></script>
   <script src="js/pickadate/picker.time.js"></script>
   <script src="js/pickadate/legacy.js"></script>

 
</body>


</html>