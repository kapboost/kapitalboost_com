<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($gId, $name, $email, $mobile, $cType, $fundingAmt, $Revenue, $tenor,$date,$source) = $mysql->GetAllGetFundedList();
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Get Funded List</title>

              <?php include_once 'include.php'; ?>

              <script>

                     $(document).ready(function () {
                            $(".DelBtn").click(function () {
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dgetfunded: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        location.reload();
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });
                     });

              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Get Funded</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">


                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="2%">No</th>
                                                                             <th width="15%">Full Name</th>
                                                                             <th width="10%">Email</th>
                                                                             <th width="8%">Mobile</th>
                                                                             <?php if (!isset($_GET['source'])): ?>
                                                                                    <th width="8%">Funding Amount</th>
                                                                             <?php endif ?>
                                                                             <th width="10%">Est Rev</th>
                                                                             <th width="10%">Funding Period</th>
                                                                             <th width="5%">Submitted Date</th>
                                                                             <?php if (isset($_GET['source'])): ?>
                                                                                    <th width="5%">Source</th>
                                                                             <?php endif ?>
                                                                             <th width="3%">Delete</th>
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($gId); $i++) { ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><a href="get-funded-edit.php?g=<?=$gId[$i]?>"><?= $name[$i] ?></a></td>
                                                                                    <td><?= $email[$i] ?></td>
                                                                                    <td><?= $mobile[$i] ?></td>
                                                                                    <?php if (!isset($_GET['source'])): ?>
                                                                                           <td><?= $fundingAmt[$i] ?></td>
                                                                                    <?php endif ?>
                                                                                    <td><?= $Revenue[$i] ?></td>
                                                                                    <td><?= $tenor[$i] ?></td>
                                                                                    <td><?=$date[$i]?></td>
                                                                                    <?php if (isset($_GET['source'])): ?>
                                                                                           <td><?= $source[$i] ?></td>
                                                                                    <?php endif ?>
                                                                                    <td style="text-align: center"><button value="<?= $gId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                                                                             </tr>
                                                                      <?php } ?>


                                                               </tbody>
                                                        </table>
                                                 </div>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


			<script>
			$(document).ready( function () {
				$('#myTable').DataTable({
          stateSave: true
        });
			} );
			</script>

       </body>


</html>
