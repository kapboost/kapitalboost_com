<?php
include_once 'security.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'mysql.php';
$mysql = new mysql();

if ($mysql->Connection()) {
   list($campaingId, $campaignName, $campaignBackground, $updateTitle, $updateContent, $updateDate, $updateids) = $mysql->GetAllCampaignUpdatesList();
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KapitalBoost Campaigns Update</title>

      <?php include_once 'include.php'; ?>


      <script>

          $(document).ready(function () {
             $(".DelBtn").click(function () {
                var index = $(this).parent().parent().index();
                var person = prompt("Please enter Master Code");
                $.post("delete.php", {updateproject: $(this).val(), MC: person}, function (reply) {
                   reply = JSON.parse(reply);
                   $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                      if (reply === "Done") {
                         $("tr").eq(index + 1).remove();
                      }
                   });
                   $("#popupText").html(reply);
                });
             });
             // $(".RefreshBtn").click(function () {
                // $.post("posts.php", {job: 'refreshtotalfunding', cId: $(this).val()}, function (reply) {
                   // $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                      // location.reload();
                   // });
                // });
                // $("#popupText").html("Refreshed");
             // });
          });


      </script>

   </head>
   <body>
      <?php include_once 'header.php'; ?>

      <?php include_once 'popup.php'; ?>

      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Campaign Update List</span>
					 <a class="btn btn-warning" style="margin: auto 20px;width:200px" href="project-update.php">Add New Campaign Update</a>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body">


                     <br/><br/>
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="myTable" name="myTable">
                           <thead>
                              <tr>
                                 <th width="2%">No</th>
                                 <th width="10%">Campaign Name</th>
                                 <th width="25%">Content</th>
                                 <th width="5%">Date</th>
                                 <!-- <th width="3%">Delete</th> -->
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                              for ($i = 0; $i < count($campaingId); $i++) {
                                 ?>
                                 <tr class="even">
                                    <td><?= $i + 1 ?></td>
                                    <td><a href="<?php echo ADMIN_PATH ?>/project-update.php?updateid=<?= $updateids[$i] ?>"><?= $campaignName[$i] ?></a></td>
                                    <td><b><?= $updateTitle[$i] ?></b> <br> 
									<?php 
									if (strlen($updateContent[$i])<200) {
										echo $updateContent[$i];
										
									} else {
										
									$string = substr($updateContent[$i],0,200).'...'; 
									echo strip_tags($string);
										
									}
									?></td>
                                    <td><?= $updateDate[$i] ?></td>
                                    <!-- <td style="text-align: center"><button value="<?#= $updateids[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td> -->
                                 </tr>
                                 <?php
                              }
                              ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

	<script>
	$(document).ready( function () {
		$('#myTable').DataTable();
	} );
	</script>
   </body>


</html>