<?php
include_once 'security.php';
include_once 'mysql.php';
$mysql = new mysql();
$bId = test_input($_GET["b"]);

$categories = $mysql->getAllCategories();
if ($mysql->Connection()) {
    list($cId, $cName, $cSlug, $cDesc) = $mysql->getAllCategories();
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

// if (isset($_POST)) {
if (isset($_POST["Submit"])) {
    $mysql->AddTrail("Category Added");
    echo '<script type="text/javascript">
      window.location = "category-list.php"
      </script>';
}

if (($_POST['submit']) == "Delete") {
    if ($mysql->Connection()) {
        $mysql->deleteCategory($cId);
        header('Location: '.'category-list.php');
    }
}

if ($date == "" or $date == null) {
    $date = date("Y-m-d");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'initialize.php'; ?>
    <title>Category List - <?= $title ?></title>
    <?php include_once 'include.php'; ?>
    <script>
        //
    </script>

</head>
<body>
<?php include_once 'header.php'; ?>
<?php include_once 'popup.php'; ?>

<div class="main-content-area-wrapper container-fluid" style="min-height: 800px;">
    <br>
    <div class="row">
        <div class="col-xs-12">
            <div class="general-panel panel">
                <div class="blue-panel-heading panel-heading">
                    <span class="header-panel">Category List <?= $title ?></span>
                    <br>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a class="btn btn-primary" href="category-create.php">Create a New Category</a>
                        </div>
                    </div>

                    <br>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="">Name</th>
                                <th width="">Slug</th>
                                <th width="">Description</th>
                                <th width="">Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php for ($i = 0; $i < count($cId); $i++) { ?>
                                <tr class="even">
                                    <td><?= $cName[$i] ?> </td>
                                    <td><?= $cSlug[$i] ?> </td>
                                    <td><?= $cDesc[$i] ?> </td>
                                    <td> <a class="btn btn-primary" href="category-edit.php?c=<?= $cId[$i] ?>">Edit</a>
                                      <a class="btn btn-danger" href="category-delete.php?c=<?= $cId[$i] ?>">Delete</a> </td>
                                    <td>  </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/pickadate/picker.js"></script>
        <script src="js/pickadate/picker.date.js"></script>
        <script src="js/pickadate/legacy.js"></script>
        <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css">
        <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
        <script>
            $('#date').pickadate({
                format: 'yyyy-mm-dd'
            });
            $(document).ready(function () {
                $('.image-link').magnificPopup({type: 'image'});
            });
        </script>
        <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>
        <script>CKEDITOR.replace('content', {});
        </script>
</body>
</html>
