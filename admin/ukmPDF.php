<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
// error_reporting( error_reporting() & ~E_NOTICE );


require 'lib/fpdi/tcpdf/tcpdf.php';
require 'lib/fpdi/fpdi.php';

//====================================================================================================================================================================================+
// Start Param Data
//====================================================================================================================================================================================+
include_once 'mysql.php';
$mysql = new mysql();

date_default_timezone_set("Asia/Jakarta");
$timestamp = date('d-m-Y',time());

$contract_ukm_id = $_GET['id'];
if ($mysql->Connection()) {

   list($id, $campaign_id, $contractNumber, $companyName, $companyCode, $customerNameEng, $customerNameIna, $companyRegNumber, $tenorEng, $tenorIna, $giroNoteEng, $giroNoteIna, $companyRegTypeEng, $companyRegTypeIna, $targetFundingAmt, $wakalahInvestor, $wakalahKB, $otherReqEng, $otherReqIna, $totalPayoutSGD, $totalPayoutIDR, $signee_1, $signee_2, $admin_fee_pct, $admin_fee_amt, $pdfFile, $email_1, $email_2, $id_number_1, $id_number_2, $position_1, $position_2, $payment_method_eng, $payment_method_ina) = $mysql->GetContractUKM($contract_ukm_id);

   $datas = $mysql->getDatas('tcampaign', 'campaign_id', $campaign_id, false, 'tcontract_ukm', 'campaign_id');
   $m_payout = $datas->m_payout;
   $total_payout_eng = $totalPayoutIDR;


   $project_returns = $datas->project_return;

   list($cName, $mPayout) = $mysql->GetSingleCampaignForPayout($campaign_id);
   $totalFundingAmt = $mysql->GetCampaignTotalAmount($campaign_id);

   list($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) = $mysql->GetSingleCampaignContractList($campaign_id);
   $subs_agency_fee = $invoSubsAgencyFee;

    $payoutA = explode("==", $mPayout);
    $amountA = $dateA = $statusA = array();
    $return = $camReturn;

    // $countPayoutA = count($payoutA);
    // echo $countPayoutA;

    for ($i = 0; $i < count($payoutA); $i++) {
      $payouts = explode("~", $payoutA[$i]);
      $countPayouts = count($payouts);

    if ($countPayouts==3){
        $amountA[$i] = $payouts[0];
        $dateA[$i] = $payouts[1];
        $statusA[$i] = $payouts[2];
      }
    }

    $count_mPayout = 0;

    for($j=1; $j < count($payoutA); $j++ ){
      if ($payoutA[$j] != '~~'){
        $count_mPayout = $count_mPayout+1;
      }
    }

    $data_images = $mysql->getDatas('tcontract_images', 'tcontract_ukm_id', $id);

}

$target_funding_amount = number_format($targetFundingAmt, 2);
$wakalah_fee_investor = number_format($wakalahInvestor, 2);
$wakalah_fee_kb = number_format($wakalahKB, 2);

$monthsID = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
$monthsENG = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

//Date Contract in English
$date_eng_day = date('d', strtotime($timestamp));
$date_eng_year = date('Y', strtotime($timestamp));
$date_eng_month = date('n', strtotime($timestamp));
$date_eng_month_ENG = $monthsENG[$date_eng_month];
$date_contract_eng = "$date_eng_day $date_eng_month_ENG $date_eng_year";

//Date Contract in Bahasa
$date_ina_day = date('d', strtotime($timestamp));
$date_ina_year = date('Y', strtotime($timestamp));
$date_ina_month = date('n', strtotime($timestamp));
$date_ina_month_ID = $monthsID[$date_ina_month];
$date_contract_ina = "$date_ina_day $date_ina_month_ID $date_ina_year";

// Company Registration Type
$companyRegEng = ["", "Company Registration No.", "Single Business No."];
$companyRegIna = ["", "Nomor Tanda Daftar Perusahaan (TDP)", "Nomor Induk Berusaha (NIB)"];
$company_reg_type_eng = $companyRegEng[$companyRegTypeEng];
$company_reg_type_ina = $companyRegIna[$companyRegTypeIna];

// $imageD1 = "https://66.media.tumblr.com/d1f165dbe4fb1ddb2701e89a5caf430e/tumblr_poqwe1liDP1ul63vpo1_400.jpg";
// $imageD2 = "https://66.media.tumblr.com/73601a48a0b8623d1ee7455c7283e90f/tumblr_ogj7j4C16h1uqtcf6o1_400.jpg";

// $companyNameID = "PT Sergio";
// $companyRegistrationID = "123456";
// $purchaseAssetID = "1234";

// $closeMonth = date('n', strtotime('2018-09-14'));
// $closeMonthId = $monthsID[$closeMonth];
// $closeDay = date('d', strtotime('2018-09-14'));
// $closeYear = date('Y', strtotime('2018-09-14'));
// $cCloseDateID = "$closeDay $closeMonthId $closeYear";

// $closeMonth1 = date('n', strtotime('2018-09-15'));
// $closeMonth1Id = $monthsID[$closeMonth1];
// $closeDay1 = date('d', strtotime('2018-09-15'));
// $closeYear1 = date('Y', strtotime('2018-09-15'));
// $cCloseDate1ID = "$closeDay1 $closeMonth1Id $closeYear1";

// $dirICID = "666";
// $invICID = "123";

// $invoContractNo = "999";
// $dateID = "2018-09-16";

//====================================================================================================================================================================================+
// End Param Data
//====================================================================================================================================================================================+


$pdf = new FPDI();

$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);


// set default header data
// $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);


$pdf->AddPage('P', array('210', '300'));
$pdf->SetFont('Helvetica');

// print a line
// $pdf->Cell(0, 12, 'CROWDFUNDING AGREEMENT / PERJANJIAN CROWDFUNDING', 0, 0, 'C');

$y = 18;

$pdf->SetFontSize("14");

$pdf->SetFont('Helvetica', 'B', 14);

$y = MCenter($pdf, "NO. IF.$contractNumber.$companyCode", $y+10);
$y = MCenter($pdf, "CROWDFUNDING AGREEMENT / PERJANJIAN CROWDFUNDING", $y+20);

$y = Right1($pdf, "<br></br><br></br><br><br>< /br><br></br><br>", $y + 5);

// $pdf->SetFontSize("12");

// $pdf->SetFontSize("14");
// $y = CenterR($pdf, "<br> _______________________ ", $y - 6);

//$pdf->SetFontSize("11.5");
$pdf->SetFont('Helvetica', '', 11.5);

$y = Right1Left($pdf, "Perjanjian Crowdfunding No. $contractNumber.$companyCode ini (“Perjanjian”) dibuat pada $date_contract_ina", $y + 5);

$y = Right1($pdf, "<b>ANTARA:</b>&nbsp;", $y + 11.5);

$y = Right1($pdf, "<b>Kapital Boost Pte Ltd</b>&nbsp;", $y + 1);

$y = Right1Left($pdf, "(Pendaftaran perusahaan Singapura no. 201525866W)", $y + 5);

$y = Right1($pdf, "dan", $y + 8);

$y = Right1($pdf, "<b>$companyName</b>&nbsp;", $y + 7);

$y = Right1($pdf, "($company_reg_type_ina Indonesia $companyRegNumber)", $y + 5);

$y = Right1($pdf, "(selanjutnya disebut “Perusahaan”)", $y + 0.5);

$y = Right1($pdf, "Kapital Boost dan Perusahaan disebut secara bersama- sama sebagai “Para Pihak” dan secara masing-masing sebagai “Pihak”. ", $y + 5);

$y = Right1($pdf, "<b>DALAM HAL INI</b>", $y + 5);

$y = Right1($pdf, "A.", $y + 5);

$y = Right2($pdf, "Perusahaan membutuhkan dana, berdasarkan piutang yang ditagih ke $customerNameIna (selanjutnya disebut sebagai “Bowheer”) (lihat Lampiran 1 untuk faktur), dari sekelompok pemilik dana (selanjutnya disebut sebagai “Pemberi Dana”) melalui platform penggalangan dana Kapital Boost. ", $y - 7);

$y = Right1($pdf, "B.", $y + 5);

$y = Right2($pdf, "Pembiayaan akan menggunakan skema Qard dan Wakalah, yang prosesnya tercantum dalam Lampiran 1. ", $y - 7);

$y = Right1($pdf, "<b>DENGAN INI DISEPAKATI </b>&nbsp; sebagai berikut:", $y + 5);

$y = Right1($pdf, "<b>Ketentuan Akad Qard: </b>&nbsp; Pokok pinjaman sebesar SGD $target_funding_amount berdasarkan prinsip Shariah Qard, dan dibayarkan kepada Pemberi Dana dalam $tenorIna bulan setelah transfer dana dari Pemberi Dana ke Perusahaan.", $y + 5);


$y = Right1($pdf, "<b>Ketentuan Perjanjian Wakalah: </b>&nbsp; Perusahaan akan menunjuk Pemberi Dana untuk mengelola dan menagih piutang dari pelanggannya $customerNameIna. Pemberi Dana kemudian akan menunjuk Kapital Boost sebagai Sub-Agen untuk tugas yang sama. Ini termasuk, tetapi tidak terbatas pada, menghubungi melalui telepon, email atau secara fisik bertemu dengan pelanggan untuk mengelola dan mengumpulkan piutang. Dalam menjalankan peran mereka sebagai Agen dan Sub-agen, Perusahaan akan membayar kepada Pemberi Dana dan Kapital Boost biaya Agensi masing-masing sebesar <b>SGD $wakalah_fee_investor</b>&nbsp; dan <b>SGD $wakalah_fee_kb</b>&nbsp;. ", $y + 5);

$y = Right1($pdf, "<b>Persyaratan untuk transfer dana :   </b> &nbsp; Perusahaan telah sepakat bahwa mereka akan memenuhi persyaratan berikut sebelum dana dari Pemberi Dana dapat ditransfer ke Perusahaan:", $y + 5);

BulletR($pdf, $y);

$y = Right2($pdf, "Penandatanganan <b><i>Surat Jaminan Pribadi </i></b> &nbsp; oleh $signee_1 and $signee_2 dari $companyName", $y + 3);

BulletR($pdf, $y);

$y = Right2($pdf, "Penerbitan $giroNoteIna oleh Perusahaan kepada PT Kapital Boost Indonesia untuk jumlah yang terhutang kepada Pemberi Dana dan tanggal sesuai dengan Jadwal Pembayaran, sebagai berikut: <br>", $y + 3);

$no = 0;
for ($k=0 ; $k < $count_mPayout; $k++){
  $calculate_idr = $totalPayoutIDR * ($amountA[$k]/100);
  //echo $calculate_idr; echo '<br>';
    $bagi = 500000000;
    $bagi_NF = number_format($bagi);
    //$total_payout = 1577744020;
    $total_payout_NF = number_format($calculate_idr);
    $hasilbagi =  intval($calculate_idr / $bagi);

  	if($hasilbagi < 1){
        $date_ina_day1 = date('d', strtotime($dateA[$k]));
        $date_ina_year1 = date('Y', strtotime($dateA[$k]));
        $date_ina_month1 = date('n', strtotime($dateA[$k]));
        $date_ina_month_ID1 = $monthsID[$date_ina_month1];
        $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";

        //$newDate = date("d F Y", strtotime($dateA[0]));
        $no += 1;
        $y = Right3($pdf, "$no. ", $y);
        $y = Right4($pdf, "BG $no – Nominal Rp. $total_payout_NF tgl. $date_contract_ina1 ", $y - 7);

    } else {

      for($i = 0; $i < $hasilbagi; $i++){
        $date_ina_day1 = date('d', strtotime($dateA[$k]));
        $date_ina_year1 = date('Y', strtotime($dateA[$k]));
        $date_ina_month1 = date('n', strtotime($dateA[$k]));
        $date_ina_month_ID1 = $monthsID[$date_ina_month1];
        $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";
          //$newDate = date("d F Y", strtotime($dateA[$i]));
          // $nomor = $no+1;
          $no += 1;
          $calculate_idr = $calculate_idr - $bagi;

            $y = Right3($pdf, "$no. ", $y);
            $y = Right4($pdf, "BG $no – Nominal Rp. $bagi_NF tgl. $date_contract_ina1 ", $y - 7);

          if($calculate_idr < $bagi){
            // $nomor_akhir = $no+1;
            $no += 1;
            //$newDate_akhir = date("d F Y", strtotime($dateA[$i+1]));
            $date_ina_day1 = date('d', strtotime($dateA[$k]));
            $date_ina_year1 = date('Y', strtotime($dateA[$k]));
            $date_ina_month1 = date('n', strtotime($dateA[$k]));
            $date_ina_month_ID1 = $monthsID[$date_ina_month1];
            $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";
            $total_payout_NF1 = number_format($calculate_idr);

            $y = Right3($pdf, "$no. ", $y);
            $y = Right4($pdf, "BG $no – Nominal Rp. $total_payout_NF1 tgl. $date_contract_ina1 ", $y - 7);
          }
      }
    }
}



$y = Right1($pdf, "<b>Persyaratan lainnya: </b> &nbsp; $otherReqIna", $y + 5);

$y = Right1($pdf, "Perusahaan setuju bahwa faktur yang diterbitkan untuk Bowheer, yang membentuk dokumen pokok Perjanjian Qard dan Wakalah, tidak boleh digunakan untuk mendapatkan pembiayaan dari pihak lain seperti, tetapi tidak terbatas pada, bank dan P2P platform.", $y + 5);

$y = Right1($pdf, "<b>Jadwal pembayaran: </b> &nbsp; Pokok pinjaman dan biaya Agensi (selanjutnya disebut sebagai “kewajipan pembayaran”) harus dibayarkan kepada Pemberi Dana dan Kapital Boost, $tenorIna bulan setelah transfer dana dari para Pemberi Dana ke Perusahaan, sesuai dengan jadwal pembayaran berikut: <br>", $y + 5);

$payoutStringR = "";
   for ($i = 0; $i < 12; $i++) {
    $date_ina_day1 = date('d', strtotime($dateA[$i]));
    $date_ina_year1 = date('Y', strtotime($dateA[$i]));
    $date_ina_month1 = date('n', strtotime($dateA[$i]));
    $date_ina_month_ID1 = $monthsID[$date_ina_month1];
    $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";
	  //$newDate = date("d F Y", strtotime($dateA[$i]));
	  $percentProfit = (float)$amountA[$i];
	  $marginMonthly = 0;
	  if ($percentProfit!="" or $percentProfit!=null) {
		  $marginTotal = ((float)$return/100)*(float)$totalFundingAmt;
		  // echo "$marginTotal+$totalFundingAmt";
		  $totalAmount = $marginTotal+$totalFundingAmt;
		  // echo "($percentProfit/100)*$totalAmount";
		  // echo "$percentProfit/100 <br />";
		  $percentageTotalAmt = $percentProfit/100;
		  // echo "$marginMonthly = $percentProfit*$totalAmount*10;  <br />";
      $marginMonthly = ($percentProfit*$totalAmount/100) + (float)$wakalah_fee_kb;
      $no = $i+1;
      $payoutStringR .=
        "<tr>
          <td style=\"text-align:center;height:18px\"> $no </td>
          <td style=\"text-align:center;height:18px\"> $date_contract_ina1 </td>
          <td style=\"text-align:center;height:18px\">SGD ".number_format($marginMonthly,2)."</td>
        </tr>";
	  }



   }

$tenorTbl= "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
<tr>
<td width=\"15%\" style=\"text-align:center\"><b>No</b>&nbsp;</td>
<td width=\"45%\" style=\"text-align:center\"><b>Tanggal jatuh tempo pembayaran</b>&nbsp;</td>
<td width=\"40%\" style=\"text-align:center\"><b>Jumlah yang wajib dibayarkan</b>&nbsp;</td>
</tr>
$payoutStringR
</table>
";
$pdf->SetMargins(110, 10, 10);
$pdf->SetX(110);
$pdf->SetFontSize(10);
$pdf->writeHTML($tenorTbl);
$pdf->SetFontSize(12);
$y = $pdf->GetY();

$y = Right1($pdf, "<b>Pembayaran awal atas kewajipan pembayaran: </b> &nbsp; Perusahaan dapat memilih dengan diskresinya untuk memberlakukan pembayaran awal atas seluruh atau sebagian Kewajipan pembayaran. Setelah pembayaran awal atas setiap bagian dari Kewajipan pembayaran, Pemberi Dana atau Pemberi Dana perseorangan dengan diskresi mereka atau diskresinya dapat memberikan ibra’ (rabat) atas sisa bagian (-bagian) Kewajipan pembayaran atau, sesuai keadaan, bagian Pemberi Dana tersebut dari Kewajipan pembayaran. Kedua belah pihak mengakui bahwa pembebanan dan/atau penuntutan biaya pelunasan awal tidak sesuai dengan hukum Syariah.", $y + 5);

$y = Right1($pdf, "<b>Metode pembayaran:</b> &nbsp; $payment_method_ina", $y + 5);

$y = Right1($pdf, "<b>Biaya admin: </b> &nbsp; Kapital Boost akan mengambil $subs_agency_fee% dari dana yang terkumpul untuk Biaya Administrasi Crowdfunding. Dengan asumsi target pendanaan terpenuhi, Biaya Administrasi Crowdfunding adalah SGD $admin_fee_amt. Biaya ini harus dibayarkan ke Kapital Boost sebelum pinjaman Qard dari Funders ditransfer ke Perusahaan.", $y + 5);

$y = Right1($pdf, "<b>Periode penggalangan dana: </b> &nbsp;  Tujuh (7) hari setelah kampenya crowdfunding untuk Perusahaan diluncurkan pada platform penggalangan dana Kapital Boost atau setelah jumlah dana target dipenuhi, (mana yang lebih pendek). Perusahaan memiliki opsi untuk memperpanjang periode crowdfunding dengan tambahan 7 (tujuh) hari jika jumlah target dana tidak terpenuhi dalam 7 (tujuh) hari pertama.", $y + 5);

$y = Right1($pdf, "<b>Transfer dana: </b> &nbsp; Perusahaan setuju untuk menunjuk Kapital Boost sebagai Agen untuk mengumpulkan dana dari Pemberi Dana dengan tujuan semata-mata untuk mentransfer dana ke rekening bank Perusahaan. Perusahaan juga setuju untuk menunjuk Kapital Boost sebagai Agen untuk mendistribusikan pembayaran pokok pinjaman dan biaya Agensi kepada Pemberi Dana.", $y + 5);

$y = Right1($pdf, "<b>Biaya transfer: </b> &nbsp; Pemberi Dana akan menanggung biaya yang terkait dengan transfer dana ke Kapital Boost, jika ada. Kapital Boost akan menanggung biaya yang terkait dengan transfer dana ke Perusahaan. Biaya yang timbul ketika mengembalikan pokok pinjaman dan membayar biaya Agensi kepada Pemberi Dana (melalui Kapital Boost) akan ditanggung oleh Perusahaan.", $y + 5);

$y = Right1($pdf, "<b>Risiko nilai tukar: </b> &nbsp; Perusahaan setuju untuk menanggung risiko nilai tukar sehubungan dengan pembayaran kembali pokok pinjaman dan biaya Agensi kepada Pemberi Dana dan Kapital Boost.", $y + 5);

$y = Right1($pdf, "<b>Hukum yang berlaku dan penyelesaian  &nbsp; sengketa : </b> Perjanjian diatur dan diinterpretasikan serta ditafsirkan dalam semua hal sesuai dengan hukum negara Republik Indonesia. ", $y + 5);

$y = Right1($pdf, "Para Pihak sepakat bahwa jika terdapat perbedaan pendapat, sengketa, pertentangan atau perdebatan ('Sengketa'), yang timbul dari atau sehubungan dengan Perjanjian ini atau pelaksanaannya, termasuk tidak terbatas, sengketa apa pun mengenai keberadaan, keabsahan Perjanjian ini, atau pengakhiran hak-hak atau kewajiban Pihak manapun, Para Pihak akan berupaya untuk menyelesaikan Sengketa tersebut dengan penyelesaian damai antara Para Pihak. ", $y + 5);

$y = Right1($pdf, "Apabila Para Pihak tidak dapat mencapai kesepakatan untuk menyelesaikan Sengketa, maka Para Pihak sepakat untuk menyelesaikan Sengketa melalui Pengadilan Negeri Di Provinsi DKI Jakarta dalam perkara perdata maupun pidana mengesampingkan Pasal 55 Undang-undang Nomor 21 tahun 2008 dan Pasal 49 Undang-undang Nomor 3 tahun 2006. ", $y + 5);

$y = Right1($pdf, "Dalam hal terdapatnya biaya untuk keperluan penyelesaian sengketa seperti biaya jasa lawyer, kurator dan semua yang berkaitan dengan proses penyelesaian sengketa maka biaya tersebut akan menjadi tanggungan Perusahaan dan dibebankan sebagai termasuk dalam hutang yang harus dibayar.", $y + 5);

$y = Right1($pdf, "DEMIKIAN, Para Pihak pada Perjanjian ini telah menandatangani Perjanjian ini secara resmi pada tanggal di bagian awal Perjanjian ini. <br>< /br><br>< /br><br>< /br><br>< /br><br>", $y + 5);

// $y = Right1($pdf, "<br></br><br></br><br>", $y + 5);
// $y = Right1($pdf, "<br></br><br></br><br>", $y + 5);
// $y = Right1($pdf, "<br></br><br>< /br><br>", $y + 5);
// $y = Right1($pdf, "<br>< /br>", $y + 5);




// $y = Right1($pdf, "<b>ANTARA:</b> ", $y + 5);

// $y = Right1($pdf, "<b>1. $companyNameID</b>&nbsp;", $y + 5);

// $y = Right1($pdf, "($companyRegistrationID)", $y);

// $y = Right1($pdf, "(selanjutnya disebut sebagai \"<b>Perusahaan</b>&nbsp;\") ", $y + 5);

// $y = Right1($pdf, "<b>DAN</b>&nbsp;", $y + 5);
// $invIC = str_replace(" IC "," Identity Card ",$invIC);
// $y = Right1($pdf, "<b>2. $invName</b>&nbsp;<br>($invIC)", $y + 5);

// $y = Right1($pdf, "(selanjutnya disebut sebagai \"<b>Perusahaan</b>&nbsp;  \")", $y + 5);




if ($y > 160) { // Shift the sign table below
  $pdf->AddPage();
  $y = 0;
}
$synp100 = $pdf->getPage();







/* ---------- END of Bahasa Version -------------- */



$pdf->SetPage(1);
$y = 21.5;

$y = Left1($pdf, "<br></br><br>< /br><br>", $y + 5);

// $y = CenterL($pdf, "DATED THIS DAY $date", $y);

// $pdf->SetFontSize("25");
// $y = CenterL($pdf, ". . . . . . . . . . . . . . . .", $y);

// $pdf->SetFontSize("14");
// $y = CenterL($pdf, "MURABAHAH AGREEMENT", $y, true);

// $pdf->SetFontSize("25");
// $y = CenterL($pdf, ". . . . . . . . . . . . . . . .", $y - 6);
// $pdf->SetFontSize("12");

// $pdf->SetFontSize("14");
// $y = CenterL($pdf, "_______________________ <br>", $y);

// $pdf->SetFontSize("12");
// $y = CenterL($pdf, "<b>&nbsp;&nbsp;&nbsp;&nbsp;MURABAHAH AGREEMENT NO. M$invoContractNo FInancing Agreement Based on the Shariah Principle of Murabahah</b>&nbsp;&nbsp;", $y);
//$y = Right1($pdf, "<b><span style=\"text-align:center !important;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERJANJIAN CROWDFUNDING&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; NO. IF$contractNumber.$companyCode</b></span>", $y);

// $pdf->SetFontSize("14");
// $y = CenterL($pdf, " <br> _______________________", $y - 6);
$pdf->SetFontSize("11.5");

$y = Left1Left($pdf, "This Crowfunding Agreement No. $contractNumber.$companyCode (the “Agreement”) is made on $date_contract_eng", $y + 5);

$y = Left1($pdf, "<b>BETWEEN:</b>&nbsp;", $y + 5);

$y = Left1($pdf, "<b>Kapital Boost Pte Ltd</b>&nbsp;", $y + 1);

$y = Left1Left($pdf, "(Singapore company registration no. 201525866W)", $y + 5);

$y = Left1($pdf, "and", $y + 7.5);

$y = Left1($pdf, "<b>$companyName</b>&nbsp;", $y + 7);

$y = Left1($pdf, "(Indonesia $company_reg_type_eng $companyRegNumber)", $y + 5);

$y = Left1($pdf, "(hereinafter referred to as “the Company”)", $y + 0.5);

$y = Left1($pdf, "Kapital Boost and the Borrower shall be referred to collectively as the “Parties” and individually as a “Party”.", $y + 5);

$y = Left1($pdf, "<b>WHEREAS:</b>&nbsp;", $y + 5);

$y = Left1($pdf, "A.", $y + 5);

$y = Left2($pdf, "The Company is seeking funding, based on receivables expected from $customerNameEng (hereinafter referred to as “Customers”) (see Appendix 1 for the invoice), from a group of Funders (hereinafter referred to as “Funders”) via the Kapital Boost crowdfunding platform. ", $y - 7);

$y = Left1($pdf, "<br>< /br>B.", $y + 5);

$y = Left2($pdf, "The financing will utilize a Qard and a Wakalah structure, of which the process is listed in Appendix 1.", $y - 7);

$y = Left1($pdf, "<b>NOW IT IS HEREBY AGREED </b>&nbsp; as follows:", $y + 5);

$y = Left1($pdf, "<b>Terms of the Qard Agreement: </b>&nbsp; The loan principal of SGD $target_funding_amount is based on the Shariah principle of Qard, that is payable to Funders within $tenorEng months after the transfer of Funds from Funders to the Company.", $y + 5);

$y = Left1($pdf, "<b>Terms of the Wakalah Agreement: </b>&nbsp; The Company will appoint Funders to manage and collect the receivables from customer $customerNameEng. Funders will then appoint Kapital Boost as a Sub-Agent for the same task. In performing their roles as Agent and Sub-agent, the Company will pay to Funders and Kapital Boost an Agency fee of <b>SGD $wakalah_fee_investor</b>&nbsp; and <b>SGD $wakalah_fee_kb</b>&nbsp;, respectively. <br></br><br></br><br></br><br></br><br></br><br></br><br>", $y + 5);

$y = Left1($pdf, "<b>Requirements for fund transfer :  </b> &nbsp; The Company has agreed that they will meet the following conditions before funds from Funders can be transferred to the Company:", $y + 5);

BulletL($pdf, $y);

$y = Left2($pdf, "The signing of a <b><i> Personal Guarantee Letter </i></b> &nbsp; by $signee_1 and $signee_2 from $companyName", $y + 3);

BulletL($pdf, $y);

$y = Left2($pdf, "The issuance of $giroNoteEng by the Company to PT Kapital Boost Indonesia for the amount owed to Funders and dated according to the Payment Schedule, as follows:", $y + 3);

// $y = Left3($pdf, "i. ", $y);

// $y = Left4($pdf, "BG 1 – Nominal Rp. 500,000,000 dated (15.1)18 October 2019 ", $y - 7);
$no = 0;
for ($k=0 ; $k < $count_mPayout; $k++){
  $calculate_idr = $totalPayoutIDR * ($amountA[$k]/100);
  //echo $calculate_idr; echo '<br>';
    $bagi = 500000000;
    $bagi_NF = number_format($bagi);
    //$total_payout = 1577744020;
    $total_payout_NF = number_format($calculate_idr);
    $hasilbagi =  intval($calculate_idr / $bagi);

  	if($hasilbagi < 1){
        $date_ina_day1 = date('d', strtotime($dateA[$k]));
        $date_ina_year1 = date('Y', strtotime($dateA[$k]));
        $date_ina_month1 = date('n', strtotime($dateA[$k]));
        $date_ina_month_ID1 = $monthsID[$date_ina_month1];
        $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";

        //$newDate = date("d F Y", strtotime($dateA[0]));
        $no += 1;
        $y = Left3($pdf, "$no. ", $y);
        $y = Left4($pdf, "BG $no – Nominal Rp. $total_payout_NF dated $date_contract_ina1 ", $y - 7);

    } else {

      for($i = 0; $i < $hasilbagi; $i++){
        $date_ina_day1 = date('d', strtotime($dateA[$k]));
        $date_ina_year1 = date('Y', strtotime($dateA[$k]));
        $date_ina_month1 = date('n', strtotime($dateA[$k]));
        $date_ina_month_ID1 = $monthsID[$date_ina_month1];
        $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";
          //$newDate = date("d F Y", strtotime($dateA[$i]));
          // $nomor = $no+1;
          $no += 1;
          $calculate_idr = $calculate_idr - $bagi;

            $y = Left3($pdf, "$no. ", $y);
            $y = Left4($pdf, "BG $no – Nominal Rp. $bagi_NF dated $date_contract_ina1 ", $y - 7);

          if($calculate_idr < $bagi){
            // $nomor_akhir = $no+1;
            $no += 1;
            //$newDate_akhir = date("d F Y", strtotime($dateA[$i+1]));
            $date_ina_day1 = date('d', strtotime($dateA[$k]));
            $date_ina_year1 = date('Y', strtotime($dateA[$k]));
            $date_ina_month1 = date('n', strtotime($dateA[$k]));
            $date_ina_month_ID1 = $monthsID[$date_ina_month1];
            $date_contract_ina1 = "$date_ina_day1 $date_ina_month_ID1 $date_ina_year1";
            $total_payout_NF1 = number_format($calculate_idr);

            $y = Left3($pdf, "$no. ", $y);
            $y = Left4($pdf, "BG $no – Nominal Rp. $total_payout_NF1 dated $date_contract_ina1 ", $y - 7);
          }
      }
    }
}


$y = Left1($pdf, "<b>Other requirements: </b> &nbsp; $otherReqEng <br>", $y + 5);

$y = Left1($pdf, "The Company agrees that the invoices issued to the Customers, that forms the underlying document for the Qard and Wakalah Agreement, cannot be used to seek financing from external parties such as, but not limited to, banks and P2P platforms.", $y + 5);

$y = Left1($pdf, "<b>Payment schedule: </b> &nbsp; The loan principal and Agency fees (hereinafter referred to as “payment obligations”) are to be paid to the Funders and Kapital Boost, $tenorEng months after the transfer of funds from Funders to the Company, according on the following payment schedule: <br> ", $y + 5);

$payoutString = "";
   for ($i = 0; $i < 12; $i++) {
	  $newDate = date("d F Y", strtotime($dateA[$i]));
	  $percentProfit = (float)$amountA[$i];
	  $marginMonthly = 0;
	  if ($percentProfit!="" or $percentProfit!=null) {
		  $marginTotal = ((float)$return/100)*(float)$totalFundingAmt;
		  // echo "$marginTotal+$totalFundingAmt";
		  $totalAmount = $marginTotal+(float)$totalFundingAmt;
		  // echo "($percentProfit/100)*$totalAmount";
		  // echo "$percentProfit/100 <br />";
		  $percentageTotalAmt = $percentProfit/100;
		  // echo "$marginMonthly = $percentProfit*$totalAmount*10;  <br />";
      $marginMonthly = ($percentProfit*$totalAmount/100) + (float)$wakalah_fee_kb;
      $no = $i+1;
      $payoutString .=
        "<tr>
          <td style=\"text-align:center;height:18px\"> $no </td>
          <td style=\"text-align:center;height:18px\"> $newDate </td>
          <td style=\"text-align:center;height:18px\">SGD ".number_format($marginMonthly,2)."</td>
        </tr>";
	  }



   }

$tenorTbl1= "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
<tr>
<td width=\"15%\" style=\"text-align:center\"><b>No</b>&nbsp;</td>
<td width=\"45%\" style=\"text-align:center\"><b>Payment date</b><br>&nbsp;</td>
<td width=\"40%\" style=\"text-align:center\"><b>Payment due</b>&nbsp;</td>
</tr>
$payoutString
</table>
";
// $pdf->SetMargins(110, 10, 10);
// $pdf->SetX(110);
// $pdf->SetFontSize(10);
// $pdf->writeHTML($tenorTbl1);
// $pdf->SetFontSize(12);
// $y = $pdf->GetY();

if ($y > 200) { // Shift the sign table below
  //$pdf->AddPage();
  $y = 20;
}
$pdf->SetMargins(10, 10, 110);
$pdf->SetFontSize(10);
$pdf->writeHTML($tenorTbl1);
$pdf->SetFontSize(12);
$y = $pdf->GetY();

$y = Left1($pdf, "<b>Early payment of payment obligations: </b> &nbsp; The Company may opt at its discretion to effect early payment of all or any part of the Payment obligations. Following any early payment of any part of the Payment obligations, the Funders or any individual Funder may at their or his absolute discretion grant ibra’ (rebate) on any remaining part(s) of the Payment obligations or, as the case may be, that Funder’s share of the Payment obligations. Both parties acknowledge that charging and/or claiming early settlement charges is not in accordance with Shariah law. <br></br><br></br><br></br><br></br>", $y + 5);

$y = Left1($pdf, "<b>Payment method: </b> &nbsp; $payment_method_eng <br>< /br><br>< /br>", $y + 5);

//$y = Left1($pdf, "<b><i>Metode pembayaran:</i></b> &nbsp; (18.2)Perusahaan akan mentransfer jumlah yang wajib dibayarkan ke<b><i> Rekening Bank Permata Syariah Nomor &nbsp;&nbsp;&nbsp;&nbsp; 007 0178 9040 a.n. PT Kapital Boost Indonesia. </i></b> &nbsp; Transfer penuh harus dilakukan pada atau sebelum tanggal jatuh tempo. Jika tidak, Kapital Boost akan menyetor Bilyet Giro yang dikeluarkan oleh Perusahaan kepada PT Kapital Boost Indonesia.", $y + 5);

$y = Left1($pdf, "<b>Admin fee: </b> &nbsp; Kapital Boost will take $subs_agency_fee% of the funding raised for Crowdfunding Administration Fee. Assuming the funding target is met, the Crowdfunding Administration Fee equals to SGD $admin_fee_amt. This fee is to be paid to Kapital Boost before the Qard proceeds from Funders are transferred to the Company. <br>< /br>", $y + 5);

$y = Left1($pdf, "<b>Crowdfunding period: </b> &nbsp; Seven (7) days after the Company’s crowdfunding campaign is launched on Kapital Boost crowdfunding platform or once the target funding amount is met, whichever is shorter. The Company has the option to extend the crowdfunding period by an additional 7 days if the target funding amount is not met in the first 7 days. <br>< /br>", $y + 5);

$y = Left1($pdf, "<b>Funds transfer: </b> &nbsp; The Company agrees to designate Kapital Boost as an Agent to collect funds from Funders for the sole purpose of transferring funds to the Company’ bank account. The Company also agrees to designate Kapital Boost as an Agent to distribute the repayment of loan principal and Agency fees to Funders. No fees will be provided for this Agency service.", $y + 5);

$y = Left1($pdf, "<b>Transfer fees: </b> &nbsp; Funders will bear the charges associated with transferring funds to Kapital Boost, if any. Kapital Boost will bear charges associated with transferring funds to the Company. Fees incurred when returning the loan principal and paying the Agency fees to Funders (via Kapital Boost) will be borne by the Company. <br>< /br>", $y + 5);

$y = Left1($pdf, "<b>Exchange rate risk: </b> &nbsp; The Company agrees to bear the exchange rate risk in relation to the repayment of loan principal and Agency fees to Funders and Kapital Boost. <br>< /br>", $y + 5);

$y = Left1($pdf, "<b>Governing law and dispute resolutions: </b> &nbsp;&nbsp; This Agreement shall be governed by and interpreted and construed in accordance with the laws of the Republic of Indonesia.  ", $y + 5);

$y = Left1($pdf, "The Parties agree that if any difference, dispute, conflict or controversy (a 'Dispute'), arises out of or in connection with this Agreement or its performance, including without limitation any dispute regarding its existence, validity, or termination of rights or obligations of any Party, the Parties will attempt to settle the Dispute by amicable settlement between the Parties. <br>< /br><br>< /br>", $y + 5);

$y = Left1($pdf, "If the Parties are unable to reach agreement to settle the Dispute, the Parties agree to settle the Dispute through the District Court in the DKI Jakarta Province in civil or criminal cases and waive Article 55 of Act Number 21 of 2008 and Article 49 of Law Number 3 of 2006.  <br>< /br><br>< /br>", $y + 5);

$y = Left1($pdf, "In the event that there are costs incurred for the purposes of resolving disputes such as legal costs, curators and all other costs related to the dispute resolution process, these costs will be borne by the Company and charged as included in the debt to be paid. <br>< /br>", $y + 5);

$y = Left1($pdf, "THEREFORE, the Parties to this Agreement have officially signed this Agreement on the date at the beginning of this Agreement.", $y + 5);

// $y = Left1($pdf, "<br>< /br><br>< /br><br>", $y + 5);
// $y = Left1($pdf, "<br>< /br><br>< /br><br>", $y + 5);
// $y = Left1($pdf, "<br></br>", $y + 5);

$pdf->AddPage();

$y = 20;

$y = Left01($pdf, "SIGNED for and on behalf of / DITANDATANGANI untuk dan atas nama ", $y + 5);

$y = Left01($pdf, "<b>$companyName</b> ", $y + 2);

$y = Left1($pdf, "<br>< /br><br>< /br><br>", $y + 5);

$y = Left1($pdf, "<b>_____________________________________</b>", $y + 3);

$y = Left1($pdf, "<b>Name / Nama : </b> $signee_1  ", $y + 1);

$y = Left1($pdf, "<b>KTP No. / Nomor KTP : </b> $id_number_1 ", $y + 1);

$y = Left1($pdf, "<b>Designation / Jabatan : </b>&nbsp; $position_1  ", $y + 1);

//$y = Left1($pdf, "<br>< /br><br>< /br><br>", $y + 5);

$y = Left01($pdf, "<br>< /br><br>< /br> SIGNED for and on behalf of / DITANDATANGANI untuk dan atas nama ", $y + 2);

$y = Left01($pdf, "<b>Kapital Boost Pte Ltd</b> ", $y + 2);

$y = Left1($pdf, "<br>< /br><br>< /br><br>", $y + 5);

$y = Left1($pdf, "<b>_____________________________________</b>", $y + 3);

$y = Left1($pdf, "<b>Name / Nama : </b> Erlangga Witoyo  ", $y + 1);

$y = Left1($pdf, "<b>Designation / Jabatan : </b>&nbsp; CEO ", $y + 1);


$y = 68.5;

$y = Right1($pdf, "<b>_____________________________________</b>", $y + 9);

$y = Right1($pdf, "<b>Name / Nama : </b> $signee_2 ", $y + 1);

$y = Right1($pdf, "<b>KTP No. / Nomor KTP : </b> $id_number_2  ", $y + 1);

$y = Right1($pdf, "<b>Designation / Jabatan : </b>&nbsp; $position_2 ", $y + 1);

// $y = Right1($pdf, "<br></br><br></br><br><br></br><br></br><br>", $y + 5);

// $y = Right1($pdf, "<br></br><br></br><br><br></br><br></br><br>", $y + 5);

// $y = Right1($pdf, "<br></br><br></br><br><br></br><br></br><br>", $y + 5);

// $y = Right1($pdf, "<br></br><br></br><br><br></br><br></br><br>", $y + 5);


// ------------ HALAMAN LAMPIRAN ------------ //

// $pdf->AddPage();

// $y = Left01($pdf, "<b>APPENDIX 1: Invoice from the Company to its Customer/(s)</b> ", 20);
// $y = Left01($pdf, "<b>LAMPIRAN 1: Kuitansi dari Perusahaan kepada Bowheer</b> ", 27);

// $y = Left1($pdf, "<br>", $y - 8);

// $lampiranTbl= "<table cellspacing=\"0\" cellpadding=\"7\" border=\"0.5\">
// <tr>
//   <td width=\"7%\" style=\"text-align:center\"><b>No</b></td>
//   <td width=\"18%\" style=\"text-align:center\"><b>Name of Customer &nbsp; <i>Nama Bowheer</i> </b><br>&nbsp;</td>
//   <td width=\"15%\" style=\"text-align:center\"><b>Date of invoice &nbsp; <i>Tanggal invoice</i></b>&nbsp;</td>
//   <td width=\"30%\" style=\"text-align:center\"><b>Invoice No. &nbsp; <i>Nomor invoice</i> </b>&nbsp;</td>
//   <td width=\"15%\" style=\"text-align:center\"><b>Maturity Date &nbsp; <i>Tanggal jatuh tempo</i></b>&nbsp;</td>
//   <td width=\"15%\" style=\"text-align:center\"><b>Amount (Rp.) &nbsp; <i>Nominal</i></b>&nbsp;</td>
// </tr>
// <tr>
//   <td>1</td>
//   <td>PT Telkom Akses</td>
//   <td>27-Aug-19</td>
//   <td> 137/INV-TELPA/JKT/VIII/2019 </td>
//   <td>25-Nov-19</td>
//   <td>72,572,500 </td>
// </tr>
// <tr>
//   <td>2</td>
//   <td>PT Telkom Akses</td>
//   <td>27-Aug-19</td>
//   <td> 136/INV-TELPA/JKT/VIII/2019 </td>
//   <td>25-Nov-19</td>
//   <td>72,572,500 </td>
// </tr>
// <tr>
//   <td>3</td>
//   <td>PT Telkom Akses</td>
//   <td>10-Aug-19</td>
//   <td> 134/INV-TELPA/JKT/VIII/2019 </td>
//   <td>10-Nov-19</td>
//   <td>237,048,900 </td>
// </tr>
// <tr>
//   <td>4</td>
//   <td>PT Telkom Akses</td>
//   <td>30-Jul-19</td>
//   <td> 122/INV-TELPA/JKT/VII/2019 </td>
//   <td>28-Oct-19</td>
//   <td>281,594,500 </td>
// </tr>
// </table>
// ";
// // $pdf->SetMargins(110, 10, 10);
// // $pdf->SetX(110);
// // $pdf->SetFontSize(10);
// // $pdf->writeHTML($tenorTbl1);
// // $pdf->SetFontSize(12);
// // $y = $pdf->GetY();

// if ($y > 200) { // Shift the sign table below
//   $pdf->AddPage();
//   $y = 20;
// }
// $pdf->SetMargins(10, 10, 10);
// $pdf->SetFontSize(10);
// $pdf->writeHTML($lampiranTbl);
// $pdf->SetFontSize(12);
// $y = $pdf->GetY();

// $y = Left01($pdf, "  <i> insert attachments here, Schedule A Image 1, Schedule A Image 2 etc</i>", $y + 4);

$pdf->AddPage();

$y = 18;
// $pdf->SetFontSize("11.5");

$y = Right1($pdf, "<b>LAMPIRAN 2: PROSES PENGGALANGAN DANA (CROWDFUNDING) KAPITAL BOOST</b>", $y + 5);

$y = Right1($pdf, "1.", $y + 5);

$y = Right2($pdf, "Perusahaan menandatangani <b>Perjanjian Qard</b> &nbsp; dan <b>Perjanjian Wakalah</b> &nbsp; dengan masing-masing Funder yang berkomitmen untuk kampanye crowdfunding.", $y - 7);

$y = Right1($pdf, "2.", $y + 5);

$y = Right2($pdf, "Perjanjian Qard dan Wakalah menguraikan persyaratan pembiayaan seperti jumlah pinjaman, tenor dan biaya Agensi.", $y - 7);

$y = Right1($pdf, "3.", $y + 5);

$y = Right2($pdf, "Kapital Boost mentransfer dana yang dikumpulkan dari Pemberi Dana, ke Perusahaan setelah Faktur diajukan dan semua persyaratan lainnya (untuk transfer dana dan uji tuntas) dipenuhi.", $y - 7);

$y = Right1($pdf, "4.", $y + 5);

$y = Right2($pdf, "Sebelum Tanggal Jatuh tempo Faktur, Kapital Meningkatkan kapasitasnya sebagai Sub-Agen melakukan layanan manajemen piutang sesuai dengan persyaratan yang ditetapkan dalam Perjanjian Wakalah.", $y - 7);

$y = Right1($pdf, "5.", $y + 5);

$y = Right2($pdf, "Pada Tanggal Jatuh Tempo, pelanggan melakukan pembayaran sama dengan nilai Faktur pada Perusahaan.", $y - 7);

$y = Right1($pdf, "6.", $y + 5);

$y = Right2($pdf, "Perusahaan membayar pinjaman kepada Pemberi Dana dan membayar biaya Agensi selambat-lambatnya satu setengah $tenorIna bulan setelah transfer Dana dari Pemberi Dana ke Perusahaan.", $y - 7);


$y = 18;
// $pdf->SetFontSize("11.5");

$y = Left1($pdf, "<b>APPENDIX 2 : KAPITAL BOOST CROWDFUNDING PROCESS</b>", $y + 5);

$y = Left1($pdf, "1.", $y + 5);

$y = Left2($pdf, "The Company signs a <b>Qard Agreement</b> &nbsp; and <b>Wakalah Agreement</b> &nbsp; with each Funder who commits to the crowdfunding campaign. ", $y - 7);

$y = Left1($pdf, "2.", $y + 5);

$y = Left2($pdf, "The Qard and Wakalah Agreement outline the financing terms such as loan amount, tenor and Agency fees.", $y - 7);

$y = Left1($pdf, "3.", $y + 5);

$y = Left2($pdf, "Kapital Boost transfers funds collected from Funders, to the Company after the Invoice is submitted and all other requirements (for fund transfers and due diligence) are met. ", $y - 7);

$y = Left1($pdf, "4.", $y + 5);

$y = Left2($pdf, "Prior to the Maturity Date of the Invoice, Kapital Boost in its capacity as Sub-Agent carries out the receivables management services in accordance to the terms stipulated in the Wakalah Agreement. ", $y - 7);

$y = Left1($pdf, "5.", $y + 5);

$y = Left2($pdf, "On the Maturity Date, customer makes payment equal to the value of the Invoice to the Company", $y - 7);

$y = Left1($pdf, "6.", $y + 5);

$y = Left2($pdf, "The Company repays loan to Funders and pays the Agency fees no later than one and a half $tenorEng months after the transfer of Funds from Funders to the Company.", $y - 7);



// ------------ END OF HALAMAN LAMPIRAN ------------ //


/*  ---------- END of English Version ------------ */



$pdf->AddPage();
$y = Left01($pdf, "<b>APPENDIX 1: Invoice from the Company to its Customer/(s)</b>&nbsp;", 20);
$y = Left01($pdf, "<b>LAMPIRAN 1: Kuitansi dari Perusahaan kepada Bowheer&nbsp;", 27);


foreach($data_images as $image){

  //$gambar = 'C:/xampp/htdocs/KB/kapitalboost_com/'.$image['path'] .''. $image['file_name'];
  //$gambar = "https://kapitalboost.com/assets/uploads/contract_ukm/CU_$id/" .''. $image['file_name'];
  $gambar = "/var/www/kapitalboost.com/public_html/assets/uploads/contract_ukm/CU_$id/" .''. $image['file_name'];
  // $gambar = "/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com/assets/uploads/contract_ukm/CU_$id/" .''. $image['file_name'];


  list($width1, $height1, $type1, $attr1) = getimagesize($gambar);
	$height1 = $height1/($width1/180);
	$minTheight = 200;
	if ($height1>$minTheight){
    $theight=$minTheight-30;
  }else{
    $theight=null;
  }

  $y = 30;

  $pdf->Image($gambar, 15, $y + 10, 180, $theight, '', '', '', true, 150, '', false, false, 1, false, false, false);
  $pdf->AddPage();
}

// if ($imageD1 != "" or $imageD1 != null) {
// 	list($width1, $height1, $type1, $attr1) = getimagesize($imageD1);
// 	$height1 = $height1/($width1/180);
// 	$minTheight = 250;
// 	if ($height1>$minTheight){
//     $theight=$minTheight-30;
//   }else{
//     $theight=null;
//   }

// 	$pdf->Image($imageD1, 15, $y + 10, 180, $theight, '', '', '', true, 150, '', false, false, 1, false, false, false);
// }

// if ($imageD2 != "" or $imageD2 != null) {
//    list($width2, $height2, $type2, $attr2) = getimagesize($imageD2);
//    $height2 = $height2/($width2/180);
//    if ($height2>$minTheight){$theight2=$minTheight;}else{$theight2=null;}

//    $pdf->AddPage();
//    $y = 5;
//    $pdf->Image($imageD2, 15, $y + 10, 180, $theight2, '', '', '', true, 150, '', false, false, 1, false, false, false);
// }


/* End of Eng Schedule B */

$token = "PTSERGIO";
$base_path = "/var/www/kapitalboost.com/public_html";
// $base_path = "/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com";
$pdf_path = "/assets/uploads/contract_ukm/CU_$id/";

if(!is_dir($base_path . $pdf_path)) {
  mkdir($base_path . $pdf_path, 0777, false);
}
if(!is_dir($base_path . $pdf_path.'pdf_file/')) {
  mkdir($base_path . $pdf_path.'pdf_file/', 0777, false);
}

$file_output = $pdf_path . 'pdf_file/' . $companyCode . '.pdf';

// $output = $pdf->Output('/var/www/kapitalboost.com/public_html/assets/pdf-ukm/' . $token . '.pdf', 'F');
// ob_end_clean();
// $output = $pdf->Output('C:/xampp/htdocs/KB/kapitalboost_com/assets/pdf-ukm/' . $token . '.pdf', 'F');
$output = $pdf->Output($base_path . $file_output, 'F');

if ($output) {
  $params = array('is_generated' => $output, 'pdf_file' => $file_output);
  list($result, $err, $id) = $mysql->updateDatas($params, 'tcontract_ukm', 'id', $id);

  echo json_encode(array('status' => 200, 'result' => $result, 'err' => $err, 'pdf_file' => $file_output));
}else{
  echo json_encode(array('status' => 200, 'result' => 0, 'err' => 'ops, somthing when wrong', 'pdf_file' => null));
}
/* STOP PDF */


/* Monolingual Functions */

function MCenter($pdf, $string, $y, $blue = false) {
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(25, 15);
   $width = $pdf->GetStringWidth($string);
   $x = 105 - ($width / 2);
   $pdf->SetXY($x, $y);
   $pdf->Write(8, $string);
}

function MCenter1($pdf, $string, $y) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $width = $pdf->GetStringWidth($string) - 20; // adjust <b> and </b>&nbsp;
   $x = 105 - ($width / 2);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft1($pdf, $string, $y, $x = 20) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft2($pdf, $string, $y, $x = 34) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 15, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MLeft3($pdf, $string, $y, $x = 46) { // 44
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 15, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MBullet($pdf, $y, $x = 40) {
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function MLeft4($pdf, $string, $y, $x = 54) { // 60
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft5($pdf, $string, $y, $x = 62) { // 70
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MLeft6($pdf, $string, $y, $x = 70) {
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   if ($newY > 250) {
      $pdf->AddPage('P', array('210', '297'));
      $pdf->SetMargins(25, 15);
      $newY = 10;
   }
   return $newY;
}

/* Bilingual Functions */

function CenterL($pdf, $string, $y, $blue = false) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetXY(10, $y);
   $string = "<span style=\"text-align:center;\">" . $string . "</span>";
   $pdf->writeHTML($string);
   return $pdf->GetY();
}

function BulletL($pdf, $y, $x = 10) {
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function Left1($pdf, $string, $y, $x = 10) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left1Left($pdf, $string, $y, $x = 10) {
  if ($y > 280) {
     $pdf->AddPage();
     $y = 10;
  }
  $pdf->SetMargins($x, 10, 110);
  $pdf->SetXY($x, $y);
  $string = "<span style=\"text-align:left;line-height:20px\">" . $string . "</span>";
  $pdf->writeHTML($string);
  $newY = $pdf->GetY();
  return $newY;
}

function Left01($pdf, $string, $y, $x = 10) {
  if ($y > 280) {
     $pdf->AddPage();
     $y = 10;
  }
  $pdf->SetMargins($x, 10, 10);
  $pdf->SetXY($x, $y);
  $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
  $pdf->writeHTML($string);
  $newY = $pdf->GetY();
  return $newY;
}

function Left1invo($pdf, $string, $y, $x = 10) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left2($pdf, $string, $y, $x = 20) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left2invo($pdf, $string, $y, $x = 20) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left21($pdf, $string, $y, $x = 23) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left3($pdf, $string, $y, $x = 26) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left3invo($pdf, $string, $y, $x = 20) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left4($pdf, $string, $y, $x = 34) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left4invo($pdf, $string, $y, $x = 28) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left41invo($pdf, $string, $y, $x = 23) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left5($pdf, $string, $y, $x = 42) { // 70
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function CenterR($pdf, $string, $y, $blue = false) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(110, 10, 7);
   $pdf->SetXY(110, $y);
   $string = "<span style=\"text-align:center;\">" . $string . "</span>";
   $pdf->writeHTML($string);
   return $pdf->GetY();
}

function BulletR($pdf, $y, $x = 110) {

   $pdf->SetMargins($x, 10,7);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function Right1($pdf, $string, $y, $x = 110) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right1Left($pdf, $string, $y, $x = 110) {
  if ($y > 280) {
     $pdf->AddPage();
     $y = 10;
  }
  $pdf->SetMargins($x, 10, 7);
  $pdf->SetXY($x, $y);
  $string = "<span style=\"text-align:left;line-height:20px\">" . $string . "</span>";
  $pdf->writeHTML($string);
  $newY = $pdf->GetY();
  return $newY;
}

function Right1invo($pdf, $string, $y, $x = 110) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right2($pdf, $string, $y, $x = 120) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right2invo($pdf, $string, $y, $x = 120) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right21($pdf, $string, $y, $x = 123) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right3($pdf, $string, $y, $x = 126) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right3invo($pdf, $string, $y, $x = 120) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right4($pdf, $string, $y, $x = 134) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right4invo($pdf, $string, $y, $x = 128) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right41invo($pdf, $string, $y, $x = 123) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right5($pdf, $string, $y, $x = 142) { // 70
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

?>
