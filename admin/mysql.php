<?php

class mysql {

   var $dbhost = null;
   var $dbuser = null;
   var $dbpass = null;
   var $dbname = null;
   var $conn = null;
   var $result = null;

   function __construct() {
      // production database

      if($_SERVER['REMOTE_ADDR']=='127.0.0.1') {
         // $mysqlfile = parse_ini_file('../.rDir/.MySQL.ini');
         $mysqlfile = ['dbhost' => 'localhost', 'dbuser' => 'owen', 'dbpass' => 'owen', 'dbname' => 'kapitalboost'];
      }else if($_SERVER['REMOTE_ADDR']=='::1'){
         $mysqlfile = ['dbhost' => 'localhost', 'dbuser' => 'root', 'dbpass' => "", 'dbname' => 'kapitalboost'];

      }else {
         $mysqlfile = parse_ini_file('/var/www/kapitalboost.com/.rDir/MySQL.ini');

      }
      $this->dbhost = $mysqlfile['dbhost'];
      $this->dbuser = $mysqlfile['dbuser'];
      $this->dbpass = $mysqlfile['dbpass'];
      $this->dbname = $mysqlfile['dbname'];

      // staging database
      // $this->dbhost = "localhost";
      // $this->dbuser = "kbadminsite";
      // $this->dbpass = "yqUF057OR7ZnId3SdAH0bJMuwTIcXZf";
      // $this->dbname = "state_kapitalboost";

      // local
      // $this->dbhost = "localhost";
      // $this->dbuser = "root";
      // $this->dbpass = "";
      // $this->dbname = "kapitalboost";
   }

   public function Connection() {
      $this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
      if (mysqli_connect_error()) {
         print_r(mysqli_connect_error());
         return false;
      }
      mysqli_set_charset($this->conn, 'utf8');
      return true;
   }

   // Function connect to sql

   public function CloseConnection() {
      if ($this->conn != null) {
         mysqli_close($this->conn);
      }
   }

   // Function disconnect to sql

   public function CheckUsername($email) {
      $sql = "SELECT User_Email FROM UserData WHERE email = '$email'";
      $this->result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($this->result) == 1) {
         return true;
      }
      return false;
   }

   public function Login($name, $password) {
      $id = 0;
      $sql = $this->conn->prepare("SELECT * FROM Admins WHERE Admin_Name = ?");
      $sql->bind_param("s", $name);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         if ($row['Admin_Password'] == md5($password)) {
            $id = $row['Admin_Id'];
         } else {
            $id = 0;
         }
      } else {
         $id = -1;
      }
      return $id;
   }

   public function GetTotalNumberOfMembers() {
      $sql = "SELECT COUNT(member_id) as total FROM tmember";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfMembers1() {
      $sql = "SELECT COUNT(member_id) as total FROM tmember WHERE insert_dt >= DATE(NOW()) - INTERVAL 0 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfMembers7() {
      $sql = "SELECT COUNT(member_id) as total FROM tmember WHERE insert_dt >= DATE(NOW()) - INTERVAL 7 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfMembers30() {
      $sql = "SELECT COUNT(member_id) as total FROM tmember WHERE insert_dt >= DATE(NOW()) - INTERVAL 30 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfInvestments() {
      $sql = "SELECT COUNT(id) as total FROM tinv";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfInvestments1() {
      $sql = "SELECT COUNT(id) as total FROM tinv WHERE date >= DATE(NOW()) - INTERVAL 0 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfInvestments7() {
      $sql = "SELECT COUNT(id) as total FROM tinv WHERE date >= DATE(NOW()) - INTERVAL 7 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfInvestments30() {
      $sql = "SELECT COUNT(id) as total FROM tinv WHERE date >= DATE(NOW()) - INTERVAL 30 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfGetFunded3() {
      $sql = "SELECT COUNT(getfunded_id) as total FROM tgetfunded WHERE date >= DATE(NOW()) - INTERVAL 3 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfContactUs3() {
      $sql = "SELECT COUNT(id) as total FROM contact_us WHERE date >= DATE(NOW()) - INTERVAL 3 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfEnquiry3() {
      $sql = "SELECT COUNT(id) as total FROM tenquire WHERE date >= DATE(NOW()) - INTERVAL 3 DAY";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfCampaigns() {
      $sql = "SELECT COUNT(campaign_id) as total FROM tcampaign";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfOnGoingCampaigns($type) {
      $sql = "SELECT COUNT(campaign_id) as total FROM tcampaign WHERE project_type = '$type' && DATEDIFF(expiry_date,NOW())>0";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfHowDidMemberKnow($opt) {
      $sql = "SELECT COUNT(member_id) as total FROM tmember WHERE how_you_know = '$opt'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfCampaignsType($type) {
      $sql = "SELECT COUNT(campaign_id) as total FROM tcampaign WHERE project_type = '$type'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalNumberOfClosedCampaigns() {
      $sql = "SELECT COUNT(campaign_id) as total FROM tcampaign WHERE DATEDIFF(expiry_date,NOW())<=0";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalInvestmentAmount() {
      $sql = "SELECT SUM(total_funding) as total FROM tinv";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalInvestmentAmountPerCampaign($cId) {
      $sql = "SELECT SUM(curr_funding_amt) as total FROM tcampaign WHERE campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalInvestorPerCampaign($cId) {
      $sql = "SELECT COUNT(total_funding) as total FROM tinv WHERE campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalReturnPerCampaign($cId) {
      $sql = "SELECT SUM(project_return) as total FROM tcampaign WHERE campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalInvestmentAmountPaid() {
      $sql = "SELECT SUM(total_funding) as total FROM tinv WHERE status = 'Paid'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function ExpectedPayoutPerMonth($cId,$mId) {
      $sql = "SELECT sum(bulan_$mId) as total FROM tinv INNER JOIN tmember ON tinv.member_id = tmember.member_id WHERE tinv.campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetTotalKbWallet() {
      $sql = "SELECT SUM(wallet) as total FROM tmember";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetWithdrawalRequest() {
      $sql = "SELECT count(amount) as total FROM wallet_withdraw inner join tmember where wallet_withdraw.member_id=tmember.member_id and wallet_withdraw.status=0";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetMmeberBalance($mId) {
      $sql = "SELECT wallet FROM tmember WHERE member_id = '$mId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['wallet'];
   }

   public function GetEmailHeader() {
      $sql = "SELECT content FROM temail WHERE name = 'header'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['content'];
   }

   public function GetEmailFooter() {
      $sql = "SELECT content FROM temail WHERE name = 'footer'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['content'];
   }

   public function GetEmailBody($emailId) {
      $sql = "SELECT content FROM temail WHERE name = '$emailId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['content'];
   }

   public function GetEmailSubject($emailId) {
      $sql = "SELECT title FROM temail WHERE name = '$emailId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['title'];
   }

   public function GetHowYouKnowUs($mId) {
      $sql = "SELECT how_you_know FROM tmember WHERE member_id = '$mId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['how_you_know'];
   }

   public function GetMemberStatus($mId) {
      $sql = "SELECT member_status FROM tmember WHERE member_id = '$mId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['member_status'];
   }

   public function GetMemberIdbyEmail($email) {
      $sql = "SELECT member_id FROM tmember WHERE member_email = '$email'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['member_id'];
   }


   public function GetMemberCountryNationality($nationality) {
      $sql = "SELECT country FROM tnationality WHERE nationality = '$nationality'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['country'];
   }

   public function getPostSlugBySlug($slug , $bId) {
      $query = "SELECT count(slug) as count FROM tblog WHERE slug = '$slug' and blog_id != $bId";
      $result = mysqli_query($this->conn, $query);
      return mysqli_fetch_assoc($result)['count'];
   }

   public function getPostSlugById($bId) {
      $sql = "SELECT slug FROM tblog WHERE blog_id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)["slug"];
   }

   public function GetCountryCaps($name) {
      $sql = "SELECT nicename FROM tcountry WHERE name = '$name'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['nicename'];
   }

   public function GetUnpaidPayout($cId, $month) {
      $sql = "SELECT count(tinv.status_$month) as total FROM tinv INNER JOIN tmember ON tinv.member_id = tmember.member_id WHERE tinv.campaign_id = '$cId' and tinv.status_$month != 2";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result)['total'];
   }

   public function GetIdNull() {
      $ids = $email = array();
      $sql = "SELECT id,email FROM tinv WHERE member_id IS NULL";
      $result = mysqli_query($this->conn, $sql);
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
         $ids[$i] = $row["id"];
         $email[$i] = $row["email"];
         $i++;
      }
      return array($ids, $email);
   }

   public function GetMemberInvestAmount($member_id, $campaign_id) {
      $sql = "SELECT total_funding FROM tinv WHERE member_id ='$member_id' and campaign_id='$campaign_id' ";
      $result = mysqli_query($this->conn, $sql);
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
         $total_funding = $row["total_funding"];
      }
      return array($total_funding);
   }

   public function GetCampaignExpiryDate($campaign_id) {
      $sql = "SELECT expiry_date FROM tcampaign WHERE campaign_id='$campaign_id' ";
      $result = mysqli_query($this->conn, $sql);
      $i = 0;
      while ($row = mysqli_fetch_assoc($result)) {
         $exp_date = $row["expiry_date"];
      }
      return ($exp_date);
   }

   public function GetMId($id, $email) {
      $sql = $this->conn->prepare("SELECT member_id FROM tmember WHERE member_email = ?");
      $sql->bind_param("s", $email);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $mid = $row["member_id"];
         $sql1 = "UPDATE tinv SET member_id = '$mid' WHERE id = '$id'";
         mysqli_query($this->conn, $sql1);
      } else {
         echo "No MID";
      }
   }

   public function GetAllCampaignsList($keyword) {
      $cId = $cName = $projectType = $releaseDate = $closingDate = $totalFund = $minAmount = $tenor = $cReturn = $currentFund = array();
      $sql = "SELECT  campaign_id, campaign_name, release_date, expiry_date, minimum_investment, funding_summary,total_funding_amt, curr_funding_amt, project_return, project_type FROM tcampaign WHERE ( campaign_name LIKE '%$keyword%' || project_type LIKE '%$keyword%' ) ORDER BY campaign_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $cId[$i] = $row['campaign_id'];
            $cName[$i] = $row['campaign_name'];
            $projectType[$i] = $row['project_type'];
            $releaseDate[$i] = $row['release_date'];
            $closingDate[$i] = $row['expiry_date'];
            $totalFund[$i] = $row['total_funding_amt'];
            $minAmount[$i] = $row['minimum_investment'];
            $tenor[$i] = $row['funding_summary'];
            $cReturn[$i] = $row['project_return'];
            $currentFund[$i] = $row['curr_funding_amt'];
            $i++;
         }
         return Array($cId, $cName, $projectType, $releaseDate, $closingDate, $totalFund, $minAmount, $tenor, $cReturn, $currentFund);
      }
   }

   public function GetSingleCampaignList($cId) {
      $sql = $this->conn->prepare("SELECT * FROM tcampaign WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $cName = $row["campaign_name"];
         $cSnippet = $row["campaign_snippet"];
         $cSnippetId = $row["campaign_snippet_id"];
         $cDes = $row["campaign_description"];
         $cDesId = $row["campaign_description_id"];
         $rDate = $row["release_date"];
         $eDate = $row["expiry_date"];
         $release_time = $row["release_time"];
         $contract = $row["contract"];
         $enabled = $row["enabled"];
         $classification = $row["classification"];
         $classificationId = $row["classification_id"];
         $country = $row["country"];
         $totalAmount = $row["total_funding_amt"];
         $totalAmountId = $row["total_funding_amt_id"];
         $industry = $row["industry"];
         $minAmt = $row["minimum_investment"];
         $minAmtId = $row["minimum_investment_id"];
         $tenor = $row["funding_summary"];
         $currentAmount = $row["curr_funding_amt"];
         $cReturn = $row["project_return"];
         $cType = $row["project_type"];
         $cSubType = $row["campaign_subtype"];
         $cSubTypeShip = $row["campaign_subtype_ship"];
         $privatePassword = $row["private_password"];
         $relatedCampaign1 = $row["related_campaign_1"];
         $relatedCampaign2 = $row["related_campaign_2"];
         $relatedCampaign3 = $row["related_campaign_3"];
         $pdfs = $row["pdfs"];
         $pdfsDes = $row["pdfs_description"];
         $imageNames = $row["Image_Name"];
         $acronim = $row["acronim"];
         $i1 = $row["image_1"];
         $i2 = $row["image_2"];
         $i3 = $row["image_3"];
         $i4 = $row["image_4"];
         $i5 = $row["image_5"];
         $i6 = $row["image_6"];
         $i7 = $row["image_7"];
         $i8 = $row["image_8"];
         $i9 = $row["image_9"];
         $i10 = $row["image_10"];
         $i11 = $row["image_11"];
         $i12 = $row["image_12"];
         $i13 = $row["image_13"];
         $i14 = $row["image_14"];
         $i15 = $row["image_15"];
         $i16 = $row["image_16"];
         $i17 = $row["image_17"];
         $i18 = $row["image_18"];
         $cLogo = $row["campaign_logo"];
         $cBackground = $row["campaign_background"];
         $risk = $row["risk"];
         $relatedc1 = $row["related_campaign_1"];
         $relatedc2 = $row["related_campaign_2"];
         $relatedc3 = $row["related_campaign_3"];
         $smeSubtype = $row["sme_subtype"];
         return Array($cName, $cSnippet, $cSnippetId, $cDes, $cDesId, $rDate, $eDate, $contract, $enabled, $classification, $classificationId, $country, $totalAmount, $totalAmountId, $industry, $minAmt, $minAmtId, $tenor, $currentAmount, $cReturn, $cType, $cSubType, $cSubTypeShip, $privatePassword, $relatedCampaign1, $relatedCampaign2, $relatedCampaign3, $pdfs, $pdfsDes, $imageNames, $i1, $i2, $i3, $i4, $i5, $i6, $i7, $i8, $i9, $i10, $i11, $i12, $i13, $i14, $i15, $i16, $i17, $i18, $cLogo, $cBackground, $risk, $relatedc1, $relatedc2, $relatedc3, $smeSubtype, $release_time, $acronim);
      } else {
         return "No Campaign";
      }
   }

   public function GetSingleCampaignName($cId) {
      $sql = $this->conn->prepare("SELECT campaign_name FROM tcampaign WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $cName = $row["campaign_name"];
         return $cName;
      }
   }

   public function GetSingleContractCampaign($cId) {
      $sql = $this->conn->prepare("SELECT total_funding_amt FROM tcampaign WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $totalFunding = $row["total_funding_amt"];
         return $totalFunding;
      }
   }

   public function GetSingleCampaignContractList($cId) {
      $sql = $this->conn->prepare("SELECT * FROM tcontract WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $conType = $row["contract_type"];
         $comName = $row["company_name"];
         $comReg = $row["company_register"];
         $comRegTypeEng = $row['company_reg_type_eng'];
         $comRegTypeIna = $row["company_reg_type_ina"];
         $purchaseAsset = $row["purchase_assets"];
         $assetCost = $row["asset_cost"];
         $salePrice = $row["sale_price"];
         $assetCostCurr = $row["asset_cost_currency"];
         $exchange = $row["exchange"];
         $profit = $row["profit"];
         $camReturn = $row["campaign_return"];
         $foreignCurrency = $row["foreign_currency"];
         $dirName = $row["dir_name"];
         $dirEmail = $row["dir_email"];
         // $komisarisEmail = $row["komisaris_email"];
         // $financeEmail = $row["finance_email"];
         $dirTitle = $row["dir_title"];
         $dirIc = $row["dir_ic"];
         $image1 = $row["image"];
         $image2 = $row["image2"];
         $image3 = $row["image3"];
         $image4 = $row["image4"];
         $image5 = $row["image5"];
         $image6 = $row["image6"];
         $image7 = $row["image7"];
         $image8 = $row["image8"];
         $image9 = $row["image9"];
         $image10 = $row["image10"];
         $image11 = $row["image11"];
         $comNameId = $row["company_name_id"];
         $comRegId = $row["company_register_id"];
         $purchaseAssetId = $row["purchase_asset_id"];
         $dirIcId = $row["dir_ic_id"];
         $closdate = $row["closdate"];
         $invoContractNo = $row["contract_no"];
         $invoAktaNo = $row["akta_no"] ?: '  ';
         $invoFeePaymentDtae = $row["fee_payment_date"];
         $invoAgencyFee = $row["agency_fee"];
         $invoMaturityDate = $row["maturity_date"];
         $invoRepaymentDate = $row["repayment_date"];
         $invoSubsAgencyFee = $row["subs_agency_fee"];
         $invoDocs = $row["underlying_doc"];
         $invoDocsId = $row["underlying_doc_id"];
         $invoAktaNoId = $row["akta_no_id"];
         $invoAssetCostSGD = $row["asset_cost_sgd"];
         $invoAssetCost = $row["asset_cost_invoice"];
         // $admin_fee_amt = $row["admin_fee_amt"];
         // $admin_fee_pct = $row["admin_fee_pct"];

         return Array($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost);
      } else {
         return "No Campaign";
      }
   }

   public function GetAllInvestorContract($cId) {
      $sql = $this->conn->prepare("SELECT * FROM tinv WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $result = $sql->get_result();
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $id[$i] = $row["id"];
            $mId[$i] = $row["member_id"];
            $name[$i] = $row["nama"];
            $email[$i] = $row["email"];
            $funding[$i] = $row["total_funding"];
            $fundingId[$i] = $row["total_funding_id"];
            $date[$i] = $row["date"];
            $token[$i] = $row["token_pp"];
            $hasContract[$i] = $row["has_contract"];
            $sentContract[$i] = $row["sent_contract"];
            $i++;
         }
         return array($id, $mId, $name, $email, $funding, $fundingId, $date, $token, $hasContract, $sentContract);
      }
   }

   public function GetSingleInvestorContract($iId) {
      $sql = $this->conn->prepare("SELECT * FROM tinv WHERE id = ?");
      $sql->bind_param("i", $iId);
      $sql->execute();
      $result = $sql->get_result();
      $row = $result->fetch_assoc();
      if (count($result) == 1) {
         $id = $row["id"];
         $mId = $row["member_id"];
         $name = $row["nama"];
         $email = $row["email"];
         $funding = $row["total_funding"];
         $fundingId = $row["total_funding_id"];
         $totalPayout = $row["expected_payout"];
         $date = $row["date"];
         $closeDate = $row["closing_date"];
         $token = $row["token_pp"];
         $hasContract = $row["has_contract"];
         $sentContract = $row["sent_contract"];
         $sign = $row["sign"];
         return array($id, $mId, $name, $email, $funding, $fundingId, $totalPayout, $date, $closeDate, $token, $hasContract, $sentContract, $sign);
      }
   }

   public function GetSingleCampaignForPayout($cId) {
      $sql = $this->conn->prepare("SELECT campaign_name, m_payout FROM tcampaign WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $cName = $row["campaign_name"];
         $mPayout = $row["m_payout"];
         return Array($cName, $mPayout);
      } else {
         return "No Campaign";
      }
   }

   public function GetCampaignPdfs($cId) {
      $sql = $this->conn->prepare("SELECT pdfs, pdfs_description FROM tcampaign WHERE campaign_id = $cId");
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $pdfs = $row["pdfs"];
         $pdfsDes = $row["pdfs_description"];
         return Array($pdfs, $pdfsDes);
      } else {
         return "no pdf";
      }
   }

   public function UpdateSingleCampaign($cId, $cName, $cSnippet, $cSnippetId, $cDes, $cDesId, $rDate, $eDate, $contract, $enabled, $classification, $classificationId, $country, $totalAmount, $totalAmountId, $industry, $minAmt, $minAmtId, $tenor, $currentAmount, $cReturn, $cType, $cSubType, $cSubTypeShip, $privatePassword, $pdfs, $pdfsDes, $imageName, $risk, $relatedc1, $relatedc2, $relatedc3,$smeSubtype,$release_time, $acronim) {
      $rDate = date('Y-m-d', strtotime($rDate));
      $eDate = date('Y-m-d', strtotime($eDate));
      $sql = "UPDATE tcampaign SET enabled = $enabled,
      campaign_name = '" . mysqli_real_escape_string($this->conn, $cName) . "',
      campaign_snippet = '" . mysqli_real_escape_string($this->conn, $cSnippet) . "',
      campaign_snippet_id = '" . mysqli_real_escape_string($this->conn, $cSnippetId) . "',
      campaign_description = '" . mysqli_real_escape_string($this->conn, $cDes) . "',
      campaign_description_id = '" . mysqli_real_escape_string($this->conn, $cDesId) . "',
      release_date = '" . mysqli_real_escape_string($this->conn, $rDate) . "',
      release_time = '" . mysqli_real_escape_string($this->conn, $release_time) . "',
      expiry_date = '" . mysqli_real_escape_string($this->conn, $eDate) . "',
      classification = '" . mysqli_real_escape_string($this->conn, $classification) . "',
      classification_id = '" . mysqli_real_escape_string($this->conn, $classificationId) . "',
      project_type = '" . mysqli_real_escape_string($this->conn, $cType) . "',
      contract = '" . $contract . "',
      campaign_subtype = '" . mysqli_real_escape_string($this->conn, $cSubType) . "',
      campaign_subtype_ship = '" . mysqli_real_escape_string($this->conn, $cSubTypeShip) . "',
      private_password = '" . mysqli_real_escape_string($this->conn, $privatePassword) . "',
      risk = '" . mysqli_real_escape_string($this->conn, $risk) . "',
      country = '" . mysqli_real_escape_string($this->conn, $country) . "',
      total_funding_amt = '" . mysqli_real_escape_string($this->conn, $totalAmount) . "',
      total_funding_amt_id = '" . mysqli_real_escape_string($this->conn, $totalAmountId) . "',
      industry = '" . mysqli_real_escape_string($this->conn, $industry) . "',
      minimum_investment = '" . mysqli_real_escape_string($this->conn, $minAmt) . "',
      minimum_investment_id = '" . mysqli_real_escape_string($this->conn, $minAmtId) . "',
      funding_summary = '" . mysqli_real_escape_string($this->conn, $tenor) . "',
      curr_funding_amt = '" . mysqli_real_escape_string($this->conn, $currentAmount) . "',
      pdfs = '" . mysqli_real_escape_string($this->conn, $pdfs) . "',
      pdfs_description = '" . mysqli_real_escape_string($this->conn, $pdfsDes) . "',
      Image_Name = '" . mysqli_real_escape_string($this->conn, $imageName) . "',
      related_campaign_1 = '" . mysqli_real_escape_string($this->conn, $relatedc1) . "',
      related_campaign_2 = '" . mysqli_real_escape_string($this->conn, $relatedc2) . "',
      related_campaign_3 = '" . mysqli_real_escape_string($this->conn, $relatedc3) . "',
      sme_subtype = '" . mysqli_real_escape_string($this->conn, $smeSubtype) . "',
      project_return = '" . mysqli_real_escape_string($this->conn, $cReturn) . "',
      acronim = '" . mysqli_real_escape_string($this->conn, $acronim) . "'
      WHERE campaign_id = " . $cId;

      $result = mysqli_query($this->conn, $sql);
      $error = mysqli_error($this->conn);
      // echo "erro = $error";
      //$insert_id = mysqli_insert_id($this->conn);
      $insert_id = $cId;
      $this->save_image($insert_id, 'campaign_logo', 'campaign_logo');
      $this->save_image($insert_id, 'campaign_background', 'campaign_background');
      $this->save_image($insert_id, 'image_1', 'image_1');
      $this->save_image($insert_id, 'image_2', 'image_2');
      $this->save_image($insert_id, 'image_3', 'image_3');
      $this->save_image($insert_id, 'image_4', 'image_4');
      $this->save_image($insert_id, 'image_5', 'image_5');
      $this->save_image($insert_id, 'image_6', 'image_6');
      $this->save_image($insert_id, 'image_7', 'image_7');
      $this->save_image($insert_id, 'image_8', 'image_8');
      $this->save_image($insert_id, 'image_9', 'image_9');
      $this->save_image($insert_id, 'image_10', 'image_10');
      $this->save_image($insert_id, 'image_11', 'image_11');
      $this->save_image($insert_id, 'image_12', 'image_12');
      $this->save_image($insert_id, 'image_13', 'image_13');
      $this->save_image($insert_id, 'image_14', 'image_14');
      $this->save_image($insert_id, 'image_15', 'image_15');
      $this->save_image($insert_id, 'image_16', 'image_16');
      $this->save_image($insert_id, 'image_17', 'image_17');
      $this->save_image($insert_id, 'image_18', 'image_18');

      if ($result) {
         return 1;
      } else {
         return 0;
      }
   }

   public function AddNewCampaign($cName, $cSnippet, $cDes, $rDate, $eDate, $enabled, $classification, $country, $totalAmount, $industry, $minAmt, $tenor, $currentAmount, $cReturn, $cType, $cSubType, $cSubTypeShip, $privatePassword, $pdfs, $pdfsDes, $risk, $relatedc1, $relatedc2, $relatedc3) {
      $rDate = date("Y-m-d", strtotime($rDate));
      $eDate = date("Y-m-d", strtotime($eDate));
      //$cNewSnippet = mysqli_real_escape_string($this->conn,$cSnippet);
      //$cDesnew = mysqli_real_escape_string($this->conn, $cDes);
      $stmt = $this->conn->prepare("INSERT INTO tcampaign (campaign_id,campaign_name,campaign_snippet,campaign_description,release_date,expiry_date,enabled,classification,country,total_funding_amt,industry,minimum_investment,funding_summary,curr_funding_amt,project_return, project_type,campaign_subtype,campaign_subtype_ship,private_password,pdfs,pdfs_description,risk,related_campaign_1,related_campaign_2,related_campaign_3) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $stmt->bind_param("isssssissisisisssssssssss", $id, $cName, $cSnippet, $cDes, $rDate, $eDate, $enabled, $classification, $country, $totalAmount, $industry, $minAmt, $tenor, $currentAmount, $cReturn, $cType, $cSubType, $cSubTypeShip, $privatePassword, $pdfs, $pdfsDes, $risk, $relatedc1, $relatedc2, $relatedc3);
      $id = "";
      $stmt->execute();
      $insert_id = mysqli_insert_id($this->conn);
      $this->save_image($insert_id, 'campaign_logo', 'campaign_logo');
      $this->save_image($insert_id, 'campaign_background', 'campaign_background');
      $this->save_image($insert_id, 'image_1', 'image_1');
      $this->save_image($insert_id, 'image_2', 'image_2');
      $this->save_image($insert_id, 'image_3', 'image_3');
      $this->save_image($insert_id, 'image_4', 'image_4');
      $this->save_image($insert_id, 'image_5', 'image_5');
      $this->save_image($insert_id, 'image_6', 'image_6');
      $this->save_image($insert_id, 'image_7', 'image_7');
      $this->save_image($insert_id, 'image_8', 'image_8');
      $this->save_image($insert_id, 'image_9', 'image_9');
      $this->save_image($insert_id, 'image_10', 'image_10');
      $this->save_image($insert_id, 'image_11', 'image_11');
      $this->save_image($insert_id, 'image_12', 'image_12');
      $this->save_image($insert_id, 'image_13', 'image_13');
      $this->save_image($insert_id, 'image_14', 'image_14');
      $this->save_image($insert_id, 'image_15', 'image_15');
      $this->save_image($insert_id, 'image_16', 'image_16');
      $this->save_image($insert_id, 'image_17', 'image_17');
      $this->save_image($insert_id, 'image_18', 'image_18');
      return $insert_id;
   }

   function save_image($cId, $file_name, $field) {
      $img_file = $_FILES[$file_name];

      if ($img_file['tmp_name']) {

         $path = $img_file['name'];
         $ext = pathinfo($path, PATHINFO_EXTENSION);

         $uploaddir .= "../assets/images/campaign";

         if (!is_dir($uploaddir)) {
            mkdir($uploaddir, 0755) or die("<b>Error</b> could not make directory [$uploaddir].");
         }
         $image_name = time() . rand(1, 10000) . "." . $ext;
         copy($img_file['tmp_name'], "$uploaddir/$image_name");

         $sql = "UPDATE tcampaign SET $field = '$image_name' WHERE campaign_id = $cId ";
         mysqli_query($this->conn, $sql);
      }
   }

   public function GetAllGetFundedList() {
      $gId = $name = $email = $mobile = $cType = $fundingAmt = $Revenue = $tenor = array();
      $sql = "SELECT *  FROM tgetfunded ORDER BY getfunded_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $gId[$i] = $row['getfunded_id'];
            $name[$i] = $row['fullname'];
            $email[$i] = $row['email'];
            $mobile[$i] = $row['mobile'];
            $cType[$i] = $row['project_type'];
            $fundingAmt[$i] = $row['funding_amount'];
            $Revenue[$i] = $row['est_an_rev'];
            $tenor[$i] = $row['funding_period'];
            $date[$i] = $row["date"];
            $i++;
         }
         return Array($gId, $name, $email, $mobile, $cType, $fundingAmt, $Revenue, $tenor, $date);
      }
   }

   public function GetAllVpBorrowList() {
      $fname = $email = $mobile = $company = $year = $currency = $revenue = $financing_type = $financing_period = array();
      $sql = "SELECT *  FROM tborrower ORDER BY id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $fname[$i] = $row['fullname'];
            $email[$i] = $row['email'];
            $mobile[$i] = $row['mobile'];
            $company[$i] = $row['company'];
            $year[$i] = $row['year_established'];
            $currency[$i] = $row['currency'];
            $revenue[$i] = $row['revenue'];
            $datetime[$i] = $row['created'];
            $financing_type[$i] = $row["financing_type"];
            $financing_period[$i] = $row["period"];
            $i++;
         }
         return Array($fname, $email, $mobile, $company, $year, $currency, $revenue, $financing_type, $financing_period, $datetime);
      }
   }

   public function GetSingleGetFundedList($gId) {
      $sql = $this->conn->prepare("SELECT * FROM tgetfunded WHERE getfunded_id = ?");
      $sql->bind_param("i", $gId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $gName = $row["fullname"];
         $email = $row["email"];
         $mobile = $row["mobile"];
         $country = $row["country"];
         $company = $row["company"];
         $industry = $row["industri"];
         $year = $row["year"];
         $currency = $row["currency"];
         $cType = $row["project_type"];
         $revenue = $row["est_an_rev"];
         $fundingAmt = $row["funding_amount"];
         $asset = $row["ass_tobe_pur"];
         $tenor = $row["funding_period"];
         $purchased = $row["have_purchased_order"];
         $purchasedOrder = $row["purchased_order"];
         $attachedFile = $row["attach_file"];
         $attachedFilePo = $row["attach_file_po"];

         return Array($gName, $email, $mobile, $country, $company, $industry, $year, $currency, $cType, $revenue, $fundingAmt, $asset, $tenor, $purchased, $purchasedOrder, $attachedFile, $attachedFilePo);
      } else {
         return "No Get Funded Person";
      }
   }

   public function GetAllInvestmentsList($category, $keyword) {
      $iId = $name = $email = $cName = $pType = $cType = $cSubType = $totalFunding = $payDate = $status = array();

      if ($category=="unpaid"){
         $sql = "SELECT id, nama, email, campaign,project_type,campaign_subtype_chosen,tipe,total_funding,date,status  FROM tinv WHERE ( nama LIKE '%$keyword%' || email LIKE '%$keyword%' || country LIKE '%$keyword%' || campaign LIKE '%$keyword%' ) && status LIKE '%Unpaid%' ORDER BY id DESC";
      } else {
         $sql = "SELECT id, nama, email, campaign,project_type,campaign_subtype_chosen,tipe,total_funding,date,status  FROM tinv WHERE ( nama LIKE '%$keyword%' || email LIKE '%$keyword%' || country LIKE '%$keyword%' || campaign LIKE '%$keyword%' ) && tipe LIKE '%$category%' ORDER BY id DESC";
      }

      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $iId[$i] = $row["id"];
            $name[$i] = $row["nama"];
            $email[$i] = $row["email"];
            $cName[$i] = $row["campaign"];
            $cType[$i] = $row["project_type"];
            $cSubType[$i] = $row["campaign_subtype_chosen"];
            $pType[$i] = $row["tipe"];
            $totalFunding[$i] = $row["total_funding"];
            $payDate[$i] = $row["date"];
            $status[$i] = $row["status"];
            $i++;
         }
         return Array($iId, $name, $email, $cName, $cType, $cSubType, $pType, $totalFunding, $payDate, $status);
      }
   }

   public function GetAllInvestmentsListJson($cat) {
      $cat_field = 'tipe';
      if (lcfirst($cat) == 'unpaid') {
         $cat_field = 'status';
      }
      $sql = "SELECT id, nama, email, campaign, project_type, tipe, sent_contract, sign_contract, document_hash, total_funding, date, status  FROM tinv WHERE $cat_field LIKE '%$cat%' ORDER BY id DESC";

      $result = mysqli_query($this->conn, $sql);

      $rows = [];
      $status = array('0' => 'Rejected', '1'=> 'Waiting to be Reviewed', '2'=> 'Approved', '3'=> 'Blacklisted');
      $labels = array('0'=> 'label-success', '1'=> 'label-warning');

      if (mysqli_num_rows($result) > 0) {
         $i = 1;

         while($row = $result->fetch_assoc()) {
            $row['no'] = $i;
            $label_class = '0';

            if (lcfirst($row['status']) == 'unpaid') {
               $label_class = '1';
            }

            $row['nama'] = "<a href='investment-detail.php?n=".$row['id']."'>".$row['nama']."</a>";
            $row['delete_button'] = "<button value='".$row['id']."' data-index='".($i-1)."' class='btn DelBtn' onClick='deleteBtn(this)'><i class='fa fa-trash' style='color:red'></i></button>";
            $row['status'] = "<label class='label ".$labels[$label_class]."'> ".ucfirst($row['status'])." </label>";

            if ( $row['sign_contract'] == 'Y' ){
               $row['sign_contracts'] = "<label class='label label-success'>Completed</label>";
            }else{
               $row['sign_contracts'] = "<label class='label label-warning'>In Process</label>";
            }

            $rows[] = $row;

            $i++;
         }
      }

      return $rows;
   }

   public function GetAllInvestmentsSimple($cId, $choice = 0) {
      $iId = $totalFunding = array();
      if ($choice == 0) {
         $sql = "SELECT id,total_funding, expected_payout FROM tinv WHERE campaign_id = '$cId'";
      } else {
         $sql = "SELECT id,total_funding, expected_payout FROM tinv WHERE campaign_id = '$cId' && no_master_payout = 0";
      }
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $iId[$i] = $row["id"];
            $totalFunding[$i] = $row["total_funding"];
            $expectedPayout[$i] = $row["expected_payout"];
            $i++;
         }
         return Array($iId, $totalFunding, $expectedPayout);
      }
   }

   function genRndString($length = 150, $chars = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') {
      if ($length > 0) {
         $len_chars = (strlen($chars) - 1);
         $the_chars = $chars{rand(0, $len_chars)};
         for ($i = 1; $i < $length; $i = strlen($the_chars)) {
            $r = $chars{rand(0, $len_chars)};
            if ($r != $the_chars{$i - 1}) {
               $the_chars .= $r;
            }
         }

         return $the_chars;
      }
   }

   public function AddInvestment($mId, $name, $email, $campaign, $cId, $pType, $amount, $date, $status) {
      $sql = $this->conn->prepare("SELECT country,project_type,expiry_date,project_return FROM tcampaign WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $country = $row["country"];
         $cType = $row["project_type"];
         $closingDate = $row["expiry_date"];
         $cReturn = $row["project_return"];
         $order_id = "A" . substr(str_shuffle(str_repeat("0123456789", 7)), 0, 7);
         $token_id = substr(str_shuffle(str_repeat("1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", 140)), 0, 140) . time();
         $expectedPayout = $amount + ($amount * $cReturn / 100);
      }
      $sql1 = $this->conn->prepare("INSERT INTO tinv (id,member_id, nama, email, campaign,campaign_id,tipe,total_funding,expected_payout,date,status,country,project_type,closing_date,order_id,token_pp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $sql1->bind_param("iisssissssssssss", $id, $mId, $name, $email, $campaign, $cId, $pType, $amount, $expectedPayout, $date, $status, $country, $cType, $closingDate, $order_id, $token_id);
      $id = "";
      return $sql1->execute();
   }

   public function GetCampaignReturnPayout($cId) {
      $sql = $this->conn->prepare("SELECT project_return,m_payout FROM tcampaign WHERE campaign_id = ?");
      $sql->bind_param("i", $cId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $pReturn = $row["project_return"];
         $mPayout = $row["m_payout"];
         return Array($pReturn, $mPayout);
      }
   }

   public function GetMmeberTransactionHistory($mId) {
      $sql = "SELECT  * from twallet_transaction where member_id like '%$mId%' order by id desc";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $transId[$i] = $row['id'];
            $transDate[$i] = $row['date'];
            $transDesc[$i] = $row['description'];
            $transStats[$i] = $row['stats'];
            $transAmount[$i] = $row['amount'];
            $status[$i] = $row['status'];
            $i++;
         }
         return Array($transDate, $transDesc, $transStats, $transAmount, $transId, $status);
      }
   }

   public function EditInvestment($iId, $name, $email, $country, $campaignName, $totalFunding, $expectedPayout, $bankName, $cType, $pType, $date, $bankAccName, $bankAccNum, $status) {
      $sql = "UPDATE tinv SET nama = '$name', email = '$email', country = '$country',campaign='$campaignName',total_funding = '$totalFunding',expected_payout='$expectedPayout',bank='$bankName',project_type='$cType',tipe='$pType',date='$date',bank_transfer_account_name='$bankAccName',bank_transfer_account_number='$bankAccNum',status='$status'"
      . "WHERE id = $iId";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function EditEvent($eId, $eName, $eDes, $eLink, $eEnabled, $eDate) {
      $sql = "UPDATE tevent SET event_name = '$eName', event_description = '$eDes',event_link = '$eLink', event_enabled = '$eEnabled',event_date = '$eDate' WHERE event_id = '$eId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateMemberWalletBalance($mId, $amount) {
      $sql = "UPDATE tmember SET wallet = '$amount' WHERE member_id = '$mId'";
      mysqli_query($this->conn, $sql);
   }

   public function EditBlog($bId, $title, $des, $enabled, $date, $slug, $tag, $lang, $meta_keywords, $meta_description) {
      $sql = "UPDATE tblog SET blog_title = '$title', blog_content = '$des',enabled = '$enabled', release_date = '$date',slug = '$slug',tag='$tag',lang='$lang', meta_keywords = '$meta_keywords', meta_description = '$meta_description' WHERE blog_id = '$bId'";
      // echo "<script>console.log( ".$sql." );</script>";
      mysqli_query($this->conn, $sql);
   }

   public function EditEvents($bId, $title, $des, $enabled, $date, $slug, $tag) {
      $sql = "UPDATE events SET event_title = '$title', event_content = '$des',enabled = '$enabled', release_date = '$date',slug = '$slug',tag='$tag' WHERE event_id = '$bId'";
      // echo "<script>console.log( ".$sql." );</script>";
      mysqli_query($this->conn, $sql);
   }

   public function GetSinglePayout($iId) {
      $amount = $date = $status = array();
      $sql = $this->conn->prepare("SELECT * FROM tinv WHERE id = ?");
      $sql->bind_param("i", $iId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         for ($i = 1; $i <= 12; $i++) {
            $amount[$i] = $row["bulan_" . $i];
            $date[$i] = $row["tanggal_" . $i];
            $status[$i] = $row["status_" . $i];
         }
         return Array($amount, $date, $status);
      } else {
         return "No Payout Schedule";
      }
   }

   public function GetCampaignTotalAmount($cId) {
      $stmt = $this->conn->prepare("SELECT total_funding_amt FROM tcampaign WHERE campaign_id = ? ");
      $stmt->bind_param("i", $cId);
      $stmt->execute();
      if ($stmt->bind_result($totalFunding)) {
         if ($stmt->fetch()) {
            return $totalFunding;
         }
      }
   }

   public function GetCampaignType($cId) {
      $stmt = $this->conn->prepare("SELECT sme_subtype FROM tcampaign WHERE campaign_id = ? ");
      $stmt->bind_param("i", $cId);
      $stmt->execute();
      if ($stmt->bind_result($subtype)) {
         if ($stmt->fetch()) {
            return $subtype;
         }
      }
   }

   public function GetFirstNameFromMId($mId) {
      $stmt = $this->conn->prepare("SELECT member_firstname FROM tmember WHERE member_id = ?");
      $stmt->bind_param("i", $mId);
      $stmt->execute();
      if ($stmt->bind_result($firstname)) {
         if ($stmt->fetch()) {
            return $firstname;
         }
      }
   }

   public function GetMemberCountry($mId) {
      $stmt = $this->conn->prepare("SELECT member_country FROM tmember WHERE member_id = ?");
      $stmt->bind_param("i", $mId);
      $stmt->execute();
      if ($stmt->bind_result($memberCountry)) {
         if ($stmt->fetch()) {
            return $memberCountry;
         }
      }
   }

   public function GetSingleInvestment($n) {
      $sql = $this->conn->prepare("SELECT * FROM tinv WHERE id = ?");
      $sql->bind_param("i", $n);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $name = $row["nama"];
         $mId = $row["member_id"];
         $email = $row["email"];
         $country = $row["country"];
         $campaignName = $row["campaign"];
         $campaignId = $row["campaign_id"];
         $totalFunding = $row["total_funding"];
         $expectedPayout = $row["expected_payout"];
         $noMasterPayout = $row["no_master_payout"];
         $bankName = $row["bank"];
         $cType = $row["project_type"];
         $pType = $row["tipe"];
         $date = $row["date"];
         $images = $row["images"];
         $bankAccName = $row["bank_transfer_account_name"];
         $bankAccNum = $row["bank_transfer_account_number"];
         $status = $row["status"];
         $attachment = $row["bukti"];

         return Array($name, $mId, $email, $country, $campaignName, $campaignId, $totalFunding, $expectedPayout, $noMasterPayout, $bankName, $cType, $pType, $date, $images, $bankAccName, $bankAccNum, $status, $attachment);
      } else {
         return "No Investment found";
      }
   }

   public function GetSingleMember($m) {
      $sql = $this->conn->prepare("SELECT * FROM tmember WHERE member_id = ?");
      $sql->bind_param("i", $m);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $mUserName = $row["member_username"];
         $mEmail = $row["member_email"];
         $mFirstName = $row["member_firstname"];
         $mLastName = $row["member_surname"];
         $mIcName = $row["member_icname"];
         $mNric = $row["nric"];
         $mIcCountry = $row["ic_country"];
         $mIcOption = $row["ic_option"];
         $autoContract = $row["auto_contract"];
         $mDob = $row["dob"];
         $mAddress = $row["residental_area"];
         $mPhone = $row["member_mobile_no"];
         $mNationality = $row["member_country"];
         $mCountry = $row["current_location"];
         $cke1 = $row["cek1"];
         $cke2 = $row["cek2"];
         $cke3 = $row["cek3"];
         $cke4 = $row["cek4"];
         $cke5 = $row["cek5"];
         $cke6 = $row["cek6"];
         $cke7 = $row["cek7"];
         $cke8 = $row["cek8"];
         $cke9 = $row["cek9"];
         $cke10 = $row["cek10"];
         $cke11 = $row["cek11"];
         $cke12 = $row["cek12"];
         $nricFile = $row["nric_file"];
         $nricFileBack = $row["nric_file_back"];
         $ap = $row["address_proof"];
         return Array($mUserName, $mEmail, $mFirstName, $mLastName, $mIcName, $mNric, $mIcCountry,$mIcOption,$autoContract, $mDob, $mAddress, $mPhone, $mNationality, $mCountry, $cke1, $cke2, $cke3, $cke4, $cke5, $cke6, $cke7, $cke8, $cke9, $cke10, $cke11, $cke12, $nricFile, $nricFileBack, $ap);
      } else {
         return "No Member found";
      }
   }

   public function GetSingleMembers($m) {
      $sql = $this->conn->prepare("SELECT * FROM tmember WHERE member_id = ?");
      $sql->bind_param("i", $m);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $mUserName = $row["member_username"];
         $mEmail = $row["member_email"];
         $mFirstName = $row["member_firstname"];
         $mLastName = $row["member_surname"];
         $mIcName = $row["member_icname"];
         $mNric = $row["nric"];
         $mIcCountry = $row["ic_country"];
         $mIcOption = $row["ic_option"];
         $mIcId = $row["ic_id"];
         $autoContract = $row["auto_contract"];
         $mDob = $row["dob"];
         $mAddress = $row["residental_area"];
         $mPhone = $row["member_mobile_no"];
         $mNationality = $row["member_country"];
         $mCountry = $row["current_location"];
         $cke1 = $row["cek1"];
         $cke2 = $row["cek2"];
         $cke3 = $row["cek3"];
         $cke4 = $row["cek4"];
         $cke5 = $row["cek5"];
         $cke6 = $row["cek6"];
         $cke7 = $row["cek7"];
         $cke8 = $row["cek8"];
         $cke9 = $row["cek9"];
         $cke10 = $row["cek10"];
         $cke11 = $row["cek11"];
         $cke12 = $row["cek12"];
         $nricFile = $row["nric_file"];
         $nricFileBack = $row["nric_file_back"];
         $ap = $row["address_proof"];
         return Array($mUserName, $mEmail, $mFirstName, $mLastName, $mIcName, $mNric, $mIcCountry, $mIcOption, $mIcId, $autoContract, $mDob, $mAddress, $mPhone, $mNationality, $mCountry, $cke1, $cke2, $cke3, $cke4, $cke5, $cke6, $cke7, $cke8, $cke9, $cke10, $cke11, $cke12, $nricFile, $nricFileBack, $ap);
      } else {
         return "No Member found";
      }
   }

   public function GetSingleContractMember($mId) {
      $sql = $this->conn->prepare("SELECT * FROM tmember WHERE member_id = ?");
      $sql->bind_param("i", $mId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $icname = $row["member_icname"];
         $icCountry = $row["ic_country"];
         $icOption = $row["ic_option"];
         $icNumber = $row["nric"];
         $icID = $row["ic_id"];
         // echo "'<script>console.log(\"$mId\")</script>'";

         return Array($icname, "$icCountry $icOption No.", $icNumber, $icID, $icCountry, $icOption);
      }
   }

   public function GetSingleEvent($e) {
      $sql = $this->conn->prepare("SELECT * FROM tevent WHERE event_id = ?");
      $sql->bind_param("i", $e);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $eventName = $row["event_name"];
         $eventDes = $row["event_description"];
         $eventImg = $row["event_image"];
         $eventLink = $row["event_link"];
         $eventEnabled = $row["event_enabled"];
         $eventDate = $row["event_date"];
         return Array($eventName, $eventDes, $eventImg, $eventLink, $eventEnabled, $eventDate);
      } else {
         return "No Event found";
      }
   }

   public function GetSingleEvents($e) {
      $sql = $this->conn->prepare("SELECT * FROM events WHERE event_id = ?");
      $sql->bind_param("i", $e);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $eventName = $row["event_title"];
         $eventDes = $row["event_content"];
         $eventImg = $row["image"];
         $eventLink = $row["slug"];
         $eventEnabled = $row["enabled"];
         $eventDate = $row["release_date"];
         $eventTags = $row["tag"];
         return Array($eventName, $eventDes, $eventImg, $eventLink,$eventTags, $eventEnabled, $eventDate);
      } else {
         return "No Event found";
      }
   }

   public function GetSingleBlog($b) {
      $sql = $this->conn->prepare("SELECT * FROM tblog WHERE blog_id = ?");
      $sql->bind_param("i", $b);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $slug = $row["slug"];
         $tags = $row["tag"];
         $title = $row["blog_title"];
         $des = $row["blog_content"];
         $images = $row["image"];
         $enabled = $row["enabled"];
         $date = $row["release_date"];
         $lang = $row["lang"];
         $meta_keywords = $row["meta_keywords"];
         $meta_description = $row["meta_description"];

         return Array($slug, $tags, $title, $des, $images, $enabled, $date, $lang, $meta_keywords, $meta_description);
      } else {
         return "No Blog found";
      }
   }

   public function AddNewMember($mUserName, $mEmail, $mFirstName, $mLastName, $mPassword, $mNric, $mDob, $mAddress, $mPhone, $mNationality, $mCountry, $cke1, $cke2, $cke3, $cke4, $cke5, $cke6, $cke7, $cke8, $cke9, $cke10, $cke11, $cke12) {
      //$encryptedPassword = password_hash($mPassword, PASSWORD_DEFAULT, ['cost' => 10]);
      $encryptedPassword = md5($mPassword);
      $stmt = $this->conn->prepare("INSERT INTO tmember (member_username,Validation_Code,Validated, member_email, member_firstname, member_surname, member_password,nric,dob,residental_area,member_mobile_no,member_country,current_location,cek1,cek2,cek3,cek4,cek5,cek6,cek7,cek8,cek9,cek10,cek11,cek12) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
      $stmt->bind_param("sssssssssssssssssssssssss", $mUserName, "By ADMIN", "1", $mEmail, $mFirstName, $mLastName, $encryptedPassword, $mNric, $mDob, $mAddress, $mPhone, $mNationality, $mCountry, $cke1, $cke2, $cke3, $cke4, $cke5, $cke6, $cke7, $cke8, $cke9, $cke10, $cke11, $cke12);
      $stmt->execute();
      $mId = mysqli_insert_id($this->conn);
      return $mId;
   }

   public function AddNewBlog() {
      $stmt = $this->conn->prepare("INSERT into tblog () VALUES ()");
      $stmt->execute();
      return mysqli_insert_id($this->conn);
   }

   public function AddNewEvents() {
      $stmt = $this->conn->prepare("INSERT into events () VALUES ()");
      $stmt->execute();
      return mysqli_insert_id($this->conn);
   }

   public function AddNewMember1() {
      $stmt = $this->conn->prepare("INSERT into tmember (Validated) VALUES ('1')");
      $stmt->execute();
      return mysqli_insert_id($this->conn);
   }

   public function AddNewCampaign1() {
      $stmt = $this->conn->prepare("INSERT INTO tcampaign () VALUES ()");
      $stmt->execute();
      return mysqli_insert_id($this->conn);
   }

   public function AddNewContract1($cId, $contract_number) {
      $stmt = $this->conn->prepare("INSERT INTO tcontract (campaign_id, contract_no) VALUES ($cId, $contract_number)");
      $stmt->execute();
   }
   public function AddNewContractUKM($cId, $contract_number) {
      $result = mysqli_query($this->conn, "SELECT content, signature, appendix FROM akad WHERE type='2'");
      $akad = mysqli_fetch_array($result);
      $akad_content = $akad['content'];
      $akad_signature = $akad['signature'];
      $akad_appendix = $akad['appendix'];

      $today = date("Y-m-d");
      $sql = "INSERT INTO tcontract_ukm (campaign_id, contract_number, akad_content, akad_signature, akad_appendix_2) VALUES ('$cId', '$contract_number', '$akad_content', '$akad_signature', '$akad_appendix')";
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();

   }

   public function InsertSingleTransaction($mId, $transDesc, $transStats, $transAmount) {
      // echo "'<script>console.log(\"amount1 : ".$_POST["transAmount"]."\")</script>'";
      $stmt = $this->conn->prepare("INSERT INTO twallet_transaction (member_id,stats,amount,description,date) VALUES ('$mId','$transStats','$transAmount','$transDesc',NOW())");
      $result = $stmt->execute();

      return $result;
   }

   public function InsertTransaction($mId, $transDesc, $transStats, $transAmount, $walletBalance) {
      // echo "'<script>console.log(\"amount1 : ".$_POST["transAmount"]."\")</script>'";
      $stmt = $this->conn->prepare("INSERT INTO twallet_transaction (member_id,stats,amount,description,date) VALUES ('$mId','$transStats','$transAmount','$transDesc',NOW())");
      $stmt->execute();

      // print_r($stmt);
      // exit();

      if ($transStats=="Withdrawal" || $transStats=="Investment"  || $transStats=="Donation"){
         $walletBalance = $walletBalance - $transAmount;
      } else {
         // echo "'<script>console.log(\"amount2 : ".$_POST["transAmount"]."\")</script>'";
         $walletBalance = $walletBalance + $transAmount;
      }
      $sql = "UPDATE tmember SET wallet='$walletBalance' WHERE member_id = $mId";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function AddNewPartner1() {
      $stmt = $this->conn->prepare("INSERT INTO tpartner () VALUES ()");
      $stmt->execute();
      return mysqli_insert_id($this->conn);
   }

   public function UpdatePayout($iId, $dId, $amount, $date, $status) {
      if ($date == "") {
         $sql = "UPDATE tinv SET bulan_$dId='$amount', tanggal_$dId=NULL, status_$dId='$status' WHERE id = $iId";
      } else {
         $date = date("Y-m-d", strtotime($date));
         $sql = "UPDATE tinv SET bulan_$dId='$amount', tanggal_$dId='$date', status_$dId='$status' WHERE id = $iId";
      }
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function GetAllCountries() {
      $sql = "SELECT name FROM tcountry";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $countries[$i] = $row['name'];
            $i++;
         }
         return $countries;
      }
   }

   public function GetAllNationality() {
      $sql = "SELECT nationality FROM tnationality";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $nationalities[$i] = $row['nationality'];
            $i++;
         }
         return $nationalities;
      }
   }

   public function GetInvestmentImage($id) {
      $sql = "SELECT images FROM tinv WHERE id = $id";
      $result = mysqli_query($this->conn, $sql);
      $row = mysqli_fetch_assoc($result);
      $images = $row["images"];
      return $images;
   }

   public function GetGeneralAbout() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '14'";
      $result = mysqli_query($this->conn, $sql);

      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageHeader = $row["page_header"];
         $pageContent = $row["page_content"];
         $releaseDate = $row["release_date"];
         return Array($pageHeader, $pageContent, $releaseDate);
      }
   }

   public function GetGeneralAboutId() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '14'";
      $result = mysqli_query($this->conn, $sql);

      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageHeader = $row["page_header_id"];
         $pageContent = $row["page_content_id"];
         $releaseDate = $row["release_date"];
         return Array($pageHeader, $pageContent, $releaseDate);
      }
   }

   public function GetGeneralContactus() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '12'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageContent = $row["page_content"];
         $releaseDate = $row["release_date"];
         return Array($pageContent, $releaseDate);
      }
   }

   public function GetGeneralFAQs() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '15'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageHeader = $row["page_header"];
         $pageContent = $row["page_content"];
         $releaseDate = $row["release_date"];
         return Array($pageHeader, $pageContent, $releaseDate);
      }
   }

   public function GetGeneralContent($cid) {
      $sql = "SELECT * FROM tcampaign WHERE campaign_id = '$cid'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageContent = $row["campaign_description"];
         return Array($pageContent);
      }
   }

   public function GetGeneralFAQsId() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '15'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageHeader = $row["page_header_id"];
         $pageContent = $row["page_content_id"];
         $releaseDate = $row["release_date"];
         return Array($pageHeader, $pageContent, $releaseDate);
      }
   }

   public function GetGeneralLegal() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '16'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageContent = $row["page_content"];
         $releaseDate = $row["release_date"];
         return Array($pageContent, $releaseDate);
      }
   }

   public function GetGeneralCareer() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '17'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageContent = $row["page_content"];
         $releaseDate = $row["release_date"];
         return Array($pageContent, $releaseDate);
      }
   }

   public function GetGeneralLegalId() {
      $sql = "SELECT * FROM tgeneral_page WHERE page_id = '16'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $pageContent = $row["page_content_id"];
         $releaseDate = $row["release_date"];
         return Array($pageContent, $releaseDate);
      }
   }

   public function GetPartners() {
      $pId = $pName = $pDes = $pImg = $pUrl = $enabled = array();
      $sql = "SELECT * FROM tpartner ORDER BY partner_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $pId[$i] = $row["partner_id"];
            $pName[$i] = $row["partner_name"];
            $pDes[$i] = $row["partner_description"];
            $pImg[$i] = $row["partner_image"];
            $pUrl[$i] = $row["partner_url"];
            $enabled[$i] = $row["enabled"];
            $i++;
         }
         return Array($pId, $pName, $pDes, $pImg, $pUrl, $enabled);
      }
   }

   public function GetAllMembers($keyword) {
      $sql = "SELECT * FROM tmember WHERE member_username LIKE '%$keyword%' || member_firstname LIKE '%$keyword%' || member_surname LIKE '%$keyword%' || member_email LIKE '%$keyword%' ORDER BY member_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $mId[$i] = $row["member_id"];
            $validated[$i] = $row["Validated"];
            $mUserName[$i] = $row["member_username"];
            $mFullname[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mEmail[$i] = $row["member_email"];
            $mCountry[$i] = $row["current_location"];
            $mCreatedDate[$i] = $row["insert_dt"];
            $mWallet[$i] = $row["wallet"];
            $member_status[$i] = $row["member_status"];
            $phone_number[$i] = $row["member_mobile_no"];
            $i++;
         }
         return Array($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet, $member_status, $phone_number);
      }
   }

   public function GetAllMembersJson() {
      $sql = "SELECT member_id, member_username, Validated, CONCAT(member_firstname, ' ', member_surname) as full_name, member_email,current_location, insert_dt, wallet, member_status, member_mobile_no FROM tmember ORDER BY member_id DESC";

      $result = mysqli_query($this->conn, $sql);

      $rows = [];
      $status = array('0' => 'New', '1'=> 'Waiting to be Reviewed', '2'=> 'Approved', '3'=> 'Blacklisted', '4' => 'Rejected');

      if (mysqli_num_rows($result) > 0) {
         $i = 1;

         while($row = $result->fetch_assoc()) {
            $row['no'] = $i;
            $row['delete_button'] = "<button value='".$row['member_id']."' class='btn DelBtn' onClick='deletMember(this)'><i class='fa fa-trash' style='color:red'></i></button>";

            $row['wallet'] = "<a href='wallet.php?m=".$row['member_id']."'>$ ".number_format($row['wallet'],2)."</a>";

            $row['full_name'] = "<a href='member-edit.php?m=".$row['member_id']."'>".$row['full_name']."</a>";


            $row['member_status'] = $status[$row['member_status']];
            $rows[] = $row;

            $i++;
         }
      }

      return $rows;
   }

   public function ExtractAllMembers() {
      $sql = "SELECT * FROM tmember  ORDER BY member_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $mId[$i] = $row["member_id"];
            $mFullname[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mEmail[$i] = $row["member_email"];
            $mCountry[$i] = $row["current_location"];
            $mCreatedDate[$i] = $row["insert_dt"];
            $mHowYouKnow[$i] = $row["how_you_know"];
            $cek1[$i] = $row["cek1"];
            $cek2[$i] = $row["cek2"];
            $cek3[$i] = $row["cek3"];
            $cek4[$i] = $row["cek4"];
            $cek5[$i] = $row["cek5"];
            $cek6[$i] = $row["cek6"];
            $cek7[$i] = $row["cek7"];
            $cek8[$i] = $row["cek8"];
            $cek9[$i] = $row["cek9"];
            $cek10[$i] = $row["cek10"];
            $cek11[$i] = $row["cek11"];
            $cek12[$i] = $row["cek12"];
            $cek13[$i] = $row["cek13"];
            $mUsername[$i] = $row["member_username"];
            $gender[$i] = $row["member_gender"];
            $mobile[$i] = $row["member_mobile_no"];
            $dob[$i] = $row["dob"];
            $member_status[$i] = $row["member_status"];
            $i++;
         }
         return Array($mId, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mHowYouKnow, $cek1, $cek2, $cek3, $cek4, $cek5, $cek6, $cek7, $cek8, $cek9, $cek10, $cek11, $cek12, $cek13, $mUsername, $gender, $mobile, $dob, $member_status);
      }
   }
   public function ExtractAllMonitoringPayout() {
      $sql = "SELECT * FROM tcampaign  ORDER BY campaign_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $campaignName[$i] = $row["campaign_name"];
            $smeSubtype[$i] = $row["campaign_subtype"];
            $projectType[$i] = $row["project_type"];
            $totalFundingAmt[$i] = $row["total_funding_amt"];
            $fundingSummary[$i] = $row["funding_summary"];
            $projectReturn[$i] = $row["project_return"];
            $expiryDate[$i] = $row["expiry_date"];
            $mPayout[$i] = $row["m_payout"];
            $i++;
         }
         return Array($campaignName, $smeSubtype, $projectType, $totalFundingAmt, $fundingSummary, $projectReturn, $expiryDate, $mPayout);
      }
   }

   public function ExtractAllInvestments() {
      $sql = "SELECT * FROM tinv a join tcampaign b where a.campaign_id = b.campaign_id  ORDER BY a.id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $invId[$i] = $row["id"];
            $campaignName[$i] = $row["campaign"];
            $campaignType[$i] = $row["project_type"];
            $campaignTotalFunding[$i] = $row["total_funding_amt"];
            $campaignReturn[$i] = $row["project_return"];
            $campaignRisk[$i] = $row["risk"];
            $investorName[$i] = $row["nama"];
            $investorEmail[$i] = $row["email"];
            $paymentType[$i] = $row["tipe"];
            $invAmount[$i] = $row["total_funding"];
            $dateOfPayment[$i] = $row["date"];
            $paymentStatus[$i] = $row["status"];
            $expectedPayout[$i] = $row["expected_payout"];
            $payoutB1[$i] = $row["bulan_1"];
            $tglB1[$i] = $row["tanggal_1"];
            $statB1[$i] = $row["status_1"];
            $payoutB2[$i] = $row["bulan_2"];
            $tglB2[$i] = $row["tanggal_2"];
            $statB2[$i] = $row["status_2"];
            $payoutB3[$i] = $row["bulan_3"];
            $tglB3[$i] = $row["tanggal_3"];
            $statB3[$i] = $row["status_3"];
            $payoutB4[$i] = $row["bulan_4"];
            $tglB4[$i] = $row["tanggal_4"];
            $statB4[$i] = $row["status_4"];
            $payoutB5[$i] = $row["bulan_5"];
            $tglB5[$i] = $row["bulan_5"];
            $statB5[$i] = $row["status_5"];
            $payoutB6[$i] = $row["bulan_6"];
            $tglB6[$i] = $row["tanggal_6"];
            $statB6[$i] = $row["status_6"];
            $i++;
         }
         return Array($invId, $campaignName, $campaignType, $campaignTotalFunding, $campaignReturn, $campaignRisk, $investorName, $investorEmail, $paymentType, $invAmount, $dateOfPayment, $paymentStatus, $expectedPayout, $payoutB1, $tglB1, $statB1, $payoutB2, $tglB2, $statB2, $payoutB3, $tglB3, $statB3, $payoutB4, $tglB4, $statB4, $payoutB5, $tglB5, $statB5, $payoutB6, $tglB6, $statB6);
      }
   }



   public function GetAllVpMembers($keyword) {
      $mId = $mUserName = $mFullname = $mEmail = $mCountry = $mCreatedDate = array();
      $sql = "SELECT member_id, member_username, Validated,member_firstname, member_surname, member_email,current_location, insert_dt, wallet FROM tmember WHERE Validation_Code like '%valuepgn%' and (member_username LIKE '%$keyword%' || member_firstname LIKE '%$keyword%' || member_surname LIKE '%$keyword%' || member_email LIKE '%$keyword%') ORDER BY member_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $mId[$i] = $row["member_id"];
            $validated[$i] = $row["Validated"];
            $mUserName[$i] = $row["member_username"];
            $mFullname[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mEmail[$i] = $row["member_email"];
            $mCountry[$i] = $row["current_location"];
            $mCreatedDate[$i] = $row["insert_dt"];
            $mWallet[$i] = $row["wallet"];
            $i++;
         }
         return Array($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet);
      }
   }

   public function GetAllLandingPageMembers($keyword) {
      $mId = $mUserName = $mFullname = $mEmail = $mCountry = $mCreatedDate = array();
      $sql = "SELECT * FROM tmember WHERE Validation_Code like '%landpage%' or Validation_Code like '%landdua%' or Validation_Code like '%landtiga%' and (member_username LIKE '%$keyword%' || member_firstname LIKE '%$keyword%' || member_surname LIKE '%$keyword%' || member_email LIKE '%$keyword%') ORDER BY member_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $mId[$i] = $row["member_id"];
            $validated[$i] = $row["Validated"];
            $mUserName[$i] = $row["member_username"];
            $mFullname[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mEmail[$i] = $row["member_email"];
            $mCountry[$i] = $row["current_location"];
            $mCreatedDate[$i] = $row["insert_dt"];
            $mWallet[$i] = $row["wallet"];
            $mValCode[$i] = $row["Validation_Code"];
            $i++;
         }
         return Array($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet, $mValCode);
      }
   }

   public function GetAllWalletMembers($keyword) {
      $mId = $mUserName = $mFullname = $mEmail = $mCountry = $mCreatedDate = array();
      $sql = "SELECT member_id, member_username, Validated,member_firstname, member_surname, member_email,current_location, insert_dt, wallet FROM tmember WHERE wallet!=0 ORDER BY member_firstname ASC";
      // echo "'<script>console.log(\"sql wallet member : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $mId[$i] = $row["member_id"];
            $validated[$i] = $row["Validated"];
            $mUserName[$i] = $row["member_username"];
            $mFullname[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mEmail[$i] = $row["member_email"];
            $mCountry[$i] = $row["current_location"];
            $mCreatedDate[$i] = $row["insert_dt"];
            $mWallet[$i] = $row["wallet"];
            $i++;
         }
         return Array($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet);
      }
   }

   public function GetEnquires() {
      $eId = $eName = $eEmail = $eProject = $eMsg = array();
      $sql = "SELECT * FROM tenquire ORDER BY id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $eId[$i] = $row["id"];
            $eName[$i] = $row["name"];
            $eEmail[$i] = $row["email"];
            $eProject[$i] = $row["project"];
            $eMsg[$i] = $row["msg"];
            $i++;
         }
         return Array($eId, $eName, $eEmail, $eProject, $eMsg);
      }
   }

   public function GetEvents() {
      $eId = $eName = $eDate = $eImage = $eDes = $eLink = $eEnabled = array();
      $sql = "SELECT * FROM tevent ORDER BY event_date DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $eId[$i] = $row["event_id"];
            $eName[$i] = $row["event_name"];
            $eDate[$i] = $row["event_date"];
            $eImage[$i] = $row["event_image"];
            $eDes[$i] = $row["event_description"];
            $eLink[$i] = $row["event_link"];
            $eEnabled[$i] = $row["event_enabled"];
            $i++;
         }
         return Array($eId, $eName, $eDate, $eImage, $eDes, $eLink, $eEnabled);
      }
   }

   public function GetMessages() {
      $eId = $eName = $eEmail = $eMsg = $date = array();
      $sql = "SELECT * FROM contact_us ORDER BY id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $eId[$i] = $row["id"];
            $eName[$i] = $row["nama"];
            $eEmail[$i] = $row["email"];
            $eMsg[$i] = $row["pesan"];
            $date[$i] = $row["date"];
            $i++;
         }
         return Array($eId, $eName, $eEmail, $eMsg, $date);
      }
   }

   public function GetBlogs() {
      $bId = $slug = $tags = $title = $des = $images = $enabled = $date = array();
      $sql = "SELECT * FROM tblog ORDER BY blog_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $bId[$i] = $row["blog_id"];
            $slug[$i] = $row["slug"];
            $tags[$i] = $row["tag"];
            $title[$i] = $row["blog_title"];
            $des[$i] = $row["blog_content"];
            $images[$i] = $row["image"];
            $enabled[$i] = $row["enabled"];
            $date[$i] = $row["release_date"];
            $lang[$i] = $row["lang"];
            $i++;
         }
         return Array($bId, $slug, $title, $des, $images, $enabled, $date, $lang);
      }
   }

   public function GetAllEvents() {
      $bId = $slug = $tags = $title = $des = $images = $enabled = $date = array();
      $sql = "SELECT * FROM events ORDER BY event_id DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $bId[$i] = $row["event_id"];
            $slug[$i] = $row["slug"];
            $tags[$i] = $row["tag"];
            $title[$i] = $row["event_title"];
            $des[$i] = $row["event_content"];
            $images[$i] = $row["image"];
            $enabled[$i] = $row["enabled"];
            $date[$i] = $row["release_date"];
            $i++;
         }
         return Array($bId, $slug, $title, $des, $images, $enabled, $date);
      }
   }

   public function GetSinglePartnerList($pId) {
      $sql = $this->conn->prepare("SELECT * FROM tpartner WHERE partner_id = ?");
      $sql->bind_param("i", $pId);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $pName = $row["partner_name"];
         $pDes = $row["partner_description"];
         $pImg = $row["partner_image"];
         $pUrl = $row["partner_url"];
         $enabled = $row["enabled"];
         return Array($pName, $pDes, $pImg, $pUrl, $enabled);
      } else {
         return "No Partner Found";
      }
   }

   public function GetAllCampaignName() {
      $sql = "SELECT campaign_name, campaign_id FROM tcampaign ORDER BY expiry_date DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $cName[$i] = $row["campaign_name"];
            $cId[$i] = $row["campaign_id"];
            $i++;
         }
         return Array($cName, $cId);
      }
   }

   public function GetAllCampaignNameAndcReturn() {
      $sql = "SELECT campaign_name, campaign_id,project_return FROM tcampaign ORDER BY expiry_date DESC";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $cName[$i] = $row["campaign_name"];
            $cId[$i] = $row["campaign_id"];
            $cReturn[$i] = $row["project_return"];
            $i++;
         }
         return Array($cName, $cId, $cReturn);
      }
   }

   public function GetUpdateCampaignList() {
      $sql = "SELECT campaign_name, campaign_id FROM tcampaign where campaign_name is not null ORDER BY campaign_id DESC";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $campaignName[$i] = $row["campaign_name"];
            $campaignID[$i] = $row["campaign_id"];
            $i++;
         }
         return Array($campaignName, $campaignID);
      }
   }
   public function GetSingleProjectUpdate($updateID) {
      $sql = "SELECT * FROM tupdate where id=$updateID";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $titles[$i] = $row["title"];
            $contents[$i] = $row["content"];
            $dates[$i] = $row["date"];
            $campaigids[$i] = $row["campaign_id"];
            $images[$i] = $row["image"];
            $images2[$i] = $row["image2"];
            $images3[$i] = $row["image3"];
            $images4[$i] = $row["image4"];
            $images5[$i] = $row["image5"];
            $images6[$i] = $row["image6"];
            $images7[$i] = $row["image7"];
            $i++;
         }
         return Array($campaigids, $titles, $contents, $dates, $images, $images2, $images3, $images4, $images5, $images6, $images7);
      }
   }

   public function GetInvestorNamesForSuggestion($term) {
      $sql = "SELECT member_id, member_firstname, member_surname,member_email FROM tmember WHERE member_firstname LIKE '%$term%' || member_surname LIKE '%$term%' ";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $mId[$i] = $row["member_id"];
            $mName[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mEmail[$i] = $row["member_email"];
            $i++;
         }
         return Array($mId, $mName, $mEmail);
      }
   }

   public function GetXfersAccessToken($mId) {
      $sql = "SELECT member_access_token FROM tmember WHERE member_id = '$mId'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) == 1) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $token = $row["member_access_token"];
            $i++;
         }
         return $token;
      } else {
         return 0;
      }
   }

   public function GetAllInvestmentsForCampaign($cId) {
      $sql1 = "SELECT total_funding_amt,project_return FROM tcampaign WHERE campaign_id = '$cId'";
      $result1 = mysqli_query($this->conn, $sql1);
      $row1 = mysqli_fetch_assoc($result1);
      $totalAmount = $row1["total_funding_amt"];
      $return1 = $row1["project_return"];

      $sql = "SELECT tinv.*, tmember.* FROM tinv INNER JOIN tmember ON tinv.member_id = tmember.member_id WHERE tinv.campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $iId[$i] = $row["id"];
            $mId[$i] = $row["member_id"];
            $mIcName[$i] = $row["member_icname"];
            $email[$i] = $row["member_email"];
            $mIcType[$i] = $row["ic_type"];
            $mNric[$i] = $row["nric"];
            $iAmount[$i] = $row["total_funding"];
            $nationality[$i] = $row["member_country"];
            $iAmountT[$i] = ($iAmount[$i] / $totalAmount) * 100;
            $iProfit[$i] = $iAmount[$i] * ($return1 / 100);
            $iReturn[$i] = $iAmount[$i] + $iProfit[$i];
            $date1[$i] = date('d F Y', strtotime($row["date"]));
            $date2[$i] = date('d F Y', strtotime($date1[$i] . ' + 3 days'));
            $i++;
         }
         return Array($iId, $mId, $mIcName, $mIcType, $mNric, $iAmount, $iAmountT, $iProfit, $iReturn, $date1, $date2,$email,$nationality);
      }
   }

   public function GetAllInvestorsForSingleCampaign($cId, $mId) {
      $sql = "SELECT tinv.*, tmember.* FROM tinv INNER JOIN tmember ON tinv.member_id = tmember.member_id WHERE tinv.campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $name[$i] = $row["member_firstname"] . " " . $row["member_surname"];
            $mobile[$i] = $row["member_mobile_no"];
            $xfersToken[$i] = $row["member_access_token"];
            $tpayout[$i] = $row["expected_payout"];
            $iId[$i] = $row["id"];
            $cName[$i] = $row["campaign"];
            $pType[$i] = $row["tipe"];
            $fundingId[$i] = $row["total_funding_id"];
            $status[$i] = $row["status"];
            $amount[$i] = $row["bulan_$mId"];
            $paid[$i] = $row["status_$mId"];
            $emailAddr[$i] = $row["email"];
            $country[$i] = $row["current_location"];
            $invoiceId[$i] = "O" . substr(str_shuffle(str_repeat("0123456789", 4)), 0, 4) . time();
            $i++;
         }
         return Array($name, $mobile, $xfersToken, $iId, $pType, $fundingId, $status, $amount, $paid, $cName, $invoiceId, $tpayout, $emailAddr, $country);
      }
   }

   public function GetAllInvestorEmailforSingleCampaign($cId) {
      $sql = "SELECT DISTINCT(tinv.email), tinv.nama, tinv.campaign FROM tinv INNER JOIN tmember ON tinv.member_id = tmember.member_id WHERE tinv.campaign_id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $name[$i] = $row["nama"];
            $cName[$i] = $row["campaign"];
            $emailAddr[$i] = $row["email"];
            $i++;
         }
         return Array($name, $cName, $emailAddr);
      }
   }

   public function DeleteCampaign($cId) {
      $sql = "DELETE FROM tcampaign WHERE campaign_id = $cId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteGetfunded($gId) {
      $sql = "DELETE FROM tgetfunded WHERE getfunded_id = $gId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteEvent($eId) {
      $sql = "DELETE FROM tevent WHERE event_id = $eId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteInvestment($iId) {
      $sql = "DELETE FROM tinv WHERE id = $iId";
      mysqli_query($this->conn, $sql);
   }

   public function DeletePartner($pId) {
      $sql = "DELETE FROM tpartner WHERE partner_id = $pId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteEnquiry($eId) {
      $sql = "DELETE FROM tenquire WHERE id = $eId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteMessages($eId) {
      $sql = "DELETE FROM contact_us WHERE id = $eId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteUpdate($eId) {
      $sql = "DELETE FROM tupdate WHERE id = $eId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteMember($mId) {
      $sql = "DELETE FROM tmember WHERE member_id = $mId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteBlog($bId) {
      $sql = "DELETE FROM tblog WHERE blog_id = $bId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteWalletTrans($wId) {
      $sql = "SELECT * FROM twallet_transaction WHERE id = $wId";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $transStats = $row['stats'];
            $amount = $row['amount'];
            $mId = $row['member_id'];
         }
      }
      $sql = "SELECT wallet FROM tmember WHERE member_id = $mId";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $walletBalance = $row['wallet'];
         }
      }
      if ($transStats=="Withdrawal" || $transStats=="Investment"  || $transStats=="Donation"){
         $walletBalance = $walletBalance + $amount;
      } else {
         $walletBalance = $walletBalance - $amount;
      }
      $sql = "UPDATE tmember SET wallet='$walletBalance' WHERE member_id = $mId";
      $result = mysqli_query($this->conn, $sql);

      $sql = "DELETE FROM twallet_transaction WHERE id = $wId";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteEvents($bId) {
      $sql = "DELETE FROM events WHERE event_id = $bId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteCampaignImage($cId, $iId) {
      $sql = "UPDATE tcampaign SET image_$iId = NULL WHERE campaign_id = $cId";
      mysqli_query($this->conn, $sql);
   }

   public function DeleteContractImage($cId, $iId) {
      if ($iId=="1"){$iId="";}
      $sql = "UPDATE tcontract SET image$iId = NULL WHERE campaign_id = $cId";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateUploadedImgInvestment($iId, $bukti) {
      $sql = "UPDATE tinv SET bukti = '$bukti' WHERE id= '$iId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateUploadedImgEvent($iId, $iName) {
      $sql = "UPDATE events SET image = '$iName' WHERE event_id = '$iId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function UpdateUploadedImgBlog($bId, $iName) {
      $sql = "UPDATE tblog SET image = '$iName' WHERE blog_id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function UpdateUploadedImgCupdate($bId, $iName) {
      $sql = "UPDATE tupdate SET image = '$iName' WHERE id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function CekUpdateCampaignDesc($desc, $cId) {
      $sql = "UPDATE tcampaign SET campaign_description = '$desc' WHERE campaign_id = $cId";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function UpdateUploadedImgCupdate2($bId, $iName) {
      $sql = "UPDATE tupdate SET image2 = '$iName' WHERE id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }
   public function UpdateUploadedImgCupdate3($bId, $iName) {
      $sql = "UPDATE tupdate SET image3 = '$iName' WHERE id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }
   public function UpdateUploadedImgCupdate4($bId, $iName) {
      $sql = "UPDATE tupdate SET image4 = '$iName' WHERE id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }
   public function UpdateUploadedImgCupdate5($bId, $iName) {
      $sql = "UPDATE tupdate SET image5 = '$iName' WHERE id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }
   public function UpdateUploadedImgCupdate6($bId, $iName) {
      $sql = "UPDATE tupdate SET image6 = '$iName' WHERE id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }
   public function UpdateUploadedImgCupdate7($bId, $iName) {
      $sql = "UPDATE tupdate SET image7 = '$iName' WHERE id = '$bId'";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function UpdateUploadedImgEvents($bId, $iName) {
      $sql = "UPDATE events SET image = '$iName' WHERE event_id = '$bId'";
      $result = mysqli_query($this->conn, $sql);
      return $result;
   }

   public function UpdateUploadedPartner($pId, $pImg) {
      $sql = "UPDATE tpartner SET partner_image = '$pImg' WHERE partner_id= '$pId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateUploadedMemberIcFront($mId, $mImg) {
      $sql = "UPDATE tmember SET nric_file = '$mImg' WHERE member_id= '$mId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateUploadedMemberIcBack($mId, $mImg) {
      $sql = "UPDATE tmember SET nric_file_back = '$mImg' WHERE member_id= '$mId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateUploadedMemberAp($mId, $mImg) {
      $sql = "UPDATE tmember SET address_proof = '$mImg' WHERE member_id= '$mId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageAboutUs($header, $content) {
      $newHeader = mysqli_real_escape_string($this->conn, $header);
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_header = '$newHeader',page_content = '$newContent' WHERE page_id=14";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageAboutUsId($header, $content) {
      $newHeader = mysqli_real_escape_string($this->conn, $header);
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_header_id = '$newHeader',page_content_id = '$newContent' WHERE page_id=14";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageContactUs($content) {
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_content = '$newContent' WHERE page_id=12";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageFAQs($header, $content) {
      $newHeader = mysqli_real_escape_string($this->conn, $header);
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_header = '$newHeader',page_content = '$newContent' WHERE page_id=15";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageFAQsId($header, $content) {
      $newHeader = mysqli_real_escape_string($this->conn, $header);
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_header_id = '$newHeader',page_content_id = '$newContent' WHERE page_id=15";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageLegal($content) {
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_content = '$newContent' WHERE page_id=16";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageCareer($content) {
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_content = '$newContent' WHERE page_id=17";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateGeneralPageLegalId($content) {
      $newContent = mysqli_real_escape_string($this->conn, $content);
      $sql = "UPDATE tgeneral_page SET page_content_id = '$newContent' WHERE page_id=16";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateSinglePartner($pId, $pName, $pDes, $pUrl, $enabled) {
      $time = date("Y-m-d H:i:s");
      $sql = "UPDATE tpartner SET partner_name = '$pName',partner_description = '$pDes',partner_url='$pUrl',enabled = '$enabled', insert_dt ='$time' WHERE partner_id = '$pId'";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateSingleMember($mId, $mUserNamef, $mFirstnamef, $mLastnamef, $mEmailf, $mIcNamef, $mIcCountryf,$mIcOptionf,$mIcIdf,$autoContractf, $mNricf, $mDobf, $mAddressf, $mPhonef, $mCountryf, $mNationalityf, $cke1f, $cke2f, $cke3f, $cke4f, $cke5f, $cke6f, $cke7f, $cke8f, $cke9f, $cke10f, $cke11f, $cke12f, $member_status) {
      $sql = "UPDATE tmember SET member_username = '$mUserNamef',member_firstname='$mFirstnamef',member_surname='$mLastnamef',member_email='$mEmailf',member_icname='$mIcNamef',ic_country='$mIcCountryf',ic_option='$mIcOptionf',ic_id='$mIcIdf',auto_contract='$autoContractf',nric='$mNricf',dob='$mDobf',residental_area='$mAddressf',member_mobile_no='$mPhonef',current_location='$mCountryf',member_country='$mNationalityf',cek1='$cke1f',cek2='$cke2f',cek3='$cke3f',cek4='$cke4f',cek5='$cke5f',cek6='$cke6f',cek7='$cke7f',cek8='$cke8f',cek9='$cke9f',cek10='$cke10f',cek11='$cke11f',cek12='$cke12f', member_status='$member_status' WHERE member_id = '$mId'";
      // echo "'<script>console.log(\"$sql\")</script>'";
      print_r(mysqli_error($this->conn));
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateSingleMemberPassword($mId, $password) {
      $password = md5($password);
      $sql = "UPDATE tmember SET member_password = '$password' WHERE member_id = $mId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateCampaignPdf($cId, $pdf, $pdfDes) {
      $sql = "UPDATE tcampaign SET pdfs = '$pdf', pdfs_description = '$pdfDes' WHERE campaign_id = '$cId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateCampaignMasterPayout($cId, $mPayout) {
      $sql = "UPDATE tcampaign SET m_payout = '$mPayout' WHERE campaign_id = '$cId'";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateCampaignIdInvestment($iId, $cId) {
      $sql = "UPDATE tinv SET campaign_id = '$cId' WHERE id = '$iId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateEachPayout($iId, $month, $amount, $date, $status) {
      if ($amount != '') {
         $datee = date("Y-m-d", strtotime($date));
         // $sql = "UPDATE tinv SET bulan_$month = '$amount', tanggal_$month = '$datee', status_$month = '$status' WHERE id = '$iId'";
         $sql = "UPDATE tinv SET bulan_$month = '$amount', tanggal_$month = '$datee' WHERE id = '$iId'";
      }else{
         // $sql = "UPDATE tinv SET bulan_$month = NULL, tanggal_$month = NULL, status_$month = NULL WHERE id = '$iId'";
         $sql = "UPDATE tinv SET bulan_$month = NULL, tanggal_$month = NULL WHERE id = '$iId'";
      }

      mysqli_query($this->conn, $sql);

   }

   public function UpdateEachPayoutStatus($iId, $mId) {
      $sql = "UPDATE tinv SET status_$mId = '2' WHERE id = '$iId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateExpectedPayout($iId, $expectedPayout) {
      $sql = "UPDATE tinv SET expected_payout = '$expectedPayout' WHERE id = '$iId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateXfersInfo($mId, $xfersInfo) {
      $sql = "UPDATE tmember SET xfers_info = '$xfersInfo' WHERE member_id = '$mId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateValidated($mId, $val) {
      $sql = "UPDATE tmember SET Validated = '$val' WHERE member_id = '$mId'";
      mysqli_query($this->conn, $sql);
   }

   public function UpdateInvestmentImage($images, $iId) {
      $sql = "UPDATE tinv SET images = '$images' WHERE id = $iId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContracts($cId, $conType, $comName, $comReg, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate,$invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) {
      $sql = "UPDATE tcontract SET contract_type='$conType',company_name='$comName',company_register='$comReg',purchase_assets='$purchaseAsset',asset_cost='$assetCost',sale_price='$salePrice',asset_cost_currency='$assetCostCurr',exchange='$exchange',profit='$profit', campaign_return='$camReturn', foreign_currency='$foreignCurrency',dir_name='$dirName',dir_email='$dirEmail',dir_title='$dirTitle',dir_ic='$dirIc',company_name_id='$comNameId',company_register_id='$comRegId',purchase_asset_id='$purchaseAssetId',dir_ic_id='$dirIcId', closdate='$closdate', contract_no='$invoContractNo', akta_no='$invoAktaNo', fee_payment_date='$invoFeePaymentDtae', agency_fee='$invoAgencyFee', maturity_date='$invoMaturityDate', repayment_date='$invoRepaymentDate', subs_agency_fee='$invoSubsAgencyFee', underlying_doc='$invoDocs',underlying_doc_id='$invoDocsId', akta_no_id='$invoAktaNoId', asset_cost_sgd='$invoAssetCostSGD', asset_cost_invoice='$invoAssetCost' WHERE campaign_id = '$cId'";
      // echo "'<script>console.log(\"$sql\")</script>'";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg1($cId, $imgName) {
      $sql = "UPDATE tcontract SET image = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg2($cId, $imgName) {
      $sql = "UPDATE tcontract SET image2 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg3($cId, $imgName) {
      $sql = "UPDATE tcontract SET image3 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg4($cId, $imgName) {
      $sql = "UPDATE tcontract SET image4 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateCampaignUpdateStatus($updateID, $titlef, $desf) {
      $sql = "UPDATE tupdate SET title = '$titlef', content='$desf' WHERE id = $updateID";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg5($cId, $imgName) {
      $sql = "UPDATE tcontract SET image5 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg6($cId, $imgName) {
      $sql = "UPDATE tcontract SET image6 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg7($cId, $imgName) {
      $sql = "UPDATE tcontract SET image7 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg8($cId, $imgName) {
      $sql = "UPDATE tcontract SET image8 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg9($cId, $imgName) {
      $sql = "UPDATE tcontract SET image9 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg10($cId, $imgName) {
      $sql = "UPDATE tcontract SET image10 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateContractImg11($cId, $imgName) {
      $sql = "UPDATE tcontract SET image11 = '$imgName' WHERE campaign_id = $cId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateInvContract($iId, $sign) {
      $sql = "UPDATE tinv SET has_contract = 1,sign='$sign' WHERE id = $iId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateSentContract($iId, $documentHash) {
      $sql = "UPDATE tinv SET sent_contract = 1, document_hash = '$documentHash' WHERE id = $iId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateSentContractSignContract($iId) {
      $sql = "UPDATE tinv SET sign_contract = 'Y' WHERE id = $iId";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateWalletStatus($id) {
      $sql = "SELECT * FROM twallet_transaction WHERE id = $id";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $transStats = $row['stats'];
            $amount = $row['amount'];
            $mId = $row['member_id'];
         }
      }
      $sql = "SELECT wallet FROM tmember WHERE member_id = $mId";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $walletBalance = $row['wallet'];
         }
      }
      if ($transStats=="Withdrawal" || $transStats=="Investment"  || $transStats=="Donation"){
         $walletBalance = $walletBalance + $amount;
      } else {
         $walletBalance = $walletBalance - $amount;
      }
      $sql = "UPDATE tmember SET wallet='$walletBalance' WHERE member_id = $mId";
      $result = mysqli_query($this->conn, $sql);

      $sql = "UPDATE twallet_transaction SET status = 0 WHERE id = $id";
      return mysqli_query($this->conn, $sql);
   }

   public function UpdateFollowMaster($iId, $val) {
      $sql = "UPDATE tinv SET no_master_payout = '$val' WHERE id = $iId";
      return mysqli_query($this->conn, $sql);
   }

   public function RefreshFunding($cId) {
      $sql = "SELECT SUM(total_funding) AS Totals FROM tinv WHERE campaign_id='$cId'";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      $row = mysqli_fetch_assoc($result);
      $totalAmount = $row["Totals"];
      if ($totalAmount==NULL or $totalAmount=="NULL"){
         $totalAmount = 0;
      }
      $sql1 = "UPDATE tcampaign SET curr_funding_amt = '$totalAmount' WHERE campaign_id = '$cId'";
      // echo "'<script>console.log(\"sql1 : $sql1\")</script>'";
      mysqli_query($this->conn, $sql1);
   }

   public function AddNewPartner($pName, $pDes, $pUrl, $enabled) {
      $stmt = $this->conn->prepare("INSERT INTO tpartner (partner_id,partner_name,partner_description,partner_url,enabled) VALUES (?,?,?,?,?)");
      $stmt->bind_param("isssi", $id, $pName, $pDes, $pUrl, $enabled);
      $id = "";
      $stmt->execute();
      $pId = mysqli_insert_id($this->conn);
      return $pId;
   }

   public function DuplicateCampaign($cId) {
      $sql1 = "INSERT INTO tcampaign (campaign_name, campaign_snippet, campaign_snippet_id, campaign_description, campaign_description_id, release_date, expiry_date, enabled, classification, classification_id, country, total_funding_amt, total_funding_amt_id, industry, minimum_investment, minimum_investment_id, funding_summary, curr_funding_amt, project_return, project_type, campaign_subtype, campaign_subtype_ship, private_password, related_campaign_3, related_campaign_1, insert_dt, contract, related_campaign_2, pdfs, pdfs_description, Image_Name, image_1, image_2, image_3, image_4, image_5, image_6, image_7, image_8, image_9, image_19, image_10, image_11, image_20, image_13, image_12, image_14, image_15, image_16, image_17, image_18, update_dt, campaign_background, risk, sme_subtype, release_time) select campaign_name, campaign_snippet, campaign_snippet_id, campaign_description, campaign_description_id, release_date, expiry_date, 0, classification, classification_id, country, total_funding_amt, total_funding_amt_id, industry, minimum_investment, minimum_investment_id, funding_summary, curr_funding_amt, project_return, project_type, campaign_subtype, campaign_subtype_ship, private_password, related_campaign_3, related_campaign_1, insert_dt, contract, related_campaign_2, pdfs, pdfs_description, Image_Name, image_1, image_2, image_3, image_4, image_5, image_6, image_7, image_8, image_9, image_19, image_10, image_11, image_20, image_13, image_12, image_14, image_15, image_16, image_17, image_18, update_dt, campaign_background, risk, sme_subtype, release_time from tcampaign where campaign_id=$cId";

      /* $sql1 = "INSERT INTO tcampaign (campaign_name, campaign_snippet, campaign_snippet_id, campaign_description, campaign_description_id, release_date, expiry_date, enabled, classification, classification_id, country, total_funding_amt, total_funding_amt_id, industry, minimum_investment, minimum_investment_id, funding_summary, curr_funding_amt, project_return, project_type, campaign_subtype, campaign_subtype_ship, private_password, related_campaign_3, related_campaign_1, insert_dt, contract, related_campaign_2, pdfs, pdfs_description, Image_Name, image_1, image_2, image_3, image_4, image_5, image_6, image_7, image_8, image_9, image_19, image_10, image_11, image_20, image_13, image_12, image_14, image_15, image_16, image_17, image_18, update_dt, campaign_logo, campaign_background, risk, m_payout, sme_subtype, release_time) select campaign_name, campaign_snippet, campaign_snippet_id, campaign_description, campaign_description_id, release_date, expiry_date, 0, classification, classification_id, country, total_funding_amt, total_funding_amt_id, industry, minimum_investment, minimum_investment_id, funding_summary, curr_funding_amt, project_return, project_type, campaign_subtype, campaign_subtype_ship, private_password, related_campaign_3, related_campaign_1, insert_dt, contract, related_campaign_2, pdfs, pdfs_description, Image_Name, image_1, image_2, image_3, image_4, image_5, image_6, image_7, image_8, image_9, image_19, image_10, image_11, image_20, image_13, image_12, image_14, image_15, image_16, image_17, image_18, update_dt, campaign_logo, campaign_background, risk, m_payout, sme_subtype, release_time from tcampaign where campaign_id=$cId"; */

      mysqli_query($this->conn, $sql1);
      $newContractId = "";
      $sql3 = "SELECT * FROM tcampaign order by campaign_id desc limit 1";
      $result = mysqli_query($this->conn, $sql3);
      if (mysqli_num_rows($result) == 1) {
         $row = mysqli_fetch_array($result);
         $newContractId = $row["campaign_id"];
      }


      $sql2 = "INSERT INTO tcontract (campaign_id, contract_type, company_name, company_register, purchase_assets, asset_cost, sale_price, asset_cost_currency, exchange, profit, campaign_return, foreign_currency, dir_name, dir_email, dir_title, dir_ic, image, company_name_id, company_register_id, purchase_asset_id, dir_ic_id, image2, image3, image4, image5, image6, image7, image8, image9, image10, image11, closdate, akta_no_id, contract_no, akta_no, fee_payment_date, agency_fee, maturity_date, repayment_date, subs_agency_fee, underlying_doc, underlying_doc_id, asset_cost_invoice, asset_cost_sgd) select $newContractId, contract_type, company_name, company_register, purchase_assets, asset_cost, sale_price, asset_cost_currency, exchange, profit, campaign_return, foreign_currency, dir_name, dir_email, dir_title, dir_ic, image, company_name_id, company_register_id, purchase_asset_id, dir_ic_id, image2, image3, image4, image5, image6, image7, image8, image9, image10, image11, closdate, akta_no_id, contract_no, akta_no, fee_payment_date, agency_fee, maturity_date, repayment_date, subs_agency_fee, underlying_doc, underlying_doc_id, asset_cost_invoice, asset_cost_sgd from tcontract where campaign_id=$cId";

      mysqli_query($this->conn, $sql2);
   }

   public function AddNewEvent($eName, $eDes, $eLink, $eEnabled, $eDate) {
      $stmt = $this->conn->prepare("INSERT INTO tevent (event_id,event_name,event_date,event_description,event_link,event_enabled) VALUES (?,?,?,?,?,?)");
      $stmt->bind_param("isssss", $id, $eName, $eDate, $eDes, $eLink, $eEnabled);
      $id = "";
      $stmt->execute();
      $eId = mysqli_insert_id($this->conn);
      return $eId;
   }

   public function AddNewUser($firstName, $email, $password, $time) {
      $sql = $this->conn->prepare("INSERT INTO User_Data (User_Id, User_F_Name, User_Email, User_Password, User_R_Date) VALUES (?,?,?,?,?)");
      $sql->bind_param("issss", $id, $firstName, $email, $password, $time);
      $id = "";

      if ($sql->execute()) {
         return true;
      } else {
         return false;
      }
   }

   public function GetUserName($id) {
      $sql = $this->conn->prepare("SELECT User_F_Name FROM User_Data WHERE User_Id = ?");
      $sql->bind_param("i", $id);
      $sql->execute();
      $this->result = $sql->get_result();
      $row = $this->result->fetch_assoc();
      if (count($this->result) == 1) {
         $userName = $row['User_F_Name'];
         return $userName;
      }
   }

   public function UpdateCampaignStatus($campaign, $titlef, $desf, $datef, $image_name, $image_name2, $image_name3, $image_name4, $image_name5, $image_name6, $image_name7) {
      $stmt = $this->conn->prepare("INSERT INTO tupdate (campaign_id, title, content, date, image, image2, image3, image4, image5, image6, image7) VALUES ('$campaign', '$titlef', '$desf', '$datef', '$image_name', '$image_name2', '$image_name3', '$image_name4', '$image_name5', '$image_name6', '$image_name7')");

      // echo "'<script>console.log(\"INSERT INTO tupdate (campaign_id, title, content, date) VALUES ('$campaign', '$titlef', '$desf', '$datef')\")</script>'";
      $stmt->execute();
      $mId = mysqli_insert_id($this->conn);
      return $mId;
   }

   public function SavePreLaunchEmail($email, $date) {
      $sql = $this->conn->prepare("INSERT INTO Pre_Launch_Registration (Pre_Email, Pre_Date) VALUES (?,?)");
      $sql->bind_param("ss", $email, $date);
      if ($sql->execute()) {
         return true;
      } else {
         return false;
      }
   }

   public function GetAllCampaignUpdatesList() {
      $campaingId = $campaignName = $campaignBackground = $updateTitle = $updateContent = $updateDate = array();
      $sql = "select * from tupdate inner join tcampaign where tupdate.campaign_id = tcampaign.campaign_id order by tupdate.id desc";
      // echo "'<script>console.log(\"sql : $sql\")</script>'";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $campaingId[$i] = $row['campaign_id'];
            $updateDate[$i] = $row['date'];
            $campaignName[$i] = $row['campaign_name'];
            $campaignBackground[$i] = $row['campaign_background'];
            $updateTitle[$i] = $row['title'];
            $updateContent[$i] = $row['content'];
            $updateids[$i] = $row['id'];

            $i++;
         }
         return Array($campaingId, $campaignName, $campaignBackground, $updateTitle, $updateContent, $updateDate, $updateids);
      }
   }

   public function prepStatement($id, $email) {
      $stmt = $this->conn->prepare("INSERT INTO Injection (id,email) VALUES (?,?)");
      $stmt->bind_param("is", $idi, $emaili);
      $idi = $id;
      $emaili = $email;
      $stmt->execute();
      if (mysqli_query($this->conn, $stmt)) {
         return true;
      } else {
         return false;
      }
   }

   public function encrypt() {
      $sql = "SELECT member_id, member_password FROM tmember";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $member_id[$i] = $row["member_id"];
            $member_password[$i] = $row["member_password"];
            $password[$i] = md5($member_password[$i]);
            $sql1 = "UPDATE tmember SET member_password = '$password[$i]' WHERE member_id = $member_id[$i]";
            mysqli_query($this->conn, $sql1);
            $i++;
         }
      }
   }

   public function UpdateCountry() {
      $sql = "SELECT member_id, member_country FROM tmember";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $country[$i] = $row["member_country"];
            $id[$i] = $row["member_id"];
            $i++;
         }
         for ($ii = 0; $ii < count($country); $ii++) {
            if ($country[$ii] == "SINGAPORE" || $country[$ii] == "Singaporean") {
               $sql1 = "UPDATE tmember SET current_location = 'SINGAPORE' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            } else if ($country[$ii] == "MALAYSIA") {
               $sql1 = "UPDATE tmember SET current_location = 'MALAYSIA' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            } else if ($country[$ii] == "INDONESIA") {
               $sql1 = "UPDATE tmember SET current_location = 'INDONESIA' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            } else if ($country[$ii] == "ALBANIA") {
               $sql1 = "UPDATE tmember SET current_location = 'ALBANIA' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            } else if ($country[$ii] == "MAURITIUS") {
               $sql1 = "UPDATE tmember SET current_location = 'MAURITIUS' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            } else if ($country[$ii] == "SOMALIA") {
               $sql1 = "UPDATE tmember SET current_location = 'SOMALIA' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            } else if ($country[$ii] == "UKRAINE") {
               $sql1 = "UPDATE tmember SET current_location = 'UKRAINE' WHERE member_id = $id[$ii]";
               $sql2 = "UPDATE tmember SET member_country = NULL WHERE member_id = $id[$ii] ";
               mysqli_query($this->conn, $sql1);
               mysqli_query($this->conn, $sql2);
            }
         }
      }
   }

   public function GetMemberDataDetail($mId)
   {
      $sql    = "SELECT bank_name, account_name, account_number, swift_code FROM tmember WHERE member_id = '$mId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result);
   }



   public function createCategory($cName, $cSlug, $cDesc)
   {

      $stmt = $this->conn->prepare("INSERT INTO tblog_category (name,slug,description) VALUES (?, ?, ?)");
      $stmt->bind_param("sss", $cName, $cSlug, $cDesc);
      $stmt->execute();
      $mId = mysqli_insert_id($this->conn);
      return $mId;
   }

   public function getAllCategories()
   {
      //        $sql = "SELECT * FROM tblog_category";
      //        $result = $this->conn->query($sql);
      //        echo 'a';
      //        return $result;
      $cId = $cName = $cSlug = $cDesc = array();
      $sql = "SELECT *  FROM tblog_category ORDER BY name";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $cId[$i] = $row['id'];
            $cName[$i] = $row['name'];
            $cSlug[$i] = $row['slug'];
            $cDesc[$i] = $row['description'];
            $i++;
         }
         return Array($cId, $cName, $cSlug, $cDesc);
      }
   }

   public function getCategory($cId)
   {
      $sql    = "SELECT name, slug, description FROM tblog_category WHERE id = '$cId'";
      $result = mysqli_query($this->conn, $sql);
      return mysqli_fetch_assoc($result);
   }

   //    public function updateCategory($cId)
   //    {
   //        $sql    = "SELECT name, slug, description FROM tblog_category WHERE id = '$cId'";
   //        $result = mysqli_query($this->conn, $sql);
   //        return mysqli_fetch_assoc($result);
   //    }

   public function updateCategory($cId, $name, $slug, $description)
   {
      $stmt = $this->conn->prepare("UPDATE tblog_category SET name = '$name',slug = '$slug',description = '$description' WHERE id = '$cId'");
      $stmt->execute();
      $cId = mysqli_insert_id($this->conn);
      return $cId;
   }

   public function deleteCategory($cId)
   {
      $sql = "DELETE FROM tblog_category WHERE id = $cId";
      mysqli_query($this->conn, $sql);
   }

   public function GetAllCampaignsListEmail() {
      // $cId = $cName = $projectType = $mPayout = $dirEmail = $komisarisEmail = $financeEmail = $companyName = array();
      $cId = $cName = $projectType = $mPayout = $dirEmail = $companyName = array();

      // $sql = "SELECT  campaign_id, campaign_name, project_type, m_payout FROM tcampaign WHERE ( project_type LIKE '%sme%' || project_type LIKE '%private%' ) AND m_payout IS NOT NULL ORDER BY campaign_id DESC";
      $sql = "SELECT a.campaign_id, campaign_name, project_type, m_payout, dir_email, company_name
      FROM tcampaign a JOIN tcontract b ON a.campaign_id = b.campaign_id
      WHERE ( project_type LIKE '%sme%' || project_type LIKE '%private%' ) AND m_payout IS NOT NULL AND dir_email IS NOT NULL ORDER BY campaign_id DESC";

      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $cId[$i] = $row['campaign_id'];
            $cName[$i] = $row['campaign_name'];
            $projectType[$i] = $row['project_type'];
            $mPayout[$i] = $row['m_payout'];
            $dirEmail[$i] = $row['dir_email'];
            // $komisarisEmail[$i] = $row['komisaris_email'];
            // $financeEmail[$i] = $row['finance_email'];
            $companyName[$i] = $row['company_name'];
            $i++;
         }
         return Array($cId, $cName, $projectType, $mPayout, $dirEmail, $companyName);
      }
   }

   public function GetContractUKM($iId) {
      $sql = $this->conn->prepare("SELECT * FROM tcontract_ukm WHERE id = ?");
      $sql->bind_param("i", $iId);
      $sql->execute();
      $result = $sql->get_result();
      $row = $result->fetch_assoc();
      if (count($result) == 1) {
         $id = $row["id"];
         $campaign_id = $row['campaign_id'];
         $contractNumber = $row["contract_number"];
         $companyName = $row["company_name"];
         $companyCode = $row["company_code"];
         $customerNameEng = $row["customer_name_eng"];
         $customerNameIna = $row["customer_name_ina"];
         $companyRegNumber = $row["company_reg_number"];
         //$dateEng = $row["date_eng"];
         //$dateIna = $row["date_ina"];
         $tenorEng = $row["tenor_eng"];
         $tenorIna = $row["tenor_ina"];
         $giroNoteEng = $row['bilyet_giro_note_eng'];
         $giroNoteIna = $row['bilyet_giro_note_ina'];
         $companyRegTypeEng = $row['company_reg_type_eng'];
         $companyRegTypeIna = $row['company_reg_type_ina'];
         $targetFundingAmt = $row['target_funding_amount'];
         $wakalahInvestor = $row['wakalah_fee_investor'];
         $wakalahKB = $row['wakalah_fee_kb'];
         $otherReqEng = $row['other_requirements_eng'];
         $otherReqIna = $row['other_requirements_ina'];
         $totalPayoutSGD = $row['total_payout_sgd'];
         $totalPayoutIDR = $row['total_payout_idr'];

         $signee_1 = $row['signee_1'];
         $signee_2 = $row['signee_2'];
         $admin_fee_pct = $row['admin_fee_pct'];
         $admin_fee_amt = $row['admin_fee_amt'];
         $pdfFile = $row['pdf_file'];
         $email_1 = $row['email_1'];
         $email_2 = $row['email_2'];
         $id_number_1 = $row['id_number_1'];
         $id_number_2 = $row['id_number_2'];
         $position_1 = $row['position_1'];
         $position_2 = $row['position_2'];

         $payment_method_eng = $row['payment_method_eng'];
         $payment_method_ina = $row['payment_method_ina'];

         $signature_page_number = $row['signature_page_number'];

         return array($id, $campaign_id, $contractNumber, $companyName, $companyCode, $customerNameEng, $customerNameIna, $companyRegNumber, $tenorEng, $tenorIna, $giroNoteEng, $giroNoteIna, $companyRegTypeEng, $companyRegTypeIna, $targetFundingAmt, $wakalahInvestor, $wakalahKB, $otherReqEng, $otherReqIna, $totalPayoutSGD, $totalPayoutIDR, $signee_1, $signee_2, $admin_fee_pct, $admin_fee_amt, $pdfFile, $email_1, $email_2, $id_number_1, $id_number_2, $position_1, $position_2, $payment_method_eng, $payment_method_ina, $signature_page_number);

      }
   }
   public function GetContractUKM2($iId) {
      $sql = $this->conn->prepare("SELECT * FROM tcontract_ukm WHERE campaign_id = ?");
      $sql->bind_param("i", $iId);
      $sql->execute();
      $result = $sql->get_result();
      $row = $result->fetch_assoc();
      if (count($result) == 1) {
         $id = $row["id"];
         $campaign_id = $row['campaign_id'];
         $contractNumber = $row["contract_number"];
         $companyName = $row["company_name"];
         $companyCode = $row["company_code"];
         $customerNameEng = $row["customer_name_eng"];
         $customerNameIna = $row["customer_name_ina"];
         $companyRegNumber = $row["company_reg_number"];
         //$dateEng = $row["date_eng"];
         //$dateIna = $row["date_ina"];
         $tenorEng = $row["tenor_eng"];
         $tenorIna = $row["tenor_ina"];
         $giroNoteEng = $row['bilyet_giro_note_eng'];
         $giroNoteIna = $row['bilyet_giro_note_ina'];
         $companyRegTypeEng = $row['company_reg_type_eng'];
         $companyRegTypeIna = $row['company_reg_type_ina'];
         $targetFundingAmt = $row['target_funding_amount'];
         $wakalahInvestor = $row['wakalah_fee_investor'];
         $wakalahKB = $row['wakalah_fee_kb'];
         $otherReqEng = $row['other_requirements_eng'];
         $otherReqIna = $row['other_requirements_ina'];
         $totalPayoutSGD = $row['total_payout_sgd'];
         $totalPayoutIDR = $row['total_payout_idr'];

         $signee_1 = $row['signee_1'];
         $signee_2 = $row['signee_2'];
         $admin_fee_pct = $row['admin_fee_pct'];
         $admin_fee_amt = $row['admin_fee_amt'];
         $pdfFile = $row['pdf_file'];
         $email_1 = $row['email_1'];
         $email_2 = $row['email_2'];
         $id_number_1 = $row['id_number_1'];
         $id_number_2 = $row['id_number_2'];
         $position_1 = $row['position_1'];
         $position_2 = $row['position_2'];
         $exchange_rate=$row['exchange_rate'];
         $payment_method_eng = $row['payment_method_eng'];
         $payment_method_ina = $row['payment_method_ina'];

         $signature_page_number = $row['signature_page_number'];

         return array($id, $campaign_id, $contractNumber, $companyName, $companyCode, $customerNameEng, $customerNameIna, $companyRegNumber, $tenorEng, $tenorIna, $giroNoteEng, $giroNoteIna, $companyRegTypeEng, $companyRegTypeIna, $targetFundingAmt, $wakalahInvestor, $wakalahKB, $otherReqEng, $otherReqIna, $totalPayoutSGD, $totalPayoutIDR, $signee_1, $signee_2, $admin_fee_pct, $admin_fee_amt, $pdfFile, $email_1, $email_2, $id_number_1, $id_number_2, $position_1, $position_2, $payment_method_eng,$exchange_rate, $payment_method_ina, $signature_page_number);

      }
   }


   function getDataContract($id) {
      $sql = $this->conn->prepare("SELECT tcontract.company_name as name, tcontract.exchange, tcontract.company_register, tcontract.subs_agency_fee, tcampaign.total_funding_amt, tcampaign.project_return, tcampaign.m_payout, tcontract_ukm.* FROM tcontract_ukm INNER JOIN tcontract ON tcontract.campaign_id = tcontract_ukm.campaign_id INNER JOIN tcampaign ON tcampaign.campaign_id = tcontract_ukm.campaign_id WHERE tcontract_ukm.campaign_id = ?");
      // $sql = $this->conn->prepare("SELECT tcontract_ukm.* FROM tcontract_ukm WHERE tcontract_ukm.campaign_id = ?");
      $sql->bind_param("i", $id);
      $sql->execute();

      $this->result = $sql->get_result();

      // $objs = $this->result->fetch_assoc();
      $objs = array();

      if ($this->result->num_rows > 0) {
         while ($row = $this->result->fetch_assoc()) {
            $objs[] = $row;
         }
      }

      return $objs;
   }

   function updateDatas($fields, $table, $field_id, $id) {
      $valueSets = array();

      foreach ($fields as $key => $value) {
         $valueSets[] = $key . " = '" . $value . "'";
      }

      $query = "UPDATE $table SET ".join(", ",$valueSets)." WHERE $field_id='$id'";
      $result = $this->conn->query($query);
      $err = $this->conn->error;

      return array($result, $err, $id);
   }

   function getUKMFiles($contract_ukm_id) {
      $sql = $this->conn->prepare("SELECT * FROM tcontract_images WHERE tcontract_ukm_id = ?");
      $sql->bind_param("i", $contract_ukm_id);
      $sql->execute();

      $this->result = $sql->get_result();

      // $objs = $this->result->fetch_assoc();
      $objs = array();

      if ($this->result->num_rows > 0) {
         while ($row = $this->result->fetch_assoc()) {
            $objs[] = $row;
         }
      }

      return $objs;
   }
   public function deleteContractUKMFile($cId) {
      $sql = "DELETE FROM tcontract_images WHERE id = $cId";
      $result = $this->conn->query($sql);
      $err = $this->conn->error;

      return array($result, $err, $cId);
   }

   public function GetHowYouKnowUs_GetFunded($cId) {
      $stmt = $this->conn->prepare("SELECT how_you_know FROM tgetfunded WHERE getfunded_id = ? ");
      $stmt->bind_param("i", $cId);
      $stmt->execute();
      if ($stmt->bind_result($howYouKnowUs)) {
         if ($stmt->fetch()) {
            return $howYouKnowUs;
         }
      }
   }

   function getDatas($table, $field_id = null, $id = null, $result_arr = false, $join = null, $join_field = null) {
      if (is_null($join)) {
         if (is_null($field_id)) {
            $query = "SELECT * FROM $table";
         }else{

               $query = "SELECT * FROM $table WHERE $field_id = $id";
         }
      }else{
         $query = "SELECT * FROM $table INNER JOIN $join ON ".$table.".".$join_field."=".$join.".".$join_field." WHERE ".$table.".".$field_id." = $id";
      }

      $sql = $this->conn->prepare($query);

      // $sql->bind_param("i", $id);

      $sql->execute();

      $this->result = $sql->get_result();

      $objs = array();

      if ($this->result->num_rows > 1 || $result_arr) {
         while ($row = $this->result->fetch_assoc()) {
            $objs[] = $row;
         }
      }else{
         $objs = $this->result->fetch_object();
      }

      return $objs;
   }

   function createAkad($type = '1') {
      $query = "INSERT INTO akad (type) VALUES ('$type')";
      $result = $this->conn->query($query);
      $err = $this->conn->error;

      return array($result, $err, $this->conn->insert_id);
   }

   function investorsStatus($campaign_id, $idx) {
      // $sql = "SELECT * FROM tinv where campaign_id = '$campaign_id' AND status_$idx = '1' OR status_$idx = 'null'";
      $sql = "SELECT * FROM tinv where campaign_id = '$campaign_id' AND (status_$idx = '1' OR status_$idx is null)";
      $result = $this->conn->query($sql);

      return $result->num_rows;
   }

   function reportPayouts($campaign_id) {
      $query = "SELECT tinv.member_id, tinv.nama, tinv.email, tinv.bulan_1, tinv.bulan_2, tinv.bulan_3,
      bank_account.account_number, bank_account.bank_name, bank_account.bank_code, bank_account.country_of_bank_account, bic_code.bank_code, bic_code.bic
      FROM tinv
      INNER JOIN bank_account ON bank_account.member_id = tinv.member_id
      INNER JOIN bic_code ON bic_code.bank_code = bank_account.bank_code
      WHERE tinv.campaign_id = '$campaign_id'";

      $sql = $this->conn->prepare($query);

      $sql->execute();

      $this->result = $sql->get_result();

      $objs = array();


      while ($row = $this->result->fetch_assoc()) {
         $objs[] = $row;
      }


      return $objs;
   }

   function GetBankCodeList() {
      $bankCode = $bankName = array();
      $sql         = "SELECT * from bic_code order by bank_name asc";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
            $bankCode[$i] = $row['bank_code'];
            $bankName[$i] = $row['bank_name'];
            $i++;
         }
         return array($bankCode, $bankName);
      }
   }

   function createBankAccount($member_id) {
      $sql = $this->conn->prepare("INSERT INTO bank_account (member_id) VALUES ('$member_id')");
      $sql->execute();
      $bank_account_id = mysqli_insert_id($this->conn);

      return $bank_account_id;
   }

}
