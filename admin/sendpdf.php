<?php

include_once 'mysql.php';
$mysql = new mysql();

$cId = $_REQUEST["c"];
$iId = $_REQUEST["i"];
$gettoken = $_REQUEST["token"];

if ($mysql->Connection()) {

   $cName = $mysql->GetSingleCampaignName($cId);
   $subtype = $mysql->GetCampaignType($cId);
   list($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) = $mysql->GetSingleCampaignContractList($cId);
   list($id, $mId, $name, $email, $funding, $fundingId, $totalPayout, $date, $closeDate, $token, $hasContract, $sentContract,$sign) = $mysql->GetSingleInvestorContract($iId);
   list($icName, $icType, $icNumber,$icId) = $mysql->GetSingleContractMember($mId);

   $title = "$cName - $icName";
   $message = "";
   $url = "https://kapitalboost.com/assets/pdfs/$token.pdf";
   if($hasContract == 0){
      echo 'no pdf file';
      exit;
   }
   $iName = $icName;
   $iEmail = $email;
   $iMessage = "";
   $oName = $dirName;
   $oEmail = $dirEmail;
   $oMessage = "";
   $kName = "Erlangga Wibisono Witoyo";
   $kEmail = "erly@kapitalboost.com";
   // $kEmail = "rief.putra@gmail.com";
   $kMessage = "";
   $signA = json_decode($sign,1);

   if ($subtype=="INVOICE  FINANCING") {
	   $inx = $signA["inx"];
	   $iny = $signA["iny"];
	   $inp = $signA["inp"];
	   $owx = $signA["owx"];
	   $owy = $signA["owy"];
	   $owp = $signA["owp"];
	   $kx = $signA["kx"];
	   $ky = $signA["ky"];
	   $kp = $signA["kp"];
	   $ix = $signA["ix"];
	   $iy = $signA["iy"];
	   $ip = $signA["ip"];
	   $ox = $signA["ox"];
	   $oy = $signA["oy"];
	   $op = $signA["op"];
   } else {
	   $ix = $signA["ix"];
	   $iy = $signA["iy"];
	   $ip = $signA["ip"];
	   $ox = $signA["ox"];
	   $oy = $signA["oy"];
	   $op = $signA["op"];
   }


   if ($gettoken != $token || $gettoken == "") {
      echo 'wrong token';
      exit;
   }

   // if($sentContract == 1){
      // echo 'contract already sent out once';
      // exit;
   // }
}

if ($subtype=="INVOICE  FINANCING") {
	// echo "'<script>console.log(\"Masuk ke invoice\")</script>'";
	$data = array(
    "is_draft" => 0,
    "embedded" => 0,
    "title" => $title,
    "message" => $message,
    "use_signer_order" => 0,
    "reminders" => 1,
    "require_all_signers" => 1,
    "files" => array(array("name" => $title, "file_url" => $url, "file_id" => "", "file_base64" => "")),
    "signers" => array(array("id" => 1, "name" => $iName, "email" => $iEmail, "message" => $iMessage), array("id" => 2, "name" => $oName, "email" => $oEmail, "message" => $oMessage), array("id" => 3, "name" => $kName, "email" => $kEmail, "message" => $kMessage)),
    "fields" => array(array(array("type" => "signature", "x" => $ix, "y" => $iy, "width" => "200", "height" => "40",
                "page" => $ip, "signer" => "1", "name" => $iName, "identifier" => "Investor_sign", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $ox, "y" => $oy, "width" => "200", "height" => "40",
                "page" => $op, "signer" => "2", "name" => $oName, "identifier" => "Owner_sign", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $kx, "y" => $ky, "width" => "200", "height" => "40",
                "page" => $kp, "signer" => "3", "name" => $kName, "identifier" => "KapitalBoost_sign", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $inx, "y" => $iny, "width" => "200", "height" => "40",
                "page" => $inp, "signer" => "1", "name" => $iName, "identifier" => "Investor_sign_2", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $owx, "y" => $owy, "width" => "200", "height" => "40",
                "page" => $owp, "signer" => "2", "name" => $oName, "identifier" => "Owner_sign_2", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => "")))
	);
} else {
	// echo "'<script>console.log(\"masuk ke murabaha\")</script>'";
	$data = array(
    "is_draft" => 0,
    "embedded" => 0,
    "title" => $title,
    "message" => $message,
    "use_signer_order" => 0,
    "reminders" => 1,
    "require_all_signers" => 1,
    "files" => array(array("name" => $title, "file_url" => $url, "file_id" => "", "file_base64" => "")),
    "signers" => array(array("id" => 1, "name" => $iName, "email" => $iEmail, "message" => $iMessage), array("id" => 2, "name" => $oName, "email" => $oEmail, "message" => $oMessage)),
    "fields" => array(array(array("type" => "signature", "x" => $ix, "y" => $iy, "width" => "200", "height" => "40",
                "page" => $ip, "signer" => "1", "name" => $iName, "identifier" => "Investor_sign", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $ox, "y" => $oy, "width" => "200", "height" => "40",
                "page" => $op, "signer" => "2", "name" => $oName, "identifier" => "Owner_sign", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => "")))
	);
}




$postData = json_encode($data);
// echo "'<script>console.log(\"postData : $postData\")</script>'";



$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.eversign.com/api/document?access_key=2cd751d948d6139a1700871a51ce4e78&business_id=3352",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 90,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $postData,
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
    ),
));

$response = curl_exec($curl);
$error = curl_error($curl);
curl_close($curl);

$responseA = json_decode($response,1);

$documentHash = $responseA["document_hash"];

if($responseA["document_hash"] != ""){
   $result = $mysql->UpdateSentContract($iId, $documentHash);
   if($result){
      echo '1';
   }else{
      echo '0';
   }
}else{
   echo 'EverSign API Error';
   echo "'<script>console.log(\"response : $response\")</script>'";
}
