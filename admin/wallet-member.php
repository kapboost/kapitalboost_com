<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

if (isset($_GET["keyword"])) {
       $keyword = $_GET["keyword"];
} else {
       $keyword = "";
}
if ($mysql->Connection()) {

       list($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet) = $mysql->GetAllWalletMembers($keyword);
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Wallet Members</title>

              <?php include_once 'include.php'; ?>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Kapital Boost's Members Wallet</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">



                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="5%">No</th>
                                                                             <th width="15%">Full Name</th>
                                                                             <th width="20%">Username</th>
                                                                             <th width="20%">Member Email</th>
                                                                             <th width="15%">Country</th>
                                                                             <th width="*">Wallet Amount</th>
                                                                             <!--<th width="5%">Validated</th>-->
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($mId); $i++) { ?>
                                                                      <?php if ($mWallet[$i]==NULL or $mWallet[$i]==""){$mWallet[$i]=0;} ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><a href="member-edit.php?m=<?= $mId[$i] ?>"><?= $mFullname[$i] ?></a></td>
                                                                                    <td><?= $mUserName[$i] ?></td>
                                                                                    <td><?= $mEmail[$i] ?></td>
                                                                                    <td><?= $mCountry[$i] ?></td>
                                                                                    <td><a href="wallet.php?m=<?= $mId[$i] ?>">$ <?= $mWallet[$i] ?></a></td>
                                                                                    <!--<td>
                                                                                           <select class="validated" >
                                                                                                  <?php if ($validated[$i] == 0) { ?>
                                                                                                         <option value="<?= $mId[$i] ?>~0" selected="">NO</option>
                                                                                                         <option value="<?= $mId[$i] ?>~1" >YES</option>
                                                                                                  <?php } else { ?>
                                                                                                         <option value="<?= $mId[$i] ?>~0" >NO</option>
                                                                                                         <option value="<?= $mId[$i] ?>~1" selected="" >YES</option>
                                                                                                  <?php } ?>

                                                                                           </select>
                                                                                    </td>-->
                                                                             </tr>

                                                                      <?php } ?>

                                                               </tbody>
                                                        </table>
                                                 </div>





                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>