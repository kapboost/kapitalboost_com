<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'mysql.php';
$mysql = new mysql();

$cId = $_GET["c"];
$iId = $_GET["i"];
$gettoken = $_GET["token"];

$monthsID = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];


if ($mysql->Connection()) {

   list($iAmount, $iDate, $iStatus) = $mysql->GetSinglePayout($iId);
   $iAmo = array_filter($iAmount);
   if (count($iAmo) == 0) {
      echo 'no payout. it could be because master payout hasn\'t been configured yet';
      exit;
   }
   for ($i = 0; $i < count($iAmo); $i++) {
      $payoutAmount[$i] = "SGD" . number_format($iAmo[$i + 1]);
      $payoutDate[$i] = date("d F Y", strtotime($iDate[$i + 1]));
      $payoutMonthId[$i] = $monthsID[date('n', strtotime($iDate[$i + 1]))];
      $payoutDayId[$i] = date('d', strtotime($iDate[$i + 1]));
      $payoutYearId[$i] = date('Y', strtotime($iDate[$i + 1]));
      $payoutDateId[$i] = "$payoutDayId[$i] $payoutMonthId[$i] $payoutYearId[$i]";
   }

   $exp_date = $mysql->GetCampaignExpiryDate($cId);
   $totalFunding = $mysql->GetSingleContractCampaign($cId);

   list($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profitD, $camReturn, $foreignCurrency,
   $dirName, $dirEmail, $dirTitle, $dirIc, $imageD1, $imageD2, $imageD3, $imageD4, $imageD5, $imageD6, $imageD7, $imageD8, $imageD9, $imageD10,
   $imageD11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee,
   $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) = $mysql->GetSingleCampaignContractList($cId);

   list($id, $mId, $name, $email, $funding, $fundingId, $totalPayout, $date, $closeDate, $token, $hasContract, $sentContract, $sign) = $mysql->GetSingleInvestorContract($iId);

   $totalFundingAmt = $mysql->GetCampaignTotalAmount($cId);
   $subtype = $mysql->GetCampaignType($cId);
 // echo "'<script>console.log(\"$mId\")</script>'";
   list($icName, $icType, $icNumber,$icId, $icCountry, $icOption) = $mysql->GetSingleContractMember($mId);
   // $icCountry = ucfirst(strtolower($icCountry));
   $allCapsCountry = $icCountry;
   $icCountry = $mysql->GetCountryCaps($icCountry);
   $memberNationality = $mysql->GetMemberCountry($mId);
   // echo "'<script>console.log(\"memberNationality : $memberNationality\")</script>'";
   $memberCountry = $mysql->GetMemberCountryNationality($memberNationality);
   if (!isset($memberCountry)){$memberCountry=$icCountry;}
   // echo "'<script>console.log(\"memberCountry : $memberCountry\")</script>'";

   $comRegTypeEngText = '';
   if($comRegTypeEng == 1) {
      $comRegTypeEngText = 'Company Registration No.';
   }else {
      $comRegTypeEngText = 'Single Business No.';
   }

   $comRegTypeInaText = '';
   if($comRegTypeIna == 1) {
      $comRegTypeInaText = 'Nomor Tanda Daftar Perusahaan (TDP)';
   }else {
      $comRegTypeInaText = 'Nomor Induk Berusaha (NIB)';
   }


   if ($gettoken != $token || $gettoken == "") {
      echo 'wrong token';
      exit;
   }

   // if ($comName == '' || $comReg == '' || $assetCost == '' || $foreignCurrency == '' || $dirName == '' || $dirEmail == '' || $dirIc == '' || $imageD1 == '') {
      // echo 'lack of contract data';
      // exit;
   // }

   if ($comName == '' || $comReg == '' || $foreignCurrency == '' || $dirName == '' || $dirEmail == '' || $dirIc == '') {
      echo 'lack of contract data';
      exit;
   }

   if ($icName == '' || $icType == '' || $icNumber == '') {
      echo 'lack of member ic info';
      exit;
   }



}


require 'lib/fpdi/tcpdf/tcpdf.php';
require 'lib/fpdi/fpdi.php';


$date = date("d F Y", time());
$companyName = $comName;
$companyRegistration = $comReg;
$purchaseAsset = $purchaseAsset;
$assetCost = $assetCost;
$salePrice = $salePrice;
$assetCostCurrency = $assetCostCurr;
$exchange = $exchange;
$return = $camReturn;
$profit = ((float)$return / 100) * (float)$totalFunding;
$profit = "SGD".number_format($profit,2,'.',',');;
// $profit = "SGD".number_format($profitD,2,'.',',');;
$cCloseDate = date("d F Y", strtotime("+1 day", strtotime($exp_date)));
$cCloseDate1 = date("d F Y", strtotime("+1 month", strtotime($exp_date)));

$foreignCurrency = $foreignCurrency;
$dirName = $dirName;
$dirTitle = $dirTitle;
$dirIC = $dirIc;
$image1 = "https://kapitalboost.com/assets/images/pdfimg/$imageD1";
$image2 = "https://kapitalboost.com/assets/images/pdfimg/$imageD2";
$image3 = "https://kapitalboost.com/assets/images/pdfimg/$imageD3";
$image4 = "https://kapitalboost.com/assets/images/pdfimg/$imageD4";
$image5 = "https://kapitalboost.com/assets/images/pdfimg/$imageD5";
$image6 = "https://kapitalboost.com/assets/images/pdfimg/$imageD6";
$image7 = "https://kapitalboost.com/assets/images/pdfimg/$imageD7";
$image8 = "https://kapitalboost.com/assets/images/pdfimg/$imageD8";
$image9 = "https://kapitalboost.com/assets/images/pdfimg/$imageD9";
$image10 = "https://kapitalboost.com/assets/images/pdfimg/$imageD10";
$image11 = "https://kapitalboost.com/assets/images/pdfimg/$imageD11";


$invName = $icName;
$icType = str_replace($allCapsCountry,$icCountry,$icType);
// $icType = ucfirst(strtolower($icType));
$icType = str_replace(" ic "," Identity Card ",$icType);


$invIC = "$icType $icNumber";
$invICIDs = "$icType $icNumber";
if ($icCountry=="Indonesia") {
  if (strtolower($icOption) == "passport") {
    $invICIDs = "Passport Indonesia No. $icNumber";
  }else{
    $invICIDs = "Kartu Tanda Penduduk Indonesia No. $icNumber";
  }
}
$invIC = str_replace(" ic "," Identity Card ",$invIC);
// echo "'<script>console.log(\"$invIC\")</script>'";
$amount = number_format($funding,2,'.','');
$amountCommitment = number_format($funding,0,'.',',');
$expect_payout = $amount + ($amount*($return/100));
$percent_return = $amount*($return/100);
// echo "'<script>console.log(\"totalFunding : $totalFunding\")</script>'";
$percent_assetCost = $totalFunding*($return/100);
$percent_assetCost = "SGD".number_format($percent_assetCost,1,'.',',');
// echo "'<script>console.log(\"percent_assetCost : $percent_assetCost\")</script>'";
$expect_payout = number_format($expect_payout,2,'.',',');
$percent_return = number_format($percent_return,2,'.',',');
$invAmount = "SGD$amountCommitment";
// $invPercent = number_format((float)(100 * $funding) / $profitD, 2,'.','');
$totalPayoutAmount = number_format($totalPayout,2,'.','');
$invSalePrice = "SGD$totalPayoutAmount";
$invProfit = $totalPayout - $funding;

// echo "($invPercent/$totalFundingAmt)*100";

$invBack = number_format((float)($funding/$totalFundingAmt)*100, 2,'.','');



$month = date('n', time());
$day = date('d', time());
$year = date('Y', time());
$monthId = $monthsID[$month];
$dateID = "$day $monthId $year";


   list($cName, $mPayout) = $mysql->GetSingleCampaignForPayout($cId);

$payoutA = explode("==", $mPayout);
$amountA = $dateA = $statusA = array();

// $countPayoutA = count($payoutA);
// echo $countPayoutA;

for ($i = 0; $i < count($payoutA); $i++) {
   $payouts = explode("~", $payoutA[$i]);
   $countPayouts = count($payouts);
	// echo $countPayouts;
 if ($countPayouts==3){
   $amountA[$i] = $payouts[0];
   $dateA[$i] = $payouts[1];
   $statusA[$i] = $payouts[2];
 }
}

if ($subtype=="INVOICE  FINANCING") { //all variables that is used on Invoice financing contract.
   if ($invoAssetCostSGD=="" or $invoAssetCostSGD==NULL  or $invoAssetCostSGD=="  "){$invoAssetCostSGD=0;}
   if ($invoAssetCost=="" or $invoAssetCost==NULL or $invoAssetCost=="  "){$invoAssetCost=0;}
   if ($invoSubsAgencyFee=="" or $invoSubsAgencyFee==NULL or $invoSubsAgencyFee=="  "){$invoSubsAgencyFee=0;}
   $invoAssetCostSGDToBeCount = $invoAssetCostSGD;
   // echo "'<script>console.log(\"$invoAssetCostSGD\")</script>'";
   $invoAssetCostSGD = number_format($invoAssetCostSGD,0,'.',',');
   $invoAssetCost = number_format($invoAssetCost,0,'.',',');
   $sub_agency_fee = $amount*($invoSubsAgencyFee/100);
   $sub_agency_fee = number_format($sub_agency_fee,2,'.','');
   $invBack2 = number_format((float)($funding/$invoAssetCostSGDToBeCount)*100, 2,'.','');
}


if ($conType != 1) {
   $pdf = new FPDI();
   $pdf->setPrintHeader(false);
   $pdf->setPrintFooter(false);

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetFont('Helvetica');

   MCenter($pdf, "_______________________ ", 20);

   $pdf->SetXY(50, 32);
   $pdf->Write(8, "MURABAHAH AGREEMENT NO. M$invoContractNo");

   MCenter($pdf, "Financing Agreement Based on the Shariah Principle of Murabahah", 40);

   MCenter($pdf, " _______________________", 45);


   MCenter($pdf, "DATED THIS DAY $date", 60);

   MCenter($pdf, "Between", 75);

   MCenter($pdf, $companyName, 100, true);

   MCenter($pdf, $companyRegistration, 110);

   $pdf->SetXY(160, 120);
   $pdf->Write(8, "...Company");

   MCenter($pdf, "and", 130);

   MCenter($pdf, $invName, 150, true);

   MCenter($pdf, "($invIC)", 160);

   $pdf->SetXY(160, 170);
   $pdf->Write(8, "...Financier");


   $pdf->SetFontSize("30");
   MCenter($pdf, ". . . . . . . . . . . . . . . . . . . . .", 190);

   $pdf->SetFontSize("16");
   MCenter($pdf, "MURABAHAH AGREEMENT", 210, true);

   $pdf->SetFontSize("30");
   MCenter($pdf, ". . . . . . . . . . . . . . . . . . . . .", 220);
   $pdf->SetFontSize("12");

// Page 2

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetMargins(25, 15);

   MCenter($pdf, "MURABAHAH AGREEMENT", 15);

   $y = MLeft1($pdf, "<b>THIS AGREEMENT</b>&nbsp; is made between the following parties: ", 35);

   $y = MLeft1($pdf, "1. <b>$companyName</b>&nbsp;", $y + 5);

   $y = MLeft1($pdf, $companyRegistration, $y);

   $y = MLeft1($pdf, "(hereinafter referred to as \"<b>the Company</b>&nbsp;  \")", $y + 5);

   $y = MLeft1($pdf, "<b>AND</b>&nbsp;", $y + 10);

   $y = MLeft1($pdf, "2. <b>$invName</b>&nbsp;", $y + 10);
	$invIC = str_replace(" IC "," Identity Card ",$invIC);
   $y = MLeft1($pdf, "($invIC)", $y);

   $y = MLeft1($pdf, "(hereinafter referred to as \"<b>the Financier</b>&nbsp;  \")", $y + 5);

   $y = MLeft1($pdf, "<b>WHEREAS:</b>&nbsp;", $y + 10);

   $y = MLeft1($pdf, "A.", $y + 15);

   $y = MLeft2($pdf, "The Company is in need of funding for the purchase, acquisition or procurement of $purchaseAsset, as described in Schedule A (“<b>the Asset</b>&nbsp; ”), which it intends to utilize for its business, and the Company wishes to obtain such financing in accordance with Shariah principles;", $y - 7);

   $y = MLeft1($pdf, "B.", $y + 15);

   $y = MLeft2($pdf, "The Financier, together with certain other financiers (together, “<b>the Investors Group</b>&nbsp; ”) have agreed to provide the required financing to the Company based on the Shariah principle of Murabahah, specifically murabahah to the purchase orderer; and", $y - 7);

   $y = MLeft1($pdf, "C.", $y + 15);

   $y = MLeft2($pdf, "The Financier has agreed to provide his share of the required financing based on the terms and subject to the conditions of this Agreement.", $y - 7);


   $y = MLeft1($pdf, "<b>NOW IT IS HEREBY AGREED</b>&nbsp; as follows:", $y + 15);
   $y = MLeft1($pdf, "1. <b>METHOD OF FINANCING</b>&nbsp;", $y + 10);

   $y = MLeft1($pdf, "1.1", $y);

   $y = MLeft2($pdf, "For the purpose of financing the acquisition of the Asset, the Company hereby requests that the Investors Group"
           . " collectively purchases the Asset from the Supplier for a total consideration of <b>$assetCost</b>&nbsp; (\"<b>the Purchase Cost</b>&nbsp;&nbsp;&nbsp;\"), "
           . "and thereafter sell the Asset to the Company at the sale price of <b>$salePrice</b>&nbsp; (\"<b>the Sale Price</b>&nbsp; \"). "
           . "The actual cost to the Company of purchasing the Asset is <b>$assetCostCurrency</b>&nbsp; (\"<b>Purchase Currency Cost </b>&nbsp;&nbsp; \"),"
           . " which currency can be purchased using the Singapore Dollar amount of the Purchase Cost at the prevailing currency"
           . " exchange rate of S$1:$foreignCurrency$exchange . The Company undertakes (via wa’d) to purchase the Asset from the Investors Group, "
           . "following the purchase of the Asset by the Investors Group in accordance with the terms of this Agreement. "
           . "The Murabahah arrangement set out above will result in profit for the Investors Group as set out below.", $y - 6);


   $html = "<table cellspacing=\"0\" cellpadding=\"10\" border=\"1\">
  <tr>
  <td><b>Purchase Cost of the Asset payable by the Investors Group</b>&nbsp;</td>
  <td><b>Sale Price payable by the Company to the Investors Group</b>&nbsp;</td>
  <td><b>Amount of Profit derived by the Investors Group</b>&nbsp;</td>
  </tr>
  <tr>
  <td>$assetCost</td>
  <td>$salePrice</td>
  <td>$profit (or $return% of the Purchase Cost of Asset)</td>
  </tr>

  </table>
  ";
   $pdf->writeHTML($html);
   $y = $pdf->GetY();
   $y = MLeft1($pdf, "1.2", $y - 5);

   $y = MLeft2($pdf, "The parties agree that the Purchase Cost refers to the actual acquisition costs of the Asset incurred or to be incurred by "
           . "the Investors Group or its agent for the acquisition of the Asset, excluding any direct expenses (such as delivery, transportation, "
           . "storage and assembly costs, taxes, takaful and insurance coverage costs) and any overhead expenditures or indirect costs "
           . "(such as staff wages and labor charges).", $y - 7);

   $y = MLeft1($pdf, "1.3", $y + 5);

   $y = MLeft2($pdf, "The Financier is willing to finance exactly $invAmount (being $invBack%) out of the"
           . " entire Purchase Cost amount. Such amount to be financed by the Financier will be paid by Financier to the Supplier, through"
           . " the Company as agent. For the purposes of enabling Kapital Boost, as agent of the Company under Clause 3.4.1, to transfer"
           . " the Purchase Cost amount to the Company by no later than $closdate (the \"<b>Purchase Cost Contribution Date</b>&nbsp;&nbsp;&nbsp;&nbsp; \"),"
           . " the Financier shall transfer the amount of its contribution to the Purchase Cost (as specified in this clause above) to the account"
           . " specified in writing by Kapital Boost, by no later than <b>three (3) working days</b>&nbsp;&nbsp;&nbsp;&nbsp; from the date of this Agreement. ", $y - 7);

   $y = MLeft1($pdf, "1.4", $y + 5);

   $y = MLeft2($pdf, "Based on the Financier’s contribution to the Purchase Cost as specified above, the Financier will own"
           . " $invBack% of the Asset upon purchase of the same by the Investors Group, and the Financier will be"
           . " entitled to $invBack% share of the Sale Price and each installment or part thereof paid by the Company,"
           . " amounting to:-", $y - 7);



   MBullet($pdf, $y);

   $y = MLeft3($pdf, "Financier’s portion of the Sale Price: SGD$expect_payout", $y + 3);

   MBullet($pdf, $y);

   $y = MLeft3($pdf, "Financier's portion of the Profit: SGD$percent_return", $y + 3);

   $y = MLeft1($pdf, "1.5", $y + 5);

   $y = MLeft2($pdf, "The Financier (as part of the Investors Group) hereby appoints the Company as his agent to purchase"
           . " the Asset on its behalf. The Company hereby accepts such appointment as agent, and will purchase and handle"
           . " the Asset in accordance with the terms of this Agreement. The Company, as agent, shall purchase the Asset in"
           . " the currency of the Purchase Currency Cost, for a consideration not exceeding the Purchase Currency Cost, and"
           . " by no later than  <b>$cCloseDate1</b>&nbsp;&nbsp;(\"<b>Date of Purchase</b>&nbsp;&nbsp; \"). ", $y - 7);

   $y = MLeft1($pdf, "1.6", $y + 5);

// star here

   $y = MLeft2($pdf, "The Financier and the Company (as agent of the Investors Group for the purposes of the purchase of the Asset)"
           . " hereby acknowledge and mutually agree that:", $y - 7);

   $y = MLeft2($pdf, "1.6.1", $y + 3);

   $y = MLeft3($pdf, "the Investors Group (including the Financier) has appointed the Company to act as agent of the Investors Group"
           . " to purchase the Asset on behalf of the Investors Group because of the Company’s market knowledge and networks"
           . " in relation to the Asset;", $y - 7);

   $y = MLeft2($pdf, "1.6.2", $y + 3);

   $y = MLeft3($pdf, "the purchase of the Asset by the Investors Group through the Company as agent may result in benefits to"
           . " the Investors Group through efficiency of the purchase process and also cost savings in the event that the Company"
           . " is able to secure or source the Asset at a price lower than the Purchase Currency Cost;", $y - 7);

   $y = MLeft2($pdf, "1.6.3", $y + 3);

   $y = MLeft3($pdf, "the event that the Company, as agent, is able to purchase the Asset at lower than the Purchase Currency Cost,"
           . " the Investors Group hereby agrees to waive, by way of tanazul (i.e. waiver), its rights to claim the excess unutilized"
           . " funding amount; and the Company, as agent, is entitled to retain the benefit of any such cost savings as incentive based"
           . " on hibah for its good work performance;", $y - 7);

   $y = MLeft2($pdf, "1.6.4", $y + 3);

   $y = MLeft3($pdf, "in the circumstances described in Clause 1.6.3, the cost to the Investors Group of purchasing the Asset would be"
           . " the Purchase Cost, as the hibah amount granted to the Company, as agent, would constitute part of the actual cost of"
           . " purchasing the Asset, and accordingly the same Purchase Cost will be reflected in the Murabahah Sale Contract to be entered"
           . " into by the Investors Group in the sale of the Asset;", $y - 7);

   $y = MLeft2($pdf, "1.6.5", $y + 3);

   $y = MLeft3($pdf, "in the event that the Company is unable to purchase sufficient $foreignCurrency to constitute the Purchase Currency"
           . " Cost using the Purchase Cost contributed by the Investors Group as a result of an decrease in the currency exchange rate from"
           . " that stated in Clause 1.1:", $y - 7);

   $y = MLeft3($pdf, "(a)", $y + 2);

   $y = MLeft4($pdf, "the Company, as agent of the Investors Group, shall either:", $y - 7);

   $y = MLeft4($pdf, "(i)", $y);

   $y = MLeft5($pdf, "promptly notify the Investors Group and return the Purchase Cost"
           . " to the Investors Group, following which this Agreement shall terminate; or", $y - 7);

   $y = MLeft4($pdf, "(ii)", $y);

   $y = MLeft5($pdf, "fund the shortfall amount in the currency of the Purchase Cost (the"
           . " \"<b>Purchase Cost Shortfall Amount</b>&nbsp;&nbsp;&nbsp; \") which would be required in order for the Company to purchase the"
           . " Purchase Currency Cost in order to complete the purchase of the Asset as agent on behalf of the Investors Group;", $y - 7);

   $y = MLeft3($pdf, "(b)", $y + 3);

   $y = MLeft4($pdf, "following the funding of the Purchase Cost Shortfall Amount by the Company and the completion of the purchase"
           . " of the Asset by the Company, as agent of the Investors Group:", $y - 7);

   $y = MLeft4($pdf, "(i)", $y);
   $y = MLeft5($pdf, "the Purchase Cost Shortfall Amount shall constitute a debt owed by the Investors Group to the Company, in its capacity as agent for the Investors Group;", $y - 7);

   $y = MLeft4($pdf, "(ii)", $y);
   $y = MLeft5($pdf, "notwithstanding paragraph (i) above, the Company shall not, prior to the transfer of title to the Asset pursuant to Clause 1.8, have any claim to any part of portion of the title to the Assets;", $y - 7);

   $y = MLeft4($pdf, "(iii)", $y);
   $y = MLeft5($pdf, "the Sale Price (or, if there is more than one installment of the Sale Price, then the first installment thereof) shall be increased by the Purchase Cost Shortfall Amount, which will be reflected in the Company’s Offer to Purchase delivered by the Company to the Investors Group; and", $y - 7);

   $y = MLeft4($pdf, "(iv)", $y);
   $y = MLeft5($pdf, "the Investors Group, the Company (in its capacity as agent of the Investors Group) and as purchaser of the Asset, all agree that the Purchase Cost Shortfall Amount owing by the Company to the Investors Group (as part of the Sale Price) shall be automatically set off against the same amount owing by the Investors Group to the Company (pursuant to paragraph (i) above), and neither the Financier nor the Company shall make any claim with respect to payment of such amount by the other party.", $y - 7);

   $y = MLeft2($pdf, "1.6.6", $y + 5);
   $y = MLeft3($pdf, "In the event that on the Purchase Cost Contribution Date, the aggregate amount of the contributions from the Investors Group"
           . " towards the Purchase Cost is lower than the quantum of the Purchase Cost specified in Clause 1.1, but higher than 50% of such quantum,"
           . " Kapital Boost (as agent acting on behalf, and at the instruction, of the Company) shall promptly notify the Investors Group of the intention"
           . " of the Company to (i) continue with the purchase of the Asset in reduced quantity for a total consideration equal to the aggregate amount"
           . " of the contributions from the Investors Group (the \"<b>Adjusted Purchase Cost</b>&nbsp;&nbsp;&nbsp;\"); or otherwise (ii) return the contributions to the contributing"
           . " members of the Investors Groups pursuant to Clause 6.2.; and", $y - 7);

   $y = MLeft3($pdf, "(a)", $y + 2);
   $y = MLeft4($pdf, "following the Company’s election pursuant to Clause 1.6.6 (i):", $y - 7);

   $y = MLeft4($pdf, "(1)", $y + 3);
   $y = MLeft5($pdf, "the Company (as agent of Investors Group) will complete the purchase of the Asset by the Date of Purchase;", $y - 7);

   $y = MLeft4($pdf, "(2)", $y + 3);
   $y = MLeft5($pdf, "this Agreement shall be read to interpret the Purchase Cost as the Adjusted Purchase Cost, and all other relevant provisions"
           . " of this Agreement shall be adjusted accordingly including: (i) a proportional reduction to the Purchase Currency Cost"
           . " and the Sale Price; and (ii) a proportional increase in the percentage of each contributing Financier’s ownership in the Asset, "
           . "each as specified in the notice delivered pursuant to Clause 1.6.6(i) (the \"<b>Adjustment Notice</b>&nbsp;&nbsp;\"); and", $y - 7);

   $y = MLeft4($pdf, "(3)", $y + 3);
   $y = MLeft5($pdf, "there will not be any change to (i) the quantum of each contributing Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price; and", $y - 7);

   $y = MLeft3($pdf, "(b)", $y + 3);
   $y = MLeft4($pdf, "upon delivery by Kapital Boost to the Investors Group of an Adjustment Notice which complies with the requirements of this Agreement, each of the contributing Financiers shall be deemed to have agreed to such adjustments and the Adjustment Notice shall constitute an effective and binding amendment to this Agreement.", $y - 7);

   $y = MLeft2($pdf, "1.6.7", $y + 5);
   $y = MLeft3($pdf, "the circumstance described in 1.6.6, the Company will proceed to purchase the Asset, in reduced quantity for a total consideration equal to the aggregate contribution from consenting Financiers (\"Adjusted Purchase Cost\"), provided that the amount is more than 50% of the Purchase Cost.", $y - 7);

   $y = MLeft1($pdf, "1.7", $y + 5);
   $y = MLeft2($pdf, "The Company, as agent of the Investors Group, undertakes to inspect the Asset prior to delivery / collection, and shall ensure that all specifications in relation to the Asset as specified in Schedule A shall be complied with. The Company undertakes to immediately indemnify the Investors Group for any losses or liabilities resulting directly or indirectly from the failure of the Company to so inspect, and ensure the fitness for purpose or compliance with specifications of, the Asset.", $y - 7);

   $y = MLeft1($pdf, "1.8", $y + 5);
   $y = MLeft2($pdf, "Following the purchase of the Asset by the Company, as agent of the Investors Group, the Company will offer to purchase the Asset (\"<b>Company's Offer to Purchase</b>&nbsp;&nbsp;&nbsp;\") from the Investors Group, for a consideration equal to the Sale Price. The sale of the Asset by the Investors Group to the Company will take effect upon the acceptance of the Company’s Offer to Purchase by Kapital Boost, acting as agent of the Investors Group, in the form of Schedule B. The Sale Price will be paid by the Company to the Investors Group on deferred payment terms as provided in Clause 2 below. The Parties hereby agree that title to the Asset will pass immediately and fully from the Investors Group to the Company upon acceptance of the Company’s Offer to Purchase, in the form of Schedule B.", $y - 7);


   $y = MLeft1($pdf, "2. <b>PAYMENT OF SALE PRICE</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "2.1", $y + 5);
   $y = MLeft2($pdf, "The Sale Price will be paid by the Company as follows:-", $y - 7);




   $payoutString = "";
   for ($i = 0; $i < count($payoutDate); $i++) {
      $payoutString .= "<tr><td style=\"text-align:center\">$payoutDate[$i]</td><td style=\"text-align:center\">$salePrice</td></tr>";
      //$y += 14;
   }

   $payoutTbl = "<table cellspacing=\"0\" cellpadding=\"10\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b>&nbsp;</td>
  <td style=\"text-align:center\"><b>Sale Price payable</b>&nbsp;</td>
  </tr>
  $payoutString
  </table>
  ";
   $pdf->writeHTML($payoutTbl);
   $y = $pdf->GetY();
   $y = MLeft1($pdf, "2.2", $y - 5);
   $y = MLeft2($pdf, "All payments hereunder by the Financier and the Company shall be made in full in the currency of the Sale Price stated in this Agreement, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority.", $y - 7);

   $y = MLeft1($pdf, "2.3", $y + 5);
   $y = MLeft2($pdf, "All payments of the Sale Price or any installments thereof shall be made to each financier within the Investors Group pari passu, based on the percentage contribution to the Purchase Price as described above. The Parties agree that in the event any such payment is not made pari passu, the Parties shall make the necessary payments as between the Investors Group to rectify any over/under payment of the Sale Price.", $y - 7);

   $y = MLeft1($pdf, "3. <b>KAPITAL BOOST AS AGENT</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "3.1", $y + 5);
   $y = MLeft2($pdf, "The Financier acknowledges that the Company has appointed, or is hereby deemed to appoint, Kapital Boost as its agent for the purposes of (a) collecting the Purchase Cost from the Investors Group, for payment to the Supplier through the Company; (b) notifying the Investors Group of the election made by the Company pursuant to Clause 1.6.5; and (c) distributing to the Investors Group monies comprising the Sale Price or installments thereof due and payable by the Company. ", $y - 7);

   $y = MLeft1($pdf, "3.2", $y + 5);
   $y = MLeft2($pdf, "The Investors Group furthermore hereby (by the electronic delivery of this Agreement to Kapital Boost) appoints Kapital Boost to accept and sign the Murabahah Sale Contract per Schedule B of this Agreement. ", $y - 7);

   $y = MLeft1($pdf, "3.3", $y + 5);
   $y = MLeft2($pdf, "The Company acknowledges that the Financier has appointed, or is deemed to appoint, Kapital Boost as its agent for the purposes of (a) collecting the Sale Price that is due and payable by the Company to the Investors Group and (b) commencing legal proceeding upon the occurrence of an Event of Default against the Company provided that Kapital Boost has received sufficient funds from the Financier to fund costs related to such legal proceedings. ", $y - 7);

   $y = MLeft1($pdf, "3.4", $y + 5);
   $y = MLeft2($pdf, "For the purposes of the agency under Clause 3.1, 3.2 and 3.3:", $y - 7);

   $y = MLeft2($pdf, "3.4.1", $y + 3);
   $y = MLeft3($pdf, "Kapital Boost shall (a) transfer to the Company by no later the Purchase Cost Contribution Date, the Purchase Cost received by it from the Investors Group; and (b) upon receipt of any amount of the Sale Price due from the Company to any member of the Investors Group on any particular date, promptly distribute to each financier within the Investors Group its proportion of the amount received, in each case, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority;", $y - 7);

   $y = MLeft2($pdf, "3.4.2", $y + 3);
   $y = MLeft3($pdf, "Kapital Boost shall ensure that the   following terms are included in the Adjustment Notice:", $y - 7);

   $y = MLeft3($pdf, "(a)", $y);
   $y = MLeft4($pdf, "the Adjusted Purchase Price; ", $y - 7);

   $y = MLeft3($pdf, "(b)", $y);
   $y = MLeft4($pdf, "the proportionally reduced Purchase Currency Cost and Sale Price;", $y - 7);

   $y = MLeft3($pdf, "(c)", $y);
   $y = MLeft4($pdf, "the proportionally increased percentage of the Financier’s ownership in the Asset; and", $y - 7);

   $y = MLeft3($pdf, "(d)", $y);
   $y = MLeft4($pdf, "express confirmation that there is no change to (i) the quantum of the Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price.", $y - 7);

   $y = MLeft2($pdf, "3.4.3", $y + 5);
   $y = MLeft3($pdf, "prior to signing the Murabahah Sale Contract as agent of the Investors Group, Kapital Boost shall check the terms of the same to ensure compliance with the terms of this Agreement;", $y - 7);

   $y = MLeft2($pdf, "3.4.4", $y + 5);
   $y = MLeft3($pdf, "Kapital Boost shall inform the Investors Group promptly upon becoming aware of any delay in the Company delivering the Murabahah Sale Contract, any deficiency in the Murabahah Sale Contract or any other reason which would result in Kapital Boost not being able to, or preferring not to, sign the Murabahah Sale Contract;", $y - 7);

   $y = MLeft2($pdf, "3.4.5", $y + 5);
   $y = MLeft3($pdf, "Kapital Boost will endeavor to take all necessary measures to recover the outstanding payments of all or any part of the Sale Price from the Company;", $y - 7);

   $y = MLeft2($pdf, "3.4.6", $y + 5);
   $y = MLeft3($pdf, "Kapital Boost shall provide to the Investors Group with fee quotations that includes legal counsel fees and other costs to be incurred by Kapital Boost as agent. Kapital Boost shall proceed to instruct the legal counsel to take legal action, as advised by such legal counsel, against the Company provided that the Financier has transferred to Kapital Boost the funds needed to cover legal costs. Such costs incurred shall be indemnified in full by the Company without prejudice to Clause 7.3;", $y - 7);

   $y = MLeft2($pdf, "3.4.7", $y + 5);
   $y = MLeft3($pdf, "Kapital Boost shall not be responsible or liable for any default or shortfall in the payment by the Company of all or any part of the Sale Price due to the Investors Group at any time; ", $y - 7);

   $y = MLeft2($pdf, "3.4.8", $y + 5);
   $y = MLeft3($pdf, "by taking any of the actions described in this Clause 3.4 as agent of the Investors Group, Kapital Boost is deemed to have accepted its appointment as such agent, and to have agreed to be bound by the terms of this Clause 3.4.", $y - 7);

   $y = MLeft1($pdf, "4. <b>TRANSFER OF OWNERSHIP & WARRANTY</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "4.1", $y + 5);
   $y = MLeft2($pdf, "The Company agrees to purchase the Asset on an as-is, where-is basis, and is deemed to have inspected the Asset prior to purchase thereof. The title and ownership of the Asset together with all rights, interest and benefits attached thereto shall pass from the Investors Group to the Company immediately upon the sale of the Asset by the Investors Group to the Company pursuant to the terms of this Agreement, notwithstanding that the Sale Price has not been paid or fully paid by the Company to the Investors Group. The Company waives all claims against the Investors Group as seller for breach in warranty or defects in respect of the Asset. The Company shall immediately at the point of purchase, collect and take possession of the Asset from the Investors Group or its agent(s), if any.", $y - 7);

   $y = MLeft1($pdf, "4.2", $y + 5);
   $y = MLeft2($pdf, "The Investors Group assigns to the Company its warranty and other rights against the supplier / seller of the Assets at the point of purchase by the Company.", $y - 7);

   $y = MLeft1($pdf, "5. <b>EARLY PAYMENT OF SALE PRICE</b>&nbsp;", $y + 15);

   $y = MLeft2($pdf, "The Company may opt at its discretion to effect early payment of all or any part of the Sale Price. Any early payment of part of the Sale Price shall reduce the Sale Price payment obligations of the Company in inverse chronological order, and any such early payment of any part of the Sale Price shall not delay, reduce or otherwise affect the remaining payment obligations of the Company in respect of the Sale Price. Following any early payment of any part of the Sale Price, the Investors Group or any individual Investor may at their or his absolute discretion grant ibra’ (rebate) on any remaining part(s) of the Sale Price or, as the case may be, that Investor’s share of the Sale Price. Both parties acknowledge that charging and/or claiming early settlement charges is not in accordance with Shariah law.", $y + 5);

   $y = MLeft1($pdf, "6. <b>AUTOMATIC CANCELLATION</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "If:", $y);

   $y = MLeft1($pdf, "6.1", $y + 5);
   $y = MLeft2($pdf, "the Company, as agent of the Investors Group, fails for whatever reason to complete the purchase of the Asset by the Date of Purchase; or", $y - 7);

   $y = MLeft1($pdf, "6.2", $y + 5);
   $y = MLeft2($pdf, "The Company, as agent of the Investors Group, does not receive the full amount of the Purchase Cost from the Investors Group, by no later than the Purchase Cost Contribution Date (unless the Company elects, pursuant to Clause 1.6.6, to complete the purchase of the Asset based on the Adjusted Purchase Cost, and thereafter completes such purchase of the Asset by the Date of Purchase); or", $y - 7);

   $y = MLeft1($pdf, "6.3", $y + 5);
   $y = MLeft2($pdf, "prior to the completion of the purchase of the Asset by the Company, as agent of the Investors Group, an Event of Default occurs or it becomes illegal in any applicable jurisdiction or under any applicable laws or regulations for the Investors Group or the Company, as agent of the Investors Group, to purchase the Asset,", $y - 7);

   $y = MLeft2($pdf, "the Parties agree that the Murabahah transaction described in this Agreement shall be immediately and automatically cancelled, and the Company shall immediately refund the Investors Group all monies advanced or paid to it for the purchase of the Asset on behalf of the Investors Group, and will in addition indemnify each Financer in respect of all actual costs and expenses incurred by him in relation to the entry into this Agreement and the advance of funds to the Company.", $y + 5);

   $y = MLeft1($pdf, "7. <b>EVENTS OF DEFAULT</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "7.1", $y + 5);
   $y = MLeft2($pdf, "An Event of Default is deemed to have occurred in any of the following circumstances, whether or not arising due to the fault of the Company:-", $y - 7);

   $y = MLeft2($pdf, "7.1.1", $y + 3);
   $y = MLeft3($pdf, "The Company fails to fulfill any of its payment obligations in respect of the Sale Price, unless such failure is a result of a technical or administrative error on the part of the bank or financial institution remitting payment by the Company to Kapital Boost, or by Kapital Boost to the Financier; or", $y - 7);

   $y = MLeft2($pdf, "7.1.2", $y + 3);
   $y = MLeft3($pdf, "The Company fails to observe and perform any of its obligations under this Agreement (including, without limitation (a) its obligation to offer to purchase the Asset from the Investors Group, and (b) its obligations as agent of the Investors Group to purchase the Asset at the Purchase Cost and to accept the Offer to Purchase); or", $y - 7);

   $y = MLeft2($pdf, "7.1.3", $y + 3);
   $y = MLeft3($pdf, "Any representation, statement, or warranty given or made by the Company in this Agreement proves to be untrue, incorrect or inaccurate or misleading in any material respect; or", $y - 7);

   $y = MLeft2($pdf, "7.1.4", $y + 3);
   $y = MLeft3($pdf, "Any step is taken for the winding up, liquidation or dissolution of the Company;", $y - 7);

   $y = MLeft2($pdf, "7.1.5", $y + 3);
   $y = MLeft3($pdf, "Where there is an element of fraud, misuse, or misappropriation of Purchase Cost by the Company", $y - 7);

   $y = MLeft1($pdf, "7.2", $y + 5);
   $y = MLeft2($pdf, "Upon the occurrence of any Event of Default, the Financier shall have the right by notice directly, or indirectly through Kapital Boost as Agent acting on behalf of Financier, to the Company to:", $y - 7);

   $y = MLeft2($pdf, "7.2.1", $y + 3);
   $y = MLeft3($pdf, "declare the entire proportion of the Sale Price outstanding from the Company to the Financier to become immediately due and payable; and/or", $y - 7);

   $y = MLeft2($pdf, "7.2.2", $y + 3);
   $y = MLeft3($pdf, "require the Company to (a) sell the Asset and apply all proceeds of sale to pay the outstanding Sale Price; and (b) otherwise not dispose of or transfer the ownership of the Asset to any other person; and/or", $y - 7);

   $y = MLeft2($pdf, "7.2.3", $y + 3);
   $y = MLeft3($pdf, "require the Company to indemnify the Investors Group for any costs, losses or expenses incurred as a result of the occurrence of any Event of Default including, without limitation (a) any loss of profit by the Financier arising from (i) the failure by the Company to fulfill its undertaking to offer to purchase the Asset from the Investors Group, or (ii) failure by the Company as agent of the Investors Group to purchase the Asset at the Purchase Cost or to accept the Offer to Purchase, (b) any costs incurred in taking physical possession of the Asset following an Event of Default, and (c) any diminution in the value of the Asset arising from a breach by the Company of the terms of this Agreement.", $y - 7);

   $y = MLeft1($pdf, "7.3", $y + 5);
   $y = MLeft2($pdf, "The right of the Financier to accelerate the obligations of the Company in respect of the portion of the Sale Price due to the Financier may be exercised irrespective of, and without being contingent upon, the action or inaction of any other financier or the rest of the Investors Group. The Financier shall be entitled to bring or prosecute any legal action or claim against the Company to recover the outstanding sum, and the Company agrees to immediately upon demand indemnify the Financier for all actual costs, losses and liabilities (including legal fees) incurred or suffered by the Financier as a result of any Event of Default.", $y - 7);

   $y = MLeft1($pdf, "8. <b>REPRESENTATIONS</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "8.1", $y + 5);
   $y = MLeft2($pdf, "The Company represents to the Financier on the date of this Agreement
  that:", $y - 7);

   $y = MLeft2($pdf, "8.1.1", $y + 3);
   $y = MLeft3($pdf, "It is duly incorporated and validly existing under the laws of Singapore;", $y - 7);

   $y = MLeft2($pdf, "8.1.2", $y + 3);
   $y = MLeft3($pdf, "It has the power to execute this Agreement, and to perform its obligations under this Agreement and has taken all necessary action to authorize such execution and performance;", $y - 7);

   $y = MLeft2($pdf, "8.1.3", $y + 3);
   $y = MLeft3($pdf, "The execution and performance of this Agreement does not violate or conflict with any applicable law or regulation, any provision of its constitutional documents, and order or judgment of any court or other agency of government applicable to it, or any contractual restriction binding on it;", $y - 7);

   $y = MLeft2($pdf, "8.1.4", $y + 3);
   $y = MLeft3($pdf, "All consent required to have been obtained by it with respect to this Agreement have been obtained and are in full force and effect;", $y - 7);

   $y = MLeft2($pdf, "8.1.5", $y + 3);
   $y = MLeft3($pdf, "Its obligations under this Agreement constitute its legal, valid and binding obligations, enforceable in accordance with their respective terms;", $y - 7);

   $y = MLeft2($pdf, "8.1.6", $y + 3);
   $y = MLeft3($pdf, "No Event of Default has occurred or would reasonably be expected to result from the entry into, or performance of, this Agreement; and", $y - 7);

   $y = MLeft2($pdf, "8.1.7", $y + 3);
   $y = MLeft3($pdf, "The core or main business of the Company and the purpose of the financing under this Agreement are and shall at all times remain Shariah compliant.", $y - 7);

   $y = MLeft1($pdf, "8.2", $y + 5);
   $y = MLeft2($pdf, "The representations and warranties in this Clause 8 are repeated by the Company on the Purchase Date and on each scheduled date for payment of all or part of the Sale Price.", $y - 7);

   $y = MLeft1($pdf, "9. <b>LATE PAYMENT CHARGES (TA’WIDH AND GHARAMAH)</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "9.1", $y + 5);
   $y = MLeft2($pdf, "If any amount which is due and payable by the Company under or in connection with this Agreement is not paid in full on the date in accordance with this Agreement (an \"<b>unpaid amount</b>&nbsp;&nbsp;\"), the Company undertakes to pay late payment charge comprising both ta’widh and gharamah (calculated below) to the Investor Group on each day that an unpaid amount remains outstanding.", $y - 7);

   $y = MLeft1($pdf, "9.2", $y + 5);
   $y = MLeft2($pdf, "The total late payment charge shall be pegged directly to average annualized returns of this Murabahah Agreement (to be determined by the Company), calculated on daily rest basis as below, provided that the accumulated late payment charges shall not exceed 100 percent of the outstanding unpaid amount:", $y - 7);

   $y = MCenter1($pdf, "Unpaid Amount X average annualized returns X", $y + 5);
   $y = MCenter1($pdf, "No. of Overdue day(s) / 365", $y);

   $y = MLeft1($pdf, "9.3", $y + 5);
   $y = MLeft2($pdf, "Each Financier may retain only such part of the late payment amount paid to it, as is necessary to compensate the Financier for any actual costs (not to include any opportunity or funding costs), subject to and in accordance with the guidelines of its own Shariah advisers, and up to a maximum aggregate amount of 1% per annum of the unpaid amount (“ta’widh”). ", $y - 7);

   $y = MLeft1($pdf, "9.4", $y + 5);
   $y = MLeft2($pdf, "The ta’widh in respect of an unpaid amount, being part of the late payment charges, may accrue on a daily basis and shall be calculated in accordance with the following formula:-", $y - 7);

   $y = MCenter1($pdf, "Actual cost incurred", $y + 3);
   $y = MCenter1($pdf, "or", $y);
   $y = MCenter1($pdf, "Unpaid Amount X 1% X No. of Overdue day(s) / 365", $y);

   $y = MLeft1($pdf, "9.5", $y + 5);
   $y = MLeft2($pdf, "The amount of ta’widh shall not be compounded.", $y - 7);

   $y = MLeft1($pdf, "9.6", $y + 5);
   $y = MLeft2($pdf, "The Investors Group hereby agree(s), covenant(s) and undertake(s) to ensure that the balance of the late payment charges (“gharamah”), if any, shall be paid to charitable bodies as may be selected by the Company in consultation with its Shariah advisers.", $y - 7);

   $y = MLeft1($pdf, "10. <b>GOVERNING LAW AND DISPUTE RESOLUTION</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "10.1", $y + 5);
   $y = MLeft2($pdf, "This Agreement and the rights and obligations of the Parties hereunder shall be governed by and interpreted and construed in all respects in accordance with the laws of Singapore without prejudice to or limitation of any other rights or remedies available to both Parties in this Agreement under the laws of any jurisdiction where the Company or his assets may be located.", $y - 7);

   $y = MLeft1($pdf, "10.2", $y + 5);
   $y = MLeft2($pdf, "The Parties hereby irrevocably agree to submit to the exclusive jurisdiction of the Courts of Singapore to resolve any disputes or make any claims with respect to this Agreement.", $y - 7);

   $y = MLeft1($pdf, "10.3", $y + 5);
   $y = MLeft2($pdf, "The Parties agree that if any difference, dispute, conflict or controversy <b>(a 'Dispute')</b>&nbsp;, arises out of or in connection with this Agreement or its performance, including without limitation any dispute regarding its existence, validity, or termination of rights or obligations of any Party, the Parties will attempt for a period of 30 (thirty) days after the receipt by one Party of a notice from the other Party of the existence of the Dispute to settle the Dispute by amicable settlement between the Parties", $y - 7);

   $y = MLeft1($pdf, "11. <b>MISCELLANEOUS</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "11.1", $y + 5);
   $y = MLeft2($pdf, "If at any time any one or more of the provisions hereof is or become illegal, invalid or unenforceable under Singapore law, neither the legality, validity or enforceability of the remaining provisions hereof nor the legality, validity or enforceability of such provisions under the laws of any other jurisdiction shall in any way be affected or impaired thereby.", $y - 7);

   $y = MLeft1($pdf, "11.2", $y + 5);
   $y = MLeft2($pdf, "If any provision of this Agreement (or part of it) or the application thereof to any person or circumstance shall be illegal, invalid or unenforceable to any extent, it must be interpreted as narrowly as necessary to allow it to be enforceable or valid and the remainder of this Agreement and the legality, validity or enforceability of such provisions to other persons or circumstances shall not be in any way affected or impaired thereby and shall be enforceable / enforced to the greatest extent permitted by law.", $y - 7);

   $y = MLeft1($pdf, "11.3", $y + 5);
   $y = MLeft2($pdf, "All rights and obligations in this Agreement are personal to the Parties and each Party in this Agreement may not assign and/or transfer any such rights and obligations to any third party without the prior consent in writing of the Parties.", $y - 7);

   $y = MLeft1($pdf, "11.4", $y + 5);
   $y = MLeft2($pdf, "This Agreement contains the entire understanding between the Parties relating to the transaction contemplated by this Agreement and shall supersede any prior expressions of intent or understandings with respect to the said transaction. All prior or contemporaneous agreements, understandings, representations and statements, oral and written, are merged in this Agreement and shall be of no further force or effect.", $y - 7);

   $y = MLeft1($pdf, "11.5", $y + 5);
   $y = MLeft2($pdf, "Any communication to be made under or in connection with this Agreement shall be made in writing and, may be made by letter or electronic mail, in each case, through Kapital Boost at <b>116 Changi Road, #05-00, Singapore 419718</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; or at such other address as Kapital Boost may notify the parties from time to time. Any communication made between Kapital Boost and any of the Parties under or in connection with this Agreement shall be made to the address or electronic mail address provided to Kapital Boost or its registered address, in the case of the Company, and shall be effective when received.", $y - 7);

   $y = MLeft1($pdf, "11.6", $y + 5);
   $y = MLeft2($pdf, "No failure to exercise, nor any delay in exercising, on the part of the Financier, any right or remedy under this Agreement shall operate as a waiver, nor shall any single or partial exercise of any right or remedy prevent any further or other exercise or the exercise of any other right or remedy. The rights and remedies under this Agreement are cumulative and not exclusive of any rights or remedies provided by law.", $y - 7);

   $y = MLeft1($pdf, "11.7", $y + 5);
   $y = MLeft2($pdf, "No provision of this Agreement may be amended, waived, discharged or terminated orally nor may any breach of or default under any of the provisions of this Agreement be waived or discharged orally but (in each case) except (a) pursuant to an Adjustment Notice which complies with the requirements of this Agreement; or (b) by an instrument in writing signed by or on behalf of the Parties or (b). Any amendments or variations to this Agreement shall be Shariah-compliant.", $y - 7);

   $y = MLeft1($pdf, "11.8", $y + 5);
   $y = MLeft2($pdf, "This Agreement may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);

   $y = MLeft1($pdf, "11.9", $y + 5);
   $y = MLeft2($pdf, "Each Party agrees to keep all information relating to this Agreement confidential, and not disclose it to anyone, save with the prior written consent of the other Parties or as required by any applicable laws or regulations.", $y - 7);

   $y = MLeft1($pdf, "11.10", $y + 5);
   $y = MLeft2($pdf, "This Agreement is intended to be Shariah-compliant. The parties hereby agree and acknowledge that their respective rights and obligations under this Agreement are intended to, and shall, be in conformity with Shariah principles.", $y - 7);

   $y = MLeft1($pdf, "11.11", $y + 5);
   $y = MLeft2($pdf, "Notwithstanding the above, each party represents to the other that it shall not raise any objections or claims against the other on the basis of Shariah-compliance or any breach of Shariah principles in respect of or otherwise in relation to any part of any provision of this Agreement.  ", $y - 7);


   $y = MLeft1($pdf, "12. <b>DEFINITIONS</b>&nbsp;", $y + 15);

   $y = MLeft1($pdf, "In this Agreement, any reference to:", $y + 5);

   $y = MLeft1($pdf, "12.1", $y + 5);
   $y = MLeft2($pdf, "any Party or Kapital Boost shall be construed so as to include its successors in title, permitted assigns and permitted transferees;", $y - 7);

   $y = MLeft1($pdf, "12.2", $y + 5);
   $y = MLeft2($pdf, "a provision of law is a reference to that provision as amended or re-enacted;", $y - 7);

   $y = MLeft1($pdf, "12.3", $y + 5);
   $y = MLeft2($pdf, "a person or Party in a particular gender shall include that person or Party regardless of his or her gender, and shall apply equally where that person or Party is a body corporate, firm, trust, joint venture or partnership;", $y - 7);

   $y = MLeft1($pdf, "12.4", $y + 5);
   $y = MLeft2($pdf, "the \"<b>Investors Group</b>&nbsp;&nbsp; \" shall be construed to mean just the Financier if the Financier is financing the entirety of the Purchase Cost;", $y - 7);

   $y = MLeft1($pdf, "12.5", $y + 5);
   $y = MLeft2($pdf, "\"<b>Kapital Boost</b>&nbsp;&nbsp; \" means <b>Kapital Boost Pte Ltd, Singapore Company Registration No. 201525866W and PT Kapital Boost Indonesia, registered in the presence of Notary Panji Kresna S.H., M.Kn. on 23 March 2016</b>&nbsp;", $y - 7);

   $y = MLeft1($pdf, "12.6", $y + 5);
   $y = MLeft2($pdf, "\"<b>Sale Price</b>&nbsp;&nbsp; \" shall mean the Sale Price as adjusted pursuant to Clause 1.6.6.", $y - 7);

   if ($y > 160) { // Shift the sign table below
      $pdf->AddPage('P', array('210', '297'));
      $y = 0;
   }

    $signTbl = "<table cellspacing=\"0\" cellpadding=\"10\" border=\"1\">
  <tr>
  <td ><b>The Company</b>&nbsp;<br>Signed for and on behalf of<br><b>$companyName</b>&nbsp;</td>
  <td ><b>The Financier</b>&nbsp;</td>
  </tr>
  <tr>
       <td style=\"line-height:20px\"><br><br><br><hr><br><br><br><b>Name: $dirName</b>&nbsp;<br><b>Title: $dirTitle</b>&nbsp;<br><b>$dirIC</b>&nbsp;</td>
       <td style=\"line-height:20px\"><br><br><br><hr><br><br><br><b>$invName</b>&nbsp;<br><b>$invIC</b>&nbsp;</td>
</tr>

  </table>";

   $pdf->writeHTML($signTbl);
   $pdf->SetMargins(25, 15);
   $y = MCenter1($pdf, "<b>Dated this day $date</b>&nbsp;<br>", $y + 5);

   $iSignx = 305;
   $iSigny = $oSigny = (($y + 30) * $pdf->getScaleFactor()) + 5;
   $iSignp = $oSignp = $pdf->getPage();
   $oSignx = 80;




   // $pdf->writeHTML($signTbl);
   $pdf->AddPage('P', array('210', '297'));
   $y = MLeft1($pdf, "<b>SCHEDULE A: Description of Assets</b>&nbsp;", 20);

   $pdf->Image($image1, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);

   if ($imageD2 != "" or $imageD2 != null) {
	   $pdf->AddPage();
	   $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	   // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
	   $pdf->Image($image2, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD3 != "" or $imageD3 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image3, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD4 != "" or $imageD4 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image4, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD5 != "" or $imageD5 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image5, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD6 != "" or $imageD6 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image6, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD7 != "" or $imageD7 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image7, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD8 != "" or $imageD8 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image8, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD9 != "" or $imageD9 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image9, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD10 != "" or $imageD10 != null) {
		$pdf->AddPage();
	    $y = Left1($pdf, "<b>Description of Assets</b>&nbsp;", 20);
	    // $y = Left1($pdf, "<b>Deskripsi Aset</b>&nbsp;", 27);
		$pdf->Image($image10, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetMargins(25, 15);

   $y = MLeft1($pdf, "<b style=\"font-size:1.2em\">SCHEDULE B: Form of Murabahah Sale Contract</b>&nbsp;", 20);

   $y = MCenter1($pdf, "<b>Murabahah Sale Contract</b>&nbsp;", $y + 15);

   $y = MCenter1($pdf, "Between $companyName (the \"<b>Company</b>&nbsp; \") and the Investors Group", $y + 5);

   $y = MLeft1($pdf, "Re: &nbsp;&nbsp;Murabahah Agreements between the Company and various Financiers (together forming the Investors Group) with respect to the purchase of the Asset described below (the \"<b>Murabahah Agreements</b>&nbsp;&nbsp;&nbsp;\", and each, a \"<b>Murabahah Agreement</b>&nbsp;&nbsp;&nbsp;\"). ", $y + 15);

   $y = MLeft1($pdf, "1.", $y + 10);
   $y = MLeft2($pdf, "All terms defined in the Murabahah Agreements have the same meaning in this Murabahah Sale Contract.", $y - 7);

   $y = MLeft1($pdf, "2.", $y + 5);
   $y = MLeft2($pdf, "The Company hereby confirms that it, as agent of the Investors Group, has purchased the Asset, and that the Asset is in the Company’s possession. The Company has attached to this Murabahah Sale Contract (a) a description of the Asset; and (b) a true copy of the receipt for the purchase of the Asset by us, as agent of the Investors Group.", $y - 7);

   $y = MLeft1($pdf, "3.", $y + 5);
   $y = MLeft2($pdf, "In accordance with the terms of the Murabahah Agreements, the Company hereby offers to purchase the Asset from the Investors Group based on the terms and subject to the conditions set out in the Murabahah Agreements. Pursuant to the terms of the Murabahah Agreements, the Sale Price is $salePrice and will be paid as follows:", $y - 7);

   $payoutBString = "";
   for ($i = 0; $i < count($payoutDate); $i++) {
      $payoutBString .= "<tr><td style=\"text-align:center\">$payoutDate[$i]</td><td style=\"text-align:center\">$salePrice</td></tr>";
   }

   $payoutBTbl = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b>&nbsp;</td>
  <td style=\"text-align:center\"><b>Installment Amount of Sale Price payable to Investors Group</b>&nbsp;</td>
  </tr>
  $payoutBString
  </table>
  ";
   $pdf->SetY($y + 10);
   $pdf->writeHTML($payoutBTbl);
   $y = $pdf->GetY();

   $y = MLeft1($pdf, "4.", $y);
   $y = MLeft2($pdf, "By accepting and agreeing to this Murabahah Sale Contract, the Investors Group shall immediately sell the Asset to the Company on the terms and subject to the conditions set out in the Murabahah Agreement.", $y - 7);

   $y = MLeft1($pdf, "5.", $y + 10);
   $y = MLeft2($pdf, "This Murabahah Sale Contract may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);

  $y = MLeft2($pdf, "For and on behalf of the Company (as buyer of Asset)", $y + 10);

   $y = MLeft2($pdf, "<hr style=\"width:150;\">", $y + 10);
   $y = MLeft2($pdf, "Name:", $y + 2);
   $y = MLeft2($pdf, "Title:", $y + 2);
   $y = MLeft2($pdf, "Date:", $y + 2);

   $y = MLeft2($pdf, "Kapital Boost, as agent for and on behalf of the Investors Group (as seller of Asset)", $y + 10);

   $y = MLeft2($pdf, "<hr style=\"width:150\">", $y + 10);
   $y = MLeft2($pdf, "Name:", $y + 2);
   $y = MLeft2($pdf, "Title:", $y + 2);
   $y = MLeft2($pdf, "Date:", $y + 2);


   $output = $pdf->Output('/var/www/kapitalboost.com/public_html/assets/pdfs/' . $token . '.pdf', 'F');
   // $output = $pdf->Output('/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com/assets/pdfs/' . $token . '.pdf', 'F');

} else if ($conType == 1) {

if ($subtype=="INVOICE  FINANCING") {

   $companyNameID = $comNameId.",";
   $companyRegistrationID = $comRegId;
   $purchaseAssetID = $purchaseAssetId;

   $closeMonth = date('n', strtotime($closeDate));
   $closeMonthId = $monthsID[$closeMonth];
   $closeDay = date('d', strtotime($closeDate));
   $closeYear = date('Y', strtotime($closeDate));
   $cCloseDateID = "$closeDay $closeMonthId $closeYear";

   $closeMonth1 = date('n', strtotime($cCloseDate1));
   $closeMonth1Id = $monthsID[$closeMonth1];
   $closeDay1 = date('d', strtotime($cCloseDate1));
   $closeYear1 = date('Y', strtotime($cCloseDate1));
   $cCloseDate1ID = "$closeDay1 $closeMonth1Id $closeYear1";

   $dirICID = $dirIcId;
   $invICID = $icId;

   // if($invICID == ''){
      // echo 'no investor IC for Bahasa';
      // exit;
   // }


   $pdf = new FPDI();
   $pdf->setPrintHeader(false);
   $pdf->setPrintFooter(false);


   $pdf->AddPage('P', array('210', '300'));
   $pdf->SetFont('Helvetica');



   $y = 20;
// awal invoice
   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "_______________________ <br> ", $y);

   $pdf->SetFontSize("12");
   $y = CenterR($pdf, "<b>PERJANJIAN QARD NO. Q$invoContractNo <br> Perjanjian Pinjaman Berdasarkan Prinsip <br>Syariah Qard</b>&nbsp;", $y);

   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "<br> _______________________ ", $y - 6);
   $pdf->SetFontSize("11.5");

   $y = Right1invo($pdf, "<b>PERJANJIAN QARD NO. Q$invoContractNo ini (“Perjanjian”)</b>&nbsp;&nbsp;&nbsp; dibuat pada <b>$dateID</b>&nbsp;&nbsp;,", $y + 5);

   $y = Right1invo($pdf, "<b>ANTARA:</b>&nbsp;", $y + 5);

   $y = Right1invo($pdf, "1. <b>$companyName</b>&nbsp;<br>suatu perusahaan perseroan terbatas yang resmi didirikan, dan tunduk berdasarkan hukum negara Republik Indonesia, berdasarkan Akta Pendirian Nomor $invoAktaNoId dengan $comRegTypeInaText $comRegId dan berkedudukan di Republik Indonesia (“<b>Peminjam</b>&nbsp;”)&nbsp;", $y + 5);

    $y = Right1invo($pdf, "dan", $y + 5);
   $y = Right1invo($pdf, "2. <b>$invName</b>&nbsp; &nbsp;, warga negara $memberCountry, pemegang $invICIDs (“<b>Pemodal</b>&nbsp;”)&nbsp;", $y + 5);

   $y = Right1invo($pdf, "Pemodal dan Peminjam disebut secara bersama-sama sebagai “Para Pihak” dan secara masing-masing sebagai “Pihak”.", $y + 5);

   $y = Right1invo($pdf, "<b>BAHWA:</b>&nbsp;", $y + 5);

   $y = Right1invo($pdf, "A.", $y + 5);

   $y = Right2invo($pdf, "Peminjam membutuhkan pendanaan jangka pendek untuk menjembatani kebutuhan modal kerja Usahanya (sebagaimana didefinisikan di bawah ini). ", $y - 5.3);

   $y = Right1invo($pdf, "B.", $y + 5);

   $y = Right2invo($pdf, "Pemodal telah sepakat untuk menyediakan kepada Peminjam suatu Fasilitas (sebagaimana didefinisikan di bawah ini) dalam mata uang Dolar SIngapura yang sama atau setara dengan $invBack2% dari Jumlah Piutang (sebagaimana didefinisikan di bawah ini) berdasarkan prinsip Syariah Qard, dengan syarat dan ketentuan Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "<b>DENGAN INI DISEPAKATI </b>&nbsp; sebagai berikut:", $y + 10);
   $y = Right1invo($pdf, "<b>1.	DEFINISI DAN PENAFSIRAN</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "1.1", $y);

   $y = Right2invo($pdf, "Dalam Perjanjian ini, istilah-istilah berikut ini memiliki pengertian sebagai berikut:", $y - 5.3);

    $tbl1 = "<table cellspacing=\"10\" cellpadding=\"0\" border=\"0\" width=\"360px\">
  <tr>
  <td align=\"justify\" width=\"80px\"><b>“Usaha”</b>&nbsp;</td>
  <td align=\"justify\">: adalah kegiatan usaha Peminjam yang tidak bertentangan dengan prinsip Syariah. </td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Hari Kerja”</b>&nbsp;</td>
  <td align=\"justify\">: adalah hari (selain Sabtu, Ahad atau hari libur nasional) di mana bank-bank di Indonesia biasanya buka untuk kegiatan usaha.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Komitmen”</b>&nbsp;</td>
  <td align=\"justify\">: adalah jumlah yang sama atau setara dengan $invBack2% dari Jumlah Piutang atau setara dengan $invAmount, sejauh tidak dibatalkan atau dikurangi berdasarkan Perjanjian ini</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Pencairan”</b>&nbsp;</td>
  <td align=\"justify\">: adalah pemanfaatan Fasilitas</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Tanggal Pencairan”</b>&nbsp;</td>
  <td align=\"justify\">: adalah tanggal Pencairan, yakni $closdate, tanggal dilakukannya Pinjaman terkait.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Sengketa”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 11.2.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Dokumen Elektronik”</b>&nbsp;</td>
  <td align=\"justify\">: adalah informasi elektronik yang dibuat, diteruskan, dikirimkan, diterima, atau disimpan dalam bentuk analog, digital, elektromagnetik, optik, atau sejenisnya, yang dapat dilihat, dapat ditampilkan dan/atau dapat didengar melalui komputer atau Sistem Elektronik, termasuk tetapi tidak terbatas pada tulisan, bunyi, gambar, peta, rancangan, foto atau sejenisnya, huruf, tanda, gambar, kode akses, simbol atau perforasi yang memiliki pengertian atau definisi tertentu atau dapat dimengerti oleh para pihak yang memenuhi syarat untuk memahaminya.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Sistem Elektronik”</b>&nbsp;</td>
  <td align=\"justify\">: adalah www.kapitalboost.com, suatu platform urun dana bersama (peer to peer crowdfunding) yang dikelola dan dilaksanakan oleh Agen Substitusi.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Peristiwa Wanprestasi”</b>&nbsp;</td>
  <td align=\"justify\">: adalah peristiwa atau keadaan sebagaimana ditetapkan dalam Pasal 10.1.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Fasilitas”</b>&nbsp;</td>
  <td align=\"justify\">: adalah fasilitas pinjaman bebas bunga (menurut prinsip Syariah Qard) yang disediakan berdasarkan Perjanjian ini oleh Pemodal kepada Peminjam, menurut syarat dan ketentuan Perjanjian ini.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Gharamah”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 6.6.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Kapital Boost”</b>&nbsp;</td>
  <td align=\"justify\">: adalah Kapital Boost Pte Ltd, sebagai penyedia Sistem Elektronik.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rekening Eskro Kapital Boost”</b>&nbsp;</td>
  <td align=\"justify\">: adalah rekening bank yang dikelola oleh Kapital Boost untuk maksud Perjanjian ini.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rekening Virtual Kapital Boost”</b>&nbsp;</td>
  <td align=\"justify\">: adalah rekening virtual, yang perinciannya diatur dalam Sistem Elektronik, yang diberikan oleh Kapital Boost kepada Pemodal semata-mata untuk tujuan (i) pencairan Fasilitas, dan (ii) menerima pelunasan Pinjaman.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Pinjaman”</b>&nbsp;</td>
  <td align=\"justify\">: adalah, sebagaimana disyaratkan berdasarkan konteks, pinjaman bebas bunga yang dilakukan atau akan dilakukan berdasarkan Fasilitas atau jumlah pokok yang tertunggak pada waktu pinjaman tersebut.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Tanggal Jatuh Tempo”</b>&nbsp;</td>
  <td align=\"justify\">: adalah $invoMaturityDate, tanggal di mana Piutang menjadi jatuh tempo dan wajib dibayarkan kepada Prinsipal.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Piutang”</b>&nbsp;</td>
  <td align=\"justify\" style=\"font-size: 11pt\">: adalah piutang yang wajib dibayarkan kepada Prinsipal yang timbul berdasarkan Dokumen Pokok sehubungan dengan barang dan/atau jasa yang diberikan oleh Prinsipal, yang dapat diterima oleh Agen.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Jumlah Piutang”</b>&nbsp;</td>
  <td align=\"justify\" style=\"font-size: 11pt\">: adalah Rp$invoAssetCost atau SGD$invoAssetCostSGD yang merupakan jumlah pokok Piutang yang wajib dibayarkan kepada Prinsipal pada Tanggal Jatuh Tempo</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Tanggal Pelunasan”</b>&nbsp;</td>
  <td align=\"justify\">: adalah $invoRepaymentDate.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rp” atau “Rupiah”</b>&nbsp;</td>
  <td align=\"justify\">: adalah mata uang sah Indonesia.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“SGD” atau “Dollar Singapura”</b>&nbsp;</td>
  <td align=\"justify\">: adalah mata uang sah Singapura.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Ta’widh”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 6.3.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Dokumen Pokok”</b>&nbsp;</td>
  <td align=\"justify\" style=\"font-size: 11pt\">: $invoDocsId</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(110, 10, 5);
   $pdf->SetX(107);
   $pdf->SetFontSize(11.5);
   $pdf->writeHTML($tbl1);
   $pdf->SetFontSize(11.5);
   $y = $pdf->GetY();

   $y = Right1invo($pdf, "1.2", $y);

   $y = Right2invo($pdf, "Kecuali jika terdapat petunjuk yang bertentangan, rujukan apa pun dalam Perjanjian ini:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "rujukan pada dokumen apa pun (termasuk Perjanjian ini) mencakup rujukan pada dokumen tersebut yang diubah, digabungkan, ditambah, dinovasi atau digantikan; rujukan pada suatu perjanjian mencakup janji, pernyataan, akta, perjanjian atau putusan yang berlaku secara sah, skema atau kesepahaman baik tertulis atau tidak;", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "“termasuk” ditafsirkan sebagai “termasuk tidak terbatas” (dan ungkapan sejenis akan ditafsirkan serupa);", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "rujukan pada “pihak” mencakup rujukan pada perseorangan, firma, perusahaan, koperasi, korporasi, pemerintah, perusahaan perseroan terbatas, negara atau instansi negara atau asosiasi, peramanatan (trust) atau wali amanat, dana pensiun, usaha patungan, konsorsium atau persekutuan (baik memiliki kedudukan hukum yang terpisah atau tidak);", $y - 5.3);

   $y = Right3invo($pdf, "(d)", $y + 2);

   $y = Right4invo($pdf, "rujukan pada “tertulis” atau “secara tertulis” mencakup metode penggandaan kata dalam bentuk yang dapat dibaca dan tidak bersifat sementara;", $y - 5.3);

   $y = Right3invo($pdf, "(e)", $y + 2);

   $y = Right4invo($pdf, "kecuali jika terdapat maksud yang bertentangan yang secara tegas ditunjukkan berdasarkan konteks, kata atau ungkapan yang menunjukkan suatu jenis kelamin mencakup jenis kelamin yang lain, dan kata bentuk tunggal mencakup kata bentuk jamak dan sebaliknya; dan", $y - 5.3);

   $y = Right3invo($pdf, "(f)", $y + 2);

   $y = Right4invo($pdf, "setiap rujukan, secara tegas atau tersirat, terhadap undang-undang atau ketentuan berdasarkan undang-undang akan ditafsirkan sebagai rujukan terhadap undang-undang atau ketentuan tersebut sebagaimana masing-masing diubah atau diberlakukan kembali atau sebagaimana penerapannya diubah dari waktu ke waktu dengan ketentuan lain (baik sebelum atau sesudah tanggal Perjanjian ini) dan akan mencakup undang-undang atau ketentuan yang merupakan pemberlakuannya kembali (baik dengan atau tanpa perubahan) dan putusan, peraturan, instrumen atau perundangan turunan lainnya berdasarkan undang-undang atau ketentuan berdasarkan undang-undang yang terkait.", $y - 5.3);

   $y = Right1invo($pdf, "1.3", $y + 5);

   $y = Right2invo($pdf, "Judul Pasal dan Lampiran hanya untuk memudahkan saja dan tidak digunakan dalam penafsirannya.", $y - 5.3);

   $y = Right1invo($pdf, "1.4", $y + 5);

   $y = Right2invo($pdf, "Rujukan pada Perjanjian ini merupakan rujukan pada Perjanjian ini dan aneks, lampiran dan ekshibit.", $y - 5.3);

   $y = Right1invo($pdf, "1.5", $y + 5);

   $y = Right2invo($pdf, "Rujukan pada Perjanjian ini pada Pasal dan Lampiran adalah pada pasal dan lampiran dalam Perjanjian ini (kecuali jika disyaratkan lain berdasarkan konteks). Pembukaan dan Lampiran pada Perjanjian ini dianggap merupakan bagian dari Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "1.6", $y + 5);

   $y = Right2invo($pdf, "Apabila istilah apa pun didefinisikan dalam suatu pasal tertentu selain Pasal 1.1 maka istilah tersebut akan memiliki pengertian yang ditetapkan dalam pasal tersebut di mana saja digunakan dalam Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "1.7", $y + 5);

   $y = Right2invo($pdf, "Apabila jumlah hari akan dihitung dari suatu hari tertentu maka jumlah tersebut akan dihitung di luar hari tertentu tersebut dan dimulai pada hari berikutnya. Jika hari terakhir dalam jumlah yang dihitung tersebut jatuh pada hari yang bukan Hari Kerja maka hari terakhir tersebut akan ditetapkan pada hari pengganti berikutnya yang merupakan Hari Kerja.", $y - 5.3);

   $y = Right1invo($pdf, "1.8", $y + 5);

   $y = Right2invo($pdf, "Setiap rujukan pada hari (selain rujukan pada Hari Kerja), bulan atau tahun adalah rujukan pada masing-masing hari kalender, bulan kalender atau tahun kalender.", $y - 5.3);

   $y = Right1invo($pdf, "1.9", $y + 5);

   $y = Right2invo($pdf, "Setiap istilah yang merujuk pada konsep atau proses hukum Indonesia dianggap mencakup rujukan pada konsep atau proses yang setara atau sama dalam yurisdiksi lainnya di mana Perjanjian ini dapat berlaku atau pada hukum di mana salah satu Pihak dapat atau menjadi tunduk padanya.", $y - 5.3);

   $y = Right1invo($pdf, "1.10", $y + 5);

   $y = Right2invo($pdf, "Ketentuan Perjanjian ini telah dinegosiasikan, pihak manapun yang menafsirkan Perjanjian ini tidak akan menggunakan aturan yang menjelaskan bahwa dalam hal terdapat ambiguitas atau ketaksaan maka suatu kontrak akan ditafsirkan bertentangan terhadap Pihak yang bertanggung jawab atas penyusunan kontrak tersebut.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

    $y = Right1invo($pdf, "<b>2. FASILITAS</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "2.1", $y + 5);

   $y = Right2invo($pdf, "Dengan tunduk pada ketentuan Perjanjian ini, Pemodal sepakat untuk menyediakan kepada Peminjam suatu Fasilitas pinjaman jangka pendek dalam Dolar Singapura sejumlah Komitmen", $y - 5.3);

   $y = Right1invo($pdf, "2.2", $y + 5);

   $y = Right2invo($pdf, "Peminjam sepakat dan berjanji dengan Pemodal bahwa Peminjam akan memanfaatkan Fasilitas dana talangan semata-mata untuk modal kerja Usaha.", $y - 5.3);

   $y = Right1invo($pdf, "<b>3. PERSYARATAN PENCAIRAN </b>&nbsp;", $y + 10);

    $y = Right1invo($pdf, "Pemodal tidak disyaratkan untuk menyediakan Fasilitas kecuali jika Pemodal telah memperoleh seluruh dokumen berikut dan bukti lain dalam bentuk dan isi yang memuaskan bagi Pemodal, yang tersedia atau telah diunggah ke Sistem Elektronik sebelum atau pada tanggal Perjanjian ini:", $y + 1);

   $y = Right1invo($pdf, "3.1", $y + 5);

   $y = Right2invo($pdf, "Salinan resmi sebenarnya dari Dokumen Pokok;", $y - 5.3);

   $y = Right1invo($pdf, "3.2", $y + 5);

   $y = Right2invo($pdf, "salinan resmi sebenarnya dari akta pendirian, anggaran dasar dan dokumen perusahaan yang berlaku lainnya dari Peminjam; dan", $y - 5.3);

   $y = Right1invo($pdf, "3.3", $y + 5);

   $y = Right2invo($pdf, "salinan resmi sebenarnya dari dokumen identitas foto para Direktur", $y - 5.3);

   $y = Right1invo($pdf, "<b>4. PENCAIRAN </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "<b>4.1</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Penyelesaian Pencairan </b>", $y - 5.3);

   $y = Right1invo($pdf, "4.1.1", $y + 5);

   $y = Right41invo($pdf, "Dengan tunduk pada ketentuan Perjanjian ini, Pemodal hanya akan diwajibkan menyediakan Fasilitas jika:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "Peminjam telah mematuhi sepenuhnya persyaratan Pasal 3;", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "Tanggal Pencairan yang diajukan adalah Hari Kerja;", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "mata uang dan jumlah Pencairan mematuhi Pasal 4.2 (Mata Uang dan Jumlah);", $y - 5.3);

   $y = Right3invo($pdf, "(d)", $y + 2);

   $y = Right4invo($pdf, "tidak ada wanprestasi yang telah terjadi atau akan, atau kemungkinan akan, terjadi sebagai akibat Pencairan yang dilakukan; dan", $y - 5.3);

   $y = Right3invo($pdf, "(e)", $y + 2);

   $y = Right4invo($pdf, "seluruh pernyataan dan jaminan yang dilakukan oleh Peminjam dalam atau sehubungan dengan Perjanjian ini adalah benar dan tepat sebagaimana pada tanggal akan dilakukannya Pencairan dengan mengacu pada fakta-fakta dan keadaan yang saat itu ada.", $y - 5.3);

   $y = Right1invo($pdf, "4.1.2", $y + 5);

   $y = Right41invo($pdf, "Pemodal akan mentransfer jumlah Fasilitas ke Rekening Virtual Kapital Boost dalam waktu 3 Hari Kerja setelah tanggal Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "<b>4.2</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Mata Uang dan Jumlah</b>", $y - 5.3);

   $y = Right1invo($pdf, "4.2.1", $y + 5);

   $y = Right41invo($pdf, "Jumlah Pinjaman adalah sejumlah Komitmen, dan mata uang harus dalam Dolar Singapura.", $y - 5.3);

   $y = Right1invo($pdf, "4.2.2", $y + 5);

   $y = Right41invo($pdf, "Para Pihak menyepakati bahwa untuk maksud Pinjaman, apabila Jumlah Piutang dalam mata wang Rupiah, untuk mengonversi mata uang dalam jumlah tersebut ke dalam Dolar Singapura menggunakan kurs spot sesuai dengan ketentuan sebagaimana disebutkan dalam Sistem Elektronik.", $y - 5.3);

   $y = Right1invo($pdf, "<b>4.3</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Penarikan Pinjaman</b>", $y - 5.3);

    $y = Right2invo($pdf, "Apabila persyaratan yang terdapat dalam pasal ini telah dipenuhi, Pemodal akan menyediakan Pinjaman pada Tanggal Pencairan.", $y + 1);

   $y = Right1invo($pdf, "<b>5. PELUNASAN DAN PEMBAYARAN DI MUKA </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "<b>5.1</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Pelunasan</b>", $y - 5.3);

   $y = Right1invo($pdf, "5.1.1", $y + 1);

   $y = Right41invo($pdf, "Peminjam harus segera melunasi Pinjaman pada Tanggal Pelunasan ke Rekening Eskro Kapital Boost.", $y - 5.3);

   $y = Right1invo($pdf, "5.1.2", $y + 5);

   $y = Right41invo($pdf, "Seluruh pembayaran oleh Peminjam berdasarkan Perjanjian ini harus dilakukan secara penuh tanpa pengurangan atau pemotongan (baik berkenaan dengan perjumpaan utang, gugatan balik, bea, pajak, biaya atau apa pun yang lainnya) kecuali jika pengurangan atau pemotongan tersebut diwajibkan berdasarkan hukum, yang dalam hal tersebut Peminjam harus:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "memastikan bahwa pengurangan atau pemotongan tersebut tidak melebihi jumlah minimum yang disyaratkan secara hukum;", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "segera membayar kepada Pemodal atas jumlah tambahan tertentu sehingga jumlah bersih yang diterima oleh Pemodal setara dengan jumlah penuh yang akan telah diterima oleh Pemodal jika pengurangan atau pemotongan tersebut tidak dilakukan;", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "membayar kepada otoritas pajak yang terkait atau otoritas lain, dalam jangka waktu pembayaran yang diizinkan berdasarkan hukum yang berlaku, atas jumlah penuh dari pengurangan atau pemotongan tersebut; dan", $y - 5.3);

   $y = Right3invo($pdf, "(d)", $y + 2);

   $y = Right4invo($pdf, "saat telah dibayarkan, memberikan kepada Pemodal tanda terima resmi dari otoritas pajak yang terkait atau otoritas lain yang terlibat atas seluruh jumlah yang dikurangi atau dipotong sebagaimana disebutkan di atas.", $y - 5.3);

   $y = Right1invo($pdf, "5.1.3", $y + 5);

   $y = Right41invo($pdf, "Peminjam tidak boleh meminjam kembali sebagian dari Fasilitas yang dilunasi.", $y - 5.3);

   $y = Right1invo($pdf, "<b>5.2</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Pembayaran di Muka Sukarela</b>", $y - 5.3);

   $y = Right2invo($pdf, "Dengan diskresi Peminjam, Pinjaman dapat dilunasi tanpa denda pembayaran di muka.", $y + 1);

   $y = Right1invo($pdf, "<b>6. BIAYA KETERLAMBATAN PEMBAYARAN  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "6.1", $y + 5);

   $y = Right2invo($pdf, "Apabila jumlah yang jatuh tempo dan wajib dibayarkan oleh Peminjam berdasarkan atau sehubungan dengan Perjanjian ini belum dilunasi pada Tanggal Pelunasan (“jumlah yang belum dibayarkan”), Peminjam berjanji untuk membayar biaya keterlambatan pembayaran termasuk ta’widh dan gharamah (dihitung di bawah ini) kepada Pemodal pada setiap hari di mana jumlah yang tertunggak belum dibayarkan.", $y - 5.3);

   $y = Right1invo($pdf, "6.2", $y + 5);

   $y = Right2invo($pdf, "Keseluruhan biaya keterlambatan pembayaran akan dipatok langsung pada laba rata-rata disetahunkan dari Perjanjian ini (akan ditentukan oleh Pemodal), yang dihitung setiap hari sebagaimana di bawah ini, dengan ketentuan bahwa akumulasi biaya keterlambatan pembayaran tidak akan melebihi 100% (seratus persen) dari Pinjaman:", $y - 5.3);

   $y = Right2invo($pdf, "jumlah yang belum dibayarkan X laba rata-rata disetahunkan X jumlah hari lewat jatuh tempo / 365 hari", $y + 5);

   $y = Right1invo($pdf, "6.3", $y + 5);

   $y = Right2invo($pdf, "Pemodal dapat menahan hanya bagian tertentu dari jumlah keterlambatan pembayaran yang dibayarkan kepadanya, sebagaimana diperlukan untuk memberikan penggantian kepada Pemodal atas setiap biaya sesungguhnya (tidak termasuk biaya peluang atau biaya pendanaan), dengan tunduk pada dan menurut pedoman para penasihat Syariahnya sendiri, dan hingga jumlah keseluruhan maksimum 1% (satu persen) dari jumlah yang belum dibayarkan (“Ta’widh”).", $y - 5.3);

   $y = Right1invo($pdf, "6.4", $y + 5);

   $y = Right2invo($pdf, "Ta’widh sehubungan dengan jumlah yang belum dibayarkan, yang merupakan bagian dari biaya keterlambatan pembayaran, dapat bertambah setiap hari dan dihitung menurut rumus berikut: ", $y - 5.3);

   $y = Right2invo($pdf, "biaya sesungguhnya yang dikeluarkan atau jumlah yang belum dibayarkan X 1% X jumlah hari lewat jatuh / 365 hari ", $y + 5);

   $y = Right1invo($pdf, "6.5", $y + 5);

   $y = Right2invo($pdf, "Jumlah Ta’widh tidak akan dilipatgandakan. ", $y - 5.3);

   $y = Right1invo($pdf, "6.6", $y + 5);

   $y = Right2invo($pdf, "Pemodal dengan ini menyepakati, berjanji dan menyanggupi untuk memastikan bahwa sisa biaya keterlambatan (“Gharamah”), jika ada, akan dibayarkan kepada badan-badan amal yang dapat dipilih oleh Peminjam dengan berkonsultasi dengan para penasihat Syariahnya. ", $y - 5.3);

   $y = Right1invo($pdf, "<b>7.	PERNYATAAN DAN JAMINAN  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "7.1", $y + 5);

   $y = Right2invo($pdf, "Peminjam menyatakan dan menjamin kepada Pemodal bahwa: ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.1", $y + 5);

   $y = Right41invo($pdf, "Peminjam resmi didirikan dan tunduk berdasarkan hukum negara Republik Indonesia; ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.2", $y + 5);

   $y = Right41invo($pdf, "Peminjam memiliki kewenangan dan hak hukum penuh untuk mengadakan dan terlibat dalam transaksi-transaksi yang dimaksudkan oleh dan sesuai  Perjanjian ini dan untuk menandatangani dan melaksanakan kewajiban-kewajibannya berdasarkan Perjanjian ini; ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.3", $y + 5);

   $y = Right41invo($pdf, "Perjanjian ini apabila ditandatangani dan diserahkan akan merupakan kewajiban-kewajiban yang legal, sah dan mengikat dari Peminjam yang dapat diberlakukan menurut ketentuan Perjanjian ini; ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.4", $y + 5);

   $y = Right41invo($pdf, "Peminjam telah memperoleh seluruh izin yang disyaratkan dari, atau melaksanakan pemberitahuan yang diperlukan kepada, badan-badan pemerintah atau pihak manapun, untuk penandatanganan, penyerahan, atau pelaksanaan Perjanjian ini; dan penandatanganan, penyerahan dan pelaksanaan Perjanjian ini tidak akan merupakan pelanggaran atas perjanjian apa pun di mana Peminjam merupakan salah satu pihak di dalamnya atau yang terikat dengannya; demikian juga Peminjam tidak akan melanggar ketentuan dokumen pendiriannya, atau melanggar, bertentangan, atau mengakibatkan pelanggaran atas undang-undang, perintah, putusan pengadilan, surat keputusan, atau peraturan yang mengikat atas Peminjam atau yang mana setiap usaha, properti atau aset Peminjam tunduk padanya; ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.5", $y + 5);

   $y = Right41invo($pdf, "Tidak ada Peristiwa Wanprestasi yang telah terjadi atau akan sewajarnya diperkirakan diakibatkan pengadaan atau pelaksanaan Perjanjian ini; ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.6", $y + 5);

   $y = Right41invo($pdf, "tidak ada klaim, tindakan hukum, gugatan atau proses hukum yang menunggu putusan terhadap Peminjam, yang akibatnya dapat secara materiil dan merugikan mempengaruhi transaksi-transaksi yang dimaksudkan oleh Perjanjian ini, dan Peminjam tidak dikenakan putusan, surat perintah, perintah pengadilan atau surat keputusan yang dapat secara materiil dan merugikan mempengaruhi kemampuan Peminjam untuk melaksanakan transaksi-transaksi yang dimaksudkan oleh Perjanjian ini;  ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.7", $y + 5);

   $y = Right41invo($pdf, "tidak ada ketentuan dari undang-undang, aturan, hipotek, surat utang, kontrak, laporan pembiayaan, perjanjian atau keputusan yang ada saat ini yang mengikat atas Peminjam yang akan bertentangan dengan atau dengan cara apa pun menghalangi penandatanganan, penyerahan, atau pelaksanaan ketentuan Perjanjian ini atau dokumen atau perjanjian lain yang disebutkan dalam Perjanjian ini; dan ", $y - 5.3);

   $y = Right1invo($pdf, "7.1.8", $y + 5);

   $y = Right41invo($pdf, "usaha inti atau utama Peminjam dan pemberian surat kuasa dan kewenangan berdasarkan Perjanjian ini adalah dan pada setiap saat tetap sesuai Syariah. ", $y - 5.3);

   $y = Right1invo($pdf, "7.2", $y + 5);

   $y = Right2invo($pdf, "Peminjam juga menyatakan, menjamin dan berjanji kepada Pemodal bahwa pernyataan dan jaminan tersebut di atas adalah benar, tepat dan diulangi pada Tanggal Pencairan dan tanggal dilunasinya seluruh atau sebagian Pinjaman. ", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

   $y = Right1invo($pdf, "<b>8.	JANJI  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "<b>8.1</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Hal-hal yang Harus Dilakukan </b>", $y - 5.3);

   $y = Right2invo($pdf, "Peminjam berjanji dan menyepakati dengan Pemodal selama berlangsungnya Fasilitas dan sepanjang setiap jumlah uang adalah atau mungkin wajib dibayarkan berdasarkan Perjanjian ini bahwa Prinsipal akan: ", $y + 1);

   $y = Right1invo($pdf, "8.1.1", $y + 5);

   $y = Right41invo($pdf, "mematuhi dalam semua hal semua undang-undang di mana Peminjam tunduk padanya, yang mana kelalaian mematuhi hal tersebut akan secara materiil menghambat kemampuan Peminjam untuk melaksanakan kewajiban-kewajibannya berdasarkan Perjanjian ini; ", $y - 5.3);

   $y = Right1invo($pdf, "8.1.2", $y + 5);

   $y = Right41invo($pdf, "memastikan bahwa kewajiban pembayaran Peminjam berdasarkan Perjanjian ini menduduki peringkat dan tetap menduduki peringkat sekurang-kurangnya pari passu (setara) dengan klaim-klaim dari semua kreditur konkuren dan kreditur utama lainnya, kecuali atas kewajiban-kewajiban yang wajib diprioritaskan berdasarkan hukum yang berlaku bagi para perusahaan secara umum; ", $y - 5.3);

   $y = Right1invo($pdf, "8.1.3", $y + 5);

   $y = Right41invo($pdf, "mematuhi kewajiban-kewajibannya berdasarkan Perjanjian ini; dan ", $y - 5.3);

   $y = Right1invo($pdf, "8.1.4", $y + 5);

   $y = Right41invo($pdf, "dengan segera setelah mengetahuinya, memberitahukan Pemodal atas peristiwa wanprestasi berdasarkan kewajiban kontraktual, litigasi, penyelidikan atau proses hukum yang mungkin, jika ditetapkan secara negatif, berdampak materiil terhadap kondisi keuangan Peminjam dan kemampuan Peminjam untuk melaksanakan kewajiban-kewajiban materiilnya berdasarkan Perjanjian ini. ", $y - 5.3);

   $y = Right1invo($pdf, "<b>8.2</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Hal-hal yang Tidak Boleh Dilakukan </b>", $y - 5.3);

   $y = Right2invo($pdf, "Kecuali jika disepakati lain secara tertulis oleh Pemodal, Peminjam berjanji dan menyepakati dengan Pemodal selama berlangsungnya Fasilitas dan sepanjang setiap jumlah uang adalah atau mungkin wajib dibayarkan berdasarkan Perjanjian ini bahwa Peminjam tidak akan:", $y + 1);

   $y = Right1invo($pdf, "8.2.1", $y + 5);

   $y = Right41invo($pdf, "Memanfaatkan Fasilitas  dana talangan  untuk maksud selain modal usaha kerja; ", $y - 5.3);

   $y = Right1invo($pdf, "8.2.2", $y + 5);

   $y = Right41invo($pdf, "melakukan perubahan substansial terhadap sifat umum usaha Peminjam dari yang diselenggarakan pada tanggal Perjanjian ini; dan ", $y - 5.3);

   $y = Right1invo($pdf, "8.2.3", $y + 5);

   $y = Right41invo($pdf, "menimbulkan utang pembiayaan selain utang pembiayaan yang ditimbulkan menurut Perjanjian ini dan dokumen apa pun dan terkait dengan utang tersebut. ", $y - 5.3);

   $y = Right1invo($pdf, "<b>9.	JAMINAN SELANJUTNYA  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "Masing-masing dari Para Pihak akan melakukan semua tindakan tertentu dan menandatangani dan menyerahkan semua dokumen tertentu sebagaimana yang sewajarnya disyaratkan untuk secara penuh melaksanakan dan melakukan ketentuan Perjanjian ini.", $y + 5);

   $y = Right1invo($pdf, "<b>10. PERISTIWA WANPRESTASI</b>&nbsp;", $y + 10);


   $y = Right1invo($pdf, "10.1", $y + 5);

   $y = Right2invo($pdf, "Peristiwa Wanprestasi dianggap telah terjadi dalam setiap keadaan berikut:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "Peminjam lalai memenuhi setiap kewajiban pembayarannya berkenaan dengan Pinjaman dan/atau kewajiban pembayaran lainnya yang terkait dengan Piutang yang dikelola pada Sistem Elektronik, kecuali jika kelalaian tersebut adalah akibat kesalahan teknis atau administratif di pihak bank atau lembaga keuangan yang mengirimkan pembayaran oleh Peminjam kepada Kapital Boost, atau oleh Kapital Boost kepada Pemodal;", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "Peminjam lalai mematuhi dan melaksanakan setiap kewajibannya berdasarkan Perjanjian ini;", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "pernyataan, keterangan, jaminan atau janji yang diberikan atau dilakukan oleh Peminjam dalam Perjanjian ini terbukti tidak benar, tidak tepat atau tidak akurat atau menyesatkan dalam setiap hal materiil; ", $y - 5.3);

   $y = Right3invo($pdf, "(d)", $y + 2);

   $y = Right4invo($pdf, "Dilakukan langkah apa pun (sebagaimana sesuai) untuk penangguhan pembayaran, penundaan pembayaran utang, kepailitan, penutupan, likuidasi atau pembubaran Peminjam; ", $y - 5.3);

   $y = Right3invo($pdf, "(e)", $y + 2);

   $y = Right4invo($pdf, "penunjukan likuidator, kurator, pengurus, kurator administratif, pengelola wajib, pengawas sementara atau petugas serupa yang lain sehubungan dengan Peminjam atau setiap asetnya; atau", $y - 5.3);

   $y = Right3invo($pdf, "(f)", $y + 2);

   $y = Right4invo($pdf, "apabila terdapat unsur penggelapan, penyalahgunaan, atau penyelewengan Pinjaman oleh Peminjam.", $y - 5.3);

   $y = Right1invo($pdf, "10.2", $y + 5);

   $y = Right2invo($pdf, "Setelah terjadinya Peristiwa Wanprestasi, Pemodal berhak memberitahukan secara langsung atau melalui Kapital Boost kepada Peminjam untuk:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "menyatakan seluruh proporsi Pinjaman yang tertunggak dari Peminjam kepada Pemodal untuk segera jatuh tempo dan wajib dibayarkan tanpa permintaan, pemberitahuan selanjutnya atau formalitas hukum yang lain dalam bentuk apa pun; dan/atau", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "mewajibkan Peminjam untuk menjual Piutang dan menggunakan seluruh hasil keuntungan penjualan untuk membayar tunggakan Pinjaman (dengan ketentuan bahwa penjualan tersebut akan tunduk pada dan memenuhi prinsip Syariah); dan/atau", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "mewajibkan Peminjam untuk memberikan ganti rugi kepada Pemodal atas biaya, kerugian atau pengeluaran apa pun yang dikeluarkan sebagai akibat terjadinya Peristiwa Wanprestasi.", $y - 5.3);

   $y = Right1invo($pdf, "10.3", $y + 5);

   $y = Right2invo($pdf, "Hak Pemodal untuk mempercepat kewajiban-kewajiban Peminjam sehubungan dengan pembayaran Pinjaman dapat digunakan terlepas dari, dan tanpa bersyarat atas, tindakan atau tidak adanya tindakan dari Pemodal. Pemodal berhak untuk mengajukan atau melakukan tindakan hukum atau klaim terhadap Peminjam untuk memperoleh jumlah yang tertunggak, dan Peminjam sepakat untuk berdasarkan permintaan segera memberikan ganti rugi kepada Pemodal atas seluruh biaya, kerugian dan kewajiban sesungguhnya (termasuk biaya hukum) yang dikeluarkan atau diderita oleh Pemodal sebagai akibat Peristiwa Wanprestasi.", $y - 5.3);

   $y = Right1invo($pdf, "10.4", $y + 5);

   $y = Right2invo($pdf, "Hanya setelah penyelesaian dan pelunasan Pinjaman saja, Perjanjian ini tidak lagi berlaku selanjutnya dan tidak ada pihak yang akan memiliki klaim terhadap pihak yang lain kecuali dan sepanjang terdapat pelanggaran sebelumnya berdasarkan Perjanjian ini. Untuk maksud pengakhiran Perjanjian ini sesuai dengan Pasal 8.4 ini, Para Pihak sepakat untuk mengesampingkan ketentuan Pasal 1266 Kitab Undang-Undang Hukum Perdata sejauh diperlukan untuk memberlakukan pengakhiran Perjanjian ini tanpa harus memperoleh surat keputusan atau putusan pengadilan sebelumnya.", $y - 5.3);

   $y = Right1invo($pdf, "<b>11. HUKUM YANG BERLAKU DAN PENYELESAIAN SENGKETA</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "11.1", $y + 5);

   $y = Right2invo($pdf, "Perjanjian ini dan hak-hak serta kewajiban Para Pihak berdasarkan Perjanjian ini diatur dan diinterpretasikan serta ditafsirkan dalam semua hal sesuai dengan hukum negara Republik Indonesia.", $y - 5.3);

   $y = Right1invo($pdf, "11.2", $y + 5);

   $y = Right2invo($pdf, "Para Pihak sepakat bahwa jika terdapat perbedaan pendapat, sengketa, pertentangan atau perdebatan (“Sengketa”), yang timbul dari atau sehubungan dengan Perjanjian ini atau pelaksanaannya, termasuk tidak terbatas, sengketa apa pun mengenai keberadaan, keabsahan Perjanjian ini, atau pengakhiran hak-hak atau kewajiban Pihak manapun, Para Pihak akan berupaya selama jangka waktu 30 (tiga puluh) hari setelah diterimanya oleh salah satu pihak suatu pemberitahuan dari Pihak yang lain atas adanya Sengketa untuk menyelesaikan Sengketa tersebut dengan penyelesaian damai antara Para Pihak.", $y - 5.3);

   $y = Right1invo($pdf, "11.3", $y + 5);

   $y = Right2invo($pdf, "Apabila Para Pihak tidak dapat mencapai kesepakatan untuk menyelesaikan Sengketa dalam jangka waktu 30 (tiga puluh) hari yang disebutkan dalam Pasal 9.2 di atas maka Para Pihak dengan ini tunduk pada yurisdiksi Kepaniteraan Pengadilan Agama Jakarta Pusat. Ketundukan tersebut tidak akan mempengaruhi hak-hak Pemodal untuk melakukan proses hukum, sejauh diizinkan berdasarkan hukum negara Indonesia, dalam yurisdiksi lain di dalam atau di luar Republik Indonesia maupun proses hukum di yurisdiksi manapun sesuai dengan peraturan perundangan yang berlaku.", $y - 5.3);

   $y = Right1invo($pdf, "<b>12. LAIN-LAIN</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "<b>12.1</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Keterpisahan</b>", $y - 5.3);

   $y = Right1invo($pdf, "12.1.1", $y + 1);

   $y = Right41invo($pdf, "Apabila pada setiap saat salah satu ketentuan atau lebih dari Perjanjian ini adalah atau menjadi melanggar hukum, tidak sah atau tidak dapat diberlakukan berdasarkan hukum negara Republik Indonesia maka legalitas, keabsahan atau keberlakuan ketentuan yang lain dari Perjanjian ini maupun legalitas, keabsahan atau keberlakuan ketentuan tersebut berdasarkan hukum yurisdiksi yang lain dengan cara apa pun tidak akan terpengaruh atau terganggu berdasarkan pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut.", $y - 5.3);

   $y = Right1invo($pdf, "12.1.2", $y + 5);

   $y = Right41invo($pdf, "Apabila ketentuan apa pun dari Perjanjian ini (atau bagiannya) atau pemberlakuannya terhadap pihak atau keadaan manapun melanggar hukum, tidak sah atau tidak dapat diberlakukan dalam hal apa pun, hal tersebut harus ditafsirkan sesempit mungkin yang diperlukan untuk menjadikannya dapat diberlakukan atau sah dan sisa Perjanjian ini dan legalitas, keabsahan atau keberlakuan ketentuan tersebut terhadap para pihak atau keadaan yang lain tidak akan dengan cara lain dipengaruhi atau terganggu berdasarkan pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut dan akan dapat diberlakukan/dilaksanakan sejauh mungkin yang diizinkan berdasarkan hukum.", $y - 5.3);

   $y = Right1invo($pdf, "<b>12.2</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Pengalihan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Seluruh hak dan kewajiban dalam Perjanjian ini bersifat pribadi terhadap Para Pihak dan masing-masing Pihak dalam Perjanjian ini tidak boleh mengalihkan dan/atau memindahkan setiap hak dan kewajiban tersebut kepada pihak ketiga manapun tanpa izin tertulis sebelumnya dari Para Pihak.", $y + 1);

   $y = Right1invo($pdf, "<b>12.3</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Kerahasiaan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Masing-masing Pihak menyepakati untuk merahasiakan seluruh informasi terkait dengan Perjanjian ini dan tidak mengungkapkannya kepada siapa pun, kecuali dengan izin tertulis sebelumnya dari Para Pihak yang lain atau sebagaimana disyaratkan berdasarkan hukum atau peraturan yang berlaku.", $y + 1);

   $y = Right1invo($pdf, "<b>12.4</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Kapital Boost</b>", $y - 5.3);

   $y = Right1invo($pdf, "12.4.1", $y + 5);

   $y = Right41invo($pdf, "Sistem Elektronik ", $y - 5.3);

   $y = Right41invo($pdf, "Para Pihak menyepakati dan mengakui bahwa transaksi-transaksi dan semua hal yang ditetapkan dalam Perjanjian ini, termasuk penandatanganan Perjanjian ini, manajemen dan administrasi terkait dengan Perjanjian ini, diatur dan dilaksanakan melalui Sistem Elektronik. ", $y + 1);

   $y = Right1invo($pdf, "12.4.2", $y + 5);

   $y = Right41invo($pdf, "Dokumen Elektronik", $y - 5.3);

   $y = Right41invo($pdf, "Perjanjian ini ditandatangani melalui Sistem Elektronik dan Perjanjian ini merupakan Dokumen Elektronik yang sah dan mengikat, sesuai dengan hukum negara Indonesia. ", $y + 1);

   $y = Right1invo($pdf, "12.4.3", $y + 5);

   $y = Right41invo($pdf, "Pemberian Ganti Rugi", $y - 5.3);

   $y = Right41invo($pdf, "Masing-masing Pihak harus memberikan ganti rugi kepada Kapital Boost terhadap setiap kerugian atau kewajiban yang dikeluarkan oleh Kapital Boost (selain karena alasan kelalaian atau kesalahan disengaja Kapital Boost) yang bertindak sebagai penyedia Sistem Elektronik berkenaan dengan Perjanjian ini dan dokumen lain yang terkait dengan Sistem Elektronik tersebut. ", $y + 1);

   $y = Right1invo($pdf, "<b>12.5</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Komunikasi</b>", $y - 5.3);

   $y = Right2invo($pdf, "Setiap komunikasi yang akan dilakukan berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan secara tertulis dan, dapat dilakukan dengan surat atau surat elektronik, dalam masing-masing hal, melalui Sistem Elektronik atau Kapital Boost di 116 Changi Road, #05-00, Singapore 419718 atau di alamat tertentu yang lain sebagaimana yang dapat diberitahukan Kapital Boost kepada Para Pihak dari waktu ke waktu. Setiap komunikasi yang dilakukan antara Kapital Boost dan setiap dari Para Pihak berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan ke alamat atau alamat surat elektronik yang diberikan kepada Kapital Boost atau alamat terdaftar Kapital Boost, dan akan berlaku apabila diterima.", $y + 1);

   $y = Right1invo($pdf, "<b>12.6</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Pengesampingan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Tidak ada kelalaian untuk menggunakan, atau keterlambatan dalam pelaksanaan, di pihak Pemodal, atas hak atau upaya hukum apa pun berdasarkan Perjanjian ini yang akan dianggap sebagai pengesampingan, atau pelaksanaan tunggal atau sebagian atas hak atau upaya hukum apa pun tidak akan menghambat pelaksanaan selanjutnya atau yang lain atau pelaksanaan hak atau upaya hukum yang lain. Hak-hak dan upaya hukum berdasarkan Perjanjian ini bersifat kumulatif dan tidak terpisah dari hak-hak atau upaya hukum yang diatur berdasarkan hukum.", $y + 1);

   $y = Right1invo($pdf, "<b>12.7</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Perubahan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Tidak ada ketentuan dari Perjanjian ini yang dapat diubah, dikesampingkan, dihapuskan atau diakhiri secara lisan maupun tidak ada pelanggaran atau wanprestasi berdasarkan setiap ketentuan Perjanjian yang dapat dikesampingkan atau dihapuskan secara lisan tetapi (dalam masing-masing hal) dengan suatu instrumen tertulis yang ditandatangani oleh atau atas nama Para Pihak. Setiap amendemen atau perubahan pada Perjanjian ini harus sesuai Syariah.", $y + 1);

   $y = Right1invo($pdf, "<b>12.8</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Bahasa</b>", $y - 5.3);

   $y = Right2invo($pdf, "Perjanjian ini dibuat dalam bahasa Indonesia dan bahasa Inggris. Kedua versi tersebut sama-sama sah. Para Pihak menyepakati bahwa dalam hal terdapat ketidaksesuaian atau perbedaan penafsiran antara versi bahasa Indonesia dan versi bahasa Inggris maka yang akan berlaku adalah versi bahasa Indonesia dan versi bahasa Inggris akan diubah untuk disesuaikan dengan versi bahasa Indonesia dan untuk menjadikan bagian yang terkait dari versi bahasa Inggris tersebut sesuai dengan bagian yang terkait dari versi bahasa Indonesia.", $y + 1);

   $y = Right1invo($pdf, "<b>12.9</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Rangkap</b>", $y - 5.3);

   $y = Right2invo($pdf, "Perjanjian ini dapat ditandatangani dalam berapa pun jumlah rangkap, dan memiliki keberlakuan yang sama seolah-olah tanda tangan-tanda tangan pada rangkap tersebut terdapat pada salinan tunggal dari Perjanjian ini.", $y + 1);

   $y = Right1invo($pdf, "<b>12.10</b>", $y + 5);

   $y = Right41invo($pdf, "<b>Sesuai Syariah</b>", $y - 5.3);

   $y = Right1invo($pdf, "12.10.1", $y + 5);

   $y = Right41invo($pdf, "&nbsp;&nbsp;Perjanjian ini dimaksudkan untuk sesuai dengan Syariah. Para Pihak dengan ini menyepakati dan mengakui bahwa masing-masing hak dan kewajiban mereka berdasarkan Perjanjian ini dimaksudkan untuk, dan akan, sesuai dengan prinsip Syariah.", $y - 5.3);

   $y = Right1invo($pdf, "12.10.2", $y + 5);

   $y = Right41invo($pdf, "&nbsp;&nbsp;Meskipun terdapat hal tersebut di atas, masing-masing Pihak menyatakan kepada yang lain bahwa tidak akan mengajukan keberatan atau tuntutan terhadap yang lain berdasarkan kepatuhan pada Syariah atau pelanggaran prinsip Syariah sehubungan dengan sebagian dari setiap ketentuan Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "DEMIKIAN, Para Pihak pada Perjanjian ini telah menandatangani Perjanjian ini secara resmi pada tanggal di bagian awal Perjanjian ini.", $y + 5);
   $pdf->AddPage();
   $y = 0;
   $pdf->SetMargins(20, 15);
   $y = MLeft1($pdf, "<b>THE BORROWER / PEMINJAM</b>&nbsp; <br>SIGNED for and on behalf of / DITANDATANGANI untuk dan atas nama<br><b>$companyName</b>&nbsp;", $y + 25);

   $owSignx = 70;
   $owSigny = ($y * $pdf->getScaleFactor()) + 5;
   $owSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);

   $y = MLeft1($pdf, "<b>Name/Nama: $dirName</b>&nbsp;<br><b>Title/Jabatan: $dirTitle</b>&nbsp;<br><b>KTP No. / Nomor Kartu Tanda Penduduk: / $dirICID</b>&nbsp;", $y - 2);

   $y = MLeft1($pdf, "<b>FINANCIER / PEMODAL</b>&nbsp;", $y + 10);

   $inSignx = 70;
   $inSigny = ($y * $pdf->getScaleFactor()) + 5;
   $inSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);
		$y = MLeft1($pdf, "<b>Name/Nama: $invName</b>&nbsp;", $y - 2);

   // Yang baru

     $pdf->AddPage('P', array('210', '300'));
   $pdf->SetFont('Helvetica');



   $y = 20;

   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "_______________________ <br>", $y);

   $pdf->SetFontSize("12");
   $y = CenterR($pdf, "<b>PERJANJIAN WAKALAH NO. W$invoContractNo</b>&nbsp;", $y);

   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "<br> _______________________ ", $y - 6);
   $pdf->SetFontSize("11.5");

   $y = Right1invo($pdf, "<b>PERJANJIAN WAKALAH NO. W$invoContractNo ini (“Perjanjian”)</b>&nbsp;&nbsp; dibuat pada <b>$dateID</b>&nbsp;&nbsp;,", $y + 5);

   $y = Right1invo($pdf, "<b>ANTARA:</b>&nbsp;", $y + 5);

   $y = Right1invo($pdf, "1. <b>$companyName</b>&nbsp;<br>suatu perusahaan perseroan terbatas yang resmi didirikan, dan tunduk berdasarkan hukum negara Republik Indonesia, berdasarkan Akta Pendirian Nomor $invoAktaNoId dengan $comRegTypeInaText $comRegId dan berkedudukan di Republik Indonesia (“<b>Prinsipal</b>&nbsp;&nbsp;”);", $y + 5);

   $y = Right1invo($pdf, "2.	<b>$invName</b>&nbsp; &nbsp;, warga negara $memberCountry, pemegang $invICIDs (“<b>Agen</b>&nbsp;”); dan", $y + 5);

   $y = Right1invo($pdf, "3.	<b>Kapital Boost Pte Ltd </b>&nbsp;, suatu perusahaan perseroan terbatas yang resmi didirikan dan tunduk berdasarkan hukum negara Singapura, berkedudukan di Singapura, dalam kedudukannya sebagai agen substitusi (“<b>Agen Substitusi</b>&nbsp;&nbsp;”).", $y + 5);

   $y = Right1invo($pdf, "Prinsipal, Agen dan Agen Substitusi disebut secara bersama-sama sebagai “Para Pihak” dan secara masing-masing sebagai “Pihak”.", $y + 5);

   $y = Right1invo($pdf, "<b>BAHWA:</b>&nbsp;", $y + 5);

   $y = Right1invo($pdf, "A.", $y + 5);

   $y = Right2invo($pdf, "Prinsipal berkeinginan menunjuk Agen sebagai agen Prinsipal berkenaan dengan Layanan Pengelolaan Piutang (sebagaimana didefinisikan di bawah ini) berdasarkan prinsip Syariah atas Wakalah bil Ujroh, dengan syarat dan ketentuan Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "B.", $y + 5);

   $y = Right2invo($pdf, "Agen berkeinginan menunjuk Agen Substitusi sebagai agen substitusi Prinsipal untuk menggunakan seluruh atau sebagian kewenangan yang diberikan oleh Prinsipal kepada Agen berkenaan dengan Layanan Pengelolaan Piutang (sebagaimana didefinisikan di bawah ini) berdasarkan prinsip Syariah atas Wakalah bil Ujroh, dengan syarat dan ketentuan Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "<b>DENGAN INI DISEPAKATI </b>&nbsp; sebagai berikut:", $y + 10);
   $y = Right1invo($pdf, "<b>1.	DEFINISI DAN PENAFSIRAN</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "1.1", $y);

   $y = Right2invo($pdf, "Dalam Perjanjian ini, istilah-istilah berikut ini memiliki pengertian sebagai berikut:", $y - 5.3);

   $tbl162 = "<table cellspacing=\"10\" cellpadding=\"0\" border=\"0\" width=\"360px\">
  <tr>
  <td align=\"justify\" width=\"80px\"><b>“Biaya Keagenan”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 2.1. </td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Usaha”</b>&nbsp;</td>
  <td align=\"justify\">: adalah kegiatan usaha Prinsipal yang tidak bertentangan dengan prinsip Syariah. </td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Hari Kerja”</b>&nbsp;</td>
  <td align=\"justify\">: adalah hari (selain Sabtu, Ahad atau hari libur nasional) di mana bank-bank di Indonesia biasanya buka untuk kegiatan usaha.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Pihak Lawan Transaksi”</b>&nbsp;</td>
  <td align=\"justify\">: adalah suatu pihak pada Dokumen Dasar selain Prinsipal.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Sengketa”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 9.2.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Dokumen Elektronik”</b>&nbsp;</td>
  <td align=\"justify\">: adalah informasi elektronik yang dibuat, diteruskan, dikirimkan, diterima, atau disimpan dalam bentuk analog, digital, elektromagnetik, optik, atau sejenisnya, yang dapat dilihat, dapat ditampilkan dan/atau dapat didengar melalui komputer atau Sistem Elektronik, termasuk tetapi tidak terbatas pada tulisan, bunyi, gambar, peta, rancangan, foto atau sejenisnya, huruf, tanda, gambar, kode akses, simbol atau perforasi yang memiliki pengertian atau definisi tertentu atau dapat dimengerti oleh para pihak yang memenuhi syarat untuk memahaminya.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Sistem Elektronik”</b>&nbsp;</td>
  <td align=\"justify\">: adalah www.kapitalboost.com, suatu platform urun dana bersama (peer to peer crowdfunding) yang dikelola dan dilaksanakan oleh Agen Substitusi.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Peristiwa Wanprestasi”</b>&nbsp;</td>
  <td align=\"justify\">: adalah peristiwa atau keadaan sebagaimana ditetapkan dalam Pasal 8.1.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Tanggal Pembayaran Imbalan”</b>&nbsp;</td>
  <td align=\"justify\">: adalah $invoRepaymentDate.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Gharamah”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 4.6.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rekening Eskro Kapital Boost”</b>&nbsp;</td>
  <td align=\"justify\">: adalah rekening bank yang dikelola oleh Kapital Boost untuk maksud Perjanjian ini.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rekening Virtual Kapital Boost”</b>&nbsp;</td>
  <td align=\"justify\">: adalah rekening virtual, yang perinciannya diatur dalam Sistem Elektronik, yang diberikan oleh Kapital Boost kepada Pemodal semata-mata untuk tujuan (i) pencairan Fasilitas, dan (ii) menerima pelunasan Pinjaman.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Tanggal Jatuh Tempo”</b>&nbsp;</td>
  <td align=\"justify\">: adalah $invoMaturityDate, tanggal di mana Piutang menjadi jatuh tempo dan wajib dibayarkan kepada Prinsipal.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Piutang”</b>&nbsp;</td>
  <td align=\"justify\">: adalah piutang yang wajib dibayarkan kepada Prinsipal yang timbul berdasarkan Dokumen Pokok sehubungan dengan barang dan/atau jasa yang diberikan oleh Prinsipal, yang dapat diterima oleh Agen.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Jumlah Piutang”</b>&nbsp;</td>
  <td align=\"justify\">: adalah Rp$invoAssetCost atau SGD$invoAssetCostSGD yang merupakan jumlah pokok Piutang yang wajib dibayarkan kepada Prinsipal pada Tanggal Jatuh Tempo</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Jangka Waktu Pengelolaan Piutang”</b>&nbsp;</td>
  <td align=\"justify\">: adalah jangka waktu yang dimulai dari tanggal Perjanjian ini sampai Tanggal Jatuh Tempo atau Tanggal Pembayaran Imbalan.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Layanan Pengelolaan Piutang”</b>&nbsp;</td>
  <td align=\"justify\">: adalah proses pengelolaan Piutang, termasuk penagihan pembayaran dari Pihak Lawan Transaksi yang terkait dengan Piutang selama Jangka Waktu Pengelolaan Piutang.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rp” atau “Rupiah”</b>&nbsp;</td>
  <td align=\"justify\">: adalah mata uang sah Indonesia.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“SGD” atau “Dollar Singapura”</b>&nbsp;</td>
  <td align=\"justify\">: adalah mata uang sah Singapura.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Ta’widh”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 4.3.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Biaya Keagenan Substitusi”</b>&nbsp;</td>
  <td align=\"justify\">: memiliki pengertian yang terdapat dalam Pasal 2.2.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Dokumen Pokok”</b>&nbsp;</td>
  <td align=\"justify\">: $invoDocsId</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetX(107);
   $pdf->SetFontSize(11.5);
   $pdf->writeHTML($tbl162);
   $pdf->SetFontSize(11.5);
   $y = $pdf->GetY();

   $y = Right1invo($pdf, "1.2", $y);

   $y = Right2invo($pdf, "Kecuali jika terdapat petunjuk yang bertentangan, rujukan apa pun dalam Perjanjian ini:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "rujukan pada dokumen apa pun (termasuk Perjanjian ini) mencakup rujukan pada dokumen tersebut yang diubah, digabungkan, ditambah, dinovasi atau digantikan; rujukan pada suatu perjanjian mencakup janji, pernyataan, akta, perjanjian atau putusan yang berlaku secara sah, skema atau kesepahaman baik tertulis atau tidak;", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "“termasuk” ditafsirkan sebagai “termasuk tidak terbatas” (dan ungkapan sejenis akan ditafsirkan serupa);", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "rujukan pada “pihak” mencakup rujukan pada perseorangan, firma, perusahaan, koperasi, korporasi, pemerintah, perusahaan perseroan terbatas, negara atau instansi negara atau asosiasi, peramanatan (trust) atau wali amanat, dana pensiun, usaha patungan, konsorsium atau persekutuan (baik memiliki kedudukan hukum yang terpisah atau tidak);", $y - 5.3);

   $y = Right3invo($pdf, "(d)", $y + 2);

   $y = Right4invo($pdf, "rujukan pada “tertulis” atau “secara tertulis” mencakup metode penggandaan kata dalam bentuk yang dapat dibaca dan tidak bersifat sementara;", $y - 5.3);

   $y = Right3invo($pdf, "(e)", $y + 2);

   $y = Right4invo($pdf, "kecuali jika terdapat maksud yang bertentangan yang secara tegas ditunjukkan berdasarkan konteks, kata atau ungkapan yang menunjukkan suatu jenis kelamin mencakup jenis kelamin yang lain, dan kata bentuk tunggal mencakup kata bentuk jamak dan sebaliknya; dan", $y - 5.3);

   $y = Right3invo($pdf, "(f)", $y + 2);

   $y = Right4invo($pdf, "setiap rujukan, secara tegas atau tersirat, terhadap undang-undang atau ketentuan berdasarkan undang-undang akan ditafsirkan sebagai rujukan terhadap undang-undang atau ketentuan tersebut sebagaimana masing-masing diubah atau diberlakukan kembali atau sebagaimana penerapannya diubah dari waktu ke waktu dengan ketentuan lain (baik sebelum atau sesudah tanggal Perjanjian ini) dan akan mencakup undang-undang atau ketentuan yang merupakan pemberlakuannya kembali (baik dengan atau tanpa perubahan) dan putusan, peraturan, instrumen atau perundangan turunan lainnya berdasarkan undang-undang atau ketentuan berdasarkan undang-undang yang terkait.", $y - 5.3);

   $y = Right1invo($pdf, "1.3", $y + 5);

   $y = Right2invo($pdf, "Judul Pasal dan Lampiran hanya untuk memudahkan saja dan tidak digunakan dalam penafsirannya.", $y - 5.3);

   $y = Right1invo($pdf, "1.4", $y + 5);

   $y = Right2invo($pdf, "Rujukan pada Perjanjian ini merupakan rujukan pada Perjanjian ini dan aneks, lampiran dan ekshibit.", $y - 5.3);

   $y = Right1invo($pdf, "1.5", $y + 5);

   $y = Right2invo($pdf, "Rujukan pada Perjanjian ini pada Pasal dan Lampiran adalah pada pasal dan lampiran dalam Perjanjian ini (kecuali jika disyaratkan lain berdasarkan konteks). Pembukaan dan Lampiran pada Perjanjian ini dianggap merupakan bagian dari Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "1.6", $y + 5);

   $y = Right2invo($pdf, "Apabila istilah apa pun didefinisikan dalam suatu pasal tertentu selain Pasal 1.1 maka istilah tersebut akan memiliki pengertian yang ditetapkan dalam pasal tersebut di mana saja digunakan dalam Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "1.7", $y + 5);

   $y = Right2invo($pdf, "Apabila jumlah hari akan dihitung dari suatu hari tertentu maka jumlah tersebut akan dihitung di luar hari tertentu tersebut dan dimulai pada hari berikutnya. Jika hari terakhir dalam jumlah yang dihitung tersebut jatuh pada hari yang bukan Hari Kerja maka hari terakhir tersebut akan ditetapkan pada hari pengganti berikutnya yang merupakan Hari Kerja.", $y - 5.3);

   $y = Right1invo($pdf, "1.8", $y + 5);

   $y = Right2invo($pdf, "Setiap rujukan pada hari (selain rujukan pada Hari Kerja), bulan atau tahun adalah rujukan pada masing-masing hari kalender, bulan kalender atau tahun kalender.", $y - 5.3);

   $y = Right1invo($pdf, "1.9", $y + 5);

   $y = Right2invo($pdf, "Setiap istilah yang merujuk pada konsep atau proses hukum Indonesia dianggap mencakup rujukan pada konsep atau proses yang setara atau sama dalam yurisdiksi lainnya di mana Perjanjian ini dapat berlaku atau pada hukum di mana salah satu Pihak dapat atau menjadi tunduk padanya.", $y - 5.3);

   $y = Right1invo($pdf, "1.10", $y + 5);

   $y = Right2invo($pdf, "Ketentuan Perjanjian ini telah dinegosiasikan, pihak manapun yang menafsirkan Perjanjian ini tidak akan menggunakan aturan yang menjelaskan bahwa dalam hal terdapat ambiguitas atau ketaksaan maka suatu kontrak akan ditafsirkan bertentangan terhadap Pihak yang bertanggung jawab atas penyusunan kontrak tersebut.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

    $y = Right1invo($pdf, "<b>2. PENUNJUKAN AGEN DAN AGEN SUBSTITUSI</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "2.1", $y + 5);

   $y = Right2invo($pdf, "Sebagai timbal balik untuk Agen yang menyetujui untuk bertindak, dan pembayaran oleh Prinsipal atas imbalan sejumlah SGD$percent_return (“Biaya Keagenan”), Prinsipal dengan tidak dapat ditarik kembali:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "menunjuk Agen untuk bertindak sebagai agen dari Prinsipal sehubungan dengan Layanan Pengelolaan Piutang; ", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "memberikan surat kuasa dan memberikan kewenangan kepada Agen untuk menandatangani dokumen dan menerima pembayaran atas nama Prinsipal sebagai bagian dari Layanan Pengelolaan Piutang, dan untuk melakukan tindakan tertentu atas nama Prinsipal dan untuk menggunakan hak-hak, upaya hukum, kewenangan dan diskresi tertentu sebagaimana yang diberikan kepada Prinsipal berdasarkan Dokumen Pokok, beserta hak-hak, upaya hukum, kewenangan dan diskresi tertentu sebagaimana yang sewajarnya merupakan tambahan terhadap Dokumen Pokok tersebut; dan", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "menyepakati penunjukan Agen Substitusi oleh Agen berdasarkan ketentuan Pasal 2.2 di bawah ini.", $y - 5.3);

   $y = Right1invo($pdf, "2.2", $y + 5);

   $y = Right2invo($pdf, "Sebagai timbal balik untuk Agen Substitusi yang menyetujui untuk bertindak, dan pembayaran oleh Agen atas imbalan sejumlah SGD$sub_agency_fee (“Biaya Keagenan Substitusi”), Agen dengan tidak dapat ditarik kembali:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "menunjuk Agen Substitusi sebagai agen substitusi dari Agen sehubungan dengan Layanan Pengelolaan Piutang; ", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "memberikan surat kuasa dan memberikan kewenangan kepada Agen Substitusi untuk menandatangani dokumen dan menerima pembayaran atas nama Agen (dan dengan demikian, atas nama Prinsipal) sebagai bagian dari Layanan Pengelolaan Piutang, dan untuk melakukan tindakan tertentu yang diperlukan atas nama Agen (dan dengan demikian, atas nama Prinsipal) dan untuk menggunakan hak-hak, upaya hukum, kewenangan dan diskresi tertentu sebagaimana yang diberikan kepada Prinsipal berdasarkan Dokumen Pokok, beserta hak-hak, upaya hukum, kewenangan dan diskresi tertentu sebagaimana yang sewajarnya merupakan tambahan terhadap Dokumen Pokok tersebut; dan", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "memberikan surat kuasa dan memberikan kewenangan kepada Agen Substitusi untuk menerima pembayaran Biaya Keagenan atas nama Agen dan untuk menggunakan sebagian Biaya Keagenan yang diterima oleh Agen Substitusi untuk pembayaran Biaya Keagenan Substitusi.", $y - 5.3);

   $y = Right1invo($pdf, "2.3", $y + 5);

   $y = Right2invo($pdf, "Penunjukan Agen sebagai agen menurut Pasal 2.1 di atas, dan Agen Substitusi sebagai agen substitusi menurut Pasal 2.2 di atas, akan berakhir dengan sendirinya pada akhir Jangka Waktu Pengelolaan Piutang, dengan ketentuan bahwa Biaya Keagenan dan Biaya Keagenan Substitusi telah resmi diterima secara penuh (tanpa pengurangan) oleh masing-masing Agen dan Agen Substitusi. Tanpa mengurangi hak-hak Agen dan/atau Agen Substitusi berdasarkan Perjanjian ini, Agen dan/atau Agen Substitusi, dengan masing-masing diskresinya sendiri, dapat mengesampingkan sebagian imbalannya masing-masing yang wajib dibayarkan kepada mereka menurut Pasal 2 ini.", $y - 5.3);

   $y = Right1invo($pdf, "2.4", $y + 5);

   $y = Right2invo($pdf, "Semua surat kuasa dan otorisasi yang diberikan dalam Perjanjian ini tidak akan diakhiri dengan alasan apa pun, termasuk, tidak terbatas, setiap alasan yang terdapat dalam Pasal 1813, Pasal 1814 dan Pasal 1816 Kitab Undang-Undang Hukum Perdata (kecuali atas penolakan oleh Agen (berkenaan dengan surat kuasa dan otorisasi yang diberikan berdasarkan Pasal 2.2)).", $y - 5.3);

   $y = Right1invo($pdf, "2.5", $y + 5);

   $y = Right2invo($pdf, "Meskipun terdapat pemberian surat kuasa dan otorisasi berdasarkan Perjanjian ini, Prinsipal akan tetap bertanggung jawab atas penagihan Piutang dan memiliki kewajiban untuk memastikan bahwa Piutang dilunasi oleh Pihak Lawan Transaksi. Tidak ada dalam Perjanjian ini yang akan ditafsirkan untuk mengurangi kewajiban Prinsipal berdasarkan Dokumen Pokok.", $y - 5.3);

   $y = Right1invo($pdf, "<b>3.	PEMBAYARAN BIAYA KEAGENAN DAN BIAYA KEAGENAN SUBSTITUSI  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "3.1", $y + 5);

   $y = Right2invo($pdf, "Pada Tanggal Pembayaran Imbalan, Prinsipal harus membayar Biaya Keagenan secara penuh ke Rekening Eskrow Kapital Boost.", $y - 5.3);

   $y = Right1invo($pdf, "3.2", $y + 5);

   $y = Right2invo($pdf, "Pada Tanggal Pembayaran Imbalan, Agen harus membayar Biaya Keagenan Substitusi secara penuh kepada Agen Substitusi.", $y - 5.3);

   $y = Right1invo($pdf, "3.3", $y + 5);

   $y = Right2invo($pdf, "Masing-masing Biaya Keagenan dan Biaya Keagenan Substitusi tidak termasuk pajak pertambahan nilai atau pajak serupa, dan Prinsipal (sehubungan dengan Biaya Keagenan) dan Agen (sehubungan dengan Biaya Keagenan Substitusi) harus melakukan pembayaran yang terkait beserta pajak pertambahan nilai atau pajak serupa yang berlaku.", $y - 5.3);

   $y = Right1invo($pdf, "3.4", $y + 5);

   $y = Right2invo($pdf, "Setiap jumlah yang wajib dibayarkan sesuai dengan Perjanjian ini harus dibayarkan tanpa pemotongan, gugatan balik, perjumpaan utang atau penahanan dalam bentuk apa pun.", $y - 5.3);

   $y = Right1invo($pdf, "<b>4. BIAYA KETERLAMBATAN PEMBAYARAN  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "4.1", $y + 5);

   $y = Right2invo($pdf, "Apabila jumlah yang jatuh tempo dan wajib dibayarkan oleh Prinsipal atau Agen berdasarkan atau sehubungan dengan Perjanjian ini belum dilunasi pada Tanggal Pembayaran Imbalan (“jumlah yang belum dibayarkan”), masing-masing dari Prinsipal atau Agen berjanji untuk membayar biaya keterlambatan pembayaran termasuk ta’widh dan gharamah (dihitung di bawah ini) kepada Agen atau Agen Substitusi pada setiap hari di mana jumlah yang tertunggak belum dibayarkan.", $y - 5.3);

   $y = Right1invo($pdf, "4.2", $y + 5);

   $y = Right2invo($pdf, "Keseluruhan biaya keterlambatan pembayaran akan dipatok langsung pada laba rata-rata disetahunkan dari Perjanjian ini (akan ditentukan oleh Agen Substitusi), yang dihitung setiap hari sebagaimana di bawah ini, dengan ketentuan bahwa akumulasi biaya keterlambatan pembayaran tidak akan melebihi 100% (seratus persen) (sesuai keadaan) dari Biaya Keagenan atau Biaya Keagenan Substitusi:", $y - 5.3);

   $y = Right2invo($pdf, "jumlah yang belum dibayarkan X laba rata-rata disetahunkan X Jumlah hari lewat jatuh tempo / 365 hari", $y + 5);

   $y = Right1invo($pdf, "4.3", $y + 5);

   $y = Right2invo($pdf, "Agen atau Agen Substitusi (sesuai keadaan) dapat menahan hanya bagian tertentu dari jumlah keterlambatan pembayaran yang dibayarkan kepadanya, sebagaimana diperlukan untuk memberikan penggantian (sesuai keadaan) kepada Agen atau Agen Substitusi atas setiap biaya sesungguhnya, dengan tunduk pada dan menurut pedoman para penasihat Syariahnya sendiri, dan hingga jumlah keseluruhan maksimum 1% (satu persen) dari jumlah yang belum dibayarkan (“Ta’widh”).", $y - 5.3);

   $y = Right1invo($pdf, "4.4", $y + 5);

   $y = Right2invo($pdf, "Ta’widh sehubungan dengan jumlah yang belum dibayarkan, yang merupakan bagian dari biaya keterlambatan pembayaran, dapat bertambah setiap hari dan dihitung menurut rumus berikut:", $y - 5.3);

   $y = Right2invo($pdf, "biaya sesungguhnya yang dikeluarkan atau jumlah yang belum dibayarkan X 1% X Jumlah hari lewat jatuh tempo / 365 hari", $y + 5);

   $y = Right1invo($pdf, "4.5", $y + 5);

   $y = Right2invo($pdf, "Jumlah Ta’widh tidak akan dilipatgandakan.", $y - 5.3);

   $y = Right1invo($pdf, "4.6", $y + 5);

   $y = Right2invo($pdf, "Masing-masing dari Agen atau Agen Substitusi dengan ini menyepakati, berjanji dan menyanggupi untuk memastikan bahwa sisa biaya keterlambatan (“Gharamah”), jika ada, akan dibayarkan kepada badan-badan amal yang dapat dipilih oleh (sesuai keadaan) Prinsipal atau Agen dengan berkonsultasi dengan para penasihat Syariahnya.", $y - 5.3);


   $y = Right1invo($pdf, "<b>5.	PERNYATAAN DAN JAMINAN</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "5.1", $y + 5);

   $y = Right2invo($pdf, "Prinsipal menyatakan dan menjamin kepada Agen dan Agen Substitusi bahwa:", $y - 5.3);

   $y = Right1invo($pdf, "5.1.1", $y + 5);

   $y = Right41invo($pdf, "Prinsipal resmi didirikan dan tunduk berdasarkan hukum negara Republik Indonesia;", $y - 5.3);

   $y = Right1invo($pdf, "5.1.2", $y + 5);

   $y = Right41invo($pdf, "Prinsipal memiliki kewenangan penuh dan hak hukum untuk mengadakan dan terlibat dalam transaksi-transaksi yang dimaksudkan oleh Perjanjian ini dan untuk menandatangani dan melaksanakan kewajiban-kewajibannya berdasarkan Perjanjian ini; ", $y - 5.3);

   $y = Right1invo($pdf, "5.1.3", $y + 5);

   $y = Right41invo($pdf, "Perjanjian ini apabila ditandatangani dan diserahkan akan merupakan kewajiban-kewajiban yang legal, sah dan mengikat dari Prinsipal yang dapat diberlakukan menurut ketentuan Perjanjian ini;", $y - 5.3);

   $y = Right1invo($pdf, "5.1.4", $y + 5);

   $y = Right41invo($pdf, "Prinsipal telah memperoleh seluruh izin yang disyaratkan dari, atau melaksanakan pemberitahuan yang diperlukan kepada, badan-badan pemerintah atau pihak manapun, untuk penandatanganan, penyerahan, atau pelaksanaan Perjanjian ini; dan penandatanganan, penyerahan dan pelaksanaan Perjanjian ini tidak akan merupakan pelanggaran atas perjanjian apa pun di mana Prinsipal merupakan salah satu pihak di dalamnya atau yang terikat dengannya; demikian juga Prinsipal tidak akan melanggar ketentuan dokumen pendiriannya, atau melanggar, bertentangan, atau mengakibatkan pelanggaran atas undang-undang, perintah, putusan pengadilan, surat keputusan, atau peraturan yang mengikat atas Prinsipal atau yang mana setiap usaha, properti atau aset Prinsipal tunduk padanya;", $y - 5.3);

   $y = Right1invo($pdf, "5.1.5", $y + 5);

   $y = Right41invo($pdf, "Tidak ada Peristiwa Wanprestasi yang telah terjadi atau akan sewajarnya diperkirakan diakibatkan pengadaan atau pelaksanaan Perjanjian ini;", $y - 5.3);

   $y = Right1invo($pdf, "5.1.6", $y + 5);

   $y = Right41invo($pdf, "tidak ada klaim, tindakan hukum, gugatan atau proses hukum yang menunggu putusan terhadap Prinsipal, yang akibatnya dapat secara materiil dan merugikan mempengaruhi transaksi-transaksi yang dimaksudkan oleh Perjanjian ini, dan Prinsipal tidak dikenakan putusan, surat perintah, perintah pengadilan atau surat keputusan yang dapat secara materiil dan merugikan mempengaruhi kemampuan Prinsipal untuk melaksanakan transaksi-transaksi yang dimaksudkan oleh Perjanjian ini; ", $y - 5.3);

   $y = Right1invo($pdf, "5.1.7", $y + 5);

   $y = Right41invo($pdf, "tidak ada ketentuan dari undang-undang, aturan, hipotek, surat utang, kontrak, laporan pembiayaan, perjanjian atau keputusan yang ada saat ini yang mengikat atas Prinsipal yang akan bertentangan dengan atau dengan cara apa pun menghalangi penandatanganan, penyerahan, atau pelaksanaan ketentuan Perjanjian ini atau dokumen atau perjanjian lain yang disebutkan dalam Perjanjian ini; dan", $y - 5.3);

   $y = Right1invo($pdf, "5.1.8", $y + 5);

   $y = Right41invo($pdf, "usaha inti atau utama Prinsipal dan pemberian surat kuasa dan kewenangan berdasarkan Perjanjian ini adalah dan pada setiap saat tetap sesuai Syariah.", $y - 5.3);

   $y = Right1invo($pdf, "5.2", $y + 5);

   $y = Right2invo($pdf, "Prinsipal juga menyatakan, menjamin dan berjanji kepada Agen dan Agen Substitusi bahwa pernyataan dan jaminan tersebut di atas adalah benar, tepat dan diulangi pada Tanggal Jatuh Tempo, Tanggal Pembayaran Imbalan, dan tanggal dilunasinya seluruh atau sebagian Biaya Keagenan dan Biaya Keagenan Substitusi.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

   $y = Right1invo($pdf, "<b>6.	JANJI  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "<b>6.1</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Hal-hal yang Harus Dilakukan </b>", $y - 5.3);

   $y = Right2invo($pdf, "Prinsipal berjanji dan menyepakati dengan Agen dan Agen Substitusi sepanjang setiap jumlah uang adalah atau mungkin wajib dibayarkan berdasarkan Perjanjian ini bahwa Prinsipal akan:", $y + 1);

   $y = Right1invo($pdf, "6.1.1", $y + 5);

   $y = Right41invo($pdf, "mematuhi dalam semua hal semua undang-undang di mana Prinsipal tunduk padanya, yang mana kelalaian mematuhi hal tersebut akan secara materiil menghambat kemampuan Prinsipal untuk melaksanakan kewajiban-kewajibannya berdasarkan Perjanjian ini;", $y - 5.3);

   $y = Right1invo($pdf, "6.1.2", $y + 5);

   $y = Right41invo($pdf, "memastikan bahwa kewajiban pembayaran Prinsipal berdasarkan Perjanjian ini menduduki peringkat dan tetap menduduki peringkat sekurang-kurangnya pari passu (setara) dengan klaim-klaim dari semua kreditur konkuren dan kreditur utama lainnya, kecuali atas kewajiban-kewajiban yang wajib diprioritaskan berdasarkan hukum yang berlaku bagi para perusahaan secara umum; ", $y - 5.3);

   $y = Right1invo($pdf, "6.1.3", $y + 5);

   $y = Right41invo($pdf, "mematuhi kewajiban-kewajibannya berdasarkan Perjanjian ini; dan ", $y - 5.3);

   $y = Right1invo($pdf, "6.1.4", $y + 5);

   $y = Right41invo($pdf, "dengan segera setelah mengetahuinya, memberitahukan Agen dan Agen Substitusi atas peristiwa wanprestasi berdasarkan kewajiban kontraktual, litigasi, penyelidikan atau proses hukum yang mungkin, jika ditetapkan secara negatif, berdampak materiil terhadap kondisi keuangan Prinsipal  dan kemampuan Peminjam untuk melaksanakan kewajiban-kewajiban materiilnya berdasarkan Perjanjian ini. ", $y - 5.3);

   $y = Right1invo($pdf, "<b>6.2</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Hal-hal yang Tidak Boleh Dilakukan </b>", $y - 5.3);

   $y = Right2invo($pdf, "Kecuali jika disepakati lain secara tertulis oleh Agen, Prinsipal  berjanji dan menyepakati dengan Agen selama berlangsungnya Fasilitas dan sepanjang setiap jumlah uang adalah atau mungkin wajib dibayarkan berdasarkan Perjanjian ini bahwa Prinsipal tidak akan:", $y + 1);

   $y = Right1invo($pdf, "6.2.1", $y + 5);

   $y = Right41invo($pdf, "melakukan perubahan substansial terhadap sifat umum Usaha Prinsipal dari yang diselenggarakan pada tanggal Perjanjian ini; dan", $y - 5.3);

   $y = Right1invo($pdf, "6.2.2", $y + 5);

   $y = Right41invo($pdf, "menimbulkan utang pembiayaan selain utang pembiayaan yang ditimbulkan menurut Perjanjian ini dan dokumen apa pun dan terkait dengan utang tersebut. ", $y - 5.3);

   $y = Right1invo($pdf, "<b>7.	JAMINAN SELANJUTNYA  </b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "Masing-masing dari Para Pihak akan melakukan semua tindakan tertentu dan menandatangani dan menyerahkan semua dokumen tertentu sebagaimana yang sewajarnya disyaratkan untuk secara penuh melaksanakan dan melakukan ketentuan Perjanjian ini.", $y + 1);

   $y = Right1invo($pdf, "<b>8. PERISTIWA WANPRESTASI</b>&nbsp;", $y + 10);


   $y = Right1invo($pdf, "8.1", $y + 5);

   $y = Right2invo($pdf, "Peristiwa Wanprestasi dianggap telah terjadi dalam setiap keadaan berikut:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "Prinsipal lalai memenuhi setiap kewajiban pembayarannya berkenaan dengan Biaya Keagenan dan/atau kewajiban pembayaran lainnya yang terkait dengan Piutang yang dikelola pada Sistem Elektronik, kecuali jika kelalaian tersebut adalah akibat kesalahan teknis atau administratif di pihak bank atau lembaga keuangan yang mengirimkan pembayaran oleh Prinsipal kepada Kapital Boost, atau oleh Kapital Boost kepada Agen Substitusi;", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "Prinsipal lalai mematuhi dan melaksanakan setiap kewajibannya berdasarkan Perjanjian ini;", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "pernyataan, keterangan, jaminan atau janji yang diberikan atau dilakukan oleh Prinsipal dalam Perjanjian ini terbukti tidak benar, tidak tepat atau tidak akurat atau menyesatkan dalam setiap hal materiil; ", $y - 5.3);

   $y = Right3invo($pdf, "(d)", $y + 2);

   $y = Right4invo($pdf, "Dilakukan langkah apa pun (sebagaimana sesuai) untuk penangguhan pembayaran, penundaan pembayaran utang, kepailitan, penutupan, likuidasi atau pembubaran Prinsipal; ", $y - 5.3);

   $y = Right3invo($pdf, "(e)", $y + 2);

   $y = Right4invo($pdf, "penunjukan likuidator, kurator, pengurus, kurator administratif, pengelola wajib, pengawas sementara atau petugas serupa yang lain sehubungan dengan Prinsipal atau setiap asetnya; atau", $y - 5.3);

   $y = Right3invo($pdf, "(f)", $y + 2);

   $y = Right4invo($pdf, "apabila terdapat peristiwa wanprestasi atau wanprestasi (bagaimanapun penjelasannya) berdasarkan perjanjian-perjanjian yang lain di mana Prinsipal dan Agen, atau Prinsipal dan Agen Substitusi, adalah para pihak.", $y - 5.3);

   $y = Right1invo($pdf, "8.2", $y + 5);

   $y = Right2invo($pdf, "Setelah terjadinya Peristiwa Wanprestasi, Agen berhak dengan pemberitahuan kepada Prinsipal untuk:", $y - 5.3);

   $y = Right3invo($pdf, "(a)", $y + 2);

   $y = Right4invo($pdf, "menyatakan Biaya Keagenan untuk segera jatuh tempo dan wajib dibayarkan tanpa permintaan, pemberitahuan selanjutnya atau formalitas hukum yang lain dalam bentuk apa pun; dan/atau", $y - 5.3);

   $y = Right3invo($pdf, "(b)", $y + 2);

   $y = Right4invo($pdf, "mewajibkan Prinsipal untuk menjual Piutang dan menggunakan seluruh hasil keuntungan penjualan untuk membayar Biaya Keagenan (dengan ketentuan bahwa penjualan tersebut akan tunduk pada dan memenuhi prinsip Syariah); dan/atau", $y - 5.3);

   $y = Right3invo($pdf, "(c)", $y + 2);

   $y = Right4invo($pdf, "mewajibkan Prinsipal untuk memberikan ganti rugi kepada Agen dan/atau Agen Substitusi atas biaya, kerugian atau pengeluaran apa pun yang dikeluarkan sebagai akibat terjadinya Peristiwa Wanprestasi.", $y - 5.3);

   $y = Right1invo($pdf, "8.3", $y + 5);

   $y = Right2invo($pdf, "Hak Agen untuk mempercepat kewajiban-kewajiban Prinsipal sehubungan dengan pembayaran Biaya Keagenan dapat digunakan terlepas dari, dan tanpa bersyarat atas, tindakan atau tidak adanya tindakan dari Agen. Agen dan/atau Agen Substitusi berhak untuk mengajukan atau melakukan tindakan hukum atau klaim terhadap Prinsipal sehubungan dengan pembayaran Biaya Keagenan, dan Prinsipal sepakat untuk berdasarkan permintaan segera memberikan ganti rugi kepada Agen dan/atau Agen Substitusi atas seluruh biaya, kerugian dan kewajiban sesungguhnya (termasuk biaya hukum) yang dikeluarkan atau diderita oleh Agen dan/atau Agen Substitusi sebagai akibat Peristiwa Wanprestasi.", $y - 5.3);

   $y = Right1invo($pdf, "8.4", $y + 5);

   $y = Right2invo($pdf, "Hanya setelah penyelesaian dan pelunasan Biaya Keagenan dan Biaya Keagenan Substitusi saja, Perjanjian ini tidak lagi berlaku selanjutnya dan tidak ada pihak yang akan memiliki klaim terhadap pihak yang lain kecuali dan sepanjang terdapat pelanggaran sebelumnya berdasarkan Perjanjian ini. Untuk maksud pengakhiran Perjanjian ini sesuai dengan Pasal 8.4 ini, Para Pihak sepakat untuk mengesampingkan ketentuan Pasal 1266 Kitab Undang-Undang Hukum Perdata sejauh diperlukan untuk memberlakukan pengakhiran Perjanjian ini tanpa harus memperoleh surat keputusan atau putusan pengadilan sebelumnya.", $y - 5.3);

   $y = Right1invo($pdf, "<b>9. HUKUM YANG BERLAKU DAN PENYELESAIAN SENGKETA</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "9.1", $y + 5);

   $y = Right2invo($pdf, "Perjanjian ini dan hak-hak serta kewajiban Para Pihak berdasarkan Perjanjian ini diatur dan diinterpretasikan serta ditafsirkan dalam semua hal sesuai dengan hukum negara Republik Indonesia.", $y - 5.3);

   $y = Right1invo($pdf, "9.2", $y + 5);

   $y = Right2invo($pdf, "Para Pihak sepakat bahwa jika terdapat perbedaan pendapat, sengketa, pertentangan atau perdebatan (“Sengketa”), yang timbul dari atau sehubungan dengan Perjanjian ini atau pelaksanaannya, termasuk tidak terbatas, sengketa apa pun mengenai keberadaan, keabsahan Perjanjian ini, atau pengakhiran hak-hak atau kewajiban Pihak manapun, Para Pihak akan berupaya selama jangka waktu 30 (tiga puluh) hari setelah diterimanya oleh salah satu pihak suatu pemberitahuan dari Pihak yang lain atas adanya Sengketa untuk menyelesaikan Sengketa tersebut dengan penyelesaian damai antara Para Pihak.", $y - 5.3);

   $y = Right1invo($pdf, "9.3", $y + 5);

   $y = Right2invo($pdf, "Apabila Para Pihak tidak dapat mencapai kesepakatan untuk menyelesaikan Sengketa dalam jangka waktu 30 (tiga puluh) hari yang disebutkan dalam Pasal 9.2 di atas maka Para Pihak dengan ini tunduk pada yurisdiksi Kepaniteraan Pengadilan Agama Jakarta Pusat. Ketundukan tersebut tidak akan mempengaruhi hak-hak Agen untuk melakukan proses hukum, sejauh diizinkan berdasarkan hukum negara Indonesia, dalam yurisdiksi lain di dalam atau di luar Republik Indonesia maupun proses hukum di yurisdiksi manapun sesuai dengan peraturan perundangan yang berlaku.", $y - 5.3);

   $y = Right1invo($pdf, "<b>10. LAIN-LAIN</b>&nbsp;", $y + 10);

   $y = Right1invo($pdf, "<b>10.1</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Keterpisahan</b>", $y - 5.3);

   $y = Right1invo($pdf, "10.1.1", $y + 5);

   $y = Right41invo($pdf, "Apabila pada setiap saat salah satu ketentuan atau lebih dari Perjanjian ini adalah atau menjadi melanggar hukum, tidak sah atau tidak dapat diberlakukan berdasarkan hukum negara Republik Indonesia maka legalitas, keabsahan atau keberlakuan ketentuan yang lain dari Perjanjian ini maupun legalitas, keabsahan atau keberlakuan ketentuan tersebut berdasarkan hukum yurisdiksi yang lain dengan cara apa pun tidak akan terpengaruh atau terganggu berdasarkan pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut.", $y - 5.3);

   $y = Right1invo($pdf, "10.1.2", $y + 5);

   $y = Right41invo($pdf, "Apabila ketentuan apa pun dari Perjanjian ini (atau bagiannya) atau pemberlakuannya terhadap pihak atau keadaan manapun melanggar hukum, tidak sah atau tidak dapat diberlakukan dalam hal apa pun, hal tersebut harus ditafsirkan sesempit mungkin yang diperlukan untuk menjadikannya dapat diberlakukan atau sah dan sisa Perjanjian ini dan legalitas, keabsahan atau keberlakuan ketentuan tersebut terhadap para pihak atau keadaan yang lain tidak akan dengan cara lain dipengaruhi atau terganggu berdasarkan pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut dan akan dapat diberlakukan/dilaksanakan sejauh mungkin yang diizinkan berdasarkan hukum.", $y - 5.3);

   $y = Right1invo($pdf, "<b>10.2</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Pengalihan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Seluruh hak dan kewajiban dalam Perjanjian ini bersifat pribadi terhadap Para Pihak dan masing-masing Pihak dalam Perjanjian ini tidak boleh mengalihkan dan/atau memindahkan setiap hak dan kewajiban tersebut kepada pihak ketiga manapun tanpa izin tertulis sebelumnya dari Para Pihak.", $y + 1);

   $y = Right1invo($pdf, "<b>10.3</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Kerahasiaan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Masing-masing Pihak menyepakati untuk merahasiakan seluruh informasi terkait dengan Perjanjian ini dan tidak mengungkapkannya kepada siapa pun, kecuali dengan izin tertulis sebelumnya dari Para Pihak yang lain atau sebagaimana disyaratkan berdasarkan hukum atau peraturan yang berlaku.", $y + 1);

   $y = Right1invo($pdf, "<b>10.4</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Sistem Elektronik dan Dokumen Elektronik</b>", $y - 5.3);

   $y = Right1invo($pdf, "10.4.1", $y + 5);

   $y = Right41invo($pdf, "Sistem Elektronik ", $y - 5.3);

   $y = Right41invo($pdf, "Para Pihak menyepakati dan mengakui bahwa transaksi-transaksi dan semua hal yang ditetapkan dalam Perjanjian ini, termasuk penandatanganan Perjanjian ini, manajemen dan administrasi terkait dengan Perjanjian ini, diatur dan dilaksanakan melalui Sistem Elektronik. ", $y + 1);

   $y = Right1invo($pdf, "10.4.2", $y + 5);

   $y = Right41invo($pdf, "Dokumen Elektronik", $y - 5.3);

   $y = Right41invo($pdf, "Perjanjian ini ditandatangani melalui Sistem Elektronik dan Perjanjian ini merupakan Dokumen Elektronik yang sah dan mengikat, sesuai dengan hukum negara Indonesia. ", $y + 1);

   $y = Right1invo($pdf, "10.4.3", $y + 5);

   $y = Right41invo($pdf, "Pemberian Ganti Rugi", $y - 5.3);

   $y = Right41invo($pdf, "Masing-masing Pihak harus memberikan ganti rugi kepada Agen Substitusi, terhadap setiap kerugian atau kewajiban yang ditimbulkan oleh Agen Substitusi (selain karena alasan kelalaian atau kesalahan disengaja Agen Substitusi) yang bertindak sebagai penyedia Sistem Elektronik berkenaan dengan Perjanjian ini atau dokumen lain yang terkait dengan Sistem Elektronik tersebut. ", $y + 1);

   $y = Right1invo($pdf, "<b>10.5</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Komunikasi</b>", $y - 5.3);

   $y = Right2invo($pdf, "Setiap komunikasi yang akan dilakukan berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan secara tertulis dan, dapat dilakukan dengan surat atau surat elektronik, dalam masing-masing hal, melalui Sistem Elektronik atau Agen Substitusi di 116 Changi Road, #05-00, Singapore 419718 atau di alamat tertentu yang lain sebagaimana yang dapat diberitahukan Agen Substitusi kepada Para Pihak yang lain dari waktu ke waktu. Setiap komunikasi yang dilakukan antara Agen Substitusi dan Para Pihak yang lain berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan ke alamat atau alamat surat elektronik yang diberikan kepada Agen Substitusi atau alamat terdaftar Agen Substitusi, dan akan berlaku apabila diterima.", $y + 1);

   $y = Right1invo($pdf, "<b>10.6</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Pengesampingan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Tidak ada kelalaian untuk menggunakan, atau keterlambatan dalam pelaksanaan, di pihak Agen atau Agen Substitusi, atas hak atau upaya hukum berdasarkan Perjanjian ini yang akan dianggap sebagai pengesampingan, atau pelaksanaan tunggal atau sebagian atas hak atau upaya hukum tidak menghambat pelaksanaan selanjutnya atau yang lain atau pelaksanaan hak atau upaya hukum yang lain. Hak-hak dan upaya hukum berdasarkan Perjanjian ini bersifat kumulatif dan tidak terpisah dari hak-hak atau upaya hukum yang diatur berdasarkan hukum.", $y + 1);

   $y = Right1invo($pdf, "<b>10.7</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Perubahan</b>", $y - 5.3);

   $y = Right2invo($pdf, "Tidak ada ketentuan dari Perjanjian ini yang dapat diubah, dikesampingkan, dihapuskan atau diakhiri secara lisan maupun tidak ada pelanggaran atau wanprestasi berdasarkan setiap ketentuan Perjanjian yang dapat dikesampingkan atau dihapuskan secara lisan tetapi (dalam masing-masing hal) dengan suatu instrumen tertulis yang ditandatangani oleh atau atas nama Para Pihak. Setiap amendemen atau perubahan pada Perjanjian ini harus sesuai Syariah.", $y + 1);

   $y = Right1invo($pdf, "<b>10.8</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Bahasa</b>", $y - 5.3);

   $y = Right2invo($pdf, "Perjanjian ini dibuat dalam bahasa Indonesia dan bahasa Inggris. Kedua versi tersebut sama-sama sah. Para Pihak menyepakati bahwa dalam hal terdapat ketidaksesuaian atau perbedaan penafsiran antara versi bahasa Indonesia dan versi bahasa Inggris maka yang akan berlaku adalah versi bahasa Indonesia dan versi bahasa Inggris akan diubah untuk disesuaikan dengan versi bahasa Indonesia dan untuk menjadikan bagian yang terkait dari versi bahasa Inggris tersebut sesuai dengan bagian yang terkait dari versi bahasa Indonesia.", $y + 1);

   $y = Right1invo($pdf, "<b>10.9</b>", $y + 5);

   $y = Right2invo($pdf, "<b>Rangkap</b>", $y - 5.3);

   $y = Right2invo($pdf, "Perjanjian ini dapat ditandatangani dalam berapa pun jumlah rangkap, dan memiliki keberlakuan yang sama seolah-olah tanda tangan-tanda tangan pada rangkap tersebut terdapat pada salinan tunggal dari Perjanjian ini.", $y + 1);

   $y = Right1invo($pdf, "<b>10.10</b>", $y + 5);

   $y = Right4invo($pdf, "<b>Sesuai Syariah</b>", $y - 5.3);

   $y = Right1invo($pdf, "10.10.1", $y + 5);

   $y = Right41invo($pdf, "&nbsp;&nbsp;Perjanjian ini dimaksudkan untuk sesuai dengan Syariah. Para Pihak dengan ini menyepakati dan mengakui bahwa masing-masing hak dan kewajiban mereka berdasarkan Perjanjian ini dimaksudkan untuk, dan akan, sesuai dengan prinsip Syariah.", $y - 5.3);

   $y = Right1invo($pdf, "10.10.2", $y + 5);

   $y = Right41invo($pdf, "&nbsp;&nbsp;Meskipun terdapat hal tersebut di atas, masing-masing Pihak menyatakan kepada yang lain bahwa tidak akan mengajukan keberatan atau tuntutan terhadap yang lain berdasarkan kepatuhan pada Syariah atau pelanggaran prinsip Syariah sehubungan dengan sebagian dari setiap ketentuan Perjanjian ini.", $y - 5.3);

   $y = Right1invo($pdf, "DEMIKIAN, Para Pihak pada Perjanjian ini telah menandatangani Perjanjian ini secara resmi pada tanggal di bagian awal Perjanjian ini.", $y + 5);
   $pdf->AddPage();
   $y = 0;

   $pdf->SetMargins(20, 15);
   $y = MLeft1($pdf, "<b>THE PRINCIPAL / PRINSIPAL</b>&nbsp; <br>SIGNED for and on behalf of / DITANDATANGANI untuk dan atas nama<br><b>$companyName</b>&nbsp;", $y + 25);

   $oSignx = 70;
   $oSigny = ($y * $pdf->getScaleFactor()) + 5;
   $oSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);

   $y = MLeft1($pdf, "<b>Name/Nama: $dirName</b>&nbsp;<br><b>Title/Jabatan: $dirTitle</b>&nbsp;<br><b>KTP No. / Nomor Kartu Tanda Penduduk: / $dirICID</b>&nbsp;", $y - 2);

   $y = MLeft1($pdf, "<b>AGENT / AGEN</b>&nbsp;", $y + 10);

   $iSignx = 70;
   $iSigny = ($y * $pdf->getScaleFactor()) + 5;
   $iSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);
		$y = MLeft1($pdf, "<b>Name/Nama: $invName</b>&nbsp;", $y - 2);



   $pdf->SetMargins(20, 15);
   $y = MLeft1($pdf, "<b>SUBSTITUTE AGENT / AGEN SUBSTITUSI</b>&nbsp; <br>SIGNED for and on behalf of / DITANDATANGANI untuk dan atas nama<br><b>KAPITAL BOOST PTE LTD</b>&nbsp;", $y + 10);

   $kSignx = 70;
   $kSigny = ($y * $pdf->getScaleFactor()) + 5;
   $kSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);
		$y = MLeft1($pdf, "<b>Name / Nama: Erlangga Wibisono Witoyo</b>&nbsp;<br><b>Title / Jabatan: Chief Executive Office (CEO) </b>&nbsp;", $y - 2);


   // Yang baru



   // if ($y > 160) { // Shift the sign table below
      // $pdf->AddPage();
      // $y = 0;
   // }
   // $synp100 = $pdf->getPage();




   /* ---------- END of Bahasa Version -------------- */



   $pdf->SetPage(1);

   $y = 20;

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, "_______________________ <br>", $y);

   $pdf->SetFontSize("12");
   $y = CenterL($pdf, "<b>QARD AGREEMENT NO. Q$invoContractNo Agreement for Loan Based on the Shariah Principle of Qard</b>&nbsp;&nbsp;
", $y);

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, " <br> _______________________", $y - 6);
   $pdf->SetFontSize("11.5");

   $y = Left1invo($pdf, "<b>THIS QARD AGREEMENT NO. Q$invoContractNo (“Agreement”)</b>&nbsp;&nbsp;  is made on <b>$date</b>&nbsp;&nbsp;,", $y + 5);

   $y = Left1invo($pdf, "<b>BETWEEN:</b>&nbsp;", $y + 5);

   // $y = Left1invo($pdf, "1.	<b>$companyName</b>&nbsp;<br>a limited liability company duly established, and validly existing under the laws of the Republic of Indonesia, under Deed of Establishment Number $invoAktaNo with $comReg and domiciled in Indonesia (<b>the “Borrower”</b>&nbsp;&nbsp;&nbsp; );", $y + 5);
   $y = Left1invo($pdf, "1.	<b>$companyName</b>&nbsp;<br>a limited liability company duly established, and validly existing under the laws of the Republic of Indonesia, under Deed of Establishment Number $invoAktaNo with $comRegTypeEngText $comReg and domiciled in Indonesia (<b>the “Borrower”</b>&nbsp;&nbsp;&nbsp; );", $y + 5);

   $y = Left1invo($pdf, "and", $y + 5);
$invIC = str_replace(" IC "," Identity Card ",$invIC);
   $y = Left1invo($pdf, "2.	<b>$invName</b>&nbsp; &nbsp;, a citizen of $memberCountry, holder of $invIC (<b>the “Financier”</b>&nbsp;&nbsp;).", $y + 5);

   $y = Left1invo($pdf, "The Financier and the Borrower shall be referred to collectively as the “Parties” and individually as a “Party”.", $y + 5);

   $y = Left1invo($pdf, "<b>WHEREAS:</b>&nbsp;", $y + 5);

   $y = Left1invo($pdf, "A.", $y + 5);

   $y = Left2invo($pdf, "The Borrower is in need of a short-term funding to bridge the working capital needs of its Business (as defined below). ", $y - 5.3);

   $y = Left1invo($pdf, "B.", $y + 5);

   $y = Left2invo($pdf, "The Financier has agreed to make available to the Borrower a Facility (as defined below) in Singapore Dollar currency equal or equivalent to $invBack2% of the Receivables Amount (as defined below) based on Shariah principle of Qard, upon the terms and conditions of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "<b>IT IS HEREBY AGREED</b>&nbsp; as follows:", $y + 10);
   $y = Left1invo($pdf, "<b>1.	DEFINITIONS AND INTERPRETATION</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "1.1", $y);

   $y = Left2invo($pdf, "In this Agreement, the following terms shall have the following meanings:", $y - 5.3);

   $tbl188 = "<table cellspacing=\"10\" cellpadding=\"0\" border=\"0\" width=\"338px\">
  <tr> <td width=\"80px\"><b>“Business”	</b>&nbsp;</td> <td align=\"justify\">	: means business activities of the Borrower that is not in contrary with the Shariah principles.

</td> </tr> <tr> <td align=\"justify\"><b>“Business Day”	</b>&nbsp;</td> <td align=\"justify\">	: means a day (other than Saturday, Sunday or public holiday) on which banks in Indonesia are generally open for business.

</td> </tr> <tr> <td align=\"justify\"><b>“Commitment”	</b>&nbsp;</td> <td align=\"justify\">	: means the amount equal or equivalent to $invBack2% of the Receivables Amount or equivalent to $invAmount, to the extent not cancelled or reduced under this Agreement.

</td> </tr> <tr> <td align=\"justify\"><b>“Disbursement”	</b>&nbsp;</td> <td align=\"justify\">	: means a utilisation of the Facility.

</td> </tr> <tr> <td align=\"justify\"><b>“Disbursement Date”	</b>&nbsp;</td> <td align=\"justify\">	: means the date of the Disbursement, being $closdate, the date on which the relevant Loan is made.

</td> </tr> <tr> <td align=\"justify\"><b>“Dispute”	</b>&nbsp;</td> <td align=\"justify\">	: shall have the meaning given to it in Clause 11.2.

</td> </tr> <tr> <td align=\"justify\"><b>“Electronic Document”	</b>&nbsp;</td> <td align=\"justify\">	: means electronic information that is created, forwarded, sent, received, or stored in analog, digital, electromagnetic, optical form, or the like, visible, displayable and/or audible via computers or Electronic Systems, including but not limited to writings, sounds, images, maps, drafts, photographs or the like, letters, signs, figures, access codes, symbols or perforations having certain meaning or definition or understandable to parties qualified to understand them.




</td> </tr> <tr> <td align=\"justify\"><b>“Electronic System”	</b>&nbsp;</td> <td align=\"justify\">	: means www.kapitalboost.com, a peer to peer crowd funding platform managed and organized by Kapital Boost.


</td> </tr> <tr> <td align=\"justify\"><b>“Event of Default”	</b>&nbsp;</td> <td align=\"justify\">	: means any event or circumstances as specified in Clause 10.1.

</td> </tr> <tr> <td align=\"justify\"><b>“Facility”	</b>&nbsp;</td> <td align=\"justify\">	: means the interest free loan facility (in accordance with the Shariah principle of Qard) made available under this Agreement by the Financier to the Borrower, in accordance with the terms and conditions of this Agreement.

</td> </tr> <tr> <td align=\"justify\"><b>“Gharamah”	</b>&nbsp;</td> <td align=\"justify\">	: shall have the meaning given to it in Clause 6.6.

</td> </tr> <tr> <td align=\"justify\"><b>“Kapital Boost”	</b>&nbsp;</td> <td align=\"justify\">	: means Kapital Boost Pte Ltd, as the Electronic System provider.

</td> </tr> <tr> <td align=\"justify\"><b>“Kapital Boost Escrow Account”	</b>&nbsp;</td> <td align=\"justify\">	: means a bank account, managed by Kapital Boost for the purpose of this Agreement.

</td> </tr> <tr> <td align=\"justify\"><b>“Kapital Boost Virtual Account”	</b>&nbsp;</td> <td align=\"justify\">	: means a bank account, which details are provided in the Electronic System, provided by Kapital Boost to the Financier solely for the purpose of (i) the disbursement of the Facility, and (ii) receiving the repayment of the Loan.

</td> </tr> <tr> <td align=\"justify\"><b>“Loan”	</b>&nbsp;</td> <td align=\"justify\">	: means, as the context requires, an interest free loan made or to be made under the Facility or the principal amount outstanding at any time of that loan.



</td> </tr> <tr> <td align=\"justify\"><b>“Maturity Date”	</b>&nbsp;</td> <td align=\"justify\">	: means $invoMaturityDate, the date on which the Receivables become due and payable to the Borrower.

</td> </tr> <tr> <td align=\"justify\"><b>“Receivables”	</b>&nbsp;</td> <td align=\"justify\" style=\"font-size: 11pt\">	: means receivables payable to the Borrower arising under the Underlying Document in respect of any goods and/or services provided by the Borrower, which are acceptable to the Financier.


</td> </tr> <tr> <td align=\"justify\"><b>“Receivables Amount”	</b>&nbsp;</td> <td align=\"justify\" style=\"font-size: 11pt\">	: means Rp$invoAssetCost or SGD$invoAssetCostSGD  being the principal amount of the Receivables that is payable to the Borrower on the Maturity Date.

</td> </tr> <tr> <td align=\"justify\"><b>“Repayment Date”	</b>&nbsp;</td> <td align=\"justify\">	: means $invoRepaymentDate.

</td> </tr> <tr> <td align=\"justify\"><b>“Rp” or Rupiah”	</b>&nbsp;</td> <td align=\"justify\">	: means the lawful currency of Indonesia.

</td> </tr> <tr> <td align=\"justify\"><b>“SGD” or Singapore Dollar”
	</b>&nbsp;</td> <td align=\"justify\">	: means the lawful currency of Singapore.

</td> </tr> <tr> <td align=\"justify\"><b>“Ta’widh”	</b>&nbsp;</td> <td align=\"justify\">	: shall have the meaning given to it in Clause 6.3.

</td> </tr> <tr> <td align=\"justify\"><b>“Underlying Documents”	</b>&nbsp;</td> <td align=\"justify\" style=\"font-size: 11pt\">	: $invoDocs</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(10, 10, 95);
   $pdf->SetFontSize(11.5);
   $pdf->SetX(8);
   $pdf->writeHTML($tbl188);
   $pdf->SetFontSize(11.5);
   $y = $pdf->GetY();

   $y = Left1invo($pdf, "1.2", $y);

   $y = Left2invo($pdf, "Unless a contrary indication appears, any reference in this Agreement to: ", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "references to any document (including this Agreement) include references to that document as amended, consolidated, supplemented, novated or replaced; references to an agreement include any undertaking, representation, deed, agreement or legally enforceable order, arrangement or understanding whether written or not;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "“including” shall be construed as “including without limitation” (and cognate expressions shall be construed similarly);", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "references to a “party” include references to an individual, firm, company, cooperative, corporation, government, limited liability company, state or agency of a state or any association, trust or trustee thereof, pension fund, joint venture, consortium or partnership (whether or not having separate legal personality);", $y - 5.3);

   $y = Left3invo($pdf, "(d)", $y + 2);

   $y = Left4invo($pdf, "references to “written” or “in writing” shall include any methods of reproducing words in a legible and non-transitory form;", $y - 5.3);

   $y = Left3invo($pdf, "(e)", $y + 2);

   $y = Left4invo($pdf, "unless the context clearly indicates a contrary intention, a word or an expression which denotes a gender includes the other gender; and the singular includes the plural and vice versa; and", $y - 5.3);

   $y = Left3invo($pdf, "(f)", $y + 2);

   $y = Left4invo($pdf, "any references, express or implied, to laws or statutory provisions shall be construed as references to those laws or provisions as respectively amended or re-enacted or as their application is modified from time to time by other provisions (whether before or after the date hereof) and shall include any laws or provisions of which they are re-enactments (whether with or without modification) and any orders, regulations, instruments or other subordinate legislation under the relevant law or statutory provision.", $y - 5.3);

   $y = Left1invo($pdf, "1.3", $y + 5);

   $y = Left2invo($pdf, "Clause and Schedule headings are for ease of reference only and shall not be used in its interpretation", $y - 5.3);

   $y = Left1invo($pdf, "1.4", $y + 5);

   $y = Left2invo($pdf, "References to this Agreement are references to this Agreement and any annexures, schedules and exhibits.", $y - 5.3);

   $y = Left1invo($pdf, "1.5", $y + 5);

   $y = Left2invo($pdf, "References in this Agreement to Clauses and Schedules are to clauses in and schedules to this Agreement (unless the context otherwise requires). The Recitals and Schedules to this Agreement shall be deemed to form part of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "1.6", $y + 5);

   $y = Left2invo($pdf, "Where any term is defined within a particular clause other than Clause 1.1, that term shall bear the meaning ascribed to it in that clause wherever it is used in this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "1.7", $y + 5);

   $y = Left2invo($pdf, "Where any number of days is to be calculated from a particular day, such number shall be calculated as excluding such particular day and commencing on the next day. If the last day of such number so calculated falls on a day which is not a Business Day, the last day shall be deemed to be the next succeeding day which is a Business Day.", $y - 5.3);

   $y = Left1invo($pdf, "1.8", $y + 5);

   $y = Left2invo($pdf, "Any reference to days (other than a reference to Business Days), months or years shall be a reference to calendar days, calendar months or calendar years respectively.", $y - 5.3);

   $y = Left1invo($pdf, "1.9", $y + 5);

   $y = Left2invo($pdf, "Any term which refers to an Indonesian legal concept or process shall be deemed to include a reference to the equivalent or analogous concept or process in any other jurisdiction in which this Agreement may apply or to the laws of which a Party may be or become subject.", $y - 5.3);

   $y = Left1invo($pdf, "1.10", $y + 5);

   $y = Left2invo($pdf, "The terms of this Agreement having been negotiated, any party who interprets this Agreement shall not apply the rule which prescribes that in the event of ambiguity a contract should be interpreted against the Party responsible for its drafting.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

    $y = Left1invo($pdf, "<b>2.	THE FACILITY</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "2.1", $y + 5);

   $y = Left2invo($pdf, "Subject to the terms of this Agreement, the Financier agrees to make available to the Borrower a Singapore Dollar short-term loan Facility in the amount of the Commitment.", $y - 5.3);

   $y = Left1invo($pdf, "2.2", $y + 5);

   $y = Left2invo($pdf, "The Borrower agrees and covenants with the Financier that the Borrower shall utilize the proceeds of the Facility solely for working capital of the Business.", $y - 5.3);

   $y = Left1invo($pdf, "<b>3.	CONDITIONS OF DISBURSEMENT  </b>&nbsp;", $y + 10);

    $y = Left1invo($pdf, "The Financier is not required to make the Facility available unless the Financier has received all of the following documents and other evidence in form and substance satisfactory to the Financier, which are available or have been uploaded to the Electronic System prior to or on the date of this Agreement:", $y + 1);

   $y = Left1invo($pdf, "3.1", $y + 5);

   $y = Left2invo($pdf, "The original scanned copy of the Underlying Documents;", $y - 5.3);

   $y = Left1invo($pdf, "3.2", $y + 5);

   $y = Left2invo($pdf, "a copy of the deed of establishment, articles of association and the other applicable corporate documents of the Borrower; and", $y - 5.3);

   $y = Left1invo($pdf, "3.3", $y + 5);

   $y = Left2invo($pdf, "original scanned copy of photo identity documents of the Directors", $y - 5.3);

   $y = Left1invo($pdf, "<b>4.	THE DISBURSEMENT  </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "<b>4.1</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Completion of Disbursement </b> ", $y - 5.3);

   $y = Left1invo($pdf, "4.1.1", $y + 5);

   $y = Left41invo($pdf, "Subject to the provisions of this Agreement, the Financier will only be obliged to make the Facility available if:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "the Borrower has complied in full with the requirements of Clause 3;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "the proposed Disbursement Date is a Business Day;", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "the currency and amount of the Disbursement comply with Clause 4.2 (Currency and Amount);", $y - 5.3);

   $y = Left3invo($pdf, "(d)", $y + 2);

   $y = Left4invo($pdf, "(d)	no default shall have occurred or would, or would be likely to, occur as a result of the Disbursement being made; and", $y - 5.3);

   $y = Left3invo($pdf, "(e)", $y + 2);

   $y = Left4invo($pdf, "all representations and warranties made by the Borrower in or in connection with this Agreement shall be true and correct as at the date on which the Disbursement is to be made with reference to the facts and circumstances then subsisting.", $y - 5.3);

   $y = Left1invo($pdf, "4.1.2", $y + 5);

   $y = Left41invo($pdf, "The Financier shall transfer the amount of the Facility to the Kapital Boost Virtual Account within 3 Business Days after the date of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "<b>4.2</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Currency and Amount </b>", $y - 5.3);

   $y = Left1invo($pdf, "4.2.1", $y + 5);

   $y = Left41invo($pdf, "The amount of the Loan is in the amount of the Commitment, and the currency must be in Singapore Dollars. ", $y - 5.3);

   $y = Left1invo($pdf, "4.2.2", $y + 5);

   $y = Left41invo($pdf, "The Parties agree that for the purpose of the Loan, if the Receivables Amount is in a Rupiah currency, to convert the currency of such amount into Singapore Dollars using the spot rate of exchange pursuant to the terms as stated in the Electronic System.", $y - 5.3);

   $y = Left1invo($pdf, "<b>4.3</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Drawdown</b>", $y - 5.3);

    $y = Left2invo($pdf, "If the conditions set out in this clause have been met, the Financier shall make the Loan available by the Disbursement Date.", $y + 1);

   $y = Left1invo($pdf, "<b>5.	REPAYMENT AND PREPAYMENT </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "<b>5.1</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Repayment</b>", $y - 5.3);

   $y = Left1invo($pdf, "5.1.1", $y + 1);

   $y = Left41invo($pdf, "The Borrower shall immediately repay the Loan on the Repayment Date to the Kapital Boost Escrow Account.", $y - 5.3);

   $y = Left1invo($pdf, "5.1.2", $y + 5);

   $y = Left41invo($pdf, "All payments by the Borrower under this Agreement shall be made in full without any deduction or withholding (whether in respect of set off, counterclaim, duties, taxes, charges or otherwise whatsoever) unless the deduction or withholding is required by law, in which event the Borrower shall:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "ensure that the deduction or withholding does not exceed the minimum amount legally required;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "promptly pay to the Financier such additional amount so that the net amount received by the Financier is equal to the full amount which would have been received by it had no such deduction or withholding been made;", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "pay to the relevant taxation or other authorities, within the period for payment permitted by applicable law, the full amount of the deduction or withholding; and", $y - 5.3);

   $y = Left3invo($pdf, "(d)", $y + 2);

   $y = Left4invo($pdf, "once paid, furnish to the Financier, an official receipt of the relevant taxation or other authorities involved for all amounts deducted or withheld as aforesaid.", $y - 5.3);

   $y = Left1invo($pdf, "5.1.3", $y + 5);

   $y = Left41invo($pdf, "The Borrower may not reborrow any part of the Facility which is repaid.", $y - 5.3);

   $y = Left1invo($pdf, "<b>5.2</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Voluntary Prepayment</b>", $y - 5.3);

   $y = Left2invo($pdf, "In the discretion of the Borrower, the Loan could be prepaid in full without any prepayment penalty.", $y + 1);

   $y = Left1invo($pdf, "<b>6. LATE PAYMENT CHARGES   </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "6.1", $y + 5);

   $y = Left2invo($pdf, "If any amount which is due and payable by the Borrower under or in connection with this Agreement is not paid in full on the Repayment Date (an “unpaid amount”), the Borrower undertakes to pay late payment charge comprising both ta’widh and gharamah (calculated below) to the Financier on each day that an unpaid amount remains outstanding.", $y - 5.3);

   $y = Left1invo($pdf, "6.2", $y + 5);

   $y = Left2invo($pdf, "The total late payment charge shall be pegged directly to average annualized returns of this Agreement (to be determined by the Financier), calculated on daily rest basis as below, provided that the accumulated late payment charges shall not exceed 100% (one hundred percent) of the Loan:", $y - 5.3);

   $y = Left2invo($pdf, "unpaid amount X average annualized returns X No. of overdue day(s) / 365 days", $y + 5);

   $y = Left1invo($pdf, "6.3", $y + 5);

   $y = Left2invo($pdf, "The Financier may retain only such part of the late payment amount paid to it, as is necessary to compensate the Financier for any actual costs (not to include any opportunity or funding costs), subject to and in accordance with the guidelines of its own Shariah advisers, and up to a maximum aggregate amount of 1% (one percent) per annum of the unpaid amount (“Ta’widh”).", $y - 5.3);

   $y = Left1invo($pdf, "6.4", $y + 5);

   $y = Left2invo($pdf, "The Ta’widh in respect of an unpaid amount, being part of the late payment charges, may accrue on a daily basis and shall be calculated in accordance with the following formula: ", $y - 5.3);

   $y = Left2invo($pdf, "actual cost incurred or unpaid amount X 1% X No. of overdue day(s) / 365 days", $y + 5);

   $y = Left1invo($pdf, "6.5", $y + 5);

   $y = Left2invo($pdf, "The amount of Ta’widh shall not be compounded.", $y - 5.3);

   $y = Left1invo($pdf, "6.6", $y + 5);

   $y = Left2invo($pdf, "The Financier hereby agrees, covenants and undertakes to ensure that the balance of the late payment charges (“Gharamah”), if any, shall be paid to charitable bodies as may be selected by the Borrower in consultation with its Shariah advisers.", $y - 5.3);

   $y = Left1invo($pdf, "<b>7.	REPRESENTATIONS AND WARRANTIES  </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "7.1", $y + 5);

   $y = Left2invo($pdf, "The Borrower represents and warrants to the Financier that:", $y - 5.3);

   $y = Left1invo($pdf, "7.1.1", $y + 5);

   $y = Left41invo($pdf, "it is duly established and validly existing under the laws of the Republic of Indonesia;", $y - 5.3);

   $y = Left1invo($pdf, "7.1.2", $y + 5);

   $y = Left41invo($pdf, "it has full power and legal right to enter into and engage in the transactions contemplated by and in accordance with this Agreement and to execute and perform its obligations under this Agreement;", $y - 5.3);

   $y = Left1invo($pdf, "7.1.3", $y + 5);

   $y = Left41invo($pdf, "this Agreement when executed and delivered will constitute, legal, valid and binding obligations of the Borrower enforceable in accordance with its terms;", $y - 5.3);

   $y = Left1invo($pdf, "7.1.4", $y + 5);

   $y = Left41invo($pdf, "it has obtained all required consent from, or conduct the necessary notification to, the government institutions or any party, for the execution, delivery, or performance of this Agreement; and the execution, delivery and performance of this Agreement will not constitute a breach of any agreement to which it is a party or by which it is bound; nor will it contravene any provision of its constituent documents, or violate, conflict with, or result in a breach of any law, order, judgment, decree, or regulation binding on it or to which any of its businesses, properties or assets are subject;", $y - 5.3);

   $y = Left1invo($pdf, "7.1.5", $y + 5);

   $y = Left41invo($pdf, "No Event of Default has occurred or would reasonably be expected to result from the entry into, or performance of, this Agreement; ", $y - 5.3);

   $y = Left1invo($pdf, "7.1.6", $y + 5);

   $y = Left41invo($pdf, "there are no claims, actions, suits or proceedings pending against it, the outcome of which could materially and adversely affect the transactions contemplated by this Agreement, and it is not subject to any order, writ, injunction or decree which could materially and adversely affect its ability to perform the transactions contemplated by this Agreement; ", $y - 5.3);

   $y = Left1invo($pdf, "7.1.7", $y + 5);

   $y = Left41invo($pdf, "there is no provision of any existing law, rule, mortgage, indenture, contract, financing statement, agreement or resolution binding on it that would conflict with or any way prevent the execution, delivery, or carrying out of the terms of this Agreement or any other document or agreement referred to herein; and", $y - 5.3);

   $y = Left1invo($pdf, "7.1.8", $y + 5);

   $y = Left41invo($pdf, "the core or main business of the Borrower and the purpose of the financing under this Agreement are and shall at all times remain Shariah compliant.", $y - 5.3);

   $y = Left1invo($pdf, "7.2", $y + 5);

   $y = Left2invo($pdf, "The Borrower also represents, warrants to and undertakes with the Financier that the foregoing representations and warranties will be true, accurate and repeated on the Disbursement Date and the date of all or part of the Loan is repaid.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

   $y = Left1invo($pdf, "<b>8.	UNDERTAKINGS  </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "<b>8.1</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Affirmative Undertakings</b>", $y - 5.3);

   $y = Left2invo($pdf, "The Borrower undertakes and agrees with the Financier throughout the continuance of the Facility and so long as any sum is or may become payable under this Agreement that the Borrower shall:", $y + 1);

   $y = Left1invo($pdf, "8.1.1", $y + 5);

   $y = Left41invo($pdf, "comply in all respects with all laws to which it may be subject, if failure so to comply would materially impair its ability to perform its obligations under this Agreement;", $y - 5.3);

   $y = Left1invo($pdf, "8.1.2", $y + 5);

   $y = Left41invo($pdf, "ensure that its payment obligations under this Agreement ranks and continue to rank at least pari passu with the claims of all of its other unsecured and unsubordinated creditors, except for obligations mandatorily preferred by law applying to companies generally;", $y - 5.3);

   $y = Left1invo($pdf, "8.1.3", $y + 5);

   $y = Left41invo($pdf, "comply with its obligations under this Agreement; and ", $y - 5.3);

   $y = Left1invo($pdf, "8.1.4", $y + 5);

   $y = Left41invo($pdf, "forthwith upon becoming aware notify the Financier of any event of default under any contractual obligation, litigation, investigation or proceeding which may, if adversely determined, have a material adverse effect on the financial condition of the Borrower and its ability to perform its material obligations under this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "<b>8.2</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Negative Undertakings</b>", $y - 5.3);

   $y = Left2invo($pdf, "Unless the Financier otherwise agrees in writing, the Borrower undertakes and agrees with the Financier throughout the continuance of the Facility and so long as any sum remains owing hereunder that the Borrower shall not:", $y + 1);

   $y = Left1invo($pdf, "8.2.1", $y + 5);

   $y = Left41invo($pdf, "utilize the proceeds of the Facility for a purpose other than as working capital of the Business; ", $y - 5.3);

   $y = Left1invo($pdf, "8.2.2", $y + 5);

   $y = Left41invo($pdf, "make substantial change to the general nature of its business from that carried on at the date of this Agreement; and", $y - 5.3);

   $y = Left1invo($pdf, "8.2.3", $y + 5);

   $y = Left41invo($pdf, "incur any financial indebtedness other than the financial indebtedness incurred pursuant to this Agreement and any documents related thereto.", $y - 5.3);

   $y = Left1invo($pdf, "<b>9.	FURTHER ASSURANCE </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "Each of the Parties shall do all such acts and execute and deliver all such documents as shall be reasonably required in order to fully perform and carry out the terms of this Agreement.", $y + 5);

   $y = Left1invo($pdf, "<b>10. EVENTS OF DEFAULT</b>&nbsp;", $y + 10);


   $y = Left1invo($pdf, "10.1", $y + 5);

   $y = Left2invo($pdf, "Event of Default is deemed to have occurred in any of the following circumstances:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "the Borrower fails to fulfill any of its payment obligations in respect of the Loan and/or any other payment obligations related to the Receivables that are administered in the Electronic System, unless such failure is a result of a technical or administrative error on the part of the bank or financial institution remitting payment by the Borrower to Kapital Boost, or by Kapital Boost to the Financier;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "the Borrower fails to observe and perform any of its obligations under this Agreement;", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "any representation, statement, warranty or undertaking given or made by the Borrower in this Agreement proves to be untrue, incorrect or inaccurate or misleading in any material respect; ", $y - 5.3);

   $y = Left3invo($pdf, "(d)", $y + 2);

   $y = Left4invo($pdf, "any step is taken for (as applicable) the suspension of payments, a moratorium of any indebtedness, bankruptcy, winding up, liquidation or dissolution of the Borrower;", $y - 5.3);

   $y = Left3invo($pdf, "(e)", $y + 2);

   $y = Left4invo($pdf, "the appointment of a liquidator, receiver, administrator, administrative receiver, compulsory manager, provisional supervisor or other similar officer in respect of the Borrower or any of its assets; or", $y - 5.3);

   $y = Left3invo($pdf, "(f)", $y + 2);

   $y = Left4invo($pdf, "where there is an element of fraud, misuse, or misappropriation of the Loan by the Borrower.", $y - 5.3);

   $y = Left1invo($pdf, "10.2", $y + 5);

   $y = Left2invo($pdf, "Upon the occurrence of any Event of Default, the Financier shall have the right by notice, directly or indirectly through Kapital Boost, to the Borrower to:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "declare the entire proportion of the Loan outstanding from the Borrower to the Financier to be immediately due and payable without further demand, notice or other legal formality of any kind; and/or", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "require the Borrower to sell the Receivables and apply all proceeds of sale to pay the outstanding Loan (provided always that such sale shall adhere and in compliance with the principle of Shariah); and/or", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "require the Borrower to indemnify the Financier for any costs, losses or expenses incurred as a result of the occurrence of any Event of Default.", $y - 5.3);

   $y = Left1invo($pdf, "10.3", $y + 5);

   $y = Left2invo($pdf, "The right of the Financier to accelerate the obligations of the Borrower in respect of the repayment of the Loan due to the Financier may be exercised irrespective of, and without being contingent upon, the action or inaction of the Financier. The Financier shall be entitled to bring or prosecute any legal action or claim against the Borrower to recover the outstanding sum, and the Borrower agrees to immediately upon demand indemnify the Financier for all actual costs, losses and liabilities (including legal fees) incurred or suffered by the Financier as a result of any Event of Default.", $y - 5.3);

   $y = Left1invo($pdf, "10.4", $y + 5);

   $y = Left2invo($pdf, "Upon the settlement and full repayment of the Loan only, this Agreement shall cease to be of further effect and neither party shall have any claims against the other save and except for any antecedent breach under this Agreement. For the purpose of terminating this Agreement pursuant to this Clause 10.4, the Parties agree to waive the provisions of Article 1266 of the Indonesian Civil Code (Kitab Undang-Undang Hukum Perdata) to the extent necessary to effectuate the termination of this Agreement without having to obtain any prior decree or decision of the court.", $y - 5.3);

   $y = Left1invo($pdf, "<b>11. GOVERNING LAW AND DISPUTE RESOLUTIONS</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "11.1", $y + 5);

   $y = Left2invo($pdf, "This Agreement and the rights and obligations of the Parties hereunder shall be governed by and interpreted and construed in all respects in accordance with the laws of the Republic of Indonesia.", $y - 5.3);

   $y = Left1invo($pdf, "11.2", $y + 5);

   $y = Left2invo($pdf, "The Parties agree that if any difference, dispute, conflict or controversy (a “Dispute”), arises out of or in connection with this Agreement or its performance, including without limitation any dispute regarding its existence, validity, or termination of rights or obligations of any Party, the Parties will attempt for a period of 30 (thirty) days after the receipt by one Party of a notice from the other Party of the existence of the Dispute to settle the Dispute by amicable settlement between the Parties.", $y - 5.3);

   $y = Left1invo($pdf, "11.3", $y + 5);

   $y = Left2invo($pdf, "If the Parties are unable to reach agreement to settle the Dispute within the 30 (thirty) day period mentioned in Clause 11.2 above, then both Parties hereby submit to the jurisdiction of the Clerk’s Office at the Jakarta Pusat Religious Court. Such submission shall not affect the rights of the Financier to take proceedings, to the extent permitted by Indonesia law, in any other jurisdiction within or outside the Republic of Indonesia nor shall the taking of proceedings in any jurisdiction pursuant to prevailing laws and regulations.", $y - 5.3);

   $y = Left1invo($pdf, "<b>12. MISCELLANEOUS</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "<b>12.1</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Severability</b>", $y - 5.3);

   $y = Left1invo($pdf, "12.1.1", $y + 5);

   $y = Left41invo($pdf, "If at any time any one or more of the provisions hereof is or become illegal, invalid or unenforceable under the laws of the Republic of Indonesia, neither the legality, validity or enforceability of the remaining provisions hereof nor the legality, validity or enforceability of such provisions under the laws of any other jurisdiction shall in any way be affected or impaired thereby.", $y - 5.3);

   $y = Left1invo($pdf, "12.1.2", $y + 5);

   $y = Left41invo($pdf, "If any provision of this Agreement (or part of it) or the application thereof to any party or circumstance shall be illegal, invalid or unenforceable to any extent, it must be interpreted as narrowly as necessary to allow it to be enforceable or valid and the remainder of this Agreement and the legality, validity or enforceability of such provisions to other parties or circumstances shall not be in any way affected or impaired thereby and shall be enforceable/ enforced to the greatest extent permitted by law.", $y - 5.3);

   $y = Left1invo($pdf, "<b>12.2</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Assignment</b>", $y - 5.3);

   $y = Left2invo($pdf, "All rights and obligations in this Agreement are personal to the Parties and each Party in this Agreement may not assign and/or transfer any such rights and obligations to any third party without the prior consent in writing of the Parties.", $y + 1);

   $y = Left1invo($pdf, "<b>12.3</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Confidentiality</b>", $y - 5.3);

   $y = Left2invo($pdf, "Each Party agrees to keep all information relating to this Agreement confidential, and not disclose it to anyone, save with the prior written consent of the other Parties or as required by any applicable laws or regulations.", $y + 1);

   $y = Left1invo($pdf, "<b>12.4</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Kapital Boost</b>", $y - 5.3);

   $y = Left1invo($pdf, "12.4.1", $y + 5);

   $y = Left41invo($pdf, "Electronic System  ", $y - 5.3);

   $y = Left4invo($pdf, "The Parties agree and acknowledge that the transactions and all matters specified in this Agreement, including the execution of this Agreement, the management and administration associated with this Agreement, are arranged and implemented through the Electronic System.", $y + 1);

   $y = Left1invo($pdf, "12.4.2", $y + 5);

   $y = Left41invo($pdf, "Electronic Document", $y - 5.3);

   $y = Left4invo($pdf, "This Agreement is executed through Electronic System and it shall constitute as a valid and binding Electronic Document, in accordance with the Indonesia law.", $y + 1);

   $y = Left1invo($pdf, "12.4.3", $y + 5);

   $y = Left41invo($pdf, "Indemnification", $y - 5.3);

   $y = Left4invo($pdf, "Each Party shall indemnify Kapital Boost against any loss or liability incurred by Kapital Boost (otherwise than by reason of Kapital Boost's negligence or willful misconduct) acting as the provider of the Electronic System with respect to this Agreement and any other documents related thereto.", $y + 1);

   $y = Left1invo($pdf, "<b>12.5</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Communication</b>", $y - 5.3);

   $y = Left2invo($pdf, "Any communication to be made under or in connection with this Agreement shall be made in writing and, may be made by letter or electronic mail, in each case, through Electronic System or Kapital Boost at 116 Changi Road, #05-00, Singapore 419718 or at such other address as Kapital Boost may notify the Parties from time to time. Any communication made between Kapital Boost and any of the Parties under or in connection with this Agreement shall be made to the address or electronic mail address provided to Kapital Boost or its registered address, and shall be effective when received.", $y + 1);

   $y = Left1invo($pdf, "<b>12.6</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Waiver</b>", $y - 5.3);

   $y = Left2invo($pdf, "No failure to exercise, nor any delay in exercising, on the part of the Financier, any right or remedy under this Agreement shall operate as a waiver, nor shall any single or partial exercise of any right or remedy prevent any further or other exercise or the exercise of any other right or remedy. The rights and remedies under this Agreement are cumulative and not exclusive of any rights or remedies provided by law.", $y + 1);

   $y = Left1invo($pdf, "<b>12.7</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Amendment</b>", $y - 5.3);

   $y = Left2invo($pdf, "No provision of this Agreement may be amended, waived, discharged or terminated orally nor may any breach of or default under any of the provisions of this Agreement be waived or discharged orally but (in each case) by an instrument in writing signed by or on behalf of the Parties. Any amendments or variations to this Agreement shall be Shariah-compliant.", $y + 1);

   $y = Left1invo($pdf, "<b>12.8</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Language</b>", $y - 5.3);

   $y = Left2invo($pdf, "This Agreement is made in the Indonesian language and the English language. Both versions are equally authentic. The Parties agree that in the event of any inconsistency or different interpretation between the Indonesian language version and the English language version, the Indonesian language version will prevail and the relevant English language version will be amended to conform with the Indonesian language version and to make relevant part of the English language version consistent with the relevant part of the Indonesian language version.", $y + 1);

   $y = Left1invo($pdf, "<b>12.9</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Counterparts</b>", $y - 5.3);

   $y = Left2invo($pdf, "This Agreement may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y + 1);

   $y = Left1invo($pdf, "<b>12.10</b>", $y + 5);

   $y = Left4invo($pdf, "<b>Shariah-compliant</b>", $y - 5.3);

   $y = Left1invo($pdf, "12.10.1", $y + 5);

   $y = Left41invo($pdf, "&nbsp;&nbsp;This Agreement is intended to be Shariah-compliant. The Parties hereby agree and acknowledge that their respective rights and obligations under this Agreement are intended to, and shall, be in conformity with Shariah principles.", $y - 5.3);

   $y = Left1invo($pdf, "12.10.2", $y + 5);

   $y = Left41invo($pdf, "&nbsp;&nbsp;Notwithstanding the above, each Party represents to the other that it shall not raise any objections or claims against the other on the basis of Shariah-compliance or any breach of Shariah principles in respect of or otherwise in relation to any part of any provision of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "IN WITNESS WHEREOF, the Parties hereto have caused this Agreement to be duly executed on the date first above written.", $y + 5);

   // Waqala


   $pdf->AddPage('P', array('210', '300'));
   $pdf->AddPage('P', array('210', '300'));
   $pdf->SetFont('Helvetica');



   $y = 20;

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, "_______________________ <br>", $y);

   $pdf->SetFontSize("12");
   $y = CenterL($pdf, "<b>WAKALAH AGREEMENT NO. W$invoContractNo</b>&nbsp;", $y);

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, "<br> _______________________ ", $y - 6);
   $pdf->SetFontSize("11");

   $y = Left1invo($pdf, "<b>THIS WAKALAH AGREEMENT NO. W$invoContractNo (“Agreement”)</b>&nbsp;&nbsp; is made on <b>$date</b>&nbsp;&nbsp;,", $y + 5);

   $y = Left1invo($pdf, "<b>BETWEEN:</b>&nbsp;", $y + 5);

   $y = Left1invo($pdf, "1. <b>$companyName</b>&nbsp;&nbsp;<br>a limited liability company duly established, and validly existing under the laws of the Republic of Indonesia, under Deed of Establishment Number $invoAktaNo with $comRegTypeEngText $comReg and domiciled in Indonesia (<b>the “Principal</b>&nbsp;”);", $y + 5);

   $y = Left1invo($pdf, "2.	<b>$invName</b>&nbsp; &nbsp;, a citizen of $memberCountry, holder of $invIC <b>(the “Agent”)</b>&nbsp;&nbsp;;and ", $y + 5);

   $y = Left1invo($pdf, "3.	<b>Kapital Boost Pte Ltd</b>&nbsp; &nbsp;, a limited liability company duly established and validly existing under the laws of the Singapore, domiciled in Singapore, in its capacity as substitute agent (the “<b>Substitute Agent</b>&nbsp;”).", $y + 5);

   $y = Left1invo($pdf, "The Principal, the Agent and the Substitute Agent shall be referred to collectively as the “Parties” and individually as a “Party”.", $y + 5);

   $y = Left1invo($pdf, "<b>WHEREAS:</b>&nbsp;", $y + 5);

   $y = Left1invo($pdf, "A.", $y + 5);

   $y = Left2invo($pdf, "The Principal wishes to appoint the Agent as its agent with respect to the Receivables Management Services (as defined below) based on Shariah principle of Wakalah bil Ujroh, upon the terms and conditions of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "B.", $y + 5);

   $y = Left2invo($pdf, "The Agent wishes to appoint the Substitute Agent as its substitute agent to exercise all or part of the authorization granted by the Principal to the Agent with respect to the Receivables Management Services (as defined below) based on Shariah principle of Wakalah bil Ujroh, upon the terms and conditions of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "<b>IT IS HEREBY AGREED </b>&nbsp;as follows", $y + 10);
   $y = Left1invo($pdf, "<b>1. DEFINITIONS AND INTERPRETATION</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "1.1", $y);

   $y = Left2invo($pdf, "In this Agreement, the following terms shall have the following meanings:", $y - 5.3);

    $tbl193 = "<table cellspacing=\"10\" cellpadding=\"0\" border=\"0\" width=\"338px\">
  <tr>
  <td align=\"justify\" width=\"80px\"><b>“Agency Fee”</b>&nbsp;</td>
  <td align=\"justify\">: shall have the meaning given to it in Clause 2.1. </td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Business”</b>&nbsp;</td>
  <td align=\"justify\">: means business activities of the Principal that is not in contrary with the Shariah principles. </td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Business Day”</b>&nbsp;</td>
  <td align=\"justify\">: means a day (other than Saturday, Sunday or public holiday) on which banks in Indonesia are generally open for business.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Counterparty”</b>&nbsp;</td>
  <td align=\"justify\">: means a party to the Underlying Document other than the Principal.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Dispute”</b>&nbsp;</td>
  <td align=\"justify\">: shall have the meaning given to it in Clause 9.2.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Electronic Document”</b>&nbsp;</td>
  <td align=\"justify\">: means electronic information that is created, forwarded, sent, received, or stored in analog, digital,
electromagnetic, optical form, or the like, visible, displayable and/or audible via computers or Electronic Systems, including but not limited to writings, sounds, images, maps, drafts, photographs or the like, letters, signs, figures, access codes, symbols or perforations having certain meaning or definition or understandable to parties qualified to understand them.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Electronic System”</b>&nbsp;</td>
  <td align=\"justify\">: means www.kapitalboost.com, a peer to peer crowd funding platform managed and organized by the Substitute Agent.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Event of Default”</b>&nbsp;</td>
  <td align=\"justify\">: means any event or circumstances as specified in Clause 8.1.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Fee Payment Date”</b>&nbsp;</td>
  <td align=\"justify\">: means $invoRepaymentDate.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Gharamah”</b>&nbsp;</td>
  <td align=\"justify\">: shall have the meaning given to it in Clause 4.6.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Kapital Boost Escrow Account”</b>&nbsp;</td>
  <td align=\"justify\">: means a bank account managed by the Substitute Agent for the purpose of this Agreement.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Kapital Boost Virtual Account”</b>&nbsp;</td>
  <td align=\"justify\">: means a bank account, which details are provided in the Electronic System, provided by the Substitute Agent to the Agent solely for the purpose of receiving the payment of the Agency Fee after being deducted with the Substitute Agency Fee.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Maturity Date”</b>&nbsp;</td>
  <td align=\"justify\">: means $invoMaturityDate, the date on which the Receivables become due and payable to the Principal.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Receivables”</b>&nbsp;</td>
  <td align=\"justify\">: means receivables payable to the Principal arising under the Underlying Document in respect of any goods and/or services provided by the Principal, which are acceptable to the Agent.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Receivables Amount”</b>&nbsp;</td>
  <td align=\"justify\">: means Rp$invoAssetCost or SGD$invoAssetCostSGD, being the principal amount of the Receivables that is payable to the Principal on the Maturity Date.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Receivables Management Period”</b>&nbsp;</td>
  <td align=\"justify\">: means a period commencing from the date of this Agreement up to the Maturity Date or Fee Payment Date.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Receivables Management Services”</b>&nbsp;</td>
  <td align=\"justify\">: means the process of managing the Receivables, including collecting payments from the Counterparty relating to the Receivables during the Receivables Management Period.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Rp” or “Rupiah”</b>&nbsp;</td>
  <td align=\"justify\">: means the lawful currency of Indonesia.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“SGD” or “Singapore Dollars”</b>&nbsp;</td>
  <td align=\"justify\">: means the lawful currency of Singapore.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Ta’widh”</b>&nbsp;</td>
  <td align=\"justify\">: shall have the meaning given to it in Clause 4.3.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Substitute Agency Fee”</b>&nbsp;</td>
  <td align=\"justify\">: shall have the meaning given to it in Clause 2.2.</td>
  </tr>
  <tr>
  <td align=\"justify\"><b>“Underlying Documents”</b>&nbsp;</td>
  <td align=\"justify\">: $invoDocs</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(10, 10, 105);
   $pdf->SetFontSize(11);
   $pdf->SetX(8);
   $pdf->writeHTML($tbl193);
   $pdf->SetFontSize(11);
   $y = $pdf->GetY();

   $y = Left1invo($pdf, "1.2", $y);

   $y = Left2invo($pdf, "Unless a contrary indication appears, any reference in this Agreement to:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "references to any document (including this Agreement) include references to that document as amended, consolidated, supplemented, novated or replaced; references to an agreement include any undertaking, representation, deed, agreement or legally enforceable order, arrangement or understanding whether written or not;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "“including” shall be construed as “including without limitation” (and cognate expressions shall be construed similarly);", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "references to a “party” include references to an individual, firm, company, cooperative, corporation, government, limited liability company, state or agency of a state or any association, trust or trustee thereof, pension fund, joint venture, consortium or partnership (whether or not having separate legal personality);", $y - 5.3);

   $y = Left3invo($pdf, "(d)", $y + 2);

   $y = Left4invo($pdf, "references to “written” or “in writing” shall include any methods of reproducing words in a legible and non-transitory form;", $y - 5.3);

   $y = Left3invo($pdf, "(e)", $y + 2);

   $y = Left4invo($pdf, "unless the context clearly indicates a contrary intention, a word or an expression which denotes a gender includes the other gender; and the singular includes the plural and vice versa; and", $y - 5.3);

   $y = Left3invo($pdf, "(f)", $y + 2);

   $y = Left4invo($pdf, "any references, express or implied, to laws or statutory provisions shall be construed as references to those laws or provisions as respectively amended or re-enacted or as their application is modified from time to time by other provisions (whether before or after the date hereof) and shall include any laws or provisions of which they are re-enactments (whether with or without modification) and any orders, regulations, instruments or other subordinate legislation under the relevant law or statutory provision.", $y - 5.3);

   $y = Left1invo($pdf, "1.3", $y + 5);

   $y = Left2invo($pdf, "Clause and Schedule headings are for ease of reference only and shall not be used in its interpretation.", $y - 5.3);

   $y = Left1invo($pdf, "1.4", $y + 5);

   $y = Left2invo($pdf, "References to this Agreement are references to this Agreement and any annexures, schedules and exhibits.", $y - 5.3);

   $y = Left1invo($pdf, "1.5", $y + 5);

   $y = Left2invo($pdf, "References in this Agreement to Clauses and Schedules are to clauses in and schedules to this Agreement (unless the context otherwise requires). The Recitals and Schedules to this Agreement shall be deemed to form part of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "1.6", $y + 5);

   $y = Left2invo($pdf, "Where any term is defined within a particular clause other than Clause 1.1, that term shall bear the meaning ascribed to it in that clause wherever it is used in this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "1.7", $y + 5);

   $y = Left2invo($pdf, "Where any number of days is to be calculated from a particular day, such number shall be calculated as excluding such particular day and commencing on the next day. If the last day of such number so calculated falls on a day which is not a Business Day, the last day shall be deemed to be the next succeeding day which is a Business Day.", $y - 5.3);

   $y = Left1invo($pdf, "1.8", $y + 5);

   $y = Left2invo($pdf, "Any reference to days (other than a reference to Business Days), months or years shall be a reference to calendar days, calendar months or calendar years respectively.", $y - 5.3);

   $y = Left1invo($pdf, "1.9", $y + 5);

   $y = Left2invo($pdf, "Any term which refers to an Indonesian legal concept or process shall be deemed to include a reference to the equivalent or analogous concept or process in any other jurisdiction in which this Agreement may apply or to the laws of which a Party may be or become subject.", $y - 5.3);

   $y = Left1invo($pdf, "1.10", $y + 5);

   $y = Left2invo($pdf, "The terms of this Agreement having been negotiated, any party who interprets this Agreement shall not apply the rule which prescribes that in the event of ambiguity a contract should be interpreted against the Party responsible for its drafting.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

    $y = Left1invo($pdf, "<b>2. APPOINTMENT OF THE AGENT AND THE SUBSTITUTE AGENT</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "2.1", $y + 5);

   $y = Left2invo($pdf, "As consideration for the Agent agreeing to act, and the payment by the Principal of a fee in the amount of SGD$percent_return (“Agency Fee”), the Principal irrevocably:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "appoints the Agent to act as its agent in relation to the Receivables Management Services;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "grants power of attorney and authorises the Agent to enter into any document and to receive payment on behalf of the Principal as part of the Receivables Management Services, and to take such action on behalf of the Principal and to exercise such rights, remedies, powers and discretions as are conferred to the Principal by the Underlying Documents, together with such rights, remedies, powers and discretions as are reasonably incidental thereto; and", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "agrees to the appointment of the Substitute Agent by the Agent under the terms of Clause 2.2 below.", $y - 5.3);

   $y = Left1invo($pdf, "2.2", $y + 5);

   $y = Left2invo($pdf, "As consideration for the Substitute Agent agreeing to act, and the payment by the Agent of a fee in the amount of SGD$sub_agency_fee (“Substitute Agency Fee”), the Agent irrevocably:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "appoints the Substitute Agent to act as its substitute agent in relation to the Receivables Management Services; ", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "grants power of attorney and authorises the Substitute Agent to enter into any document and to receive payment on behalf of the Agent (and therefore, on behalf of the Principal) as part of the Receivables Management Services, and to take such action necessary on behalf of the Agent (and therefore, on behalf of the Principal) and to exercise such rights, remedies, powers and discretions as are conferred to the Principal by the Underlying Documents, together with such rights, remedies, powers and discretions as are reasonably incidental thereto; and", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "grants power of attorney and authorises the Substitute Agent to receive payment of the Agency Fee on behalf of the Agent and to apply a part of the Agency Fee received by the Substitute Agent for the payment of the Substitute Agency Fee", $y - 5.3);

   $y = Left1invo($pdf, "2.3", $y + 5);

   $y = Left2invo($pdf, "The appointment of the Agent as agent pursuant to Clause 2.1 above, and the Substitute Agent as substitute agent pursuant to Clause 2.2 above, will terminate automatically at the end of the Receivables Management Period, provided that Agency Fee and the Substitute Agency Fee have been duly received in full (without deduction) by the Agent and the Substitute Agent, respectively. Without prejudice to the rights of the Agent and/or the Substitute Agent under this Agreement, the Agent and/or the Substitute Agent may, in their respective sole discretions, waive part of their respective fees payable to them pursuant to this Clause 2.", $y - 5.3);

   $y = Left1invo($pdf, "2.4", $y + 5);

   $y = Left2invo($pdf, "All powers of attorney and authorisation granted in this Agreement shall not terminate for any reason, including, without limitation, any reason set forth in Articles 1813, 1814 and 1816 of the Indonesian Civil Code (Kitab Undang-Undang Hukum Perdata) (except for renunciation by the Agent (with respect to the powers of attorney and authorisation granted under Clause 2.1) or the Substitute Agent (with respect to the powers of attorney and authorisation granted under Clause 2.2)).", $y - 5.3);

   $y = Left1invo($pdf, "2.5", $y + 5);

   $y = Left2invo($pdf, "Notwithstanding the grant of powers of attorney and authorisation under this Agreement, the Principal shall remain responsible for the collection of the Receivables and shall have the obligation to ensure that the Receivables are paid in full by the Counter party. Nothing in this Agreement shall be construed so as to reduce the Principal’s obligations under the Underlying Documents.", $y - 5.3);

   $y = Left1invo($pdf, "<b>3.	PAYMENT OF AGENCY FEE AND SUBSTITUTE AGENCY FEE</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "3.1", $y + 5);

   $y = Left2invo($pdf, "On the Fee Payment Date, the Principal shall pay the Agency Fee in full to Kapital Boost Escrow Account.", $y - 5.3);

   $y = Left1invo($pdf, "3.2", $y + 5);

   $y = Left2invo($pdf, "On the Fee Payment Date, the Agent shall pay the Substitute Agency Fee in full to the Substitute Agent.", $y - 5.3);

   $y = Left1invo($pdf, "3.3", $y + 5);

   $y = Left2invo($pdf, "Each of the Agency Fee and the Substitute Agency Fee is exclusive of any value added tax or similar tax, and the Principal (with respect the Agency Fee) and the Agent (with respect to the Substitute Agency Fee) to shall make the relevant payment together with any applicable value added tax or similar tax.", $y - 5.3);

   $y = Left1invo($pdf, "3.4", $y + 5);

   $y = Left2invo($pdf, "Any amount payable pursuant to this Agreement shall be paid without deduction, any counterclaim, set-off or withholding of any kind.", $y - 5.3);

   $y = Left1invo($pdf, "<b>4. LATE PAYMENT CHARGES  </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "4.1", $y + 5);

   $y = Left2invo($pdf, "If any amount which is due and payable by the Principal or the Agent under or in connection with this Agreement is not paid in full on the Fee Payment Date (an “unpaid amount”), each of the Principal or the Agent undertakes to pay late payment charge comprising both ta’widh and gharamah (calculated below) to (as the case may be) the Agent or the Substitute Agent on each day that an unpaid amount remains outstanding.", $y - 5.3);

   $y = Left1invo($pdf, "4.2", $y + 5);

   $y = Left2invo($pdf, "The total late payment charge shall be pegged directly to average annualized returns of this Agreement (to be determined by the Substitute Agent), calculated on daily rest basis as below, provided that the accumulated late payment charges shall not exceed 100% (one hundred percent) of (as the case may be) the Agency Fee or the Substitute Agency Fee:", $y - 5.3);

   $y = Left2invo($pdf, "unpaid amount X average annualized returns X No. of overdue day(s)/ 365 day", $y + 5);

   $y = Left1invo($pdf, "4.3", $y + 5);

   $y = Left2invo($pdf, "The Agent or the Substitute Agent (as the case may be)may retain only such part of the late payment amount paid to it, as is necessary to compensate (as the case may be) the Agent or the Substitute Agent for any actual costs, subject to and in accordance with the guidelines of its own Shariah advisers, and up to a maximum aggregate amount of 1% (one percent) per annum of the unpaid amount (“Ta’widh”).", $y - 5.3);

   $y = Left1invo($pdf, "4.4", $y + 5);

   $y = Left2invo($pdf, "The Ta’widh in respect of an unpaid amount, being part of the late payment charges, may accrue on a daily basis and shall be calculated in accordance with the following formula:", $y - 5.3);

   $y = Left2invo($pdf, "actual cost incurred or unpaid amount X 1% X No. of overdue day(s) / 365", $y + 5);

   $y = Left1invo($pdf, "4.5", $y + 5);

   $y = Left2invo($pdf, "The amount of Ta’widh shall not be compounded.", $y - 5.3);

   $y = Left1invo($pdf, "4.6", $y + 5);

   $y = Left2invo($pdf, "Each of the Agent or the Substitute Agent hereby agrees, covenants and undertakes to ensure that the balance of the late payment charges (“Gharamah”), if any, shall be paid to charitable bodies as may be selected by (as the case may be) the Principal or the Agent in consultation with its Shariah advisers.", $y - 5.3);


   $y = Left1invo($pdf, "<b>5.	REPRESENTATIONS AND WARRANTIES</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "5.1", $y + 5);

   $y = Left2invo($pdf, "The Principal represents and warrants to the Agent and the Substitute Agent that:", $y - 5.3);

   $y = Left1invo($pdf, "5.1.1", $y + 5);

   $y = Left41invo($pdf, "It is duly established and validly existing    under the laws of the Republic of Indonesia;", $y - 5.3);

   $y = Left1invo($pdf, "5.1.2", $y + 5);

   $y = Left41invo($pdf, "It has full power and legal right to enter into and engage in the transactions contemplated by this Agreement and to execute and perform its obligations under this Agreement;", $y - 5.3);

   $y = Left1invo($pdf, "5.1.3", $y + 5);

   $y = Left41invo($pdf, "this Agreement when executed and delivered will constitute, legal, valid and binding obligations of the Principal enforceable in accordance with its terms;", $y - 5.3);

   $y = Left1invo($pdf, "5.1.4", $y + 5);

   $y = Left41invo($pdf, "it has obtained all required consent from, or conduct the necessary notification to, the government institutions or any party, for the execution, delivery, or performance of this Agreement; and the execution, delivery and performance of this Agreement will not constitute a breach of any agreement to which it is a party or by which it is bound; nor will it contravene any provision of its constituent documents, or violate, conflict with, or result in a breach of any law, order, judgment, decree, or regulation binding on it or to which any of its businesses, properties or assets are subject;", $y - 5.3);

   $y = Left1invo($pdf, "5.1.5", $y + 5);

   $y = Left41invo($pdf, "No Event of Default has occurred or would reasonably be expected to result from the entry into, or performance of, this Agreement;", $y - 5.3);

   $y = Left1invo($pdf, "5.1.6", $y + 5);

   $y = Left41invo($pdf, "there are no claims, actions, suits or proceedings pending against it, the outcome of which could materially and adversely affect the transactions contemplated by this Agreement, and it is not subject to any order, writ, injunction or decree which could materially and adversely affect its ability to perform the transactions contemplated by this Agreement;", $y - 5.3);

   $y = Left1invo($pdf, "5.1.7", $y + 5);

   $y = Left41invo($pdf, "there is no provision of any existing law, rule, mortgage, indenture, contract, financing statement, agreement or resolution binding on it that would conflict with or any way prevent the execution, delivery, or carrying out of the terms of this Agreement or any other document or agreement referred to herein; and", $y - 5.3);

   $y = Left1invo($pdf, "5.1.8", $y + 5);

   $y = Left41invo($pdf, "the core or main business of the Principal and the grant of powers of attorney and authority under this Agreement are and shall at all times remain Shariah compliant.", $y - 5.3);

   $y = Left1invo($pdf, "5.2", $y + 5);

   $y = Left2invo($pdf, "The Principal also represents, warrants to and undertakes with the Agent and the Substitute Agent that the foregoing representations and warranties will be true, accurate and repeated on the Maturity Date, the Fee Payment Date, and the date of all or part of the Agency Fee and the Substitute Agency Fee, is repaid.", $y - 5.3);

   $pdf->AddPage('P', array('210', '300'));
   $pdf->AddPage('P', array('210', '300'));
   $y = 10;

   $y = Left1invo($pdf, "<b>6. UNDERTAKINGS  </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "<b>6.1</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Affirmative Undertakings </b>", $y - 5.3);

   $y = Left2invo($pdf, "The Principal undertakes and agrees with the Agent and the Substitute Agent so long as any sum is or may become payable under this Agreement that the Principal shall:", $y + 1);

   $y = Left1invo($pdf, "6.1.1", $y + 5);

   $y = Left41invo($pdf, "comply in all respects with all laws to which it may be subject, if failure so to comply would materially impair its ability to perform its obligations under this Agreement;", $y - 5.3);

   $y = Left1invo($pdf, "6.1.2", $y + 5);

   $y = Left41invo($pdf, "ensure that its payment obligations under this Agreement ranks and continue to rank at least paripassu with the claims of all of its other unsecured and unsubordinated creditors, except for obligations mandatorily preferred by law applying to companies generally;", $y - 5.3);

   $y = Left1invo($pdf, "6.1.3", $y + 5);

   $y = Left41invo($pdf, "comply with its obligations under this Agreement; and", $y - 5.3);

   $y = Left1invo($pdf, "6.1.4", $y + 5);

   $y = Left41invo($pdf, "forthwith upon becoming aware notify the Agent and the Substitute Agent of any event of default under any contractual obligation, litigation, investigation or proceeding which may, if adversely determined, have a material adverse effect on the financial condition of the Principal and its ability to perform its material obligations under this Agreement. ", $y - 5.3);

   $y = Left1invo($pdf, "<b>6.2</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Negative Undertakings </b>", $y - 5.3);

   $y = Left2invo($pdf, "Unless the Agent otherwise agrees in writing, the Principal undertakes and agrees with the Agent so long as any sum is or may become payable under this Agreement that the Principal shall not:", $y + 1);

   $y = Left1invo($pdf, "6.2.1", $y + 5);

   $y = Left41invo($pdf, "make substantial change to the general nature of its Business from that carried on at the date of this Agreement; and", $y - 5.3);

   $y = Left1invo($pdf, "6.2.2", $y + 5);

   $y = Left41invo($pdf, "incur any financial indebtedness other than the financial indebtedness incurred pursuant to this Agreement and any documents related thereto ", $y - 5.3);

   $y = Left1invo($pdf, "<b>7. FURTHER ASSURANCE  </b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "Each of the Parties shall do all such acts and execute and deliver all such documents as shall be reasonably required in order to fully perform and carry out the terms of this Agreement.", $y + 1);

   $y = Left1invo($pdf, "<b>8. EVENTS OF DEFAULT</b>&nbsp;", $y + 10);


   $y = Left1invo($pdf, "8.1", $y + 5);

   $y = Left2invo($pdf, "Event of Default is deemed to have occurred in any of the following circumstances:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "The Principal fails to fulfill any of its payment obligations in respect of the Agency Fee and/or any other payment obligations related to the Receivables that are administered in the Electronic System, unless such failure is a result of a technical or administrative error on the part of the bank or financial institution remitting payment by the Principal to the Agent, or by the Agent to the Substitute Agent;", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "the Principal fails to observe and perform any of its obligations under this Agreement;", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "any representation, statement, warranty or undertaking given or made by the Principal in this Agreement proves to be untrue, incorrect or inaccurate or misleading in any material respect; ", $y - 5.3);

   $y = Left3invo($pdf, "(d)", $y + 2);

   $y = Left4invo($pdf, "any step is taken for (as applicable) the suspension of payments, a moratorium of any indebtedness, bankruptcy, winding up, liquidation or dissolution of the Principal; ", $y - 5.3);

   $y = Left3invo($pdf, "(e)", $y + 2);

   $y = Left4invo($pdf, "the appointment of a liquidator, receiver, administrator, administrative receiver, compulsory manager, provisional supervisor or other similar officer in respect of the Principal or any of its assets; or", $y - 5.3);

   $y = Left3invo($pdf, "(f)", $y + 2);

   $y = Left4invo($pdf, "where there is an event of default or a default (however described) under other agreements in which the Principal and the Agent, or the Principal and the Substitute Agent, are parties.", $y - 5.3);

   $y = Left1invo($pdf, "8.2", $y + 5);

   $y = Left2invo($pdf, "Upon the occurrence of any Event of Default, the Agent shall have the right by notice to the Principal to:", $y - 5.3);

   $y = Left3invo($pdf, "(a)", $y + 2);

   $y = Left4invo($pdf, "declare the Agency Fee to be immediately due and payable without further demand, notice or other legal formality of any kind; and/or", $y - 5.3);

   $y = Left3invo($pdf, "(b)", $y + 2);

   $y = Left4invo($pdf, "require the Principal to sell the Receivables and apply a portion of the sale proceeds to pay the Agency Fee (provided always that such sale shall adhere and in compliance with the principle of Shariah); and/or", $y - 5.3);

   $y = Left3invo($pdf, "(c)", $y + 2);

   $y = Left4invo($pdf, "require the Principal to indemnify the Agent and/or the Substitute Agent for any costs, losses or expenses incurred as a result of the occurrence of any Event of Default.", $y - 5.3);

   $y = Left1invo($pdf, "8.3", $y + 5);

   $y = Left2invo($pdf, "The right of the Agent to accelerate the obligations of the Principal in respect of the payment of the Agency Fee may be exercised irrespective of, and without being contingent upon, the action or inaction of the Agent. The Agent and/or the Substitute Agent shall be entitled to bring or prosecute any legal action or claim against the Principal with respect to payment of the Agency Fee, and the Principal agrees to immediately upon demand indemnify the Agent and/or the Substitute Agent for all actual costs, losses and liabilities (including legal fees) incurred or suffered by the Agent and/or the Substitute Agent as a result of any Event of Default.", $y - 5.3);

   $y = Left1invo($pdf, "8.4", $y + 5);

   $y = Left2invo($pdf, "Upon the settlement and full payment of the Agency Fee and the Substitute Agency Fee only, this Agreement shall cease to be of further effect and neither party shall have any claims against the other save and except for any antecedent breach under this Agreement. For the purpose of terminating this Agreement pursuant to this Clause 8.4, the Parties agree to waive the provisions of Article 1266 of the Indonesian Civil Code (Kitab Undang-Undang Hukum Perdata) to the extent necessary to effectuate the termination of this Agreement without having to obtain any prior decree or decision of the court.", $y - 5.3);

   $y = Left1invo($pdf, "<b>9.	GOVERNING LAW AND DISPUTE RESOLUTIONS</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "9.1", $y + 5);

   $y = Left2invo($pdf, "This Agreement and the rights and obligations of the Parties hereunder shall be governed by and interpreted and construed in all respects in accordance with the laws of the Republic of Indonesia.", $y - 5.3);

   $y = Left1invo($pdf, "9.2", $y + 5);

   $y = Left2invo($pdf, "The Parties agree that if any difference, dispute, conflict or controversy (a “Dispute”), arises out of or in connection with this Agreement or its performance, including without limitation any dispute regarding its existence, validity, or termination of rights or obligations of any Party, the Parties will attempt for a period of 30 (thirty) days after the receipt by one Party of a notice from the other Party of the existence of the Dispute to settle the Dispute by amicable settlement between the Parties.", $y - 5.3);

   $y = Left1invo($pdf, "9.3", $y + 5);

   $y = Left2invo($pdf, "If the Parties are unable to reach agreement to settle the Dispute within the 30 (thirty)day period mentioned in Clause 9.2 above, then the Parties hereby submit to the jurisdiction of the Clerk’s Office at the Jakarta Pusat Religious Court. Such submission shall not affect the rights of the Agent and/or the Substitute Agent to take proceedings, to the extent permitted by Indonesia law, in any other jurisdiction within or outside the Republic of Indonesia nor shall the taking of proceedings in any jurisdiction pursuant to prevailing laws and regulations.", $y - 5.3);

   $y = Left1invo($pdf, "<b>10. MISCELLANEOUS</b>&nbsp;", $y + 10);

   $y = Left1invo($pdf, "<b>10.1</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Severability</b>", $y - 5.3);

   $y = Left1invo($pdf, "10.1.1", $y + 5);

   $y = Left41invo($pdf, "If at any time any one or more of the provisions hereof is or become illegal, invalid or unenforceable under the laws of the Republic of Indonesia, neither the legality, validity or enforceability of the remaining provisions hereof nor the legality, validity or enforceability of such provisions under the laws of any other jurisdiction shall in any way be affected or impaired thereby.", $y - 5.3);

   $y = Left1invo($pdf, "10.1.2", $y + 5);

   $y = Left41invo($pdf, "If any provision of this Agreement (or part of it) or the application thereof to any party or circumstance shall be illegal, invalid or unenforceable to any extent, it must be interpreted as narrowly as necessary to allow it to be enforceable or valid and the remainder of this Agreement and the legality, validity or enforceability of such provisions to other parties or circumstances shall not be in any way affected or impaired thereby and shall be enforceable/ enforced to the greatest extent permitted by law.", $y - 5.3);

   $y = Left1invo($pdf, "<b>10.2</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Assignment</b>", $y - 5.3);

   $y = Left2invo($pdf, "All rights and obligations in this Agreement are personal to the Parties and each Party in this Agreement may not assign and/or transfer any such rights and obligations to any third party without the prior consent in writing of the Parties.", $y + 1);

   $y = Left1invo($pdf, "<b>10.3</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Confidentiality</b>", $y - 5.3);

   $y = Left2invo($pdf, "Each Party agrees to keep all information relating to this Agreement confidential, and not disclose it to anyone, save with the prior written consent of the other Parties or as required by any applicable laws or regulations.", $y + 1);

   $y = Left1invo($pdf, "<b>10.4</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Electronic System and Electronic Document</b>", $y - 5.3);

   $y = Left1invo($pdf, "10.4.1", $y + 5);

   $y = Left41invo($pdf, "Electronic System ", $y - 5.3);

   $y = Left41invo($pdf, "The Parties agree and acknowledge that the transactions and all matters specified in this Agreement, including the execution of this Agreement, the management and administration associated with this Agreement, are arranged and implemented through the Electronic System. ", $y + 1);

   $y = Left1invo($pdf, "10.4.2", $y + 5);

   $y = Left41invo($pdf, "Electronic Document", $y - 5.3);

   $y = Left41invo($pdf, "This Agreement is executed through Electronic System and it shall constitute as a valid and binding Electronic Document, in accordance with the Indonesia law. ", $y + 1);

   $y = Left1invo($pdf, "10.4.3", $y + 5);

   $y = Left41invo($pdf, "Indemnification", $y - 5.3);

   $y = Left41invo($pdf, "Each Party shall indemnify the Substitute Agent, against any loss or liability incurred by the Substitute Agent (otherwise than by reason of the Substitute Agent's negligence or willful misconduct) in acting as the provider of the Electronic System with respect to this Agreement and any other documents related thereto. ", $y + 1);

   $y = Left1invo($pdf, "<b>10.5</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Communication</b>", $y - 5.3);

   $y = Left2invo($pdf, "Any communication to be made under or in connection with this Agreement shall be made in writing and, may be made by letter or electronic mail, in each case, through Electronic System or the Substitute Agent at 116 Changi Road, #05-00, Singapore 419718 or at such other address as the Substitute Agent may notify the other Parties from time to time. Any communication made between the Substitute Agent and any other Parties under or in connection with this Agreement shall be made to the address or electronic mail address provided to the Substitute Agent or its registered address, and shall be effective when received.", $y + 1);

   $y = Left1invo($pdf, "<b>10.6</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Waiver</b>", $y - 5.3);

   $y = Left2invo($pdf, "No failure to exercise, nor any delay in exercising, on the part of the Agent or the Substitute Agent, any right or remedy under this Agreement shall operate as a waiver, nor shall any single or partial exercise of any right or remedy prevent any further or other exercise or the exercise of any other right or remedy. The rights and remedies under this Agreement are cumulative and not exclusive of any rights or remedies provided by law.", $y + 1);

   $y = Left1invo($pdf, "<b>10.7</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Amendment</b>", $y - 5.3);

   $y = Left2invo($pdf, "No provision of this Agreement may be amended, waived, discharged or terminated orally nor may any breach of or default under any of the provisions of this Agreement be waived or discharged orally but (in each case) by an instrument in writing signed by or on behalf of the Parties. Any amendments or variations to this Agreement shall be Shariah-compliant.", $y + 1);

   $y = Left1invo($pdf, "<b>10.8</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Language</b>", $y - 5.3);

   $y = Left2invo($pdf, "This Agreement is made in the Indonesian language and the English language. Both versions are equally authentic. The Parties agree that in the event of any inconsistency or different interpretation between the Indonesian language version and the English language version, the Indonesian language version will prevail and the relevant English language version will be amended to conform with the Indonesian language version and to make relevant part of the English language version consistent with the relevant part of the Indonesian language version.", $y + 1);

   $y = Left1invo($pdf, "<b>10.9</b>", $y + 5);

   $y = Left2invo($pdf, "<b>Counterparts</b>", $y - 5.3);

   $y = Left2invo($pdf, "This Agreement may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y + 1);

   $y = Left1invo($pdf, "<b>10.10</b>", $y + 5);

   $y = Left4invo($pdf, "<b>Sharia-compliant</b>", $y - 5.3);

   $y = Left1invo($pdf, "10.10.1", $y + 5);

   $y = Left41invo($pdf, "&nbsp;&nbsp;This Agreement is intended to be Shariah-compliant. The Parties hereby agree and acknowledge that their respective rights and obligations under this Agreement are intended to, and shall, be in conformity with Shariah principles.", $y - 5.3);

   $y = Left1invo($pdf, "10.10.2", $y + 5);

   $y = Left41invo($pdf, "&nbsp;&nbsp;Notwithstanding the above, each Party represents to the other that it shall not raise any objections or claims against the other on the basis of Shariah-compliance or any breach of Shariah principles in respect of or otherwise in relation to any part of any provision of this Agreement.", $y - 5.3);

   $y = Left1invo($pdf, "IN WITNESSWHEREOF, the Parties hereto have caused this Agreement to be duly executed on the date first above written.", $y + 5);

   /*  ---------- END of English Version ------------ */
// akhir invoice

    $output = $pdf->Output('/var/www/kapitalboost.com/public_html/assets/pdfs/' . $token . '.pdf', 'F');
// $output = $pdf->Output('/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com/assets/pdfs/' . $token . '.pdf', 'F');

} else {

   $companyNameID = $comNameId;
   $companyRegistrationID = $comRegId;
   $purchaseAssetID = $purchaseAssetId;

   $closeMonth = date('n', strtotime($closeDate));
   $closeMonthId = $monthsID[$closeMonth];
   $closeDay = date('d', strtotime($closeDate));
   $closeYear = date('Y', strtotime($closeDate));
   $cCloseDateID = "$closeDay $closeMonthId $closeYear";

   $closeMonth1 = date('n', strtotime($cCloseDate1));
   $closeMonth1Id = $monthsID[$closeMonth1];
   $closeDay1 = date('d', strtotime($cCloseDate1));
   $closeYear1 = date('Y', strtotime($cCloseDate1));
   $cCloseDate1ID = "$closeDay1 $closeMonth1Id $closeYear1";

   $dirICID = $dirIcId;
   $invICID = $icId;

   // if($invICID == ''){
      // echo 'no investor IC for Bahasa';
      // exit;
   // }


   $pdf = new FPDI();
   $pdf->setPrintHeader(false);
   $pdf->setPrintFooter(false);


   $pdf->AddPage('P', array('210', '300'));
   $pdf->SetFont('Helvetica');



   $y = 20;

   // $y = CenterR($pdf, "TERTANGGAL HARI INI $dateID", $y);

   // $pdf->SetFontSize("25");
   // $y = CenterR($pdf, ". . . . . . . . . . . . . . . .", $y);

   // $pdf->SetFontSize("14");
   // $y = CenterR($pdf, "PERJANJIAN MURABAHAH", $y, true);

   // $pdf->SetFontSize("25");
   // $y = CenterR($pdf, ". . . . . . . . . . . . . . . .", $y - 6);
   // $pdf->SetFontSize("12");

   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "_______________________ <br> ", $y);

   $pdf->SetFontSize("12");
   $y = Right1($pdf, "<b><span style=\"text-align:center !important;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PERJANJIAN MURABAHAH NO. M$invoContractNo &nbsp;&nbsp;&nbsp;  Perjanjian Pembiayaan Berdasarkan Prinsip  &nbsp;&nbsp;&nbsp;Syariah Murabahah</b>&nbsp;</span>", $y);

   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "<br> _______________________ ", $y - 6);
   $pdf->SetFontSize("11.5");

   $y = Right1invo($pdf, "<b>PERJANJIAN MURABAHAH NO. M$invoContractNo ini (“Perjanjian”)</b>&nbsp;&nbsp;&nbsp; dibuat pada <b>$dateID</b>&nbsp;&nbsp;,", $y + 5);

   $y = Right1($pdf, "<b>ANTARA:</b> ", $y + 5);

   //$y = Right1($pdf, "<b>1. $companyNameID</b>&nbsp;", $y + 5);

   // $y = Right1($pdf, "1. <b>$companyNameID</b>&nbsp;<br>suatu perusahaan perseroan terbatas yang resmi didirikan, dan tunduk berdasarkan hukum negara Republik Indonesia, berdasarkan Akta Pendirian Nomor $invoAktaNoId dengan $companyRegistrationID dan berkedudukan di Republik Indonesia (“<b>Peminjam</b>&nbsp;”&nbsp;)&nbsp;", $y + 5);
   $y = Right120($pdf, "1. <b>$companyName</b>&nbsp;<br>suatu perusahaan perseroan terbatas yang resmi didirikan, dan tunduk berdasarkan hukum negara Republik Indonesia, berdasarkan Akta Pendirian Nomor $invoAktaNoId dengan $comRegTypeInaText $companyRegistrationID dan berkedudukan di Republik Indonesia", $y + 5);

   //$y = Right1($pdf, "($companyRegistrationID)", $y);

   $y = Right1($pdf, "(selanjutnya disebut sebagai \"<b>Perusahaan</b>&nbsp;\") ", $y + 5);

   $y = Right1($pdf, "<b>DAN</b>&nbsp;", $y + 5);
$invIC = str_replace(" IC "," Identity Card ",$invIC);
   $y = Right1($pdf, "<b>2. $invName</b>&nbsp;<br>($invIC)", $y + 5);

   $y = Right1($pdf, "(selanjutnya disebut sebagai \"<b>Pemodal</b>&nbsp;  \")", $y + 5);

   $y = Right1($pdf, "<b>BAHWA:</b>&nbsp;", $y + 5);

   $y = Right1($pdf, "A.", $y + 5);

   $y = Right2($pdf, "Perusahaan memerlukan pendanaan untuk pembelian, akuisisi atau pengadaan $purchaseAssetID, yang dijelaskan dalam Lampiran A (\"<b>Aset</b>&nbsp; \"), yang bermaksud digunakan Perusahaan untuk usahanya, dan Perusahaan berkeinginan memperoleh pembiayaan tersebut sesuai dengan prinsip-prinsip Syariah; ", $y - 7);

   $y = Right1($pdf, "B.", $y + 5);

   $y = Right2($pdf, "Pemodal, bersama-sama dengan beberapa pemodal lainnya (secara bersama-sama disebut \"<b>Grup Investor</b>&nbsp; \") telah sepakat untuk menyediakan pembiayaan yang diperlukan kepada Perusahaan berdasarkan prinsip Murabahah dalam Syariah, khususnya Murabahah terhadap pemberi pesanan pembelian; dan", $y - 7);

   $y = Right1($pdf, "C.", $y + 5);

   $y = Right2($pdf, "Pemodal telah sepakat untuk memberikan bagiannya dalam pembiayaan yang diperlukan berdasarkan ketentuan dan tunduk pada persyaratan Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "<b>DENGAN INI DISEPAKATI </b>&nbsp; sebagai berikut:", $y + 10);
   $y = Right1($pdf, "<b>1. METODE PEMBIAYAAN</b>&nbsp;", $y + 10);

   $y = Right1($pdf, "1.1", $y);

   $y = Right2($pdf, "Untuk tujuan pembiayaan akuisisi Aset, Perusahaan dengan ini meminta agar Grup Investor secara bersama-sama membeli Aset dari Pemasok untuk total imbalan <b>$assetCost</b>&nbsp; (\"<b>Biaya Pembelian</b>&nbsp;&nbsp;&nbsp;\"), dan selanjutnya menjual Aset kepada Perusahaan dengan harga jual  <b>$salePrice</b>&nbsp; (\"<b>Harga Jual</b>&nbsp; \"). Biaya sesungguhnya kepada Perusahaan atas pembelian Aset adalah <b>$assetCostCurrency</b>&nbsp; (\"<b>Biaya Mata Uang Pembelian </b>&nbsp;&nbsp; \"), yang mata uangnya dapat dibeli menggunakan jumlah Dolar Singapura atas Biaya Pembelian dengan nilai tukar mata uang yang berlaku sebesar  S$1:$exchange Perusahaan berjanji (melalui wa’d) untuk membeli Aset dari Grup Investor, setelah pembelian Aset oleh Grup Investor sesuai dengan ketentuan Perjanjian ini. Skema Murabahah yang ditetapkan di atas akan menghasilkan laba bagi Grup Investor sebagaimana ditetapkan di bawah ini.", $y - 7);

   $tbl1 = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td><b>Biaya Pembelian Aset yang wajib dibayarkan oleh Grup Investor</b>&nbsp;</td>
  <td><b>Harga Jual yang wajib dibayarkan oleh Perusahaan kepada Grup Investor</b>&nbsp;</td>
  <td><b>Jumlah Laba yang diperoleh dari Grup Investor</b>&nbsp;</td>
  </tr>
  <tr>
  <td>$assetCost</td>
  <td>$salePrice</td>
  <td>$percent_assetCost (atau $return% dari Biaya Pembelian Aset)</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetX(120);
   $pdf->SetFontSize(10);
   $pdf->writeHTML($tbl1);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Right1($pdf, "1.2", $y);

   $y = Right2($pdf, "Para pihak sepakat bahwa Biaya Pembelian mengacu pada biaya akuisisi sesungguhnya atas Aset yang dikeluarkan atau akan dikeluarkan oleh Grup Investor atau agen Grup Investor untuk akuisisi Aset, di luar pengeluaran-pengeluaran langsung (seperti biaya-biaya pengiriman, transportasi, penyimpanan dan perakitan, pajak-pajak, biaya takaful dan biaya pertanggungan asuransi) dan belanja rutin atau biaya-biaya tidak langsung (seperti gaji staf dan biaya buruh).", $y - 7);

   $y = Right1($pdf, "1.3", $y + 5);

   $y = Right2($pdf, "Pemodal bersedia membiayai persisnya  $invAmount (yakni $invBack%) dari keseluruhan jumlah Biaya Pembelian. Jumlah tersebut yang akan dibiayai oleh Pemodal akan dibayarkan oleh Pemodal kepada Pemasok, melalui Perusahaan sebagai agen. Untuk tujuan memungkinkan Kapital Boost, sebagai agen Perusahaan berdasarkan Pasal 3.4.1, untuk mengalihkan jumlah Biaya Pembelian kepada Perusahaan selambat-lambatnya $closdate (the \"<b>Tanggal Kontribusi Biaya Pembelian</b>&nbsp;&nbsp; \"), Pemodal akan mengalihkan jumlah kontribusinya terhadap Biaya Pembelian (sebagaimana ditetapkan dalam pasal ini di atas) ke rekening yang ditetapkan secara tertulis oleh Kapital Boost, paling lambat <b>tiga (3) hari kerja</b>&nbsp;&nbsp;&nbsp;&nbsp; sejak tanggal Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "1.4", $y + 5);

   $y = Right2($pdf, "Berdasarkan kontribusi Pemodal terhadap Biaya Pembelian sebagaimana ditetapkan di atas, Pemodal akan memiliki $invBack% dari Aset setelah pembelian Aset oleh Grup Investor, dan Pemodal akan berhak atas bagian $invBack% dari Harga Jual dan setiap angsuran atau bagiannya dibayarkan oleh Perusahaan, sejumlah:-", $y - 7);


   BulletR($pdf, $y);

   $y = Right3($pdf, "Bagian Pemodal dari Harga Jual: SGD$expect_payout", $y + 3);

   BulletR($pdf, $y);

   $y = Right3($pdf, "Bagian Pemodal dari Laba: SGD$percent_return", $y + 3);

   $y = Right1($pdf, "1.5", $y + 5);

   $y = Right2($pdf, "Pemodal (sebagai bagian dari Grup Investor) dengan ini menunjuk Perusahaan sebagai agennya untuk membeli Aset atas nama Pemodal. Perusahaan dengan ini menerima penunjukan sebagai agen tersebut, dan akan membeli dan menangani Aset sesuai dengan ketentuan Perjanjian ini. Perusahaan, sebagai agen, akan membeli Aset dalam mata uang Biaya Mata Uang Pembelian, untuk imbalan yang tidak melebihi Biaya Mata Uang Pembelian, dan pada selambat-lambatnya <b>$cCloseDate1ID</b>&nbsp;&nbsp;(\"<b>Tanggal Pembelian</b>&nbsp;&nbsp; \"). ", $y - 7);

   $y = Right1($pdf, "1.6", $y + 5);

   $y = Right2($pdf, "Pemodal dan Perusahaan (sebagai agen Grup Investor untuk tujuan pembelian Aset) dengan ini mengakui dan menyepakati bersama bahwa:", $y - 7);

   $y = Right1($pdf, "1.6.1", $y + 3);

   $y = Right3($pdf, "Grup Investor (termasuk Pemodal) telah menunjuk Perusahaan untuk bertindak sebagai agen Grup Investor untuk membeli Aset atas nama Grup Investor karena pengetahuan dan jaringan pasar Perusahaan terkait dengan Aset;", $y - 7);

   $y = Right1($pdf, "1.6.2", $y + 3);

   $y = Right3($pdf, "pembelian Aset oleh Grup Investor melalui Perusahaan sebagai agen dapat menghasilkan manfaat bagi Grup Investor melalui efisiensi proses pembelian dan juga penghematan biaya dalam hal Perusahaan dapat menjamin atau memperoleh Aset dengan harga yang lebih rendah daripada Biaya Mata Uang Pembelian;", $y - 7);

   $y = Right1($pdf, "1.6.3", $y + 3);

   $y = Right3($pdf, "dalam hal Perusahaan, sebagai agen, dapat membeli Aset dengan harga lebih rendah daripada Biaya Mata Uang Pembelian, Grup Investor dengan ini sepakat untuk mengesampingkan, dengan cara tanazul (yakni pengesampingan), hak-haknya untuk mengklaim kelebihan jumlah pendanaan yang tidak digunakan; dan Perusahaan, sebagai agen, berhak untuk menyimpan manfaat penghematan biaya tersebut sebagai insentif berdasarkan hibah atas pelaksanaan kerjanya yang baik; ", $y - 7);

   $y = Right1($pdf, "1.6.4", $y + 3);

   $y = Right3($pdf, "dalam keadaan yang dijelaskan dalam Pasal 1.6.3, biaya terhadap Grup Investor atas pembelian Aset akan merupakan Biaya Pembelian, sebagai jumlah hibah yang diberikan kepada Perusahaan, sebagai agen, akan merupakan bagian dari biaya sesungguhnya atas pembelian Aset, dan dengan demikian Biaya Pembelian yang sama akan dicerminkan pada Kontrak Penjualan Murabahah yang akan dibuat oleh Grup Investor dalam penjualan Aset; ", $y - 7);

   $y = Right1($pdf, "1.6.5", $y + 3);
   $synp0 = $pdf->getPage();
   $y = Right3($pdf, "dalam hal Perusahaan tidak dapat membeli $foreignCurrency yang memadai sebagai Biaya Mata Uang Pembelian menggunakan Biaya Pembelian yang dikontribusikan oleh Grup Investor sebagai hasil penurunan nilai tukar mata uang dari nilai tukar mata uang yang disebutkan dalam Pasal 1.1:", $y - 7);

   $y = Right3($pdf, "(a)", $y + 2);

   $y = Right4($pdf, "Perusahaan, sebagai agen Grup Investor, akan:", $y - 7);

   $y = Right4($pdf, "(i)", $y);

   $y = Right5($pdf, "segera memberitahukan Grup Investor dan mengembalikan Biaya Pembelian kepada Grup Investor, yang setelahnya Perjanjian ini akan berakhir; atau ", $y - 7);

   $y = Right4($pdf, "(ii)", $y);

   $y = Right5($pdf, "mendanai jumlah kekurangan dalam mata uang Biaya Pembelian (the \"<b>Jumlah Kekurangan Biaya Pembelian</b>&nbsp;&nbsp;&nbsp; \") yang akan disyaratkan bagi Perusahaan untuk membeli Biaya Mata Uang Pembelian untuk merampungkan pembelian Aset sebagai agen atas nama Grup Investor;", $y - 7);

   $y = Right3($pdf, "(b)", $y + 3);

   $y = Right4($pdf, "mengikuti pendanaan Jumlah Kekurangan Biaya Pembelian oleh Perusahaan dan perampungan pembelian Aset oleh Perusahaan, sebagai agen Grup Investor:", $y - 7);

   $y = Right4($pdf, "(i)", $y);
   $y = Right5($pdf, "Jumlah Kekurangan Biaya Pembelian akan merupakan utang yang terutang oleh Grup Investor kepada Perusahaan, dalam kedudukannya sebagai agen bagi Grup Investor;", $y - 7);

   $y = Right4($pdf, "(ii)", $y);
   $y = Right5($pdf, "meskipun terdapat ayat (i) di atas, Perusahaan tidak akan, sebelum balik nama Aset menurut Pasal 1.8, memiliki klaim apa pun terhadap bagian apa pun dari bagian hak milik terhadap Aset; ", $y - 7);

   $y = Right4($pdf, "(iii)", $y);
   $y = Right5($pdf, "Harga Jual (atau, jika terdapat lebih dari satu angsuran atas Harga Jual, maka yang angsuran pertama) akan dinaikkan dengan Jumlah Kekurangan Biaya Pembelian, yang akan dicerminkan pada Penawaran Pembelian Perusahaan yang diserahkan oleh Perusahaan kepada Grup Investor; dan", $y - 7);

   $y = Right4($pdf, "(iv)", $y);
   $y = Right5($pdf, "Grup Investor, Perusahaan (dalam kedudukannya sebagai agen Grup Investor) dan sebagai pembeli Aset, semuanya sepakat bahwa Jumlah Kekurangan Biaya Pembelian yang terutang oleh Perusahaan kepada Grup Investor (sebagai bagian dari Harga Jual) akan dengan sendirinya dipotong terhadap jumlah yang sama yang terutang oleh Grup Investor kepada Perusahaan (sesuai dengan ayat (i) di atas), dan baik Pemodal atau Perusahaan tidak akan mengajukan tuntutan sehubungan dengan pembayaran atas jumlah tersebut oleh pihak lain.", $y - 7);

   $y = Right1($pdf, "1.6.6", $y + 5);
   $y = Right3($pdf, "Dalam hal pada Tanggal Kontribusi Biaya Pembelian, keseluruhan jumlah kontribusi dari Grup Investor terhadap Biaya Pembelian lebih rendah daripada jumlah Biaya Pembelian yang ditetapkan pada Pasal 1.1, tetapi lebih tinggi daripada 50% dari jumlah tersebut, Kapital Boost (sebagai agen yang bertindak atas nama, dan dengan instruksi, Perusahaan) akan segera memberitahukan Grup Investor atas maksud Perusahaan untuk (i) melanjutkan pembelian Aset dalam jumlah yang berkurang untuk total imbalan yang setara dengan keseluruhan jumlah kontribusi dari Grup Investor (the \"<b>Biaya Pembelian yang Disesuaikan</b>&nbsp;&nbsp;&nbsp;\"); atau yang lain (ii) mengembalikan kontribusi kepada anggota-anggota Grup Investor menurut Pasal 6.2.; dan", $y - 7);

   $y = Right3($pdf, "(a)", $y + 2);
   $y = Right4($pdf, "mengikuti pemilihan Perusahaan menurut Pasal 1.6.6 (i):", $y - 7);

   $y = Right4($pdf, "(1)", $y + 3);
   $y = Right5($pdf, "Perusahaan (sebagai agen Grup Investor) akan merampungkan pembelian Aset pada Tanggal Pembelian;", $y - 7);

   $y = Right4($pdf, "(2)", $y + 3);
   $y = Right5($pdf, "Perjanjian ini akan dibaca untuk menafsirkan Biaya Pembelian sebagai Biaya Pembelian yang Disesuaikan, dan semua ketentuan yang terkait lainnya dari Perjanjian ini dengan demikian akan ditafsirkan termasuk: (i) pengurangan proporsional terhadap Biaya Mata Uang Pembelian dan Harga Jual; dan (ii) kenaikan yang proporsional dalam persentase kepemilikan setiap Pemodal yang berkontribusi atas Aset, masing-masing sebagaimana ditetapkan dalam pemberitahuan yang diserahkan menurut Pasal 1.6.6(i) (the \"<b>Pemberitahuan Penyesuaian</b>&nbsp;&nbsp;\"); dan", $y - 7);

   $y = Right4($pdf, "(3)", $y + 3);
   $y = Right5($pdf, "tidak akan ada perubahan terhadap (i) jumlah bagian Pemodal yang berkontribusi atas Laba; (ii) Tanggal Pembelian; dan (iii) Tanggal Jatuh Tempo dan tanggal-tanggal lainnya yang ditetapkan dalam Perjanjian untuk pembayaran angsuran Harga Jual; dan", $y - 7);

   $y = Right3($pdf, "(b)", $y + 3);
   $y = Right4($pdf, "setelah penyerahan oleh Kapital Boost kepada Grup Investor atas Pemberitahuan Penyesuaian yang memenuhi persyaratan Perjanjian ini, setiap Pemodal yang berkontribusi dianggap telah menyepakati penyesuaian-penyesuaian tersebut dan Pemberitahuan Penyesuaian merupakan perubahan yang berlaku dan mengikat terhadap Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "1.6.7", $y + 5);
   $synp6 = $pdf->getPage();
   $y = Right3($pdf, "dalam keadaan yang dijelaskan dalam Pasal 1.6.6, Perusahaan akan melanjutkan membeli Aset, dalam jumlah yang berkurang untuk total imbalan yang setara dengan keseluruhan kontribusi dari Para Pemodal yang memberikan persetujuan (\"Biaya Pembelian yang Disesuaikan\"), dengan ketentuan bahwa jumlahnya lebih dari 50% dari Harga Pembelian.", $y - 7);

   $y = Right1($pdf, "1.7", $y + 5);
   $y = Right2($pdf, "Perusahaan, sebagai agen Grup Investor, berjanji untuk memeriksa Aset sebelum penyerahan/penagihan, dan akan memastikan bahwa semua spesifikasi sehubungan dengan Aset sebagaimana ditetapkan dalam Lampiran A akan terpenuhi. Perusahaan berjanji untuk segera memberikan ganti rugi kepada Grup Investor untuk kerugian atau kewajiban apa pun yang diakibatkan secara langsung atau tidak langsung dari kelalaian Perusahaan untuk melakukan pemeriksaan, dan memastikan kelayakan untuk tujuan atau pemenuhan spesifikasi atas Aset.", $y - 7);

   $y = Right1($pdf, "1.8", $y + 5);
   $y = Right2($pdf, "Setelah pembelian Aset oleh Perusahaan, sebagai agen Grup Investor, Perusahaan akan melakukan penawaran untuk membeli Aset (\"<b>Penawaran Perusahaan untuk Pembelian</b>&nbsp;&nbsp;&nbsp;\") dari Grup Investor, untuk imbalan yang setara dengan Harga Jual. Penjualan Aset oleh Grup Investor kepada Perusahaan akan berlaku setelah penerimaan atas Penawaran Perusahaan untuk Pembelian oleh Kapital Boost, yang bertindak sebagai agen Grup Investor, dalam bentuk Lampiran B. Harga Jual akan dibayarkan oleh Perusahaan kepada Grup Investor dengan ketentuan pembayaran tunda sebagaimana diatur dalam Pasal 2 di bawah ini. Para Pihak dengan ini sepakat bahwa hak milik atas Aset akan diteruskan dengan segera dan secara penuh dari Grup Investor kepada Perusahaan setelah penerimaan atas Penawaran Perusahaan untuk Pembelian dalam bentuk Lampiran B.", $y - 7);


   $y = Right1($pdf, "<b>2. PEMBAYARAN HARGA JUAL</b>&nbsp;", $y + 15);
   $synp1 = $pdf->getPage();
   $y = Right1($pdf, "2.1", $y + 10);
   $y = Right2($pdf, "Harga Jual akan dibayarkan oleh Perusahaan sebagai berikut:- ", $y - 7);

   $payoutStringR = "";
   for ($i = 0; $i < 12; $i++) {
	  $newDate = date("d F Y", strtotime($dateA[$i]));
	  $percentProfit = $amountA[$i];
	  $marginMonthly = 0;
	  if ($percentProfit!="" or $percentProfit!=null) {
		  $marginTotal = ($return/100)*$totalFundingAmt;
		  // echo "$marginTotal+$totalFundingAmt";
		  $totalAmount = $marginTotal+$totalFundingAmt;
		  // echo "($percentProfit/100)*$totalAmount";
		  // echo "$percentProfit/100 <br />";
		  $percentageTotalAmt = $percentProfit/100;
		  // echo "$marginMonthly = $percentProfit*$totalAmount*10;  <br />";
		  $marginMonthly = $percentProfit*$totalAmount/100;
		  $payoutStringR .= "<tr><td style=\"text-align:center;height:18px\"> $newDate </td><td style=\"text-align:center;height:18px\">SGD ".number_format($marginMonthly,2)."</td></tr>";
	  }



   }

   $payoutTblR = "<table cellspacing=\"0\" cellpadding=\"3\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Tanggal</b>&nbsp;</td>
  <td style=\"text-align:center\"><b>Harga Jual yang wajib dibayarkan</b>&nbsp;</td>
  </tr>
  $payoutStringR
  </table>
  ";
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetX(110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutTblR);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Right1($pdf, "2.2", $y - 5);
   $y = Right2($pdf, "Seluruh pembayaran berdasarkan Perjanjian ini oleh Pemodal dan Perusahaan akan dilakukan secara penuh dalam mata uang Harga Jual yang disebutkan dalam Perjanjian ini, tanpa pengurangan apa pun untuk, dan bebas dari, pajak, pungutan, bea, biaya, imbalan jasa, pengurangan saat ini atau yang akan datang, atau persyaratan dalam jenis apa pun yang dibebankan atau dikenakan oleh otoritas perpajakan.", $y - 7);

   $y = Right1($pdf, "2.3", $y + 5);
   $y = Right2($pdf, "Seluruh pembayaran Harga Jual atau angsurannya akan dilakukan kepada setiap pemodal dalam Grup Investor secara pari passu, berdasarkan kontribusi persentase terhadap Harga Pembelian sebagaimana dijelaskan di atas. Para Pihak sepakat bahwa dalam hal pembayaran tersebut tidak dilakukan secara pari passu, Para Pihak akan melakukan pembayaran-pembayaran yang dilakukan sebagaimana antara Grup Investor untuk memperbaiki kelebihan/kekurangan pembayaran Harga Jual.", $y - 7);

   $y = Right1($pdf, "<b>3. KAPITAL BOOST SEBAGAI AGEN </b>&nbsp;", $y + 15);
   $synp7 = $pdf->getPage();
   $y = Right1($pdf, "3.1", $y + 10);
   $y = Right2($pdf, "Pemodal mengakui bahwa Perusahaan telah menunjuk, atau dengan ini dianggap menunjuk, Kapital Boost sebagai agen Perusahaan untuk tujuan (a) menagih Biaya Pembelian dari Grup Investor, untuk pembayaran kepada Pemasok melalui Perusahaan; (b) memberitahukan Grup Investor atas pilihan yang dibuat oleh Perusahaan menurut Pasal 1.6.5; dan (c) mendistribusikan kepada Grup Investor uang yang mencakup Harga Jual atau angsuran-angsurannya yang jatuh tempo dan wajib dibayarkan oleh Perusahaan.", $y - 7);

   $y = Right1($pdf, "3.2", $y + 5);
   $y = Right2($pdf, "Grup Investor selanjutnya dengan ini (dengan penyerahan Perjanjian ini secara elektronik kepada Kapital Boost) menunjuk Kapital Boost untuk menerima dan menandatangani Kontrak Penjualan Murabahah sesuai Lampiran B Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "3.3", $y + 5);
   $y = Right2($pdf, "Perusahaan mengakui bahwa Pemodal telah menunjuk, atau dengan ini dianggap menunjuk, Kapital Boost sebagai agen Pemodal untuk tujuan (a) menagih Harga Jual atau angsuran-angsurannya yang jatuh tempo dan wajib dibayarkan oleh Perusahaan kepada Grup Investor dan (b) memulai proses hukum setelah terjadinya Peristiwa Wanprestasi terhadap Perusahaan, dengan ketentuan bahawa Kapital Boost telah menerima dana yang cukup dari Pemodal untuk mendanai biaya terkait dengan proses hukum tersebut. ", $y - 7);

   $y = Right1($pdf, "3.4", $y + 5);
   $y = Right2($pdf, "Untuk tujuan keagenan berdasarkan Pasal 3.1, 3.2 dan 3.3:", $y - 7);

   $y = Right1($pdf, "3.4.1", $y + 3);
   $y = Right3($pdf, "Kapital Boost akan (a) mengalihkan kepada Perusahaan pada selambat-lambatnya tanggal Kontribusi Biaya Pembelian atas Biaya Pembelian yang diterimanya dari Grup Investor; dan (b) setelah diterimanya jumlah Harga Jual yang jatuh tempo dari Perusahaan kepada setiap anggota Grup Investor pada tanggal tertentu, segera mendistribusikan kepada setiap pemodal dalam Grup Investor proporsinya atas jumlah yang diterima, dalam setiap hal, tanpa pengurangan apa pun untuk, dan bebas dari, pajak, pungutan, bea, biaya, imbalan jasa, pengurangan saat ini atau yang akan datang, atau persyaratan dalam jenis apa pun yang dibebankan atau dikenakan oleh otoritas perpajakan; ", $y - 7);

   $y = Right1($pdf, "3.4.2", $y + 3);
   $y = Right3($pdf, "Kapital Boost akan memastikan bahwa ketentuan berikut ini dimasukkan dalam Pemberitahuan Penyesuaian:", $y - 7);

   $y = Right3($pdf, "(a)", $y);
   $y = Right4($pdf, "Harga Pembelian yang Disesuaikan; ", $y - 7);

   $y = Right3($pdf, "(b)", $y);
   $y = Right4($pdf, "Biaya Mata Uang Pembelian dan Harga Jual yang dikurangi secara proporsional;", $y - 7);

   $y = Right3($pdf, "(c)", $y);
   $y = Right4($pdf, "(a)	persentase kepemilikan Pemodal yang meningkat secara proporsional atas Aset; dan", $y - 7);

   $y = Right3($pdf, "(d)", $y);
   $y = Right4($pdf, "penegasan yang jelas bahwa tidak ada perubahan terhadap (i) jumlah bagian Pemodal atas Laba; (ii) Tanggal Pembelian; dan (iii) Tanggal Jatuh Tempo dan tanggal-tanggal lainnya yang ditetapkan dalam Perjanjian untuk pembayaran angsuran Harga Jual.", $y - 7);

   $y = Right1($pdf, "3.4.3", $y + 5);
   $y = Right3($pdf, "sebelum penandatanganan Kontrak Penjualan Murabahah sebagai agen Grup Investor, Kapital Boost akan mengecek ketentuan Perjanjian ini untuk memastikan pemenuhan atas ketentuan Perjanjian ini;", $y - 7);

   $y = Right1($pdf, "3.4.4", $y + 5);
   $y = Right3($pdf, "Kapital Boost akan menginformasikan Grup Investor dengan segera setelah mengetahui keterlambatan Perusahaan menyerahkan Kontrak Penjualan Murabahah atas kekurangan dalam Kontrak Penjualan Murabahah atau alasan lain yang akan mengakibatkan Kapital Boost tidak dapat untuk, atau memprioritaskan untuk tidak; menandatangani Kontrak Penjualan Murabahah;", $y - 7);

   $y = Right1($pdf, "3.4.5", $y + 5);
   $y = Right3($pdf, "Kapital Boost akan berusaha untuk mengambil semua tindakan yang diperlukan untuk memperoleh jumlah yang belum dibayarkan atas seluruh atau sebagian Harga Jual dari Perusahaan; ", $y - 7);

   $y = Right1($pdf, "3.4.6", $y + 5);
   $y = Right3($pdf, "Kapital Boost akan memberikan kepada Grup Investor dengan kutipan biaya yang mencakup biaya pengacara dan biaya lainnya yang harus dikeluarkan oleh Kapital Boost sebagai agen. Kapital Boost akan melanjutkan untuk menginstruksikan penasihat hukum untuk mengambil tindakan hukum, seperti yang disarankan oleh penasihat hukum tersebut, terhadap Perusahaan, dengan ketentuan bahawa Pemodal telah mengalihkan kepada Kapital Boost dana yang diperlukan untuk menutupi biaya hukum. Biaya-biaya tersebut yang timbul harus di bayar ganti secara penuh oleh Perusahaan tanpa mengurangi Pasal 7.3;", $y - 7);

   $y = Right1($pdf, "3.4.7", $y + 5);
   $y = Right3($pdf, "Kapital Boost tidak akan bertanggung jawab atau berkewajiban atas wanprestasi atau kekurangan pembayaran oleh Perusahaan atas seluruh atau sebagian Harga Jual yang jatuh tempo kepada Grup Investor pada setiap waktu;", $y - 7);

   $y = Right1($pdf, "3.4.8", $y + 5);
   $y = Right3($pdf, "Dengan mengambil setiap tindakan yang dijelaskan dalam Pasal 3.4 ini sebagai agen Grup Investor, Kapital Boost dianggap telah menyetujui penunjukannya sebagai agen tersebut, dan telah sepakat untuk terikat oleh ketentuan Pasal 3.4. ini.", $y - 7);

   if ($y > 200) {
      $pdf->AddPage();
      $y = -5;
   }
   $y = Right1($pdf, "<b>4. PENGALIHAN KEPEMILIKAN & JAMINAN</b>&nbsp;", $y + 15);
   $synp2 = $pdf->getPage();

   $y = Right1($pdf, "4.1", $y + 10);
   $y = Right2($pdf, "Perusahaan sepakat untuk membeli Aset sebagaimana adanya, di manapun tempatnya, dan dianggap telah memeriksa Aset sebelum pembelian Aset tersebut. Hak milik dan kepemilikan Aset bersama-sama dengan seluruh hak, kepentingan dan manfaat yang melekat pada Aset tersebut akan diteruskan dari Grup Investor kepada Perusahaan dengan segera setelah penjualan Aset oleh Grup Investor kepada Perusahaan menurut ketentuan Perjanjian ini, meskipun Harga Jual belum dibayar atau dilunasi oleh Perusahaan kepada Grup Investor. Perusahaan mengesampingkan seluruh klaim terhadap Grup Investor sebagai penjual atas pelanggaran jaminan atau cacat sehubungan dengan Aset. Perusahaan akan segera pada tempat pembelian, menagih dan mengambil alih kepemilikan Aset dari Grup Investor atau agen (-agennya), jika ada.", $y - 7);

   $y = Right1($pdf, "4.2", $y + 5);
   $y = Right2($pdf, "Grup Investor mengalihkan kepada Perusahaan jaminannya dan hak-hak lain terhadap pemasok/penjual Aset pada saat pembelian oleh Perusahaan.", $y - 7);

   $y = Right1($pdf, "<b>5. PEMBAYARAN AWAL ATAS HARGA JUAL</b>&nbsp;", $y + 15);

   $y = Right2($pdf, "Perusahaan dapat memilih dengan diskresinya untuk memberlakukan pembayaran awal atas seluruh atau sebagian Harga Jual. Setiap pembayaran awal atas Harga Jual akan mengurangi kewajiban-kewajiban pembayaran Harga Jual dari Perusahaan dalam urutan kronologi terbalik, dan setiap pembayaran awal tersebut atas sebagian Harga Jual tidak akan menunda, mengurangi atau mempengaruhi sisa kewajiban pembayaran Perusahaan berkenaan dengan Harga Jual. Setelah pembayaran awal atas setiap bagian dari Harga Jual, Grup Investor atau Investor perseorangan dengan diskresi mereka atau diskresinya dapat memberikan ibra’ (rabat) atas sisa bagian (-bagian) Harga Jual atau, sesuai keadaan, bagian Investor tersebut dari Harga Jual. Kedua belah pihak mengakui bahwa pembebanan dan/atau penuntutan biaya pelunasan awal tidak sesuai dengan hukum Syariah.", $y + 10);

   $y = Right1($pdf, "<b>6. PEMBATALAN DENGAN SENDIRINYA</b>&nbsp;", $y + 15);
   $synp10 = $pdf->getPage();

   $y = Right1($pdf, "Apabila:", $y + 5);

   $y = Right1($pdf, "6.1", $y + 5);
   $y = Right2($pdf, "Perusahaan, sebagai agen Grup Investor, lalai dengan alasan apa pun untuk merampungkan pembelian Aset pada Tanggal Pembelian; atau", $y - 7);

   $y = Right1($pdf, "6.2", $y + 5);
   $y = Right2($pdf, "Perusahaan, sebagai agen Grup Investor, tidak menerima jumlah penuh atas Biaya Pembelian dari Grup Investor, pada selambat-lambatnya Tanggal Kontribusi Biaya Pembelian (kecuali jika Perusahaan memilih, menurut Pasal Pasal 1.6.6, untuk merampungkan pembelian Aset berdasarkan Biaya Pembelian yang Disesuaikan, dan selanjutnya merampungkan pembelian Aset tersebut pada Tanggal Pembelian);", $y - 7);

   $y = Right1($pdf, "6.3", $y + 5);
   $y = Right2($pdf, "sebelum perampungan pembelian Aset oleh Perusahaan, sebagai agen Grup Investor, terjadi suatu Peristiwa Wanprestasi atau hal tersebut menjadi melanggar hukum dalam yurisdiksi yang berlaku atau berdasarkan peraturan atau perundangan yang berlaku bagi Grup Investor atau Perusahaan, sebagai agen Grup Investor, untuk membeli Aset,", $y - 7);

   $y = Right2($pdf, "Para Pihak sepakat bahwa transaksi Murabahah yang dijelaskan dalam Perjanjian ini akan segera dan dengan sendirinya dibatalkan, dan Perusahaan akan segera mengembalikan kepada Grup Investor seluruh uang yang dipinjamkan atau dibayarkan kepada Perusahaan untuk pembelian Aset atas nama Grup Investor, dan selain itu akan memberikan ganti rugi kepada setiap Pemodal berkenaan dengan semua biaya dan pengeluaran sesungguhnya yang dikeluarkan oleh setiap Pemodal sehubungan dengan pengadaan Perjanjian ini dan peminjaman dana kepada Perusahaan.", $y + 5);

   $y = Right1($pdf, "<b>7. PERISTIWA WANPRESTASI</b>&nbsp;", $y + 15);
   $synp3 = $pdf->getPage();

   $y = Right1($pdf, "7.1", $y + 10);
   $y = Right2($pdf, "Peristiwa Wanprestasi dianggap telah terjadi dalam setiap keadaan berikut, baik ditimbulkan karena kelalaian Perusahaan atau tidak:-", $y - 7);

   $y = Right1($pdf, "7.1.1", $y + 3);
   $y = Right3($pdf, "Perusahaan lalai memenuhi setiap kewajiban pembayarannya berkenaan dengan Harga Jual, kecuali jika kelalaian tersebut adalah akibat kesalahan teknis atau administratif di pihak bank atau lembaga keuangan yang mengirimkan pembayaran oleh Perusahaan kepada Kapital Boost, atau oleh Kapital Boost kepada Pemodal; atau", $y - 7);

   $y = Right1($pdf, "7.1.2", $y + 3);
   $y = Right3($pdf, "Perusahaan lalai mematuhi dan melaksanakan setiap kewajibannya berdasarkan Perjanjian ini (termasuk, tidak terbatas (a) kewajibannya untuk menawarkan membeli Aset dari Grup Investor, dan (b) kewajiban-kewajibannya sebagai agen Grup Investor untuk membeli Aset dengan Biaya Pembelian dan menerima Penawaran Pembelian)); atau", $y - 7);

   $y = Right1($pdf, "7.1.3", $y + 3);
   $synp12 = $pdf->getPage();
   $y = Right3($pdf, "Pernyataan, keterangan atau jaminan yang diberikan atau dibuat oleh Perusahaan dalam Perjanjian ini terbukti tidak benar, tidak tepat atau tidak akurat atau menyesatkan dalam setiap hal materiil; atau", $y - 7);

   $y = Right1($pdf, "7.1.4", $y + 3);
   $y = Right3($pdf, "Diambil langkah apa pun untuk pembubaran, likuidasi atau penutupan Perusahaan;", $y - 7);

   $y = Right1($pdf, "7.1.5", $y + 3);
   $y = Right3($pdf, "Apabila terdapat unsur kecurangan, penyalahgunaan, penyelewengan  atau penggelapan Biaya Pembelian oleh Perusahaan.", $y - 7);

   $y = Right1($pdf, "7.2", $y + 5);
   $y = Right2($pdf, "Pada waktu terjadinya Peristiwa Wanprestasi, Pemodal dapat dengan pemberitahuan secara langsung, atau  melalui Kapital Boost sebagai agen Pemodal, kepada Perusahaan untuk:", $y - 7);

   $y = Right1($pdf, "7.2.1", $y + 3);
   $y = Right3($pdf, "menyatakan seluruh proporsi Harga Jual yang belum dibayarkan dari Perusahaan kepada Pemodal menjadi segera jatuh tempo dan wajib dibayarkan; dan/atau", $y - 7);

   $y = Right1($pdf, "7.2.2", $y + 3);
   $y = Right3($pdf, "mensyaratkan Perusahaan untuk (a) menjual Aset dan menggunakan seluruh hasil keuntungan penjualan untuk membayar tunggakan Harga Jual; dan (b) tidak melepaskan atau mengalihkan kepemilikan Aset kepada orang lain; dan/atau", $y - 7);

   $y = Right1($pdf, "7.2.3", $y + 3);
   $y = Right3($pdf, "mensyaratkan Perusahaan untuk memberikan ganti rugi kepada Grup Investor atas biaya, kerugian atau pengeluaran apa pun yang dikeluarkan sebagai akibat terjadinya Peristiwa Wanprestasi termasuk, tidak terbatas (a) kerugian laba oleh Pemodal yang timbul dari (i) kelalaian oleh Perusahaan untuk memenuhi janjinya memberikan penawaran untuk membeli Aset dari Grup Investor, atau (ii) kelalaian oleh Perusahaan sebagai agen Grup Investor untuk membeli Aset dengan Biaya Pembelian atau menerima Penawaran Pembelian, (b) biaya apa pun yang dikeluarkan dalam penguasaan secara fisik atas Aset setelah Peristiwa Wanprestasi, dan (c) pengurangan nilai Aset yang timbul dari pelanggaran oleh Perusahaan atas ketentuan Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "7.3", $y + 5);
   $y = Right2($pdf, "Hak Pemodal untuk mempercepat kewajiban-kewajiban Perusahaan sehubungan dengan porsi Harga Jual yang jatuh tempo kepada Pemodal dapat digunakan terlepas dari, dan tanpa bersyarat atas, tindakan atau tidak adanya tindakan dari pemodal yang lain atau yang sebagian besar dari Grup Investor. Pemodal berhak untuk mengajukan atau melaksanakan tindakan hukum atau tuntutan terhadap Perusahaan untuk memperoleh jumlah yang belum dibayarkan, dan Perusahaan sepakat untuk segera berdasarkan permintaan memberikan ganti rugi kepada Pemodal atas seluruh biaya, kerugian dan kewajiban sesungguhnya (termasuk biaya hukum) yang dikeluarkan atau ditanggung oleh Pemodal sebagai akibat Peristiwa Wanprestasi.", $y - 7);

   $y = Right1($pdf, "<b>8. PERNYATAAN</b>&nbsp;", $y + 15);
   $synp11 = $pdf->getPage();

   $y = Right1($pdf, "8.1", $y + 10);
   $y = Right2($pdf, "Perusahaan menyatakan kepada Pemodal pada tanggal Perjanjian ini bahwa:", $y - 7);

   $y = Right1($pdf, "8.1.1", $y + 3);
   $y = Right3($pdf, "Perusahaan resmi didirikan dan tunduk berdasarkan hukum negara Republik Indonesia;", $y - 7);

   $y = Right1($pdf, "8.1.2", $y + 3);
   $y = Right3($pdf, "Perusahaan memiliki kewenangan untuk menandatangani Perjanjian ini, dan untuk melaksanakan kewajiban-kewajibannya berdasarkan Perjanjian ini dan telah mengambil semua tindakan yang diperlukan untuk mengesahkan penandatanganan dan pelaksanaan tersebut;", $y - 7);

   $y = Right1($pdf, "8.1.3", $y + 3);
   $y = Right3($pdf, "Penandatanganan dan pelaksanaan Perjanjian ini tidak melanggar atau bertentangan dengan peraturan atau perundangan yang berlaku, ketentuan dokumen pendirian Perusahaan, dan perintah atau putusan pengadilan atau badan pemerintah yang lain yang berlaku bagi Perusahaan, atau pembatasan bersifat kontrak yang mengikat atas Perusahaan;", $y - 7);

   $y = Right1($pdf, "8.1.4", $y + 3);
   $y = Right3($pdf, "Seluruh izin yang disyaratkan untuk diperoleh Perusahaan sehubungan dengan Perjanjian ini telah diperoleh dan berlaku penuh;", $y - 7);

   $y = Right1($pdf, "8.1.5", $y + 3);
   $y = Right3($pdf, "Kewajiban-kewajiban Perusahaan berdasarkan Perjanjian ini merupakan kewajiban-kewajiban yang legal, sah dan mengikat, yang dapat diberlakukan menurut ketentuannya masing-masing;", $y - 7);

   $y = Right1($pdf, "8.1.6", $y + 3);
   $y = Right3($pdf, "Tidak ada Peristiwa Wanprestasi yang telah terjadi atau akan sewajarnya diperkirakan diakibatkan pengadaan atau pelaksanaan Perjanjian ini; dan", $y - 7);

   $y = Right1($pdf, "8.1.7", $y + 3);
   $y = Right3($pdf, "Usaha inti atau utama Perusahaan dan tujuan pembiayaan berdasarkan Perjanjian ini adalah dan pada setiap saat tetap sesuai Syariah.", $y - 7);

   $y = Right1($pdf, "8.2", $y + 5);
   $y = Right2($pdf, "Pernyataan dan jaminan dalam Pasal 6 ini diulangi oleh Perusahaan pada Tanggal Pembelian dan pada setiap tanggal yang dijadwalkan untuk pembayaran seluruh atau sebagian Harga Jual.", $y - 7);

   $y = Right1($pdf, "<b>9. BIAYA KETERLAMBATAN PEMBAYARAN (TA’WIDH DAN GHARAMAH)</b>&nbsp;", $y + 15);

   $y = Right1($pdf, "9.1", $y + 10);
   $y = Right2($pdf, "Apabila jumlah yang jatuh tempo dan wajib dibayarkan oleh Perusahaan berdasarkan atau sehubungan dengan Perjanjian ini belum dilunasi menurut Perjanjian ini (“jumlah yang belum dibayarkan”), Perusahaan berjanji untuk membayar biaya keterlambatan pembayaran termasuk ta’widh dan gharamah (dihitung di bawah ini) kepada Grup Investor pada setiap hari belum dibayarkannya jumlah yang belum dibayarkan.", $y - 7);

   $y = Right1($pdf, "9.2", $y + 5);
   $y = Right2($pdf, "Keseluruhan biaya keterlambatan pembayaran akan dipatok langsung pada laba rata-rata yang disetahunkan dari Perjanjian Murabahah ini (akan ditentukan oleh Perusahaan), yang dihitung setiap hari sebagaimana di bawah ini, dengan ketentuan bahwa akumulasi biaya keterlambatan pembayaran tidak melebihi 100% dari jumlah tunggakan yang belum dibayarkan:", $y - 7);

   $y = CenterR($pdf, "Jumlah yang Belum Dibayarkan X laba rata-rata yang disetahunkan X", $y + 5);
   $y = CenterR($pdf, "Jumlah hari yang lewat jatuh tempo  / 365", $y);

   $y = Right1($pdf, "9.3", $y + 5);
   $y = Right2($pdf, "Masing-masing Pemodal dapat menahan hanya bagian tertentu dari jumlah keterlambatan pembayaran yang dibayarkan kepadanya, sebagaimana diperlukan untuk memberikan penggantian kepada Pemodal atas setiap biaya sesungguhnya (tidak termasuk peluang atau biaya pendanaan), dengan tunduk pada dan menurut pedoman para penasihat Syariahnya sendiri, dan hingga jumlah keseluruhan maksimum 1% per tahun dari jumlah yang belum dibayarkan (“ta’widh”). ", $y - 7);

   $y = Right1($pdf, "9.4", $y + 5);
   $y = Right2($pdf, "Ta’widh sehubungan dengan jumlah yang belum dibayarkan, merupakan bagian dari biaya keterlambatan pembayaran, dapat timbul setiap hari dan dihitung menurut rumus berikut:-", $y - 7);

   $y = CenterR($pdf, "Biaya sesungguhnya yang dikeluarkan atau Jumlah yang Belum Dibayarkan X 1% X Jumlah hari yang lewat jatuh tempo / 365", $y + 3);

   $y = Right1($pdf, "9.5", $y + 5);
   $y = Right2($pdf, "Jumlah ta’widh tidak akan dilipatgandakan.", $y - 7);

   $y = Right1($pdf, "9.6", $y + 5);
   $y = Right2($pdf, "Grup Investor dengan ini menyepakati, berjanji dan menyanggupi untuk memastikan bahwa sisa biaya keterlambatan (“gharamah”), jika ada, akan dibayarkan kepada badan-badan amal yang dapat dipilih oleh Perusahaan dengan berkonsultasi dengan para penasihat Syariahnya.", $y - 7);

   $y = Right1($pdf, "<b>10. HUKUM YANG BERLAKU DAN PENYELESAIAN SENGKETA</b>&nbsp;", $y + 15);
   $synp4 = $pdf->getPage();

   $y = Right1($pdf, "10.1", $y + 10);
   $y = Right2($pdf, "Perjanjian ini dan hak-hak serta kewajiban Para Pihak berdasarkan Perjanjian ini diatur oleh dan diinterpretasikan serta ditafsirkan dalam semua hal menurut hukum negara Republik Indonesia tanpa mengurangi atau membatasi hak-hak atau upaya hukum lainnya yang tersedia bagi Para Pihak dalam Perjanjian ini berdasarkan hukum yurisdiksi di mana Perusahaan atau aset-asetnya mungkin ditempatkan.", $y - 7);

   $y = Right1($pdf, "10.2", $y + 5);
   $y = Right2($pdf, "Para Pihak dengan ini dengan tidak dapat dicabut kembali sepakat untuk tunduk pada yurisdiksi eksklusif Pengadilan Republik Indonesia untuk menyelesaikan sengketa atau mengajukan tuntutan berkenaan dengan Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "10.3", $y + 5);
   $y = Right2($pdf, "Para Pihak sepakat bahwa jika terdapat perbedaan pendapat, sengketa, pertentangan atau perdebatan <b>('Sengketa')</b>&nbsp;, yang timbul dari atau sehubungan dengan Perjanjian ini atau pelaksanaannya, termasuk tidak terbatas, sengketa apa pun mengenai keberadaan, keabsahan Perjanjian ini, atau pengakhiran hak-hak atau kewajiban Pihak manapun, Para Pihak akan berupaya selama jangka waktu 30 (tiga puluh) hari setelah diterimanya oleh salah satu pihak suatu pemberitahuan dari Pihak yang lain atas adanya Sengketa untuk menyelesaikan Sengketa tersebut dengan penyelesaian damai antara Para Pihak", $y - 7);

   $y = Right1($pdf, "10.4", $y + 5);
   $y = Right2($pdf, "Apabila Para Pihak tidak dapat mencapai kesepakatan untuk menyelesaikan Sengketa dalam jangka waktu 30 (tiga puluh) hari yang disebutkan dalam Pasal 10.3 di atas maka Para Pihak dengan ini tunduk pada yurisdiksi Kepaniteraan Pengadilan Agama Jakarta Pusat. Ketundukan tersebut tidak akan mempengaruhi hak-hak Pemodal untuk melakukan proses hukum, sejauh diizinkan berdasarkan hukum negara Indonesia, dalam yurisdiksi lain di dalam atau di luar Republik Indonesia maupun proses hukum di yurisdiksi manapun sesuai dengan peraturan perundangan yang berlaku.", $y - 7);

   $y = Right1($pdf, "<b>11. LAIN-LAIN</b>&nbsp;", $y + 15);
   $synp5 = $pdf->getPage();

   $y = Right1($pdf, "11.1", $y + 10);
   $y = Right21($pdf, "Apabila pada setiap saat satu ketentuan atau lebih dari Perjanjian ini adalah atau menjadi melanggar hukum, tidak sah atau tidak dapat diberlakukan berdasarkan hukum Republik Indonesia maka legalitas, keabsahan atau keberlakuan ketentuan-ketentuan lainnya dari Perjanjian ini atau legalitas, keabsahan atau keberlakuan ketentuan-ketentuan tersebut berdasarkan hukum yurisdiksi yang lain dengan cara apa pun tidak terpengaruh atau terganggu karena pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut.", $y - 7);

   $y = Right1($pdf, "11.2", $y + 5);
   $synp13 = $pdf->getPage();
   $y = Right21($pdf, "Apabila setiap ketentuan Perjanjian ini (atau bagian dari ketentuan tersebut) atau pemberlakuannya terhadap orang manapun atau keadaan apa pun melanggar hukum, tidak sah atau tidak dapat diberlakukan dalam hal apa pun maka harus ditafsirkan secara sempit sebagaimana diperlukan untuk mengizinkannya dapat diberlakukan atau sah dan ketentuan yang lain dari Perjanjian ini dan legalitas, keabsahan atau keberlakuan ketentuan-ketentuan tersebut terhadap orang-orang atau keadaan lain tidak akan dengan cara apa pun terpengaruh atau terganggu karena pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut dan akan dapat diberlakukan/dilaksanakan sejauh mungkin yang diizinkan berdasarkan hukum.", $y - 7);

   $y = Right1($pdf, "11.3", $y + 5);
   $synp9 = $pdf->getPage();
   $y = Right21($pdf, "Seluruh hak dan kewajiban dalam Perjanjian ini bersifat pribadi terhadap Para Pihak dan masing-masing Pihak dalam Perjanjian ini tidak boleh mengalihkan dan/atau memindahtangankan setiap hak dan kewajiban tersebut kepada pihak ketiga tanpa izin tertulis terlebih dahulu dari Para Pihak.", $y - 7);

   $y = Right1($pdf, "11.4", $y + 5);
   $synp15 = $pdf->getPage();
   $y = Right21($pdf, "Perjanjian ini memuat keseluruhan kesepahaman antara Para Pihak terkait dengan transaksi yang dimaksud oleh Perjanjian ini dan akan menggantikan setiap pernyataan minat atau kesepahaman sehubungan dengan transaksi tersebut. Seluruh perjanjian, kesepahaman, pernyataan dan keterangan sebelumnya atau yang bersamaan, secara lisan dan tertulis, digabungkan dalam Perjanjian ini dan tidak lagi berkekuatan atau berlaku selanjutnya.", $y - 7);

   $y = Right1($pdf, "11.5", $y + 5);
   $synp16 = $pdf->getPage();
   $y = Right21($pdf, "Setiap komunikasi yang akan dilakukan berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan secara tertulis dan, dapat dilakukan dengan surat atau surat elektronik, dalam masing-masing hal, melalui Kapital Boost yang beralamat di <b>116 Changi Road, #05-00, Singapore 419718</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; atau di alamat tertentu lainnya yang dapat diberitahukan oleh Kapital Boost kepada para pihak dari waktu ke waktu. Setiap komunikasi yang dilakukan antara Kapital Boost dan setiap Pihak berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan ke alamat atau alamat surat elektronik yang diberikan kepada Kapital Boost atau alamat terdaftarnya, dalam hal Perusahaan, dan berlaku apabila diterima.", $y - 7);

   $y = Right1($pdf, "11.6", $y + 5);
   $synp17 = $pdf->getPage();
   $y = Right21($pdf, "Tidak ada kelalaian untuk melaksanakan, atau setiap keterlambatan dalam pelaksanaan, di pihak Pemodal, hak atau upaya hukum apa pun berdasarkan Perjanjian ini yang dianggap sebagai pengesampingan, demikian juga pelaksanaan tunggal atau sebagian atas hak atau upaya hukum apa pun tidak akan menghalangi pelaksanaan selanjutnya atau yang lain atau pelaksanaan hak atau upaya hukum yang lain. Hak-hak dan upaya hukum berdasarkan Perjanjian ini bersifat kumulatif dan tidak terpisah dari hak atau upaya hukum yang diatur oleh hukum.", $y - 7);

   $y = Right1($pdf, "11.7", $y + 5);
   $synp18 = $pdf->getPage();
   $y = Right21($pdf, "Tidak ada ketentuan dari Perjanjian ini yang dapat diubah, dikesampingkan, dilepaskan atau dihentikan secara lisan atau demikian juga tidak ada pelanggaran atau wanprestasi berdasarkan setiap ketentuan Perjanjian ini yang dapat dikesampingkan atau dilepaskan secara lisan tetapi (dalam masing-masing hal) kecuali (a) sesuai dengan Pemberitahuan Penyesuaian yang memenuhi persyaratan Perjanjian ini; atau (b) dengan instrumen tertulis yang ditandatangani oleh atau atas nama Para Pihak atau (b). Setiap amendemen atau perubahan pada Perjanjian ini harus sesuai Syariah.", $y - 7);

   $y = Right1($pdf, "11.8", $y + 5);
   $synp19 = $pdf->getPage();
   $y = Right21($pdf, "Perjanjian ini dapat ditandatangani dalam berapa pun jumlah rangkap, dan memiliki keberlakuan yang sama seolah-olah tanda tangan-tanda tangan pada rangkap tersebut terdapat pada salinan tunggal dari Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "11.9", $y + 5);
   $synp20 = $pdf->getPage();
   $y = Right21($pdf, "Masing-masing Pihak sepakat untuk merahasiakan seluruh informasi berkaitan dengan Perjanjian ini, dan tidak mengungkapkannya kepada siapa pun, kecuali dengan izin tertulis terlebih dahulu dari Para Pihak yang lain atau sebagaimana disyaratkan oleh peraturan atau perundangan yang berlaku.", $y - 7);

   $y = Right1($pdf, "11.10", $y + 5);
   $synp21 = $pdf->getPage();
   $y = Right21($pdf, "Perjanjian ini dimaksudkan untuk sesuai dengan Syariah. Para Pihak dengan ini menyepakati dan mengakui bahwa masing-masing hak dan kewajiban mereka berdasarkan Perjanjian ini dimaksudkan untuk, dan akan, sesuai dengan prinsip-prinsip Syariah.", $y - 7);

   $y = Right1($pdf, "11.11", $y + 5);
   $y = Right21($pdf, "Meskipun terdapat hal di atas, masing-masing pihak menyatakan kepada yang lain bahwa tidak akan mengajukan keberatan atau tuntutan terhadap yang lain berdasarkan kepatuhan pada Syariah atau pelanggaran prinsip-prinsip Syariah sehubungan atau yang terkait dengan bagian manapun dari setiap ketentuan Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "<b>12. DEFINISI</b>&nbsp;", $y + 15);

   $y = Right1($pdf, "Dalam Perjanjian ini, rujukan pada:", $y + 10);

   $y = Right1($pdf, "12.1", $y + 5);
   $y = Right21($pdf, "Pihak manapun atau Kapital Boost akan ditafsirkan sehingga mencakup para penerusnya yang berhak, para penerima pengalihan resmi dan para penerima pemindahtanganan resminya;", $y - 7);

   $y = Right1($pdf, "12.2", $y + 5);
   $y = Right21($pdf, "ketentuan hukum adalah rujukan pada ketentuan tersebut sebagaimana diubah atau diberlakukan kembali;", $y - 7);

   $y = Right1($pdf, "12.3", $y + 5);
   $y = Right21($pdf, "orang atau Pihak dengan jenis kelamin tertentu mencakup orang atau Pihak tersebut terlepas dari apa pun jenis kelaminnya, dan akan berlaku sama apabila orang atau Pihak tersebut adalah badan perusahaan, firma, peramanatan (trust), usaha patungan atau persekutuan;", $y - 7);

   $y = Right1($pdf, "12.4", $y + 5);
   $y = Right21($pdf, "the \"<b>Grup Investor</b>&nbsp;&nbsp; \" ditafsirkan bermakna sekadar Pemodal jika Pemodal membiayai keseluruhan Biaya Pembelian;", $y - 7);

   $y = Right1($pdf, "12.5", $y + 5);
   $y = Right21($pdf, "\"<b>Kapital Boost</b>&nbsp;&nbsp; \" adalah <b>Kapital Boost Pte Ltd, No. Pendaftaran Perusahaan Singapura 201525866W dan PT Kapital Boost Indonesia, yang terdaftar di hadapan Notaris Panji Kresna S.H., M.Kn. pada tanggal 23 Maret 2016</b>&nbsp;", $y - 7);

   $y = Right1($pdf, "12.6", $y + 5);
   $y = Right21($pdf, "\"<b>Harga Jual</b>&nbsp;&nbsp; \" adalah Harga Jual sebagaimana diubah menurut Pasal 1.6.6.", $y - 7);



   if ($y > 160) { // Shift the sign table below
      $pdf->AddPage();
      $y = 0;
   }
   $synp100 = $pdf->getPage();







   /* ---------- END of Bahasa Version -------------- */



   $pdf->SetPage(1);
   $y = 19;

   // $y = CenterL($pdf, "DATED THIS DAY $date", $y);

   // $pdf->SetFontSize("25");
   // $y = CenterL($pdf, ". . . . . . . . . . . . . . . .", $y);

   // $pdf->SetFontSize("14");
   // $y = CenterL($pdf, "MURABAHAH AGREEMENT", $y, true);

   // $pdf->SetFontSize("25");
   // $y = CenterL($pdf, ". . . . . . . . . . . . . . . .", $y - 6);
   // $pdf->SetFontSize("12");

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, "_______________________ <br>", $y);

   $pdf->SetFontSize("12");
   $y = CenterL($pdf, "<b>&nbsp;&nbsp;&nbsp;&nbsp;MURABAHAH AGREEMENT NO. M$invoContractNo FInancing Agreement Based on the Shariah Principle of Murabahah</b>&nbsp;&nbsp;
", $y);

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, " <br> _______________________", $y - 6);
   $pdf->SetFontSize("11.5");

   $y = Left1invo($pdf, "<b>THIS MURABAHAH AGREEMENT NO. M$invoContractNo (“Agreement”)</b>&nbsp;&nbsp;  is made on <b>$date</b>&nbsp;&nbsp;,", $y + 5);


   $y = Left1($pdf, "<b>BETWEEN:</b>  ", $y + 5);

   //$y = Left1($pdf, "<b>1. $companyName</b>&nbsp;", $y + 5);

   //$y = Left1($pdf, "1.	<b>$companyName</b>&nbsp;<br>a limited liability company duly established, and validly existing under the laws of the Republic of Indonesia, under Deed of Establishment Number $invoAktaNo with $companyRegistration and domiciled in Indonesia (<b>the “Borrower”</b>&nbsp;&nbsp;&nbsp; );", $y + 5);
   $y = Left1($pdf, "1.	<b>$companyName</b>&nbsp;<br>a limited liability company duly established, and validly existing under the laws of the Republic of Indonesia, under Deed of Establishment Number $invoAktaNo with $comRegTypeEngText $companyRegistration and domiciled in Indonesia", $y + 5);

   //$y = Left1($pdf, "($companyRegistration)", $y);

   $y = Left1($pdf, "(hereinafter referred to as \"<b>the Company</b>&nbsp;  \")", $y + 5);

   $y = Left1($pdf, "<b>AND</b>&nbsp;", $y + 5);
$invIC = str_replace(" IC "," Identity Card ",$invIC);
   $y = Left1($pdf, "<b>2. $invName</b>&nbsp;<br>($invIC)", $y + 5);

   $y = Left1($pdf, "(hereinafter referred to as \"<b>the Financier</b>&nbsp;  \")", $y + 5);

   $y = Left1($pdf, "<b>WHEREAS:</b>&nbsp;", $y + 5);

   $y = Left1($pdf, "A.", $y + 5);

   $y = Left2($pdf, "The Company is in need of funding for the purchase, acquisition or procurement of $purchaseAsset, as described in Schedule A (“<b>the Asset</b>&nbsp; ”), which it intends to utilize for its business, and the Company wishes to obtain such financing in accordance with Shariah principles;", $y - 7);

   $y = Left1($pdf, "B.", $y + 5);

   $y = Left2($pdf, "The Financier, together with certain other financiers (together, “<b>the Investors Group</b>&nbsp; ”) have agreed to provide the required financing to the Company based on the Shariah principle of Murabahah, specifically murabahah to the purchase orderer; and", $y - 7);

   $y = Left1($pdf, "C.", $y + 5);

   $y = Left2($pdf, "The Financier has agreed to provide his share of the required financing based on the terms and subject to the conditions of this Agreement.", $y - 7);

   $y = Left1($pdf, "<b>NOW IT IS HEREBY AGREED</b>&nbsp; as follows:", $y + 10);
   $y = Left1($pdf, "<b>1. METHOD OF FINANCING</b>&nbsp;", $y + 10);

   $y = Left1($pdf, "1.1", $y);

   $y = Left2($pdf, "For the purpose of financing the acquisition of the Asset, the Company hereby requests that the Investors Group collectively purchases the Asset from the Supplier for a total consideration of <b>$assetCost</b>&nbsp; (\"<b>the Purchase Cost</b>&nbsp;&nbsp;&nbsp;\"), and thereafter sell the Asset to the Company at the sale price of <b>$salePrice</b>&nbsp; (\"<b>the Sale Price</b>&nbsp; \"). The actual cost to the Company of purchasing the Asset is <b>$assetCostCurrency</b>&nbsp; (\"<b>Purchase Currency Cost </b>&nbsp;&nbsp; \"), which currency can be purchased using the Singapore Dollar amount of the Purchase Cost at the prevailing currency exchange rate of S$1:$exchange . The Company undertakes (via wa’d) to purchase the Asset from the Investors Group, following the purchase of the Asset by the Investors Group in accordance with the terms of this Agreement. The Murabahah arrangement set out above will result in profit for the Investors Group as set out below.", $y - 7);


   $html = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td><b>Purchase Cost of the Asset payable by the Investors Group</b>&nbsp;</td>
  <td><b>Sale Price payable by the Company to the Investors Group</b>&nbsp;</td>
  <td><b>Amount of Profit derived by the Investors Group</b>&nbsp;</td>
  </tr>
  <tr>
  <td>$assetCost</td>
  <td>$salePrice</td>
  <td>$percent_assetCost (or $return% of the Purchase Cost of Asset)</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetFontSize(10);
   $pdf->writeHTML($html);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Left1($pdf, "1.2", $y);

   $y = Left2($pdf, "The parties agree that the Purchase Cost refers to the actual acquisition costs of the Asset incurred or to be incurred by the Investors Group or its agent for the acquisition of the Asset, excluding any direct expenses (such as delivery, transportation, storage and assembly costs, taxes, takaful and insurance coverage costs) and any overhead expenditures or indirect costs (such as staff wages and labor charges).", $y - 7);

   $y = Left1($pdf, "1.3", $y + 5);

   $y = Left2($pdf, "The Financier is willing to finance exactly $invAmount (being $invBack%) out of the entire Purchase Cost amount. Such amount to be financed by the Financier will be paid by Financier to the Supplier, through the Company as agent. For the purposes of enabling Kapital Boost, as agent of the Company under Clause 3.4.1, to transfer the Purchase Cost amount to the Company by no later than $closdate (the \"<b>Purchase Cost Contribution Date</b>&nbsp;&nbsp;&nbsp;&nbsp; \"), the Financier shall transfer the amount of its contribution to the Purchase Cost (as specified in this clause above) to the account specified in writing by Kapital Boost, by no later than <b>three (3) working days</b>&nbsp;&nbsp;&nbsp;&nbsp; from the date of this Agreement. ", $y - 7);

   $y = Left1($pdf, "1.4", $y + 5);

   $y = Left2($pdf, "Based on the Financier’s contribution to the Purchase Cost as specified above, the Financier will own $invBack% of the Asset upon purchase of the same by the Investors Group, and the Financier will be entitled to $invBack% share of the Sale Price and each installment or part thereof paid by the Company, amounting to:-", $y - 7);



   BulletL($pdf, $y);

   $y = Left3($pdf, "Financier’s portion of the Sale Price: SGD$expect_payout", $y + 3);

   BulletL($pdf, $y);

   $y = Left3($pdf, "Financier's portion of the Profit: SGD$percent_return ", $y + 3);

   $y = Left1($pdf, "1.5", $y + 5);



   $y = Left2($pdf, "The Financier (as part of the Investors Group) hereby appoints the Company as his agent to purchase the Asset on its behalf. The Company hereby accepts such appointment as agent, and will purchase and handle the Asset in accordance with the terms of this Agreement. The Company, as agent, shall purchase the Asset in the currency of the Purchase Currency Cost, for a consideration not exceeding the Purchase Currency Cost, and by no later than  <b>$cCloseDate1</b>&nbsp;&nbsp;(\"<b>Date of Purchase</b>&nbsp;&nbsp; \"). ", $y - 7);

   $y = Left1($pdf, "1.6", $y + 5);

// star here

   $y = Left2($pdf, "The Financier and the Company (as agent of the Investors Group for the purposes of the purchase of the Asset) hereby acknowledge and mutually agree that:", $y - 7);

   $y = Left1($pdf, "1.6.1", $y + 3);

   $y = Left3($pdf, "the Investors Group (including the Financier) has appointed the Company to act as agent of the Investors Group to purchase the Asset on behalf of the Investors Group because of the Company’s market knowledge and networks in relation to the Asset;", $y - 7);

   $y = Left1($pdf, "1.6.2", $y + 3);

   $y = Left3($pdf, "the purchase of the Asset by the Investors Group through the Company as agent may result in benefits to the Investors Group through efficiency of the purchase process and also cost savings in the event that the Company is able to secure or source the Asset at a price lower than the Purchase Currency Cost;", $y - 7);

   $y = Left1($pdf, "1.6.3", $y + 3);

   $y = Left3($pdf, "the event that the Company, as agent, is able to purchase the Asset at lower than the Purchase Currency Cost, the Investors Group hereby agrees to waive, by way of tanazul (i.e. waiver), its rights to claim the excess unutilized funding amount; and the Company, as agent, is entitled to retain the benefit of any such cost savings as incentive based on hibah for its good work performance;", $y - 7);

   $y = Left1($pdf, "1.6.4", $y + 3);

   $y = Left3($pdf, "in the circumstances described in Clause 1.6.3, the cost to the Investors Group of purchasing the Asset would be the Purchase Cost, as the hibah amount granted to the Company, as agent, would constitute part of the actual cost of purchasing the Asset, and accordingly the same Purchase Cost will be reflected in the Murabahah Sale Contract to be entered into by the Investors Group in the sale of the Asset;", $y - 7);

//    $pdf->setPage($synp0);
//    $y = 10;
   $y = Left1($pdf, "1.6.5", $y);

   $y = Left3($pdf, "in the event that the Company is unable to purchase sufficient $foreignCurrency to constitute the Purchase Currency Cost using the Purchase Cost contributed by the Investors Group as a result of an decrease in the currency exchange rate from that stated in Clause 1.1:", $y - 7);

   $y = Left3($pdf, "(a)", $y + 2);

   $y = Left4($pdf, "the Company, as agent of the Investors Group, shall either:", $y - 7);

   $y = Left4($pdf, "(i)", $y);

   $y = Left5($pdf, "promptly notify the Investors Group and return the Purchase Cost"
           . " to the Investors Group, following which this Agreement shall terminate; or", $y - 7);

   $y = Left4($pdf, "(ii)", $y);

   $y = Left5($pdf, "fund the shortfall amount in the currency of the Purchase Cost (the \"<b>Purchase Cost Shortfall Amount</b>&nbsp;&nbsp;&nbsp; \") which would be required in order for the Company to purchase the Purchase Currency Cost in order to complete the purchase of the Asset as agent on behalf of the Investors Group;", $y - 7);

   $y = Left3($pdf, "(b)", $y + 3);

   $y = Left4($pdf, "following the funding of the Purchase Cost Shortfall Amount by the Company and the completion of the purchase of the Asset by the Company, as agent of the Investors Group:", $y - 7);

   $y = Left4($pdf, "(i)", $y);
   $y = Left5($pdf, "the Purchase Cost Shortfall Amount shall constitute a debt owed by the Investors Group to the Company, in its capacity as agent for the Investors Group;", $y - 7);

   $y = Left4($pdf, "(ii)", $y);
   $y = Left5($pdf, "notwithstanding paragraph (i) above, the Company shall not, prior to the transfer of title to the Asset pursuant to Clause 1.8, have any claim to any part of portion of the title to the Assets;", $y - 7);

   $y = Left4($pdf, "(iii)", $y);
   $y = Left5($pdf, "the Sale Price (or, if there is more than one installment of the Sale Price, then the first installment thereof) shall be increased by the Purchase Cost Shortfall Amount, which will be reflected in the Company’s Offer to Purchase delivered by the Company to the Investors Group; and", $y - 7);

   $y = Left4($pdf, "(iv)", $y);
   $y = Left5($pdf, "the Investors Group, the Company (in its capacity as agent of the Investors Group) and as purchaser of the Asset, all agree that the Purchase Cost Shortfall Amount owing by the Company to the Investors Group (as part of the Sale Price) shall be automatically set off against the same amount owing by the Investors Group to the Company (pursuant to paragraph (i) above), and neither the Financier nor the Company shall make any claim with respect to payment of such amount by the other party.", $y - 7);

   $y = Left1($pdf, "1.6.6", $y + 5);
   $y = Left3($pdf, "In the event that on the Purchase Cost Contribution Date, the aggregate amount of the contributions from the Investors Group towards the Purchase Cost is lower than the quantum of the Purchase Cost specified in Clause 1.1, but higher than 50% of such quantum, Kapital Boost (as agent acting on behalf, and at the instruction, of the Company) shall promptly notify the Investors Group of the intention of the Company to (i) continue with the purchase of the Asset in reduced quantity for a total consideration equal to the aggregate amount of the contributions from the Investors Group (the \"<b>Adjusted Purchase Cost</b>&nbsp;&nbsp;&nbsp;\"); or otherwise (ii) return the contributions to the contributing members of the Investors Groups pursuant to Clause 6.2.; and", $y - 7);

   $y = Left3($pdf, "(a)", $y + 2);
   $y = Left4($pdf, "following the Company’s election pursuant to Clause 1.6.6 (i):", $y - 7);

   $y = Left4($pdf, "(1)", $y + 3);
   $y = Left5($pdf, "the Company (as agent of Investors Group) will complete the purchase of the Asset by the Date of Purchase;", $y - 7);

   $y = Left4($pdf, "(2)", $y + 3);
   $y = Left5($pdf, "this Agreement shall be read to interpret the Purchase Cost as the Adjusted Purchase Cost, and all other relevant provisions of this Agreement shall be adjusted accordingly including: (i) a proportional reduction to the Purchase Currency Cost and the Sale Price; and (ii) a proportional increase in the percentage of each contributing Financier’s ownership in the Asset, each as specified in the notice delivered pursuant to Clause 1.6.6(i) (the \"<b>Adjustment Notice</b>&nbsp;&nbsp;\"); and", $y - 7);

   $y = Left4($pdf, "(3)", $y + 3);
   $y = Left5($pdf, "there will not be any change to (i) the quantum of each contributing Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price; and", $y - 7);

   $y = Left3($pdf, "(b)", $y + 3);
   $y = Left4($pdf, "upon delivery by Kapital Boost to the Investors Group of an Adjustment Notice which complies with the requirements of this Agreement, each of the contributing Financiers shall be deemed to have agreed to such adjustments and the Adjustment Notice shall constitute an effective and binding amendment to this Agreement.", $y - 7);

   $pdf->setPage($synp6);
   $y = 10;
   $y = Left1($pdf, "1.6.7", $y);
   $y = Left3($pdf, "the circumstance described in 1.6.6, the Company will proceed to purchase the Asset, in reduced quantity for a total consideration equal to the aggregate contribution from consenting Financiers (\"Adjusted Purchase Cost\"), provided that the amount is more than 50% of the Purchase Cost.", $y - 7);

   $y = Left1($pdf, "1.7", $y + 5);
   $y = Left2($pdf, "The Company, as agent of the Investors Group, undertakes to inspect the Asset prior to delivery / collection, and shall ensure that all specifications in relation to the Asset as specified in Schedule A shall be complied with. The Company undertakes to immediately indemnify the Investors Group for any losses or liabilities resulting directly or indirectly from the failure of the Company to so inspect, and ensure the fitness for purpose or compliance with specifications of, the Asset.", $y - 7);

   $y = Left1($pdf, "1.8", $y + 5);
   $y = Left2($pdf, "Following the purchase of the Asset by the Company, as agent of the Investors Group, the Company will offer to purchase the Asset (\"<b>Company's Offer to Purchase</b>&nbsp;&nbsp;&nbsp;\") from the Investors Group, for a consideration equal to the Sale Price. The sale of the Asset by the Investors Group to the Company will take effect upon the acceptance of the Company’s Offer to Purchase by Kapital Boost, acting as agent of the Investors Group, in the form of Schedule B. The Sale Price will be paid by the Company to the Investors Group on deferred payment terms as provided in Clause 2 below. The Parties hereby agree that title to the Asset will pass immediately and fully from the Investors Group to the Company upon acceptance of the Company’s Offer to Purchase, in the form of Schedule B.", $y - 7);


   $y = Left1($pdf, "<b>2. PAYMENT OF SALE PRICE</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "2.1", $y + 10);
   $y = Left2($pdf, "The Sale Price will be paid by the Company as follows:-", $y - 7);

   $payoutString = "";

   print_r($dateA);
   exit(0);

   for ($i = 0; $i < 12; $i++) {
	  $newDate = date("d F Y", strtotime($dateA[$i]));
	  $percentProfit = $amountA[$i];
	  $marginMonthly = 0;
	  if ($percentProfit!="" or $percentProfit!=null) {
		  $marginTotal = ($return/100)*$totalFundingAmt;
		  // echo "$marginTotal+$totalFundingAmt";
		  $totalAmount = $marginTotal+$totalFundingAmt;
		  // echo "($percentProfit/100)*$totalAmount";
		  // echo "$percentProfit/100 <br />";
		  $percentageTotalAmt = $percentProfit/100;
		  // echo "$marginMonthly = $percentProfit*$totalAmount*10;  <br />";
		  $marginMonthly = $percentProfit*$totalAmount/100;
		  $payoutString .= "<tr><td style=\"text-align:center;height:18px\"> $newDate </td><td style=\"text-align:center;height:18px\">SGD ".number_format($marginMonthly,2)."</td></tr>";
	  }



   }

   $payoutTbl = "<table cellspacing=\"0\" cellpadding=\"3\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b>&nbsp;</td>
  <td style=\"text-align:center\"><b>Sale Price payable</b>&nbsp;</td>
  </tr>
  $payoutString
  </table>
  ";
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutTbl);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();
   $y = Left1($pdf, "2.2", $y - 5);
   $y = Left2($pdf, "All payments hereunder by the Financier and the Company shall be made in full in the currency of the Sale Price stated in this Agreement, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority.", $y - 7);

   $y = Left1($pdf, "2.3", $y + 5);
   $y = Left2($pdf, "All payments of the Sale Price or any installments thereof shall be made to each financier within the Investors Group pari passu, based on the percentage contribution to the Purchase Price as described above. The Parties agree that in the event any such payment is not made pari passu, the Parties shall make the necessary payments as between the Investors Group to rectify any over/under payment of the Sale Price.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp7) {
      $pdf->setPage($synp7);
      $y = -5;
   }

   $y = Left1($pdf, "3. <b>KAPITAL BOOST AS AGENT</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "3.1", $y + 10);
   $y = Left2($pdf, "The Financier acknowledges that the Company has appointed, or is hereby deemed to appoint, Kapital Boost as its agent for the purposes of (a) collecting the Purchase Cost from the Investors Group, for payment to the Supplier through the Company; (b) notifying the Investors Group of the election made by the Company pursuant to Clause 1.6.5; and (c) distributing to the Investors Group monies comprising the Sale Price or installments thereof due and payable by the Company. ", $y - 7);

   $y = Left1($pdf, "3.2", $y + 5);
   $y = Left2($pdf, "The Investors Group furthermore hereby (by the electronic delivery of this Agreement to Kapital Boost) appoints Kapital Boost to accept and sign the Murabahah Sale Contract per Schedule B of this Agreement. ", $y - 7);

   $y = Left1($pdf, "3.3", $y + 5);
   $y = Left2($pdf, "The Company acknowledges that the Financier has appointed, or is deemed to appoint, Kapital Boost as its agent for the purposes of (a) collecting the Sale Price that is due and payable by the Company to the Investors Group and (b) commencing legal proceeding upon the occurrence of an Event of Default against the Company provided that Kapital Boost has received sufficient funds from the Financier to fund costs related to such legal proceedings. ", $y - 7);

   $y = Left1($pdf, "3.4", $y + 5);
   $y = Left2($pdf, "For the purposes of the agency under Clause 3.1, 3.2 and 3.3:", $y - 7);

   $y = Left1($pdf, "3.4.1", $y + 3);
   $y = Left2($pdf, "Kapital Boost shall (a) transfer to the Company by no later the Purchase Cost Contribution Date, the Purchase Cost received by it from the Investors Group; and (b) upon receipt of any amount of the Sale Price due from the Company to any member of the Investors Group on any particular date, promptly distribute to each financier within the Investors Group its proportion of the amount received, in each case, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority;", $y - 7);

   $y = Left1($pdf, "3.4.2", $y + 3);
   $y = Left2($pdf, "Kapital Boost shall ensure that the   following terms are included in the Adjustment Notice:", $y - 7);

   $y = Left2($pdf, "(a)", $y);
   $y = Left3($pdf, "the Adjusted Purchase Price; ", $y - 7);

   $y = Left2($pdf, "(b)", $y);
   $y = Left3($pdf, "the proportionally reduced Purchase Currency Cost and Sale Price;", $y - 7);

   $y = Left2($pdf, "(c)", $y);
   $y = Left3($pdf, "the proportionally increased percentage of the Financier’s ownership in the Asset; and", $y - 7);

   $y = Left2($pdf, "(d)", $y);
   $y = Left3($pdf, "express confirmation that there is no change to (i) the quantum of the Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price.", $y - 7);

   $y = Left1($pdf, "3.4.3", $y + 5);
   $y = Left2($pdf, "prior to signing the Murabahah Sale Contract as agent of the Investors Group, Kapital Boost shall check the terms of the same to ensure compliance with the terms of this Agreement;", $y - 7);

   $y = Left1($pdf, "3.4.4", $y + 5);
   $y = Left2($pdf, "Kapital Boost shall inform the Investors Group promptly upon becoming aware of any delay in the Company delivering the Murabahah Sale Contract, any deficiency in the Murabahah Sale Contract or any other reason which would result in Kapital Boost not being able to, or preferring not to, sign the Murabahah Sale Contract;", $y - 7);

   $y = Left1($pdf, "3.4.5", $y + 5);
   $y = Left2($pdf, "Kapital Boost will endeavor to take all necessary measures to recover the outstanding payments of all or any part of the Sale Price from the Company;", $y - 7);

   $y = Left1($pdf, "3.4.6", $y + 5);
   $y = Left2($pdf, "Kapital Boost shall provide to the Investors Group with fee quotations that includes legal counsel fees and other costs to be incurred by Kapital Boost as agent. Kapital Boost shall proceed to instruct the legal counsel to take legal action, as advised by such legal counsel, against the Company provided that the Financier has transferred to Kapital Boost the funds needed to cover legal costs. Such costs incurred shall be indemnified in full by the Company without prejudice to Clause 7.3;", $y - 7);

   $y = Left1($pdf, "3.4.7", $y + 5);
   $y = Left2($pdf, "Kapital Boost shall not be responsible or liable for any default or shortfall in the payment by the Company of all or any part of the Sale Price due to the Investors Group at any time; ", $y - 7);

   $y = Left1($pdf, "3.4.8", $y + 5);
   $y = Left2($pdf, "by taking any of the actions described in this Clause 3.4 as agent of the Investors Group, Kapital Boost is deemed to have accepted its appointment as such agent, and to have agreed to be bound by the terms of this Clause 3.4.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp2) {
      $pdf->setPage($synp2);
      $y = -5;
   }
   $y = Left1($pdf, "<b>4. TRANSFER OF OWNERSHIP & WARRANTY</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "4.1", $y + 10);
   $y = Left2($pdf, "The Company agrees to purchase the Asset on an as-is, where-is basis, and is deemed to have inspected the Asset prior to purchase thereof. The title and ownership of the Asset together with all rights, interest and benefits attached thereto shall pass from the Investors Group to the Company immediately upon the sale of the Asset by the Investors Group to the Company pursuant to the terms of this Agreement, notwithstanding that the Sale Price has not been paid or fully paid by the Company to the Investors Group. The Company waives all claims against the Investors Group as seller for breach in warranty or defects in respect of the Asset. The Company shall immediately at the point of purchase, collect and take possession of the Asset from the Investors Group or its agent(s), if any.", $y - 7);

   $y = Left1($pdf, "4.2", $y + 5);
   $y = Left2($pdf, "The Investors Group assigns to the Company its warranty and other rights against the supplier / seller of the Assets at the point of purchase by the Company.", $y - 7);

   $y = Left1($pdf, "<b>5. EARLY PAYMENT OF SALE PRICE</b>&nbsp;", $y + 15);

   $y = Left2($pdf, "The Company may opt at its discretion to effect early payment of all or any part of the Sale Price. Any early payment of part of the Sale Price shall reduce the Sale Price payment obligations of the Company in inverse chronological order, and any such early payment of any part of the Sale Price shall not delay, reduce or otherwise affect the remaining payment obligations of the Company in respect of the Sale Price. Following any early payment of any part of the Sale Price, the Investors Group or any individual Investor may at their or his absolute discretion grant ibra’ (rebate) on any remaining part(s) of the Sale Price or, as the case may be, that Investor’s share of the Sale Price. Both parties acknowledge that charging and/or claiming early settlement charges is not in accordance with Shariah law.", $y + 10);

   $pg = $pdf->getPage();
   if ($pg < $synp10) {
      $pdf->setPage($synp10);
      $y = -5;
   }
   $y = Left1($pdf, "<b>6. AUTOMATIC CANCELLATION</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "If:", $y + 5);

   $y = Left1($pdf, "6.1", $y + 5);
   $y = Left2($pdf, "the Company, as agent of the Investors Group, fails for whatever reason to complete the purchase of the Asset by the Date of Purchase; or", $y - 7);

   $y = Left1($pdf, "6.2", $y + 5);
   $y = Left2($pdf, "the Company, as agent of the Investors Group, does not receive the full amount of the Purchase Cost from the Investors Group, by no later than the Purchase Cost Contribution Date (unless the Company elects, pursuant to Clause 1.6.6, to complete the purchase of the Asset based on the Adjusted Purchase Cost, and thereafter completes such purchase of the Asset by the Date of Purchase); or", $y - 7);

   $y = Left1($pdf, "6.3", $y + 5);
   $y = Left2($pdf, "prior to the completion of the purchase of the Asset by the Company, as agent of the Investors Group, an Event of Default occurs or it becomes illegal in any applicable jurisdiction or under any applicable laws or regulations for the Investors Group or the Company, as agent of the Investors Group, to purchase the Asset,", $y - 7);

   $y = Left2($pdf, "the Parties agree that the Murabahah transaction described in this Agreement shall be immediately and automatically cancelled, and the Company shall immediately refund the Investors Group all monies advanced or paid to it for the purchase of the Asset on behalf of the Investors Group, and will in addition indemnify each Financer in respect of all actual costs and expenses incurred by him in relation to the entry into this Agreement and the advance of funds to the Company.", $y + 5);

   $pg = $pdf->getPage();
   if ($pg < $synp3) {
      $pdf->setPage($synp3);
      $y = -5;
   }
   $y = Left1($pdf, "<b>7. EVENTS OF DEFAULT</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "7.1", $y + 10);
   $y = Left2($pdf, "An Event of Default is deemed to have occurred in any of the following circumstances, whether or not arising due to the fault of the Company:-", $y - 7);

   $y = Left1($pdf, "7.1.1", $y + 3);
   $y = Left3($pdf, "The Company fails to fulfill any of its payment obligations in respect of the Sale Price, unless such failure is a result of a technical or administrative error on the part of the bank or financial institution remitting payment by the Company to Kapital Boost, or by Kapital Boost to the Financier; or", $y - 7);

   $y = Left1($pdf, "7.1.2", $y + 3);
   $y = Left3($pdf, "The Company fails to observe and perform any of its obligations under this Agreement (including, without limitation (a) its obligation to offer to purchase the Asset from the Investors Group, and (b) its obligations as agent of the Investors Group to purchase the Asset at the Purchase Cost and to accept the Offer to Purchase); or", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp12) {
      $pdf->setPage($synp12);
      $y = 7;
   }
   $y = Left1($pdf, "7.1.3", $y + 3);
   $y = Left3($pdf, "Any representation, statement, or warranty given or made by the Company in this Agreement proves to be untrue, incorrect or inaccurate or misleading in any material respect; or", $y - 7);

   $y = Left1($pdf, "7.1.4", $y + 3);
   $y = Left3($pdf, "Any step is taken for the winding up, liquidation or dissolution of the Company;", $y - 7);

   $y = Left1($pdf, "7.1.5", $y + 3);
   $y = Left3($pdf, "Where there is an element of fraud, misuse, or misappropriation of Purchase Cost by the Company", $y - 7);

   $y = Left1($pdf, "7.2", $y + 5);
   $y = Left2($pdf, "Upon the occurrence of any Event of Default, the Financier shall have the right by notice directly, or indirectly through Kapital Boost as Agent acting on behalf of Financier, to the Company to:", $y - 7);

   $y = Left1($pdf, "7.2.1", $y + 3);
   $y = Left3($pdf, "declare the entire proportion of the Sale Price outstanding from the Company to the Financier to become immediately due and payable; and/or", $y - 7);

   $y = Left1($pdf, "7.2.2", $y + 3);
   $y = Left3($pdf, "require the Company to (a) sell the Asset and apply all proceeds of sale to pay the outstanding Sale Price; and (b) otherwise not dispose of or transfer the ownership of the Asset to any other person; and/or", $y - 7);

   $y = Left1($pdf, "7.2.3", $y + 3);
   $y = Left3($pdf, "require the Company to indemnify the Investors Group for any costs, losses or expenses incurred as a result of the occurrence of any Event of Default including, without limitation (a) any loss of profit by the Financier arising from (i) the failure by the Company to fulfill its undertaking to offer to purchase the Asset from the Investors Group, or (ii) failure by the Company as agent of the Investors Group to purchase the Asset at the Purchase Cost or to accept the Offer to Purchase, (b) any costs incurred in taking physical possession of the Asset following an Event of Default, and (c) any diminution in the value of the Asset arising from a breach by the Company of the terms of this Agreement.", $y - 7);

   $y = Left1($pdf, "7.3", $y + 5);
   $y = Left2($pdf, "The right of the Financier to accelerate the obligations of the Company in respect of the portion of the Sale Price due to the Financier may be exercised irrespective of, and without being contingent upon, the action or inaction of any other financier or the rest of the Investors Group. The Financier shall be entitled to bring or prosecute any legal action or claim against the Company to recover the outstanding sum, and the Company agrees to immediately upon demand indemnify the Financier for all actual costs, losses and liabilities (including legal fees) incurred or suffered by the Financier as a result of any Event of Default.", $y - 7);

   if ($pg < $synp11) {
      $pdf->setPage($synp11);
      $y = -5;
   }
   $y = Left1($pdf, "<b>8. REPRESENTATIONS</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "8.1", $y + 10);
   $y = Left2($pdf, "The Company represents to the Financier on the date of this Agreement
  that:", $y - 7);

   $y = Left1($pdf, "8.1.1", $y + 3);
   $y = Left3($pdf, "It is duly incorporated and validly existing under the laws of Republic of Indonesia;", $y - 7);

   $y = Left1($pdf, "8.1.2", $y + 3);
   $y = Left3($pdf, "It has the power to execute this Agreement, and to perform its obligations under this Agreement and has taken all necessary action to authorize such execution and performance;", $y - 7);

   $y = Left1($pdf, "8.1.3", $y + 3);
   $y = Left3($pdf, "The execution and performance of this Agreement does not violate or conflict with any applicable law or regulation, any provision of its constitutional documents, and order or judgment of any court or other agency of government applicable to it, or any contractual restriction binding on it;", $y - 7);

   $y = Left1($pdf, "8.1.4", $y + 3);
   $y = Left3($pdf, "All consent required to have been obtained by it with respect to this Agreement have been obtained and are in full force and effect;", $y - 7);

   $y = Left1($pdf, "8.1.5", $y + 3);
   $y = Left3($pdf, "Its obligations under this Agreement constitute its legal, valid and binding obligations, enforceable in accordance with their respective terms;", $y - 7);

   $y = Left1($pdf, "8.1.6", $y + 3);
   $y = Left3($pdf, "No Event of Default has occurred or would reasonably be expected to result from the entry into, or performance of, this Agreement; and", $y - 7);

   $y = Left1($pdf, "8.1.7", $y + 3);
   $y = Left3($pdf, "The core or main business of the Company and the purpose of the financing under this Agreement are and shall at all times remain Shariah compliant.", $y - 7);

   $y = Left1($pdf, "8.2", $y + 5);
   $y = Left2($pdf, "The representations and warranties in this Clause 6 are repeated by the Company on the Purchase Date and on each scheduled date for payment of all or part of the Sale Price.", $y - 7);

   $y = Left1($pdf, "<b>9. LATE PAYMENT CHARGES (TA’WIDH AND GHARAMAH)</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "9.1", $y + 10);
   $y = Left2($pdf, "If any amount which is due and payable by the Company under or in connection with this Agreement is not paid in full on the date in accordance with this Agreement (an \"<b>unpaid amount</b>&nbsp;&nbsp;\"), the Company undertakes to pay late payment charge comprising both ta’widh and gharamah (calculated below) to the Investor Group on each day that an unpaid amount remains outstanding.", $y - 7);

   $y = Left1($pdf, "9.2", $y + 5);
   $y = Left2($pdf, "The total late payment charge shall be pegged directly to average annualized returns of this Murabahah Agreement (to be determined by the Company), calculated on daily rest basis as below, provided that the accumulated late payment charges shall not exceed 100 percent of the outstanding unpaid amount:", $y - 7);

   $y = CenterL($pdf, "Unpaid Amount X average annualized returns X", $y + 5);
   $y = CenterL($pdf, "No. of Overdue day(s) / 365", $y);

   $y = Left1($pdf, "9.3", $y + 5);
   $y = Left2($pdf, "Each Financier may retain only such part of the late payment amount paid to it, as is necessary to compensate the Financier for any actual costs (not to include any opportunity or funding costs), subject to and in accordance with the guidelines of its own Shariah advisers, and up to a maximum aggregate amount of 1% per annum of the unpaid amount (“ta’widh”). ", $y - 7);

   $y = Left1($pdf, "9.4", $y + 5);
   $y = Left2($pdf, "The ta’widh in respect of an unpaid amount, being part of the late payment charges, may accrue on a daily basis and shall be calculated in accordance with the following formula:-", $y - 7);

   $y = CenterL($pdf, "Actual cost incurred", $y + 3);
   $y = CenterL($pdf, "or", $y);
   $y = CenterL($pdf, "Unpaid Amount X 1% X No. of Overdue day(s) / 365", $y);

   $y = Left1($pdf, "9.5", $y + 5);
   $y = Left2($pdf, "The amount of ta’widh shall not be compounded.", $y - 7);

   $y = Left1($pdf, "9.6", $y + 5);
   $y = Left2($pdf, "The Investor Group hereby agree(s), covenant(s) and undertake(s) to ensure that the balance of the late payment charges (“gharamah”), if any, shall be paid to charitable bodies as may be selected by the Company in consultation with its Shariah advisers.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp4) {
      $pdf->setPage($synp4);
      $y = -5;
   }
   $y = Left1($pdf, "<b>10. GOVERNING LAW AND DISPUTE RESOLUTION</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "10.1", $y + 10);
   $y = Left2($pdf, "This Agreement and the rights and obligations of the Parties hereunder shall be governed by and interpreted and construed in all respects in accordance with the laws of Republic of Indonesia without prejudice to or limitation of any other rights or remedies available to both Parties in this Agreement under the laws of any jurisdiction where the Company or his assets may be located.", $y - 7);

   $y = Left1($pdf, "10.2", $y + 5);
   $y = Left2($pdf, "The Parties hereby irrevocably agree to submit to the exclusive jurisdiction of the Courts of Republic of Indonesia to resolve any disputes or make any claims with respect to this Agreement.", $y - 7);

   $y = Left1($pdf, "10.3", $y + 5);
   $y = Left2($pdf, "The Parties agree that if any difference, dispute, conflict or controversy <b>(a 'Dispute')</b>&nbsp;, arises out of or in connection with this Agreement or its performance, including without limitation any dispute regarding its existence, validity, or termination of rights or obligations of any Party, the Parties will attempt for a period of 30 (thirty) days after the receipt by one Party of a notice from the other Party of the existence of the Dispute to settle the Dispute by amicable settlement between the Parties", $y - 7);

   $y = Left1($pdf, "10.4", $y + 5);
   $y = Left2($pdf, "If the Parties are unable to reach agreement to settle the Dispute within the 30 (thirty) day period mentioned in Clause 10.3 above, then both Parties hereby submit to the jurisdiction of the Clerk’s Office at the Jakarta Pusat Religious Court. Such submission shall not affect the rights of the Financier to take proceedings, to the extent permitted by Indonesia law, in any other jurisdiction within or outside the Republic of Indonesia nor shall the taking of proceedings in any jurisdiction pursuant to prevailing laws and regulations.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp5) {
      $pdf->setPage($synp5);
      $y = -5;
   }
   $y = Left1($pdf, "<b>11. MISCELLANEOUS</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "11.1", $y + 10);
   $y = Left21($pdf, "If at any time any one or more of the provisions hereof is or become illegal, invalid or unenforceable under Republic of Indonesia law, neither the legality, validity or enforceability of the remaining provisions hereof nor the legality, validity or enforceability of such provisions under the laws of any other jurisdiction shall in any way be affected or impaired thereby.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp13) {
      $pdf->setPage($synp13);
      $y = 5;
   }
   $y = Left1($pdf, "11.2", $y + 5);
   $y = Left21($pdf, "If any provision of this Agreement (or part of it) or the application thereof to any person or circumstance shall be illegal, invalid or unenforceable to any extent, it must be interpreted as narrowly as necessary to allow it to be enforceable or valid and the remainder of this Agreement and the legality, validity or enforceability of such provisions to other persons or circumstances shall not be in any way affected or impaired thereby and shall be enforceable / enforced to the greatest extent permitted by law.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp9) {
      $pdf->setPage($synp9);
      $y = 5;
   }
   $y = Left1($pdf, "11.3", $y + 5);
   $y = Left21($pdf, "All rights and obligations in this Agreement are personal to the Parties and each Party in this Agreement may not assign and/or transfer any such rights and obligations to any third party without the prior consent in writing of the Parties.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp15) {
      $pdf->setPage($synp15);
      $y = 5;
   }
   $y = Left1($pdf, "11.4", $y + 5);
   $y = Left21($pdf, "This Agreement contains the entire understanding between the Parties relating to the transaction contemplated by this Agreement and shall supersede any prior expressions of intent or understandings with respect to the said transaction. All prior or contemporaneous agreements, understandings, representations and statements, oral and written, are merged in this Agreement and shall be of no further force or effect.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp16) {
      $pdf->setPage($synp16);
      $y = 5;
   }
   $y = Left1($pdf, "11.5", $y + 5);
   $y = Left21($pdf, "Any communication to be made under or in connection with this Agreement shall be made in writing and, may be made by letter or electronic mail, in each case, through Kapital Boost at <b>116 Changi Road, #05-00, Singapore 419718</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; or at such other address as Kapital Boost may notify the parties from time to time. Any communication made between Kapital Boost and any of the Parties under or in connection with this Agreement shall be made to the address or electronic mail address provided to Kapital Boost or its registered address, in the case of the Company, and shall be effective when received.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp17) {
      $pdf->setPage($synp17);
      $y = 5;
   }
   $y = Left1($pdf, "11.6", $y + 5);
   $y = Left21($pdf, "No failure to exercise, nor any delay in exercising, on the part of the Financier, any right or remedy under this Agreement shall operate as a waiver, nor shall any single or partial exercise of any right or remedy prevent any further or other exercise or the exercise of any other right or remedy. The rights and remedies under this Agreement are cumulative and not exclusive of any rights or remedies provided by law.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp18) {
      $pdf->setPage($synp18);
      $y = 5;
   }
   $y = Left1($pdf, "11.7", $y + 5);
   $y = Left21($pdf, "No provision of this Agreement may be amended, waived, discharged or terminated orally nor may any breach of or default under any of the provisions of this Agreement be waived or discharged orally but (in each case) except (a) pursuant to an Adjustment Notice which complies with the requirements of this Agreement; or (b) by an instrument in writing signed by or on behalf of the Parties or (b). Any amendments or variations to this Agreement shall be Shariah-compliant.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp19) {
      $pdf->setPage($synp19);
      $y = 5;
   }
   $y = Left1($pdf, "11.8", $y + 5);
   $y = Left21($pdf, "This Agreement may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp20) {
      $pdf->setPage($synp20);
      $y = 5;
   }
   $y = Left1($pdf, "11.9", $y + 5);
   $y = Left21($pdf, "Each Party agrees to keep all information relating to this Agreement confidential, and not disclose it to anyone, save with the prior written consent of the other Parties or as required by any applicable laws or regulations.", $y - 7);

   $y = Left1($pdf, "11.10", $y + 5);
   $y = Left21($pdf, "This Agreement is intended to be Shariah-compliant. The parties hereby agree and acknowledge that their respective rights and obligations under this Agreement are intended to, and shall, be in conformity with Shariah principles.", $y - 7);


   $y = Left1($pdf, "11.11", $y + 5);
   $y = Left21($pdf, "Notwithstanding the above, each party represents to the other that it shall not raise any objections or claims against the other on the basis of Shariah-compliance or any breach of Shariah principles in respect of or otherwise in relation to any part of any provision of this Agreement.  ", $y - 7);

   $y = Left1($pdf, "<b>12. DEFINITIONS</b>&nbsp;", $y + 15);

   $y = Left1($pdf, "In this Agreement, any reference to:", $y + 10);

   $y = Left1($pdf, "12.1", $y + 5);
   $y = Left21($pdf, "any Party or Kapital Boost shall be construed so as to include its successors in title, permitted assigns and permitted transferees;", $y - 7);

   $y = Left1($pdf, "12.2", $y + 5);
   $y = Left21($pdf, "a provision of law is a reference to that provision as amended or re-enacted;", $y - 7);

   $y = Left1($pdf, "12.3", $y + 5);
   $y = Left21($pdf, "a person or Party in a particular gender shall include that person or Party regardless of his or her gender, and shall apply equally where that person or Party is a body corporate, firm, trust, joint venture or partnership;", $y - 7);

   $y = Left1($pdf, "12.4", $y + 5);
   $y = Left21($pdf, "the \"<b>Investors Group</b>&nbsp;&nbsp; \" shall be construed to mean just the Financier if the Financier is financing the entirety of the Purchase Cost;", $y - 7);

   $y = Left1($pdf, "12.5", $y + 5);
   $y = Left21($pdf, "\"<b>Kapital Boost</b>&nbsp;&nbsp; \" means <b>Kapital Boost Pte Ltd, Singapore Company Registration No. 201525866W and PT Kapital Boost Indonesia, registered in the presence of Notary Panji Kresna S.H., M.Kn. on 23 March 2016</b>&nbsp;", $y - 7);

   $y = Left1($pdf, "12.6", $y + 5);
   $y = Left21($pdf, "\"<b>Sale Price</b>&nbsp;&nbsp; \" shall mean the Sale Price as adjusted pursuant to Clause 1.6.6.", $y - 7);

   // $pdf->setPage($synp100);
   $pdf->AddPage();
   $y = 0;
   $pdf->SetMargins(20, 15);
   $y = MLeft1($pdf, "<br><br><b>Dated this day $date / Tertanggal hari $dateID</b>&nbsp;", 5);

   $y = MLeft1($pdf, "Signed by / Tertanda oleh<br>for and on behalf of / untuk dan atas nama<br><b>$companyName</b>&nbsp;", $y + 15);

   $oSignx = 70;
   $oSigny = ($y * $pdf->getScaleFactor()) + 5;
   $oSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);


	if (strpos(strtolower($dirIC), 'singapore') !== false) {
		// if (strpos(strtolower($icType), 'passport') !== false) {
			// $dirIC = "Singapore Passport No.";
		// } else if (strpos(strtolower($icType), 'fin') !== false) {
			// $dirIC = "Singapore FIN No.";
		// } else {
			$dirIC = "Singapore Identity Card No.";
		// }
	}
	if (strpos(strtolower($dirIC), 'indonesia') !== false) {
		// if (strpos(strtolower($icType), 'passport') !== false) {
			// $dirIC = "Indonesia Passport No.";
		// } else if (strpos(strtolower($icType), 'fin') !== false) {
			// $dirIC = "Indonesia FIN No.";
		// } else {
			$dirIC = "Indonesia KTP No.";
		// }
	}
	if (strpos(strtolower($dirIC), 'malaysia') !== false) {
		// if (strpos(strtolower($icType), 'passport') !== false) {
			// $dirIC = "Malaysia Passport No.";
		// } else if (strpos(strtolower($icType), 'fin') !== false) {
			// $dirIC = "Malaysia FIN No.";
		// } else {
			$dirIC = "Malaysia Identity Card No.";
		// }
	}
	if (strpos(strtolower($invIC), 'singapore') !== false) {
		if (strpos(strtolower($icType), 'passport') !== false) {
			$invIC = "Singapore Passport No.";
		} else if (strpos(strtolower($icType), 'fin') !== false) {
			$invIC = "Singapore FIN No.";
		} else {
			$invIC = "Singapore Identity Card No.";
		}
	}
	if (strpos(strtolower($invIC), 'indonesia') !== false) {
		if (strpos(strtolower($icType), 'passport') !== false) {
			$invIC = "Indonesia Passport No.";
		} else if (strpos(strtolower($icType), 'fin') !== false) {
			$invIC = "Indonesia FIN No.";
		} else {
			$invIC = "Indonesia KTP No.";
		}
	}
	if (strpos(strtolower($invIC), 'malaysia') !== false) {
		if (strpos(strtolower($icType), 'passport') !== false) {
			$invIC = "Malaysia Passport No.";
		} else if (strpos(strtolower($icType), 'fin') !== false) {
			$invIC = "Malaysia FIN No.";
		} else {
			$invIC = "Malaysia Identity Card No.";
		}
	}

   $y = MLeft1($pdf, "<b>Name/Nama: $dirName</b>&nbsp;<br><b>Title/Jabatan: $dirTitle</b>&nbsp;<br><b>KTP No. / Nomor Kartu Tanda Penduduk: / $dirICID</b>&nbsp;", $y - 2);

   $y = MLeft1($pdf, "Signed by/Tertanda oleh", $y + 10);

   $iSignx = 70;
   $iSigny = ($y * $pdf->getScaleFactor()) + 5;
   $iSignp = $pdf->getPage();

   $pdf->SetMargins(20, 15, 110);
   $y = MLeft1($pdf, "<hr>", $y + 10);
   $pdf->SetMargins(20, 15);
   if (!strpos($invIC, $icNumber)) {
		$y = MLeft1($pdf, "<b>Name/Nama: $invName</b>&nbsp;<br><b>$invIC  $icNumber</b>&nbsp;", $y - 2);
   } else {
		$y = MLeft1($pdf, "<b>Name/Nama: $invName</b>&nbsp;<br><b>$invIC</b>&nbsp;", $y - 2);
   }


   /*  ---------- END of English Version ------------ */



   $pdf->AddPage();
   $y = Left1($pdf, "<b>SCHEDULE A: Description of Assets</b>&nbsp;", 20);
   $y = Left1($pdf, "<b>LAMPIRAN A: Deskripsi Aset</b>&nbsp;", 27);

   list($width1, $height1, $type1, $attr1) = getimagesize($image1);
   $height1 = $height1/($width1/180);
   $minTheight = 250;
   if ($height1>$minTheight){$theight=$minTheight-30;}else{$theight=null;}

   $pdf->Image($image1, 15, $y + 10, 180, $theight, '', '', '', true, 150, '', false, false, 1, false, false, false);

   if ($imageD2 != "" or $imageD2 != null) {
	   list($width2, $height2, $type2, $attr2) = getimagesize($image2);
	   $height2 = $height2/($width2/180);
	   if ($height2>$minTheight){$theight2=$minTheight;}else{$theight2=null;}

	   $pdf->AddPage();
	   $y = 5;
	   $pdf->Image($image2, 15, $y + 10, 180, $theight2, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD3 != "" or $imageD3 != null) {
	   list($width3, $height3, $type3, $attr3) = getimagesize($image3);
	   $height3 = $height3/($width3/180);
	   if ($height3>$minTheight){$theight3=$minTheight;}else{$theight3=null;}

		$pdf->AddPage();
		$y = 5;
		$pdf->Image($image3, 15, $y + 10, 180, $theight3, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD4 != "" or $imageD4 != null) {
	   list($width4, $height4, $type4, $attr4) = getimagesize($image4);
	   $height4 = $height4/($width4/180);
	   if ($height4>$minTheight){$theight4=$minTheight;}else{$theight4=null;}

		$pdf->AddPage();
		$pdf->Image($image4, 15, $y + 10, 180, $theight4, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD5 != "" or $imageD5 != null) {
	   list($width5, $height5, $type5, $attr5) = getimagesize($image5);
	   $height5 = $height5/($width5/180);
	   if ($height5>$minTheight){$theight5=$minTheight;}else{$theight5=null;}

		$pdf->AddPage();
		$pdf->Image($image5, 15, $y + 10, 180, $theight5, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD6 != "" or $imageD6 != null) {
	   list($width6, $height6, $type6, $attr6) = getimagesize($image6);
	   $height6 = $height6/($width6/180);
	   if ($height6>$minTheight){$theight6=$minTheight;}else{$theight6=null;}

		$pdf->AddPage();
		$pdf->Image($image6, 15, $y + 10, 180, $theight6, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD7 != "" or $imageD7 != null) {
	   list($width7, $height7, $type7, $attr7) = getimagesize($image7);
	   $height7 = $height7/($width7/180);
	   if ($height7>$minTheight){$theight7=$minTheight;}else{$theight7=null;}

		$pdf->AddPage();
		$pdf->Image($image7, 15, $y + 10, 180, $theight7, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD8 != "" or $imageD8 != null) {
	   list($width8, $height8, $type8, $attr8) = getimagesize($image8);
	   $height8 = $height8/($width8/180);
	   if ($height8>$minTheight){$theight8=$minTheight;}else{$theight8=null;}

		$pdf->AddPage();
		$pdf->Image($image8, 15, $y + 10, 180, $theight8, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD9 != "" or $imageD9 != null) {
	   list($width9, $height9, $type9, $attr9) = getimagesize($image9);
	   $height9 = $height9/($width9/180);
	   if ($height9>$minTheight){$theight9=$minTheight;}else{$theight9=null;}

		$pdf->AddPage();
		$pdf->Image($image9, 15, $y + 10, 180, $theight9, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD10 != "" or $imageD10 != null) {
	   list($width10, $height10, $type10, $attr10) = getimagesize($image10);
	   $height10 = $height10/($width10/180);
	   if ($height10>$minTheight){$theight10=$minTheight;}else{$theight10=null;}

		$pdf->AddPage();
		$pdf->Image($image10, 15, $y + 10, 180, $theight10, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }
   if ($imageD11 != "" or $imageD11 != null) {
	   list($width11, $height11, $type11, $attr11) = getimagesize($image11);
	   $height11 = $height11/($width11/180);
	   if ($height11>$minTheight){$theight11=$minTheight;}else{$theight11=null;}

		$pdf->AddPage();
		$pdf->Image($image11, 15, $y + 10, 180, $theight11, '', '', '', true, 150, '', false, false, 1, false, false, false);
   }


   $pdf->AddPage();
   $synp101 = $pdf->getPage();
   $y = Left1($pdf, "<b style=\"font-size:1.2em\">SCHEDULE B: Form of Murabahah Sale Contract</b>&nbsp;", 20);

   $y = CenterL($pdf, "<b>Murabahah Sale Contract</b>&nbsp;", $y + 5);

   $y = CenterL($pdf, "Between $companyName (the \"<b>Company</b>&nbsp; \") and the Investors Group", $y + 5);

   $y = Left1($pdf, "Re: &nbsp;&nbsp;Murabahah Agreements between the Company and various Financiers (together forming the Investors Group) with respect to the purchase of the Asset described below (the \"<b>Murabahah Agreements</b>&nbsp;&nbsp;&nbsp;\", and each, a \"<b>Murabahah Agreement</b>&nbsp;&nbsp;&nbsp;\"). ", $y + 5);

   $y = Left1($pdf, "1.", $y + 5);
   $y = Left2($pdf, "All terms defined in the Murabahah Agreements have the same meaning in this Murabahah Sale Contract.", $y - 7);

   $y = Left1($pdf, "2.", $y + 5);
   $y = Left2($pdf, "The Company hereby confirms that it, as agent of the Investors Group, has purchased the Asset, and that the Asset is in the Company’s possession. The Company has attached to this Murabahah Sale Contract (a) a description of the Asset; and (b) a true copy of the receipt for the purchase of the Asset by us, as agent of the Investors Group.", $y - 7);

   $y = Left1($pdf, "3.", $y + 5);
   $y = Left2($pdf, "In accordance with the terms of the Murabahah Agreements, the Company hereby offers to purchase the Asset from the Investors Group based on the terms and subject to the conditions set out in the Murabahah Agreements. Pursuant to the terms of the Murabahah Agreements, the Sale Price is $salePrice and will be paid as follows:", $y - 7);


   $payoutBTbl = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b>&nbsp;</td>
  <td style=\"text-align:center\"><b>Installment Amount of Sale Price payable to Investors Group</b>&nbsp;</td>
  </tr>
  $payoutString
  </table>
  ";
   $pdf->SetY($y + 10);
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutBTbl);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Left1($pdf, "4.", $y);
   $y = Left2($pdf, "By accepting and agreeing to this Murabahah Sale Contract, the Investors Group shall immediately sell the Asset to the Company on the terms and subject to the conditions set out in the Murabahah Agreement.", $y - 7);

   $y = Left1($pdf, "5.", $y + 10);
   $y = Left2($pdf, "This Murabahah Sale Contract may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);


   $y = Left1($pdf, "For and on behalf of the Company (as buyer of Asset)", $y + 20);

   $y = Left1($pdf, "<hr style=\"width:150;\">", $y + 25);
   $y = Left1($pdf, "Name:", $y + 2);
   $y = Left1($pdf, "Title:", $y + 2);
   $y = Left1($pdf, "Date:", $y + 2);

   $y = Left1($pdf, "Kapital Boost, as agent for and on behalf of the Investors Group (as seller of Asset)", $y + 15);

   if ($y > 255) {
      $pdf->AddPage();
      $y = 10;
   }
   $y = Left1($pdf, "<hr style=\"width:150\">", $y + 25);
   $y = Left1($pdf, "Name:", $y + 2);
   $y = Left1($pdf, "Title:", $y + 2);
   $y = Left1($pdf, "Date:", $y + 2);

   /* End of Eng Schedule B */


   $pdf->setPage($synp101);

   $y = Right1($pdf, "<b style=\"font-size:1.2em\">LAMPIRAN B: Lembar Kontrak Penjualan Murabahah</b>&nbsp;", 20);

   $y = CenterR($pdf, "<b>Kontrak Penjualan Murabahah</b>&nbsp;", $y + 5);

   $y = CenterR($pdf, "Antara $companyName (the \"<b>Perusahaan</b>&nbsp; \") dan Grup Investor", $y + 5);

   $y = Right1($pdf, "Hal: &nbsp;&nbsp;&nbsp; Perjanjian-perjanjian Murabahah antara Perusahaan dan berbagai Pemodal (secara bersama-sama merupakan Grup Investor) sehubungan dengan pembelian Aset yang dijelaskan di bawah ini (<b>\"Perjanjian-perjanjian Murabahah\"</b>&nbsp;, dan masing-masing, <b>\"Perjanjian Murabahah\"</b>&nbsp;)", $y + 5);

   $y = Right1($pdf, "1.", $y + 5);
   $y = Right2($pdf, "Seluruh istilah yang didefinisikan dalam Perjanjian-perjanjian Murabahah memiliki pengertian yang sama dalam Kontrak Penjualan Murabahah ini.", $y - 7);

   $y = Right1($pdf, "2.", $y + 5);
   $y = Right2($pdf, "Perusahaan dengan ini menegaskan bahwa Perusahaan, sebagai agen Grup Investor, telah membeli Aset, dan bahwa Aset adalam penguasaan Perusahaan. Perusahaan telah melampirkan pada Kontrak Penjualan Murabahah ini (a) penjelasan tentang Aset;  dan (b) salinan sebenarnya dari tanda terima pembelian Aset oleh kami, sebagai agen Grup Investor.", $y - 7);

   $y = Right1($pdf, "3.", $y + 5);
   $y = Right2($pdf, "Menurut ketentuan Perjanjian-perjanjian Murabahah, Perusahaan dengan ini menawarkan untuk membeli Aset dari Grup Investor berdasarkan ketentuan dan tunduk pada persyaratan yang terdapat dalam Perjanjian-perjanjian Murabahah. Sesuai ketentuan Perjanjian-perjanjian Murabahah, Harga Jual sebesar $salePrice dan akan dibayarkan sebagai berikut:", $y - 7);


   $payoutBTbl = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Tanggal</b>&nbsp;</td>
  <td style=\"text-align:center\"><b>Jumlah Angsuran Harga Jual yang wajib dibayarkan kepada Grup Investor </b>&nbsp;</td>
  </tr>
  $payoutStringR
  </table>
  ";

   $pdf->SetY($y + 10);
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetX(110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutBTbl);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Right1($pdf, "4.", $y);
   $y = Right2($pdf, "Dengan menerima dan menyepakati Kontrak Penjualan Murabahah ini, Grup Investor akan segera menjual Aset kepada Perusahaan dengan ketentuan dan tunduk pada persyaratan yang terdapat dalam Perjanjian Murabahah.", $y - 7);

   $y = Right1($pdf, "5.", $y + 10);
   $y = Right2($pdf, "Kontrak Penjualan Murabahah ini dapat ditandatangani dalam berapa pun jumlah rangkap, dan memiliki keberlakuan yang sama seolah-olah tanda tangan-tanda tangan pada rangkap tersebut terdapat pada salinan tunggal dari Perjanjian ini.", $y - 7);


   $y = Right1($pdf, "Untuk dan atas nama Perusahaan (sebagai pembeli Aset)", $y + 20);

   $y = Right1($pdf, "<hr style=\"width:150;\">", $y + 25);
   $y = Right1($pdf, "Nama:", $y + 2);
   $y = Right1($pdf, "Gelar:", $y + 2);
   $y = Right1($pdf, "Tanggal:", $y + 2);

   $y = Right1($pdf, "Kapital Boost, sebagai agen untuk dan atas nama Grup Investor (sebagai penjual Aset)", $y + 15);

   if ($y > 255) {
      $pdf->AddPage();
      $y = 10;
   }
   $y = Right1($pdf, "<hr style=\"width:150\">", $y + 25);
   $y = Right1($pdf, "Nama:", $y + 2);
   $y = Right1($pdf, "Gelar:", $y + 2);
   $y = Right1($pdf, "Tanggal:", $y + 2);



   $output = $pdf->Output('/var/www/kapitalboost.com/public_html/assets/pdfs/' . $token . '.pdf', 'F');
    // $output = $pdf->Output('/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com/assets/pdfs/' . $token . '.pdf', 'F');
}
}

if ($output == 1) {

   if ($subtype=="INVOICE  FINANCING") {
		$sign = array("inx" => $inSignx, "iny" => $inSigny, "inp" => $inSignp,  "owx" => $owSignx, "owy" => $owSigny, "owp" => $owSignp, "ix" => $iSignx, "iy" => $iSigny, "ip" => $iSignp, "ox" => $oSignx, "oy" => $oSigny, "op" => $oSignp,  "kx" => $kSignx, "ky" => $kSigny, "kp" => $kSignp);
   } else {
	   $sign = array("ix" => $iSignx, "iy" => $iSigny, "ip" => $iSignp, "ox" => $oSignx, "oy" => $oSigny, "op" => $oSignp);
   }

   $signCoded = json_encode($sign);
   $result = $mysql->UpdateInvContract($iId, $signCoded);
   // $result = $mysql->UpdateInvContract($iId, 1);
   echo $result;
} else {
   echo -1;
}


/* Monolingual Functions */

function MCenter($pdf, $string, $y, $blue = false) {
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(25, 15);
   $width = $pdf->GetStringWidth($string);
   $x = 105 - ($width / 2);
   $pdf->SetXY($x, $y);
   $pdf->Write(8, $string);
}

function MCenter1($pdf, $string, $y) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $width = $pdf->GetStringWidth($string) - 20; // adjust <b> and </b>&nbsp;
   $x = 105 - ($width / 2);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft1($pdf, $string, $y, $x = 20) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft2($pdf, $string, $y, $x = 34) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 15, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MLeft3($pdf, $string, $y, $x = 46) { // 44
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 15, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MBullet($pdf, $y, $x = 40) {
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function MLeft4($pdf, $string, $y, $x = 54) { // 60
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft5($pdf, $string, $y, $x = 62) { // 70
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MLeft6($pdf, $string, $y, $x = 70) {
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   if ($newY > 250) {
      $pdf->AddPage('P', array('210', '297'));
      $pdf->SetMargins(25, 15);
      $newY = 10;
   }
   return $newY;
}

/* Bilingual Functions */

function CenterL($pdf, $string, $y, $blue = false) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetXY(10, $y);
   $string = "<span style=\"text-align:center;\">" . $string . "</span>";
   $pdf->writeHTML($string);
   return $pdf->GetY();
}

function BulletL($pdf, $y, $x = 20) {
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function Left1($pdf, $string, $y, $x = 10) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left1invo($pdf, $string, $y, $x = 10) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left2($pdf, $string, $y, $x = 20) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left2invo($pdf, $string, $y, $x = 20) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left21($pdf, $string, $y, $x = 23) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left3($pdf, $string, $y, $x = 26) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left3invo($pdf, $string, $y, $x = 20) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left4($pdf, $string, $y, $x = 34) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left4invo($pdf, $string, $y, $x = 28) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left41invo($pdf, $string, $y, $x = 23) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left5($pdf, $string, $y, $x = 42) { // 70
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function CenterR($pdf, $string, $y, $blue = false) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(110, 10, 7);
   $pdf->SetXY(110, $y);
   $string = "<span style=\"text-align:center;\">" . $string . "</span>";
   $pdf->writeHTML($string);
   return $pdf->GetY();
}

function BulletR($pdf, $y, $x = 120) {

   $pdf->SetMargins($x, 10,7);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function Right1($pdf, $string, $y, $x = 110) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right120($pdf, $string, $y, $x = 110) {
        if ($y > 280) {
           $pdf->AddPage();
           $y = 10;
        }
        $pdf->SetMargins($x, 10, 7);
        $pdf->SetXY($x, $y);
        $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
        $pdf->writeHTML($string);
        $newY = $pdf->GetY();
        return $newY;
     }


function Right1invo($pdf, $string, $y, $x = 110) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right2($pdf, $string, $y, $x = 120) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right2invo($pdf, $string, $y, $x = 120) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right21($pdf, $string, $y, $x = 123) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right3($pdf, $string, $y, $x = 126) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right3invo($pdf, $string, $y, $x = 120) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right4($pdf, $string, $y, $x = 134) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right4invo($pdf, $string, $y, $x = 128) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right41invo($pdf, $string, $y, $x = 123) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:15px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right5($pdf, $string, $y, $x = 142) { // 70
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}
