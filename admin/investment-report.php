<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
$cId = $_GET["c"];

if($mysql->Connection()){

       list($iId,$mId, $mIcName,$mIcType, $mNric,$iAmount,$iAmountT,$iProfit,$iReturn,$date1,$date2,$email,$nationality) = $mysql->GetAllInvestmentsForCampaign($cId);


}



?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KapitalBoost Admin Generate Report</title>

              <?php include_once 'include.php'; ?>

              <style>
                     .tds{
                            width:95%;height:100%;padding:7px;border-radius: 3px;
                     }
              </style>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Generate Campaign Report For </span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">

                                                 <form method="POST" action="/admin/generate-xlsx.php" id="formsub">
                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="">No</th>
                                                                             <th width="">Investor's Name</th>
                                                                             <th>No. Identification</th>
                                                                             <th width="">ID Type</th>
                                                                             <th width="">Email</th>
                                                                             <th width="">Amount Invested</th>
                                                                             <th>Profit</th>
                                                                             <!-- <th>Investor Returns</th> -->
                                                                             <!-- <th width="">Amount Invested %</th>
                                                                             <th width="">Date 1</th>
                                                                             <th width="">Date 2</th> -->

                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php
                                                                      for ($i = 0; $i < count($iId); $i++) {
                                                                             ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><input type="text" class="tds" name="name[<?=$i?>]" value='<?=$mIcName[$i]?>' /></td>
                                                                                    <td><input type="text" class="tds" name="nric[<?=$i?>]" value='<?=$mNric[$i]?>'></td>
                                                                                    <td><input type="text" class="tds" name="icType[<?=$i?>]" value='<?=$mIcType[$i]?>'></td>
                                                                                    <td><input type="text" class="tds" name="email[<?=$i?>]" value='<?=$email[$i]?>'></td>
                                                                                    <td><input type="text" class="tds" name="amount[<?=$i?>]" value='<?=$iAmount[$i]?>'></td>
                                                                                     <td><input type="text" class="tds" name="profit[<?=$i?>]" value='<?=$iProfit[$i]?>'></td>
                                                                                      <!-- <td><input type="text" class="tds" name="return[<?=$i?>]" value='<?=$iReturn[$i]?>'</td> -->
                                                                                    <!-- <td><input type="text" class="tds" name="amountT[<?=$i?>]" value='<?=round($iAmountT[$i],2)?>%'</td>
                                                                                    <td><input type="text" class="tds" name="date1[<?=$i?>]" value='<?=$date1[$i]?>'</td>
                                                                                    <td><input type="text" class="tds" name="date2[<?=$i?>]" value='<?=$date2[$i]?>'</td> -->

                                                                             </tr>
                                                                             <?php
                                                                      }
                                                                      ?>
                                                               </tbody>
                                                        </table>
                                                        <button id="btn" class="btn btn-primary" style="float:right">Generate Excel Report</button>
                                                 </div>

                                                 </form>

                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>

              <script>
                     $(document).ready(function(){
                            $("#btn").on("click",function(){
                                   $("#formsub").submit();
                            });

                     });
              </script>


       </body>


</html>
