<?php
require '../lib/fpdi/tcpdf/tcpdf_config_alt.php';
require '../lib/fpdi/tcpdf/tcpdf.php';

include_once '../security.php';
include_once '../mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
	$akad = $mysql->getDatas('akad', 'id', $_GET['id']);
	$types = array('1' => 'Wakalah', '2' => 'Crowdfunding');

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('AmdRam | Rly');
	$pdf->SetTitle('Kapitalboost Akad Crowdfunding');
	$pdf->SetSubject('Akad PT.ABC');
	$pdf->SetKeywords('Crowdfunding, Kapitalboost');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, '', array(0,0,0), array(0,0,0), PDF_HEADER_X_POSSION);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	// 	require_once(dirname(__FILE__).'/lang/eng.php');
	// 	$pdf->setLanguageArray($l);
	// }

	// set font
	$pdf->SetFont('Helvetica', '', 11);

	// -------------------- ######## ----------------------
	// add a page for texts
	$pdf->AddPage();
	// set list indent
	$pdf->setListIndentWidth(4);
	// output the HTML content
	$pdf->writeHTML($akad->content, true, false, true, false, '');

	// reset pointer to the last page
	$pdf->lastPage();

	// -------------------- ######## ----------------------
	// add a page for sign
	$pdf->AddPage();
	$pdf->writeHTML($akad->signature, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();

	// -------------------- ######## ----------------------
	// add a page for sign
	$pdf->AddPage();
	$pdf->writeHTML($akad->appendix, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();

	$pdf->Output($types[$akad->type] . ".pdf", 'I');
}else{

}

?>
