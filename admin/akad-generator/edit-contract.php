<!DOCTYPE html>
<html>
<?php
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
include_once '../security.php';
include_once '../mysql.php';
include_once '../lib/helper.php';
$mysql = new mysql();



?>
<head>
	<?php include_once '../initialize.php'; ?>
	<title>Kapitalboost | Akad Generator</title>
	<?php include_once '../include.php'; ?>
	<script src="https://cdn.tiny.cloud/1/kjwvahkftdzi9tbun3uhmtrvb01k9419m6vy9eiahopwc8e0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

	<style type="text/css">
	#content.container-fluid {
		margin-top: 80px;
	}
	body{
		background: #FFF !important;
	}
	.akad-content {
		width: 794px; height: 1123px;margin: auto;
	}
	#loading {
		width: 100%;
		height: 100%;
		position: fixed;
		top: 0;
		z-index: 9999;
		display: table;
		background: rgba(0,0,0,.5);
	}
	#loading .animation-size {
		vertical-align: middle;
		display: table-cell;
		text-align: center;
	}
	#loading .animation-size h5 {
		color: #fff;
		font-size: 20px;
	}
	#loading .animation-size h3 {
		color: #fff;
		font-size: 42px;
	}
	#loading.hide {
		display: none;
	}
	</style>
</head>
<body>
	<div class="loading hide" id="loading">
		<div class="animation-size">
			<svg width="150" height="130" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-infinity">
				<path fill="none" ng-attr-stroke="{{config.stroke}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke-dasharray="{{config.dasharray}}" d="M24.3,30C11.4,30,5,43.3,5,50s6.4,20,19.3,20c19.3,0,32.1-40,51.4-40 C88.6,30,95,43.3,95,50s-6.4,20-19.3,20C56.4,70,43.6,30,24.3,30z" stroke="#fdfdfd" stroke-width="4" stroke-dasharray="205.271142578125 51.317785644531256"><animate attributeName="stroke-dashoffset" calcMode="linear" values="0;256.58892822265625" keyTimes="0;1" dur="1" begin="0s" repeatCount="indefinite"></animate></path>
			</svg>
			<h5 class="text-loading"></h5>
		</div>
	</div>
	<!-- Get Datas From DB -->
	<?php
	$types = array('1' => 'Wakalah', '2' => 'Crowdfunding');
	$reg_type_eng = array('1' => 'Company Registration No.', '2' => 'Single Business No.');
	$reg_type_ina = array('1' => 'Nomor Tanda Daftar Perusahaan (TDP)', '2' => 'Nomor Induk Berusaha (NIB)');

	if ($mysql->Connection()) {
		if ($_POST) {
			$id = $_POST['id'];
			unset($_POST['id']);

			list($result, $err, $saved_id) = $mysql->updateDatas($_POST, 'tcontract_ukm', 'id', $id);
		}

		$contract = $mysql->getDatas('tcontract_ukm', 'id', $_GET['id']);
		$contract_info = $mysql->getDatas('tcontract', 'campaign_id', $contract->campaign_id, false);
		if (isset($_GET['r']) && $_GET['r'] == 't') {
			$akad = $mysql->getDatas('akad', 'id', '2');
			$akad_content = $akad->content;
			$akad_signature = $akad->signature;
			$akad_appendix = $akad->appendix;
		}else{
			$akad_content = $contract->akad_content;
			$akad_signature = $contract->akad_signature;
			$akad_appendix = $contract->akad_appendix_2;
		}

		$currencyFields = array("target_funding_amount", "wakalah_fee_investor", "wakalah_fee_kb");

		foreach ($contract as $key => $value) {
			if ("#$key#" == "#company_reg_type_eng#") {
				$akad_content = str_replace("#$key#", $reg_type_eng[$value], $akad_content);
			}
			else if ("#$key#" == "#company_reg_type_ina#") {
				$akad_content = str_replace("#$key#", $reg_type_ina[$value], $akad_content);
			}else{
				if (in_array($key, $currencyFields)) {
					$currency_value = number_format($value, 1, '.', ',');
					$akad_content = str_replace("#$key#", $currency_value, $akad_content);
				}else{
					$akad_content = str_replace("#$key#", $value, $akad_content);
				}
				$akad_signature = str_replace("#$key#", $value, $akad_signature);
				$akad_appendix = str_replace("#$key#", $value, $akad_appendix);
			}
		}

		$list_bg_ina = create_bilyetgiro_CF($mysql, $contract, 'ina');
		$list_bg_eng = create_bilyetgiro_CF($mysql, $contract, 'eng');

		$payout_sgd = $contract->total_payout_sgd;
		$table_pembayaran_ina = create_table_payment_schedule_CF($mysql, $contract, 'ina', $payout_sgd);
		$table_pembayaran_eng = create_table_payment_schedule_CF($mysql, $contract, 'eng', $payout_sgd);

		$akad_content = str_replace("#akta_no#", $contract_info->akta_no, $akad_content);
		$akad_content = str_replace("#akta_no_id#", $contract_info->akta_no_id, $akad_content);
		$akad_content = str_replace("#date_created_eng#", getToday($lang = 'eng'), $akad_content);
		$akad_content = str_replace("#date_created_ina#", getToday($lang = 'ina'), $akad_content);
		$akad_content = str_replace("#list_bg_ina#", $list_bg_ina, $akad_content);
		$akad_content = str_replace("#list_bg_eng#", $list_bg_eng, $akad_content);
		$akad_content = str_replace("#table_pembayaran_ina#", $table_pembayaran_ina, $akad_content);
		$akad_content = str_replace("#table_pembayaran_eng#", $table_pembayaran_eng, $akad_content);
	}
	?>
	<?php include_once '../header.php'; ?>
	<div class="container-fluid" id="content">
		<div class="row">
			<div class="col-lg-12" align="center">
				<h3 class="my-3">Akad Editor</h3>
				<br>
				<div class="akad-content">
					<a href="/admin/contract_ukm.php?c=<?php echo $contract->campaign_id ?>" class="btn btn-default btn-sm pull-left">Back</a>
					<button class="btn btn-primary btn-sm pull-right generate-contract" data-id="<?php echo $contract->id ?>" style="margin-left: 10px;">Generate to PDF</button>
					<a href="/admin/akad-generator/generate.php?id=<?php echo $contract->id ?>" target="_blank" class="btn btn-info btn-sm pull-right" style="margin-left: 10px;">Preview Akad</a>
					<a href="/admin/akad-generator/edit-contract.php?id=<?php echo $contract->id ?>&r=t" class="btn btn-danger btn-sm pull-right" style="margin-left: 10px;">Reload Datas</a>
					<div class="clearfix"></div>
					<br>
					<form method="post" action="/admin/akad-generator/edit-contract.php?id=<?php echo $_GET['id'] ?>">
						<input type="hidden" name="id" value="<?php echo $contract->id ?>">
						<label for="">Akad Content</label>
						<textarea id="editor-content" name="akad_content"><?php echo $akad_content ?></textarea>
						<br>
						<br>
						<label for="">Akad Signature</label>
						<textarea id="editor-signature" name="akad_signature"><?php echo $akad_signature ?></textarea>
						<br>
						<!-- <div class="row">
						<div class="col-md-6">
						<label for="">Akad Appendix 2 (English)</label>
						<input type="text" id="editor-appendix-1-eng" name="akad_appendix_1_eng" class="form-control" value="<//?php echo $contract->akad_appendix_1_eng ?>">
					</div>
					<div class="col-md-6">
					<label for="">Akad Appendix 2 (Bahasa)</label>
					<input type="text" id="editor-appendix-1-ina" name="akad_appendix_1_ina" class="form-control" value="<//?php echo $contract->akad_appendix_1_ina ?>">
				</div>
			</div> -->
			<br>
			<label for="">Akad Appendix</label>
			<textarea id="editor-appendix" name="akad_appendix_2"><?php echo $akad_appendix ?></textarea>
			<br>
			<label for="">Signature Page Number</label>
			<input type="text" name="signature_page_number" class="form-control" value="<?php echo $contract->signature_page_number ?>">
			<br>
			<button type="submit" class="btn btn-warning btn-lg" style="margin-bottom: 30px;">Save Akad</button>
		</form>
	</div>
</div>
</div>
</div>

<script>
tinymce.init({
	selector: 'textarea',
	plugins: ["table", "pagebreak", "code", "lists", "advlist"],
	toolbar: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | styleselect fontselect fontsizeselect| numlist bullist alphalist',
	fontsize_formats: '10pt 11pt 12pt 14pt 16pt 18pt 24pt 30pt',
	font_formats: 'Helvetica=helvetica;',
	block_formats: 'Paragraph=p; Header 1=h1; Header 2=h2; Header 3=h3',
	width : '100%',
	height : 680,
	table_grid: false
});

$(document).ready(function() {
	$(".generate-contract").click(function() {
		$("#loading .text-loading").text("Generating Contract to PDF...");
		$("#loading").removeClass("hide");

		id = $(this).data('id');

		$.ajax({
			type: 'GET',
			url: "./generate.php?action=g&id="+id,
			contentType: false,
			cache: false,
			processData:false,
			success: function(callback){
				var result = JSON.parse(callback);

				if (result['result']) {
					$elm = "<h3>PDF File successfully to create.</h3>";
					$("#loading .animation-size").html($elm);
				}else{
					$elm = "<h3>Failed to generate PDF.</h3>";
					$("#loading .animation-size").html($elm);

				}
				setTimeout(function() {
					$("#loading").addClass("hide");
				}, 1500);

			}
		})
	})
})
</script>
</body>
</html>
