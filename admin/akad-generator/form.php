<!DOCTYPE html>
<html>
<?php
include_once '../security.php';
include_once '../mysql.php';
include_once '../lib/helper.php';
$mysql = new mysql();
?>
<head>
	<?php include_once '../initialize.php'; ?>
	<title>Kapitalboost | Akad Generator</title>
	<?php include_once '../include.php'; ?>
	<!-- <script src="https://cdn.tiny.cloud/1/yqtaitloxauc6z9p5d80f32rrxyk12h7t80ckvj5h6x16zvf/tinymce/5/tinymce.min.js"></script> -->
	<script src="https://cdn.tiny.cloud/1/kjwvahkftdzi9tbun3uhmtrvb01k9419m6vy9eiahopwc8e0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

	<style type="text/css">
	#content.container-fluid {
		margin-top: 80px;
	}
	body{
		background: #FFF !important;
	}
	.akad-content {
		width: 794px; height: 1123px;margin: auto;
	}
	</style>
</head>
<body>
	<!-- Get Datas From DB -->
	<?php
	$types = array('1' => 'Wakalah', '2' => 'Crowdfunding');
	if ($mysql->Connection()) {
		if ($_POST) {
			$id = $_POST['id'];
			unset($_POST['id']);
			
			list($result, $err, $saved_id) = $mysql->updateDatas($_POST, 'akad', 'id', $id);
			// print_r($_POST);
			// echo "<hr>";
			// print_r($err);
			// exit();
		}

		$akad = $mysql->getDatas('akad', 'id', $_GET['id']);
	}
	?>
	<?php include_once '../header.php'; ?>
	<div class="container-fluid" id="content">
		<div class="row">
			<div class="col-lg-12" align="center">
				<h3 class="my-3">Akad Editor (<?php echo $types[$akad->type] ?>)</h3>
				<div class="akad-content">
					<a href="/admin/akad-generator/" class="btn btn-default pull-left">Back</a>
					<a href="/admin/akad-generator/preview.php?id=<?php echo $akad->id ?>" target="_blank" class="btn btn-primary pull-right">Preview Akad</a>
					<div class="clearfix"></div>
					<br>
					<form method="post">
						<input type="hidden" name="id" value="<?php echo $akad->id ?>">
						<label for="">Akad Content</label>
						<textarea id="editor-content" name="content"><?php echo $akad->content ?></textarea>
						<br>
						<br>
						<label for="">Akad Signature</label>
						<textarea id="editor-signature" name="signature"><?php echo $akad->signature ?></textarea>
						<br>
						<label for="">Akad Appendix</label>
						<textarea id="editor-appendix" name="appendix"><?php echo $akad->appendix ?></textarea>
						<br>
						<button type="submit" class="btn btn-warning btn-lg" style="margin-bottom: 30px;">Save Akad</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<script>
	tinymce.init({
		selector: 'textarea',
		plugins: ["table", "pagebreak", "code", "lists", "advlist"],
		toolbar: 'undo redo | alignleft aligncenter alignright alignjustify | bold italic underline | styleselect fontselect fontsizeselect| numlist bullist alphalist',
		fontsize_formats: '10pt 11pt 12pt 14pt 16pt 18pt 24pt 30pt',
		font_formats: 'Helvetica=helvetica;',
		block_formats: 'Paragraph=p; Header 1=h1; Header 2=h2; Header 3=h3',
		width : '100%',
		height : 680,
		table_grid: false
	});
	</script>
</body>
</html>
