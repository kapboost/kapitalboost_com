<?php
require '../lib/fpdi/tcpdf/tcpdf_config_alt.php';
require '../lib/fpdi/tcpdf/tcpdf.php';

include_once '../security.php';
include_once '../mysql.php';
include_once '../lib/helper.php';

$mysql = new mysql();
$reg_type_eng = array('0' => '----------', '1' => 'Company Registration No.', '2' => 'Single Business No.');
$reg_type_ina = array('0' => '----------', '1' => 'Nomor Tanda Daftar Perusahaan (TDP)', '2' => 'Nomor Induk Berusaha (NIB)');

if ($mysql->Connection()) {
	$contract_ukm = $mysql->getDatas('tcontract_ukm', 'id', $_GET['id'], false);
	$contract_info = $mysql->getDatas('tcontract', 'campaign_id', $contract_ukm->campaign_id, false);
	$contract_document = $mysql->getDatas('tcontract_images', 'tcontract_ukm_id', $contract_ukm->id, true);

	$akad_content = $contract_ukm->akad_content;
	$currencyFields = array("target_funding_amount", "wakalah_fee_investor", "wakalah_fee_kb");
	foreach ($contract_ukm as $key => $value) {
		if ("#$key#" == "#company_reg_type_eng#") {
			$akad_content = str_replace("#$key#", $reg_type_eng[$value], $akad_content);
		}
		else if ("#$key#" == "#company_reg_type_ina#") {
			$akad_content = str_replace("#$key#", $reg_type_ina[$value], $akad_content);
		}else{
			if (in_array($key, $currencyFields)) {
				$currency_value = number_format($value);
				$akad_content = str_replace("#$key#", $currency_value, $akad_content);	
			}else{
				$akad_content = str_replace("#$key#", $value, $akad_content);
			}
		}
	}
	$akad_content = str_replace("#subs_agency_fee#", $contract_info->subs_agency_fee, $akad_content);
	$akad_content = str_replace("#date_created_eng#", getToday($lang = 'eng'), $akad_content);
	$akad_content = str_replace("#date_created_ina#", getToday($lang = 'ina'), $akad_content);

	// print_r($akad_content);

	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

	// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Kapital Boost');
	$pdf->SetTitle('Kapitalboost Akad Crowdfunding');
	$pdf->SetSubject('Akad '.$contract_ukm->contract_number);
	$pdf->SetKeywords('Crowdfunding, Kapitalboost');

	// set default header data
	$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, '', array(0,0,0), array(0,0,0), PDF_HEADER_X_POSSION);

	// set header and footer fonts
	$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
	$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

	// set default monospaced font
	$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

	// set margins
	$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
	$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
	$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

	// set auto page breaks
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

	// set image scale factor
	$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

	// set some language-dependent strings (optional)
	// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	// 	require_once(dirname(__FILE__).'/lang/eng.php');
	// 	$pdf->setLanguageArray($l);
	// }

	// set font
	$pdf->SetFont('Helvetica', '', 10);

	// -------------------- ######## ----------------------
	// add a page for texts
	$pdf->AddPage();
	// set list indent
	$pdf->setListIndentWidth(4);
	// output the HTML content
	$pdf->writeHTML($akad_content, true, false, true, false, '');

	// reset pointer to the last page
	$pdf->lastPage();

	// -------------------- ######## ----------------------
	// add a page for sign
	$pdf->AddPage();
	$pdf->writeHTML($contract_ukm->akad_signature, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();

	$pdf->AddPage();
	$pdf->writeHTML($contract_ukm->akad_appendix_2, true, false, true, false, '');
	// reset pointer to the last page
	$pdf->lastPage();

	// -------------------- ######## ----------------------
	// add a page for Documents
	$midx = 1;
	foreach ($contract_document as $document) {
		//$gambar = 'C:/xampp/htdocs/KB/kapitalboost_com/'.$image['path'] .''. $document['file_name'];
	  //$gambar = "https://kapitalboost.com/assets/uploads/contract_ukm/CU_$id/" .''. $document['file_name'];
	  $gambar = "/var/www/kapitalboost.com/public_html/assets/uploads/contract_ukm/CU_$contract_ukm->id/" .''. $document['file_name'];
	  // $gambar = "/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com/assets/uploads/contract_ukm/CU_$contract_ukm->id/" .''. $document['file_name'];

		$pdf->AddPage();
		list($width1, $height1, $type1, $attr1) = getimagesize($gambar);
		// $height1 = $height1/($width1/180);
		// $minTheight = 150;
		// if ($height1>$minTheight){
		//     $theight=$minTheight-30;
		//   }else{
		    // $theight=null;
		//   }
		 $maxWidth = 180;
		if ($width1 < $maxWidth) {
			$maxWidth = $width1 - 30;
		}

	  $y = 30;
		if ($midx == 1) {
			$pdf->writeHTML("<b>APPENDIX 2: Invoice from the Company to its Customer/(s)</b><br/><b>LAMPIRAN 2: Kuitansi dari Perusahaan kepada Bowheer</b>", 20);
			$y += 10;
		}
		$pdf->Image($gambar, 15, $y, $maxWidth, null, '', '', '', true, 150, '', false, false, 1, false, false, false);
		$pdf->lastPage();
		$midx += 1;
	}


	if (isset($_GET['action']) && $_GET['action'] == 'g') {
		$base_path = "/var/www/kapitalboost.com/public_html";
		// $base_path = "/Applications/XAMPP/htdocs/MyWorks/com/kapitalboost_com";
		$pdf_path = "/assets/uploads/contract_ukm/CU_$contract_ukm->id/";

		if(!is_dir($base_path . $pdf_path)) {
		  mkdir($base_path . $pdf_path, 0777, false);
		}
		if(!is_dir($base_path . $pdf_path.'pdf_file/')) {
		  mkdir($base_path . $pdf_path.'pdf_file/', 0777, false);
		}

		$file_output = $pdf_path . 'pdf_file/' . $contract_ukm->company_code . '.pdf';

		$output = $pdf->Output($base_path . $file_output, 'F');

		if ($output) {
		  $params = array('is_generated' => $output, 'pdf_file' => $file_output);
		  list($result, $err, $id) = $mysql->updateDatas($params, 'tcontract_ukm', 'id', $contract_ukm->id);

		  echo json_encode(array('status' => 200, 'result' => $result, 'err' => $err, 'pdf_file' => $file_output));
		}else{
		  echo json_encode(array('status' => 200, 'result' => 0, 'err' => 'ops, somthing when wrong', 'pdf_file' => null));
		}
	}else{
		$pdf->Output('AkadCF.pdf', 'I');
	}
}else{
	echo "Something When Wrong.";
}

?>
