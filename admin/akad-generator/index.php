<!DOCTYPE html>
<html>
<?php
	include_once '../security.php';
	include_once '../mysql.php';
	include_once '../lib/helper.php';
	$mysql = new mysql();

	$type = isset($_GET['type']) && (int)$_GET['type'] <= 2 ? $_GET['type'] : 1;
?>
<head>
	<?php include_once '../initialize.php'; ?>
	<title>Kapitalboost | List Akad</title>
	<?php include_once '../include.php'; ?>

	<style type="text/css">
		#content.container {
			margin-top: 80px;
		}
		body{
			background: #FFF !important;
		}
	</style>
</head>
<body>
	<!-- Get Datas From DB -->
	<?php
		$types = array('1' => 'Wakalah', '2' => 'Crowdfunding Agreement');
		if ($mysql->Connection()) {
			if ($_POST) {
				$id = $_POST['id'];
          		unset($_POST['id']);

				list($result, $err, $saved_id) = $mysql->updateDatas($_POST, 'akad', 'id', $id);
			}

			$akad = $mysql->getDatas('akad');
		}
	?>
	<?php include_once '../header.php'; ?>
	<div class="container" id="content">
		<div class="row">
			<div class="col-lg-8">
				<h3 class="my-3">List Akad</h3>
			</div>
			<div class="col-lg-4" align="right">
				<br>
				<div class="dropdown">
				  <button class="btn btn-default btn-xs dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<i class="fa fa-plus fa-fw"></i>
						Create New Akad
				    <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu1">
				    <li><a href="#" class="create-akad" data-type="1">Akad Wakalah</a></li>
				    <li><a href="#" class="create-akad" data-type="2">Akad Crowdfunding</a></li>
				  </ul>
				</div>
			</div>
			<div class="col-lg-12">
				<table class="table table-bordered">
					<tr>
						<th>No</th>
						<th>Type</th>
						<th>Updated at</th>
						<th>Action</th>
					</tr>
					<?php $no=0; foreach ($akad as $akd): ?>
						<tr>
							<td><?php echo $no+=1; ?></td>
							<td><?php echo $types[$akd['type']] ?></td>
							<td><?php echo $akd['updated_at'] ?></td>
							<td>
								<a href="/admin/akad-generator/preview.php?id=<?php echo $akd['id'] ?>" target="_blank" class="btn btn-primary btn-xs"><i class="fa fa-file-o fa-fw"></i> Preview Akad</a>
								<?php if ($akd['is_active']): ?>
									<a href="/admin/akad-generator/form.php?id=<?php echo $akd['id'] ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit fa-fw"></i> Edit</a>
									<button type="button" class="btn btn-xs btn-danger remove-akad" data-value="0" data-id="<?php echo $akd['id'] ?>">Deactivate</button>
								<?php else: ?>
									<button type="button" class="btn btn-xs btn-default remove-akad" data-value="1" data-id="<?php echo $akd['id'] ?>">Activate</button>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			$(".create-akad").click(function() {
				$type = $(this).data('type');

				$.ajax({
          type: 'POST',
          url: '/admin/api.php?api_action=create-akad',
          contentType: false,
          cache: false,
          processData:false,
          success: function(callback){
            var result = JSON.parse(callback);
            // console.log(result);

            alert(result['message']);
          }
        })
			})

			$(".remove-akad").click(function() {
				$id = $(this).data('id');
				$val = $(this).data('value');

				$.ajax({
          type: 'GET',
          url: '/admin/api.php?api_action=delete-akad&id='+$id+'&val='+$val,
          contentType: false,
          cache: false,
          processData:false,
          success: function(callback){
            var result = JSON.parse(callback);
            // console.log(result);
						location.reload();
            // alert(result['message']);
          }
        })
			})
		})
	</script>
</body>
</html>
