<?php
    ini_set('display_errors', '1');
    ini_set('error_reporting', E_ALL);
?>
<?php

include_once 'mysql.php';
$mysql = new mysql();


if (isset($_POST["job"])) {

   $job = $_POST["job"];
   switch ($job) {
      case "updatePayout":
         $iId = $_POST["iId"];
         $dId = $_POST["dId"];
         $amount = $_POST["amount"];
         $date = $_POST["date"];
         $status = $_POST["status"];
         $email = $_POST["email"];
         $cname = $_POST["cname"];
         if ($date != "") {
            $date = date("Y-m-d", strtotime($date));
         }

         if ($mysql->Connection()) {
			if ($status == "3" ){
				$member_id = $mysql->GetMemberIdbyEmail($email);
				$memberWalletBalance = $mysql->GetMmeberBalance($member_id);
				$newBalance = $memberWalletBalance + $amount;
				$mysql->UpdateMemberWalletBalance($member_id, $newBalance);
				$mysql->InsertSingleTransaction($member_id, "$cname Payout", "Payout", $amount);
			}
            if ($mysql->UpdatePayout($iId, $dId, $amount, $date, $status)) {
               echo json_encode("Successful");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }
         break;

      case "updategeneralaboutus":
         $headers = $_POST["headers"];
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageAboutUs($headers, $contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updategeneralaboutusid":
         $headers = $_POST["headers"];
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageAboutUsId($headers, $contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updategeneralcontactus":
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageContactUs($contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updategeneralfaqs":
         $headers = $_POST["headers"];
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageFAQs($headers, $contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updategeneralfaqsid":
         $headers = $_POST["headers"];
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageFAQsId($headers, $contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updategenerallegal":
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageLegal($contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updatecareer":
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageCareer($contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "updategenerallegalid":
         $contents = $_POST["contents"];
         if ($mysql->Connection()) {
            if ($mysql->UpdateGeneralPageLegalId($contents)) {
               echo json_encode("Changes Made");
            } else {
               echo json_encode("Failed");
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "getallnationalities":
         if ($mysql->Connection()) {
            $nationalities = $mysql->GetAllNationality();
            echo json_encode($nationalities);
         } else {
            echo json_encode("Database Error");
         }
         break;


      case "getallcountries":
         if ($mysql->Connection()) {
            $countries = $mysql->GetAllCountries();
            if (isset($_POST["selected"])) {

               for ($i = 0; $i < count($countries); $i++) {
                  if ($countries[$i] == $_POST["selected"]) {
                     $countries[$i] = "<option value='" . $countries[$i] . "' selected>" . $countries[$i] . "</option>";
                  } else {
                     $countries[$i] = "<option value='" . $countries[$i] . "'>" . $countries[$i] . "</option>";
                  }
               }
               echo json_encode($countries);
            } else {
               for ($i = 0; $i < count($countries); $i++) {
                  $countries[$i] = "<option value='" . $countries[$i] . "'>" . $countries[$i] . "</option>";
               }
               echo json_encode($countries);
            }
         } else {
            echo json_encode("Database Error");
         }
         break;

      case "getallcountriessimple":
         if ($mysql->Connection()) {
            $countries = $mysql->GetAllCountries();
            echo json_encode($countries);
         } else {
            echo json_encode("Database Error");
         }
         break;

      case "getcampaignnames":
         if ($mysql->Connection()) {
            list($cName, $cId) = $mysql->GetAllCampaignName();
            for ($i = 0; $i < count($cId); $i++) {
               $cName[$i] = $cName[$i] . " ~" . $cId[$i];
            }
            echo json_encode($cName);
         } else {
            echo json_encode("Database Error");
         }
         break;

      case "refreshtotalfunding":
         if ($mysql->Connection()) {
            if (isset($_POST["cId"])) {
               $cId = $_POST["cId"];
			   // echo "'<script>console.log(\"cId : $cId\")</script>'";
               $mysql->RefreshFunding($cId);
            }
         } else {
            echo json_encode("Database Error");
         }
         break;
		 
      case "duplicatecampaign":
         if ($mysql->Connection()) {
            if (isset($_POST["cId"])) {
               $cId = $_POST["cId"];
			   echo "'<script>console.log(\"cId : $cId\")</script>'";
               $mysql->DuplicateCampaign($cId);
            }
         } else {
            echo json_encode("Database Error");
         }
         break;

      case "validatedalter":

         if ($mysql->Connection()) {
            if (isset($_POST["mId"]) && isset($_POST["val"])) {
               $mId = $_POST["mId"];
               $val = $_POST["val"];
               echo $mysql->UpdateValidated($mId, $val);
            }
         } else {
            echo json_encode("Database Error");
         }

         break;

      case "xferspayout":
         $iId = $_POST["iId"];
         $mId = $_POST["mId"];
         $name = $_POST["name"];
         $xfersToken = $_POST["xfersToken"];
         $amount = $_POST["amount"];
         $cName = $_POST["cName"];
         $description = "Payout $mId for $cName Campaign to $name";
         $invoice_id = $_POST["invoiceId"];
         $tpayout = $_POST["tpayout"];
         $emailAddr = $_POST["emailAddr"];
		 $share= $amount/$tpayout*100;
		 $share= number_format($share, 0);;
		 

         $arr_xudata = array(
             "amount" => $amount,
             "invoice_id" => $invoice_id,
             "user_api_token" => $xfersToken,
             "descriptions" => $description
         );

         $xudata = json_encode($arr_xudata);

         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => "https://www.xfers.io/api/v3/payouts",
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_SSL_VERIFYPEER => false,
             CURLOPT_ENCODING => "",
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 60,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "POST",
             CURLOPT_POSTFIELDS => $xudata,
             CURLOPT_HTTPHEADER => array(
                 "cache-control: no-cache",
                 "content-type: application/json",
                 // "x-xfers-user-api-key: oNSDZeAAy9Bnj6NtyzCLQ9HPuoyzcReuUnPgmT_MmKA"
                 "x-xfers-user-api-key: CyuGadY-P5KqvJYYCZQJm6DdjFnPdsG-pm9UPAqZ5Fo"
             ),
         ));

         $response = curl_exec($curl);
         $error = curl_error($curl);

         curl_close($curl);
         $response1 = json_decode($response, true);

         if ($response1["status"] == "completed") {

            if ($mysql->Connection()) {
               $mysql->UpdateEachPayoutStatus($iId, $mId);
            }
			
			$message = "
			 <html>
				   <head>
						  <title>Kapital Boost's Enquiry</title>
				   </head>
				   <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
						  <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
								 <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
										<img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
										<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
											   Dear " . $name . ",
										</p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
											   We have made a transfer of SGD" . $amount . " to your Xfers e-wallet. This is $share% of the total payout of SGD$tpayout from your investment in the " . $cName . " crowdfunding project.
										</p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
											    To view the proof of transfer, please visit the <a href='https://kapitalboost.com/dashboard/payment-detail/$iId'>Project page</a> in your dashboard.
										 </p>
										 <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
												You should have received a payment confirmation email from Xfers.io. If not, please contact support@kapitalboost.com.
										 </p>
										<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
											   Best regards,<br><br>Kapital Boost
										</p>
								 </div>
								 <div style='width:100%;height:7px;background-color:#00b0fd'></div>
								 <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
										<p style='font-size: 12px;color: white;text-align:center'>
											   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
										</p>
								 </div>
						  </div>
				   </body>
			</html>
			";

                  include_once 'email.php';
                  $email = new email();
                  $email->SendEmail($emailAddr, "admin@kapitalboost.com", "Kapital Boost - Proof of payout for $cName campaign", $message);
					// echo "'<script>console.log(\"emailAddr : $emailAddr\")</script>'";

            echo json_encode("success");
         } else {
            echo json_encode("failed");
         }

         break;
		 
      case "walletpayout":
         $iId = $_POST["iId"];
         $mId = $_POST["mId"];
         $name = $_POST["name"];
         $amount = $_POST["amount"];
         $cName = $_POST["cName"];
         $description = "Payout $mId for $cName Campaign to $name";
         $invoice_id = $_POST["invoiceId"];
         $tpayout = $_POST["tpayout"];
         $emailAddr = $_POST["emailAddr"];
		 $share= $amount/$tpayout*100;
		 $share= number_format($share, 0);
		 $currentBalance = 0;
		 
		 //ambil jumlah wallet sekarang
		 if ($mysql->Connection()) {
			$memberId = $mysql->GetMemberIdbyEmail($emailAddr);
			$currentBalance = $mysql->GetMmeberBalance($memberId);
         }
		 
		 $payoutWallet = $currentBalance + $amount;
         

            if ($mysql->Connection()) {
				//tambahkanwallet
			   $transDesc = "$cName - payout";
			   $mysql->UpdateMemberWalletBalance($memberId, $payoutWallet);
			   $mysql->InsertSingleTransaction($memberId, $transDesc, "Payout", $amount);
               $mysql->UpdateEachPayoutStatus($iId, $mId); 
            }
			
			$message = "
			 <html>
				   <head>
						  <title>Kapital Boost's Enquiry</title>
				   </head>
				   <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
						  <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
								 <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
										<img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
										<p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
											   Dear " . $name . ",
										</p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
											   We have made a transfer of SGD" . number_format($amount, 1, '.', ',') . " to your Kapital Boost e-wallet. This is $share% of the total payout of SGD".number_format($tpayout, 1, '.', ',')." from your investment in the " . $cName . " crowdfunding project.
										</p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
											    You may check the payout transfer and the outstanding balance of your e-wallet in your Kapital Boost dashboard.
										 </p>
										<p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
											    If you would like to withdraw the funds from your e-wallet, please provide us with your bank account details (account name, bank name, account number, and bank Swift/BIC code) and we shall make the transfer within 2-3 days. You may also use the amount in your e-wallet for future crowdfunding investments. 
										 </p>
										<p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
											   Best regards,<br><br>Kapital Boost
										</p>
								 </div>
								 <div style='width:100%;height:7px;background-color:#00b0fd'></div>
								 <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
										<p style='font-size: 12px;color: white;text-align:center'>
											   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
										</p>
								 </div>
						  </div>
				   </body>
			</html>
			";

                  include_once 'email.php';
                  $email = new email();
                  $email->SendEmail($emailAddr, "admin@kapitalboost.com", "Kapital Boost - Proof of payout for $cName campaign", $message);
					// echo "'<script>console.log(\"emailAddr : $emailAddr\")</script>'";

            echo json_encode("success");

         break;

      case 'payViaBank':
         $iId = $_POST["iId"];
         $mId = $_POST["mId"];
         $name = $_POST["name"];
         $amount = $_POST["amount"];
         $cName = $_POST["cName"];
         $description = "Payout $mId for $cName Campaign to $name";
         $invoice_id = $_POST["invoiceId"];
         $tpayout = $_POST["tpayout"];
         $emailAddr = $_POST["emailAddr"];
         $share= $amount/$tpayout*100;
         $share= number_format($share, 0);
         $currentBalance = 0;

         if ($mysql->Connection()) {
            $mysql->UpdateEachPayoutStatus($iId, $mId); 
         }

         $message = "
             <html>
                   <head>
                          <title>Kapital Boost's Enquiry</title>
                   </head>
                   <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
                          <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
                                 <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
                                        <img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
                                        <p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
                                               Dear " . $name . ",
                                        </p>
                                        <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                               We have made a transfer of SGD" . $amount . " to your bank account. This is $share% of the total payout of SGD$tpayout from your investment in the " . $cName . " crowdfunding project.
                                        </p>
                                         <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                                If you do not recive the funds please contact support@kapitalboost.com.
                                         </p> 
                                        <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                               Best regards,<br><br>Kapital Boost
                                        </p>
                                 </div>
                                 <div style='width:100%;height:7px;background-color:#00b0fd'></div>
                                 <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                                        <p style='font-size: 12px;color: white;text-align:center'>
                                               &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
                                        </p>
                                 </div>
                          </div>
                   </body>
            </html>
            ";
         include_once 'email.php';
         $email = new email();
         $email->SendEmail($emailAddr, "admin@kapitalboost.com", "Kapital Boost - Proof of payout for $cName campaign", $message);

         echo json_encode("success");
         break;

      case 'removeinvimglink':
         $iId = $_POST["iId"];
         $dId = $_POST["dId"];
         $imgStr = "";
         if ($mysql->Connection()) {
            $images = $mysql->GetInvestmentImage($iId);
            $imagesA = explode("~", $images);
            $imagesA[$dId - 1] = "";
            for ($i = 0; $i < 12; $i++) {
               $imgStr .= "$imagesA[$i]~";
            }
            if ($mysql->UpdateInvestmentImage($imgStr, $iId)) {
               echo json_encode("Delete Successful");
            } else {
               echo json_encode("Delete Successful");
            }
         }

         break;

      case 'updatecontract':

         $cId = $_POST["cId"] ?: '  ';
         $conType = $_POST["conType"] ?: '  ';
         $comName = $_POST["comName"] ?: '  ';
         $comReg = $_POST["comReg"] ?: '  ';
         $purchaseAsset = $_POST["purchaseAsset"] ?: '  ';
         $assetCost = $_POST["assetCost"] ?: '  ';
         $salePrice = $_POST["salePrice"] ?: '  ';
         $assetCostCurr = $_POST["assetCostCurr"] ?: '  ';
         $exchange = $_POST["exchange"] ?: '  ';
         $profit = $_POST["profit"] ?: '  ';
         $foreignCurr = $_POST["foreignCurr"] ?: '  ';
         $dirTitle = $_POST["dirTitle"] ?: '  ';
         $dirName = $_POST["dirName"] ?: '  ';
         $dirEmail = $_POST["dirEmail"] ?: '  ';
         $dirIc = $_POST["dirIc"] ?: '  ';
         $comNameId = $_POST["comNameId"] ?: '  ';
         $comRegId = $_POST["comRegId"] ?: '  ';
         $camReturn = $_POST["camReturn"] ?: '  ';
         $purchaseAssetId = $_POST["purchaseAssetId"] ?: '  ';
         $dirIcId = $_POST["dirIcId"] ?: '  ';
         $closdate = $_POST["closdate"] ?: '  ';
         $invoContractNo = $_POST["invoContractNo"] ?: '  ';
         $invoAktaNo = $_POST["invoAktaNo"] ?: '  ';
         $invoFeePaymentDtae = $_POST["invoFeePaymentDtae"] ?: '  ';
         $invoAgencyFee = $_POST["invoAgencyFee"] ?: '  ';
         $invoMaturityDate = $_POST["invoMaturityDate"] ?: '  ';
         $invoRepaymentDate = $_POST["invoRepaymentDate"] ?: '  ';
         $invoSubsAgencyFee = $_POST["invoSubsAgencyFee"] ?: '  ';
         $invoDocs = $_POST["invoDocs"] ?: '  ';
         $invoDocsId = $_POST["invoDocsId"] ?: '  ';
         $invoAktaNoId = $_POST["invoAktaNoId"] ?: '  ';
         $invoAssetCostSGD = $_POST["invoAssetCostSGD"] ?: '  ';
         $invoAssetCost = $_POST["invoAssetCost"] ?: '  ';
         // $admin_fee_pct = $_POST["admin_fee_pct"] ?: '  ';
         // $admin_fee_amt = $_POST["admin_fee_amt"] ?: '  ';
		 if (is_numeric ($profit)) {}else{$profit=0;}

         if ($mysql->Connection()) {
            $result = $mysql->UpdateContracts($cId, $conType, $comName, $comReg, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurr, $dirName,$dirEmail, $dirTitle, $dirIc, $comNameId, $comRegId, $purchaseAssetId,  $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost);
			// echo "'<script>console.log(\"amount : $result\")</script>'";
            echo json_encode($result);
         }

         break;
         
      // case 'updatemembercountry':

         // $mId = $_POST["mId"] ?: '  ';
         // $ic_country = $_POST["ic_country"] ?: '  ';
         // $auto_contract = $_POST["auto_contract"] ?: '  ';

         // if ($mysql->Connection()) {
            // $results = $mysql->UpdateMemberCountry($mId, $ic_country, $auto_contract);
			
            // echo json_encode($results);
         // }

         // break;
         
      case 'updatefollowmaster':
         
         $iId = $_POST["iId"];
         $val = $_POST["val"];
         
         if($mysql->Connection()){
            $result = $mysql->UpdateFollowMaster($iId, $val);
            echo $result;
         }
         
         break;
         
     

      default:
         return;
   }
}

if (isset($_GET["duty"])) {
   $duty = $_GET["duty"];

   ini_set('display_errors', 1);
   error_reporting(E_ALL);

   switch ($duty) {

      case "updatePayout":

         $iId = $_GET["iId"];
         $dId = $_GET["dId"];
         $emailAddr = $_GET["email"];
         $fullName = $_GET["name"];
         $amount = $_GET["amount"];
         $cName = $_GET["cName"];
         $pType = $_GET["pType"];
         $tPayout = $_GET["tPayout"];
         $share = $_GET["share"];

         $imageFile = $_FILES["file$dId"]["tmp_name"];
         $oriName = $_FILES["file$dId"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time() . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/payouts/$name")) {
            if ($mysql->Connection()) {
               $images = $mysql->GetInvestmentImage($iId);
               $imgStr = "";
               if ($images == "") {
                  for ($i = 0; $i < 12; $i++) {
                     if ($i == $dId - 1) {
                        $imgStr .= $name;
                     }
                     $imgStr .= "~";
                  }
               } else {
                  $imagesA = explode("~", $images);
                  $imagesA[$dId - 1] = $name;
                  for ($i = 0; $i < 12; $i++) {
                     $imgStr .= "$imagesA[$i]~";
                  }
               }

               $imgStr;
               if ($mysql->UpdateInvestmentImage($imgStr, $iId)) {

                  echo json_encode(["Image Upload Successful. Email Sent to $emailAddr", $name]);

                  if ($pType == 'Xfers' || $pType == 'xfers') {
                     $addOn = "";
                     $addOn1 = "Xfers e-wallet";
                     $addOn2 = "You should have received a payment confirmation email from Xfers.io. If not,";
                  } else {
                     $addOn = "Please allow 2-5 working days for the funds to reach your account. ";
                     $addOn1 = "bank account";
                     $addOn2 = "If you do not receive the funds transfer,";
                  }

                  $message = "
 <html>
       <head>
              <title>Kapital Boost's Enquiry</title>
       </head>
       <body style='background: #305e8e;height: 100%;width: 100%;padding: 0;margin: 0;'>
              <div style='background-color:#305e8e;text-align: left;margin: 0;border:20px solid #305e8e'>
                     <div style='width: 100%;padding: 0;margin: 0 0 0 0;background: white;'>
                            <img src='https://kapitalboost.com/assets/images/logo-baru.png'  style='margin: 3% 0 0 3%;width:30%;'/>
                            <p style='margin:3% 3% 3% 3%;font-size:22px;color:#00adfd;' >
                                   Dear " . $fullName . ",
                            </p>
                            <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                   We have made a transfer of SGD" . $amount . " to your $addOn1. This is $share% of the total payout of SGD$tPayout from your investment in the " . $cName . " crowdfunding project.
                            </p>
                            <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                   $addOn To view the proof of transfer, please visit the <a href='https://kapitalboost.com/dashboard/payment-detail/$iId'>Project page</a> in your dashboard.
                             </p>
                             <p style='margin:0 3% 3% 3%;text-align:left;color:black;font-size:16px;'>
                                    $addOn2 please contact support@kapitalboost.com.
                             </p> 
                            <p style='padding:0 3% 3% 3%;text-align:left;color:black;font-size:16px;margin-bottom:0'>
                                   Best regards,<br><br>Kapital Boost
                            </p>
                     </div>
                     <div style='width:100%;height:7px;background-color:#00b0fd'></div>
                     <div style='position: fixed;bottom: 0;left: 0;height: auto;width: 100%;padding: 15px 0;margin: 0;background: #323c51;'>
                            <p style='font-size: 12px;color: white;text-align:center'>
                                   &copy; COPYRIGHT 2018 - KAPITAL BOOST PTE LTD
                            </p>
                     </div>
              </div>
       </body>
</html>
";

                  include_once 'email.php';
                  $email = new email();
                  $email->SendEmail($emailAddr, "admin@kapitalboost.com", "Kapital Boost - Proof of payout for $cName campaign", $message);
               } else {
                  echo json_encode(["Linking Error", ""]);
               }
            }
         } else {
            echo json_encode(["Upload Failed", ""]);
         }



         break;
         
      case 'updatecontractimage1':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg1($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;   
      case 'updatecontractimage2':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg2($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;   
      case 'updatecontractimage3':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg3($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;   
      case 'updatecontractimage4':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg4($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;   
      case 'updatecontractimage5':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg5($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;  
      case 'updatecontractimage6':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg6($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;  
      case 'updatecontractimage7':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg7($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;  
      case 'updatecontractimage8':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg8($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;  
      case 'updatecontractimage9':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg9($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;  
      case 'updatecontractimage10':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg10($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;  
      case 'updatecontractimage11':
         $cId = $_GET["cId"];
         $imageFile = $_FILES["file"]["tmp_name"];
         $oriName = $_FILES["file"]["name"];
         $ext = explode(".", $oriName);
         $type = end($ext);
         $name = time(). mt_rand(0,100000) . ".$type";
         if (move_uploaded_file($imageFile, "../assets/images/pdfimg/$name")) {
            if($mysql->Connection()){
               $result[0] = $mysql->UpdateContractImg11($cId, $name);
               $result[1] = $name;
               echo json_encode($result);
            }
         }
         
         break;


      default:
         break;
   }
}