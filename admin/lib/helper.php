<?php
	function contract_number_generator($mysql) {
		$code = date("my");
		$sql = $mysql->conn->prepare("SELECT tcontract_ukm.* FROM tcontract_ukm WHERE contract_number LIKE '%$code%' order by id DESC LIMIT 1");
		$sql->execute();

		$mysql->result = $sql->get_result();
		if ($mysql->result->num_rows > 0) {
			$data = $mysql->result->fetch_object();
			$currCode = explode("/", $data->contract_number);
			$newCode = (int)$currCode[0] + 1;
			$newCode = (strlen((string)$newCode) == 1) ? "0".$newCode : $newCode;

			$gCode = "$newCode/$code";
		}else{
			$gCode = "01/".$code;
		}

		return $gCode;
	}

	function company_code_generator($company_name) {
		if ($company_name) {
			$words = explode(" ", $company_name);
			$acronym = "";

			foreach ($words as $w) {
				if ($w == "PT" || $w == "CV") {
					$acronym .= $w;
				}else{
			  		$acronym .= $w[0];
				}
			}
		}else{
			$acronym = "";
		}

		return $acronym;
	}

	function auto_filled_contract_datas($mysql, $contract_ukm) {
		$ex_rate = (float)$contract_ukm['exchange_rate'];
		$id = $contract_ukm['id'];
		$params = array();

		$params['target_funding_amount'] = $contract_ukm['total_funding_amt'];
		// print_r($params);
		// exit();

		// if (is_null($contract_ukm['wakalah_fee_kb']) || $contract_ukm['wakalah_fee_kb'] == 0) {
		$subs_agency_fee = (float)$contract_ukm['subs_agency_fee']/100;
		$params['wakalah_fee_kb'] = (float)$contract_ukm['total_funding_amt'] * $subs_agency_fee;
		// }

		// if (is_null($contract_ukm['wakalah_fee_investor']) || $contract_ukm['wakalah_fee_investor'] == 0) {
		$project_return = (float)$contract_ukm['project_return']/100;
		$params['wakalah_fee_investor'] = (float)$contract_ukm['total_funding_amt'] * $project_return;
		// }

		$payoutA = explode("==", $contract_ukm['m_payout']);
		$dateA = array();

		for ($i = 0; $i < count($payoutA); $i++) {
		   $payouts = explode("~", $payoutA[$i]);
		   if (isset($payouts[1])) {
		   	$dateA[$i] = $payouts[1];
		   }
		}

		if ($contract_ukm['repayment_date_eng'] == "0000-00-00" || is_null($contract_ukm['repayment_date_eng'])) {
			$date = strtotime($dateA[0]);
			$params['repayment_date_eng'] = date('Y-m-d', $date);
		}

		if ($contract_ukm['repayment_date_ina'] == "0000-00-00" || is_null($contract_ukm['repayment_date_ina'])) {
			$date = strtotime($dateA[0]);
			$params['repayment_date_ina'] = date('Y-m-d', $date);
		}

		$wakalah_fee_kb = isset($params['wakalah_fee_kb']) ? $params['wakalah_fee_kb'] : $contract_ukm['wakalah_fee_kb'];
		$wakalah_fee_investor = isset($params['wakalah_fee_investor']) ? $params['wakalah_fee_investor'] : $contract_ukm['wakalah_fee_investor'];

		$params['total_payout_sgd'] = (float)$contract_ukm['total_funding_amt'] + (float)$wakalah_fee_kb + (float)$wakalah_fee_investor;
		// if ($contract_ukm['total_payout_sgd'] == 0 || is_null($contract_ukm['total_payout_sgd'])) {
		// }

		if ($contract_ukm['exchange_rate'] != 0 || !is_null($contract_ukm['exchange_rate'])) {
			$wakalah_fee_kb = isset($params['wakalah_fee_kb']) ? $params['wakalah_fee_kb'] : $contract_ukm['wakalah_fee_kb'];
			$wakalah_fee_investor = isset($params['wakalah_fee_investor']) ? $params['wakalah_fee_investor'] : $contract_ukm['wakalah_fee_investor'];

			$params['total_payout_idr'] = $ex_rate * ((float)$contract_ukm['total_funding_amt'] + (float)$wakalah_fee_kb + (float)$wakalah_fee_investor);
		}

		if (is_null($contract_ukm['target_funding_amount']) || $contract_ukm['target_funding_amount'] == "") {
			$params['target_funding_amount'] = $contract_ukm['total_funding_amt'];
		}

		if (is_null($contract_ukm['company_name']) || $contract_ukm['company_name'] == "") {
			$params['company_name'] = $contract_ukm['name'];
		}

		if (is_null($contract_ukm['company_code']) || $contract_ukm['company_code'] == "") {
			$params['company_code'] = company_code_generator($contract_ukm['name']);
		}

		if (count($params) > 0) {
			list($result, $err, $saved_id) = $mysql->updateDatas($params, 'tcontract_ukm', 'id', $id);
		}

		if (isset($result) && $result == 1) {
			$callback = $mysql->getDataContract($contract_ukm['campaign_id']);
			$callback = $callback[0];
		}else{
			$callback = $contract_ukm;
		}

		return $callback;
	}

	function upload_contract($mysql, $id, $file_name, $uFiles, $folder_name, $path='../assets/uploads/contract_ukm/') {
		$location = $path."$folder_name/";

		if(!is_dir($location)) {
            mkdir($location, 0777, false);
        }

		for ($i=0; $i < count($uFiles["files"]["name"]); $i++) {
			$doc_name = $uFiles["files"]["name"][$i];
			$tmp_name = $uFiles["files"]['tmp_name'][$i];
	        $file_array = explode(".", $doc_name);
	        $file_extension = end($file_array);
			$timeStamp= time();
	        $name = $file_name."-".$doc_name;
	        $file_detail = $location.$name;

	        if(move_uploaded_file($tmp_name, $file_detail)){
	        	$sql = "INSERT INTO tcontract_images (tcontract_ukm_id, file_name, folder_path) VALUES ('$id', '$name', '$location')";
	        	$query = $mysql->conn->prepare($sql);
	        	$query->execute();
	        }
		}
	}

	function getToday($lang = 'eng') {
		date_default_timezone_set("Asia/Jakarta");
		$monthsID = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
		$monthsENG = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
		// print_r($date);
		// exit();
		$date_day = date('d');
		$date_month = date('n');
		$date_year = date('Y');
		
		if ($lang == 'ina') {
			$date_month = $monthsID[$date_month];
		}else{
			$date_month = $monthsENG[$date_month];
		}

		return "$date_day $date_month $date_year";
	}

	function create_bilyetgiro_CF($mysql, $contract, $bahasa){
		$months = ["January" => "Januari", "February" => "Februari", "March" => "Maret", "April" => "April", "May" => "Mei", "June" => "Juni", "July" => "Juli", "August" => "Agustus", "September" => "September", "October" => "Oktober", "November" => "November", "December" => "Desember"];
		$totalPayoutIDR = $contract->total_payout_idr;
		list($cName, $mPayout) = $mysql->GetSingleCampaignForPayout($contract->campaign_id);
		$payoutA = explode("==", $mPayout);
		$amountA = $dateA = $statusA = array();
		
		for ($i = 0; $i < count($payoutA); $i++) {
			$payouts = explode("~", $payoutA[$i]);
			$countPayouts = count($payouts);	
		  if ($countPayouts==3){
			  $amountA[$i] = $payouts[0];
			  $dateA[$i] = $payouts[1];
			  $statusA[$i] = $payouts[2];
			}
		}

		$count_mPayout = 0;
		for($j=1; $j < count($payoutA); $j++ ){
			if ($payoutA[$j] != '~~'){
				$count_mPayout = $count_mPayout+1;
			}
		}

		switch ($bahasa){
			case 'ina':
				$string_bg = "";
				$no = 0;
				for ($k=0 ; $k < $count_mPayout; $k++){
					
					$calculate_idr = $totalPayoutIDR * ($amountA[$k]/100);
					$bagi = 500000000;
					$bagi_NF = number_format($bagi);
					$total_payout_NF = number_format($calculate_idr);
					$hasilbagi =  intval($calculate_idr / $bagi);

					if($hasilbagi < 1){

						$due_date = date('d-F-Y', strtotime($dateA[$k]));
						$due_date_explode = explode("-",$due_date);
						$due_date_ina = $due_date_explode[0] . " " . $months[$due_date_explode[1]] . " " . $due_date_explode[2];
						$no += 1;
						$string_bg .= '<li dir="ltr"><span style="font-family: helvetica; font-size: 11pt;">BG '.$no.' &ndash; Nominal <span lang="EN-GB">Rp. '.$total_payout_NF.' tgl '.$due_date_ina.' </span></span></li>';        
					
					} else {
						for($i = 0; $i < $hasilbagi; $i++){
							$due_date = date('d-F-Y', strtotime($dateA[$k]));
							$due_date_explode = explode("-",$due_date);
							$due_date_ina = $due_date_explode[0] . " " . $months[$due_date_explode[1]] . " " . $due_date_explode[2];

							$no += 1;
							$calculate_idr = $calculate_idr - $bagi;

								$string_bg .= '<li dir="ltr"><span style="font-family: helvetica; font-size: 11pt;">BG '.$no.' &ndash; Nominal <span lang="EN-GB">Rp. '.$bagi_NF.' tgl '.$due_date_ina.' </span></span></li>';

							if($calculate_idr < $bagi){          
								$no += 1;
								$due_date = date('d-F-Y', strtotime($dateA[$k]));
								$due_date_explode = explode("-",$due_date);
								$due_date_ina = $due_date_explode[0] . " " . $months[$due_date_explode[1]] . " " . $due_date_explode[2];
								$total_payout_NF1 = number_format($calculate_idr);

								$string_bg .= '<li dir="ltr"><span style="font-family: helvetica; font-size: 11pt;">BG '.$no.' &ndash; Nominal <span lang="EN-GB">Rp. '.$total_payout_NF1.' tgl '.$due_date_ina.' </span></span></li>';

							}
						}
					}
				} 

				$list_BG = "<ol> $string_bg </ol>";
				return $list_BG;
				break;

			case 'eng':
				$string_bg = "";
				$no = 0;
				for ($k=0 ; $k < $count_mPayout; $k++){
				$calculate_idr = $totalPayoutIDR * ($amountA[$k]/100);
					$bagi = 500000000;
					$bagi_NF = number_format($bagi);
					$total_payout_NF = number_format($calculate_idr);
					$hasilbagi =  intval($calculate_idr / $bagi);

					if($hasilbagi < 1){
						$due_date = date('d-F-Y', strtotime($dateA[$k]));
						$due_date_explode = explode("-",$due_date);
						$due_date_eng = $due_date_explode[0] . " " . $due_date_explode[1] . " " . $due_date_explode[2];
						$no += 1;
						$string_bg .= '<li dir="ltr"><span style="font-family: helvetica; font-size: 11pt;">BG '.$no.' &ndash; Nominal <span lang="EN-GB">Rp. '.$total_payout_NF.' dated '.$due_date_eng.' </span></span></li>';        

					} else {
					
						for($i = 0; $i < $hasilbagi; $i++){
							$due_date = date('d-F-Y', strtotime($dateA[$k]));
							$due_date_explode = explode("-",$due_date);
							$due_date_eng = $due_date_explode[0] . " " . $due_date_explode[1] . " " . $due_date_explode[2];
							$no += 1;
							$calculate_idr = $calculate_idr - $bagi;
							$string_bg .= '<li dir="ltr"><span style="font-family: helvetica; font-size: 11pt;">BG '.$no.' &ndash; Nominal <span lang="EN-GB">Rp. '.$bagi_NF.' dated '.$due_date_eng.' </span></span></li>';

							if($calculate_idr < $bagi){          
								$no += 1;
								$due_date = date('d-F-Y', strtotime($dateA[$k]));
								$due_date_explode = explode("-",$due_date);
								$due_date_eng = $due_date_explode[0] . " " . $due_date_explode[1] . " " . $due_date_explode[2];
								$total_payout_NF1 = number_format($calculate_idr);
								$string_bg .= '<li dir="ltr"><span style="font-family: helvetica; font-size: 11pt;">BG '.$no.' &ndash; Nominal <span lang="EN-GB">Rp. '.$total_payout_NF1.' dated '.$due_date_eng.' </span></span></li>';
							}
						}
					}
				} 

				$list_BG = "<ol> $string_bg </ol>";
				return $list_BG;
				break;

			default:
				return '';
				break;
		}

	}

	function create_table_payment_schedule_CF($mysql, $contract, $bahasa, $payout_sgd){
		list($cName, $mPayout) = $mysql->GetSingleCampaignForPayout($contract->campaign_id);

		$payoutA = explode("==", $mPayout);
		$amountA = $dateA = $statusA = array();
		
		for ($i = 0; $i < count($payoutA); $i++) {
			$payouts = explode("~", $payoutA[$i]);
			$countPayouts = count($payouts);
			
		  if ($countPayouts==3){
			  $amountA[$i] = $payouts[0];
			  $dateA[$i] = $payouts[1];
			  $statusA[$i] = $payouts[2];
			}
		}

		$count_mPayout = 0;
		for($j=1; $j < count($payoutA); $j++ ){
			if ($payoutA[$j] != '~~'){
				$count_mPayout = $count_mPayout+1;
			}
		}

		$months = ["January" => "Januari", "February" => "Februari", "March" => "Maret", "April" => "April", "May" => "Mei", "June" => "Juni", "July" => "Juli", "August" => "Agustus", "September" => "September", "October" => "Oktober", "November" => "November", "December" => "Desember"];

		$currtotalFunding = $mysql->GetTotalInvestmentAmountPerCampaign($contract->campaign_id);
		$returnFunding = $mysql->GetTotalReturnPerCampaign($contract->campaign_id);

		switch($bahasa){
			case 'eng':
				$table_pembayaran = '<table style="border-collapse: collapse; width: 100%;" border="1" cellpadding="5">
				<tr>
					<td style="width: 10%;"><span style="font-size: 11pt;"><strong>No</strong></span></td>
					<td style="width: 45%;"><span style="font-size: 11pt;"><strong>Payment date</strong></span></td>
					<td style="width: 45%;"><span style="font-size: 11pt;"><strong>Payment due^</strong></span></td>
				</tr>';

				for ($k=0 ; $k < $count_mPayout; $k++){
					//$totalReturn = ((100+$returnFunding)/100)*$currtotalFunding;
					$totalPayout = $payout_sgd*($amountA[$k]/100);

					$due_date = date('d-F-Y', strtotime($dateA[$k]));
					$due_date_explode = explode("-",$due_date);
					$due_date_eng = $due_date_explode[0] . " " . $due_date_explode[1] . " " . $due_date_explode[2];

					$no=$k+1;
					$table_pembayaran = $table_pembayaran . '<tr>
						<td style="width: 10%;"><span style="font-size: 11pt;">'.$no.'</span></td>
						<td style="width: 45%;"><span style="font-size: 11pt;">'.$due_date_eng.'</span></td>
						<td style="width: 45%;"><span style="font-size: 11pt;">SGD '.number_format($totalPayout, 1, '.', ',').'</span></td>
					</tr>';
				}

				$table_pembayaran = $table_pembayaran . "</table>";
				return $table_pembayaran;
				break;

			case 'ina':
				$table_pembayaran = '<table style="border-collapse: collapse; width: 100%;" border="1" cellpadding="5">
				<tr>
					<td style="width: 10%;"><span style="font-size: 11pt;"><strong>No</strong></span></td>
					<td style="width: 45%;"><span style="font-size: 11pt;"><strong>Tanggal jatuh tempo pembayaran</strong></span></td>
					<td style="width: 45%;"><span style="font-size: 11pt;"><strong>Jumlah yang wajib dibayarkan^</strong></span></td>
				</tr>';

				for ($k=0 ; $k < $count_mPayout; $k++){
					//$totalReturn = ((100+$returnFunding)/100)*$currtotalFunding;
					$totalPayout = $payout_sgd*($amountA[$k]/100);

					$due_date = date('d-F-Y', strtotime($dateA[$k]));
					$due_date_explode = explode("-",$due_date);
					$due_date_ina = $due_date_explode[0] . " " . $months[$due_date_explode[1]] . " " . $due_date_explode[2];

					$no=$k+1;
					$table_pembayaran = $table_pembayaran . '<tr>
						<td style="width: 10%;"><span style="font-size: 11pt;">'.$no.'</span></td>
						<td style="width: 45%;"><span style="font-size: 11pt;">'.$due_date_ina.'</span></td>
						<td style="width: 45%;"><span style="font-size: 11pt;">SGD '.number_format($totalPayout, 1, '.', ',').'</span></td>
					</tr>';
				}

				$table_pembayaran = $table_pembayaran . "</table>";
				return $table_pembayaran;
				break;

			default:
				return '';
				break;
		}

		
	}
?>
