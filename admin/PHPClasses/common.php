<?php

include_once("../config.php");
include_once(DOC_ROOT.'console/libs/smarty/Smarty.class.php');
include_once(DOC_ROOT.'console/libs/kgPager.class.php');
include_once(DOC_ROOT."console/incs/Functions.php");
include_once(DOC_ROOT."console/incs/Image_GD_Functions.php");
include_once(DOC_ROOT."console/CModules/core.DB.php");
include_once(DOC_ROOT."console/CModules/core.Spine.php");
include_once(DOC_ROOT."console/libs/DVSecurity.php"); 

define('CRONJOB_FOLDER', DOC_ROOT.'console/cronjob/');

$currdatetime = date("Y-m-d H:i:s");

$sys_msg = array();

fix_gpc_magic();

//#######################################################################
//#Database Connection
///////////////////////////////////////////////////////////////////////
$db = new DB();
$sqllink = $db->connect(SQL_HOST, SQL_USER, SQL_PASSWORD);
$db->use_db(SQL_DB);

//#######################################################################
//# CONSOLESPINE - REQUIRES db.class.php and an open connection called $db
///////////////////////////////////////////////////////////////////////
$spine = new spine();
$smarty = new Smarty;
$smarty->compile_check = true;
//$smarty->debugging = DEBUG;
$smarty->clear_compiled_tpl();

#DV9 Security Class
if(!isset($_POST['module_id']) || $_POST['module_id']!=102){
	//$DVSecurity = new DVSecurity; 
	//$DVSecurity->clean_all();  
}

?>
