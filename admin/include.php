<script src="<?php echo ADMIN_PATH ?>/bootstrap/jquery.min.js"></script>
<script src="<?php echo ADMIN_PATH ?>/bootstrap/bootstrap.min.js"></script>
<script src="<?php echo ADMIN_PATH ?>/bootstrap/bootstrap-select.min.js"></script>
<script src="<?php echo ADMIN_PATH ?>/bootstrap/jquery-ui.min.js"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/bootstrap/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/bootstrap/jquery-ui.min.css">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/bootstrap/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">


<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/css/admin-panel.css">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/js/pickadate/themes/default.css" id="theme_base">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/js/pickadate/themes/default.date.css" id="theme_date">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/js/pickadate/themes/default.time.css" id="theme_time">
<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/css/custom.css" >