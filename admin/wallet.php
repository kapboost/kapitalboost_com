<?php
ini_set('display_errors', 1);
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$mId = $_GET["m"];

if (isset($_POST["transAmount"]) and $_POST["transAmount"]!=0) {
    // echo "'<script>console.log(\"amount : ".$_POST["transAmount"]."\")</script>'";
    $transAmount = $_POST["transAmount"];
    $transDesc = $_POST["transDesc"];
    $transStats = $_POST["transStats"];
    $walletBalance = $_POST["walletBalance"];


    if ($mysql->Connection()) {
        $mysql->InsertTransaction($mId, $transDesc, $transStats, $transAmount, $walletBalance);
    }
}


if ($mysql->Connection()) {
    $walletBalance = $mysql->GetMmeberBalance($mId);
    list($transDate, $transDesc, $transStats, $transAmount, $transId, $status) = $mysql->GetMmeberTransactionHistory($mId);
    if ($walletBalance==null or $walletBalance=="") {
        $walletBalance=0;
    }
    list($mUserName, $mEmail, $mFirstName, $mLastName) = $mysql->GetSingleMembers($mId);
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KB Admin Member Wallet - </title>

      <?php include_once 'include.php'; ?>
      
      <script>
                     $(document).ready(function () {
                            $(".DelBtn").click(function () {
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dwallet: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        window.location = window.location.href.split("#")[0];
                                                        
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });

                     });
              </script>
   <body>
      <?php include_once 'header.php'; ?>
      <?php include_once 'popup.php'; ?>


      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Member Wallet | <?= $mFirstName." ".$mLastName ?></span>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body" style="text-align: center">
                        <div class="col-xs-12 col-md-6">

							<form action="" method="POST" enctype="multipart/form-data">
                           <div class="form-group">
                              <label>Current Wallet Balance : </label>
                              <input onclick="sensative()"  type="number" name="walletBalance" value="<?= $walletBalance ?>" class="form-control" readonly="">
                           </div>

                           <div class="form-group">
                              <label>Amount : </label>
                              <input type="number" step="0.1" name="transAmount" value="" class="form-control" required>
                           </div>

                           <div class="form-group">
                              <label>Action : </label>
                              <select name="transStats" id="transStats" class="form-control" required>
                                    <option value="Top Up">Top Up</option>
                                    <option value="Withdrawal">Withdrawal</option>
                                    <option value="Investment">Investment</option>
                                    <option value="Payout">Payout</option>
                              </sel ect>
                                    <option value="Donation">Donation</option>
                              </select>
                           </div>

                           <div class="form-group">
                              <label>Description : </label>
                              <input type="text" name="transDesc" placeholder="" value="" class="form-control" required>
                           </div>
						    <input name="button" type="submit" class="btn btn-primary" id="button" value="Save Transaction" />
							</form>
                        </div>

						<div class="col-xs-12 col-md-6">
						<h3> Transaction History </h3>
							<div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="5%">No</th>
                                                                             <th width="15%">Date</th>
                                                                             <th width="20%">Action</th>
                                                                             <th width="20%">Description</th>
                                                                             <th width="20%">Amount</th>
                                                                             <th width="20%">Action</th>
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($transAmount); $i++) {
    $fontcolor = "black";
    if ($transStats[$i]=="Investment" || $transStats[$i]=="Reduce Balance" || $transStats[$i]=="Withdrawal" || $transStats[$i]=="Donation") {
        $fontcolor = "red";
    } ?>
                                                                             <tr class="even" style="color: <?= $fontcolor?>">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><?= $transDate[$i] ?></td>
                                                                                    <td><?= $transStats[$i] ?></td>
                                                                                    <!--<td></td>-->
                                                                                    <td><?= $transDesc[$i] ?></td>
                                                                                    <td>SGD <?= $transAmount[$i] ?></td>
                                                                                    <td>
                                                                                      <?php if ($status[$i] == 0): ?>
                                                                                        <label for="" class="label label-danger">Deleted</label>
                                                                                      <?php else: ?>
                                                                                        <button value="<?= $transId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button>
                                                                                      <?php endif; ?>
                                                                                    </td>
                                                                             </tr>

                                                                      <?php
} ?>

                                                               </tbody>
                                                        </table>
                                                 </div>
                        </div>

                        </div>
                  </div>
               </div>
            </div>


            <script src="js/pickadate/picker.js"></script>
            <script src="js/pickadate/picker.date.js"></script>
            <script src="js/pickadate/legacy.js"></script>
            <script>

                                  $('#dob').pickadate({
                                     format: 'yyyy-mm-dd'
                                  });

            </script>


            </body>


            </html>
