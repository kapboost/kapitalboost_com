<?php
include_once 'security.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'model/withdraw-wallet.php';
$WithdrawWallet = new WithdrawWallet(); 

if ($WithdrawWallet->Connection()) {
   list($id, $mId, $amount, $date, $status, $proof_payment, $date_transfer, $mName, $bankName, $bankAccName, $bankAccNumber, $bankSwift) = $WithdrawWallet->GetAllWithdrawRequest();
}


?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KapitalBoost Withdraw Request</title>

      <?php include_once 'include.php'; ?>


   </head>
   <body>
      <?php include_once 'header.php'; ?>
	  <?php include_once 'popup.php'; ?>
	  <?php
		if($c == md5("SuccessKBAdminTransferWithdraw")){
			echo '<script>$("#popup").fadeIn(50).delay(1000).fadeOut(200);
			$("#popupText").html("Data Saved");
			</script>';
		}
	  ?>

      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Withdraw Request</span>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body">


                     <br/><br/>
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                           <thead>
                              <tr>
                                 <th width="2%">No</th>
                                 <th width="20%">Member Name</th>
                                 <th width="20%">Date Requested</th>
                                 <th width="20%">Amount</th>
                                 <!--<th width="20%">Bank Data</th>-->
                                 <th width="20%">Status</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                              for ($i = 0; $i < count($amount); $i++) {
								  if($status[$i]==0){$statusX="New";}else{$statusX="Paid";}
                                 ?>
                                 <tr class="even">
                                    <td><?= $i + 1 ?></td>
                                    <td><a href="withdraw-wallet-detail.php?c=<?= $id[$i] ?>"><?= $mName[$i] ?></a></td>
                                    <td><?= $date[$i] ?></td>
                                    <td>$<?= $amount[$i] ?></td>
                                    <!--<td>
										Bank Name: <b><?= $bankName[$i] ?></b> <br />
										Account Name: <b><?= $bankAccName[$i] ?></b> <br />
										Account Number: <b><?= $bankAccNumber[$i] ?></b> <br />
										SWIFT Code: <b><?= $bankSwift[$i] ?></b>
									</td>-->
                                    <td><?= $statusX ?> <?php if($status[$i]==1){ echo " on $date_transfer[$i]";}?></td>
                                 </tr>
                                 <?php
                              }
                              ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <script>
		$(document).ready( function () {
			$('#myTable').DataTable();
		} );
		</script>

   </body>


</html>