<?php
include_once 'security.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'lib/helper.php';

include_once 'mysql.php';
$mysql = new mysql();

$report_payouts = array();

if ($mysql->Connection()) {
	$report_payouts = $mysql->reportPayouts($_GET['id']);
	$campaign = $mysql->getDatas("tcampaign", "campaign_id", $_GET['id'], false);
	// print_r($report_payouts); exit();
}
?>

<!DOCTYPE html>
<html>
<head>
	<?php include_once 'initialize.php'; ?>

      <title>KapitalBoost | Report Payout</title>

      <?php include_once 'include.php'; ?>
      <style type="text/css">
      	div.dataTables_wrapper {
	        width: 100%;
	        margin: 0 auto;
	    }
      </style>
</head>
<body>
	<?php include_once 'header.php'; ?>

	<div class="container-fluid" style="margin-top: 90px;">
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<div class="row">
							<div class="col-lg-6">
								<h4>Report Payout - Campaign <b><?php echo $campaign->campaign_name; ?></b></h4>
							</div>
							<div class="col-lg-6" align="right">
								<form method="POST" action="excel-extract.php">
									<input type="hidden" name="action" value="report-payout">
									<input type="hidden" name="id" value="<?php echo $_GET['id']?>">
									<button class="btn btn-primary">
										Export Datas
									</button>
								</form>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<table class="table table-bordered display nowrap" id="report-payout" style="width:100%">
							<thead>
								<tr>
									<th>No.</th>
									<th>A/C Number</th>
									<th>Investor Name</th>
									<th>Email</th>
									<th>BIC Code</th>
									<?php for ($i=1; $i <= 4; $i++) { ?>
										<th>Payout <?php echo $i ?></th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php $no=1; foreach ($report_payouts as $report): ?>
									<tr>
										<td><?php echo $no ?></td>
										<td><?php echo "'".$report['account_number'] ?></td>
										<td align="left"><?php echo $report['nama'] ?></td>
										<td><?php echo $report['email'] ?></td>
										<td align="center"><?php echo $report['bic'] ?></td>
										<?php for ($i=1; $i <= 4; $i++) { ?>
											<?php if (isset($report["bulan_$i"])): ?>
												<td>SGD <?php echo number_format($report["bulan_$i"], 2, '.', ',') ?></td>
											<?php else: ?>
												<td>-</td>
											<?php endif ?>
										<?php } ?>
									</tr>
								<?php $no++; endforeach ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			var table = $('#report-payout').DataTable({
				stateSave: true,
				"scrollX": true
			});
		})
	</script>
</body>
</html>