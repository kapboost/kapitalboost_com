<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$pId = $_GET["p"];

if (isset($_POST["partner_name"])) {
       $pNamef = $_POST["partner_name"];
       $pDesf = $_POST["partner_description"];
       $pUrlf = $_POST["partner_url"];
       $enabledf = $_POST["status"];

       if (isset($_FILES["fileToUpload"])&& count($_FILES['fileToUpload']['error']) == 1 && $_FILES['fileToUpload']['error'][0] > 0) {
              
       }else if(!is_uploaded_file($_FILES['fileToUpload']['tmp_name'])){
              
       }
       else if(isset($_FILES['fileToUpload'])){
              $file = $_FILES["fileToUpload"]["tmp_name"];
              $filename = $_FILES['fileToUpload']['name'];
              $ext = pathinfo($filename, PATHINFO_EXTENSION);
              $uploaddir .= "../assets/images/partner/";
              $image_name = time() .".". $ext;
              move_uploaded_file($file, $uploaddir . $image_name);
              if ($mysql->Connection()) {
                     $mysql->UpdateUploadedPartner($pId, $image_name);
              }
       }
       if ($mysql->Connection()) {
              $mysql->UpdateSinglePartner($pId, $pNamef, $pDesf, $pUrlf, $enabledf);
       }
}


if ($mysql->Connection()) {
       list($pName, $pDes, $pImg, $pUrl, $enabled) = $mysql->GetSinglePartnerList($pId);
}

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Partner List Detail - <?= $pName ?></title>

              <?php include_once 'include.php'; ?>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Partner List - <?= $pName ?></span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">


                                                 <form action="" method="POST" enctype="multipart/form-data">

                                                        <div class="col-xs-12 col-md-12">

                                                               <div class="form-group">
                                                                      <label>Partner Name : </label>
                                                                      <input type="text" name="partner_name" value="<?= $pName ?>" class="form-control">
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Partner Description : </label>
                                                                      <textarea style="height: 150px" name="partner_description" class="form-control"><?= $pDes ?></textarea>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Image : </label>
                                                                      <input type="file" name="fileToUpload" id="fileToUpload" >
                                                                      <?php if ($pImg != "") { ?>
                                                                             <br />
                                                                             <div class="graph-wrapper" style="margin-left:-40px;">
                                                                                    <ul class="image_wrapper">
                                                                                           <li>
                                                                                                  <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/partner/<?= $pImg ?>" class="image-link" title="<?= $pName ?>"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/partner/<?= $pImg ?>&w=150&bg=D4E9F7" alt=""></a><br>
                                                                                           </li>
                                                                                    </ul>
                                                                             </div>
                                                                             <div class="clear"></div>
                                                                      <?php } ?>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Partner url : </label>
                                                                      <input type="text" name="partner_url" value="<?= $pUrl ?>" class="form-control">
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Status : </label>
                                                                      <br />
                                                                      <?php if ($enabled == 0) { ?>
                                                                             <input type="radio" name="status" value="0" checked>Offline<br>
                                                                             <input type="radio" name="status" value="1">Online<br>

                                                                      <?php } else { ?>
                                                                             <input type="radio" name="status" value="0">Offline<br>
                                                                             <input type="radio" name="status" value="1" checked>Online<br>

                                                                      <?php } ?>

                                                               </div>

                                                               <input name="button" type="submit" class="btn btn-primary" id="button" value="Update" />


                                                        </div>

                                                 </form>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>