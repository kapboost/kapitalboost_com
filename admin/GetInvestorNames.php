<?php

if (isset($_REQUEST["term"])) {
       $term = $_REQUEST["term"];

       include_once 'mysql.php';
       $mysql = new mysql();
       if ($mysql->Connection()) {
              list($mId, $mName, $mEmail) = $mysql->GetInvestorNamesForSuggestion($term);
              for ($i = 0; $i < count($mId); $i++) {
                     $response[$i] = array(label => $mName[$i] . " ~ " . $mEmail[$i], value =>$mName[$i] . " ~ " . $mEmail[$i] ." ~ " . $mId[$i]);
              }
              echo json_encode($response);
       } else {
              echo json_encode("Database Error");
       }
}