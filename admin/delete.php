<?php

session_start();
include_once 'mysql.php';
$mysql = new mysql();


if (isset($_POST["delete"])) {
       $cId = $_POST["delete"];
       $mc = $_POST["MC"];

       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteCampaign($cId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}

if (isset($_POST["updateproject"])) {
       $cId = $_POST["updateproject"];
       $mc = $_POST["MC"];

       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteUpdate($cId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}


if (isset($_POST["event"])) {
       $eId = $_POST["event"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteEvent($eId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}

if (isset($_POST["dinvestment"])) {
       $iId = $_POST["dinvestment"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteInvestment($iId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}

if (isset($_POST["dpartner"])) {
       $pId = $_POST["dpartner"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeletePartner($pId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}
if (isset($_POST["denquiry"])) {
       $eId = $_POST["denquiry"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteEnquiry($eId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}
if (isset($_POST["dmessages"])) {
       $eId = $_POST["dmessages"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteMessages($eId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}
if (isset($_POST["dmember"])) {
       $mId = $_POST["dmember"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteMember($mId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}
if (isset($_POST["dcampaignimage"])) {
       $cId = $_POST["cid"];
       $iId = $_POST["iid"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteCampaignImage($cId, $iId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database Error");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}

if (isset($_POST["dcontractimage"])) {
       $cId = $_POST["cid"];
       $conimage = $_POST["conimage"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteContractImage($cId, $conimage);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database Error");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}
if (isset($_POST["dblog"])) {
       $bId = $_POST["dblog"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteBlog($bId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database Error");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}

if (isset($_POST["dwallet"])) {
	
    $wId = $_POST["dwallet"];
	// echo "'<script>console.log(\"dwallet : $wId\")</script>'";
    $mc = $_POST["MC"];
    if ($mc == $_SESSION["MasterCode"]) {
           if ($mysql->Connection()) {
                  // $mysql->DeleteWalletTrans($wId);
                  $mysql->UpdateWalletStatus($wId);
                  echo json_encode("Done");
           } else {
                  echo json_encode("Database Error");
           }
    } else {
           echo json_encode("Wrong Code");
    }
}

if (isset($_POST["devent"])) {
       $bId = $_POST["devent"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     $mysql->DeleteEvents($bId);
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database Error");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}
if (isset($_POST["dcampaignpdf"])) {
       $cId = $_POST["cid"];
       $pdf = $_POST["pdf"];
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              if ($mysql->Connection()) {
                     if ($mysql->Connection()) {
                            list($pdf_names,$pdf_descriptions) = $mysql->GetCampaignPdfs($cId);
                     }
                     $pdf_name = explode("~", $pdf_names);
                     $pdf_description = explode("~",$pdf_descriptions);
                     $pdf_name[$pdf] = "";
                     $pdf_description[$pdf] = "";
                     $pdfsf = "";
                     $pdfsDesf = "";
                     for ($i = 0; $i < 8; $i++) {
                            $pdfsf .= $pdf_name[$i] . "~";
                            $pdfsDesf .= $pdf_description[$i] . "~";
                     }
                     if($mysql->Connection()){
                            $mysql->UpdateCampaignPdf($cId, $pdfsf,$pdfsDesf);
                     }
                     echo json_encode("Done");
              } else {
                     echo json_encode("Database Error");
              }
       } else {
              echo json_encode("Wrong Code");
       }
}

if (isset($_POST["sensative"])) {
       $mc = $_POST["MC"];
       if ($mc == $_SESSION["MasterCode"]) {
              echo json_encode("Granted");
       } else {
              echo json_encode("Wrong Code");
       }
}

       
       
       
       
       
       