<?php 
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
$cId = $_GET["c"];

if ($mysql->Connection()) {
       list($pageContent) = $mysql->GetGeneralContent($cId);
}

?>

<!DOCTYPE html>
<html lang="en" xmlns:og="http://ogp.me/ns#" prefix="og: http://ogp.me/ns#" xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
	<base href="https://kapitalboost.com/">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script src='https://www.google.com/recaptcha/api.js'></script>
	<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<meta name="title" content="Kapital Boost - crowdfunding of Test Campaign (1) campaign"/>
	<meta name="description" content=" OVERVIEW PT Instyle Furniture, an Indonesia-based manufacturer&nbsp;of&nbsp;home furniture, is raising SGD80,000 to purchase solid teak wood, in order to fulfill a purchase order (PO) from PT Dekorasi Indah. &nbsp; BUSINESS PROFILE...}"/>
	<meta name="keywords" content="Furniture supplier Instyle Furniture is raising funds to supply PT Dekorasi Indah with ready-made furnitures"/>
	<meta name="author" content="">
	<link rel="shortcut icon" href="ico/favicon.ico">

	<title>Kapital Boost - crowdfunding of Test Campaign (1) campaign</title>


	<script type="text/javascript">var jslang = 'EN';</script>
	<link rel="shortcut icon" type="assets/image/png" href="assets/images/kp-fav.png" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="assets/css/new-kp-main.css" />
	<link rel="stylesheet" href="assets/css/new-kp-home.css" />
	<link rel="stylesheet" href="assets/css/kp-about.css" />
	<link rel="stylesheet" href="assets/css/kp-default.css" />
	<link rel="stylesheet" href="assets/css/kp-blog.css" />
	<link rel="stylesheet" href="assets/css/kp-event.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/sandbox.css">
	<link rel="stylesheet" type="text/css" href="assets/css/content-tools.min.css">


	<link rel="stylesheet" href="assets/css/Box-vs=b1967.r482151-phase1.css" type="text/css" media="screen" />
	<script type="text/javascript" src="assets/js/jquery-2.1.3.min.js"></script>
	<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/Java_Box.js?vs=b1967.r482151-phase1"></script>
	<link rel="stylesheet" href="assets/css/lightbox.min.css" />
	
	<!-- crazyegg -->
	<script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0075/8729.js" async="async"></script>

</head>

<body>

	<div id="main">
		<div id="container1">

			<link rel="stylesheet" href="assets/css/new-kp-campaigns.css" />
			<div id="campaigns-detail">
				<div id="campaigns-detail-2">
					<div id="campaigns-detail-2-inner">
						<div id="campaigns-detail-2-inner-left" class="leftRight textCenter">
						<div data-editable data-name="all-content">
							
							<?php echo $pageContent; ?>
							
						</div>
						</div>
						
					</div>
				</div>



			</div>

			
		</div>
		
	</div>
	
	
	

	






	<script type="text/javascript" src="assets/js/kp-main.js"></script>
	<script type="text/javascript" src="assets/js/kp-default.js"></script>
	<script type="text/javascript" src="assets/js/lightbox.min.js"></script>
	<script type="text/javascript" src="assets/js/content-tools.js"></script>
	<script type="text/javascript" src="assets/js/sandbox.js"></script>
	
	<script>
	window.addEventListener('load', function() {
		var editor;

	});

	editor = ContentTools.EditorApp.get();
	editor.init('*[data-editable]', 'data-name');

	editor.addEventListener('saved', function (ev) {
		var name, payload, regions, xhr;

		// Check that something changed
		regions = ev.detail().regions;
		if (Object.keys(regions).length == 0) {
			return;
		}

		// Set the editor as busy while we save our changes
		this.busy(true);

		// Collect the contents of each region into a FormData instance
		payload = new FormData();
		for (name in regions) {
			if (regions.hasOwnProperty(name)) {
				payload.append(name, regions[name]);
			}
		}

		// Send the update content to the server to be saved
		function onStateChange(ev) {
			// Check if the request is finished
			if (ev.target.readyState == 4) {
				editor.busy(false);
				if (ev.target.status == '200') {
					// Save was successful, notify the user with a flash
					new ContentTools.FlashUI('ok');
				} else {
					// Save failed, notify the user with a flash
					new ContentTools.FlashUI('no');
				}
			}
		};

		xhr = new XMLHttpRequest();
		xhr.addEventListener('readystatechange', onStateChange);
		xhr.open('POST', '<?php echo ADMIN_PATH ?>/live-campaign-save.php?c=<?php echo $cId;?>');
		console.log(payload); 
		xhr.send(payload);
	});
	
	</script>
	

</body>
</html>
