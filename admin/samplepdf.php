<?php

include_once 'mysql.php';
$mysql = new mysql();

$cId = $_GET["c"];

$monthsID = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

if ($mysql->Connection()) {

   $payoutAmount = ["SGD1,000", "SGD500", "SGD1,200"];
   $payoutDate = ["1 January 2017", "15 April 2017", "24 October 2017"];
   $payoutDateId = ["1 Januari 2017", "15 April 2017", "24 Oktober 2017"];

   list($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) = $mysql->GetSingleCampaignContractList($cId);


   $icName = "Investor Name";
   $icType = "Singapore FIN No.";
   $icNumber = "123456";

}


require 'lib/fpdi/tcpdf/tcpdf.php';
require 'lib/fpdi/fpdi.php';


$date = date("d F Y", time());
$companyName = $comName;
$companyRegistration = $comReg;
$purchaseAsset = $purchaseAsset;
$assetCost = $assetCost;
$salePrice = $salePrice;
$assetCostCurrency = $assetCostCurr;
$exchange = $exchange;
$profit = $profit;
$return = $camReturn;
$cCloseDate = "1 January 2018";
$cCloseDate1 = "1 March 2018";

$foreignCurrency = $foreignCurrency;
$dirName = $dirName;
$dirTitle = $dirTitle;
$dirIC = $dirIc;
$image = "https://kapitalboost.com/assets/images/pdfimg/$image";

$invName = $icName;
$invIC = "$icType $icNumber";
$invAmount = "SGD10,000";
$invPercent = 10;

$invSalePrice = "SGD12,000";
$invProfit = "SGD2,000";




$month = date('n', time());
$day = date('d', time());
$year = date('Y', time());
$monthId = $monthsID[$month];
$dateID = "$day $monthId $year";





if ($conType == 0) {
   $pdf = new FPDI();
   $pdf->setPrintHeader(false);
   $pdf->setPrintFooter(false);

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetFont('Helvetica');

   MCenter($pdf, "DATED THIS DAY $date", 30);

   MCenter($pdf, "Between", 55);

   MCenter($pdf, $companyName, 80, true);

   MCenter($pdf, $companyRegistration, 90);

   $pdf->SetXY(160, 100);
   $pdf->Write(8, "...Company");

   MCenter($pdf, "and", 120);

   MCenter($pdf, $invName, 140, true);

   MCenter($pdf, "($invIC)", 150);

   $pdf->SetXY(160, 160);
   $pdf->Write(8, "...Financier");


   $pdf->SetFontSize("30");
   MCenter($pdf, ". . . . . . . . . . . . . . . . . . . . .", 180);

   $pdf->SetFontSize("16");
   MCenter($pdf, "MURABAHAH AGREEMENT", 200, true);

   $pdf->SetFontSize("30");
   MCenter($pdf, ". . . . . . . . . . . . . . . . . . . . .", 210);
   $pdf->SetFontSize("12");

// Page 2

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetMargins(25, 15);

   MCenter($pdf, "MURABAHAH AGREEMENT", 15);

   $y = MLeft1($pdf, "<b>THIS AGREEMENT</b> is made between the following parties: ", 35);

   $y = MLeft1($pdf, "1. <b>$companyName</b>", $y + 5);

   $y = MLeft1($pdf, $companyRegistration, $y);

   $y = MLeft1($pdf, "(hereinafter referred to as \"<b>the Company</b>  \")", $y + 5);

   $y = MLeft1($pdf, "<b>AND</b>", $y + 10);

   $y = MLeft1($pdf, "2. <b>$invName</b>", $y + 10);

   $y = MLeft1($pdf, "($invIC)", $y);

   $y = MLeft1($pdf, "(hereinafter referred to as \"<b>the Financier</b>  \")", $y + 5);

   $y = MLeft1($pdf, "<b>WHEREAS:</b>", $y + 10);

   $y = MLeft1($pdf, "A.", $y + 15);

   $y = MLeft2($pdf, "The Company is in need of funding for the purchase, acquisition or procurement of $purchaseAsset, as described in Schedule A (“<b>the Asset</b> ”), which it intends to utilize for its business, and the Company wishes to obtain such financing in accordance with Shariah principles;", $y - 7);

   $y = MLeft1($pdf, "B.", $y + 15);

   $y = MLeft2($pdf, "The Financier, together with certain other financiers (together, “<b>the Investors Group</b> ”) have agreed to provide the required financing to the Company based on the Shariah principle of Murabahah, specifically murabahah to the purchase orderer; and", $y - 7);

   $y = MLeft1($pdf, "C.", $y + 15);

   $y = MLeft2($pdf, "The Financier has agreed to provide his share of the required financing based on the terms and subject to the conditions of this Agreement.", $y - 7);


   $y = MLeft1($pdf, "<b>NOW IT IS HEREBY AGREED</b> as follows:", $y + 15);
   $y = MLeft1($pdf, "1. <b>METHOD OF FINANCING</b>", $y + 10);

   $y = MLeft1($pdf, "1.1", $y + 10);

   $y = MLeft2($pdf, "For the purpose of financing the acquisition of the Asset, the Company hereby requests that the Investors Group"
           . " collectively purchases the Asset from the Supplier for a total consideration of <b>$assetCost</b> (\"<b>the Purchase Cost</b>&nbsp;&nbsp;\"), "
           . "and thereafter sell the Asset to the Company at the sale price of <b>$salePrice</b> (\"<b>the Sale Price</b> \"). "
           . "The actual cost to the Company of purchasing the Asset is <b>$assetCostCurrency</b> (\"<b>Purchase Currency Cost </b>&nbsp; \"),"
           . " which currency can be purchased using the Singapore Dollar amount of the Purchase Cost at the prevailing currency"
           . " exchange rate of S$1:$exchange The Company undertakes (via wa’d) to purchase the Asset from the Investors Group, "
           . "following the purchase of the Asset by the Investors Group in accordance with the terms of this Agreement. "
           . "The Murabahah arrangement set out above will result in profit for the Investors Group as set out below.", $y - 6);


   $html = "<table cellspacing=\"0\" cellpadding=\"10\" border=\"1\">
  <tr>
  <td><b>Purchase Cost of the Asset payable by the Investors Group</b></td>
  <td><b>Sale Price payable by the Company to the Investors Group</b></td>
  <td><b>Amount of Profit derived by the Investors Group</b></td>
  </tr>
  <tr>
  <td>$assetCost</td>
  <td>$salePrice</td>
  <td>$profit (or $return% of the Purchase Cost of Asset)</td>
  </tr>

  </table>
  ";
   $pdf->writeHTML($html);
   $y = $pdf->GetY();
   $y = MLeft1($pdf, "1.2", $y - 5);

   $y = MLeft2($pdf, "The parties agree that the Purchase Cost refers to the actual acquisition costs of the Asset incurred or to be incurred by "
           . "the Investors Group or its agent for the acquisition of the Asset, excluding any direct expenses (such as delivery, transportation, "
           . "storage and assembly costs, taxes, takaful and insurance coverage costs) and any overhead expenditures or indirect costs "
           . "(such as staff wages and labor charges).", $y - 7);

   $y = MLeft1($pdf, "1.3", $y + 5);

   $y = MLeft2($pdf, "The Financier is willing to finance exactly $invAmount (being $invPercent%) out of the"
           . " entire Purchase Cost amount. Such amount to be financed by the Financier will be paid by Financier to the Supplier, through"
           . " the Company as agent. For the purposes of enabling Kapital Boost, as agent of the Company under Clause 3.3.1, to transfer"
           . " the Purchase Cost amount to the Company by no later than $cCloseDate (the \"<b>Purchase Cost Contribution Date</b>&nbsp;&nbsp;&nbsp; \"),"
           . " the Financier shall transfer the amount of its contribution to the Purchase Cost (as specified in this clause above) to the account"
           . " specified in writing by Kapital Boost, by no later than <b>three (3) working days</b>&nbsp;&nbsp;&nbsp; from the date of this Agreement. ", $y - 7);

   $y = MLeft1($pdf, "1.4", $y + 5);

   $y = MLeft2($pdf, "Based on the Financier’s contribution to the Purchase Cost as specified above, the Financier will own"
           . " $invPercent% of the Asset upon purchase of the same by the Investors Group, and the Financier will be"
           . " entitled to $invPercent% share of the Sale Price and each installment or part thereof paid by the Company,"
           . " amounting to:-", $y - 7);



   MBullet($pdf, $y);

   $y = MLeft3($pdf, "Financier’s portion of the Sale Price: $invSalePrice", $y + 3);

   MBullet($pdf, $y);

   $y = MLeft3($pdf, "Financier's portion of the Profit: $invProfit", $y + 3);

   $y = MLeft1($pdf, "1.5", $y + 5);

   $y = MLeft2($pdf, "The Financier (as part of the Investors Group) hereby appoints the Company as his agent to purchase"
           . " the Asset on its behalf. The Company hereby accepts such appointment as agent, and will purchase and handle"
           . " the Asset in accordance with the terms of this Agreement. The Company, as agent, shall purchase the Asset in"
           . " the currency of the Purchase Currency Cost, for a consideration not exceeding the Purchase Currency Cost, and"
           . " by no later than  <b>$cCloseDate1</b>&nbsp;(\"<b>Date of Purchase</b>&nbsp; \"). ", $y - 7);

   $y = MLeft1($pdf, "1.6", $y + 5);

// star here

   $y = MLeft2($pdf, "The Financier and the Company (as agent of the Investors Group for the purposes of the purchase of the Asset)"
           . " hereby acknowledge and mutually agree that:", $y - 7);

   $y = MLeft2($pdf, "1.6.1", $y + 3);

   $y = MLeft3($pdf, "the Investors Group (including the Financier) has appointed the Company to act as agent of the Investors Group"
           . " to purchase the Asset on behalf of the Investors Group because of the Company’s market knowledge and networks"
           . " in relation to the Asset;", $y - 7);

   $y = MLeft2($pdf, "1.6.2", $y + 3);

   $y = MLeft3($pdf, "the purchase of the Asset by the Investors Group through the Company as agent may result in benefits to"
           . " the Investors Group through efficiency of the purchase process and also cost savings in the event that the Company"
           . " is able to secure or source the Asset at a price lower than the Purchase Currency Cost;", $y - 7);

   $y = MLeft2($pdf, "1.6.3", $y + 3);

   $y = MLeft3($pdf, "the event that the Company, as agent, is able to purchase the Asset at lower than the Purchase Currency Cost,"
           . " the Investors Group hereby agrees to waive, by way of tanazul (i.e. waiver), its rights to claim the excess unutilized"
           . " funding amount; and the Company, as agent, is entitled to retain the benefit of any such cost savings as incentive based"
           . " on hibah for its good work performance;", $y - 7);

   $y = MLeft2($pdf, "1.6.4", $y + 3);

   $y = MLeft3($pdf, "in the circumstances described in Clause 1.6.3, the cost to the Investors Group of purchasing the Asset would be"
           . " the Purchase Cost, as the hibah amount granted to the Company, as agent, would constitute part of the actual cost of"
           . " purchasing the Asset, and accordingly the same Purchase Cost will be reflected in the Murabahah Sale Contract to be entered"
           . " into by the Investors Group in the sale of the Asset;", $y - 7);

   $y = MLeft2($pdf, "1.6.5", $y + 3);

   $y = MLeft3($pdf, "in the event that the Company is unable to purchase sufficient $foreignCurrency to constitute the Purchase Currency"
           . " Cost using the Purchase Cost contributed by the Investors Group as a result of an decrease in the currency exchange rate from"
           . " that stated in Clause 1.1:", $y - 7);

   $y = MLeft3($pdf, "(a)", $y + 2);

   $y = MLeft4($pdf, "the Company, as agent of the Investors Group, shall either:", $y - 7);

   $y = MLeft4($pdf, "(i)", $y);

   $y = MLeft5($pdf, "promptly notify the Investors Group and return the Purchase Cost"
           . " to the Investors Group, following which this Agreement shall terminate; or", $y - 7);

   $y = MLeft4($pdf, "(ii)", $y);

   $y = MLeft5($pdf, "fund the shortfall amount in the currency of the Purchase Cost (the"
           . " \"<b>Purchase Cost Shortfall Amount</b>&nbsp;&nbsp; \") which would be required in order for the Company to purchase the"
           . " Purchase Currency Cost in order to complete the purchase of the Asset as agent on behalf of the Investors Group;", $y - 7);

   $y = MLeft3($pdf, "(b)", $y + 3);

   $y = MLeft4($pdf, "following the funding of the Purchase Cost Shortfall Amount by the Company and the completion of the purchase"
           . " of the Asset by the Company, as agent of the Investors Group:", $y - 7);

   $y = MLeft4($pdf, "(i)", $y);
   $y = MLeft5($pdf, "the Purchase Cost Shortfall Amount shall constitute a debt owed by the Investors Group to the Company, in its capacity as agent for the Investors Group;", $y - 7);

   $y = MLeft4($pdf, "(ii)", $y);
   $y = MLeft5($pdf, "notwithstanding paragraph (i) above, the Company shall not, prior to the transfer of title to the Asset pursuant to Clause 1.8, have any claim to any part of portion of the title to the Assets;", $y - 7);

   $y = MLeft4($pdf, "(iii)", $y);
   $y = MLeft5($pdf, "the Sale Price (or, if there is more than one installment of the Sale Price, then the first installment thereof) shall be increased by the Purchase Cost Shortfall Amount, which will be reflected in the Company’s Offer to Purchase delivered by the Company to the Investors Group; and", $y - 7);

   $y = MLeft4($pdf, "(iv)", $y);
   $y = MLeft5($pdf, "the Investors Group, the Company (in its capacity as agent of the Investors Group) and as purchaser of the Asset, all agree that the Purchase Cost Shortfall Amount owing by the Company to the Investors Group (as part of the Sale Price) shall be automatically set off against the same amount owing by the Investors Group to the Company (pursuant to paragraph (i) above), and neither the Financier nor the Company shall make any claim with respect to payment of such amount by the other party.", $y - 7);

   $y = MLeft2($pdf, "1.6.6", $y + 5);
   $y = MLeft3($pdf, "In the event that on the Purchase Cost Contribution Date, the aggregate amount of the contributions from the Investors Group"
           . " towards the Purchase Cost is lower than the quantum of the Purchase Cost specified in Clause 1.1, but higher than 50% of such quantum,"
           . " Kapital Boost (as agent acting on behalf, and at the instruction, of the Company) shall promptly notify the Investors Group of the intention"
           . " of the Company to (i) continue with the purchase of the Asset in reduced quantity for a total consideration equal to the aggregate amount"
           . " of the contributions from the Investors Group (the \"<b>Adjusted Purchase Cost</b>&nbsp;&nbsp;\"); or otherwise (ii) return the contributions to the contributing"
           . " members of the Investors Groups pursuant to Clause 6.2.; and", $y - 7);

   $y = MLeft3($pdf, "(a)", $y + 2);
   $y = MLeft4($pdf, "following the Company’s election pursuant to Clause 1.6.6 (i):", $y - 7);

   $y = MLeft4($pdf, "(1)", $y + 3);
   $y = MLeft5($pdf, "the Company (as agent of Investors Group) will complete the purchase of the Asset by the Date of Purchase;", $y - 7);

   $y = MLeft4($pdf, "(2)", $y + 3);
   $y = MLeft5($pdf, "this Agreement shall be read to interpret the Purchase Cost as the Adjusted Purchase Cost, and all other relevant provisions"
           . " of this Agreement shall be adjusted accordingly including: (i) a proportional reduction to the Purchase Currency Cost"
           . " and the Sale Price; and (ii) a proportional increase in the percentage of each contributing Financier’s ownership in the Asset, "
           . "each as specified in the notice delivered pursuant to Clause 1.6.6(i) (the \"<b>Adjustment Notice</b>&nbsp;\"); and", $y - 7);

   $y = MLeft4($pdf, "(3)", $y + 3);
   $y = MLeft5($pdf, "there will not be any change to (i) the quantum of each contributing Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price; and", $y - 7);

   $y = MLeft3($pdf, "(b)", $y + 3);
   $y = MLeft4($pdf, "upon delivery by Kapital Boost to the Investors Group of an Adjustment Notice which complies with the requirements of this Agreement, each of the contributing Financiers shall be deemed to have agreed to such adjustments and the Adjustment Notice shall constitute an effective and binding amendment to this Agreement.", $y - 7);

   $y = MLeft2($pdf, "1.6.7", $y + 5);
   $y = MLeft3($pdf, "the circumstance described in 1.6.6, the Company will proceed to purchase the Asset, in reduced quantity for a total consideration equal to the aggregate contribution from consenting Financiers (\"Adjusted Purchase Cost\"), provided that the amount is more than 50% of the Purchase Cost.", $y - 7);

   $y = MLeft1($pdf, "1.7", $y + 5);
   $y = MLeft2($pdf, "The Company, as agent of the Investors Group, undertakes to inspect the Asset prior to delivery / collection, and shall ensure that all specifications in relation to the Asset as specified in Schedule A shall be complied with. The Company undertakes to immediately indemnify the Investors Group for any losses or liabilities resulting directly or indirectly from the failure of the Company to so inspect, and ensure the fitness for purpose or compliance with specifications of, the Asset.", $y - 7);

   $y = MLeft1($pdf, "1.8", $y + 5);
   $y = MLeft2($pdf, "Following the purchase of the Asset by the Company, as agent of the Investors Group, the Company will offer to purchase the Asset (\"<b>Company's Offer to Purchase</b>&nbsp;&nbsp;\") from the Investors Group, for a consideration equal to the Sale Price. The sale of the Asset by the Investors Group to the Company will take effect upon the acceptance of the Company’s Offer to Purchase by Kapital Boost, acting as agent of the Investors Group, in the form of Schedule B. The Sale Price will be paid by the Company to the Investors Group on deferred payment terms as provided in Clause 2 below. The Parties hereby agree that title to the Asset will pass immediately and fully from the Investors Group to the Company upon acceptance of the Company’s Offer to Purchase, in the form of Schedule B.", $y - 7);


   $y = MLeft1($pdf, "2. <b>PAYMENT OF SALE PRICE</b>", $y + 15);

   $y = MLeft1($pdf, "2.1", $y + 5);
   $y = MLeft2($pdf, "The Sale Price will be paid by the Company as follows:-", $y - 7);




   $payoutString = "";
   for ($i = 0; $i < count($payoutDate); $i++) {
      $payoutString .= "<tr><td style=\"text-align:center\">$payoutDate[$i]</td><td style=\"text-align:center\">$payoutAmount[$i]</td></tr>";
      //$y += 14;
   }

   $payoutTbl = "<table cellspacing=\"0\" cellpadding=\"10\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b></td>
  <td style=\"text-align:center\"><b>Sale Price payable</b></td>
  </tr>
  $payoutString
  </table>
  ";
   $pdf->writeHTML($payoutTbl);
   $y = $pdf->GetY();
   $y = MLeft1($pdf, "2.2", $y - 5);
   $y = MLeft2($pdf, "All payments hereunder by the Financier and the Company shall be made in full in the currency of the Sale Price stated in this Agreement, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority.", $y - 7);

   $y = MLeft1($pdf, "2.3", $y + 5);
   $y = MLeft2($pdf, "All payments of the Sale Price or any installments thereof shall be made to each financier within the Investors Group pari passu, based on the percentage contribution to the Purchase Price as described above. The Parties agree that in the event any such payment is not made pari passu, the Parties shall make the necessary payments as between the Investors Group to rectify any over/under payment of the Sale Price.", $y - 7);

   $y = MLeft1($pdf, "3. <b>KAPITAL BOOST AS AGENT</b>", $y + 15);

   $y = MLeft1($pdf, "3.1", $y + 5);
   $y = MLeft2($pdf, "The Financier acknowledges that the Company has appointed, or is hereby deemed to appoint, Kapital Boost as its agent for the purposes of (a) collecting the Purchase Cost from the Investors Group, for payment to the Supplier through the Company; (b) notifying the Investors Group of the election made by the Company pursuant to Clause 1.6.5; and (c) distributing to the Investors Group monies comprising the Sale Price or installments thereof due and payable by the Company.", $y - 7);

   $y = MLeft1($pdf, "3.2", $y + 5);
   $y = MLeft2($pdf, "The Investors Group furthermore hereby (by the electronic delivery of this Agreement to Kapital Boost) appoints Kapital Boost to accept and sign the Murabahah Sale Contract per Schedule B of this Agreement.", $y - 7);

   $y = MLeft1($pdf, "3.3", $y + 5);
   $y = MLeft2($pdf, "For the purposes of the agency under Clause 3.1 and 3.2:", $y - 7);

   $y = MLeft2($pdf, "3.3.1", $y + 3);
   $y = MLeft3($pdf, "Kapital Boost shall (a) transfer to the Company by no later the Purchase Cost Contribution Date, the Purchase Cost received by it from the Investors Group; and (b) upon receipt of any amount of the Sale Price due from the Company to any member of the Investors Group on any particular date, promptly distribute to each financier within the Investors Group its proportion of the amount received, in each case, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority; ", $y - 7);

   $y = MLeft2($pdf, "3.3.2", $y + 3);
   $y = MLeft3($pdf, "Kapital Boost shall not be responsible or liable for any default or shortfall in the payment by the Company of all or any part of the Sale Price due to the Investors Group at any time;", $y - 7);

   $y = MLeft2($pdf, "3.3.3", $y + 3);
   $y = MLeft3($pdf, "Kapital Boost shall ensure that the following terms are included in the Adjustment Notice:", $y - 7);

   $y = MLeft3($pdf, "(a)", $y);
   $y = MLeft4($pdf, "the Adjusted Purchase Price; ", $y - 7);

   $y = MLeft3($pdf, "(b)", $y);
   $y = MLeft4($pdf, "the proportionally reduced Purchase Currency Cost and Sale Price;", $y - 7);

   $y = MLeft3($pdf, "(c)", $y);
   $y = MLeft4($pdf, "the proportionally increased percentage of the Financier’s ownership in the Asset; and", $y - 7);

   $y = MLeft3($pdf, "(d)", $y);
   $y = MLeft4($pdf, "express confirmation that there is no change to (i) the quantum of the Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price.", $y - 7);

   $y = MLeft2($pdf, "3.3.4", $y + 5);
   $y = MLeft3($pdf, "prior to signing the Murabahah Sale Contract as agent of the Investors Group, Kapital Boost shall check the terms of the same to ensure compliance with the terms of this Agreement;", $y - 7);

   $y = MLeft2($pdf, "3.3.5", $y + 5);
   $y = MLeft3($pdf, "Kapital Boost shall inform the Investors Group promptly upon becoming aware of any delay in the Company delivering the Murabahah Sale Contract, any deficiency in the Murabahah Sale Contract or any other reason which would result in Kapital Boost not being able to, or preferring not to, sign the Murabahah Sale Contract; and", $y - 7);

   $y = MLeft2($pdf, "3.3.6", $y + 5);
   $y = MLeft3($pdf, "by taking any of the actions described in this Clause 3.3 as agent of the Investors Group, Kapital Boost is deemed to have accepted its appointment as such agent, and to have agreed to be bound by the terms of this Clause 3.3.", $y - 7);

   $y = MLeft1($pdf, "4. <b>TRANSFER OF OWNERSHIP & WARRANTY</b>", $y + 15);

   $y = MLeft1($pdf, "4.1", $y + 5);
   $y = MLeft2($pdf, "The Company agrees to purchase the Asset on an as-is, where-is basis, and is deemed to have inspected the Asset prior to purchase thereof. The title and ownership of the Asset together with all rights, interest and benefits attached thereto shall pass from the Investors Group to the Company immediately upon the sale of the Asset by the Investors Group to the Company pursuant to the terms of this Agreement, notwithstanding that the Sale Price has not been paid or fully paid by the Company to the Investors Group. The Company waives all claims against the Investors Group as seller for breach in warranty or defects in respect of the Asset. The Company shall immediately at the point of purchase, collect and take possession of the Asset from the Investors Group or its agent(s), if any.", $y - 7);

   $y = MLeft1($pdf, "4.2", $y + 5);
   $y = MLeft2($pdf, "The Investors Group assigns to the Company its warranty and other rights against the supplier / seller of the Assets at the point of purchase by the Company.", $y - 7);

   $y = MLeft1($pdf, "5. <b>EARLY PAYMENT OF SALE PRICE</b>", $y + 15);

   $y = MLeft2($pdf, "The Company may opt at its discretion to effect early payment of all or any part of the Sale Price. Any early payment of part of the Sale Price shall reduce the Sale Price payment obligations of the Company in inverse chronological order, and any such early payment of any part of the Sale Price shall not delay, reduce or otherwise affect the remaining payment obligations of the Company in respect of the Sale Price. Following any early payment of any part of the Sale Price, the Investors Group or any individual Investor may at their or his absolute discretion grant ibra’ (rebate) on any remaining part(s) of the Sale Price or, as the case may be, that Investor’s share of the Sale Price. Both parties acknowledge that charging and/or claiming early settlement charges is not in accordance with Shariah law.", $y + 5);

   $y = MLeft1($pdf, "6. <b>AUTOMATIC CANCELLATION</b>", $y + 15);

   $y = MLeft1($pdf, "If:", $y);

   $y = MLeft1($pdf, "6.1", $y + 5);
   $y = MLeft2($pdf, "the Company, as agent of the Investors Group, fails for whatever reason to complete the purchase of the Asset by the Date of Purchase; or", $y - 7);

   $y = MLeft1($pdf, "6.2", $y + 5);
   $y = MLeft2($pdf, "he Company, as agent of the Investors Group, does not receive the full amount of the Purchase Cost from the Investors Group, by no later than the Purchase Cost Contribution Date (unless the Company elects, pursuant to Clause 1.6.6, to complete the purchase of the Asset based on the Adjusted Purchase Cost, and thereafter completes such purchase of the Asset by the Date of Purchase); or", $y - 7);

   $y = MLeft1($pdf, "6.3", $y + 5);
   $y = MLeft2($pdf, "prior to the completion of the purchase of the Asset by the Company, as agent of the Investors Group, an Event of Default occurs or it becomes illegal in any applicable jurisdiction or under any applicable laws or regulations for the Investors Group or the Company, as agent of the Investors Group, to purchase the Asset,", $y - 7);

   $y = MLeft2($pdf, "the Parties agree that the Murabahah transaction described in this Agreement shall be immediately and automatically cancelled, and the Company shall immediately refund the Investors Group all monies advanced or paid to it for the purchase of the Asset on behalf of the Investors Group, and will in addition indemnify each Financer in respect of all actual costs and expenses incurred by him in relation to the entry into this Agreement and the advance of funds to the Company.", $y + 5);

   $y = MLeft1($pdf, "7. <b>EVENTS OF DEFAULT</b>", $y + 15);

   $y = MLeft1($pdf, "7.1", $y + 5);
   $y = MLeft2($pdf, "An Event of Default is deemed to have occurred in any of the following circumstances, whether or not arising due to the fault of the Company:-", $y - 7);

   $y = MLeft2($pdf, "7.1.1", $y + 3);
   $y = MLeft3($pdf, "The Company fails to fulfill any of its payment obligations in respect of the Sale Price, unless such failure is a result of a technical or administrative error on the part of the bank or financial institution remitting payment by the Company to Kapital Boost, or by Kapital Boost to the Financier; or", $y - 7);

   $y = MLeft2($pdf, "7.1.2", $y + 3);
   $y = MLeft3($pdf, "The Company fails to observe and perform any of its obligations under this Agreement (including, without limitation (a) its obligation to offer to purchase the Asset from the Investors Group, and (b) its obligations as agent of the Investors Group to purchase the Asset at the Purchase Cost and to accept the Offer to Purchase); or", $y - 7);

   $y = MLeft2($pdf, "7.1.3", $y + 3);
   $y = MLeft3($pdf, "Any representation, statement, or warranty given or made by the Company in this Agreement proves to be untrue, incorrect or inaccurate or misleading in any material respect; or", $y - 7);

   $y = MLeft2($pdf, "7.1.4", $y + 3);
   $y = MLeft3($pdf, "Any step is taken for the winding up, liquidation or dissolution of the Company;", $y - 7);

   $y = MLeft2($pdf, "7.1.5", $y + 3);
   $y = MLeft3($pdf, "Where there is an element of fraud, misuse, or misappropriation of Purchase Cost by the Company", $y - 7);

   $y = MLeft1($pdf, "7.2", $y + 5);
   $y = MLeft2($pdf, "Upon the occurrence of any Event of Default, the Financier shall have the right by notice to the Company to:", $y - 7);

   $y = MLeft2($pdf, "7.2.1", $y + 3);
   $y = MLeft3($pdf, "declare the entire proportion of the Sale Price outstanding from the Company to the Financier to become immediately due and payable; and/or", $y - 7);

   $y = MLeft2($pdf, "7.2.2", $y + 3);
   $y = MLeft3($pdf, "require the Company to (a) sell the Asset and apply all proceeds of sale to pay the outstanding Sale Price; and (b) otherwise not dispose of or transfer the ownership of the Asset to any other person; and/or", $y - 7);

   $y = MLeft2($pdf, "7.2.3", $y + 3);
   $y = MLeft3($pdf, "require the Company to indemnify the Investors Group for any costs, losses or expenses incurred as a result of the occurrence of any Event of Default including, without limitation (a) any loss of profit by the Financier arising from (i) the failure by the Company to fulfill its undertaking to offer to purchase the Asset from the Investors Group, or (ii) failure by the Company as agent of the Investors Group to purchase the Asset at the Purchase Cost or to accept the Offer to Purchase, (b) any costs incurred in taking physical possession of the Asset following an Event of Default, and (c) any diminution in the value of the Asset arising from a breach by the Company of the terms of this Agreement.", $y - 7);

   $y = MLeft1($pdf, "7.3", $y + 5);
   $y = MLeft2($pdf, "The right of the Financier to accelerate the obligations of the Company in respect of the portion of the Sale Price due to the Financier may be exercised irrespective of, and without being contingent upon, the action or inaction of any other financier or the rest of the Investors Group. The Financier shall be entitled to bring or prosecute any legal action or claim against the Company to recover the outstanding sum, and the Company agrees to immediately upon demand indemnify the Financier for all actual costs, losses and liabilities (including legal fees) incurred or suffered by the Financier as a result of any Event of Default.", $y - 7);

   $y = MLeft1($pdf, "8. <b>REPRESENTATIONS</b>", $y + 15);

   $y = MLeft1($pdf, "8.1", $y + 5);
   $y = MLeft2($pdf, "The Company represents to the Financier on the date of this Agreement
  that:", $y - 7);

   $y = MLeft2($pdf, "8.1.1", $y + 3);
   $y = MLeft3($pdf, "It is duly incorporated and validly existing under the laws of Singapore;", $y - 7);

   $y = MLeft2($pdf, "8.1.2", $y + 3);
   $y = MLeft3($pdf, "It has the power to execute this Agreement, and to perform its obligations under this Agreement and has taken all necessary action to authorize such execution and performance;", $y - 7);

   $y = MLeft2($pdf, "8.1.3", $y + 3);
   $y = MLeft3($pdf, "The execution and performance of this Agreement does not violate or conflict with any applicable law or regulation, any provision of its constitutional documents, and order or judgment of any court or other agency of government applicable to it, or any contractual restriction binding on it;", $y - 7);

   $y = MLeft2($pdf, "8.1.4", $y + 3);
   $y = MLeft3($pdf, "All consent required to have been obtained by it with respect to this Agreement have been obtained and are in full force and effect;", $y - 7);

   $y = MLeft2($pdf, "8.1.5", $y + 3);
   $y = MLeft3($pdf, "Its obligations under this Agreement constitute its legal, valid and binding obligations, enforceable in accordance with their respective terms;", $y - 7);

   $y = MLeft2($pdf, "8.1.6", $y + 3);
   $y = MLeft3($pdf, "No Event of Default has occurred or would reasonably be expected to result from the entry into, or performance of, this Agreement; and", $y - 7);

   $y = MLeft2($pdf, "8.1.7", $y + 3);
   $y = MLeft3($pdf, "The core or main business of the Company and the purpose of the financing under this Agreement are and shall at all times remain Shariah compliant.", $y - 7);

   $y = MLeft1($pdf, "8.2", $y + 5);
   $y = MLeft2($pdf, "The representations and warranties in this Clause 6 are repeated by the Company on the Purchase Date and on each scheduled date for payment of all or part of the Sale Price.", $y - 7);

   $y = MLeft1($pdf, "9. <b>LATE PAYMENT CHARGES (TA’WIDH AND GHARAMAH)</b>", $y + 15);

   $y = MLeft1($pdf, "9.1", $y + 5);
   $y = MLeft2($pdf, "If any amount which is due and payable by the Company under or in connection with this Agreement is not paid in full on the date in accordance with this Agreement (an \"<b>unpaid amount</b>&nbsp;\"), the Company undertakes to pay late payment charge comprising both ta’widh and gharamah (calculated below) to the Investor Group on each day that an unpaid amount remains outstanding.", $y - 7);

   $y = MLeft1($pdf, "9.2", $y + 5);
   $y = MLeft2($pdf, "The total late payment charge shall be pegged directly to average annualized returns of this Murabahah Agreement (to be determined by the Company), calculated on daily rest basis as below, provided that the accumulated late payment charges shall not exceed 100 percent of the outstanding unpaid amount:", $y - 7);

   $y = MCenter1($pdf, "Unpaid Amount X average annualized returns X", $y + 5);
   $y = MCenter1($pdf, "No. of Overdue day(s) / 365", $y);

   $y = MLeft1($pdf, "9.3", $y + 5);
   $y = MLeft2($pdf, "Each Financier may retain only such part of the late payment amount paid to it, as is necessary to compensate the Financier for any actual costs (not to include any opportunity or funding costs), subject to and in accordance with the guidelines of its own Shariah advisers, and up to a maximum aggregate amount of 1% per annum of the unpaid amount (“ta’widh”). ", $y - 7);

   $y = MLeft1($pdf, "9.4", $y + 5);
   $y = MLeft2($pdf, "The ta’widh in respect of an unpaid amount, being part of the late payment charges, may accrue on a daily basis and shall be calculated in accordance with the following formula:-", $y - 7);

   $y = MCenter1($pdf, "Actual cost incurred", $y + 3);
   $y = MCenter1($pdf, "or", $y);
   $y = MCenter1($pdf, "Unpaid Amount X 1% X No. of Overdue day(s) / 365", $y);

   $y = MLeft1($pdf, "9.5", $y + 5);
   $y = MLeft2($pdf, "The amount of ta’widh shall not be compounded.", $y - 7);

   $y = MLeft1($pdf, "9.6", $y + 5);
   $y = MLeft2($pdf, "The Investor Group hereby agree(s), covenant(s) and undertake(s) to ensure that the balance of the late payment charges (“gharamah”), if any, shall be paid to charitable bodies as may be selected by the Company in consultation with its Shariah advisers.", $y - 7);

   $y = MLeft1($pdf, "10. <b>GOVERNING LAW AND DISPUTE RESOLUTION</b>", $y + 15);

   $y = MLeft1($pdf, "10.1", $y + 5);
   $y = MLeft2($pdf, "This Agreement and the rights and obligations of the Parties hereunder shall be governed by and interpreted and construed in all respects in accordance with the laws of Singapore without prejudice to or limitation of any other rights or remedies available to both Parties in this Agreement under the laws of any jurisdiction where the Company or his assets may be located.", $y - 7);

   $y = MLeft1($pdf, "10.2", $y + 5);
   $y = MLeft2($pdf, "The Parties hereby irrevocably agree to submit to the exclusive jurisdiction of the Courts of Singapore to resolve any disputes or make any claims with respect to this Agreement.", $y - 7);

   $y = MLeft1($pdf, "11. <b>MISCELLANEOUS</b>", $y + 15);

   $y = MLeft1($pdf, "11.1", $y + 5);
   $y = MLeft2($pdf, "If at any time any one or more of the provisions hereof is or become illegal, invalid or unenforceable under Singapore law, neither the legality, validity or enforceability of the remaining provisions hereof nor the legality, validity or enforceability of such provisions under the laws of any other jurisdiction shall in any way be affected or impaired thereby.", $y - 7);

   $y = MLeft1($pdf, "11.2", $y + 5);
   $y = MLeft2($pdf, "If any provision of this Agreement (or part of it) or the application thereof to any person or circumstance shall be illegal, invalid or unenforceable to any extent, it must be interpreted as narrowly as necessary to allow it to be enforceable or valid and the remainder of this Agreement and the legality, validity or enforceability of such provisions to other persons or circumstances shall not be in any way affected or impaired thereby and shall be enforceable / enforced to the greatest extent permitted by law.", $y - 7);

   $y = MLeft1($pdf, "11.3", $y + 5);
   $y = MLeft2($pdf, "All rights and obligations in this Agreement are personal to the Parties and each Party in this Agreement may not assign and/or transfer any such rights and obligations to any third party without the prior consent in writing of the Parties.", $y - 7);

   $y = MLeft1($pdf, "11.4", $y + 5);
   $y = MLeft2($pdf, "The Contracts (Rights of Third Parties) Act 2001 shall not apply to this Agreement and no person who is not a party of this Agreement shall have any rights under the Contracts (Rights of Third Parties) Act 2001 to enforce any term of this Agreement.", $y - 7);

   $y = MLeft1($pdf, "11.5", $y + 5);
   $y = MLeft2($pdf, "This Agreement contains the entire understanding between the Parties relating to the transaction contemplated by this Agreement and shall supersede any prior expressions of intent or understandings with respect to the said transaction. All prior or contemporaneous agreements, understandings, representations and statements, oral and written, are merged in this Agreement and shall be of no further force or effect.", $y - 7);

   $y = MLeft1($pdf, "11.6", $y + 5);
   $y = MLeft2($pdf, "Any communication to be made under or in connection with this Agreement shall be made in writing and, may be made by letter or electronic mail, in each case, through Kapital Boost at <b>150 Changi Road, #04-07, Singapore 419973</b>&nbsp;&nbsp;&nbsp;&nbsp; or at such other address as Kapital Boost may notify the parties from time to time. Any communication made between Kapital Boost and any of the Parties under or in connection with this Agreement shall be made to the address or electronic mail address provided to Kapital Boost or its registered address, in the case of the Company, and shall be effective when received.", $y - 7);

   $y = MLeft1($pdf, "11.7", $y + 5);
   $y = MLeft2($pdf, "No failure to exercise, nor any delay in exercising, on the part of the Financier, any right or remedy under this Agreement shall operate as a waiver, nor shall any single or partial exercise of any right or remedy prevent any further or other exercise or the exercise of any other right or remedy. The rights and remedies under this Agreement are cumulative and not exclusive of any rights or remedies provided by law.", $y - 7);

   $y = MLeft1($pdf, "11.8", $y + 5);
   $y = MLeft2($pdf, "No provision of this Agreement may be amended, waived, discharged or terminated orally nor may any breach of or default under any of the provisions of this Agreement be waived or discharged orally but (in each case) except (a) pursuant to an Adjustment Notice which complies with the requirements of this Agreement; or (b) by an instrument in writing signed by or on behalf of the Parties or (b). Any amendments or variations to this Agreement shall be Shariah-compliant.", $y - 7);

   $y = MLeft1($pdf, "11.9", $y + 5);
   $y = MLeft2($pdf, "This Agreement may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);

   $y = MLeft1($pdf, "11.10", $y + 5);
   $y = MLeft2($pdf, "Each Party agrees to keep all information relating to this Agreement confidential, and not disclose it to anyone, save with the prior written consent of the other Parties or as required by any applicable laws or regulations.", $y - 7);

   $y = MLeft1($pdf, "11.11", $y + 5);
   $y = MLeft2($pdf, "This Agreement is intended to be Shariah-compliant. The parties hereby agree and acknowledge that their respective rights and obligations under this Agreement are intended to, and shall, be in conformity with Shariah principles.", $y - 7);

   $y = MLeft1($pdf, "11.12", $y + 5);
   $y = MLeft2($pdf, "Notwithstanding the above, each party represents to the other that it shall not raise any objections or claims against the other on the basis of Shariah-compliance or any breach of Shariah principles in respect of or otherwise in relation to any part of any provision of this Agreement.  ", $y - 7);


   $y = MLeft1($pdf, "12. <b>DEFINITIONS</b>", $y + 15);

   $y = MLeft1($pdf, "In this Agreement, any reference to:", $y + 5);

   $y = MLeft1($pdf, "12.1", $y + 5);
   $y = MLeft2($pdf, "any Party or Kapital Boost shall be construed so as to include its successors in title, permitted assigns and permitted transferees;", $y - 7);

   $y = MLeft1($pdf, "12.2", $y + 5);
   $y = MLeft2($pdf, "a provision of law is a reference to that provision as amended or re-enacted;", $y - 7);

   $y = MLeft1($pdf, "12.3", $y + 5);
   $y = MLeft2($pdf, "a person or Party in a particular gender shall include that person or Party regardless of his or her gender, and shall apply equally where that person or Party is a body corporate, firm, trust, joint venture or partnership;", $y - 7);

   $y = MLeft1($pdf, "12.4", $y + 5);
   $y = MLeft2($pdf, "the \"<b>Investors Group</b>&nbsp; \" shall be construed to mean just the Financier if the Financier is financing the entirety of the Purchase Cost;", $y - 7);

   $y = MLeft1($pdf, "12.5", $y + 5);
   $y = MLeft2($pdf, "\"<b>Kapital Boost</b>&nbsp; \" means <b>Kapital Boost Pte Ltd, Singapore Company Registration No. 201525866W</b>", $y - 7);

   $y = MLeft1($pdf, "12.6", $y + 5);
   $y = MLeft2($pdf, "\"<b>Sale Price</b>&nbsp; \" shall mean the Sale Price as adjusted pursuant to Clause 1.6.6.", $y - 7);

   if ($y > 160) { // Shift the sign table below
      $pdf->AddPage('P', array('210', '297'));
      $y = 0;
   }
   $pdf->SetMargins(25, 15);
   $y = MCenter1($pdf, "<b>Dated this day $date</b><br>", $y + 15);

   $signTbl = "<table cellspacing=\"0\" cellpadding=\"10\" border=\"1\">
  <tr>
  <td ><b>The Company</b><br>Signed for and on behalf of<br><b>$companyName</b></td>
  <td ><b>The Financier</b></td>
  </tr>
  <tr>
       <td style=\"line-height:20px\"><br><br><br><hr><br><br><br><b>Name: $dirName</b><br><b>Title: $dirTitle</b><br><b>$dirIC</b></td>
       <td style=\"line-height:20px\"><br><br><br><hr><br><br><br><b>$invName</b><br><b>$invIC</b></td>
</tr>

  </table>";

   $pdf->writeHTML($signTbl);
   $pdf->AddPage('P', array('210', '297'));
   $y = MLeft1($pdf, "<b>SCHEDULE A: Description of Assets</b>", 20);


   $pdf->Image($image, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetMargins(25, 15);

   $y = MLeft1($pdf, "<b style=\"font-size:1.2em\">SCHEDULE B: Form of Murabahah Sale Contract</b>", 20);

   $y = MCenter1($pdf, "<b>Murabahah Sale Contract</b>", $y + 15);

   $y = MCenter1($pdf, "Between $companyName (the \"<b>Company</b> \") and the Investors Group", $y + 5);

   $y = MLeft1($pdf, "Re: &nbsp;&nbsp;Murabahah Agreements between the Company and various Financiers (together forming the Investors Group) with respect to the purchase of the Asset described below (the \"<b>Murabahah Agreements</b>&nbsp;&nbsp;\", and each, a \"<b>Murabahah Agreement</b>&nbsp;&nbsp;\"). ", $y + 15);

   $y = MLeft1($pdf, "1.", $y + 10);
   $y = MLeft2($pdf, "All terms defined in the Murabahah Agreements have the same meaning in this Murabahah Sale Contract.", $y - 7);

   $y = MLeft1($pdf, "2.", $y + 5);
   $y = MLeft2($pdf, "The Company hereby confirms that it, as agent of the Investors Group, has purchased the Asset, and that the Asset is in the Company’s possession. The Company has attached to this Murabahah Sale Contract (a) a description of the Asset; and (b) a true copy of the receipt for the purchase of the Asset by us, as agent of the Investors Group.", $y - 7);

   $y = MLeft1($pdf, "3.", $y + 5);
   $y = MLeft2($pdf, "In accordance with the terms of the Murabahah Agreements, the Company hereby offers to purchase the Asset from the Investors Group based on the terms and subject to the conditions set out in the Murabahah Agreements. Pursuant to the terms of the Murabahah Agreements, the Sale Price is $salePrice, and will be paid as follows:", $y - 7);

   $payoutBString = "";
   for ($i = 0; $i < count($payoutDate); $i++) {
      $payoutBString .= "<tr><td style=\"text-align:center\">$payoutDate[$i]</td><td style=\"text-align:center\">$payoutAmount[$i]</td></tr>";
   }

   $payoutBTbl = "<table cellspacing=\"0\" cellpadding=\"5\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b></td>
  <td style=\"text-align:center\"><b>[Installment Amount of] Sale Price payable to Investors Group</b></td>
  </tr>
  $payoutBString
  </table>
  ";
   $pdf->SetY($y + 10);
   $pdf->writeHTML($payoutBTbl);
   $y = $pdf->GetY();

   $y = MLeft1($pdf, "4.", $y);
   $y = MLeft2($pdf, "By accepting and agreeing to this Murabahah Sale Contract, the Investors Group shall immediately sell the Asset to the Company on the terms and subject to the conditions set out in the Murabahah Agreement.", $y - 7);

   $y = MLeft1($pdf, "5.", $y + 10);
   $y = MLeft2($pdf, "This Murabahah Sale Contract may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);

   $pdf->AddPage('P', array('210', '297'));
   $pdf->SetMargins(25, 15);

   $y = MLeft1($pdf, "For and on behalf of the Company (as buyer of Asset)", 20);

   $y = MLeft1($pdf, "<hr style=\"width:150;\">", $y + 25);
   $y = MLeft1($pdf, "Name:", $y + 2);
   $y = MLeft1($pdf, "Title:", $y + 2);
   $y = MLeft1($pdf, "Date:", $y + 2);

   $y = MLeft1($pdf, "Kapital Boost, as agent for and on behalf of the Investors Group (as seller of Asset)", $y + 15);

   $y = MLeft1($pdf, "<hr style=\"width:150\">", $y + 25);
   $y = MLeft1($pdf, "Name:", $y + 2);
   $y = MLeft1($pdf, "Title:", $y + 2);
   $y = MLeft1($pdf, "Date:", $y + 2);


   $output = $pdf->Output();
} else if ($conType == 1) {

   $companyRegistrationID = $comRegId;
   $purchaseAssetID = $purchaseAssetId;


   $cCloseDateID = "1 Januari 2018";

   $cCloseDate1ID = "1 Maret 2018";

   $dirICID = $dirIcId;

   $pdf = new FPDI();
   $pdf->setPrintHeader(false);
   $pdf->setPrintFooter(false);


   $pdf->AddPage('P', array('210', '300'));
   $pdf->SetFont('Helvetica');



   $y = 20;

   $y = CenterR($pdf, "TERTANGGAL HARI INI $dateID", $y);

   $pdf->SetFontSize("25");
   $y = CenterR($pdf, ". . . . . . . . . . . . . . . .", $y);

   $pdf->SetFontSize("14");
   $y = CenterR($pdf, "PERJANJIAN MURABAHAH", $y, true);

   $pdf->SetFontSize("25");
   $y = CenterR($pdf, ". . . . . . . . . . . . . . . .", $y - 6);
   $pdf->SetFontSize("12");

   $y = Right1($pdf, "<b>PERJANJIAN INI</b> dibuat antara para pihak sebagai berikut: ", $y + 5);

   $y = Right1($pdf, "<b>1. $companyNameID</b>", $y + 5);

   $y = Right1($pdf, "($companyRegistrationID)", $y);

   $y = Right1($pdf, "(selanjutnya disebut sebagai \"<b>Perusahaan</b>\") ", $y + 5);

   $y = Right1($pdf, "<b>DAN</b>", $y + 5);

   $y = Right1($pdf, "<b>2. $invName</b><br> ($invIC)", $y + 5);

   $y = Right1($pdf, "(selanjutnya disebut sebagai \"<b>Pemodal</b>  \")", $y + 5);

   $y = Right1($pdf, "<b>BAHWA:</b>", $y + 5);

   $y = Right1($pdf, "A.", $y + 5);

   $y = Right2($pdf, "Perusahaan memerlukan pendanaan untuk pembelian, akuisisi atau pengadaan $purchaseAssetID, yang dijelaskan dalam Lampiran A] (\"<b>Aset</b> \"), yang bermaksud digunakan Perusahaan untuk usahanya, dan Perusahaan berkeinginan memperoleh pembiayaan tersebut sesuai dengan prinsip-prinsip Syariah; ", $y - 7);

   $y = Right1($pdf, "B.", $y + 5);

   $y = Right2($pdf, "Pemodal, bersama-sama dengan beberapa pemodal lainnya (secara bersama-sama disebut \"<b>Grup Investor</b> \") telah sepakat untuk menyediakan pembiayaan yang diperlukan kepada Perusahaan berdasarkan prinsip Murabahah dalam Syariah, khususnya Murabahah terhadap pemberi pesanan pembelian; dan", $y - 7);

   $y = Right1($pdf, "C.", $y + 5);

   $y = Right2($pdf, "Pemodal telah sepakat untuk memberikan bagiannya dalam pembiayaan yang diperlukan berdasarkan ketentuan dan tunduk pada persyaratan Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "<b>DENGAN INI DISEPAKATI </b> sebagai berikut:", $y + 10);
   $y = Right1($pdf, "<b>1. METODE PEMBIAYAAN</b>", $y + 10);

   $y = Right1($pdf, "1.1", $y + 10);

   $y = Right2($pdf, "Untuk tujuan pembiayaan akuisisi Aset, Perusahaan dengan ini meminta agar Grup Investor secara bersama-sama membeli Aset dari Pemasok untuk total imbalan <b>$assetCost</b> (\"<b>Biaya Pembelian</b>&nbsp;&nbsp;\"), dan selanjutnya menjual Aset kepada Perusahaan dengan harga jual  <b>$salePrice</b> (\"<b>Harga Jual</b> \"). [Biaya sesungguhnya kepada Perusahaan atas pembelian Aset adalah <b>$assetCostCurrency</b> (\"<b>Biaya Mata Uang Pembelian </b>&nbsp; \"), yang mata uangnya dapat dibeli menggunakan jumlah Dolar Singapura atas Biaya Pembelian dengan nilai tukar mata uang yang berlaku sebesar  S$1:$exchange Perusahaan berjanji (melalui wa’d) untuk membeli Aset dari Grup Investor, setelah pembelian Aset oleh Grup Investor sesuai dengan ketentuan Perjanjian ini. Skema Murabahah yang ditetapkan di atas akan menghasilkan laba bagi Grup Investor sebagaimana ditetapkan di bawah ini.", $y - 7);

   $tbl1 = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td><b>Biaya Pembelian Aset yang wajib dibayarkan oleh Grup Investor</b></td>
  <td><b>Harga Jual yang wajib dibayarkan oleh Perusahaan kepada Grup Investor</b></td>
  <td><b>umlah Laba yang berasal dari Grup Investor</b></td>
  </tr>
  <tr>
  <td>$assetCost</td>
  <td>$salePrice</td>
  <td>$profit (atau $return% dari Biaya Pembelian Aset)</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetX(120);
   $pdf->SetFontSize(10);
   $pdf->writeHTML($tbl1);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();




   $y = Right1($pdf, "1.2", $y);

   $y = Right2($pdf, "Para pihak sepakat bahwa Biaya Pembelian mengacu pada biaya akuisisi sesungguhnya atas Aset yang dikeluarkan atau akan dikeluarkan oleh Grup Investor atau agen Grup Investor untuk akuisisi Aset, di luar pengeluaran-pengeluaran langsung (seperti biaya-biaya pengiriman, transportasi, penyimpanan dan perakitan, pajak-pajak, biaya takaful dan biaya pertanggungan asuransi) dan belanja rutin atau biaya-biaya tidak langsung (seperti gaji staf dan biaya buruh).", $y - 7);

   $y = Right1($pdf, "1.3", $y + 5);

   $y = Right2($pdf, "Pemodal bersedia membiayai persisnya  $invAmount (yakni $invPercent%) dari keseluruhan jumlah Biaya Pembelian. Jumlah tersebut yang akan dibiayai oleh Pemodal akan dibayarkan oleh Pemodal kepada Pemasok, melalui Perusahaan sebagai agen. Untuk tujuan memungkinkan Kapital Boost, sebagai agen Perusahaan berdasarkan Pasal 3.3.1, untuk mengalihkan jumlah Biaya Pembelian kepada Perusahaan selambat-lambatnya $cCloseDateID (the \"<b>Tanggal Kontribusi Biaya Pembelian</b>&nbsp; \"), Pemodal akan mengalihkan jumlah kontribusinya terhadap Biaya Pembelian (sebagaimana ditetapkan dalam pasal ini di atas) ke rekening yang ditetapkan secara tertulis oleh Kapital Boost, paling lambat <b>tiga (3) hari kerja</b>&nbsp;&nbsp;&nbsp; sejak tanggal Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "1.4", $y + 5);

   $y = Right2($pdf, "Berdasarkan kontribusi Pemodal terhadap Biaya Pembelian sebagaimana ditetapkan di atas, Pemodal akan memiliki $invPercent% dari Aset setelah pembelian Aset oleh Grup Investor, dan Pemodal akan berhak atas bagian $invPercent% dari Harga Jual dan setiap angsuran atau bagiannya dibayarkan oleh Perusahaan, sejumlah:-", $y - 7);


   BulletR($pdf, $y);

   $y = Right3($pdf, "Bagian Pemodal dari Harga Jual: $invSalePrice", $y + 3);

   BulletR($pdf, $y);

   $y = Right3($pdf, "Bagian Pemodal dari Laba: $invProfit", $y + 3);

   $y = Right1($pdf, "1.5", $y + 5);

   $y = Right2($pdf, "Pemodal (sebagai bagian dari Grup Investor) dengan ini menunjuk Perusahaan sebagai agennya untuk membeli Aset atas nama Pemodal. Perusahaan dengan ini menerima penunjukan sebagai agen tersebut, dan akan membeli dan menangani Aset sesuai dengan ketentuan Perjanjian ini. Perusahaan, sebagai agen, akan membeli Aset dalam mata uang Biaya Mata Uang Pembelian, untuk imbalan yang tidak melebihi Biaya Mata Uang Pembelian, dan pada selambat-lambatnya <b>$cCloseDate1ID</b>&nbsp;(\"<b>Tanggal Pembelian</b>&nbsp; \"). ", $y - 7);

   $y = Right1($pdf, "1.6", $y + 5);

   $y = Right2($pdf, "Pemodal dan Perusahaan (sebagai agen Grup Investor untuk tujuan pembelian Aset) dengan ini mengakui dan menyepakati bersama bahwa:", $y - 7);

   $y = Right1($pdf, "1.6.1", $y + 3);

   $y = Right3($pdf, "Grup Investor (termasuk Pemodal) telah menunjuk Perusahaan untuk bertindak sebagai agen Grup Investor untuk membeli Aset atas nama Grup Investor karena pengetahuan dan jaringan pasar Perusahaan terkait dengan Aset;", $y - 7);

   $y = Right1($pdf, "1.6.2", $y + 3);

   $y = Right3($pdf, "pembelian Aset oleh Grup Investor melalui Perusahaan sebagai agen dapat menghasilkan manfaat bagi Grup Investor melalui efisiensi proses pembelian dan juga penghematan biaya dalam hal Perusahaan dapat menjamin atau memperoleh Aset dengan harga yang lebih rendah daripada Biaya Mata Uang Pembelian;", $y - 7);

   $y = Right1($pdf, "1.6.3", $y + 3);

   $y = Right3($pdf, "dalam hal Perusahaan, sebagai agen, dapat membeli Aset dengan harga lebih rendah daripada Biaya Mata Uang Pembelian, Grup Investor dengan ini sepakat untuk mengesampingkan, dengan cara tanazul (yakni pengesampingan), hak-haknya untuk mengklaim kelebihan jumlah pendanaan yang tidak digunakan; dan Perusahaan, sebagai agen, berhak untuk menyimpan manfaat penghematan biaya tersebut sebagai insentif berdasarkan hibah atas pelaksanaan kerjanya yang baik; ", $y - 7);

   $y = Right1($pdf, "1.6.4", $y + 3);

   $y = Right3($pdf, "dalam keadaan yang dijelaskan dalam Pasal 1.6.3, biaya terhadap Grup Investor atas pembelian Aset akan merupakan Biaya Pembelian, sebagai jumlah hibah yang diberikan kepada Perusahaan, sebagai agen, akan merupakan bagian dari biaya sesungguhnya atas pembelian Aset, dan dengan demikian Biaya Pembelian yang sama akan dicerminkan pada Kontrak Penjualan Murabahah yang akan dibuat oleh Grup Investor dalam penjualan Aset; ", $y - 7);

   $y = Right1($pdf, "1.6.5", $y + 3);
   $synp0 = $pdf->getPage();
   $y = Right3($pdf, "dalam hal Perusahaan tidak dapat membeli $foreignCurrency yang memadai sebagai Biaya Mata Uang Pembelian menggunakan Biaya Pembelian yang dikontribusikan oleh Grup Investor sebagai hasil penurunan nilai tukar mata uang dari nilai tukar mata uang yang disebutkan dalam Pasal 1.1:", $y - 7);

   $y = Right3($pdf, "(a)", $y + 2);

   $y = Right4($pdf, "Perusahaan, sebagai agen Grup Investor, akan:", $y - 7);

   $y = Right4($pdf, "(i)", $y);

   $y = Right5($pdf, "segera memberitahukan Grup Investor dan mengembalikan Biaya Pembelian kepada Grup Investor, yang setelahnya Perjanjian ini akan berakhir; atau ", $y - 7);

   $y = Right4($pdf, "(ii)", $y);

   $y = Right5($pdf, "mendanai jumlah kekurangan dalam mata uang Biaya Pembelian (the \"<b>Jumlah Kekurangan Biaya Pembelian</b>&nbsp;&nbsp; \") yang akan disyaratkan bagi Perusahaan untuk membeli Biaya Mata Uang Pembelian untuk merampungkan pembelian Aset sebagai agen atas nama Grup Investor;", $y - 7);

   $y = Right3($pdf, "(b)", $y + 3);

   $y = Right4($pdf, "mengikuti pendanaan Jumlah Kekurangan Biaya Pembelian oleh Perusahaan dan perampungan pembelian Aset oleh Perusahaan, sebagai agen Grup Investor:", $y - 7);

   $y = Right4($pdf, "(i)", $y);
   $y = Right5($pdf, "Jumlah Kekurangan Biaya Pembelian akan merupakan utang yang terutang oleh Grup Investor kepada Perusahaan, dalam kedudukannya sebagai agen bagi Grup Investor;", $y - 7);

   $y = Right4($pdf, "(ii)", $y);
   $y = Right5($pdf, "meskipun terdapat ayat (i) di atas, Perusahaan tidak akan, sebelum balik nama Aset menurut Pasal 1.8, memiliki klaim apa pun terhadap bagian apa pun dari bagian hak milik terhadap Aset; ", $y - 7);

   $y = Right4($pdf, "(iii)", $y);
   $y = Right5($pdf, "Harga Jual (atau, jika terdapat lebih dari satu angsuran atas Harga Jual, maka yang angsuran pertama) akan dinaikkan dengan Jumlah Kekurangan Biaya Pembelian, yang akan dicerminkan pada Penawaran Pembelian Perusahaan yang diserahkan oleh Perusahaan kepada Grup Investor; dan", $y - 7);

   $y = Right4($pdf, "(iv)", $y);
   $y = Right5($pdf, "Grup Investor, Perusahaan (dalam kedudukannya sebagai agen Grup Investor) dan sebagai pembeli Aset, semuanya sepakat bahwa Jumlah Kekurangan Biaya Pembelian yang terutang oleh Perusahaan kepada Grup Investor (sebagai bagian dari Harga Jual) akan dengan sendirinya dipotong terhadap jumlah yang sama yang terutang oleh Grup Investor kepada Perusahaan (sesuai dengan ayat (i) di atas), dan baik Pemodal atau Perusahaan tidak akan mengajukan tuntutan sehubungan dengan pembayaran atas jumlah tersebut oleh pihak lain.", $y - 7);

   $y = Right1($pdf, "1.6.6", $y + 5);
   $y = Right3($pdf, "Dalam hal pada Tanggal Kontribusi Biaya Pembelian, keseluruhan jumlah kontribusi dari Grup Investor terhadap Biaya Pembelian lebih rendah daripada jumlah Biaya Pembelian yang ditetapkan pada Pasal 1.1, tetapi lebih tinggi daripada 50% dari jumlah tersebut, Kapital Boost (sebagai agen yang bertindak atas nama, dan dengan instruksi, Perusahaan) akan segera memberitahukan Grup Investor atas maksud Perusahaan untuk (i) melanjutkan pembelian Aset dalam jumlah yang berkurang untuk total imbalan yang setara dengan keseluruhan jumlah kontribusi dari Grup Investor (the \"<b>Biaya Pembelian yang Disesuaikan</b>&nbsp;&nbsp;\"); atau yang lain (ii) mengembalikan kontribusi kepada anggota-anggota Grup Investor menurut Pasal 6.2.; dan", $y - 7);

   $y = Right3($pdf, "(a)", $y + 2);
   $y = Right4($pdf, "mengikuti pemilihan Perusahaan menurut Pasal 1.6.6 (i):", $y - 7);

   $y = Right4($pdf, "(1)", $y + 3);
   $y = Right5($pdf, "Perusahaan (sebagai agen Grup Investor) akan merampungkan pembelian Aset pada Tanggal Pembelian;", $y - 7);

   $y = Right4($pdf, "(2)", $y + 3);
   $y = Right5($pdf, "Perjanjian ini akan dibaca untuk menafsirkan Biaya Pembelian sebagai Biaya Pembelian yang Disesuaikan, dan semua ketentuan yang terkait lainnya dari Perjanjian ini dengan demikian akan ditafsirkan termasuk: (i) pengurangan proporsional terhadap Biaya Mata Uang Pembelian dan Harga Jual; dan (ii) kenaikan yang proporsional dalam persentase kepemilikan setiap Pemodal yang berkontribusi atas Aset, masing-masing sebagaimana ditetapkan dalam pemberitahuan yang diserahkan menurut Pasal 1.6.6(i) (the \"<b>Pemberitahuan Penyesuaian</b>&nbsp;\"); dan", $y - 7);

   $y = Right4($pdf, "(3)", $y + 3);
   $y = Right5($pdf, "tidak akan ada perubahan terhadap (i) jumlah bagian Pemodal yang berkontribusi atas Laba; (ii) Tanggal Pembelian; dan (iii) Tanggal Jatuh Tempo dan tanggal-tanggal lainnya yang ditetapkan dalam Perjanjian untuk pembayaran angsuran Harga Jual; dan", $y - 7);

   $y = Right3($pdf, "(b)", $y + 3);
   $y = Right4($pdf, "setelah penyerahan oleh Kapital Boost kepada Grup Investor atas Pemberitahuan Penyesuaian yang memenuhi persyaratan Perjanjian ini, setiap Pemodal yang berkontribusi dianggap telah menyepakati penyesuaian-penyesuaian tersebut dan Pemberitahuan Penyesuaian merupakan perubahan yang berlaku dan mengikat terhadap Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "1.6.7", $y + 5);
   $synp6 = $pdf->getPage();
   $y = Right3($pdf, "dalam keadaan yang dijelaskan dalam Pasal 1.6.6, Perusahaan akan melanjutkan membeli Aset, dalam jumlah yang berkurang untuk total imbalan yang setara dengan keseluruhan kontribusi dari Para Pemodal yang memberikan persetujuan (\"Biaya Pembelian yang Disesuaikan\"), dengan ketentuan bahwa jumlahnya lebih dari 50% dari Harga Pembelian.", $y - 7);

   $y = Right1($pdf, "1.7", $y + 5);
   $y = Right2($pdf, "Perusahaan, sebagai agen Grup Investor, berjanji untuk memeriksa Aset sebelum penyerahan/penagihan, dan akan memastikan bahwa semua spesifikasi sehubungan dengan Aset sebagaimana ditetapkan dalam Lampiran A akan terpenuhi. Perusahaan berjanji untuk segera memberikan ganti rugi kepada Grup Investor untuk kerugian atau kewajiban apa pun yang diakibatkan secara langsung atau tidak langsung dari kelalaian Perusahaan untuk melakukan pemeriksaan, dan memastikan kelayakan untuk tujuan atau pemenuhan spesifikasi atas Aset.", $y - 7);

   $y = Right1($pdf, "1.8", $y + 5);
   $y = Right2($pdf, "Setelah pembelian Aset oleh Perusahaan, sebagai agen Grup Investor, Perusahaan akan melakukan penawaran untuk membeli Aset (\"<b>Penawaran Perusahaan untuk Pembelian</b>&nbsp;&nbsp;\") dari Grup Investor, untuk imbalan yang setara dengan Harga Jual. Penjualan Aset oleh Grup Investor kepada Perusahaan akan berlaku setelah penerimaan atas Penawaran Perusahaan untuk Pembelian oleh Kapital Boost, yang bertindak sebagai agen Grup Investor, dalam bentuk Lampiran B. Harga Jual akan dibayarkan oleh Perusahaan kepada Grup Investor dengan ketentuan pembayaran tunda sebagaimana diatur dalam Pasal 2 di bawah ini. Para Pihak dengan ini sepakat bahwa hak milik atas Aset akan diteruskan dengan segera dan secara penuh dari Grup Investor kepada Perusahaan setelah penerimaan atas Penawaran Perusahaan untuk Pembelian dalam bentuk Lampiran B.", $y - 7);


   $y = Right1($pdf, "<b>2. PEMBAYARAN HARGA JUAL</b>", $y + 15);
   $synp1 = $pdf->getPage();
   $y = Right1($pdf, "2.1", $y + 10);
   $y = Right2($pdf, "Harga Jual akan dibayarkan oleh Perusahaan sebagai berikut:- ", $y - 7);

   $payoutStringR = "";
   for ($i = 0; $i < count($payoutDateId); $i++) {
      $payoutStringR .= "<tr><td style=\"text-align:center;height:18px\">$payoutDateId[$i]</td><td style=\"text-align:center;height:18px\">$payoutAmount[$i]</td></tr>";
   }

   $payoutTblR = "<table cellspacing=\"0\" cellpadding=\"3\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Tanggal</b></td>
  <td style=\"text-align:center\"><b>Harga Jual yang wajib dibayarkan</b></td>
  </tr>
  $payoutStringR
  </table>
  ";
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetX(110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutTblR);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Right1($pdf, "2.2", $y - 5);
   $y = Right2($pdf, "Seluruh pembayaran berdasarkan Perjanjian ini oleh Pemodal dan Perusahaan akan dilakukan secara penuh dalam mata uang Harga Jual yang disebutkan dalam Perjanjian ini, tanpa pengurangan apa pun untuk, dan bebas dari, pajak, pungutan, bea, biaya, imbalan jasa, pengurangan saat ini atau yang akan datang, atau persyaratan dalam jenis apa pun yang dibebankan atau dikenakan oleh otoritas perpajakan.", $y - 7);

   $y = Right1($pdf, "2.3", $y + 5);
   $y = Right2($pdf, "Seluruh pembayaran Harga Jual atau angsurannya akan dilakukan kepada setiap pemodal dalam Grup Investor secara pari passu, berdasarkan kontribusi persentase terhadap Harga Pembelian sebagaimana dijelaskan di atas. Para Pihak sepakat bahwa dalam hal pembayaran tersebut tidak dilakukan secara pari passu, Para Pihak akan melakukan pembayaran-pembayaran yang dilakukan sebagaimana antara Grup Investor untuk memperbaiki kelebihan/kekurangan pembayaran Harga Jual.", $y - 7);

   $y = Right1($pdf, "<b>3. KAPITAL BOOST SEBAGAI AGEN</b>", $y + 15);
   $synp7 = $pdf->getPage();
   $y = Right1($pdf, "3.1", $y + 10);
   $y = Right2($pdf, "Pemodal mengakui bahwa Perusahaan telah menunjuk, atau dengan ini dianggap menunjuk, Kapital Boost sebagai agen Pemodal untuk tujuan (a) menagih Biaya Pembelian dari Grup Investor, untuk pembayaran kepada Pemasok melalui Perusahaan; (b) memberitahukan Grup Investor atas pilihan yang dibuat oleh Perusahaan menurut Pasal 1.6.5; dan (c) mendistribusikan kepada Grup Investor uang yang mencakup Harga Jual atau angsuran-angsurannya yang jatuh tempo dan wajib dibayarkan oleh Perusahaan. ", $y - 7);

   $y = Right1($pdf, "3.2", $y + 5);
   $y = Right2($pdf, "Grup Investor selanjutnya dengan ini (dengan penyerahan Perjanjian ini secara elektronik kepada Kapital Boost) menunjuk Kapital Boost untuk menerima dan menandatangani Kontrak Penjualan Murabahah sesuai Lampiran B Perjanjian ini. ", $y - 7);

   $y = Right1($pdf, "3.3", $y + 5);
   $y = Right2($pdf, "Untuk tujuan keagenan berdasarkan Pasal 3.1 dan Pasal 3.2:", $y - 7);

   $y = Right1($pdf, "3.3.1", $y + 3);
   $y = Right3($pdf, "Kapital Boost akan (a) mengalihkan kepada Perusahaan pada selambat-lambatnya tanggal Kontribusi Biaya Pembelian atas Biaya Pembelian yang diterimanya dari Grup Investor; dan (b) setelah diterimanya jumlah Harga Jual yang jatuh tempo dari Perusahaan kepada setiap anggota Grup Investor pada tanggal tertentu, segera mendistribusikan kepada setiap pemodal dalam Grup Investor proporsinya atas jumlah yang diterima, dalam setiap hal, tanpa pengurangan apa pun untuk, dan bebas dari, pajak, pungutan, bea, biaya, imbalan jasa, pengurangan saat ini atau yang akan datang, atau persyaratan dalam jenis apa pun yang dibebankan atau dikenakan oleh otoritas perpajakan; ", $y - 7);

   $y = Right1($pdf, "3.3.2", $y + 3);
   $y = Right3($pdf, "Kapital Boost tidak akan bertanggung jawab atau berkewajiban atas wanprestasi atau kekurangan pembayaran oleh Perusahaan atas seluruh atau sebagian Harga Jual yang jatuh tempo kepada Grup Investor pada setiap waktu; ", $y - 7);

   $y = Right1($pdf, "3.3.3", $y + 3);
   $y = Right3($pdf, "Kapital Boost akan memastikan bahwa ketentuan berikut ini dimasukkan dalam Pemberitahuan Penyesuaian:", $y - 7);

   $y = Right3($pdf, "(a)", $y);
   $y = Right4($pdf, "Harga Pembelian yang Disesuaikan; ", $y - 7);

   $y = Right3($pdf, "(b)", $y);
   $y = Right4($pdf, "Biaya Mata Uang Pembelian dan Harga Jual yang dikurangi secara proporsional;", $y - 7);

   $y = Right3($pdf, "(c)", $y);
   $y = Right4($pdf, "(a)	persentase kepemilikan Pemodal yang meningkat secara proporsional atas Aset; dan", $y - 7);

   $y = Right3($pdf, "(d)", $y);
   $y = Right4($pdf, "penegasan yang jelas bahwa tidak ada perubahan terhadap (i) jumlah bagian Pemodal atas Laba; (ii) Tanggal Pembelian; dan (iii) Tanggal Jatuh Tempo dan tanggal-tanggal lainnya yang ditetapkan dalam Perjanjian untuk pembayaran angsuran Harga Jual.", $y - 7);

   $y = Right1($pdf, "3.3.4", $y + 5);
   $y = Right3($pdf, "sebelum penandatanganan Kontrak Penjualan Murabahah sebagai agen Grup Investor, Kapital Boost akan mengecek ketentuan Perjanjian ini untuk memastikan pemenuhan atas ketentuan Perjanjian ini;", $y - 7);

   $y = Right1($pdf, "3.3.5", $y + 5);
   $y = Right3($pdf, "Kapital Boost akan menginformasikan Grup Investor dengan segera setelah mengetahui keterlambatan Perusahaan menyerahkan Kontrak Penjualan Murabahah atas kekurangan dalam Kontrak Penjualan Murabahah atau alasan lain yang akan mengakibatkan Kapital Boost tidak dapat untuk, atau memprioritaskan untuk tidak; menandatangani Kontrak Penjualan Murabahah; dan", $y - 7);

   $y = Right1($pdf, "3.3.6", $y + 5);
   $y = Right3($pdf, "dengan mengambil setiap tindakan yang dijelaskan dalam Pasal 3.3 ini sebagai agen Grup Investor, Kapital Boost dianggap telah menyetujui penunjukannya sebagai agen tersebut, dan telah sepakat untuk terikat oleh ketentuan Pasal 3.3. ini. ", $y - 7);

   if ($y > 200) {
      $pdf->AddPage();
      $y = -5;
   }
   $y = Right1($pdf, "<b>4. PENGALIHAN KEPEMILIKAN & JAMINAN</b>", $y + 15);
   $synp2 = $pdf->getPage();

   $y = Right1($pdf, "4.1", $y + 10);
   $y = Right2($pdf, "Perusahaan sepakat untuk membeli Aset sebagaimana adanya, di manapun tempatnya, dan dianggap telah memeriksa Aset sebelum pembelian Aset tersebut. Hak milik dan kepemilikan Aset bersama-sama dengan seluruh hak, kepentingan dan manfaat yang melekat pada Aset tersebut akan diteruskan dari Grup Investor kepada Perusahaan dengan segera setelah penjualan Aset oleh Grup Investor kepada Perusahaan menurut ketentuan Perjanjian ini, meskipun Harga Jual belum dibayar atau dilunasi oleh Perusahaan kepada Grup Investor. Perusahaan mengesampingkan seluruh klaim terhadap Grup Investor sebagai penjual atas pelanggaran jaminan atau cacat sehubungan dengan Aset. Perusahaan akan segera pada tempat pembelian, menagih dan mengambil alih kepemilikan Aset dari Grup Investor atau agen (-agennya), jika ada.", $y - 7);

   $y = Right1($pdf, "4.2", $y + 5);
   $y = Right2($pdf, "Grup Investor mengalihkan kepada Perusahaan jaminannya dan hak-hak lain terhadap pemasok/penjual/penjual Aset pada tempat pembelian oleh Perusahaan. ", $y - 7);

   $y = Right1($pdf, "<b>5. PEMBAYARAN AWAL ATAS HARGA JUAL</b>", $y + 15);

   $y = Right2($pdf, "Perusahaan dapat memilih dengan diskresinya untuk memberlakukan pembayaran awal atas seluruh atau sebagian Harga Jual. Setiap pembayaran awal atas Harga Jual akan mengurangi kewajiban-kewajiban pembayaran Harga Jual dari Perusahaan dalam urutan kronologi terbalik, dan setiap pembayaran awal tersebut atas sebagian Harga Jual tidak akan menunda, mengurangi atau mempengaruhi sisa kewajiban pembayaran Perusahaan berkenaan dengan Harga Jual. Setelah pembayaran awal atas setiap bagian dari Harga Jual, Grup Investor atau Investor perseorangan dengan diskresi mereka atau diskresinya dapat memberikan ibra’ (rabat) atas sisa bagian (-bagian) Harga Jual atau, sesuai keadaan, bagian Investor tersebut dari Harga Jual. Kedua belah pihak mengakui bahwa pembebanan dan/atau penuntutan biaya pelunasan awal tidak sesuai dengan hukum Syariah.", $y + 10);

   $y = Right1($pdf, "<b>6. PEMBATALAN DENGAN SENDIRINYA</b>", $y + 15);
   $synp10 = $pdf->getPage();

   $y = Right1($pdf, "Apabila:", $y + 5);

   $y = Right1($pdf, "6.1", $y + 5);
   $y = Right2($pdf, "Perusahaan, sebagai agen Grup Investor, lalai dengan alasan apa pun untuk merampungkan pembelian Aset pada Tanggal Pembelian; atau", $y - 7);

   $y = Right1($pdf, "6.2", $y + 5);
   $y = Right2($pdf, "Perusahaan, sebagai agen Grup Investor, tidak menerima jumlah penuh atas Biaya Pembelian dari Grup Investor, pada selambat-lambatnya Tanggal Kontribusi Biaya Pembelian (kecuali jika Perusahaan memilih, menurut Pasal Pasal 1.6.6, untuk merampungkan pembelian Aset berdasarkan Biaya Pembelian yang Disesuaikan, dan selanjutnya merampungkan pembelian Aset tersebut pada Tanggal Pembelian);", $y - 7);

   $y = Right1($pdf, "6.3", $y + 5);
   $y = Right2($pdf, "sebelum perampungan pembelian Aset oleh Perusahaan, sebagai agen Grup Investor, terjadi suatu Peristiwa Wanprestasi atau hal tersebut menjadi melanggar hukum dalam yurisdiksi yang berlaku atau berdasarkan peraturan atau perundangan yang berlaku bagi Grup Investor atau Perusahaan, sebagai agen Grup Investor, untuk membeli Aset,", $y - 7);

   $y = Right2($pdf, "Para Pihak sepakat bahwa transaksi Murabahah yang dijelaskan dalam Perjanjian ini akan segera dan dengan sendirinya dibatalkan, dan Perusahaan akan segera mengembalikan kepada Grup Investor seluruh uang yang dipinjamkan atau dibayarkan kepada Perusahaan untuk pembelian Aset atas nama Grup Investor, dan selain itu akan memberikan ganti rugi kepada setiap Pemodal berkenaan dengan semua biaya dan pengeluaran sesungguhnya yang dikeluarkan oleh setiap Pemodal sehubungan dengan pengadaan Perjanjian ini dan peminjaman dana kepada Perusahaan.", $y + 5);

   $y = Right1($pdf, "<b>7. PERISTIWA WANPRESTASI</b>", $y + 15);
   $synp3 = $pdf->getPage();

   $y = Right1($pdf, "7.1", $y + 10);
   $y = Right2($pdf, "Peristiwa Wanprestasi dianggap telah terjadi dalam setiap keadaan berikut, baik ditimbulkan karena kelalaian Perusahaan atau tidak:-", $y - 7);

   $y = Right1($pdf, "7.1.1", $y + 3);
   $y = Right3($pdf, "Perusahaan lalai memenuhi setiap kewajiban pembayarannya berkenaan dengan Harga Jual, kecuali jika kelalaian tersebut adalah akibat kesalahan teknis atau administratif di pihak bank atau lembaga keuangan yang mengirimkan pembayaran oleh Perusahaan kepada Kapital Boost, atau oleh Kapital Boost kepada Pemodal; atau", $y - 7);

   $y = Right1($pdf, "7.1.2", $y + 3);
   $y = Right3($pdf, "Perusahaan lalai mematuhi dan melaksanakan setiap kewajibannya berdasarkan Perjanjian ini (termasuk, tidak terbatas (a) kewajibannya untuk menawarkan membeli Aset dari Grup Investor, dan (b) kewajiban-kewajibannya sebagai agen Grup Investor untuk membeli Aset dengan Biaya Pembelian dan menerima Penawaran Pembelian)); atau", $y - 7);

   $y = Right1($pdf, "7.1.3", $y + 3);
   $synp12 = $pdf->getPage();
   $y = Right3($pdf, "Pernyataan, keterangan atau jaminan yang diberikan atau dibuat oleh Perusahaan dalam Perjanjian ini terbukti tidak benar, tidak tepat atau tidak akurat atau menyesatkan dalam setiap hal materiil; atau", $y - 7);

   $y = Right1($pdf, "7.1.4", $y + 3);
   $y = Right3($pdf, "Diambil langkah apa pun untuk pembubaran, likuidasi atau penutupan Perusahaan;", $y - 7);

   $y = Right1($pdf, "7.1.5", $y + 3);
   $y = Right3($pdf, "Apabila terdapat unsur kecurangan, penyalahgunaan atau penggelapan Biaya Pembelian oleh Perusahaan.", $y - 7);

   $y = Right1($pdf, "7.2", $y + 5);
   $y = Right2($pdf, "Pada waktu terjadinya Peristiwa Wanprestasi, Pemodal berhak dengan pemberitahuan kepada Perusahaan untuk:", $y - 7);

   $y = Right1($pdf, "7.2.1", $y + 3);
   $y = Right3($pdf, "menyatakan seluruh proporsi Harga Jual yang belum dibayarkan dari Perusahaan kepada Pemodal menjadi segera jatuh tempo dan wajib dibayarkan; dan/atau", $y - 7);

   $y = Right1($pdf, "7.2.2", $y + 3);
   $y = Right3($pdf, "mensyaratkan Perusahaan untuk (a) menjual Aset dan menggunakan seluruh hasil keuntungan penjualan untuk membayar tunggakan Harga Jual; dan (b) tidak melepaskan atau mengalihkan kepemilikan Aset kepada orang lain; dan/atau", $y - 7);

   $y = Right1($pdf, "7.2.3", $y + 3);
   $y = Right3($pdf, "mensyaratkan Perusahaan untuk memberikan ganti rugi kepada Grup Investor atas biaya, kerugian atau pengeluaran apa pun yang dikeluarkan sebagai akibat terjadinya Peristiwa Wanprestasi termasuk, tidak terbatas (a) kerugian laba oleh Pemodal yang timbul dari (i) kelalaian oleh Perusahaan untuk memenuhi janjinya memberikan penawaran untuk membeli Aset dari Grup Investor, atau (ii) kelalaian oleh Perusahaan sebagai agen Grup Investor untuk membeli Aset dengan Biaya Pembelian atau menerima Penawaran Pembelian, (b) biaya apa pun yang dikeluarkan dalam penguasaan secara fisik atas Aset setelah Peristiwa Wanprestasi, dan (c) pengurangan nilai Aset yang timbul dari pelanggaran oleh Perusahaan atas ketentuan Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "7.3", $y + 5);
   $y = Right2($pdf, "Hak Pemodal untuk mempercepat kewajiban-kewajiban Perusahaan sehubungan dengan porsi Harga Jual yang jatuh tempo kepada Pemodal dapat digunakan terlepas dari, dan tanpa bersyarat atas, tindakan atau tidak adanya tindakan dari pemodal yang lain atau yang sebagian besar dari Grup Investor. Pemodal berhak untuk mengajukan atau melaksanakan tindakan hukum atau tuntutan terhadap Perusahaan untuk memperoleh jumlah yang belum dibayarkan, dan Perusahaan sepakat untuk segera berdasarkan permintaan memberikan ganti rugi kepada Pemodal atas seluruh biaya, kerugian dan kewajiban sesungguhnya (termasuk biaya hukum) yang dikeluarkan atau ditanggung oleh Pemodal sebagai akibat Peristiwa Wanprestasi.", $y - 7);

   $y = Right1($pdf, "<b>8. PERNYATAAN</b>", $y + 15);
   $synp11 = $pdf->getPage();

   $y = Right1($pdf, "8.1", $y + 10);
   $y = Right2($pdf, "Perusahaan menyatakan kepada Pemodal pada tanggal Perjanjian ini bahwa:", $y - 7);

   $y = Right1($pdf, "8.1.1", $y + 3);
   $y = Right3($pdf, "Perusahaan resmi didirikan dan tunduk berdasarkan hukum negara Republik Indonesia;", $y - 7);

   $y = Right1($pdf, "8.1.2", $y + 3);
   $y = Right3($pdf, "Perusahaan memiliki kewenangan untuk menandatangani Perjanjian ini, dan untuk melaksanakan kewajiban-kewajibannya berdasarkan Perjanjian ini dan telah mengambil semua tindakan yang diperlukan untuk mengesahkan penandatanganan dan pelaksanaan terseb;", $y - 7);

   $y = Right1($pdf, "8.1.3", $y + 3);
   $y = Right3($pdf, "Penandatanganan dan pelaksanaan Perjanjian ini tidak melanggar atau bertentangan dengan peraturan atau perundangan yang berlaku, ketentuan dokumen pendirian Perusahaan, dan perintah atau putusan pengadilan atau badan pemerintah yang lain yang berlaku bagi Perusahaan, atau pembatasan bersifat kontrak yang mengikat atas Perusahaan;", $y - 7);

   $y = Right1($pdf, "8.1.4", $y + 3);
   $y = Right3($pdf, "Seluruh izin yang disyaratkan untuk diperoleh Perusahaan sehubungan dengan Perjanjian ini telah diperoleh dan berlaku penuh;", $y - 7);

   $y = Right1($pdf, "8.1.5", $y + 3);
   $y = Right3($pdf, "Kewajiban-kewajiban Perusahaan berdasarkan Perjanjian ini merupakan kewajiban-kewajiban yang legal, sah dan mengikat, yang dapat diberlakukan menurut ketentuannya masing-masing;", $y - 7);

   $y = Right1($pdf, "8.1.6", $y + 3);
   $y = Right3($pdf, "Tidak ada Peristiwa Wanprestasi yang telah terjadi atau akan sewajarnya diperkirakan diakibatkan pengadaan atau pelaksanaan Perjanjian ini; dan", $y - 7);

   $y = Right1($pdf, "8.1.7", $y + 3);
   $y = Right3($pdf, "Usaha inti atau utama Perusahaan dan tujuan pembiayaan berdasarkan Perjanjian ini adalah dan pada setiap saat tetap sesuai Syariah.", $y - 7);

   $y = Right1($pdf, "8.2", $y + 5);
   $y = Right2($pdf, "Pernyataan dan jaminan dalam Pasal 6 ini diulangi oleh Perusahaan pada Tanggal Pembelian dan pada setiap tanggal yang dijadwalkan untuk pembayaran seluruh atau sebagian Harga Jual.", $y - 7);

   $y = Right1($pdf, "<b>9. BIAYA KETERLAMBATAN PEMBAYARAN (TA’WIDH DAN GHARAMAH)</b>", $y + 15);

   $y = Right1($pdf, "9.1", $y + 10);
   $y = Right2($pdf, "Apabila jumlah yang jatuh tempo dan wajib dibayarkan oleh Perusahaan berdasarkan atau sehubungan dengan Perjanjian ini belum dilunasi menurut Perjanjian ini (“jumlah yang belum dibayarkan”), Perusahaan berjanji untuk membayar biaya keterlambatan pembayaran termasuk ta’widh dan gharamah (dihitung di bawah ini) kepada Grup Investor pada setiap hari belum dibayarkannya jumlah yang belum dibayarkan.", $y - 7);

   $y = Right1($pdf, "9.2", $y + 5);
   $y = Right2($pdf, "Keseluruhan biaya keterlambatan pembayaran akan dipatok langsung pada laba rata-rata yang disetahunkan dari Perjanjian Murabahah ini (akan ditentukan oleh Perusahaan), yang dihitung setiap hari sebagaimana di bawah ini, dengan ketentuan bahwa akumulasi biaya keterlambatan pembayaran tidak melebihi 100% dari jumlah tunggakan yang belum dibayarkan:", $y - 7);

   $y = CenterR($pdf, "Jumlah yang Belum Dibayarkan X laba rata-rata yang disetahunkan X", $y + 5);
   $y = CenterR($pdf, "Jumlah hari yang lewat jatuh tempo  / 365", $y);

   $y = Right1($pdf, "9.3", $y + 5);
   $y = Right2($pdf, "Masing-masing Pemodal dapat menahan hanya bagian tertentu dari jumlah keterlambatan pembayaran yang dibayarkan kepadanya, sebagaimana diperlukan untuk memberikan penggantian kepada Pemodal atas setiap biaya sesungguhnya (tidak termasuk peluang atau biaya pendanaan), dengan tunduk pada dan menurut pedoman para penasihat Syariahnya sendiri, dan hingga jumlah keseluruhan maksimum 1% per tahun dari jumlah yang belum dibayarkan (“ta’widh”). ", $y - 7);

   $y = Right1($pdf, "9.4", $y + 5);
   $y = Right2($pdf, "Ta’widh sehubungan dengan jumlah yang belum dibayarkan, merupakan bagian dari biaya keterlambatan pembayaran, dapat timbul setiap hari dan dihitung menurut rumus berikut:-", $y - 7);

   $y = CenterR($pdf, "Biaya sesungguhnya yang dikeluarkan atau Jumlah yang Belum Dibayarkan X 1% X Jumlah hari yang lewat jatuh tempo / 365", $y + 3);

   $y = Right1($pdf, "9.5", $y + 5);
   $y = Right2($pdf, "Jumlah ta’widh tidak akan dilipatgandakan.", $y - 7);

   $y = Right1($pdf, "9.6", $y + 5);
   $y = Right2($pdf, "Grup Investor dengan ini menyepakati, berjanji dan menyanggupi untuk memastikan bahwa sisa biaya keterlambatan (“gharamah”), jika ada, akan dibayarkan kepada badan-badan amal yang dapat dipilih oleh Perusahaan dengan berkonsultasi dengan para penasihat Syariahnya.", $y - 7);

   $y = Right1($pdf, "<b>10. HUKUM YANG BERLAKU DAN PENYELESAIAN SENGKETA</b>", $y + 15);
   $synp4 = $pdf->getPage();

   $y = Right1($pdf, "10.1", $y + 10);
   $y = Right2($pdf, "Perjanjian ini dan hak-hak serta kewajiban Para Pihak berdasarkan Perjanjian ini diatur oleh dan diinterpretasikan serta ditafsirkan dalam semua hal menurut hukum negara Singapura tanpa mengurangi atau membatasi hak-hak atau upaya hukum lainnya yang tersedia bagi Para Pihak dalam Perjanjian ini berdasarkan hukum yurisdiksi di mana Perusahaan atau aset-asetnya mungkin ditempatkan.", $y - 7);

   $y = Right1($pdf, "10.2", $y + 5);
   $y = Right2($pdf, "Para Pihak dengan ini dengan tidak dapat dicabut kembali sepakat untuk tunduk pada yurisdiksi eksklusif Pengadilan Singapura untuk menyelesaikan sengketa atau mengajukan tuntutan berkenaan dengan Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "<b>11. LAIN-LAIN</b>", $y + 15);
   $synp5 = $pdf->getPage();

   $y = Right1($pdf, "11.1", $y + 10);
   $y = Right21($pdf, "Apabila pada setiap saat satu ketentuan atau lebih dari Perjanjian ini adalah atau menjadi melanggar hukum, tidak sah atau tidak dapat diberlakukan berdasarkan hukum Singapura maka legalitas, keabsahan atau keberlakuan ketentuan-ketentuan lainnya dari Perjanjian ini atau legalitas, keabsahan atau keberlakuan ketentuan-ketentuan tersebut berdasarkan hukum yurisdiksi yang lain dengan cara apa pun tidak terpengaruh atau terganggu karena pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut.", $y - 7);

   $y = Right1($pdf, "11.2", $y + 5);
   $synp13 = $pdf->getPage();
   $y = Right21($pdf, "Apabila setiap ketentuan Perjanjian ini (atau bagian dari ketentuan tersebut) atau pemberlakuannya terhadap orang manapun atau keadaan apa pun melanggar hukum, tidak sah atau tidak dapat diberlakukan dalam hal apa pun maka harus ditafsirkan secara sempit sebagaimana diperlukan untuk mengizinkannya dapat diberlakukan atau sah dan ketentuan yang lain dari Perjanjian ini dan legalitas, keabsahan atau keberlakuan ketentuan-ketentuan tersebut terhadap orang-orang atau keadaan lain tidak akan dengan cara apa pun terpengaruh atau terganggu karena pelanggaran hukum, ketidakabsahan atau ketidakberlakuan tersebut dan akan dapat diberlakukan/dilaksanakan sejauh mungkin yang diizinkan berdasarkan hukum.", $y - 7);

   $y = Right1($pdf, "11.3", $y + 5);
   $synp9 = $pdf->getPage();
   $y = Right21($pdf, "Seluruh hak dan kewajiban dalam Perjanjian ini bersifat pribadi terhadap Para Pihak dan masing-masing Pihak dalam Perjanjian ini tidak boleh mengalihkan dan/atau memindahtangankan setiap hak dan kewajiban tersebut kepada pihak ketiga tanpa izin tertulis terlebih dahulu dari Para Pihak.", $y - 7);

   $y = Right1($pdf, "11.4", $y + 5);
   $synp14 = $pdf->getPage();
   $y = Right21($pdf, "UU Kontrak (Hak-hak Para Pihak Ketiga) Tahun 2001 tidak berlaku pada Perjanjian ini dan tidak ada orang yang bukan merupakan salah satu pihak pada Perjanjian ini yang memiliki hak apa pun berdasarkan UU Kontrak (Hak-hak Para Pihak Ketiga) Tahun 2001 untuk melaksanakan ketentuan apa pun dari Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "11.5", $y + 5);
   $synp15 = $pdf->getPage();
   $y = Right21($pdf, "Perjanjian ini memuat keseluruhan kesepahaman antara Para Pihak terkait dengan transaksi yang dimaksud oleh Perjanjian ini dan akan menggantikan setiap pernyataan minat atau kesepahaman sehubungan dengan transaksi tersebut. Seluruh perjanjian, kesepahaman, pernyataan dan keterangan sebelumnya atau yang bersamaan, secara lisan dan tertulis, digabungkan dalam Perjanjian ini dan tidak lagi berkekuatan atau berlaku selanjutnya.", $y - 7);

   $y = Right1($pdf, "11.6", $y + 5);
   $synp16 = $pdf->getPage();
   $y = Right21($pdf, "Setiap komunikasi yang akan dilakukan berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan secara tertulis dan, dapat dilakukan dengan surat atau surat elektronik, dalam masing-masing hal, melalui Kapital Boost yang beralamat di <b>150 Changi Road, #04-07, Singapore 419973</b>&nbsp;&nbsp;&nbsp;&nbsp; atau di alamat tertentu lainnya yang dapat diberitahukan oleh Kapital Boost kepada para pihak dari waktu ke waktu. Setiap komunikasi yang dilakukan antara Kapital Boost dan setiap Pihak berdasarkan atau sehubungan dengan Perjanjian ini akan dilakukan ke alamat atau alamat surat elektronik yang diberikan kepada Kapital Boost atau alamat terdaftarnya, dalam hal Perusahaan, dan berlaku apabila diterima.", $y - 7);

   $y = Right1($pdf, "11.7", $y + 5);
   $synp17 = $pdf->getPage();
   $y = Right21($pdf, "Tidak ada kelalaian untuk melaksanakan, atau setiap keterlambatan dalam pelaksanaan, di pihak Pemodal, hak atau upaya hukum apa pun berdasarkan Perjanjian ini yang dianggap sebagai pengesampingan, demikian juga pelaksanaan tunggal atau sebagian atas hak atau upaya hukum apa pun tidak akan menghalangi pelaksanaan selanjutnya atau yang lain atau pelaksanaan hak atau upaya hukum yang lain. Hak-hak dan upaya hukum berdasarkan Perjanjian ini bersifat kumulatif dan tidak terpisah dari hak atau upaya hukum yang diatur oleh hukum.", $y - 7);

   $y = Right1($pdf, "11.8", $y + 5);
   $synp18 = $pdf->getPage();
   $y = Right21($pdf, "Tidak ada ketentuan dari Perjanjian ini yang dapat diubah, dikesampingkan, dilepaskan atau dihentikan secara lisan atau demikian juga tidak ada pelanggaran atau wanprestasi berdasarkan setiap ketentuan Perjanjian ini yang dapat dikesampingkan atau dilepaskan secara lisan tetapi (dalam masing-masing hal) kecuali (a) sesuai dengan Pemberitahuan Penyesuaian yang memenuhi persyaratan Perjanjian ini; atau (b) dengan instrumen tertulis yang ditandatangani oleh atau atas nama Para Pihak atau (b). Setiap amendemen atau perubahan pada Perjanjian ini harus sesuai Syariah.", $y - 7);

   $y = Right1($pdf, "11.9", $y + 5);
   $synp19 = $pdf->getPage();
   $y = Right21($pdf, "Perjanjian ini dapat ditandatangani dalam berapa pun jumlah rangkap, dan memiliki keberlakuan yang sama seolah-olah tanda tangan-tanda tangan pada rangkap tersebut terdapat pada salinan tunggal dari Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "11.10", $y + 5);
   $synp20 = $pdf->getPage();
   $y = Right21($pdf, "Masing-masing Pihak sepakat untuk merahasiakan seluruh informasi berkaitan dengan Perjanjian ini, dan tidak mengungkapkannya kepada siapa pun, kecuali dengan izin tertulis terlebih dahulu dari Para Pihak yang lain atau sebagaimana disyaratkan oleh peraturan atau perundangan yang berlaku.", $y - 7);

   $y = Right1($pdf, "11.11", $y + 5);
   $synp21 = $pdf->getPage();
   $y = Right21($pdf, "Perjanjian ini dimaksudkan untuk sesuai dengan Syariah. Para Pihak dengan ini menyepakati dan mengakui bahwa masing-masing hak dan kewajiban mereka berdasarkan Perjanjian ini dimaksudkan untuk, dan akan, sesuai dengan prinsip-prinsip Syariah.", $y - 7);

   $y = Right1($pdf, "11.12", $y + 5);
   $y = Right21($pdf, "Meskipun terdapat hal di atas, masing-masing pihak menyatakan kepada yang lain bahwa tidak akan mengajukan keberatan atau tuntutan terhadap yang lain berdasarkan kepatuhan pada Syariah atau pelanggaran prinsip-prinsip Syariah sehubungan atau yang terkait dengan bagian manapun dari setiap ketentuan Perjanjian ini.", $y - 7);

   $y = Right1($pdf, "<b>12. DEFINISI</b>", $y + 15);

   $y = Right1($pdf, "Dalam Perjanjian ini, rujukan pada:", $y + 10);

   $y = Right1($pdf, "12.1", $y + 5);
   $y = Right21($pdf, "Pihak manapun atau Kapital Boost akan ditafsirkan sehingga mencakup para penerusnya yang berhak, para penerima pengalihan resmi dan para penerima pemindahtanganan resminya;", $y - 7);

   $y = Right1($pdf, "12.2", $y + 5);
   $y = Right21($pdf, "ketentuan hukum adalah rujukan pada ketentuan tersebut sebagaimana diubah atau diberlakukan kembali;", $y - 7);

   $y = Right1($pdf, "12.3", $y + 5);
   $y = Right21($pdf, "orang atau Pihak dengan jenis kelamin tertentu mencakup orang atau Pihak tersebut terlepas dari apa pun jenis kelaminnya, dan akan berlaku sama apabila orang atau Pihak tersebut adalah badan perusahaan, firma, peramanatan (trust), usaha patungan atau persekutuan;", $y - 7);

   $y = Right1($pdf, "12.4", $y + 5);
   $y = Right21($pdf, "the \"<b>Grup Investor</b>&nbsp; \" ditafsirkan bermakna sekadar Pemodal jika Pemodal membiayai keseluruhan Biaya Pembelian;", $y - 7);

   $y = Right1($pdf, "12.5", $y + 5);
   $y = Right21($pdf, "\"<b>Kapital Boost</b>&nbsp; \" adalah <b>Kapital Boost Pte Ltd, No. Pendaftaran Perusahaan Singapura 201525866W</b>", $y - 7);

   $y = Right1($pdf, "12.6", $y + 5);
   $y = Right21($pdf, "\"<b>Harga Jual</b>&nbsp; \" adalah Harga Jual sebagaimana diubah menurut Pasal 1.6.6.", $y - 7);

   if ($y > 160) { // Shift the sign table below
      $pdf->AddPage();
      $y = 0;
   }
   $synp100 = $pdf->getPage();







   /* ---------- END of Bahasa Version -------------- */



   $pdf->SetPage(1);
   $y = 19;

   $y = CenterL($pdf, "DATED THIS DAY $date", $y);

   $pdf->SetFontSize("25");
   $y = CenterL($pdf, ". . . . . . . . . . . . . . . .", $y);

   $pdf->SetFontSize("14");
   $y = CenterL($pdf, "MURABAHAH AGREEMENT", $y, true);

   $pdf->SetFontSize("25");
   $y = CenterL($pdf, ". . . . . . . . . . . . . . . .", $y - 6);
   $pdf->SetFontSize("12");

   $y = Left1($pdf, "<b>THIS AGREEMENT</b> is made between the following parties: ", $y + 5);

   $y = Left1($pdf, "<b>1. $companyName</b>", $y + 5);

   $y = Left1($pdf, "($companyRegistration)", $y);

   $y = Left1($pdf, "(hereinafter referred to as \"<b>the Company</b>  \")", $y + 5);

   $y = Left1($pdf, "<b>AND</b>", $y + 5);

   $y = Left1($pdf, "<b>2. $invName</b><br> ($invIC)", $y + 5);

   $y = Left1($pdf, "(hereinafter referred to as \"<b>the Financier</b>  \")", $y + 5);

   $y = Left1($pdf, "<b>WHEREAS:</b>", $y + 5);

   $y = Left1($pdf, "A.", $y + 5);

   $y = Left2($pdf, "The Company is in need of funding for the purchase, acquisition or procurement of $purchaseAsset, as described in Schedule A (“<b>the Asset</b> ”), which it intends to utilize for its business, and the Company wishes to obtain such financing in accordance with Shariah principles;", $y - 7);

   $y = Left1($pdf, "B.", $y + 5);

   $y = Left2($pdf, "The Financier, together with certain other financiers (together, “<b>the Investors Group</b> ”) have agreed to provide the required financing to the Company based on the Shariah principle of Murabahah, specifically murabahah to the purchase orderer; and", $y - 7);

   $y = Left1($pdf, "C.", $y + 5);

   $y = Left2($pdf, "The Financier has agreed to provide his share of the required financing based on the terms and subject to the conditions of this Agreement.", $y - 7);

   $y = Left1($pdf, "<b>NOW IT IS HEREBY AGREED</b> as follows:", $y + 10);
   $y = Left1($pdf, "<b>1. METHOD OF FINANCING</b>", $y + 10);

   $y = Left1($pdf, "1.1", $y);

   $y = Left2($pdf, "For the purpose of financing the acquisition of the Asset, the Company hereby requests that the Investors Group collectively purchases the Asset from the Supplier for a total consideration of <b>$assetCost</b> (\"<b>the Purchase Cost</b>&nbsp;&nbsp;\"), and thereafter sell the Asset to the Company at the sale price of <b>$salePrice</b> (\"<b>the Sale Price</b> \"). The actual cost to the Company of purchasing the Asset is <b>$assetCostCurrency</b> (\"<b>Purchase Currency Cost </b>&nbsp; \"), which currency can be purchased using the Singapore Dollar amount of the Purchase Cost at the prevailing currency exchange rate of S$1:$exchange The Company undertakes (via wa’d) to purchase the Asset from the Investors Group, following the purchase of the Asset by the Investors Group in accordance with the terms of this Agreement. The Murabahah arrangement set out above will result in profit for the Investors Group as set out below.", $y - 7);


   $html = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td><b>Purchase Cost of the Asset payable by the Investors Group</b></td>
  <td><b>Sale Price payable by the Company to the Investors Group</b></td>
  <td><b>Amount of Profit derived by the Investors Group</b></td>
  </tr>
  <tr>
  <td>$assetCost</td>
  <td>$salePrice</td>
  <td>$profit (or $return% of the Purchase Cost of Asset)</td>
  </tr>

  </table>
  ";
   if ($y > 200) { // Shift the sign table below
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetFontSize(10);
   $pdf->writeHTML($html);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Left1($pdf, "1.2", $y);

   $y = Left2($pdf, "The parties agree that the Purchase Cost refers to the actual acquisition costs of the Asset incurred or to be incurred by the Investors Group or its agent for the acquisition of the Asset, excluding any direct expenses (such as delivery, transportation, storage and assembly costs, taxes, takaful and insurance coverage costs) and any overhead expenditures or indirect costs (such as staff wages and labor charges).", $y - 7);

   $y = Left1($pdf, "1.3", $y + 5);

   $y = Left2($pdf, "The Financier is willing to finance exactly $invAmount (being $invPercent%) out of the entire Purchase Cost amount. Such amount to be financed by the Financier will be paid by Financier to the Supplier, through the Company as agent. For the purposes of enabling Kapital Boost, as agent of the Company under Clause 3.3.1, to transfer the Purchase Cost amount to the Company by no later than $cCloseDate (the \"<b>Purchase Cost Contribution Date</b>&nbsp;&nbsp;&nbsp; \"), the Financier shall transfer the amount of its contribution to the Purchase Cost (as specified in this clause above) to the account specified in writing by Kapital Boost, by no later than <b>three (3) working days</b>&nbsp;&nbsp;&nbsp; from the date of this Agreement. ", $y - 7);

   $y = Left1($pdf, "1.4", $y + 5);

   $y = Left2($pdf, "Based on the Financier’s contribution to the Purchase Cost as specified above, the Financier will own $invPercent% of the Asset upon purchase of the same by the Investors Group, and the Financier will be entitled to $invPercent% share of the Sale Price and each installment or part thereof paid by the Company, amounting to:-", $y - 7);



   BulletL($pdf, $y);

   $y = Left3($pdf, "Financier’s portion of the Sale Price: $invSalePrice", $y + 3);

   BulletL($pdf, $y);

   $y = Left3($pdf, "Financier's portion of the Profit: $invProfit", $y + 3);

   $y = Left1($pdf, "1.5", $y + 5);



   $y = Left2($pdf, "The Financier (as part of the Investors Group) hereby appoints the Company as his agent to purchase the Asset on its behalf. The Company hereby accepts such appointment as agent, and will purchase and handle the Asset in accordance with the terms of this Agreement. The Company, as agent, shall purchase the Asset in the currency of the Purchase Currency Cost, for a consideration not exceeding the Purchase Currency Cost, and by no later than  <b>$cCloseDate1</b>&nbsp;(\"<b>Date of Purchase</b>&nbsp; \"). ", $y - 7);

   $y = Left1($pdf, "1.6", $y + 5);

// star here

   $y = Left2($pdf, "The Financier and the Company (as agent of the Investors Group for the purposes of the purchase of the Asset) hereby acknowledge and mutually agree that:", $y - 7);

   $y = Left1($pdf, "1.6.1", $y + 3);

   $y = Left3($pdf, "the Investors Group (including the Financier) has appointed the Company to act as agent of the Investors Group to purchase the Asset on behalf of the Investors Group because of the Company’s market knowledge and networks in relation to the Asset;", $y - 7);

   $y = Left1($pdf, "1.6.2", $y + 3);

   $y = Left3($pdf, "the purchase of the Asset by the Investors Group through the Company as agent may result in benefits to the Investors Group through efficiency of the purchase process and also cost savings in the event that the Company is able to secure or source the Asset at a price lower than the Purchase Currency Cost;", $y - 7);

   $y = Left1($pdf, "1.6.3", $y + 3);

   $y = Left3($pdf, "the event that the Company, as agent, is able to purchase the Asset at lower than the Purchase Currency Cost, the Investors Group hereby agrees to waive, by way of tanazul (i.e. waiver), its rights to claim the excess unutilized funding amount; and the Company, as agent, is entitled to retain the benefit of any such cost savings as incentive based on hibah for its good work performance;", $y - 7);

   $y = Left1($pdf, "1.6.4", $y + 3);

   $y = Left3($pdf, "in the circumstances described in Clause 1.6.3, the cost to the Investors Group of purchasing the Asset would be the Purchase Cost, as the hibah amount granted to the Company, as agent, would constitute part of the actual cost of purchasing the Asset, and accordingly the same Purchase Cost will be reflected in the Murabahah Sale Contract to be entered into by the Investors Group in the sale of the Asset;", $y - 7);

   $pdf->setPage($synp0);
   $y = 10;
   $y = Left1($pdf, "1.6.5", $y);

   $y = Left3($pdf, "in the event that the Company is unable to purchase sufficient $foreignCurrency to constitute the Purchase Currency Cost using the Purchase Cost contributed by the Investors Group as a result of an decrease in the currency exchange rate from that stated in Clause 1.1:", $y - 7);

   $y = Left3($pdf, "(a)", $y + 2);

   $y = Left4($pdf, "the Company, as agent of the Investors Group, shall either:", $y - 7);

   $y = Left4($pdf, "(i)", $y);

   $y = Left5($pdf, "promptly notify the Investors Group and return the Purchase Cost"
           . " to the Investors Group, following which this Agreement shall terminate; or", $y - 7);

   $y = Left4($pdf, "(ii)", $y);

   $y = Left5($pdf, "fund the shortfall amount in the currency of the Purchase Cost (the \"<b>Purchase Cost Shortfall Amount</b>&nbsp;&nbsp; \") which would be required in order for the Company to purchase the Purchase Currency Cost in order to complete the purchase of the Asset as agent on behalf of the Investors Group;", $y - 7);

   $y = Left3($pdf, "(b)", $y + 3);

   $y = Left4($pdf, "following the funding of the Purchase Cost Shortfall Amount by the Company and the completion of the purchase of the Asset by the Company, as agent of the Investors Group:", $y - 7);

   $y = Left4($pdf, "(i)", $y);
   $y = Left5($pdf, "the Purchase Cost Shortfall Amount shall constitute a debt owed by the Investors Group to the Company, in its capacity as agent for the Investors Group;", $y - 7);

   $y = Left4($pdf, "(ii)", $y);
   $y = Left5($pdf, "notwithstanding paragraph (i) above, the Company shall not, prior to the transfer of title to the Asset pursuant to Clause 1.8, have any claim to any part of portion of the title to the Assets;", $y - 7);

   $y = Left4($pdf, "(iii)", $y);
   $y = Left5($pdf, "the Sale Price (or, if there is more than one installment of the Sale Price, then the first installment thereof) shall be increased by the Purchase Cost Shortfall Amount, which will be reflected in the Company’s Offer to Purchase delivered by the Company to the Investors Group; and", $y - 7);

   $y = Left4($pdf, "(iv)", $y);
   $y = Left5($pdf, "the Investors Group, the Company (in its capacity as agent of the Investors Group) and as purchaser of the Asset, all agree that the Purchase Cost Shortfall Amount owing by the Company to the Investors Group (as part of the Sale Price) shall be automatically set off against the same amount owing by the Investors Group to the Company (pursuant to paragraph (i) above), and neither the Financier nor the Company shall make any claim with respect to payment of such amount by the other party.", $y - 7);

   $y = Left1($pdf, "1.6.6", $y + 5);
   $y = Left3($pdf, "In the event that on the Purchase Cost Contribution Date, the aggregate amount of the contributions from the Investors Group towards the Purchase Cost is lower than the quantum of the Purchase Cost specified in Clause 1.1, but higher than 50% of such quantum, Kapital Boost (as agent acting on behalf, and at the instruction, of the Company) shall promptly notify the Investors Group of the intention of the Company to (i) continue with the purchase of the Asset in reduced quantity for a total consideration equal to the aggregate amount of the contributions from the Investors Group (the \"<b>Adjusted Purchase Cost</b>&nbsp;&nbsp;\"); or otherwise (ii) return the contributions to the contributing members of the Investors Groups pursuant to Clause 6.2.; and", $y - 7);

   $y = Left3($pdf, "(a)", $y + 2);
   $y = Left4($pdf, "following the Company’s election pursuant to Clause 1.6.6 (i):", $y - 7);

   $y = Left4($pdf, "(1)", $y + 3);
   $y = Left5($pdf, "the Company (as agent of Investors Group) will complete the purchase of the Asset by the Date of Purchase;", $y - 7);

   $y = Left4($pdf, "(2)", $y + 3);
   $y = Left5($pdf, "this Agreement shall be read to interpret the Purchase Cost as the Adjusted Purchase Cost, and all other relevant provisions of this Agreement shall be adjusted accordingly including: (i) a proportional reduction to the Purchase Currency Cost and the Sale Price; and (ii) a proportional increase in the percentage of each contributing Financier’s ownership in the Asset, each as specified in the notice delivered pursuant to Clause 1.6.6(i) (the \"<b>Adjustment Notice</b>&nbsp;\"); and", $y - 7);

   $y = Left4($pdf, "(3)", $y + 3);
   $y = Left5($pdf, "there will not be any change to (i) the quantum of each contributing Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price; and", $y - 7);

   $y = Left3($pdf, "(b)", $y + 3);
   $y = Left4($pdf, "upon delivery by Kapital Boost to the Investors Group of an Adjustment Notice which complies with the requirements of this Agreement, each of the contributing Financiers shall be deemed to have agreed to such adjustments and the Adjustment Notice shall constitute an effective and binding amendment to this Agreement.", $y - 7);

   $pdf->setPage($synp6);
   $y = 10;
   $y = Left1($pdf, "1.6.7", $y);
   $y = Left3($pdf, "the circumstance described in 1.6.6, the Company will proceed to purchase the Asset, in reduced quantity for a total consideration equal to the aggregate contribution from consenting Financiers (\"Adjusted Purchase Cost\"), provided that the amount is more than 50% of the Purchase Cost.", $y - 7);

   $y = Left1($pdf, "1.7", $y + 5);
   $y = Left2($pdf, "The Company, as agent of the Investors Group, undertakes to inspect the Asset prior to delivery / collection, and shall ensure that all specifications in relation to the Asset as specified in Schedule A shall be complied with. The Company undertakes to immediately indemnify the Investors Group for any losses or liabilities resulting directly or indirectly from the failure of the Company to so inspect, and ensure the fitness for purpose or compliance with specifications of, the Asset.", $y - 7);

   $y = Left1($pdf, "1.8", $y + 5);
   $y = Left2($pdf, "Following the purchase of the Asset by the Company, as agent of the Investors Group, the Company will offer to purchase the Asset (\"<b>Company's Offer to Purchase</b>&nbsp;&nbsp;\") from the Investors Group, for a consideration equal to the Sale Price. The sale of the Asset by the Investors Group to the Company will take effect upon the acceptance of the Company’s Offer to Purchase by Kapital Boost, acting as agent of the Investors Group, in the form of Schedule B. The Sale Price will be paid by the Company to the Investors Group on deferred payment terms as provided in Clause 2 below. The Parties hereby agree that title to the Asset will pass immediately and fully from the Investors Group to the Company upon acceptance of the Company’s Offer to Purchase, in the form of Schedule B.", $y - 7);


   $y = Left1($pdf, "<b>2. PAYMENT OF SALE PRICE</b>", $y + 15);

   $y = Left1($pdf, "2.1", $y + 10);
   $y = Left2($pdf, "The Sale Price will be paid by the Company as follows:-", $y - 7);

   $payoutString = "";
   for ($i = 0; $i < count($payoutDate); $i++) {
      $payoutString .= "<tr><td style=\"text-align:center;height:18px\">$payoutDate[$i]</td><td style=\"text-align:center;height:18px\">$payoutAmount[$i]</td></tr>";
   }

   $payoutTbl = "<table cellspacing=\"0\" cellpadding=\"3\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b></td>
  <td style=\"text-align:center\"><b>Sale Price payable</b></td>
  </tr>
  $payoutString
  </table>
  ";
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutTbl);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();
   $y = Left1($pdf, "2.2", $y - 5);
   $y = Left2($pdf, "All payments hereunder by the Financier and the Company shall be made in full in the currency of the Sale Price stated in this Agreement, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority.", $y - 7);

   $y = Left1($pdf, "2.3", $y + 5);
   $y = Left2($pdf, "All payments of the Sale Price or any installments thereof shall be made to each financier within the Investors Group pari passu, based on the percentage contribution to the Purchase Price as described above. The Parties agree that in the event any such payment is not made pari passu, the Parties shall make the necessary payments as between the Investors Group to rectify any over/under payment of the Sale Price.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp7) {
      $pdf->setPage($synp7);
      $y = -5;
   }
   $y = Left1($pdf, "<b>3. KAPITAL BOOST AS AGENT</b>", $y + 15);

   $y = Left1($pdf, "3.1", $y + 10);
   $y = Left2($pdf, "The Financier acknowledges that the Company has appointed, or is hereby deemed to appoint, Kapital Boost as its agent for the purposes of (a) collecting the Purchase Cost from the Investors Group, for payment to the Supplier through the Company; (b) notifying the Investors Group of the election made by the Company pursuant to Clause 1.6.5; and (c) distributing to the Investors Group monies comprising the Sale Price or installments thereof due and payable by the Company.", $y - 7);

   $y = Left1($pdf, "3.2", $y + 5);
   $y = Left2($pdf, "The Investors Group furthermore hereby (by the electronic delivery of this Agreement to Kapital Boost) appoints Kapital Boost to accept and sign the Murabahah Sale Contract per Schedule B of this Agreement.", $y - 7);

   $y = Left1($pdf, "3.3", $y + 5);
   $y = Left2($pdf, "For the purposes of the agency under Clause 3.1 and 3.2:", $y - 7);

   $y = Left1($pdf, "3.3.1", $y + 3);
   $y = Left3($pdf, "Kapital Boost shall (a) transfer to the Company by no later the Purchase Cost Contribution Date, the Purchase Cost received by it from the Investors Group; and (b) upon receipt of any amount of the Sale Price due from the Company to any member of the Investors Group on any particular date, promptly distribute to each financier within the Investors Group its proportion of the amount received, in each case, without any deduction whatsoever for, and free from, any present or future taxes, levies, duties, charges, fees, deductions, or conditions of any nature imposed or assessed by any taxing authority; ", $y - 7);

   $y = Left1($pdf, "3.3.2", $y + 3);
   $y = Left3($pdf, "Kapital Boost shall not be responsible or liable for any default or shortfall in the payment by the Company of all or any part of the Sale Price due to the Investors Group at any time;", $y - 7);

   $y = Left1($pdf, "3.3.3", $y + 3);
   $y = Left3($pdf, "Kapital Boost shall ensure that the following terms are included in the Adjustment Notice:", $y - 7);

   $y = Left3($pdf, "(a)", $y);
   $y = Left4($pdf, "the Adjusted Purchase Price; ", $y - 7);

   $y = Left3($pdf, "(b)", $y);
   $y = Left4($pdf, "the proportionally reduced Purchase Currency Cost and Sale Price;", $y - 7);

   $y = Left3($pdf, "(c)", $y);
   $y = Left4($pdf, "the proportionally increased percentage of the Financier’s ownership in the Asset; and", $y - 7);

   $y = Left3($pdf, "(d)", $y);
   $y = Left4($pdf, "express confirmation that there is no change to (i) the quantum of the Financier’s portion of the Profit; (ii) the Date of Purchase; and (iii) the Maturity Date and any other dates specified in the Agreement for payment of any installment of the Sale Price.", $y - 7);

   $y = Left1($pdf, "3.3.4", $y + 5);
   $y = Left3($pdf, "prior to signing the Murabahah Sale Contract as agent of the Investors Group, Kapital Boost shall check the terms of the same to ensure compliance with the terms of this Agreement;", $y - 7);

   $y = Left1($pdf, "3.3.5", $y + 5);
   $y = Left3($pdf, "Kapital Boost shall inform the Investors Group promptly upon becoming aware of any delay in the Company delivering the Murabahah Sale Contract, any deficiency in the Murabahah Sale Contract or any other reason which would result in Kapital Boost not being able to, or preferring not to, sign the Murabahah Sale Contract; and", $y - 7);

   $y = Left1($pdf, "3.3.6", $y + 5);
   $y = Left3($pdf, "by taking any of the actions described in this Clause 3.3 as agent of the Investors Group, Kapital Boost is deemed to have accepted its appointment as such agent, and to have agreed to be bound by the terms of this Clause 3.3.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp2) {
      $pdf->setPage($synp2);
      $y = -5;
   }
   $y = Left1($pdf, "<b>4. TRANSFER OF OWNERSHIP & WARRANTY</b>", $y + 15);

   $y = Left1($pdf, "4.1", $y + 10);
   $y = Left2($pdf, "The Company agrees to purchase the Asset on an as-is, where-is basis, and is deemed to have inspected the Asset prior to purchase thereof. The title and ownership of the Asset together with all rights, interest and benefits attached thereto shall pass from the Investors Group to the Company immediately upon the sale of the Asset by the Investors Group to the Company pursuant to the terms of this Agreement, notwithstanding that the Sale Price has not been paid or fully paid by the Company to the Investors Group. The Company waives all claims against the Investors Group as seller for breach in warranty or defects in respect of the Asset. The Company shall immediately at the point of purchase, collect and take possession of the Asset from the Investors Group or its agent(s), if any.", $y - 7);

   $y = Left1($pdf, "4.2", $y + 5);
   $y = Left2($pdf, "The Investors Group assigns to the Company its warranty and other rights against the supplier / seller of the Assets at the point of purchase by the Company.", $y - 7);

   $y = Left1($pdf, "<b>5. EARLY PAYMENT OF SALE PRICE</b>", $y + 15);

   $y = Left2($pdf, "The Company may opt at its discretion to effect early payment of all or any part of the Sale Price. Any early payment of part of the Sale Price shall reduce the Sale Price payment obligations of the Company in inverse chronological order, and any such early payment of any part of the Sale Price shall not delay, reduce or otherwise affect the remaining payment obligations of the Company in respect of the Sale Price. Following any early payment of any part of the Sale Price, the Investors Group or any individual Investor may at their or his absolute discretion grant ibra’ (rebate) on any remaining part(s) of the Sale Price or, as the case may be, that Investor’s share of the Sale Price. Both parties acknowledge that charging and/or claiming early settlement charges is not in accordance with Shariah law.", $y + 10);

   $pg = $pdf->getPage();
   if ($pg < $synp10) {
      $pdf->setPage($synp10);
      $y = -5;
   }
   $y = Left1($pdf, "<b>6. AUTOMATIC CANCELLATION</b>", $y + 15);

   $y = Left1($pdf, "If:", $y + 5);

   $y = Left1($pdf, "6.1", $y + 5);
   $y = Left2($pdf, "the Company, as agent of the Investors Group, fails for whatever reason to complete the purchase of the Asset by the Date of Purchase; or", $y - 7);

   $y = Left1($pdf, "6.2", $y + 5);
   $y = Left2($pdf, "the Company, as agent of the Investors Group, does not receive the full amount of the Purchase Cost from the Investors Group, by no later than the Purchase Cost Contribution Date (unless the Company elects, pursuant to Clause 1.6.6, to complete the purchase of the Asset based on the Adjusted Purchase Cost, and thereafter completes such purchase of the Asset by the Date of Purchase); or", $y - 7);

   $y = Left1($pdf, "6.3", $y + 5);
   $y = Left2($pdf, "prior to the completion of the purchase of the Asset by the Company, as agent of the Investors Group, an Event of Default occurs or it becomes illegal in any applicable jurisdiction or under any applicable laws or regulations for the Investors Group or the Company, as agent of the Investors Group, to purchase the Asset,", $y - 7);

   $y = Left2($pdf, "the Parties agree that the Murabahah transaction described in this Agreement shall be immediately and automatically cancelled, and the Company shall immediately refund the Investors Group all monies advanced or paid to it for the purchase of the Asset on behalf of the Investors Group, and will in addition indemnify each Financer in respect of all actual costs and expenses incurred by him in relation to the entry into this Agreement and the advance of funds to the Company.", $y + 5);

   $pg = $pdf->getPage();
   if ($pg < $synp3) {
      $pdf->setPage($synp3);
      $y = -5;
   }
   $y = Left1($pdf, "<b>7. EVENTS OF DEFAULT</b>", $y + 15);

   $y = Left1($pdf, "7.1", $y + 10);
   $y = Left2($pdf, "An Event of Default is deemed to have occurred in any of the following circumstances, whether or not arising due to the fault of the Company:-", $y - 7);

   $y = Left1($pdf, "7.1.1", $y + 3);
   $y = Left3($pdf, "The Company fails to fulfill any of its payment obligations in respect of the Sale Price, unless such failure is a result of a technical or administrative error on the part of the bank or financial institution remitting payment by the Company to Kapital Boost, or by Kapital Boost to the Financier; or", $y - 7);

   $y = Left1($pdf, "7.1.2", $y + 3);
   $y = Left3($pdf, "The Company fails to observe and perform any of its obligations under this Agreement (including, without limitation (a) its obligation to offer to purchase the Asset from the Investors Group, and (b) its obligations as agent of the Investors Group to purchase the Asset at the Purchase Cost and to accept the Offer to Purchase); or", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp12) {
      $pdf->setPage($synp12);
      $y = 7;
   }
   $y = Left1($pdf, "7.1.3", $y + 3);
   $y = Left3($pdf, "Any representation, statement, or warranty given or made by the Company in this Agreement proves to be untrue, incorrect or inaccurate or misleading in any material respect; or", $y - 7);

   $y = Left1($pdf, "7.1.4", $y + 3);
   $y = Left3($pdf, "Any step is taken for the winding up, liquidation or dissolution of the Company;", $y - 7);

   $y = Left1($pdf, "7.1.5", $y + 3);
   $y = Left3($pdf, "Where there is an element of fraud, misuse, or misappropriation of Purchase Cost by the Company", $y - 7);

   $y = Left1($pdf, "7.2", $y + 5);
   $y = Left2($pdf, "Upon the occurrence of any Event of Default, the Financier shall have the right by notice to the Company to:", $y - 7);

   $y = Left1($pdf, "7.2.1", $y + 3);
   $y = Left3($pdf, "declare the entire proportion of the Sale Price outstanding from the Company to the Financier to become immediately due and payable; and/or", $y - 7);

   $y = Left1($pdf, "7.2.2", $y + 3);
   $y = Left3($pdf, "require the Company to (a) sell the Asset and apply all proceeds of sale to pay the outstanding Sale Price; and (b) otherwise not dispose of or transfer the ownership of the Asset to any other person; and/or", $y - 7);

   $y = Left1($pdf, "7.2.3", $y + 3);
   $y = Left3($pdf, "require the Company to indemnify the Investors Group for any costs, losses or expenses incurred as a result of the occurrence of any Event of Default including, without limitation (a) any loss of profit by the Financier arising from (i) the failure by the Company to fulfill its undertaking to offer to purchase the Asset from the Investors Group, or (ii) failure by the Company as agent of the Investors Group to purchase the Asset at the Purchase Cost or to accept the Offer to Purchase, (b) any costs incurred in taking physical possession of the Asset following an Event of Default, and (c) any diminution in the value of the Asset arising from a breach by the Company of the terms of this Agreement.", $y - 7);

   $y = Left1($pdf, "7.3", $y + 5);
   $y = Left2($pdf, "The right of the Financier to accelerate the obligations of the Company in respect of the portion of the Sale Price due to the Financier may be exercised irrespective of, and without being contingent upon, the action or inaction of any other financier or the rest of the Investors Group. The Financier shall be entitled to bring or prosecute any legal action or claim against the Company to recover the outstanding sum, and the Company agrees to immediately upon demand indemnify the Financier for all actual costs, losses and liabilities (including legal fees) incurred or suffered by the Financier as a result of any Event of Default.", $y - 7);

   if ($pg < $synp11) {
      $pdf->setPage($synp11);
      $y = -5;
   }
   $y = Left1($pdf, "<b>8. REPRESENTATIONS</b>", $y + 15);

   $y = Left1($pdf, "8.1", $y + 10);
   $y = Left2($pdf, "The Company represents to the Financier on the date of this Agreement
  that:", $y - 7);

   $y = Left1($pdf, "8.1.1", $y + 3);
   $y = Left3($pdf, "It is duly incorporated and validly existing under the laws of Republic of Indonesia;", $y - 7);

   $y = Left1($pdf, "8.1.2", $y + 3);
   $y = Left3($pdf, "It has the power to execute this Agreement, and to perform its obligations under this Agreement and has taken all necessary action to authorize such execution and performance;", $y - 7);

   $y = Left1($pdf, "8.1.3", $y + 3);
   $y = Left3($pdf, "The execution and performance of this Agreement does not violate or conflict with any applicable law or regulation, any provision of its constitutional documents, and order or judgment of any court or other agency of government applicable to it, or any contractual restriction binding on it;", $y - 7);

   $y = Left1($pdf, "8.1.4", $y + 3);
   $y = Left3($pdf, "All consent required to have been obtained by it with respect to this Agreement have been obtained and are in full force and effect;", $y - 7);

   $y = Left1($pdf, "8.1.5", $y + 3);
   $y = Left3($pdf, "Its obligations under this Agreement constitute its legal, valid and binding obligations, enforceable in accordance with their respective terms;", $y - 7);

   $y = Left1($pdf, "8.1.6", $y + 3);
   $y = Left3($pdf, "No Event of Default has occurred or would reasonably be expected to result from the entry into, or performance of, this Agreement; and", $y - 7);

   $y = Left1($pdf, "8.1.7", $y + 3);
   $y = Left3($pdf, "The core or main business of the Company and the purpose of the financing under this Agreement are and shall at all times remain Shariah compliant.", $y - 7);

   $y = Left1($pdf, "8.2", $y + 5);
   $y = Left2($pdf, "The representations and warranties in this Clause 6 are repeated by the Company on the Purchase Date and on each scheduled date for payment of all or part of the Sale Price.", $y - 7);

   $y = Left1($pdf, "<b>9. LATE PAYMENT CHARGES (TA’WIDH AND GHARAMAH)</b>", $y + 15);

   $y = Left1($pdf, "9.1", $y + 10);
   $y = Left2($pdf, "If any amount which is due and payable by the Company under or in connection with this Agreement is not paid in full on the date in accordance with this Agreement (an \"<b>unpaid amount</b>&nbsp;\"), the Company undertakes to pay late payment charge comprising both ta’widh and gharamah (calculated below) to the Investor Group on each day that an unpaid amount remains outstanding.", $y - 7);

   $y = Left1($pdf, "9.2", $y + 5);
   $y = Left2($pdf, "The total late payment charge shall be pegged directly to average annualized returns of this Murabahah Agreement (to be determined by the Company), calculated on daily rest basis as below, provided that the accumulated late payment charges shall not exceed 100 percent of the outstanding unpaid amount:", $y - 7);

   $y = CenterL($pdf, "Unpaid Amount X average annualized returns X", $y + 5);
   $y = CenterL($pdf, "No. of Overdue day(s) / 365", $y);

   $y = Left1($pdf, "9.3", $y + 5);
   $y = Left2($pdf, "Each Financier may retain only such part of the late payment amount paid to it, as is necessary to compensate the Financier for any actual costs (not to include any opportunity or funding costs), subject to and in accordance with the guidelines of its own Shariah advisers, and up to a maximum aggregate amount of 1% per annum of the unpaid amount (“ta’widh”). ", $y - 7);

   $y = Left1($pdf, "9.4", $y + 5);
   $y = Left2($pdf, "The ta’widh in respect of an unpaid amount, being part of the late payment charges, may accrue on a daily basis and shall be calculated in accordance with the following formula:-", $y - 7);

   $y = CenterL($pdf, "Actual cost incurred", $y + 3);
   $y = CenterL($pdf, "or", $y);
   $y = CenterL($pdf, "Unpaid Amount X 1% X No. of Overdue day(s) / 365", $y);

   $y = Left1($pdf, "9.5", $y + 5);
   $y = Left2($pdf, "The amount of ta’widh shall not be compounded.", $y - 7);

   $y = Left1($pdf, "9.6", $y + 5);
   $y = Left2($pdf, "The Investor Group hereby agree(s), covenant(s) and undertake(s) to ensure that the balance of the late payment charges (“gharamah”), if any, shall be paid to charitable bodies as may be selected by the Company in consultation with its Shariah advisers.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp4) {
      $pdf->setPage($synp4);
      $y = -5;
   }
   $y = Left1($pdf, "<b>10. GOVERNING LAW AND DISPUTE RESOLUTION</b>", $y + 15);

   $y = Left1($pdf, "10.1", $y + 10);
   $y = Left2($pdf, "This Agreement and the rights and obligations of the Parties hereunder shall be governed by and interpreted and construed in all respects in accordance with the laws of Republic of Indonesia without prejudice to or limitation of any other rights or remedies available to both Parties in this Agreement under the laws of any jurisdiction where the Company or his assets may be located.", $y - 7);

   $y = Left1($pdf, "10.2", $y + 5);
   $y = Left2($pdf, "The Parties hereby irrevocably agree to submit to the exclusive jurisdiction of the Courts of Republic of Indonesia to resolve any disputes or make any claims with respect to this Agreement.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp5) {
      $pdf->setPage($synp5);
      $y = -5;
   }
   $y = Left1($pdf, "<b>11. MISCELLANEOUS</b>", $y + 15);

   $y = Left1($pdf, "11.1", $y + 10);
   $y = Left21($pdf, "If at any time any one or more of the provisions hereof is or become illegal, invalid or unenforceable under Republic of Indonesia law, neither the legality, validity or enforceability of the remaining provisions hereof nor the legality, validity or enforceability of such provisions under the laws of any other jurisdiction shall in any way be affected or impaired thereby.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp13) {
      $pdf->setPage($synp13);
      $y = 5;
   }
   $y = Left1($pdf, "11.2", $y + 5);
   $y = Left21($pdf, "If any provision of this Agreement (or part of it) or the application thereof to any person or circumstance shall be illegal, invalid or unenforceable to any extent, it must be interpreted as narrowly as necessary to allow it to be enforceable or valid and the remainder of this Agreement and the legality, validity or enforceability of such provisions to other persons or circumstances shall not be in any way affected or impaired thereby and shall be enforceable / enforced to the greatest extent permitted by law.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp9) {
      $pdf->setPage($synp9);
      $y = 5;
   }
   $y = Left1($pdf, "11.3", $y + 5);
   $y = Left21($pdf, "All rights and obligations in this Agreement are personal to the Parties and each Party in this Agreement may not assign and/or transfer any such rights and obligations to any third party without the prior consent in writing of the Parties.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp14) {
      $pdf->setPage($synp14);
      $y = 5;
   }
   $y = Left1($pdf, "11.4", $y + 5);
   $y = Left21($pdf, "The Contracts (Rights of Third Parties) Act 2001 shall not apply to this Agreement and no person who is not a party of this Agreement shall have any rights under the Contracts (Rights of Third Parties) Act 2001 to enforce any term of this Agreement.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp15) {
      $pdf->setPage($synp15);
      $y = 5;
   }
   $y = Left1($pdf, "11.5", $y + 5);
   $y = Left21($pdf, "This Agreement contains the entire understanding between the Parties relating to the transaction contemplated by this Agreement and shall supersede any prior expressions of intent or understandings with respect to the said transaction. All prior or contemporaneous agreements, understandings, representations and statements, oral and written, are merged in this Agreement and shall be of no further force or effect.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp16) {
      $pdf->setPage($synp16);
      $y = 5;
   }
   $y = Left1($pdf, "11.6", $y + 5);
   $y = Left21($pdf, "Any communication to be made under or in connection with this Agreement shall be made in writing and, may be made by letter or electronic mail, in each case, through Kapital Boost at <b>150 Changi Road, #04-07, Singapore 419973</b>&nbsp;&nbsp;&nbsp;&nbsp; or at such other address as Kapital Boost may notify the parties from time to time. Any communication made between Kapital Boost and any of the Parties under or in connection with this Agreement shall be made to the address or electronic mail address provided to Kapital Boost or its registered address, in the case of the Company, and shall be effective when received.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp17) {
      $pdf->setPage($synp17);
      $y = 5;
   }
   $y = Left1($pdf, "11.7", $y + 5);
   $y = Left21($pdf, "No failure to exercise, nor any delay in exercising, on the part of the Financier, any right or remedy under this Agreement shall operate as a waiver, nor shall any single or partial exercise of any right or remedy prevent any further or other exercise or the exercise of any other right or remedy. The rights and remedies under this Agreement are cumulative and not exclusive of any rights or remedies provided by law.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp18) {
      $pdf->setPage($synp18);
      $y = 5;
   }
   $y = Left1($pdf, "11.8", $y + 5);
   $y = Left21($pdf, "No provision of this Agreement may be amended, waived, discharged or terminated orally nor may any breach of or default under any of the provisions of this Agreement be waived or discharged orally but (in each case) except (a) pursuant to an Adjustment Notice which complies with the requirements of this Agreement; or (b) by an instrument in writing signed by or on behalf of the Parties or (b). Any amendments or variations to this Agreement shall be Shariah-compliant.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp19) {
      $pdf->setPage($synp19);
      $y = 5;
   }
   $y = Left1($pdf, "11.9", $y + 5);
   $y = Left21($pdf, "This Agreement may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);

   $pg = $pdf->getPage();
   if ($pg < $synp20) {
      $pdf->setPage($synp20);
      $y = 5;
   }
   $y = Left1($pdf, "11.10", $y + 5);
   $y = Left21($pdf, "Each Party agrees to keep all information relating to this Agreement confidential, and not disclose it to anyone, save with the prior written consent of the other Parties or as required by any applicable laws or regulations.", $y - 7);

   $y = Left1($pdf, "11.11", $y + 5);
   $y = Left21($pdf, "This Agreement is intended to be Shariah-compliant. The parties hereby agree and acknowledge that their respective rights and obligations under this Agreement are intended to, and shall, be in conformity with Shariah principles.", $y - 7);


   $y = Left1($pdf, "11.12", $y + 5);
   $y = Left21($pdf, "Notwithstanding the above, each party represents to the other that it shall not raise any objections or claims against the other on the basis of Shariah-compliance or any breach of Shariah principles in respect of or otherwise in relation to any part of any provision of this Agreement.  ", $y - 7);

   $y = Left1($pdf, "<b>12. DEFINITIONS</b>", $y + 15);

   $y = Left1($pdf, "In this Agreement, any reference to:", $y + 10);

   $y = Left1($pdf, "12.1", $y + 5);
   $y = Left21($pdf, "any Party or Kapital Boost shall be construed so as to include its successors in title, permitted assigns and permitted transferees;", $y - 7);

   $y = Left1($pdf, "12.2", $y + 5);
   $y = Left21($pdf, "a provision of law is a reference to that provision as amended or re-enacted;", $y - 7);

   $y = Left1($pdf, "12.3", $y + 5);
   $y = Left21($pdf, "a person or Party in a particular gender shall include that person or Party regardless of his or her gender, and shall apply equally where that person or Party is a body corporate, firm, trust, joint venture or partnership;", $y - 7);

   $y = Left1($pdf, "12.4", $y + 5);
   $y = Left21($pdf, "the \"<b>Investors Group</b>&nbsp; \" shall be construed to mean just the Financier if the Financier is financing the entirety of the Purchase Cost;", $y - 7);

   $y = Left1($pdf, "12.5", $y + 5);
   $y = Left21($pdf, "\"<b>Kapital Boost</b>&nbsp; \" means <b>Kapital Boost Pte Ltd, Singapore Company Registration No. 201525866W</b>", $y - 7);

   $y = Left1($pdf, "12.6", $y + 5);
   $y = Left21($pdf, "\"<b>Sale Price</b>&nbsp; \" shall mean the Sale Price as adjusted pursuant to Clause 1.6.6.", $y - 7);

   $pdf->setPage($synp100);

   $pdf->SetMargins(20, 15);
   $y = MCenter1($pdf, "<b>Dated this day $date / Tertanggal hari $dateID</b>", 15);

   $y = MLeft1($pdf, "Signed by / Tertanda oleh<br>for and on behalf of / untuk dan atas nama<br><b>$companyName</b><br><br><hr><br><b>Name: $dirName</b><br><b>Title: $dirTitle</b><br><b>$dirIC / $dirICID</b>", $y + 15);

   $oSignx = $pdf->GetX() + 60;
   $oSigny = $y + 70;
   $oSignp = $pdf->getPage();

   $y = MLeft1($pdf, "Signed by/Tertanda oleh", $y + 10);

   $iSignx = $pdf->GetX() + 60;
   $iSigny = $y + 212;
   $iSignp = $pdf->getPage();

   $y = MLeft1($pdf, "<hr><br><br><b>Name: $invName</b><br><b>$invIC</b>", $y + 10);



   /*  ---------- END of English Version ------------ */



   $pdf->AddPage();
   $y = Left1($pdf, "<b>SCHEDULE A: Description of Assets</b>", 20);
   $y = Left1($pdf, "<b>LAMPIRAN A: Deskripsi Aset</b>", 27);

   $pdf->Image($image, 15, $y + 10, 180, null, '', '', '', true, 150, '', false, false, 1, false, false, false);

   $pdf->AddPage();
   $synp101 = $pdf->getPage();
   $y = Left1($pdf, "<b style=\"font-size:1.2em\">SCHEDULE B: Form of Murabahah Sale Contract</b>", 20);

   $y = CenterL($pdf, "<b>Murabahah Sale Contract</b>", $y + 5);

   $y = CenterL($pdf, "Between $companyName (the \"<b>Company</b> \") and the Investors Group", $y + 5);

   $y = Left1($pdf, "Re: &nbsp;&nbsp;Murabahah Agreements between the Company and various Financiers (together forming the Investors Group) with respect to the purchase of the Asset described below (the \"<b>Murabahah Agreements</b>&nbsp;&nbsp;\", and each, a \"<b>Murabahah Agreement</b>&nbsp;&nbsp;\"). ", $y + 5);

   $y = Left1($pdf, "1.", $y + 5);
   $y = Left2($pdf, "All terms defined in the Murabahah Agreements have the same meaning in this Murabahah Sale Contract.", $y - 7);

   $y = Left1($pdf, "2.", $y + 5);
   $y = Left2($pdf, "The Company hereby confirms that it, as agent of the Investors Group, has purchased the Asset, and that the Asset is in the Company’s possession. The Company has attached to this Murabahah Sale Contract (a) a description of the Asset; and (b) a true copy of the receipt for the purchase of the Asset by us, as agent of the Investors Group.", $y - 7);

   $y = Left1($pdf, "3.", $y + 5);
   $y = Left2($pdf, "In accordance with the terms of the Murabahah Agreements, the Company hereby offers to purchase the Asset from the Investors Group based on the terms and subject to the conditions set out in the Murabahah Agreements. Pursuant to the terms of the Murabahah Agreements, the Sale Price is $salePrice, and will be paid as follows:", $y - 7);


   $payoutBTbl = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Date</b></td>
  <td style=\"text-align:center\"><b>[Installment Amount of] Sale Price payable to Investors Group</b></td>
  </tr>
  $payoutString
  </table>
  ";
   $pdf->SetY($y + 10);
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutBTbl);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Left1($pdf, "4.", $y);
   $y = Left2($pdf, "By accepting and agreeing to this Murabahah Sale Contract, the Investors Group shall immediately sell the Asset to the Company on the terms and subject to the conditions set out in the Murabahah Agreement.", $y - 7);

   $y = Left1($pdf, "5.", $y + 10);
   $y = Left2($pdf, "This Murabahah Sale Contract may be executed in any number of counterparts, and this has the same effect as if the signatures on the counterparts were on a single copy of this Agreement.", $y - 7);


   $y = Left1($pdf, "For and on behalf of the Company (as buyer of Asset)", $y + 20);

   $y = Left1($pdf, "<hr style=\"width:150;\">", $y + 25);
   $y = Left1($pdf, "Name:", $y + 2);
   $y = Left1($pdf, "Title:", $y + 2);
   $y = Left1($pdf, "Date:", $y + 2);

   $y = Left1($pdf, "Kapital Boost, as agent for and on behalf of the Investors Group (as seller of Asset)", $y + 15);

   if ($y > 255) {
      $pdf->AddPage();
      $y = 10;
   }
   $y = Left1($pdf, "<hr style=\"width:150\">", $y + 25);
   $y = Left1($pdf, "Name:", $y + 2);
   $y = Left1($pdf, "Title:", $y + 2);
   $y = Left1($pdf, "Date:", $y + 2);

   /* End of Eng Schedule B */


   $pdf->setPage($synp101);

   $y = Right1($pdf, "<b style=\"font-size:1.2em\">LAMPIRAN B: Lembar Kontrak Penjualan Murabahah</b>", 20);

   $y = CenterR($pdf, "<b>Kontrak Penjualan Murabahah</b>", $y + 5);

   $y = CenterR($pdf, "Antara $companyName (the \"<b>Perusahaan</b> \") dan Grup Investor", $y + 5);

   $y = Right1($pdf, "Hal: &nbsp;&nbsp;Antara [	] (“Perusahaan”) dan Grup Investor (\"<b>Perjanjian-perjanjian Murabahah</b>&nbsp;&nbsp;\", Perjanjian-perjanjian Murabahah, \"<b>Perjanjian Murabahah</b>&nbsp;&nbsp;\"). ", $y + 5);

   $y = Right1($pdf, "1.", $y + 5);
   $y = Right2($pdf, "Seluruh istilah yang didefinisikan dalam Perjanjian-perjanjian Murabahah memiliki pengertian yang sama dalam Kontrak Penjualan Murabahah ini.", $y - 7);

   $y = Right1($pdf, "2.", $y + 5);
   $y = Right2($pdf, "Perusahaan dengan ini menegaskan bahwa Perusahaan, sebagai agen Grup Investor, telah membeli Aset, dan bahwa Aset adalam penguasaan Perusahaan. Perusahaan telah melampirkan pada Kontrak Penjualan Murabahah ini (a) penjelasan tentang Aset;  dan (b) salinan sebenarnya dari tanda terima pembelian Aset oleh kami, sebagai agen Grup Investor.", $y - 7);

   $y = Right1($pdf, "3.", $y + 5);
   $y = Right2($pdf, "Menurut ketentuan Perjanjian-perjanjian Murabahah, Perusahaan dengan ini menawarkan untuk membeli Aset dari Grup Investor berdasarkan ketentuan dan tunduk pada persyaratan yang terdapat dalam Perjanjian-perjanjian Murabahah. Sesuai ketentuan Perjanjian-perjanjian Murabahah, Harga Jual sebesar $salePrice, dan akan dibayarkan sebagai berikut:", $y - 7);


   $payoutBTbl = "<table cellspacing=\"0\" cellpadding=\"7\" border=\"1\">
  <tr>
  <td style=\"text-align:center\"><b>Tanggal</b></td>
  <td style=\"text-align:center\"><b>[Jumlah Angsuran] Harga Jual yang wajib dibayarkan kepada Grup Investor </b></td>
  </tr>
  $payoutStringR
  </table>
  ";
   $pdf->SetY($y + 10);
   $pdf->SetMargins(110, 10, 10);
   $pdf->SetFontSize(11);
   $pdf->writeHTML($payoutBTbl);
   $pdf->SetFontSize(12);
   $y = $pdf->GetY();

   $y = Right1($pdf, "4.", $y);
   $y = Right2($pdf, "Dengan menerima dan menyepakati Kontrak Penjualan Murabahah ini, Grup Investor akan segera menjual Aset kepada Perusahaan dengan ketentuan dan tunduk pada persyaratan yang terdapat dalam Perjanjian Murabahah.", $y - 7);

   $y = Right1($pdf, "5.", $y + 10);
   $y = Right2($pdf, "Kontrak Penjualan Murabahah ini dapat ditandatangani dalam berapa pun jumlah rangkap, dan memiliki keberlakuan yang sama seolah-olah tanda tangan-tanda tangan pada rangkap tersebut terdapat pada salinan tunggal dari Perjanjian ini.", $y - 7);


   $y = Right1($pdf, "Untuk dan atas nama Perusahaan (sebagai pembeli Aset)", $y + 20);

   $y = Right1($pdf, "<hr style=\"width:150;\">", $y + 25);
   $y = Right1($pdf, "Nama:", $y + 2);
   $y = Right1($pdf, "Gelar:", $y + 2);
   $y = Right1($pdf, "Tanggal:", $y + 2);

   $y = Right1($pdf, "Kapital Boost, sebagai agen untuk dan atas nama Grup Investor (sebagai penjual Aset)", $y + 15);

   if ($y > 255) {
      $pdf->AddPage();
      $y = 10;
   }
   $y = Right1($pdf, "<hr style=\"width:150\">", $y + 25);
   $y = Right1($pdf, "Nama:", $y + 2);
   $y = Right1($pdf, "Gelar:", $y + 2);
   $y = Right1($pdf, "Tanggal:", $y + 2);




   $output = $pdf->Output();
}

if ($output == 1) {

   $sign = array("ix" => $iSignx, "iy" => $iSigny, "ip" => $iSignp, "ox" => $oSignx, "oy" => $oSigny, "op" => $oSignp);
   $signCoded = json_encode($sign);
   $result = $mysql->UpdateInvContract($iId, $signCoded);
   echo $result;
} else {
   echo -1;
}


/* Monolingual Functions */

function MCenter($pdf, $string, $y, $blue = false) {
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(25, 15);
   $width = $pdf->GetStringWidth($string);
   $x = 105 - ($width / 2);
   $pdf->SetXY($x, $y);
   $pdf->Write(8, $string);
}

function MCenter1($pdf, $string, $y) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $width = $pdf->GetStringWidth($string) - 20; // adjust <b> and </b>
   $x = 105 - ($width / 2);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft1($pdf, $string, $y, $x = 20) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft2($pdf, $string, $y, $x = 34) {
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 15, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MLeft3($pdf, $string, $y, $x = 46) { // 44
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 15, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MBullet($pdf, $y, $x = 40) {
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function MLeft4($pdf, $string, $y, $x = 54) { // 60
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function MLeft5($pdf, $string, $y, $x = 62) { // 70
   if ($y > 270) {
      $pdf->AddPage('P', array('210', '297'));
      $y = 20;
   }
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function MLeft6($pdf, $string, $y, $x = 70) {
   $pdf->SetMargins($x, 10, 20);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   if ($newY > 250) {
      $pdf->AddPage('P', array('210', '297'));
      $pdf->SetMargins(25, 15);
      $newY = 10;
   }
   return $newY;
}

/* Bilingual Functions */

function CenterL($pdf, $string, $y, $blue = false) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(10, 10, 110);
   $pdf->SetXY(10, $y);
   $string = "<span style=\"text-align:center;\">" . $string . "</span>";
   $pdf->writeHTML($string);
   return $pdf->GetY();
}

function BulletL($pdf, $y, $x = 20) {
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function Left1($pdf, $string, $y, $x = 10) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left2($pdf, $string, $y, $x = 20) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left21($pdf, $string, $y, $x = 23) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left3($pdf, $string, $y, $x = 26) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Left4($pdf, $string, $y, $x = 34) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Left5($pdf, $string, $y, $x = 42) { // 70
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 110);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function CenterR($pdf, $string, $y, $blue = false) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   if ($blue == true) {
      $pdf->SetTextColor(4, 51, 255);
   } else {
      $pdf->SetTextColor(0, 0, 0);
   }
   $pdf->SetMargins(110, 10, 7);
   $pdf->SetXY(110, $y);
   $string = "<span style=\"text-align:center;\">" . $string . "</span>";
   $pdf->writeHTML($string);
   return $pdf->GetY();
}

function BulletR($pdf, $y, $x = 130) {
   $pdf->SetMargins($x, 1);
   $pdf->SetXY($x, $y);
   $pdf->writeHTML("<span style=\"text-align:justify;line-height:20px;font-size:30px\">.</span>");
}

function Right1($pdf, $string, $y, $x = 110) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right2($pdf, $string, $y, $x = 120) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right21($pdf, $string, $y, $x = 123) {
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right3($pdf, $string, $y, $x = 126) { // 44
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}

function Right4($pdf, $string, $y, $x = 134) { // 60
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();
   return $newY;
}

function Right5($pdf, $string, $y, $x = 142) { // 70
   if ($y > 280) {
      $pdf->AddPage();
      $y = 10;
   }
   $pdf->SetMargins($x, 10, 7);
   $pdf->SetXY($x, $y);
   $string = "<span style=\"text-align:justify;line-height:20px\">" . $string . "</span>";
   $pdf->writeHTML($string);
   $newY = $pdf->GetY();

   return $newY;
}
