<?php
include_once 'security.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once 'lib/helper.php';

include_once 'mysql.php';
$mysql = new mysql();

if (isset($_GET["add"])) {
   if ($_GET["add"] == "1") {
      if ($mysql->Connection()) {
         $newcId = $mysql->AddNewCampaign1();
         $contract_number = contract_number_generator($mysql);
         $mysql->AddNewContractUKM($newcId, $contract_number);

         $mysql->AddNewContract1($newcId, $contract_number);
         header("Location: campaign-edit.php?c=$newcId");
      }
   }
}

if (isset($_GET["c"])) {
   $c = $_GET["c"];
   header("Location: campaign-edit.php?c=$c");
}

if (isset($_GET["keyword"])) {
   $keyword = $_GET["keyword"];
} else {
   $keyword = "";
}

if ($mysql->Connection()) {
   list($cId, $cName, $projectType, $releaseDate, $closingDate, $totalFund, $minAmount, $tenor, $return, $currentFund) = $mysql->GetAllCampaignsList($keyword);
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KapitalBoost Campaigns</title>

      <?php include_once 'include.php'; ?>


      <script>

          $(document).ready(function () {
             $(".DelBtn").click(function () {
                var index = $(this).parent().parent().index();
                var person = prompt("Please enter Master Code");
                $.post("delete.php", {delete: $(this).val(), MC: person}, function (reply) {
                   reply = JSON.parse(reply);
                   $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                      if (reply === "Done") {
                         $("tr").eq(index + 1).remove();
                      }
                   });
                   $("#popupText").html(reply);
                });
             });
             $(".RefreshBtn").click(function () {
                $.post("posts.php", {job: 'refreshtotalfunding', cId: $(this).val()}, function (reply) {
                   $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                      location.reload();
                   });
                });
                $("#popupText").html("Refreshed");
             });
			 $(".DuplicateBtn").click(function () {
                $.post("posts.php", {job: 'duplicatecampaign', cId: $(this).val()}, function (reply) {
                   $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                      location.reload();
                   });
                });
                $("#popupText").html("Duplicated");
             });
          });


      </script>

   </head>
   <body>
      <?php include_once 'header.php'; ?>

      <?php include_once 'popup.php'; ?>

      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Campaign List</span>
                     <a class="btn btn-warning" style="margin: auto 20px;width:200px" href="campaigns.php?add=1">Add New Campaign</a>
					 <form method="post" action="excel-extract.php" style="display:inline-block;margin: auto 20px;">
							<input type="hidden" name="action"  value="payout"/>
							<input class="btn btn-info" type="submit" value="Extract Payout Data"/>
					 </form>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body">


                     <br/><br/>
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover"  name="myTable" id="myTable">
                           <thead>
                              <tr>
                                 <th width="2%">No</th>
                                 <th width="20%">Campaign Name</th>
                                 <th width="5%">Type</th>
                                 <th width="12%">Release Date</th>
                                 <!-- <th width="12%">Closing Date</th> -->
                                 <th width="10%">Current Funding AMT</th>
                                 <th width="10%">Total Funding</th>
                                 <th width="8%">Tenor</th>
                                 <th width="5%">Contract Info</th>
                                 <th width="7%">Refresh Total Funding</th>
                                 <th width="10%">Master Payout</th>
                                 <th width="10%">Payout Report</th>
                                 <th width="6%">Campaign Report</th>
                                 <th width="6%">Duplicate</th>
                                 <th width="3%">Check Payout</th>
                                 <th width="3%">Delete</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                              for ($i = 0; $i < count($cId); $i++) {
                                 ?>
                                 <tr class="even">
                                    <td><?= $i + 1 ?></td>
                                    <td><a href="<?php echo ADMIN_PATH ?>/campaign-edit.php?c=<?= $cId[$i] ?>"><?= $cName[$i] ?></a></td>
                                    <td><?= $projectType[$i] ?></td>
                                    <td><?= $releaseDate[$i] ?></td>
                                    <!-- <td><?#= $closingDate[$i] ?></td> -->
                                    <td><?= $currentFund[$i] ?></td>
                                    <td><?= $totalFund[$i] ?></td>
                                    <td><?= $tenor[$i] ?></td>
                                    <td style="text-align:center">
                                      <a href="contract.php?c=<?=$cId[$i]?>">
                                        <i class="fa fa-tasks" style="color:orange;font-size:1.5em"></i>
                                      </a>
                                      &nbsp;&nbsp;
                                      <a href="contract_ukm.php?c=<?=$cId[$i]?>">
                                        <i class="fa fa-list" style="color:silver;font-size:1.5em"></i>
                                      </a>
                                    </td>
                                    <td style="text-align:center"><button value="<?= $cId[$i] ?>" class="btn RefreshBtn"><i class="fa fa-refresh" style="font-size:1.3em;color:blueviolet"></i></button></td>
                                    <td style="text-align: center"><a href="master-payout.php?c=<?= $cId[$i] ?>"><i class="fa fa-money" style="color:#0066ff;font-size:2em"></i></a></td>
                                    <td><a href="/admin/report-payout.php?id=<?= $cId[$i] ?>" class="btn btn-xs btn-info">Report Payout</a></td>
                                    <td style="text-align: center"><a href="investment-report.php?c=<?= $cId[$i] ?>"><i class="fa fa-book" style="color:#00cc00;font-size:2em"></i></a></td>
                                    <td style="text-align: center"><button value="<?= $cId[$i] ?>" class="btn DuplicateBtn"><i class="fa fa-copy" style="color:blue"></i></button></td>
                                    <td style="text-align: center">
                                      <button type="button" class="btn get-payout-data" data-toggle="modal" data-target="#myModal" data-id="<?= $cId[$i] ?>" data-title="Checking amount payout (<?= $cName[$i] ?>)">
                                        <!-- <i class="fa fa-refresh" style="color:orange"></i> -->
                                        <img src="/assets/icon/check-payout.png" width="24" />
                                      </button>
                                    </td>
									                  <td style="text-align: center"><button value="<?= $cId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                                 </tr>
                                 <?php
                              }
                              ?>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Check Payout</h4>
          </div>
          <div class="modal-body">
            <p class="message-update" style="color: #f39c12;padding: 10px 0;margin-bottom: 10px;"></p>
            <table class="table table-bordered" id="list-payouts">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Investor Name</th>
                  <th>Campaign</th>
                  <th>Expected Payout</th>
                  <th>Total per-Month</th>
                  <th>Fix Amount</th>
                  <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary update-payout-data">Update</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
	  <script>

    var updatePayout = function() {
      $(".update-payout-data").click(function() {
        var r = confirm("Are you sure want to update?");
        if (r == true) {
          id = $(this).data('id');
          $(".update-payout-data").hide();
          $.ajax({
            method: "GET",
            url: "./api.php?api_action=update-payout&id=" + id,
            dataType: "json",
            success: function(result){
              if (result['updated']) {
                $(".message-update").text('Data berhasil di update');
                $("#list-payouts tbody").html("");
                $no = 1;
                $elment = "";
                $row_color = ["danger", "success"];

                $.each(result['datas'], function(idx, data) {
                  $elm = "<tr class='"+data['status']+"'>";
                    $elm += "<td>"+$no+"</td>";
                    $elm += "<td>"+data['investor_name']+"</td>";
                    $elm += "<td>"+data['campaign_name']+"</td>";
                    $elm += "<td>"+data['expected_payout']+"</td>";
                    $elm += "<td>"+data['total_payout']+"</td>";
                    $elm += "<td><b>"+data['nominal']+"</b></td>";
                  $elm += "</tr>";

                  // ========
                  $elment = $elment + $elm;
                  $no+=1;
                })
                $("#list-payouts tbody").append($elment);
              }else{
                $(".message-update").text('Terjadi Kesalahan dalam update');
              }
            }
          });
        } else {
          return false;
        }
      })
    }

    var payoutAction = function() {
      setTimeout(function() {
        $(".get-payout-data").click(function(event) {
          event.preventDefault();

          $("#list-payouts tbody").html("");
          id = $(this).data('id');

          $(".update-payout-data").attr("data-id", id);
          $(".message-update").text('');

          $.ajax({
            method: "GET",
            url: "./api.php?api_action=check-payout&id=" + id,
            dataType: "json",
            success: function(result){
              if (result['datas'].length < 1) {
                $("#list-payouts tbody").append("<td colspan='6' align='center' style='padding: 10px'><b>"+result['message']+"</b></td>")
                $(".update-payout-data").hide();
              }else{
                $(".update-payout-data").show();
                $no = 1;
                $elment = "";
                $.each(result['datas'], function(idx, data) {
                  $elm = "<tr>";
                    $elm += "<td>"+$no+"</td>";
                    $elm += "<td>"+data['investor_name']+"</td>";
                    $elm += "<td>"+data['campaign_name']+"</td>";
                    $elm += "<td>"+data['expected_payout']+"</td>";
                    $elm += "<td>"+data['total_payout']+"</td>";
                    $elm += "<td><b>"+data['nominal']+"</b></td>";
                  $elm += "</tr>";

                  // ========
                  $elment = $elment + $elm;
                  $no+=1;
                })
                $("#list-payouts tbody").append($elment);
              }

            }
          });

        })
      }, 500)
    }

		$(document).ready( function () {
			var table = $('#myTable').DataTable({
        stateSave: true
      });
      updatePayout();
      payoutAction();
      $('#myTable').on( 'page.dt', function () {
        // var info = table.page.info();
        // console.log(info);
        payoutAction();
      });
		});
		</script>

   </body>


</html>
