<!DOCTYPE html>
<html>

<?php 
	include_once 'security.php';
	include_once 'mysql.php';
	$mysql = new mysql();
	$id = $_GET['id'];

	if ($mysql->Connection()) {
		list($name, $mId, $email, $country, $campaignName, $campaignId, $totalFunding, $expectedPayout, $noMasterPayout, $bankName, $cType, $pType, $date, $images, $bankAccName, $bankAccNum, $status, $attachment) = $mysql->GetSingleInvestment($id);
		list($pAmount, $pMonth, $pStatus) = $mysql->GetSinglePayout($id);

		$imagesA = explode("~", $images);
		// print_r($imagesA);
		// echo "<br>";
		// print_r($pMonth);
		// echo "<br>";
		// print_r($pStatus);
		// exit();
	}
?>

<head>
	<?php include_once 'initialize.php'; ?>
	<title>Investment Payout</title>
	<?php include_once 'include.php'; ?>

	<style type="text/css">
		.title-line {
			position: relative;
			margin-bottom: 40px;
		}
		.title-line:after {
			content: ' ';
			width: 100px;
			height: 4px;
			background: #7f8c8d;
			position: absolute;
			bottom: -15px;
			left: 0;
		}
		.btn-status {
			width: 170px;
		}
	</style>
</head>
<body>
	<?php include_once 'header.php'; ?>

	<div class="main-content-area-wrapper container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="general-panel panel" style="margin-top: 30px;">
					<div class="container">
						<div class="row">
							<div class="col-lg-12">
								<h3 class="title-line">Investment Payout</h3>

								<form>
									<div class="row">
										<?php foreach ($pAmount as $key => $value): ?>
											<?php if ($key % 2): ?>
												<div class="col-sm-5">
											<?php else: ?>
												<div class="col-sm-5 col-sm-offset-1">
											<?php endif ?>
												<h4 style="margin-bottom: 15px;">Payout <?= $key ?></h4>
												<div class="row item" style="margin-bottom: 30px;">
													<div class="col-sm-6">
														<div class="form-group">
															<label for="file">File Upload</label>
															<input type="file" class="form-control" id="file" placeholder="Email">
														</div>
													</div>
													<div class="col-sm-6">
														<label>&nbsp;</label>
														<div class="form-group">
															<?php if ($imagesA[$key - 1]) { ?>
																	<div class="imageLink" style="display:inline-block">
																		<a target="_blank" href="../assets/images/payouts/<?= $imagesA[$key - 1] ?>">Proof of Payout</a>
																	</div>
																	<div class="deleteLink" id="<?= $key ?>" style="display: inline-block;color:red;margin-left:20px">Delete</div>
															 <?php } else { ?>
																	<div class="imageLink" style="display:inline-block"></div>
																	<div class="deleteLink" id="<?= $key ?>" style="display: none;color:red;margin-left:20px">Delete</div>
															 <?php } ?>
														 </div>
													</div>
													<div class="clearfix"></div>
													<div class="col-sm-6">
														<div class="form-group">
															<label for="amount">Amount</label>
															<input type="text" class="form-control" id="amount_<?= $key ?>" placeholder="0" value="<?= $value ?>">
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group">
															<label for="date">Date</label>
															<input type="date" class="form-control" id="date_<?= $key ?>">
														</div>
													</div>
													<div class="clearfix"></div>
													<div class="col-lg-12">
														<label style="display: block;">Status</label>
														<div class="btn-group btn-group-justified" role="group" aria-label="...">
															<div class="btn-group" role="group">
																<button type="button" class="btn <?= ($pStatus[$key] == 1) ? 'btn-warning':'btn-default' ?> change-status" data-value="1">On Going</button>
															</div>
															<div class="btn-group" role="group">
																<button type="button" class="btn <?= ($pStatus[$key] == 2) ? 'btn-warning':'btn-success' ?> change-status" data-value="2">Paid</button>
															</div>
															<div class="btn-group" role="group">
																<button type="button" class="btn <?= ($pStatus[$key] == 3) ? 'btn-warning':'btn-primary' ?> change-status" data-value="3">Paid KB E-Wallet</button>
															</div>
														</div>
														<input type="text" name="status_<?= $key ?>" id="status_<?= $key ?>">
														<!-- <div class="btn-group btn-group-justified" style="width: 100%;">
															<button class="btn btn-status btn-warning">On Going</button>
															<button class="btn btn-status btn-success">Paid</button>
															<button class="btn btn-status btn-info">Paid KB E-Wallet</button>
														</div> -->
													</div>
												</div>
											</div>
										<?php endforeach ?>
										<div class="col-sm-6">
											<h4 style="margin-bottom: 15px;">Payout 1</h4>
											<div class="row item" style="margin-bottom: 30px;">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="file">File Upload</label>
														<input type="file" class="form-control" id="file" placeholder="Email">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="amount">Amount</label>
														<input type="text" class="form-control" id="amount" placeholder="1500.52">
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="date">Date</label>
														<input type="date" class="form-control" id="date">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-12">
													<label style="display: block;">Status</label>
													<button class="btn btn-status btn-default">On Going</button>
													<button class="btn btn-status btn-success">Paid</button>
													<button class="btn btn-status btn-default">Paid KB E-Wallet</button>
												</div>
											</div>
										</div>
										<div class="col-sm-6">
											<h4 style="margin-bottom: 15px;">Payout 2</h4>
											<div class="row item" style="margin-bottom: 30px;">
												<div class="col-sm-8">
													<div class="form-group">
														<label for="file">File Upload</label>
														<input type="file" class="form-control" id="file" placeholder="Email">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="amount">Amount</label>
														<input type="text" class="form-control" id="amount" placeholder="1500.52">
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group">
														<label for="date">Date</label>
														<input type="date" class="form-control" id="date">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="col-lg-12">
													<label style="display: block;">Status</label>
													<button class="btn btn-status btn-warning">On Going</button>
													<button class="btn btn-status btn-default">Paid</button>
													<button class="btn btn-status btn-default">Paid KB E-Wallet</button>
												</div>
											</div>
										</div>
										<div class="col-sm-6"></div>
									</div>
									
									<div style="margin: 20px auto;text-align: center;">
										<button type="submit" class="btn btn-status btn-primary">Update Payout</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		$(document).ready(function() {

			$(".").click(function() {

			})

			$(".files").on("change", function () {
				 var dId = this.id;
				 var formData = new FormData();
				 var amount = $("#pAmount" + dId).val();
				 var totalAmount = '<?= $expectedPayout ?>';
				 var share = (amount / totalAmount) * 100;
				 var share1 = Math.round(share * 100) / 100;
				 formData.append("file" + dId, $('.files')[dId - 1].files[0]);
				 $.ajax({
						type: 'POST',
						url: 'posts.php?duty=updatePayout&iId=<?= $n ?>' + '&dId=' + dId + '&email=<?= $email ?>&name=<?= $firstname ?>' + '&amount=' + amount + '&cName=<?= $campaignName ?>&pType=<?= $pType ?>&tPayout=<?= $expectedPayout ?>&share=' + share1,
						data: formData,
						cache: false,
						contentType: false,
						processData: false,
						success: function (data) {
							 var reply = JSON.parse(data);
							 $("#popup").fadeIn(50).delay(3000).fadeOut(200);
							 $("#popupText").html(reply[0]);
							 $(".imageLink").eq(dId - 1).html("<a target='_blank' href='../assets/images/payouts/" + reply[1] + "'>Proof of Payout</a>");
							 $(".deleteLink").eq(dId - 1).css('display', 'inline-block');
							 $(".files").val("");
						},
						error: function (data) {
							 $("#popup").fadeIn(50).delay(1000).fadeOut(200);
							 $("#popupText").html(data);
						}
				 });

			});

			$(".deleteLink").on("click", function () {
				 var dId = this.id;
				 var r = confirm("Do you really need to delete?");
				 if (r == true) {
						$.post("posts.php", {job: 'removeinvimglink', iId: '<?= $n ?>', dId: dId}, function (reply) {
							 $("#popup").fadeIn(50).delay(2000).fadeOut(200);
							 $("#popupText").html(reply);
							 $(".imageLink").eq(dId - 1).html("");
							 $(".deleteLink").eq(dId - 1).hide();


						});
				 }
			});
		})
	</script>
</body>
</html>