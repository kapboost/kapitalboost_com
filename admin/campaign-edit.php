<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
$cId = $_GET["c"];

include_once 'PHPClasses/Image_GD_Functions.php';


if (isset($_POST["campaign_name"]) && isset($_POST["enabled"])) {
       $cNamef = $_POST["campaign_name"];
       $cSnippetf = $_POST["campaign_snippet"];
       $cSnippetIdf = $_POST["campaign_snippet_id"];
       $cDesf = $_POST["campaign_description"];
       $cDesIdf = $_POST["campaign_description_id"];
       $rDatef = $_POST["release_date"];
       $eDatef = $_POST["closing_date"];
       $enabledf = $_POST["enabled"];
       $classificationf = $_POST["classification"];
       $classificationIdf = $_POST["classification_id"];
       $countryf = $_POST["country"];
       $totalAmountf = $_POST["total_funding_amt"];
       $totalAmountIdf = $_POST["total_funding_amt_id"];
       $industryf = $_POST["industry"];
       $minAmtf = $_POST["minimum_investment"];
       $minAmtIdf = $_POST["minimum_investment_id"];
       $tenorf = $_POST["tenor"];
       $currentAmountf = $_POST["curr_funding_amt"];
       $cReturnf = $_POST["project_return"];
       $contractf = $_POST["contract"];
       $cTypef = $_POST["project_type"];
       $cSubTypef = $_POST["campaign_subtype"];
       $smeSubtype = $_POST["sme_subtype"];
       $cSubTypeShipf = $_POST["campaign_subtype_ship"];
       $privatePasswordf = $_POST["campaign_password"];
       $imageNamesf = $_POST["image_name"];
       $release_time = $_POST["release_time"];
       $acronim = $_POST["acronim"];

       $imageNamesff = "";
       for ($i = 0; $i < 18; $i++) {
              $imageNamesff .= $imageNamesf[$i] . "~";
       }


       if ($totalAmountIdf == "" or $totalAmountIdf == NULL) {
              $totalAmountIdf = 0;
       }
       if ($totalAmountf == "" || $totalAmountf == NULL) {
              $totalAmountf = 1;
       }
       if ($minAmtf == "" || $minAmtf == NULL) {
              $minAmtf = 1;
       }
       if ($currentAmountf == "" || $currentAmountf == NULL) {
              $currentAmountf = 0;
       }

       $riskf = $_POST["risk"];
       $relatedc1f = $_POST["relatedc1"];
       $relatedc2f = $_POST["relatedc2"];
       $relatedc3f = $_POST["relatedc3"];
       if ($mysql->Connection()) {
              $pdf_names = $mysql->GetCampaignPdfs($cId)[0];
       }
       $pdf_name = explode("~", $pdf_names);
       $pdfsf = "";
       $pdfsDesf = "";

       for ($i = 0; $i < 8; $i++) {
              if (isset($_FILES["pdf$i"]) && count($_FILES["pdf$i"]['error']) == 1 && $_FILES["pdf$i"]['error'][0] > 0) {
                     
              } else if (!is_uploaded_file($_FILES["pdf$i"]['tmp_name'])) {
                     
              } else if (isset($_FILES["pdf$i"])) {

                     $pdf[$i] = $_FILES["pdf$i"]["tmp_name"];
                     $pdf_name[$i] = str_replace(" ", "-", $cNamef) . $i;
                     move_uploaded_file($pdf[$i], "../assets/images/campaign-pdf/" . $pdf_name[$i]);
              }

              $pdfDes[$i] = $_POST["pdf_description$i"];
              $pdfsDesf .= $pdfDes[$i] . "~";
              $pdfsf .= $pdf_name[$i] . "~";
       }

       if ($mysql->Connection()) {
              $mysql->UpdateSingleCampaign($cId, $cNamef, $cSnippetf, $cSnippetIdf, $cDesf, $cDesIdf, $rDatef, $eDatef, $contractf, $enabledf, $classificationf, $classificationIdf, $countryf, $totalAmountf, $totalAmountIdf, $industryf, $minAmtf, $minAmtIdf, $tenorf, $currentAmountf, $cReturnf, $cTypef, $cSubTypef, $cSubTypeShipf, $privatePasswordf, $pdfsf, $pdfsDesf, $imageNamesff, $riskf, $relatedc1f, $relatedc2f, $relatedc3f, $smeSubtype, $release_time, $acronim);
       }
       $uploaddir = "../assets/images/campaign-content/";
       if (isset($_FILES["contentI1"])) {
              $file1 = $_FILES["contentI1"]["tmp_name"];
              $image_name1 = $_POST["contentI1h"];
              move_uploaded_file($file1, $uploaddir . $image_name1);
       }
       if (isset($_FILES["contentI2"])) {
              $file2 = $_FILES["contentI2"]["tmp_name"];
              $image_name2 = $_POST["contentI2h"];
              move_uploaded_file($file2, $uploaddir . $image_name2);
       }
       if (isset($_FILES["contentI3"])) {
              $file3 = $_FILES["contentI3"]["tmp_name"];
              $image_name3 = $_POST["contentI3h"];
              move_uploaded_file($file3, $uploaddir . $image_name3);
       }
}

if ($mysql->Connection()) {
       list($cName, $cSnippet, $cSnippetId, $cDes, $cDesId, $rDate, $eDate, $contract, $enabled, $classification, $classificationId, $country, $totalAmount, $totalAmountId, $industry, $minAmt, $minAmtId, $tenor, $currentAmount, $cReturn, $cType, $cSubType, $cSubTypeShip, $privatePassword, $relatedCampaign1, $relatedCampaign2, $relatedCampaign3, $pdfs, $pdfsDes, $imageNames, $i1, $i2, $i3, $i4, $i5, $i6, $i7, $i8, $i9, $i10, $i11, $i12, $i13, $i14, $i15, $i16, $i17, $i18, $cLogo, $cBackground, $risk, $relatedc1, $relatedc2, $relatedc3, $smeSubtype, $release_time, $acronim) = $mysql->GetSingleCampaignList($cId);
}
$is = [$i1, $i2, $i3, $i4, $i5, $i6, $i7, $i8, $i9, $i10, $i11, $i12, $i13, $i14, $i15, $i16, $i17, $i18];
$rDatee = date("d-m-Y", strtotime($rDate));
$eDatee = date("d-m-Y", strtotime($eDate));
$pdfsA = explode("~", $pdfs);
$pdfsDesA = explode("~", $pdfsDes);
$imageNamesA = explode("~", $imageNames);
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Edit <?= $cName ?></title>

              <?php include_once 'include.php'; ?>

              <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>

              <script>
                     $(document).ready(function () {
                            $(".DelImgBtn").on('click', function (event) {
                                   event.preventDefault();
                                   var deletingDiv = $(this).parent().parent().eq(0);
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dcampaignimage: "1", cid:<?= $cId ?>, iid: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          if (reply === "Done") {
                                                 event.preventDefault();
                                                 deletingDiv.remove();
                                          }
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {

                                          });
                                          $("#popupText").html(reply);
                                   });
                            });
                            $(".DelPdfBtn").on('click', function (event) {
                                   event.preventDefault();
                                   var deletingDiv = $(this).eq(0);
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dcampaignpdf: "1", cid:<?= $cId ?>, pdf: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          if (reply === "Done") {
                                                 deletingDiv.remove();
                                          }
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100);
                                          $("#popupText").html(reply);
                                   });
                            });
                            $("#IntBtn").on("click", function () {

                                   $("#indonesia").fadeOut(200);
                                   $("#international").delay(200).fadeIn(300);
                            });
                            $("#IdBtn").on('click', function () {
                                   $("#indonesia").delay(200).fadeIn(300);
                                   $("#international").fadeOut(200);
                            });
                     });
              </script>

              <style>
                     #indonesia{
                            display:none;
                     }
              </style>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>




              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;text-align:center">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel" style="">Edit Campaign - <?= $cName ?> </span>

                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">
                                                 <button id="IntBtn" class="btn btn-primary">International</button>
                                                 <button id="IdBtn" class="btn btn-danger">Indonesia</button>

                                                 <form action="campaign-edit.php?c=<?= $cId ?>" method="POST" enctype="multipart/form-data">
                                                        <div id="international">
                                                               <div  class="row">
                                                                      <h1>International</h1><br>
                                                                      <div class="col-xs-12 col-md-6">

                                                                             <div class="form-group">
                                                                                    <label>Campaign Kod : </label>
                                                                                    <input type="text" name="acronim" value="<?= $acronim ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Campaign Name : </label>
                                                                                    <input type="text" name="campaign_name" value="<?= $cName ?>" class="form-control">
                                                                             </div>


                                                                             <div class="form-group">
                                                                                    <label>Item to be purchased : </label>
                                                                                    <input type="text" name="classification" value="<?= $classification ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Project Type : </label><br/>
                                                                                    <select name="project_type" id="project_type" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                           <?php if ($cType == "sme") { ?>
                                                                                                  <option value="">-- Please Select --</option>
                                                                                                  <option value="sme" selected>SME Crowdfunding</option>
                                                                                                  <option value="donation">Donation</option>
                                                                                                  <option value="private">Private Crowdfunding</option>  
                                                                                           <?php } else if ($cType == "private") { ?>
                                                                                                  <option value="">-- Please Select --</option>
                                                                                                  <option value="sme">SME Crowdfunding</option>
                                                                                                  <option value="donation">Donation</option>
                                                                                                  <option value="private" selected>Private Crowdfunding</option>
                                                                                           <?php } else if ($cType == "donation") { ?>
                                                                                                  <option value="">-- Please Select --</option>
                                                                                                  <option value="sme">SME Crowdfunding</option>
                                                                                                  <option value="donation" selected>Donation</option>
                                                                                                  <option value="private">Private Crowdfunding</option>
                                                                                           <?php } else { ?>
                                                                                                  <option value="" selected>-- Please Select --</option>
                                                                                                  <option value="sme">SME Crowdfunding</option>
                                                                                                  <option value="donation">Donation</option>
                                                                                                  <option value="private">Private Crowdfunding</option>
                                                                                           <?php } ?>
                                                                                    </select>
                                                                             </div>
																			 <div class="form-group">
                                                                                    <label>SME Sub Type : </label><br/>
                                                                                    <select name="sme_subtype" id="sme_subtype" class="form-control" data-live-search="true" data-style="btn-default">
																						<option value="">-- Please Select --</option>
                                                                                        <option value="ASSET PURCHASE FINANCING" <?php if ($smeSubtype=="ASSET PURCHASE FINANCING") {echo "selected";} ?>>Asset Purchase Financing</option>
                                                                                        <option value="INVOICE  FINANCING" <?php if ($smeSubtype=="INVOICE  FINANCING") {echo "selected";} ?>>Invoice  Financing</option>   
                                                                                    </select>
                                                                             </div>
                                                                             <div class="form-group">
                                                                                    <div style="width:45%;display:inline-block">
                                                                                           <label>Donation Sub Type :</label>
                                                                                           <select name="campaign_subtype" id="campaign_subtype" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                                  <?php if ($cSubType == "reward") { ?>
                                                                                                         <option value="">-- Please Select --</option>
                                                                                                         <option value="reward" selected="">Rewards Base</option>
                                                                                                         <option value="interest">Interest-free Loan</option>
                                                                                                         <option value="hybrid">All</option>
                                                                                                  <?php } else if ($cSubType == "interest") { ?>
                                                                                                         <option value="">-- Please Select --</option>
                                                                                                         <option value="reward">Rewards Base</option>
                                                                                                         <option value="interest" selected="">Interest-free Loan</option>
                                                                                                         <option value="hybrid">All</option>
                                                                                                  <?php } else if ($cSubType == "hybrid") { ?>
                                                                                                         <option value="">-- Please Select --</option>
                                                                                                         <option value="reward">Rewards Base</option>
                                                                                                         <option value="interest">Interest-free Loan</option>
                                                                                                         <option value="hybrid" selected="">All</option>
                                                                                                  <?php } else { ?>
                                                                                                         <option value="" selected="">-- Please Select --</option>
                                                                                                         <option value="reward">Rewards Base</option>
                                                                                                         <option value="interest">Interest-free Loan</option>
                                                                                                         <option value="hybrid" >All</option>
                                                                                                  <?php } ?>
                                                                                           </select>
                                                                                    </div>
                                                                                    <div style="width:45%;display:inline-block">
                                                                                           <label>Shipping Rewards :</label>
                                                                                           <select name="campaign_subtype_ship" id="campaign_subtype_ship" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                                  <?php if ($cSubTypeShip == "1") { ?>
                                                                                                         <option value="0" >No (Default)</option>
                                                                                                         <option value="1" selected="">Yes</option>
                                                                                                  <?php } else { ?>
                                                                                                         <option value="0" selected="">No (Default)</option>
                                                                                                         <option value="1">Yes</option>
                                                                                                  <?php } ?>
                                                                                           </select>
                                                                                    </div>
                                                                             </div>



                                                                             <div class="form-group">
                                                                                    <label>Industry : </label>
                                                                                    <input type="text" name="industry" value="<?= $industry ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Auto Contract : </label>
                                                                                    <select name="contract" class="form-control">
                                                                                           <?php if ($contract == 0) { ?>
                                                                                                  <option value="0" selected="">OFF</option>
                                                                                                  <option value="1">ON</option>
                                                                                           <?php } else if ($contract == 1) { ?>
                                                                                                  <option value="0">OFF</option>
                                                                                                  <option value="1" selected="">ON</option>
                                                                                           <?php } ?>
                                                                                    </select>
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Risk : </label>
                                                                                    <select name="risk" value="Medium" class="form-control">
																								  <option value="N/A" <?php if ($risk == "N/A") {echo "selected=\"selected\""; }?>>N/A</option>
                                                                                                  <option value="A" <?php if ($risk == "A") {echo "selected=\"selected\""; }?>>A</option>
                                                                                                  <option value="A-" <?php if ($risk == "A-") {echo "selected=\"selected\""; }?>>A-</option>
                                                                                                  <option value="B+" <?php if ($risk == "B+") {echo "selected=\"selected\""; }?>>B+</option>
                                                                                                  <option value="B" <?php if ($risk == "B") {echo "selected=\"selected\""; }?>>B</option>
                                                                                                  <option value="B-" <?php if ($risk == "B-") {echo "selected=\"selected\""; }?>>B-</option>
                                                                                                  <option value="C+" <?php if ($risk == "C+") {echo "selected=\"selected\""; }?>>C+</option>
                                                                                                  <option value="C" <?php if ($risk == "C") {echo "selected=\"selected\""; }?>>C</option>
                                                                                           
                                                                                    </select>
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Country : </label>
                                                                                    <select id="countries" name="country" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                           <option value="See Campaign">See Campaign</option>
                                                                                    </select>
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Release Date : </label>
                                                                                    <input type="text" name="release_date" id="datepicker" value="<?= $rDatee ?>" required>
                                                                             </div>
																			 
																			 <div class="form-group">
                                                                                    <label>Release Time : </label>
                                                                                    <input type="text" name="release_time" id="timepicker" value="<?= $release_time ?>" required>
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Closing Date : </label>
                                                                                    <input type="text" name="closing_date" id="datepicker" value="<?= $eDatee ?>" required>
                                                                             </div>

                                                                             <div>
                                                                                    <label>Campaign Snippet</label>
                                                                                    <textarea maxlength="130" name="campaign_snippet" style="width:100%"><?= $cSnippet ?></textarea>
                                                                             </div>

                                                                      </div>

                                                                      <div class="col-xs-12 col-md-6">
                                                                             <div class="form-group">
                                                                                    <label>Total Funding AMT : </label>
                                                                                    <input type="number" name="total_funding_amt" value="<?= $totalAmount ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Minimum Investment : </label>
                                                                                    <input type="number" name="minimum_investment" value="<?= $minAmt ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Tenor : </label>
                                                                                    <input type="text" name="tenor" value="<?= $tenor ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Curr Funding AMT : </label>
                                                                                    <input type="number" name="curr_funding_amt" value="<?= $currentAmount ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Project Return : </label>
                                                                                    <input type="text" name="project_return" value="<?= $cReturn ?>" class="form-control">
                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Campaign Password : </label>
                                                                                    <input type="text" name="campaign_password" value="<?= $privatePassword ?>" class="form-control">
                                                                             </div>

                                                                             <div style="min-height: 180px;">

                                                                                    <div class="form-group">
                                                                                           <label>Campaign Logo : </label>
                                                                                           <input type="file" name="campaign_logo" >
                                                                                           <?php if ($cLogo != "") { ?>
                                                                                                  <br />
                                                                                                  <div class="graph-wrapper" style="margin-left:-40px;">
                                                                                                         <ul class="image_wrapper">
                                                                                                                <li>
                                                                                                                       <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/<?= $cLogo ?>" class="image-link" title="{$campaign_logo}"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/<?= $cLogo ?>&w=150" alt=""></a><br>
                                                                                                                </li>
                                                                                                         </ul>
                                                                                                  </div>
                                                                                                  <div class="clear"></div>
                                                                                           <?php } ?>
                                                                                    </div>

                                                                                    <div class="form-group">
                                                                                           <label>Campaign Background : </label>
                                                                                           <input type="file" name="campaign_background" >
                                                                                           <?php if ($cBackground != "") { ?>
                                                                                                  <br />
                                                                                                  <div class="graph-wrapper" style="margin-left:-40px;">
                                                                                                         <ul class="image_wrapper">
                                                                                                                <li>
                                                                                                                       <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/<?= $cBackground ?>" class="image-link" title="<?= $cBackground ?>"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/<?= $cBackground ?>&w=150" alt=""></a><br>
                                                                                                                </li>
                                                                                                         </ul>
                                                                                                  </div>
                                                                                                  <div class="clear"></div>
                                                                                           <?php } ?>
                                                                                    </div>
                                                                             </div>
                                                                      </div>

                                                               </div>

                                                               <div class="row">
                                                                      <div class="col-xs-12 col-md-12">
                                                                             <div class="form-group">
                                                                                    <label>Campaign Description : </label> 
																					<a href="live-campaign.php?c=<?php echo $cId; ?>" target="_blank" class="btn btn-primary">Front Editor</a>
																					<br /><br />
                                                                                    <textarea name="campaign_description" class="form-control"><?= $cDes ?></textarea>
                                                                             </div>
                                                                      </div>
                                                               </div>
                                                               <style>
                                                                      .divs{
                                                                             text-align: center;margin: 20px;background-color: #e6e6e6;border-radius: 10px;
                                                                             padding:20px 0
                                                                      }
                                                               </style>
                                                               <div class="row divs" style="">
                                                                      <h3>Content Images</h3>
                                                                      <div class="col-md-4 col-lg-4">
                                                                             <input id='contentI1' name="contentI1" type="file" />
                                                                             <input id='contentI1h' name='contentI1h' type="hidden" value="" />
                                                                             <p id='contentI1p' style='text-align: left'></p>
                                                                      </div>
                                                                      <div class="col-md-4 col-lg-4">
                                                                             <input id='contentI2' name='contentI2' type="file" />
                                                                             <input id='contentI2h' name='contentI2h' type="hidden" value="" />
                                                                             <p id='contentI2p' style='text-align: left'></p>
                                                                      </div>
                                                                      <div class="col-md-4 col-lg-4">
                                                                             <input id='contentI3' name='contentI3' type="file" />
                                                                             <input id='contentI3h'  name='contentI3h'type='hidden' value="" />
                                                                             <p id='contentI3p' style="text-align: left"></p>
                                                                      </div>

                                                               </div>
                                                               <style>
                                                                      .delImgLi{
                                                                             margin:0 !important;padding:0 !important;border:none !important;margin: auto
                                                                      }
                                                                      .DelPdfBtn{
                                                                             margin-left: 15px;font-size:0.8em !important;padding:3px !important
                                                                      }
                                                               </style>
                                                               <div class="row divs" style="text-align:left" >
                                                                      <h3 style="text-align:center">PDFs</h3>
                                                                      <?php for ($i = 0; $i < 8; $i++) { ?>
                                                                             <div class="col-md3 col-lg-3 col-xs-3">
                                                                                    <p>pdf <?= $i + 1 ?></p>
                                                                                    <input id='pdf_description<?= $i ?>' name='pdf_description<?= $i ?>' placeholder="PDF Name" value="<?= $pdfsDesA[$i] ?>" />
                                                                                    <input id='pdf<?= $i ?>' name='pdf<?= $i ?>' type='file'/>
                                                                                    <?php if ($pdfsA[$i] != "") { ?>
                                                                                           <a href="../assets/images/campaign-pdf/<?= $pdfsA[$i] ?>" target="_blank">PDF Link</a>
                                                                                           <button class="btn btn-danger DelPdfBtn" type="button" value="<?= $i ?>">Delete</button>
                                                                                    <?php } ?>
                                                                             </div>
                                                                      <?php } ?>
                                                               </div>



                                                               <div class="row divs">
                                                                      <h3>Gallery Images</h3>

                                                                      <?php for ($i = 0; $i < 18; $i++) { ?>
                                                                             <div class="col-md-4 col-lg-4 col-xs-4">
                                                                                    <div class="form-group">
                                                                                           <label>Image <?= $i + 1 ?> : </label><br>
                                                                                           <input type='text' name='image_name[]' value="<?= $imageNamesA[$i] ?>" placeholder="Image Name"/>
                                                                                           <input type="file" name="image_<?= $i + 1 ?>" >
                                                                                           <?php if ($is[$i] != "") { ?>
                                                                                                  <br />
                                                                                                  <div class="graph-wrapper">
                                                                                                         <ul class="image_wrapper">
                                                                                                                <li>
                                                                                                                       <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/<?= $is[$i] ?>" class="image-link" title="<?= $is[$i] ?>"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/campaign/<?= $is[$i] ?>&w=150" alt=""></a><br>
                                                                                                                </li>
                                                                                                                <li class="delImgLi">
                                                                                                                       <button class="btn btn-danger DelImgBtn" type="button" value="<?= $i + 1 ?>">Delete</button>
                                                                                                                </li>
                                                                                                         </ul>
                                                                                                  </div>
                                                                                                  <div class="clear"></div>
                                                                                           <?php } ?>
                                                                                    </div>
                                                                             </div>

                                                                      <?php } ?>
                                                               </div>



                                                               <div class="row divs" > 
                                                                      <h3>Related Campaigns</h3>
                                                                      <div class="col-md-4 col-lg-4">
                                                                             <h4>Related Campaign 1</h4>
                                                                             <select class="form-control" id="relatedc1" name="relatedc1"></select>
                                                                      </div>
                                                                      <div class="col-md-4 col-lg-4">
                                                                             <h4>Related Campaign 2</h4>
                                                                             <select class="form-control"  id="relatedc2" name="relatedc2"></select>
                                                                      </div>
                                                                      <div class="col-md-4 col-lg-4">
                                                                             <h4>Related Campaign 3</h4>
                                                                             <select class="form-control" id="relatedc3" name="relatedc3"></select>
                                                                      </div>

                                                               </div>


                                                        </div>

                                                        <div id="indonesia">
                                                               <h1 >Indonesia</h1><br>

                                                               <div class="row">
                                                                      <div class="col-md-6 col-lg-6 col-xs-12">
                                                                             <div class="form-group">
                                                                                    <label>Total Funding AMT IDR : </label>
                                                                                    <input type="number" name="total_funding_amt_id" value="<?= $totalAmountId ?>" class="form-control" />
                                                                             </div>


                                                                             <div class="form-group">
                                                                                    <label>Minimum Investment Indonesia : </label>
                                                                                    <input type="number" name="minimum_investment_id" value="<?= $minAmtId ?>" class="form-control">
                                                                             </div>

                                                                      </div>
                                                                      <div class="col-md-6 col-lg-6 col-xs-12">
                                                                             <div>
                                                                                    <label>Campaign Snippet in Indonesian</label>
                                                                                    <textarea maxlength="130" name="campaign_snippet_id" style="width:100%"><?= $cSnippetId ?></textarea>
                                                                             </div>
                                                                      </div>
                                                                      <div class="col-md-6 col-lg-6 col-xs-12">
                                                                             <div class="form-group">
                                                                                    <label>Item to be purchased in Indonesian : </label>
                                                                                    <input type="text" name="classification_id" value="<?= $classificationId ?>" class="form-control">
                                                                             </div>
                                                                      </div>
                                                               </div>
                                                               <div class="row">
                                                                      <div class="col-xs-12 col-md-12">
                                                                             <div class="form-group">
                                                                                    <label>Campaign Description in Indonesian : </label>
                                                                                    <textarea name="campaign_description_id" class="form-control"><?= $cDesId ?></textarea>
                                                                             </div>
                                                                      </div>
                                                               </div>



                                                        </div>
                                                        <div class="row">

                                                               <div class="col-lg-12">
                                                                      <div class="form-group">
                                                                             <label>Status : </label>
                                                                             <select name="enabled">
                                                                                    <?php if ($enabled == 0) { ?>
                                                                                           <option value="1" >Online</option>
                                                                                           <option value ="0" selected>Offline</option>
                                                                                    <?php } else { ?>
                                                                                           <option value="1" selected="" >Online</option>
                                                                                           <option value ="0" >Offline</option>                                            
                                                                                    <?php } ?>
                                                                             </select>
                                                                      </div>
                                                                      <div>
                                                                             <a href="https://kapitalboost.com/campaign/view/<?= $cId ?>?preview=1" target="_blank">
																			 <?php 
																					echo "Preview This Campaign on English version";
																			 ?>
																			
																			 
																			 </a>
                                                                      </div>
																	  <br />
                                                                      <div>
                                                                             <a href="https://kapitalboost.co.id/campaign/view/<?= $cId ?>?preview=1" target="_blank">
																			 <?php 
																					echo "Preview This Campaign on Bahasa version";
																			 ?>
																			
																			 
																			 </a>
                                                                      </div>
																	  <br />
                                                                      <div>
                                                                             <a href="https://kapitalboost.com/campaign/view/<?= $cId ?>?private=1" target="_blank">
																			 <?php 
																					echo "Private Link";
																			 ?>
																			
																			 
																			 </a>
                                                                      </div>
                                                                      <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Submit" style="margin-left:15px;margin-top:10px;"/>
                                                                      <input name="button" type="reset" class="btn btn-danger" id="button" value="Reset To Previous Data" style="margin-left:5px;margin-top:10px;"/>
                                                               </div>
                                                        </div>
                                                 </form>

                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


              <script>CKEDITOR.replace('campaign_description', {});
                     CKEDITOR.replace('campaign_description_id', {});
              </script>

              <script src="js/pickadate/picker.js"></script>
              <script src="js/pickadate/picker.date.js"></script>
              <script src="js/pickadate/picker.time.js"></script>
              <script src="js/pickadate/legacy.js"></script>

              <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css"> 
              <script src="js/magnific-popup/jquery.magnific-popup.js"></script>

              <script type="text/javascript">
                     $(document).ready(function () {
                            $.post("countries.php", {selected: '<?= $country ?>'}, function (reply) {
                                   reply = JSON.parse(reply);
                                   $("#countries").append(reply);
                            });

                            $.post("<?php echo ADMIN_PATH ?>/posts.php", {job: "getcampaignnames"}, function (data) {
                                   data = JSON.parse(data);
                                   var selected1 = "<?= $relatedc1 ?>";
                                   var selected2 = "<?= $relatedc2 ?>";
                                   var selected3 = "<?= $relatedc3 ?>";

                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          if (data[i] === selected1) {
                                                 x.setAttribute("selected", "true");
                                          }
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("relatedc1").appendChild(x);
                                   }
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          if (data[i] === selected2) {
                                                 x.setAttribute("selected", "true");
                                          }
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("relatedc2").appendChild(x);
                                   }
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          if (data[i] === selected3) {
                                                 x.setAttribute("selected", "true");
                                          }
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("relatedc3").appendChild(x);
                                   }
                            });

                            $('.selectpicker').selectpicker();


                            $('#datepicker,#datepicker2').pickadate({
                                   format: 'dd-mm-yyyy'
                            });
							$('#timepicker').pickatime({
								format: 'HH:i',
								interval: 5
							});

                            $('.image-link').magnificPopup({type: 'image'});


                            $("#button").click(function () {
                                   location.reload();
                            });

                            $("#contentI1").on('click', function () {
                                   var timeNow = Math.floor(Date.now() / 1000);
                                   $("#contentI1h").val(timeNow);
                                   $("#contentI1p").html("https://kapitalboost.com/assets/images/campaign-content/" + timeNow);
                            });
                            $("#contentI2").on('click', function () {
                                   var timeNow = Math.floor(Date.now() / 1000);
                                   $("#contentI2h").val(timeNow);
                                   $("#contentI2p").html("https://kapitalboost.com/assets/images/campaign-content/" + timeNow);
                            });
                            $("#contentI3").on('click', function () {
                                   var timeNow = Math.floor(Date.now() / 1000);
                                   $("#contentI3h").val(timeNow);
                                   $("#contentI3p").html("https://kapitalboost.com/assets/images/campaign-content/" + timeNow);
                            });


                     });
              </script>

       </body>


</html> 