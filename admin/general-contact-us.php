<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($pageContents, $releaseDate) = $mysql->GetGeneralContactus();
       $pageContentsA = explode("~~~", $pageContents);
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KapitalBoost Admin</title>

              <?php include_once 'include.php'; ?>

              <script>
                     var headers = "";
                     var contents = "";
                     $(document).ready(function () {
                            $("#AddBtn").on('click', function () {
                                   var contentInput = document.createElement("INPUT");
                                   contentInput.setAttribute("class","contents");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea");
                                   mainDiv.appendChild(contentDiv);
                                   
                            });

                            $("#UpdateBtn").on('click', function () {
                                   $(".contents").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });
                                   contents.slice(0, -3);

                                   $.post("posts.php", {job: "updategeneralcontactus", contents: contents}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(2000).fadeOut(100);
                                          $("#popupText").html(reply);
                                          contents = "";
                                   });

                            });
                     });
              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Contact us</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body" style="text-align:center">



                                                 <style>
                                                       
                                                        .contents{
                                                               width:  95%;
                                                               min-height: 50px;margin:15px;padding:10px;border-radius: 10px;font-size: 1.1em;background-color: #d7e6f4
                                                        }
                                                        #AddBtn{
                                                               background-color: white;border: none;display: inline-block
                                                        }

                                                 </style>
                                                 <div id="contentArea">
                                                        <?php for ($i = 0; $i < count($pageContentsA) - 1; $i++) { ?>

                                                               <div><textarea class="contents" ><?= $pageContentsA[$i] ?></textarea></div>

                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>

                                                 <button class="btn btn-primary" disabled="" id="UpdateBtn" style="margin-bottom: 20px">Update</button>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>