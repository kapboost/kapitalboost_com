<?php
include_once 'security.php';
include_once 'mysql.php';
$mysql = new mysql();
$bId = test_input($_GET["b"]);

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function slug_formatter($slug)
{
    $formatted_slug = str_replace(" ", "-", $slug);
    $formatted_slug = str_replace("amp", "", $slug);
    $formatted_slug = preg_replace("/[^A-Za-z0-9\-]/", "", $formatted_slug);
    $formatted_slug = preg_replace("/\-+/", "-", $formatted_slug);
    $formatted_slug = strtolower($formatted_slug);
    return $formatted_slug;
}

// if (isset($_POST)) {
if (isset($_POST["Submit"])) {
    $name = test_input($_POST['name']);
    $name = strtolower($name);
    $slug = test_input($_POST['slug']);
    $description = test_input($_POST['description']);
    $slugf = slug_formatter($slug);
    if ($mysql->Connection()) {
        $mysql->createCategory($name, $slugf, $description);
        header('Location: '.'category-list.php');
//        echo '<script type="text/javascript">
//        window.location = "category-list.php"; </script>';
    }


}
if ($date == "" or $date == null) {
    $date = date("Y-m-d");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'initialize.php'; ?>
    <title>Category List - <?= $title ?></title>
    <?php include_once 'include.php'; ?>
    <script>
        //
    </script>

</head>
<body>
<?php include_once 'header.php'; ?>
<?php include_once 'popup.php'; ?>
<div class="main-content-area-wrapper container-fluid" style="min-height: 800px;">
    <div class="row">
        <div class="col-xs-12">
            <div class="general-panel panel">
                <div class="blue-panel-heading panel-heading">
                    <span class="header-panel">Category List - <?= $title ?></span>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body" style="text-align: center">
                    <h1>Create a new Category</h1>
                    <form class="" action="" method="POST">
                        <div class="form-group">
                            <label class="require">Category Name</label>
                            <input class="form-control" type="text" name="name" id="name" required>
                        </div>
                        <div class="form-group">
                            <label class="require">Category Slug</label>
                            <input class="form-control" type="text" name="slug" id="slug" required>
                        </div>
                        <div class="form-group">
                            <label class="require">Category Description</label>
                            <input class="form-control" type="text" name="description" id="description" required>
                        </div>
                        <input class="btn btn-info" type="submit" name="Submit" value="Save">
                    </form>
                </div>
            </div>
        </div>
        <script src="js/pickadate/picker.js"></script>
        <script src="js/pickadate/picker.date.js"></script>
        <script src="js/pickadate/legacy.js"></script>
        <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css">
        <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
        <script>
            $('#date').pickadate({
                format: 'yyyy-mm-dd'
            });
            $(document).ready(function () {
                $('.image-link').magnificPopup({type: 'image'});
            });
        </script>
        <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>
        <script>CKEDITOR.replace('content', {});
        </script>
</body>
</html>
