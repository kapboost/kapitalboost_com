<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

if (isset($_POST["member_username"])) {
       $mUserName = $_POST["member_username"];
       $mFirstname = $_POST["member_firstname"];
       $mLastname = $_POST["member_lastname"];
       $mPassword = $_POST["member_password"];
       $mEmail = $_POST["member_email"];
       $mNric = $_POST["nric"];
       $mDob = $_POST["dob"];
       $mAddress = $_POST["address"];
       $mPhone = $_POST["member_mobile"];
       $mCountry = $_POST["member_country"];
       $mNationality = $_POST["member_nationality"];
       $cke1 = $_POST["check_1"];
       $cke2 = $_POST["check_2"];
       $cke3 = $_POST["check_3"];
       $cke4 = $_POST["check_4"];
       $cke5 = $_POST["check_5"];
       $cke6 = $_POST["check_6"];
       $cke7 = $_POST["check_7"];
       $cke8 = $_POST["check_8"];
       $cke9 = $_POST["check_9"];
       $cke10 = $_POST["check_10"];
       $cke11 = $_POST["check_11"];
       $cke12 = $_POST["check_12"];

       if ($mysql->Connection()) {
              $mId = $mysql->AddNewMember($mUserName, $mEmail, $mFirstname, $mLastname, $mPassword, $mNric, $mDob, $mAddress, $mPhone, $mNationality, $mCountry, $cke1, $cke2, $cke3, $cke4, $cke5, $cke6, $cke7, $cke8, $cke9, $cke10, $cke11, $cke12);
       }

       if (isset($_FILES["fileToUpload"]) && count($_FILES['fileToUpload']['error']) == 1 && $_FILES['fileToUpload']['error'][0] > 0) {
              
       } else if (!is_uploaded_file($_FILES['fileToUpload']['tmp_name'])) {
              
       } else if (isset($_FILES['fileToUpload'])) {
              $file = $_FILES["fileToUpload"]["tmp_name"];
              $filename = $_FILES['fileToUpload']['name'];
              $ext = pathinfo($filename, PATHINFO_EXTENSION);
              $uploaddir .= "../assets/nric/datanric/";
              $image_name = time() . "." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name);
              if ($mysql->Connection()) {
                     $mysql->UpdateUploadedMember($mId, $image_name);
              }
       }
       header("Location: member-edit.php?m=$mId");
       
}

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
<?php include_once 'initialize.php'; ?>

              <title>KB AdminAdd New Member</title>

<?php include_once 'include.php'; ?>

              <script>

                     $(document).ready(function () {
                            var nationality = "<?= $mNationality ?>";
                            var country = "<?= $mCountry ?>";
                            $.post("posts.php", {job: 'getallnationalities'}, function (data) {
                                   data = JSON.parse(data);
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("nationality").appendChild(x);
                                   }
                            });
                            $.post("posts.php", {job: 'getallcountriessimple'}, function (data) {
                                   data = JSON.parse(data);
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("country").appendChild(x);
                                   }
                            });
                     });
              </script>

       </head>
       <body>
<?php include_once 'header.php'; ?>
<?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Add New Member</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body" style="text-align: center">


                                                 <form action="" method="POST" enctype="multipart/form-data">

                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>User Name : </label>
                                                                      <input  type="text" name="member_username" class="form-control">
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>First Name : </label>
                                                                      <input type="text" name="member_firstname" class="form-control" >
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Last Name : </label>
                                                                      <input type="text" name="member_lastname" class="form-control" >
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Email : </label>
                                                                      <input type="text" name="member_email"  class="form-control">
                                                               </div>


                                                               <div class="form-group">
                                                                      <label>Password : </label>
                                                                      <input type="password" name="member_password"   class="form-control">
                                                               </div>
                                                        </div>
                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>NRIC/Pasport No : </label>
                                                                      <input type="text" name="nric" value="<?= $mNric ?>" class="form-control" >


                                                               </div>
                                                               <div class="form-group">
                                                                      <label>DOB: </label>
                                                                      <input type="text" id="dob" name="dob" class="form-control" >
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Residential Address  : </label>
                                                                      <input type="text" name="address"  class="form-control" >
                                                               </div>
                                                               <div class="form-group">
                                                                      <label>Handphone Number  : </label>
                                                                      <input type="text" name="member_mobile"class="form-control" >
                                                               </div>
                                                               <div class="form-group">
                                                                      <label>Country : </label>
                                                                      <select name="member_country" id="country" class="cat_dropdown" class="form-control" >
                                                                             <option value="">----------</option>
                                                                      </select>
                                                               </div>
                                                               <div class="form-group">
                                                                      <label>Nationality : </label>
                                                                      <select name="member_nationality" id="nationality" class="cat_dropdown" class="form-control">
                                                                             <option value="">----------</option>
                                                                      </select>
                                                               </div>
                                                        </div>
                                                        <div class="form-group">

                                                               <label for="CAT_Custom_20043780_157746">Upload copy of NRIC</label>
                                                               <br />
                                                               <input type="file" name="fileToUpload" style="margin: auto" />
                                                        </div>

                                                        <div class="invest-form-layer" style="margin-bottom: 30px;margin-top: 25px;">

                                                               <input class="ver-inv" type="hidden" value="" />
                                                               <label class="labelIn2">Which of these financial products have the member invested in?</label>
                                                               <div class="checkIn">

                                                                      <input type="checkbox" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks"/> Stocks

                                                                      <input type="checkbox" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" />Bonds

                                                                      <input type="checkbox" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" />Mutual funds

                                                                      <input type="checkbox" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" /> Alternative investments (specify)

                                                                      <input type="checkbox" name="check_5" id="CAT_Custom_20047261_157746_4" value="None of the above" /> None of the above

                                                               </div>
                                                        </div>
                                                        <div class="" style="padding-right: 0;padding-left: 0;width: 97%;margin-top: 25px;">

                                                               <div class="invest-form-layer" style="margin-bottom: 30px;padding-right: 0;padding-left: 0;width: 100%;margin-top: 25px;">
                                                                      <input class="ver-inv" type="hidden" value="" />
                                                                      <label class="labelIn2">Member's interest in investing through Kapital Boost</label>
                                                                      <div class="checkIn">

                                                                             <input type="checkbox" name="check_6" id="CAT_Custom_20043779_157746_0" value="Attractive returns" /> Attractive returns

                                                                             <input type="checkbox" name="check_7" id="CAT_Custom_20043779_157746_1" value="Short tenor" /> Short tenor

                                                                             <input type="checkbox" name="check_8" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" /> Social/ethical reasons

                                                                             <input type="checkbox" name="check_9" id="CAT_Custom_20043779_157746_3" value="Investment diversification"/> Investment diversification

                                                                             <input type="checkbox" name="check_10" id="CAT_Custom_20043779_157746_4" value="Transparency"/> Transparency

                                                                             <input type="checkbox" name="check_11" id="CAT_Custom_20043779_157746_5" value="Others" /> Others
                                                                             <br />
                                                                             <br />
                                                                             <div class="invest-form-layer" style="margin-bottom: 30px;width: 97.5%;">
                                                                                    <input style="float: left;" type="checkbox" name="check_12" id="CAT_Custom_20043781_157746_0" value="yes" checked />
                                                                                    <label style="margin-left: 10px;float: left;max-width: 95%;text-align: left;" class="labelIn3">The member agree to Kapital Boost’s <font style="color: #609edf;">Terms of Use</font> and <font style="color: #609edf;"> Privacy Policy </font>, and have read understand the <font style="color: #609edf;">Risk Statement</font>.</label>
                                                                             </div>

                                                                      </div>

                                                               </div>


                                                               <input name="button" type="submit" class="btn btn-primary" id="button" value="Update" />


                                                        </div>

                                                 </form>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


              <script src="js/pickadate/picker.js"></script>
              <script src="js/pickadate/picker.date.js"></script>
              <script src="js/pickadate/legacy.js"></script>
              <script>

                     $('#dob').pickadate({
                            format: 'yyyy-mm-dd',
                            selectMonths: true,
                            selectYears: true
                     });

              </script>


       </body>


</html>