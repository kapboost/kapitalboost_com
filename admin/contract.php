<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();


$cId = $_GET["c"];


if ($mysql->Connection()) {
   // $cName = $mysql->GetSingleCampaignName($cId);
   list($cName, $cSnippet, $cSnippetId, $cDes, $cDesId, $rDate, $eDate, $contract, $enabled, $classification, $classificationId, $country, $totalAmount, $totalAmountId, $industry, $minAmt, $minAmtId, $tenor, $currentAmount, $cReturn, $cType, $cSubType, $cSubTypeShip, $privatePassword, $relatedCampaign1, $relatedCampaign2, $relatedCampaign3, $pdfs, $pdfsDes, $imageNames, $i1, $i2, $i3, $i4, $i5, $i6, $i7, $i8, $i9, $i10, $i11, $i12, $i13, $i14, $i15, $i16, $i17, $i18, $cLogo, $cBackground, $risk, $relatedc1, $relatedc2, $relatedc3, $smeSubtype, $release_time, $acronim) = $mysql->GetSingleCampaignList($cId);
   // list($conType, $comName, $comReg, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName,$dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs, $invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost, $admin_fee_pct, $admin_fee_amt) = $mysql->GetSingleCampaignContractList($cId);
   list($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) = $mysql->GetSingleCampaignContractList($cId);
}
?>
<?php
    ini_set('display_errors', '1');
    ini_set('error_reporting', E_ALL);
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>
      <title>KB Admin Contracts</title>
      <?php include_once 'include.php'; ?>
   </head>
   <body>

      <script>

          $(document).ready(function () {

			  $(".DelPdfBtn").on('click', function (event) {
					   event.preventDefault();
					   var deletingDiv = $(this).parent().eq(0);
					   var person = prompt("Please enter Master Code");
					   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
							  reply = JSON.parse(reply);
							  if (reply === "Done") {
									 deletingDiv.remove();
							  }
							  $("#popup").fadeIn(100).delay(500).fadeOut(100);
							  $("#popupText").html(reply);
					   });
				});

             $("#image1").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage1&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv1").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='1'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image2").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage2&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv2").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='2'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });


             $("#image3").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage3&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv3").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='3'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });


             $("#image4").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage4&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv4").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='4'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });


             $("#image5").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage5&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv5").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='5'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image6").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage6&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv6").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='6'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image7").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage7&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv7").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='7'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image8").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage8&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv8").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='8'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image9").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage9&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv9").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='9'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image10").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage10&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv10").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='10'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});
                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });

             $("#image11").on("change", function () {
                var formData = new FormData();

                formData.append("file", $(this)[0].files[0]);
                $.ajax({
                   type: 'POST',
                   url: 'posts.php?duty=updatecontractimage11&cId=<?= $cId ?>',
                   data: formData,
                   cache: false,
                   contentType: false,
                   processData: false,
                   success: function (data) {
                      var reply = JSON.parse(data);
                      $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                      if (reply[0] == true) {
                         $("#popupText").html("Uploaded");
                      } else {
                         $("#popupText").html("Failed!");
                      }
                      $("#imgDiv11").html("<img src='../assets/images/pdfimg/" + reply[1] + "' width='80%'><br><br><button class='btn btn-danger DelPdfBtn' type='button' value='11'>Delete</button>");
					  $(".DelPdfBtn").on('click', function (event) {
							   event.preventDefault();
							   var deletingDiv = $(this).parent().eq(0);
							   var person = prompt("Please enter Master Code");
							   $.post("delete.php", {dcontractimage: "1", cid:<?= $cId ?>, conimage: $(this).val(), MC: person}, function (reply) {
									  reply = JSON.parse(reply);
									  if (reply === "Done") {
											 deletingDiv.remove();
									  }
									  $("#popup").fadeIn(100).delay(500).fadeOut(100);
									  $("#popupText").html(reply);
							   });
						});

                   },
                   error: function (data) {
                      $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                      $("#popupText").html(data);
                   }
                });
             });





             $("#save").on("click", function (event) {
                event.preventDefault();

                var cId = '<?= $cId ?>';
                var conType = $("#conType").val();
                var comName = $("#comName").val();
                var comReg = $("#comReg").val();
               //  var comRegIna = $("#comRegIna").val();
                var camReturn = $("#camReturn").val();
                var purchaseAsset = $("#purchaseAsset").val();
                var assetCost = $("#assetCost").val();
                var salePrice = $("#salePrice").val();
                var assetCostCurr = $("#assetCostCurr").val();
                var exchange = $("#exchange").val();
                // var profit = $("#profit").val();
                var profit = 0;
                var foreignCurr = $("#foreignCurrency").val();
                var dirTitle = $("#dirTitle").val();
                var dirName = $("#dirName").val();
                var dirEmail = $("#dirEmail").val();
                // var dirIc = $("#dirIc").val();
                // var comNameId = $("#comNameId").val();
                var comRegId = $("#comRegId").val();
                var purchaseAssetId = $("#purchaseAssetId").val();
                var closdate = $("#closdate").val();
                var invoContractNo = $("#invoContractNo").val();
                var invoAktaNo = $("#invoAktaNo").val();
                var invoFeePaymentDtae = $("#invoFeePaymentDtae").val();
                var invoAgencyFee = $("#invoAgencyFee").val();
                var invoMaturityDate = $("#invoMaturityDate").val();
                var invoRepaymentDate = $("#invoRepaymentDate").val();
                var invoSubsAgencyFee = $("#invoSubsAgencyFee").val();
                var invoDocs = $("#invoDocs").val();
                var invoDocsId = $("#invoDocsId").val();
                var invoAktaNoId = $("#invoAktaNoId").val();
                var invoAssetCostSGD = $("#invoAssetCostSGD").val();
                var invoAssetCost = $("#invoAssetCost").val();
                // var admin_fee_pct = $("#admin_fee_pct").val();
                // var admin_fee_amt = $("#admin_fee_amt").val();

                var dirIcId = $("#dirIcId").val();

                $.post("posts.php", {job: 'updatecontract', cId: cId, conType: conType, comName: comName, comReg: comReg, purchaseAsset: purchaseAsset, assetCost: assetCost, salePrice: salePrice,
                   assetCostCurr: assetCostCurr, exchange: exchange, profit: profit, camReturn: camReturn, foreignCurr: foreignCurr, dirTitle: dirTitle,dirEmail:dirEmail, dirName: dirName,
                   comRegId: comRegId, purchaseAssetId: purchaseAssetId,  dirIcId: dirIcId, closdate: closdate, invoContractNo: invoContractNo, invoAktaNo: invoAktaNo, invoFeePaymentDtae: invoFeePaymentDtae, invoAgencyFee: invoAgencyFee, invoMaturityDate: invoMaturityDate, invoRepaymentDate: invoRepaymentDate, invoSubsAgencyFee: invoSubsAgencyFee, invoDocs: invoDocs, invoDocsId: invoDocsId, invoAktaNoId: invoAktaNoId, invoAssetCostSGD: invoAssetCostSGD, invoAssetCost: invoAssetCost}, function (reply) {
                   reply = JSON.parse(reply);
                   if (reply == true) {
                      $("#popup").fadeIn(100).delay(500).fadeOut(100);
                      $("#popupText").html("Updated");
                   } else {
                      alert("Update Error!");
                   }
                });

             });

          });


      </script>

      <?php include_once 'header.php'; ?>
      <?php include_once 'popup.php'; ?>
      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">
         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Contracts for <?= $cName ?> Campaign</span>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body">

                     <div class="row" style='background-color:#e6ffff;padding:20px 0'>


                        <div class="col-xs-12 col-md-6">

                           <div class="form-group">
                              <label>Contract Type : </label>
                              <select id="conType" class="form-control">
                                 <?= $conType ?>
                                 <?php if ($conType == 0) { ?>
                                    <option value="0" selected="">English</option>
                                    <option value="1">English + Bahasa</option>
                                 <?php } else { ?>
                                    <option value="0">English</option>
                                    <option selected="" value="1">English + Bahasa</option>
                                 <?php } ?>
                              </select>
                              <p>Mono or Multi Lingual Contract</p>
                           </div>

                           <div class="form-group">
                              <label>Company Name : </label>
                              <input  type="text" id="comName" value="<?= $comName ?>" class="form-control" disabled/>
                              <p>Eg. Maxima Juta Niaga Sdn Bhd</p>
                           </div>

                           <div class="form-group">
                              <label for="company_reg_type_eng">Company Registration Type (English)</label>
                              <select class="form-control" id="company_reg_type_eng" name="company_reg_type_eng" disabled>
                                 <option value="0" <?php echo ($comRegTypeEng == 0) ? "selected":"" ?>>--- Please Select ---</option>
                                 <option value="1" <?php echo ($comRegTypeEng == 1) ? "selected":"" ?>>Company Registration No.</option>
                                 <option value="2" <?php echo ($comRegTypeEng == 2) ? "selected":"" ?>>Single Business No.</option>
                              </select>
                           </div>



                           <div class="form-group">
                              <label>Asset to be purchased : </label>
                              <input type="text" id="purchaseAsset" value="<?= $purchaseAsset ?>" class=" form-control" />
                              <p>Eg. TV set-top boxes and SIM card</p>
                           </div>

                           <div class="form-group hide">
                              <label>Asset Cost in SGD: </label>
                              <?php
                              $amountFunding = "SGD".number_format($totalAmount);
                              if ($amountFunding != $assetCost): ?>
                                <input type="text" id="assetCost" value="<?= $amountFunding ?>" class="form-control" />
                              <?php else: ?>
                                <input type="text" id="assetCost" value="<?= $assetCost ?>" class="form-control" />
                              <?php endif ?>
                              <p>Eg. SGD120,000</p>
                           </div>

                           <div class="form-group">
                              <label>Sale Price in SGD: </label>
                              <input type="text" id="salePrice" value="<?= $salePrice ?>" class="form-control" />
                              <p>Eg. SGD130,800</p>
                           </div>

                           <div class="form-group">
                              <label>Asset Cost in campaign country's currency : </label>
                              <input type="text" id="assetCostCurr" value="<?= $assetCostCurr ?>" class="form-control" />
                              <p>Eg. MYR360,000</p>
                           </div>

						   <div class="form-group hide">
                              <label>Campaign Return : </label>
                              <?php if ($cReturn != $camReturn): ?>
                                <input type="text" id="camReturn" value="<?= $cReturn ?>" class="form-control" />
                              <?php else: ?>
                                <input type="text" id="camReturn" value="<?= $camReturn ?>" class="form-control" />
                              <?php endif ?>
                              <p>Eg. 5</p>
                           </div>

                           <div class="form-group">
                              <label>Purchase cost contribution date / Disbursement date: </label>
                              <input type="text" id="closdate" value="<?= $closdate ?>" class="form-control" />
                              <p> Eg. Closing date + 3 working days </p>
                           </div>

                        </div>

                        <div class="col-xs-12 col-md-6">
                           <div class="form-group">
                              <label>Exchange rate : </label>
                              <input type="text" id="exchange" value="<?= $exchange ?>" class="form-control" disabled/>
                              <p>Eg. MYR3</p>
                           </div>
                           <!-- <div class="form-group">
                              <label>Investment Profit : (number only)</label>
                              <input type="number" id="profit" value="<?php // $profit ?>" class="form-control" />
                              <p>Eg. 10800</p>
                           </div> -->


                           <div class="form-group">
                              <label>Campaign country's currency : </label>
                              <input type="text" id="foreignCurrency" value="<?= $foreignCurrency ?>" class="form-control" />
                              <p>Eg. MYR</p>
                           </div>


                           <div class="form-group">
                              <label for="company_reg_type_ina">Company Registration Type (Bahasa): </label>
                              <select class="form-control" id="company_reg_type_ina" name="company_reg_type_ina" disabled>
                                 <option value="0" <?php echo ($comRegTypeIna == 0) ? "selected":"" ?>>--- Please Select ---</option>
                                 <option value="1" <?php echo ($comRegTypeIna == 1) ? "selected":"" ?>>Nomor Tanda Daftar Perusahaan (TDP)</option>
                                 <option value="2" <?php echo ($comRegTypeIna == 2) ? "selected":"" ?>>Nomor Induk Berusaha (NIB)</option>
                              </select>
                           </div>

                           <div class="form-group">
                              <label>Job Position 1: </label>
                              <input type="text" id="dirTitle" value="<?= $dirTitle ?>" class="form-control" disabled />
                              <p>Eg. Owner / Co-founder</p>
                           </div>

                           <div class="form-group">
                              <label>Signee 1 : </label>
                              <input type="text" disabled id="dirName" value="<?= $dirName ?>" class="form-control" />
                              <p>Eg. Muhammad Haryzat bin Zulzahary</p>
                           </div>

                           <div>
                              <label>Email UKM 1 : </label>
                              <input type="text" id="dirEmail" value="<?= $dirEmail ?>" class="form-control" disabled/>
                              <p>Eg. muhammad@gmail.com</p>
                           </div>
<!--
                           <div class="form-group">
                              <label>ID Number 1 : </label>
                              <input type="text" id="dirIc" value="KTP No./Nomor Kartu Tanda Penduduk:<?= $dirIc ?>" class="form-control" disabled />
                              <p>Eg. KTP No.</p>
                           </div> -->

                           <!-- <div class="row">
                             <div class="col-lg-3">
                               <div class="form-group">
                                  <label>Admin fee (PCT) : </label>
                                  <input type="text" id="admin_fee_pct" value="<?#= $admin_fee_pct ?>" class="form-control" />
                               </div>
                             </div>
                             <div class="col-lg-9">
                               <div class="form-group">
                                  <label>Admin fee (AMT) : </label>
                                  <input type="text" id="admin_fee_amt" value="<?#= $admin_fee_amt ?>" class="form-control" data-total-amount="<?= $totalAmount ?>"/>
                               </div>
                             </div>
                           </div> -->
                        </div>
                     </div>

                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 1 : </label>
                        <input type='file' name='image1' id='image1' />
                        <div id='imgDiv1'>
                           <?php if ($image1 != "" or $image1 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image1 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="1">Delete</button>
                           <?php } ?>
                        </div>

                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 2 : </label>
                        <input type='file' name='image2' id='image2' />
                        <div id='imgDiv2'>
                           <?php if ($image2 != "" or $image2 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image2 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="2">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 3 : </label>
                        <input type='file' name='image3' id='image3' />
                        <div id='imgDiv3'>
                           <?php if ($image3 != "" or $image3 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image3 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="3">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 4 : </label>
                        <input type='file' name='image4' id='image4' />
                        <div id='imgDiv4'>
                           <?php if ($image4 != "" or $image4 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image4 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="4">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 5 : </label>
                        <input type='file' name='image5' id='image5' />
                        <div id='imgDiv5'>
                           <?php if ($image5 != "" or $image5 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image5 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="5">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 6 : </label>
                        <input type='file' name='image6' id='image6' />
                        <div id='imgDiv6'>
                           <?php if ($image6 != "" or $image6 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image6 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="6">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 7 : </label>
                        <input type='file' name='image7' id='image7' />
                        <div id='imgDiv7'>
                           <?php if ($image7 != "" or $image7 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image7 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="7">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 8 : </label>
                        <input type='file' name='image8' id='image8' />
                        <div id='imgDiv8'>
                           <?php if ($image8 != "" or $image8 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image8 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="8">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 9 : </label>
                        <input type='file' name='image9' id='image9' />
                        <div id='imgDiv9'>
                           <?php if ($image9 != "" or $image9 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image9 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="9">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 10 : </label>
                        <input type='file' name='image10' id='image10' />
                        <div id='imgDiv10'>
                           <?php if ($image10 != "" or $image10 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image10 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="10">Delete</button>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="form-group" style='margin:20px auto'>
                        <label>Schedule A Image 11 : </label>
                        <input type='file' name='image11' id='image11' />
                        <div id='imgDiv11'>
                           <?php if ($image11 != "" or $image11 != NULL) { ?><br>
                              <img src='../assets/images/pdfimg/<?= $image11 ?>' width="80%"><br><br>
							  <button class="btn btn-danger DelPdfBtn" type="button" value="11">Delete</button>
                           <?php } ?>
                        </div>
                     </div>


					 <div class="row" style='background-color:#D2D7D3;padding:20px 0'>
                        <h1 style='text-align: center;margin:20px auto'>Only for Invoice Financing</h1>
                        <div class="col-xs-12 col-md-6">

                           <div class="form-group">
                              <label>Contract No : </label>
                              <input type="text" id="invoContractNo" value="<?= $invoContractNo ?>" class="form-control" disabled />

                           </div>

                           <div class="form-group">
                              <label>AKTA No : </label>
                              <textarea type="text" id="invoAktaNo" value="" class="form-control" ><?= $invoAktaNo ?></textarea>
                              <p>Eg. Notary Saefudin, SH on 16 February 2016</p>
                           </div>
                           <div class="form-group">
                              <!-- <label>Fee Payment Date: </label> -->
                              <input type="hidden" id="invoFeePaymentDtae" value="<?= $invoFeePaymentDtae ?>" class="form-control" />
                              <p></p>
                           </div>
                           <div class="form-group">
                               <!-- <label>Agency Fee: </label>-->
                              <input type="hidden" id="invoAgencyFee" value="<?= $invoAgencyFee ?>" class="form-control" />
                              <p></p>
                           </div>

                           <div class="form-group">
                              <label>Receivables Amount as per invoice : (number only)</label>
                              <input type="number" id="invoAssetCost" value="<?= $invoAssetCost ?>" class="form-control" />
                              <p></p>
                           </div>
						    <div class="form-group">
                              <label>Receivables Amount in SGD : (number only)</label>
                              <input type="number" id="invoAssetCostSGD" value="<?= $invoAssetCostSGD ?>" class="form-control" />
                              <p></p>
                           </div>



                        </div>

                        <div class="col-xs-12 col-md-6">
                           <div class="form-group">
                              <label>Maturity Date : </label>
                              <input type="text" id="invoMaturityDate" value="<?= $invoMaturityDate ?>" class="form-control" />
                              <p></p>
                           </div>

                           <div class="form-group">
                              <label>Repayment Date : </label>
                              <input type="text" id="invoRepaymentDate" value="<?= $invoRepaymentDate ?>" class="form-control" />
                              <p></p>
                           </div>
                           <div class="form-group">
                              <label>Subs-Agency Fee : </label>
                              <input type="text" id="invoSubsAgencyFee" value="<?= $invoSubsAgencyFee ?>" class="form-control" />
                              <p></p>
                           </div>
                           <div class="form-group">
                              <label>Underlying Documents : </label>
                              <textarea type="text" id="invoDocs" value="" class="form-control" ><?= $invoDocs ?></textarea>
                              <p></p>
                           </div>


                        </div>
                     </div>

                     <div class="row" style='background-color:#e6ffff;padding:20px 0'>
                        <h1 style='text-align: center;margin:20px auto'>Only for Bilingual Contracts</h1>
                        <div class="col-xs-12 col-md-6">

                           <!-- <div class="form-group">
                              <label>Company Name : </label>
                              <input type="text" id="comNameId" value="<?= $comNameId ?>" class="form-control" disabled/>
                           </div> -->

                           <div class="form-group">
                              <label>Company Registration No : </label>
                              <input type="text" id="comRegId" value="<?= $comReg ?>" class="form-control" disabled/>
                           </div>

                           <div class="form-group">
                              <label>AKTA No ID : </label>
                              <textarea  id="invoAktaNoId" value="" class="form-control" ><?= $invoAktaNoId ?></textarea>
                              <p>Eg. Notaris Saefudin, SH pada tanggal 16 Februari 2016</p>
                           </div>



                        </div>

                        <div class="col-xs-12 col-md-6">
                           <div class="form-group">
                              <label>Asset to be purchased in ID : </label>
                              <input type="text" id="purchaseAssetId" value="<?= $purchaseAssetId ?>" class="form-control" />
                              <p>Eg. Kotak set-top TV dan kartu SIM</p>
                           </div>

                           <div class="form-group">
                              <label>Job Position 1: </label>
                              <input type="text" id="dirIcId" value="<?= $dirIcId?>" class="form-control" disabled/>
                              <!-- <p>Eg. Nomor No.: IC Malaysia No .: 830826-05-5079</p> -->
                           </div>
                           <div class="form-group">
                              <label>Underlying Documents ID: </label>
                              <textarea type="text" id="invoDocsId" value="" class="form-control" ><?= $invoDocsId ?></textarea>
                              <p></p>
                           </div>
                        </div>
                     </div>


                     <button id='save' class="btn btn-block btn-primary" style='margin: 20px auto'>Save</button>
                     <a href='contract-individual.php?c=<?=$cId?>' class="btn btn-block btn-success" style='margin:20px auto'>View Individual Contracts</a>
                     <a href='samplepdf.php?c=<?=$cId?>' target="_blank" class="btn btn-block btn-info" style='margin:20px auto'>View Sample Contracts</a>
                  </div>
               </div>
            </div>
         </div>

      </div>
      <script type="text/javascript">
       $(document).ready(function() {
        $("#admin_fee_pct").on("keyup", function() {
          $total_amount = $("#admin_fee_amt").data("total-amount");
          cal = (parseFloat($(this).val()) / 100) * parseFloat($total_amount);
          $("#admin_fee_amt").val(cal);
        })
       })
     </script>

   </body>


</html>
