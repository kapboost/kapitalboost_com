<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$eId = test_input($_GET["e"]);

if (isset($_POST)) {
       $eNamef = test_input($_POST["eName"]);
       $eDesf = test_input($_POST["eDes"]);
       $eEnabledf = test_input($_POST["eEnabled"]);
       $eLinkf = test_input($_POST["eLink"]);
       $eDatef = test_input($_POST["eDate"]);

       if ($mysql->Connection()) {
              $mysql->EditEvent($eId, $eNamef, $eDesf, $eLinkf, $eEnabledf, $eDatef);
       }

       if (isset($_FILES["eImage"]) && $_FILES["eImage"]["name"] != "") {
              $file = $_FILES["eImage"]["tmp_name"];
              $path = $_FILES["eImage"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/events/";
              $image_name = rand(1, 10) . time() . "." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name);
              if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgEvent($eId, $image_name);
              }
       }
}



if ($mysql->Connection()) {
       list($eventName, $eventDes, $eventImg, $eventLink, $eventEnabled, $eventDate) = $mysql->GetSingleEvent($eId);
}

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Edit Event - <?= $eventName ?></title>

              <?php include_once 'include.php'; ?>

              <script>


                     $(document).ready(function () {

                     });
              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Event Detail - <?= $eventName ?></span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body" style="text-align: center">


                                                 <form action="" method="POST" enctype="multipart/form-data">

                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Event Name (FRO) : </label>
                                                                      <input  type="text" name="eName" value="<?= $eventName ?>" class="form-control" >
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Event Date :</label>
                                                                      <input  type="text" name="eDate" id="eDate" value="<?= $eventDate ?>" class="form-control" >
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Enable / Disable : </label>
                                                                      <select name="eEnabled"  class="form-control">
                                                                             <?php if ($eventEnabled == 0) {
                                                                                    ?> 
                                                                                    <option value="1">Enabled</option>
                                                                                    <option value="0" selected>Disabled</option>

                                                                             <?php } else { ?>
                                                                                    <option value="1" selected>Enabled</option>
                                                                                    <option value="0" >Disabled</option>

                                                                             <?php } ?>
                                                                      </select>
                                                               </div>



                                                        </div>
                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Event Image : </label>
                                                                      <input type="file" name="eImage" >
                                                                      <?php if ($eventImg != "") {
                                                                             ?>
                                                                             <br><br>
                                                                             <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/events/<?= $eventImg ?>" class="image-link" ><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/events/<?= $eventImg ?>&w=300" alt=""></a>

                                                                      <?php }
                                                                      ?>
                                                               </div>

                                                        </div>

                                                        <div>
                                                               <div class="form-group">
                                                                      <label>Event Link :</label>
                                                                      <input  type="text" name="eLink"  value="<?= $eventLink ?>" class="form-control" >
                                                               </div>
                                                               <div class="form-group">
                                                                      <label>Event Description : </label>
                                                                      <textarea rows="6" name="eDes"  class="form-control"><?= $eventDes ?></textarea>
                                                               </div>

                                                               <input type="submit" class="btn btn-primary" value="Update"/>

                                                        </div>

                                          </div>
                                   </div>
                            </div>


                            <script src="js/pickadate/picker.js"></script>
                            <script src="js/pickadate/picker.date.js"></script>
                            <script src="js/pickadate/legacy.js"></script>
                            <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css"> 
                            <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
                            <script>

                     $('#eDate').pickadate({
                            format: 'yyyy-mm-dd'
                     });

                     $(document).ready(function () {
                            $('.image-link').magnificPopup({type: 'image'});


                     });


                            </script>


                            </body>


                            </html>