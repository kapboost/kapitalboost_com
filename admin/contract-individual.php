<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$cId = $_GET["c"];

if ($mysql->Connection()) {
   list($cName, $mPayout) = $mysql->GetSingleCampaignForPayout($cId);
   $payoutA = explode("==", $mPayout);
   for ($i = 0; $i < count($payoutA); $i++) {
      $payouts = explode("~", $payoutA[$i]);
      $payoutPercent[$i] = $payouts[0];
      $payoutDate[$i] = $payouts[1];
   }

   list($conType, $comName, $comReg, $comRegTypeEng, $comRegTypeIna, $purchaseAsset, $assetCost, $salePrice, $assetCostCurr, $exchange, $profit, $camReturn, $foreignCurrency, $dirName, $dirEmail, $dirTitle, $dirIc, $image1, $image2, $image3, $image4, $image5, $image6, $image7, $image8, $image9, $image10, $image11, $comNameId, $comRegId, $purchaseAssetId, $dirIcId, $closdate, $invoContractNo, $invoAktaNo, $invoFeePaymentDtae, $invoAgencyFee, $invoMaturityDate, $invoRepaymentDate, $invoSubsAgencyFee, $invoDocs,$invoDocsId, $invoAktaNoId, $invoAssetCostSGD, $invoAssetCost) = $mysql->GetSingleCampaignContractList($cId);
   list($id, $mId, $name, $email, $funding, $fundingId, $date, $token, $hasContract, $sentContract) = $mysql->GetAllInvestorContract($cId);



   for ($i = 0; $i < count($id); $i++) {

      $newDate[$i] = date("d F Y", strtotime($date[$i]));

      list($icName, $icType, $icNumber,$icId) = $mysql->GetSingleContractMember($mId[$i]);
      $name[$i] = $icName;
   }
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KB Admin - <?= $cName ?></title>

      <?php include_once 'include.php'; ?>


   </head>
   <body>
      <?php include_once 'header.php'; ?>
      <?php include_once 'popup.php'; ?>


      <script>
          $(document).ready(function () {
             var hasPayout = '<?= $payoutPercent[0] ?>';
             if (hasPayout == '') {
                alert("Please Update the Master Payout first.");
                location.href = "<?php echo ADMIN_PATH ?>/master-payout.php?c=<?= $cId ?>";
             }

             $(document).on('click', '.createContract', function () {
                var index = $(this).parent().parent().index();
                var cId = '<?= $cId ?>';
                var iId = this.id;
                var token = $(this).data("token");
                $(this).text("Creating...");
                $.get("createpdf.php?c=" + cId + "&i=" + iId + "&token=" + token, function (reply) {
                   if (reply == 1) {
                      $(".createContract").text("Create Contract");
                      $(".previewContract").eq(index).text("Preview Contract");

                      $(".sendBtn").eq(index).removeAttr('disabled');
                      $(".sendBtn").eq(index).css({"background-color": "green"});
                   } else {
                      alert("create failed - " + reply);
                   }
                });
             });

             $(document).on('click', '.previewContract', function () {
                var token = $(this).data("token");
                window.open("https://kapitalboost.com/assets/pdfs/" + token + ".pdf");
             });

             $(document).on('click', '.sendBtn', function () {
                var index = $(this).parent().parent().index();
                var cId = '<?= $cId ?>';
                var iId = $(this).data("iid");
                var token = $(this).data("token");
                var name = $(this).data("name");
                var iEmail = $(this).data("iemail");
                var oEmail = $(this).data("oemail");
                if (token == '') {
                   alert("Contract has already been sent out once");
                } else {
                   var r = confirm("Confirm sending the contract to " + name + " (" + iEmail + ") and the campaign Owner (" + oEmail + ")");
                   if (r == true) {
                      $(".sendBtn").eq(index).html("<span style='color:white'>Sending...</span>");
                      $.post("sendpdf.php", {c: cId, i: iId, token: token}, function (reply) {
                         if (reply == 1) {
                            $("#popup").fadeIn(100).delay(500).fadeOut(100);
                            $("#popupText").html("Contract sent out successfully");
                            $(".sendBtn").eq(index).html("<i class='fa fa-check-square fa-2x' style='color:white'></i>");
                            $(".sendBtn").eq(index).data("token", null);
                            $(".sendBtn").eq(index).css('background-color', 'blue');
                         } else {
                            alert("Error Sending - " + reply);
                         }
                      });
                   }
                }
             });




          });



      </script>


      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Contract Detail for <?= $cName ?> Campaign</span>
                     <div class="clearfix"></div>
                  </div>

                  <div class="panel-body">


                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                           <thead>
                              <tr>
                                 <th width="5%">No</th>
                                 <th width="15%">Investor Name</th>
                                 <th width="20%">Investor Email</th>
                                 <th width="20%">Funding Amount</th>
                                 <th width="15%">Investment Date</th>
                                 <th width="10%">Create Contract</th>
                                 <th width="10%">Preview Contract</th>
                                 <td width='10%'>Send</td>
                              </tr>
                           </thead>
                           <tbody>
                              <?php for ($i = 0; $i < count($id); $i++) { ?>
                                 <tr class="even">
                                    <td><?= $i + 1 ?></td>
                                    <td><?= $name[$i] ?></td>
                                    <td><?= $email[$i] ?></td>
                                    <td><?= $funding[$i] ?></td>
                                    <td><?= $newDate[$i] ?></td>
                                    <td><button class="createContract btn btn-blue" id='<?= $id[$i] ?>' data-token='<?= $token[$i] ?>' >Create Contract</button></td>
                                    <?php if ($hasContract[$i] == 0) { ?>
                                       <td><button class="previewContract btn btn-default" data-token="<?= $token[$i] ?>">Create one first</button></td>
                                    <?php } else { ?>
                                       <td><button class="previewContract btn" data-token='<?= $token[$i] ?>'>Preview Contract</button></td>

                                    <?php } if ($sentContract[$i] == 0) { ?>
                                       <?php if ($hasContract[$i] == 0) { ?>
                                          <td><button class="sendBtn btn" disabled="" data-token='<?= $token[$i] ?>' data-name='<?= $name[$i] ?>' data-iid='<?= $id[$i] ?>' data-iemail='<?= $email[$i] ?>' data-oemail='<?= $dirEmail ?>'><i class="fa fa-send-o fa-2x" style='color:white'></i></button></td>
                                       <?php } else { ?>
                                          <td><button class="sendBtn btn" data-token='<?= $token[$i] ?>'data-iid='<?= $id[$i] ?>' data-name='<?= $name[$i] ?>' data-iemail='<?= $email[$i] ?>' data-oemail='<?= $dirEmail ?>' style='background-color:green'><i class="fa fa-send-o fa-2x" style="color:white" ></i></button></td>
                                          <?php
                                       }
                                    } else {
                                       ?>
                                       <td><button class="sendBtn btn" data-token='<?= $token[$i] ?>'data-iid='<?= $id[$i] ?>' data-name='<?= $name[$i] ?>' data-iemail='<?= $email[$i] ?>' data-oemail='<?= $dirEmail ?>' style="background-color:blue"><i class="fa fa-check-square fa-2x" style='color:white'></i></button></td>
                                          <?php } ?>

                                 </tr>

                              <?php } ?>

                           </tbody>
                        </table>
                     </div>








                  </div>
               </div>
            </div>
         </div>
      </div>




   </body>


</html>
