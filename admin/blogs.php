<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();


if(isset($_GET["add"])){
  if($_GET["add"] == "1"){
    if($mysql->Connection()){
      $newbId = $mysql->AddNewBlog();
      header("Location: blogs-edit.php?b=$newbId");
    }
  }
}

if ($mysql->Connection()) {
  list($bId,$slug, $title, $des, $images,$enabled,$date,$lang) = $mysql->GetBlogs();
}

for($i=0;$i<count($des);$i++){
  $des[$i] = substr($des[$i],0,300) . "..............";
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <?php include_once 'initialize.php'; ?>

  <title>KB Admin Blogs</title>

  <?php include_once 'include.php'; ?>

  <script>
  $(document).ready(function () {
    $(".DelBtn").click(function () {
      var person = prompt("Please enter Master Code");
      $.post("delete.php", {dblog: $(this).val(), MC: person}, function (reply) {
        reply = JSON.parse(reply);
        $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
          if (reply === "Done") {
            location.reload();

          }
        });
        $("#popupText").html(reply);
      });
    });

  });
  </script>

</head>
<body>
  <?php include_once 'header.php'; ?>
  <?php include_once 'popup.php'; ?>


  <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

    <div class="row">
      <div class="col-xs-12">
        <div class="general-panel panel">

          <div class="blue-panel-heading panel-heading">
            <span class="header-panel" style='margin-right:20px'>Kapital Boost's Blogs </span>
            <a class="btn btn-primary" href="blogs.php?add=1">Add New Blog</a>
            <div class="clearfix"></div>
          </div>

          <div class="panel-body">



            <div class="table-responsive">
              <table class="table table-striped table-bordered table-hover" id="myTable" name="myTable">
                <thead>
                  <tr>
                    <th width="2%">No</th>
                    <th width="5%">Blog Title</th>
                    <th width="7%">Released Date</th>
                    <th width='10%'>Blog Image</th>
                    <th width="*%">Blog Content</th>
                    <th width="*%">Languange</th>
                    <th width="10%">Visibility</th>
                    <th width="7%">Delete</th>
                  </tr>
                </thead>
                <tbody>
                  <?php for ($i = 0; $i < count($bId); $i++) { ?>
                    <tr class="even">
                      <td><?= $i + 1 ?></td>
                      <td>
                        <a href='blogs-edit.php?b=<?=$bId[$i]?>'>
                          <?php if ($title[$i] == "" or $title[$i]== null) {  echo "<i>No Title</i>"; } else {?>
                            <?=$title[$i]?>
                          <?php } ?>
                        </a>
                      </td>
                      <td><?=$date[$i]?></td>
                      <td>
                        <?php if ($images[$i] == "" or $images[$i]== null) {  echo "<i>No Thumbnail Image</i>"; } else {?>
                          <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/blog/<?= $images[$i] ?>" class="image-link" ><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/blog/<?= $images[$i] ?>&w=150" alt="<?=$images[$i]?>"></a>
                        <?php } ?>
                      </td>
                      <td><?=$des[$i]?></td>
                      <?php if ($lang[$i] == 1) { ?>
                        <td><p>Bahasa Indonesia</p></td>
                      <?php } else { ?>
                        <td><p>English</p></td>
                      <?php } ?>
                      <?php if ($enabled[$i] == 1) { ?>
                        <td style="color:white"><p style="background-color:green;padding:5px;">Enabled</p></td>
                      <?php } else { ?>
                        <td style="color:white"><p style="background-color:gray;padding:5px;">Disabled</p></td>
                      <?php } ?>
                      <td style="text-align: center"><button value="<?= $bId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                    </tr>

                  <?php } ?>

                </tbody>
              </table>
            </div>





          </div>
        </div>
      </div>
    </div>
  </div>


  <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css">
  <script src="js/magnific-popup/jquery.magnific-popup.js"></script>


  <script>
  $(document).ready(function () {


    $('.image-link').magnificPopup({type: 'image'});




  });
  </script>
  <script>
	$(document).ready( function () {
		$('#myTable').DataTable();
	} );
	</script>

</body>


</html>
