<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       $gId = $_GET["g"];
       list($gName, $email, $mobile, $country, $company, $industry, $year, $currency, $cType, $revenue, $fundingAmt, $asset, $tenor, $purchased, $purchasedOrder, $attachedFile,$attachedFilePo) = $mysql->GetSingleGetFundedList($gId);

       $howYouKnowUs = $mysql->GetHowYouKnowUs_GetFunded($gId);
       switch ($howYouKnowUs) {
          case "1":
            $howYouKnowUs = "Search Engine";
            break;
          case "2":
            $howYouKnowUs = "Ads";
            break;
          case "3":
            $howYouKnowUs = "Social Media";
            break;
          case "4":
            $howYouKnowUs = "Friend/Family";
            break;
          case "5":
            $howYouKnowUs = "Community Event";
            break;
          case "6":
            $howYouKnowUs = "News/Blog/Magazine";
            break;
          default:
            $howYouKnowUs = "Other";
        }
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Edit Get Funded</title>

              <?php include_once 'include.php'; ?>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Edit Get Funded List - <?=$gName?></span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">


                                                 <form action="" method="POST" enctype="multipart/form-data">

                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Full Name : </label>
                                                                      <input type="text" name="fullname" value="<?= $gName ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Email Address : </label>
                                                                      <input type="text" name="email" value="<?= $email ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Mobile Number : </label>
                                                                      <input type="text" name="mobile" id="mobile" value="<?= $mobile ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Country : </label>
                                                                      <input type="text" name="country" value="<?= $country ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Company : </label>
                                                                      <input type="text" name="company" value="<?= $company ?>" class="form-control"readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Industry : </label>
                                                                      <input type="text" name="industri" value="<?= $industry ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Year Established : </label>
                                                                      <input type="text" name="year" value="<?= $year ?>" class="form-control" readonly>
                                                               </div>
                                                               <?php if($attachedFile != ""){?>
                                                               <div class="form-group">
                                                                      <label>Financial Report : </label>
                                                                      <a href="../assets/images/getfunded/<?= $attachedFile ?>"target="_blank">Download</a>
                                                               </div>
                                                               <?php }?>

                                                               <div class="form-group">
                                                                      <label>How You Know Us : </label>
                                                                      <input type="text" name="how_you_know" value="<?= $howYouKnowUs ?>" class="form-control" readonly>
                                                               </div>

                                                        </div>

                                                        <div class="col-xs-12 col-md-6">
                                                               <div class="form-group">
                                                                      <label>Currency : </label>
                                                                      <input type="text" name="currency" value="<?= $currency ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Est Annual Revenue  : </label>
                                                                      <input type="text" name="est_an_rev" value="<?= $revenue ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Funding Amount Required : </label>
                                                                      <input type="text" name="funding_amount" value="<?= $fundingAmt ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Assets To Be Purchased : </label>
                                                                      <input type="text" name="ass_tobe_pur" value="<?= $asset ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Tenor : </label>
                                                                      <input type="text" name="funding_period" value="<?= $tenor ?>" class="form-control" readonly>
                                                               </div>

                                                               <div class="form-group">
                                                                      <label>Have purchase orders  : </label>
                                                                      <input type="text" name="have_purchased_order" value="<?= $purchased ?>" class="form-control" readonly>
                                                               </div>
                                                               <div class="form-group">
                                                                      <label>Purchase Orders  : </label>
                                                                      <input type="text" name="have_purchased_order" value="<?= $purchasedOrder ?>" class="form-control" readonly>
                                                               </div>
                                                               
                                                               <?php if($attachedFilePo != ""){?>
                                                               <div class="form-group">
                                                                      <label>Purchase orders  : </label>
                                                                      <a href="../assets/images/getfunded/<?= $attachedFilePo ?>"target="_blank">Download</a>
                                                               </div>
                                                               <?php } ?>
                                                        </div>

                                                 </form>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>