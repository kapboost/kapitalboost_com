<?php

/* StartParamData */

include_once 'mysql.php';
$mysql = new mysql();

$Id = $_GET["id"];

if ($mysql->Connection()) {
    // list($companyName, $contractNumber, $companyCode, $pdfFile, $signee_1, $email_1, $signee_2, $email_2) = $mysql->GetContractUKM($Id);
	// list($id, $campaign_id, $contractNumber, $companyName, $companyCode, $customerName, $companyRegNumber, $dateEng, $dateIna, $tenorEng, $tenorIna, $giroNoteEng, $giroNoteIna, $companyRegTypeEng, $companyRegTypeIna, $targetFundingAmt, $wakalahInvestor, $wakalahKB, $otherReqEng, $otherReqIna, $totalPayoutSGD, $totalPayoutIDR, $signee_1, $signee_2, $admin_fee_pct, $admin_fee_amt, $pdfFile, $email_1, $email_2) = $mysql->GetContractUKM($Id);
	list($id, $campaign_id, $contractNumber, $companyName, $companyCode, $customerNameEng, $customerNameIna, $companyRegNumber, $tenorEng, $tenorIna, $giroNoteEng, $giroNoteIna, $companyRegTypeEng, $companyRegTypeIna, $targetFundingAmt, $wakalahInvestor, $wakalahKB, $otherReqEng, $otherReqIna, $totalPayoutSGD, $totalPayoutIDR, $signee_1, $signee_2, $admin_fee_pct, $admin_fee_amt, $pdfFile, $email_1, $email_2, $id_number_1, $id_number_2, $position_1, $position_2, $payment_method_eng, $payment_method_ina, $signature_page_number) = $mysql->GetContractUKM($Id);
}


$url		= "https://kapitalboost.com$pdfFile";
$title		= "Perjanjian Crowdfunding $companyName - $contractNumber.$companyCode";
$message	= "";

$nameDirectorOne	= $signee_1;
$emailDirectorOne	= $email_1;

$nameDirectorTwo	= $signee_2;
$emailDirectorTwo	= $email_2;

$nameDirectorKB		= "Erlangga Wibisono Witoyo";
$emailDirectorKB	= "erly@kapitalboost.com";

$Message			= "";

/*--------- setSign ---------------*/
// $signPage		= "7";
$signPage		= $signature_page_number;

$x				= "50";
$yDirectorOne	= "135.86614299212602";

$xDirectorTwo	= "320";
$yDirectorTwo	= "135.86614299212602";

$yDirectorKB	= "479.0533946456693";
/*--------- setSign ---------------*/

/* EndParamData */

$data = array(
    "is_draft" => 0,
    "embedded" => 0,
    "title" => $title,
    "message" => $message,
    "use_signer_order" => 0,
    "reminders" => 1,
    "require_all_signers" => 1,
    "files" => array(array("name" => $title, "file_url" => $url, "file_id" => "", "file_base64" => "")),
    "signers" => array(array("id" => 1, "name" => $nameDirectorOne, "email" => $emailDirectorOne, "message" => $Message), array("id" => 2, "name" => $nameDirectorTwo, "email" => $emailDirectorTwo, "message" => $Message), array("id" => 3, "name" => $nameDirectorKB, "email" => $emailDirectorKB, "message" => $Message)),
    "fields" => array(array(array("type" => "signature", "x" => $x, "y" => $yDirectorOne, "width" => "200", "height" => "40",
                "page" => $signPage, "signer" => "1", "name" => $nameDirectorOne, "identifier" => "UKM_SIGNER_DIRECTOR", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $xDirectorTwo, "y" => $yDirectorTwo, "width" => "200", "height" => "40",
                "page" => $signPage, "signer" => "2", "name" => $nameDirectorTwo, "identifier" => "UKM_SIGNER", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => ""), array("type" => "signature", "x" => $x, "y" => $yDirectorKB, "width" => "200", "height" => "40",
                "page" => $signPage, "signer" => "3", "name" => $nameDirectorKB, "identifier" => "KAPITALBOOST_SIGNER", "required" => 1, "readonly" => 0,
                "text_size" => "", "text_color" => "", "text_font" => "", "text_style" => "", "validation_type" => "", "values" => "",
                "options" => [], "group" => "")))
	);

// print_r("Datas => ");
// print_r($data); die;
// echo "<br><hr>";

$postData = json_encode($data);

$curl = curl_init();
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://api.eversign.com/api/document?access_key=2cd751d948d6139a1700871a51ce4e78&business_id=3352",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 90,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "POST",
    CURLOPT_POSTFIELDS => $postData,
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "content-type: application/json",
    ),
));

$response = curl_exec($curl);
$error = curl_error($curl);
curl_close($curl);

$responseA = json_decode($response,1);

// print_r($responseA);
// exit();

if($responseA["document_hash"] != ""){
   // $result = $mysql->UpdateContractUKM($Id);
    $params = array('sent' => 1, 'document_hash' => $responseA["document_hash"]);
    list($result, $err, $id) = $mysql->updateDatas($params, 'tcontract_ukm', 'id', $id);

   if($result){
      echo json_encode(array('status' => 200, 'result' => $result, 'err' => null));
   }else{
      echo json_encode(array('status' => 200, 'result' => $result, 'err' => "ops, somthing when wrong"));
   }
}else{
   // echo 'EverSign API Error';
   echo json_encode(array('status' => 200, 'result' => 0, 'err' => "EverSign API Error"));
   // echo "'<script>console.log(\"response : $response\")</script>'";
}


?>
