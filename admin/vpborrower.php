<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($fname, $email, $mobile, $company, $year, $currency, $revenue, $financing_type, $financing_period, $datetime) = $mysql->GetAllVpBorrowList();
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin VP Borrower List</title>

              <?php include_once 'include.php'; ?>

              <script>

                     $(document).ready(function () {
                            $(".DelBtn").click(function () {
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dgetfunded: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        location.reload();
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });
                     });

              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">VP Borrower List</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">


                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="2%">No</th>
                                                                             <th width="15%">Full Name</th>
                                                                             <th width="10%">Email</th>
                                                                             <th width="8%">Mobile</th>
                                                                             <th width="8%">Company</th>
                                                                             <th width="8%">Year Established</th>
                                                                             <th width="10%">Est Rev</th>
                                                                             <th width="10%">Financing Type</th>
                                                                             <th width="10%">Funding Period</th>
                                                                             <th width="10%">Date Created</th>
                                                                             <!--<th width="3%">Delete</th>-->
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($fname); $i++) { ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><?= $fname[$i] ?></td>
                                                                                    <td><?= $email[$i] ?></td>
                                                                                    <td><?= $mobile[$i] ?></td>
                                                                                    <td><?= $company[$i] ?></td>
                                                                                    <td><?= $year[$i] ?></td>
                                                                                    <td><?= $currency[$i] ?> <?= $revenue[$i] ?></td>
                                                                                    <td><?=$financing_type[$i]?></td>
                                                                                    <td><?=$financing_period[$i]?> months</td>
                                                                                    <td><?=$datetime[$i]?> </td>
                                                                                    <!--<td style="text-align: center"><button value="<?= $gId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>-->
                                                                             </tr>
                                                                      <?php } ?>


                                                               </tbody>
                                                        </table>
                                                 </div>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


<script>
		$(document).ready( function () {
			$('#myTable').DataTable();
		} );
		</script>

       </body>


</html>