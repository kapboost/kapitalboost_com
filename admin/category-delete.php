<?php

include_once 'security.php';
include_once 'mysql.php';
$mysql = new mysql();

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

$cId = test_input($_GET["c"]);

if ($mysql->Connection()) {
    $mysql->deleteCategory($cId);
    header('Location: '.'category-list.php');
}

?>
