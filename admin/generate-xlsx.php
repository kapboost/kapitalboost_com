<?php

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$name = $_POST["name"];
$email = $_POST["email"];
$icType = $_POST["icType"];
$mNric = $_POST["nric"];
$profit = $_POST["profit"];
$amount = $_POST["amount"];
$member_country = $_POST["member_country"];

$spreadsheet = new Spreadsheet();
$sheet = $spreadsheet->getActiveSheet();
$sheet->setCellValueByColumnAndRow(1, 1, 'Name');
$sheet->setCellValueByColumnAndRow(2, 1, 'Nationality');
$sheet->setCellValueByColumnAndRow(3, 1, 'Type of Identification');
$sheet->setCellValueByColumnAndRow(4, 1, 'Identification No.');
$sheet->setCellValueByColumnAndRow(5, 1, 'Amount Invested');
$sheet->setCellValueByColumnAndRow(6, 1, 'Profit');
$sheet->setCellValueByColumnAndRow(7, 1, 'Email');

for ($i = 2; $i < count($name)+2; $i++) {
	$v = $i-2;
	$sheet->setCellValueByColumnAndRow(1, $i, $name[$v]);
	$sheet->setCellValueByColumnAndRow(2, $i, $member_country[$v]);
	$sheet->setCellValueByColumnAndRow(3, $i, $icType[$v]);
	$sheet->setCellValueByColumnAndRow(4, $i, $mNric[$v]);
	$sheet->setCellValueByColumnAndRow(5, $i, $amount[$v]);
	$sheet->setCellValueByColumnAndRow(6, $i, $profit[$v]);
	$sheet->setCellValueByColumnAndRow(7, $i, $email[$v]);
}

$spreadsheet->setActiveSheetIndex(0);

$filename = 'Campaign-report';

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

$writer->save('php://output');

?>
