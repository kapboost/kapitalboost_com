<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$cId = $_GET["c"];
$mId = $_GET["m"];

if ($mysql->Connection()) {
       list($name, $mobile, $xfersToken, $iId, $pType, $fundingId, $status, $amount, $paid, $cName, $invoiceId, $tpayout, $emailAddr, $country) = $mysql->GetAllInvestorsForSingleCampaign($cId, $mId);
}

?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Xfers Payout</title>

              <?php include_once 'include.php'; ?>

              <script>
                     $(document).ready(function () {
                            $(".DelBtn").on('click', function () {
                                   var index = $(this).parent().parent().index();
                                   $("tr").eq(index + 1).remove();
                            });

                            $(".PayBtn").on('click', function () {
                                   var iId = $(this).val();
                                   var xfersToken = $(this).data("xtoken");
                                   //var xfersToken = "x1q5t28yt6b2kNRUdY6xj94zkYkPt86x3avTy_bJtg4";
                                   var index = $(this).parent().parent().index();
                                   var amount = $(".amount").eq(index).val();
                                   var name = $(".name").eq(index).val();
                                   var invoiceId = $(".invoiceId").eq(index).val();
                                   var mId = "<?= $mId ?>";
                                   var cName = "<?= $cName[0] ?>";
                                   var tpayout = $(".tpayout").eq(index).val();
                                   var emailAddr = $(".emailAddr").eq(index).val();

                                   var r = confirm("Confirm payout of $" + amount + " to " + name);
                                   if (r == true) {
                                          $(this).children().removeClass("fa fa-arrow-right fa-2x");
                                          $(this).children().text("Loading");
                                          var btn = $(this);
                                          $(this).prop("disabled", true);

                                          $.post("posts.php", {job: "xferspayout", iId: iId, mId: mId, name: name, invoiceId: invoiceId, xfersToken: xfersToken, cName: cName, amount: amount, tpayout: tpayout, emailAddr: emailAddr}, function (reply) {
                                                 var repli = JSON.parse(reply);
                                                 if (repli == "failed") {
                                                        alert("Error Occur, please contact the developer");
                                                 } else {
                                                        btn.children().text("Payout Done");
                                                        $(this).removeClass("PayBtn");
                                                 }
                                          });
                                   } else {
                                          
                                   }


                            });
							
                            $(".WalletBtn").on('click', function () {
                                   var iId = $(this).val();
                                   var index = $(this).parent().parent().index();
                                   var amount = $(".amount").eq(index).val();
                                   var name = $(".name").eq(index).val();
                                   var invoiceId = $(".invoiceId").eq(index).val();
                                   var mId = "<?= $mId ?>";
                                   var cName = "<?= $cName[0] ?>";
                                   var tpayout = $(".tpayout").eq(index).val();
                                   var emailAddr = $(".emailAddr").eq(index).val();

                                   var r = confirm("Confirm payout of $" + amount + " to " + name);
                                   if (r == true) {
                                          $(this).children().text("Loading");
                                          var btn = $(this);
                                          $(this).prop("disabled", true);
										  
                                          $.post("posts.php", {job: "walletpayout", iId: iId, mId: mId, name: name, invoiceId: invoiceId, cName: cName, amount: amount, tpayout: tpayout, emailAddr: emailAddr}, function (reply) {
                                                 var repli = JSON.parse(reply);
                                                 if (repli == "failed") {
                                                        alert("Error Occur, please contact the developer");
                                                 } else {
                                                        btn.children().text("Payout Done");
                                                        $(this).removeClass("WalletBtn");
                                                 }
                                          });
                                   } else {
                                          
                                   }


                            });

                            $(".PayTransfer").on('click', function () {
                                   var iId = $(this).val();
                                   var xfersToken = $(this).data("xtoken");
                                   //var xfersToken = "x1q5t28yt6b2kNRUdY6xj94zkYkPt86x3avTy_bJtg4";
                                   var index = $(this).parent().parent().index();
                                   var amount = $(".amount").eq(index).val();
                                   var name = $(".name").eq(index).val();
                                   var invoiceId = $(".invoiceId").eq(index).val();
                                   var mId = "<?= $mId ?>";
                                   var cName = "<?= $cName[0] ?>";
                                   var tpayout = $(".tpayout").eq(index).val();
                                   var emailAddr = $(".emailAddr").eq(index).val();

                                   var r = confirm("Confirm payout of $" + amount + " to " + name);
                                   if (r == true) {
                                          $(this).children().text("Loading..");
                                          var btn = $(this);
                                          $(this).prop("disabled", true);

                                          $.post("posts.php", {job: "payViaBank", iId: iId, mId: mId, name: name, invoiceId: invoiceId, xfersToken: xfersToken, cName: cName, amount: amount, tpayout: tpayout, emailAddr: emailAddr}, function (reply) {
                                                 var repli = JSON.parse(reply);
                                                 if (repli == "failed") {
                                                        alert("Error Occur, please contact the developer");
                                                 } else {
                                                        btn.removeClass("PayTransfer");
                                                        btn.children().text("Payout Done");
                                                 }
                                          });
                                   } else {
                                          
                                   }


                            });
                     });

              </script>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Master Payout for <?= $cName[0] ?></span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <style>
                                                 .dark{
                                                        color:#000;
                                                 }
                                                 .light{

                                                 }

                                                 table tr{

                                                 }
                                                 .nopay{
                                                        padding:8px;border-radius: 5px;background-color: #cccccc;color:#808080;
                                                 }
                                                 .amount{
                                                        padding: 5px;font-size:1.2em;
                                                 }

                                          </style>

                                          <div class="panel-body">


                                                 <table class="table table-bordered">
                                                        <thead>
                                                               <tr>
                                                                      <th>Investor Name</th>
                                                                      <th>Payment Type</th>
                                                                      <th>Payout Amount </th>
                                                                      <th>Pay Individually </th>
                                                                      <th>Pay Wallet </th>
                                                                      <th>Bank Transfer</th>
                                                               </tr>
                                                        </thead>

                                                        <tbody>
                                                               <?php
                                                               $total = 0;
                                                               for ($i = 0; $i < count($name); $i++) {
																	  // $amount[$i] = number_format((float)$amount[$i], 1, '.', '');
                                                                             $total += $amount[$i];
                                                                      if (($pType[$i] == "Xfers" || $pType[$i] == "xfers") && $fundingId[$i] == 0 && $status[$i] == 'Paid' && ($paid[$i] != '2' && $paid[$i] != 'Paid')) {
                                                                             ?>
                                                                             <tr class="light">
                                                                             <?php } else { ?>
                                                                             <tr class="dark">
                                                                             <?php } ?>
                                                                             <td><?= $name[$i] ?></td>
                                                                             <td><?= $pType[$i] ?></td>
                                                                             <td>
                                                                                    <input name="amount" class="amount"  value="<?= $amount[$i] ?>" readonly/>
                                                                                    <input class="name" type="hidden" value="<?= $name[$i] ?>" />
                                                                                    <input class="invoiceId" type="hidden" value="<?= $invoiceId[$i] ?>"/>
                                                                                    <input class="tpayout" type="hidden" value="<?= $tpayout[$i] ?>"/>
                                                                                    <input class="emailAddr" type="hidden" value="<?= $emailAddr[$i] ?>"/>
                                                                             </td>

                                                                             <?php if ($paid[$i] == '2' || $paid[$i] == 'Paid') { ?>

                                                                                    <td><p class="nopay" style="background-color:green;color:white">Payout Done</p></td>

                                                                             <?php } else if ($fundingId[$i] != 0 and $pType[$i] != "Xfers") { ?>
                                                                                    <td><p class="nopay">Indonesia Investor</p></td>
                                                                             <?php } else if ($status[$i] != 'Paid') { ?>
                                                                                    <td><p class="nopay">Investment UnPaid</p></td>
                                                                             <?php } else if ($pType[$i] != "Xfers" && $pType[$i] != "xfers") { ?>
                                                                                    <td><p class="nopay">Not paid with Xfers</p></td>
                                                                             <?php } else { ?>
                                                                                    <td><button  class="btn btn-primary PayBtn" data-xtoken="<?= $xfersToken[$i] ?>" value="<?= $iId[$i] ?>"><i class="fa fa-arrow-right fa-2x"></i></button></td>

                                                                             <?php } ?>
												<td>
												  <?php if ($country[$i] != "SINGAPORE"): ?>
												    <?php if (($status[$i] == 'Paid' || $status[$i] == 'Wallet') and $paid[$i] != '2' and $paid[$i] != 'Paid') {?><button  class="btn btn-danger WalletBtn" value="<?= $iId[$i] ?>"><span>Pay to Wallet</span></button>
										                  <?php } ?>
                                                                                      <?php endif ?>
												
                                                                                    </td>

                                                                      <td width="100">
                                                                             <?#php echo ($country[$i]); ?>

                                                                             <?php if ($country[$i] == "SINGAPORE"): ?>
                                                                                    <?php if (($pType[$i] == "Bank Transfer" OR $pType[$i] == "bank transfer" OR $pType[$i] == "Bank transfer" OR $pType[$i] == "Xfers" OR $pType[$i] == "xfers") AND ($paid[$i] != '2')): ?>
                                                                                           <button type="button" value="<?= $iId[$i] ?>" class="btn btn-info PayTransfer"><span>Pay via Bank Transfer</span></button>
                                                                                    <?php endif ?>
                                                                             <?php endif ?>
                                                                      </td>

                                                                                                         <!--<td style="text-align: center"><button value="<?= $iId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>-->
                                                                      </tr>

                                                               <?php } ?>
                                                               <tr>
                                                                      <td colspan="2"><h4>Total</h4></td>
                                                                      <td colspan="4"><h4><?php echo "SGD " . number_format($total, 1, '.', ',') ?></h4></td>
                                                               </tr>
                                                        </tbody>



                                                 </table>








                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>