<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();


if (isset($_GET["add"])) {
       if ($_GET["add"] == "1") {
              if ($mysql->Connection()) {
                     $newpId = $mysql->AddNewPartner1();
                     header("Location: partner-edit.php?p=$newpId");
              }
       }
}


if ($mysql->Connection()) {
       list($pId, $pName, $pDes, $pImg, $pUrl, $enabled) = $mysql->GetPartners();
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Partners</title>

              <?php include_once 'include.php'; ?>

              <script>
                     $(document).ready(function () {
                            $(".DelBtn").click(function () {
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dpartner: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        location.reload();
                                                        
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });

                     });
              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Kapital Boost's Partners</span>
                                                 <a class="btn btn-warning" style="margin-left: 20px;width:200px" href="partners.php?add=1">Add New Partner</a>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">



                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="5%">No</th>
                                                                             <th width="25%">Partner Name</th>
                                                                             <th width="40%">Partner Image</th>
                                                                             <th width="*">Partner URL</th>
                                                                             <th width="7%">Delete</th>
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($pId); $i++) { ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><a href="partner-edit.php?p=<?= $pId[$i] ?>"><?= $pName[$i] ?></a></td>
                                                                                    <td>
                                                                                           <?php if ($pImg[$i] != "") { ?>
                                                                                                  <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/partner/<?= $pImg[$i] ?>" class="image-link" title="<?= $pName[$i] ?>"><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/partner/<?= $pImg[$i] ?>&w=150" alt=""></a><br>
                                                                                                  <?php } ?>
                                                                                    </td>
                                                                                    <td><?= $pUrl[$i] ?></td>
                                                                                    <td style="text-align: center"><button value="<?= $pId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                                                                             </tr>

                                                                      <?php } ?>

                                                               </tbody>
                                                        </table>
                                                 </div>





                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>