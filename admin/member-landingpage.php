<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$source = "";

if (isset($_GET["add"])) {
       if ($_GET["add"] == "1") {
              if ($mysql->Connection()) {
                     $newmId = $mysql->AddNewMember1();
                     header("Location: member-edit.php?m=$newmId");
              }
       }
}

if (isset($_GET["keyword"])) {
       $keyword = $_GET["keyword"];
} else {
       $keyword = "";
}
if ($mysql->Connection()) {

       list($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet, $mValCode) = $mysql->GetAllLandingPageMembers($keyword);


}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Members</title>

              <?php include_once 'include.php'; ?>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Kapital Boost's Members from Landing Page</span>

                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">



                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="5%">No</th>
                                                                             <th width="15%">Full Name</th>
                                                                             <th width="20%">Username</th>
                                                                             <th width="20%">Source</th>
                                                                             <th width="20%">Member Email</th>
                                                                             <th width="15%">Country</th>
                                                                             <th width="*">Created Date</th>
                                                                             <th width="*">Wallet Amount</th>
                                                                             <!--<th width="5%">Validated</th>-->
                                                                             <!--<th width="7%">Delete</th> -->
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($mId); $i++) { ?>
                                                                      <?php if ($mWallet[$i]==NULL or $mWallet[$i]==""){$mWallet[$i]=0;}
																		if (strpos($mValCode[$i], "landdua") !== false ) {
																			$source = "investor-sign-up";
																		} else if (strpos($mValCode[$i], "landtiga") !== false ) {
																			$source = "new-investor-signup";
																		} else if (strpos($mValCode[$i], "amrabu") !== false ) {
																			$source = "Amrabu Khadra";
																		} else {
																			$source = "investor-signup";
																		}
																	  ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><a href="member-edit.php?m=<?= $mId[$i] ?>"><?= $mFullname[$i] ?></a></td>
                                                                                    <td><?= $mUserName[$i] ?></td>
                                                                                    <td><?= $source ?></td>
                                                                                    <td><?= $mEmail[$i] ?></td>
                                                                                    <td><?= $mCountry[$i] ?></td>
                                                                                    <td><?= $mCreatedDate[$i] ?></td>
                                                                                    <td><a href="wallet.php?m=<?= $mId[$i] ?>">$ <?= $mWallet[$i] ?></a></td>
                                                                                    <!--<td>
                                                                                           <select class="validated" >
                                                                                                  <?php if ($validated[$i] == 0) { ?>
                                                                                                         <option value="<?= $mId[$i] ?>~0" selected="">NO</option>
                                                                                                         <option value="<?= $mId[$i] ?>~1" >YES</option>
                                                                                                  <?php } else { ?>
                                                                                                         <option value="<?= $mId[$i] ?>~0" >NO</option>
                                                                                                         <option value="<?= $mId[$i] ?>~1" selected="" >YES</option>
                                                                                                  <?php } ?>

                                                                                           </select>
                                                                                    </td>-->
                                                                                    <!--<td style="text-align: center"><button value="<?= $mId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button>&nbsp;&nbsp;&nbsp;</td>-->
                                                                             </tr>

                                                                      <?php } ?>

                                                               </tbody>
                                                        </table>
                                                 </div>





                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>

<script>
		$(document).ready( function () {
			$('#myTable').DataTable();
		} );
		</script>


       </body>


</html>
