<?php
	include_once 'security.php';
  include_once 'mysql.php';
  include_once '../libs/helper.php';

  $mysql = new mysql();

  if ($mysql->Connection()) {
  	switch ($_GET['api_action']) {

      case 'check-payout':

      	// $result = check_payout_calculation($_GET['id']);
      	$result = check_payout_calculation('', $_GET['id'], false);

      	// $ex_datas = array('datas' => array(
      	// 	'3949' => array ('tinv_id' => '3949', 'investor_name' => 'Rajena Begam', 'campaign_name' => 'Laras Mitra Lestari Abadi', 'status' => 1, 'total_payout' => 12312.2, 'expected_payout' => 12312.05, 'nominal' => 12312, 'msg' => ''),
      	// 	'3950' => array ('tinv_id' => '3950', 'investor_name' => 'Ebadullah Siddiq', 'campaign_name' => 'Laras Mitra Lestari Abadi', 'status' => 1, 'total_payout' => 3078, 'expected_payout' => 3078.92, 'nominal' => 3078, 'msg' => ''),
      	// 	'3951' => array ('tinv_id' => '3951', 'investor_name' => 'Mohamed Sahaf Ibrahim', 'campaign_name' => 'Laras Mitra Lestari Abadi', 'status' => 1, 'total_payout' => 21543, 'expected_payout' => 21546.44, 'nominal' => 21546, 'msg' => '')
      	// ), 'message' => 'Terdapat data yang tidak sesuai' );

				echo json_encode($result);
				// echo json_encode($ex_datas);
      	break;

      case 'update-payout':
      	$result = check_payout_calculation('', $_GET['id'], true);

      	echo json_encode($result);
      	break;

      case 'delete-contract-files':
        $id = $_GET['c'];

        list($result, $err, $id) = $mysql->deleteContractUKMFile($id);

        echo json_encode(array("status" => 200, "result" => $result, "err" => $err));
        break;

			case 'create-akad':
				list($result, $err, $id) = $mysql->createAkad($_POST['type']);

				if ($result) {
					header('Location: /admin/akad-generator/form.php?id='.$id);
				}else{
					echo json_encode(array("status" => 200, "result" => $result, "err" => $err, "message" => "Ops, Something when wrong, Please try again."));
				}
				break;

			case 'delete-akad':
				$params = array('is_active' => $_GET['val']);
				list($result, $err) = $mysql->updateDatas($params, 'akad', 'id', $_GET['id']);

				echo json_encode(array("status" => 200, "result" => $result, "err" => $err));
				break;
    }
  }
?>
