<?php
include_once 'security.php';

// include_once 'mysql.php';
// $mysql = new mysql();

$category = $_GET["show"];
if ($category != "paypal" && $category != "bank" && $category != "xfers" && $category != "unpaid") {
       $category = "";
}else if ($category == 'unpaid') {
       $category = "Unpaid";
}
// if (isset($_GET["keyword"])) {
//        $keyword = $_GET["keyword"];
// } else {
//        $keyword = "";
// }

// if ($mysql->Connection()) {
//        list($iId, $name, $email, $cName, $cType,$cSubType, $pType, $totalFunding, $payDate, $status) = $mysql->GetAllInvestmentsList($category, $keyword);
// }
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Investments - <?= $category ?></title>

              <?php include_once 'include.php'; ?>

              <script>
                     function deleteBtn($this){
                            $this = $($this);
                            var index = $this.closest('tr').index();
                            var person = prompt("Please enter Master Code");
                            $.post("delete.php", {dinvestment: $this.val(), MC: person}, function (reply) {
                                   reply = JSON.parse(reply);
                                   $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                          if (reply === "Done") {
                                                 // $(".even").eq(index ).remove();
                                                 $this.parents('tr').remove();
                                          }
                                   });
                                   $("#popupText").html(reply);
                            });
                     }
                     $(document).ready(function () {

                            $("#iName").autocomplete({
                                   source: "GetInvestorNames.php",
                                   minLength: 2,
                                   delay: 50,
                                   select: function (event, ui) {
                                          $("#iName").css("color", "green");
                                   }

                            });
                            $.post("<?php echo ADMIN_PATH ?>/posts.php", {job:"getcampaignnames"} ,function (data) {
                                   data = JSON.parse(data);
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("campaignSelect").appendChild(x);
                                   }
                            });

                            $('.selectpicker').selectpicker();
                            $('#datepicker').pickadate({
                                   format: 'dd/mm/yyyy'
                            });
                            $('#timepicker').pickatime({
                                   format: 'HH:i',
                                   interval: 10
                            });
                            $("#addIBtn").on("click", function () {
                                   var iName = $("#iName").val();
                                   var campaignSelect = $("#campaignSelect").val();
                                   var campaignName = campaignSelect.split("~")[0];
                                   var cId = campaignSelect.split("~")[1];
                                   var pType = $("#pType").val();
                                   var iAmount = $("#iAmount").val();
                                   var iDate = $("#datepicker").val();
                                   var iTime = $("#timepicker").val();
                                   var status = $("#status").val();
                                   iDate = iDate.replace(/\//g, "-");
                                   if (iName !== "" && campaignSelect !== "" && iAmount !== "") {
                                          $.post("SaveInvestment.php", {name: iName, campaign: campaignName, cid: cId, ptype: pType, amount: iAmount, date: iDate, time: iTime, status: status}, function (reply) {
              console.log(reply);
              reply = JSON.parse(reply);
                                                 if (reply == 1) {
                                                        $("#popup").fadeIn(200).delay(500).fadeOut(200, function () {
                                                               location.reload();
                                                        });
                                                        $("#popupText").html("Investment Added!");
                                                 } else {
                                                        $("#popup").fadeIn(200).delay(2000).fadeOut(200);
                                                        $("#popupText").html("Failed");
                                                 }
                                          });
                                   } else {
                                          $("#popup").fadeIn(200).delay(2000).fadeOut(200);
                                          $("#popupText").html("Please complete the Name, Campaign and Funding Amount");
                                   }
                            });
                     });

              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Investments - <?= $category ?></span>
                                                 <form method="get" action="investments.php" style="display:inline-block;margin-left: 20px">
                                                        <input type='hidden' name='show' value="<?= $category ?>"/>
                                                        <input type="text" name="keyword" placeholder="Investors' Name | Email | Campaign Name" value="<?= $keyword ?>" style="width: 500px;color:black;font-size:0.7em;padding:5px;border-radius:5px" />
                                                        <input class="btn btn-primary" type="submit" value="Search"/>
                                                 </form>
												  <form method="post" action="excel-extract.php" style="display:inline-block;">
														<input type="hidden" name="action"  value="investments"/>
                                                        <input class="btn btn-info" type="submit" value="Extract Investments Data"/>
                                                 </form>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">

                                                 <style>
                                                        .addNewIs{
                                                               margin: 10px
                                                        }
                                                        #addIBtn{
                                                               min-width: 100px;
                                                        }
                                                        #addIDiv{
                                                               background-color: #e6e6e6;border-radius: 10px;padding: 5px;margin:10px 20px 10px ;text-align: center
                                                        }
                                                        #iName,#iAmount,#datepicker,#timepicker{
                                                               border-radius: 5px;padding:5px;
                                                        }
                                                        #datepicker,#timepicker{
                                                               max-width: 100px
                                                        }


                                                 </style>

                                                 <!--<div style="text-align: center">
                                                        <label>Search with | Investors' Names | Emails | Countries | Campaign Name :</label>
                                                        <input type="text" id="keyword" name="keyword" placeholder="Default : Search ALL"   max-width="400px" style="border:1px solid black;border-radius:10px;margin:5px 20px;height:35px;width:300px;padding-left: 10px" />
                                                        <input type="button" class="btn btn-primary go" name="go" value="Search" onclick="Search()"  style="width: 80px" />
                                                 </div>-->

                                                 <div id="addIDiv" >
                                                        <input id="iName" style="width:300px" class="addNewIs" type="text" placeholder="Investors' Name" />
                                                        <select name="campaign" id="campaignSelect" class="cat_dropdown addNewIs">
                                                               <option value="">Select Campaign</option>
                                                        </select>
                                                        <select name="pType" id="pType"  class="addNewIs">
                                                               <option value="Bank Transfer" selected>Bank Transfer</option>
                                                               <option value="Xfers">Xfers</option>
                                                               <option value="Paypal">Paypal</option>
                                                        </select>
                                                        <input id="iAmount" type="text" placeholder="Funding Amount" class="addNewIs" />
                                                        <input  id="datepicker" type="date" placeholder="Date" />
                                                        <input id="timepicker" type="time" placeholder="Time"/>
                                                        <select name="status" id="status">
                                                               <option value="Paid">Paid</option>
                                                               <option value="Unpaid">Unpaid</option>
                                                        </select>
                                                        <button class="btn btn-info" id="addIBtn">Add New Investments</button>
                                                 </div>




                                                 <div class="table-responsive col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="3%">No</th>
                                                                             <th width="10%">Investor Name</th>
                                                                             <th width="10%">Investor Email</th>
                                                                             <th width="12%">Campaign Name</th>
                                                                             <th width="10%">Campaign Type</th>
                                                                             <th width="10%">Status Contract</th>
                                                                             <th width="10%"> Payment Type</th>
                                                                             <th width="8%">Total Funding</th>
                                                                             <th width="10%">Date Of Payment </th>
                                                                             <th width="5%"> Status</th>
                                                                             <th width="4%">Delete</th>
                                                                      </tr>
                                                               </thead>

                                                        </table>
                                                 </div>

                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>

              <script src="js/pickadate/picker.js"></script>
              <script src="js/pickadate/picker.date.js"></script>
              <script src="js/pickadate/picker.time.js"></script>
              <script src="js/pickadate/legacy.js"></script>
			<script>
			var myTable = $('#myTable').DataTable( {
                         "iDisplayLength": 10,
                          "bProcessing": true,
                          stateSave: true,
                          "sAjaxSource": "getJson.php?table=investment&cat=<?= $category ?>",
                          "aoColumns": [
                                { mData: 'no' },
                                { mData: 'nama' },
                                { mData: 'email' },
                                { mData: 'campaign' },
                                { mData: 'project_type' },
                                { mData: 'sign_contracts' },
                                { mData: 'tipe' },
                                { mData: 'total_funding' },
                                { mData: 'date' },
                                { mData: 'status' },
                                { mData: 'delete_button' }
                          ]
                      });
			</script>



       </body>


</html>
