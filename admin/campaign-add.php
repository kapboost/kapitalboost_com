<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();


if (isset($_GET["c"]) && $_GET["c"] == "new") {

       $cNamef = $_POST["campaign_name"];
       $cSnippet = $_POST["campaign_snippet"];
       $cDesf = $_POST["campaign_description"];
       $rDatef = $_POST["release_date"];
       $eDatef = $_POST["closing_date"];
       $enabledf = $_POST["enabled"];
       $classificationf = $_POST["classification"];
       $countryf = $_POST["country"];
       $totalAmountf = $_POST["total_funding_amt"];
       $industryf = $_POST["industry"];
       $minAmtf = $_POST["minimum_investment"];
       $tenorf = $_POST["tenor"];
       $currentAmountf = $_POST["curr_funding_amt"];
       $cReturnf = $_POST["project_return"];
       $cTypef = $_POST["project_type"];
       $cSubTypef = $_POST["campaign_subtype"];
       $cSubTypeShipf = $_POST["campaign_subtype_ship"];
       $privatePasswordf = $_POST["private_password"];

       $riskf = $_POST["risk"];
       $relatedc1f = $_POST["relatedc1"];
       $relatedc2f = $_POST["relatedc2"];
       $relatedc3f = $_POST["relatedc3"];
       $pdf_name = "";

       for ($i = 0; $i < 8; $i++) {
              if (isset($_FILES["pdf$i"]) && count($_FILES["pdf$i"]['error']) == 1 && $_FILES["pdf$i"]['error'][0] > 0) {
                     
              } else if (!is_uploaded_file($_FILES["pdf$i"]['tmp_name'])) {
                     
              } else if (isset($_FILES["pdf$i"])) {
                     $pdf[$i] = $_FILES["pdf$i"]["tmp_name"];
                     $pdf_name[$i] = str_replace(" ", "-", $cNamef) . $i;
                     move_uploaded_file($pdf[$i], "../assets/images/campaign-pdf/" . $pdf_name[$i]);
              }
              $pdfDes[$i] = $_POST["pdf_description$i"];
              $pdfsDesf .= $pdfDes[$i] . "~";
              $pdfsf .= $pdf_name[$i] . "~";
       }

       if ($mysql->Connection()) {
              $cId = $mysql->AddNewCampaign($cNamef, $cSnippet, $cDesf, $rDatef, $eDatef, $enabledf, $classificationf, $countryf, $totalAmountf, $industryf, $minAmtf, $tenorf, $currentAmountf, $cReturnf, $cTypef, $cSubTypef, $cSubTypeShipf, $privatePasswordf, $pdfsf, $pdfsDesf, $riskf, $relatedc1f, $relatedc2f, $relatedc3f);
       }
       $uploaddir = "../assets/images/campaign-content/";
       if (isset($_FILES["contentI1"])) {
              $file1 = $_FILES["contentI1"]["tmp_name"];
              $image_name1 = $_POST["contentI1h"];
              move_uploaded_file($file1, $uploaddir . $image_name1);
       }
       if (isset($_FILES["contentI2"])) {
              $file2 = $_FILES["contentI2"]["tmp_name"];
              $image_name2 = $_POST["contentI2h"];
              move_uploaded_file($file2, $uploaddir . $image_name2);
       }
       if (isset($_FILES["contentI3"])) {
              $file3 = $_FILES["contentI3"]["tmp_name"];
              $image_name3 = $_POST["contentI3h"];
              move_uploaded_file($file3, $uploaddir . $image_name3);
       }
       header("Location: campaign-edit.php?c=$cId");
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Add New Campaign</title>

              <?php include_once 'include.php'; ?>

              <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>



       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>




              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;text-align:center">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Add New Campaign</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">


                                                 <form action="campaign-add.php?c=new" method="POST" enctype="multipart/form-data">

                                                        <div class="row">
                                                               <div class="col-xs-12 col-md-6">

                                                                      <div class="form-group">
                                                                             <label>Campaign Name : </label>
                                                                             <input type="text" name="campaign_name"class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Item to be purchased : </label>
                                                                             <input type="text" name="classification" class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Project Type : </label><br/>
                                                                             <select name="project_type" id="project_type" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                    <option value="" selected="">-- Please Select --</option>
                                                                                    <option value="sme" >SME Crowdfunding</option>
                                                                                    <option value="donation">Donation</option>
                                                                                    <option value="private">Private Crowdfunding</option>  
                                                                             </select>
                                                                      </div>
                                                                      <div class="form-group">
                                                                             <div style="width:45%;display:inline-block">
                                                                                    <select name="campaign_subtype" id="campaign_subtype" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                           <option value="" selected="">-- Please Select --</option>
                                                                                           <option value="reward">Rewards Base</option>
                                                                                           <option value="interest">Interest-free Loan</option>
                                                                                           <option value="hybrid" >All</option>
                                                                                    </select>
                                                                             </div>
                                                                             <div style="width:45%;display:inline-block">
                                                                                    <label>Shipping Rewards :</label>
                                                                                    <select name="campaign_subtype_ship" id="campaign_subtype_ship" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                           <option value="0" selected="">No (Default)</option>
                                                                                           <option value="1" >Yes</option>

                                                                                    </select>
                                                                             </div>
                                                                      </div>

                                                                      <div class="form-group" id="private_password" style="display:none;">
                                                                             <label>Private Password : </label><br/>
                                                                             <input type="text" name="private_password"  class="form-control">
                                                                      </div>



                                                                      <div class="form-group">
                                                                             <label>Industry : </label>
                                                                             <input type="text" name="industry" industry  class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Risk : </label>
                                                                             <select name="risk" value="Medium" class="form-control">
                                                                                    <option value="N/A" selected="selected" >N/A</option>
                                                                                    <option value="Low">Low</option>
                                                                                    <option value="Medium">Medium</option>
                                                                                    <option value="High">High</option>

                                                                             </select>
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Country : </label>
                                                                             <select id="countries" name="country" class="form-control" data-live-search="true" data-style="btn-default">
                                                                                    <option value="">-----Select A Country----</option>
                                                                             </select>
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Release Date : </label>
                                                                             <input type="text" name="release_date" id="datepicker"  >
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Closing Date : </label>
                                                                             <input type="text" name="closing_date" id="datepicker">
                                                                      </div>
                                                                      <div>
                                                                             <label>Campaign Snippet</label>
                                                                             <textarea maxlength="130" name="campaign_snippet" style="width:100%"><?= $cSnippet ?></textarea>
                                                                      </div>

                                                               </div>

                                                               <div class="col-xs-12 col-md-6">
                                                                      <div class="form-group">
                                                                             <label>Total Funding AMT : </label>
                                                                             <input type="number" name="total_funding_amt"   class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Minimum Investment : </label>
                                                                             <input type="number" name="minimum_investment"  class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Tenor : </label>
                                                                             <input type="text" name="tenor"   class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Curr Funding AMT : </label>
                                                                             <input type="number" name="curr_funding_amt" class="form-control">
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Project Return : </label>
                                                                             <input type="text" name="project_return" class="form-control" placeholder="No % at the end. Just pure numbers">
                                                                      </div>

                                                                      <div style="min-height: 180px;">

                                                                             <div class="form-group">
                                                                                    <label>Campaign Logo : </label>
                                                                                    <input type="file" name="campaign_logo" >

                                                                             </div>

                                                                             <div class="form-group">
                                                                                    <label>Campaign Background : </label>
                                                                                    <input type="file" name="campaign_background" >

                                                                             </div>
                                                                      </div>
                                                               </div>

                                                        </div>
                                                        <style>
                                                               .divs{
                                                                      text-align: center;margin: 20px;background-color: #e6e6e6;border-radius: 10px;
                                                                      padding:20px 0
                                                               }
                                                               .delImgLi{
                                                                      margin:0 !important;padding:0 !important;border:none !important;margin: auto
                                                               }
                                                               .DelPdfBtn{
                                                                      margin-left: 15px;font-size:0.8em !important;padding:3px !important
                                                               }
                                                        </style>

                                                        <div class="row">
                                                               <div class="col-xs-12 col-md-12">
                                                                      <div class="form-group">
                                                                             <label>Campaign Description : </label>
                                                                             <textarea name="campaign_description" class="form-control"></textarea>
                                                                      </div>
                                                               </div>
                                                        </div>


                                                        <div class="row divs" style="text-align: center;margin-bottom: 10px">
                                                               <h3>Content Images</h3>
                                                               <div class="col-md-4 col-lg-4">
                                                                      <input id='contentI1' name="contentI1" type="file" />
                                                                      <input id='contentI1h' name='contentI1h' type="hidden" value="" />
                                                                      <p id='contentI1p' style='text-align: left'></p>
                                                               </div>
                                                               <div class="col-md-4 col-lg-4">
                                                                      <input id='contentI2' name='contentI2' type="file" />
                                                                      <input id='contentI2h' name='contentI2h' type="hidden" value="" />
                                                                      <p id='contentI2p' style='text-align: left'></p>
                                                               </div>
                                                               <div class="col-md-4 col-lg-4">
                                                                      <input id='contentI3' name='contentI3' type="file" />
                                                                      <input id='contentI3h'  name='contentI3h'type='hidden' value="" />
                                                                      <p id='contentI3p' style="text-align: left"></p>
                                                               </div>

                                                        </div>

                                                        <div class="row divs" style="text-align:left" >
                                                               <h3 style="text-align:center">PDFs</h3>
                                                               <?php for ($i = 0; $i < 8; $i++) { ?>
                                                                      <div class="col-md3 col-lg-3">
                                                                             <p>pdf <?= $i + 1 ?></p>
                                                                             <input id='pdf_description<?= $i ?>' name='pdf_description<?= $i ?>' placeholder="PDF Name" />
                                                                             <input id='pdf<?= $i ?>' name='pdf<?= $i ?>' type='file'/>

                                                                      </div>
                                                               <?php } ?>
                                                        </div>



                                                        <div class="row divs" style="text-align:center">
                                                               <h3>Gallery Images</h3>
                                                               <div class="col-md-4">
                                                                      <div class="form-group">
                                                                             <label>Image 1 : </label>
                                                                             <input type="file" name="image_1" >
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 2 : </label>
                                                                             <input type="file" name="image_2" >
                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 3 : </label>
                                                                             <input type="file" name="image_3" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 4 : </label>
                                                                             <input type="file" name="image_4" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 5 : </label>
                                                                             <input type="file" name="image_5" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 6 : </label>
                                                                             <input type="file" name="image_6" >

                                                                      </div>
                                                               </div>

                                                               <div class="col-md-4">
                                                                      <div class="form-group">
                                                                             <label>Image 7 : </label>
                                                                             <input type="file" name="image_7" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 8 : </label>
                                                                             <input type="file" name="image_8" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 9 : </label>
                                                                             <input type="file" name="image_9" >

                                                                      </div>



                                                                      <div class="form-group">
                                                                             <label>Image 10 : </label>
                                                                             <input type="file" name="image_10" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 11 : </label>
                                                                             <input type="file" name="image_11" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 12 : </label>
                                                                             <input type="file" name="image_12" >

                                                                      </div>
                                                               </div>

                                                               <div class="col-md-4">

                                                                      <div class="form-group">
                                                                             <label>Image 13 : </label>
                                                                             <input type="file" name="image_13" >

                                                                      </div>


                                                                      <div class="form-group">
                                                                             <label>Image 14 : </label>
                                                                             <input type="file" name="image_14" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 15 : </label>
                                                                             <input type="file" name="image_15" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 16 : </label>
                                                                             <input type="file" name="image_16" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 17 : </label>
                                                                             <input type="file" name="image_17" >

                                                                      </div>

                                                                      <div class="form-group">
                                                                             <label>Image 18 : </label>
                                                                             <input type="file" name="image_18" >

                                                                      </div>

                                                               </div>
                                                        </div>


                                                        <div class="row divs" > 
                                                               <h3>Related Campaigns</h3>
                                                               <div class="col-md-4">
                                                                      <h4>Related Campaign 1</h4>
                                                                      <select class="form-control" id="relatedc1" name="relatedc1"></select>
                                                               </div>
                                                               <div class="col-md-4">
                                                                      <h4>Related Campaign 2</h4>
                                                                      <select class="form-control"  id="relatedc2" name="relatedc2"></select>
                                                               </div>
                                                               <div class="col-md-4">
                                                                      <h4>Related Campaign 3</h4>
                                                                      <select class="form-control" id="relatedc3" name="relatedc3"></select>
                                                               </div>

                                                        </div>

                                                        <div class="row">

                                                               <div class="col-lg-12">
                                                                      <div class="form-group">
                                                                             <label>Status : </label>
                                                                             <select name="enabled">
                                                                                    <option value="1" >Online</option>
                                                                                    <option value ="0" selected>Offline</option>

                                                                             </select>
                                                                      </div>
                                                                      <input name="button2" type="submit" class="btn btn-primary" id="button2" value="Add New Campaign" style="margin-left:15px;margin-top:10px;"/>
                                                               </div>
                                                        </div>
                                                 </form>

                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


              <script>CKEDITOR.replace('campaign_description', {

                     });
              </script>

              <script src="js/pickadate/picker.js"></script>
              <script src="js/pickadate/picker.date.js"></script>
              <script src="js/pickadate/picker.time.js"></script>
              <script src="js/pickadate/legacy.js"></script>

              <script type="text/javascript">
                     $(document).ready(function () {
                            $.post("<?php echo ADMIN_PATH ?>/posts.php", {job: "getallcountries"}, function (reply) {
                                   reply = JSON.parse(reply);
                                   $("#countries").append(reply);
                            });
                            $.post("<?php echo ADMIN_PATH ?>/posts.php", {job: "getcampaignnames"}, function (data) {
                                   data = JSON.parse(data);
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("relatedc1").appendChild(x);
                                   }
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);

                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("relatedc2").appendChild(x);
                                   }
                                   for (var i = 0; i < data.length; i++) {
                                          var x = document.createElement("OPTION");
                                          x.setAttribute("value", data[i]);
                                          var t = document.createTextNode(data[i]);
                                          x.appendChild(t);
                                          document.getElementById("relatedc3").appendChild(x);
                                   }
                            });
                            $('.selectpicker').selectpicker();


                            $('#datepicker,#datepicker2').pickadate({
                                   format: 'dd-mm-yyyy'
                            });




                            $("#button").click(function () {
                                   location.reload();
                            });

                            $("#contentI1").on('click', function () {
                                   var timeNow = Math.floor(Date.now() / 1000);
                                   $("#contentI1h").val(timeNow);
                                   $("#contentI1p").html("https://kapitalboost.com/assets/images/campaign-content/" + timeNow);
                            });
                            $("#contentI2").on('click', function () {
                                   var timeNow = Math.floor(Date.now() / 1000);
                                   $("#contentI2h").val(timeNow);
                                   $("#contentI2p").html("https://kapitalboost.com/assets/images/campaign-content/" + timeNow);
                            });
                            $("#contentI3").on('click', function () {
                                   var timeNow = Math.floor(Date.now() / 1000);
                                   $("#contentI3h").val(timeNow);
                                   $("#contentI3p").html("https://kapitalboost.com/assets/images/campaign-content/" + timeNow);
                            });


                     });
              </script>

       </body>


</html> 