<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($eId, $eName, $eEmail, $eMsg,$date) = $mysql->GetMessages();
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Messages</title>

              <?php include_once 'include.php'; ?>

              <script>
                     $(document).ready(function () {
                            $(".DelBtn").click(function () {
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dmessages: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        location.reload();
                                                        
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });

                     });
              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Kapital Boost's Contact Us Messages</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">



                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="2%">No</th>
                                                                             <th width="5%">Name</th>
                                                                             <th width="7%">Email</th>
                                                                             <th width="*%">Message</th>
                                                                             <th width="10%">Date</th>
                                                                             <th width="7%">Delete</th>
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($eId); $i++) { ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><?=$eName[$i]?></td>
                                                                                    <td><?=$eEmail[$i]?></td>
                                                                                    <td><?=$eMsg[$i]?></td>
                                                                                    <td><?=$date[$i]?></td>
                                                                                    <td style="text-align: center"><button value="<?= $eId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                                                                             </tr>

                                                                      <?php } ?>

                                                               </tbody>
                                                        </table>
                                                 </div>





                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


<script>
		$(document).ready( function () {
			$('#myTable').DataTable();
		} );
		</script>

       </body>


</html>