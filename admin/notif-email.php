<?php
include_once 'mysql.php';
$mysql = new mysql();

include_once 'email.php';
$email = new email();

// Fungsi Untuk Edit Tanggal Indonesia
function tanggal_indo($tanggal, $cetak_hari = false)
{
	$hari = array ( 1 =>    'Senin',
				'Selasa',
				'Rabu',
				'Kamis',
				'Jumat',
				'Sabtu',
				'Minggu'
			);
			
	$bulan = array (1 =>   'Januari',
				'Februari',
				'Maret',
				'April',
				'Mei',
				'Juni',
				'Juli',
				'Agustus',
				'September',
				'Oktober',
				'November',
				'Desember'
			);
	$split 	  = explode('-', $tanggal);
	$tgl_indo = $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	
	if ($cetak_hari) {
		$num = date('N', strtotime($tanggal));
		return $hari[$num] . ', ' . $tgl_indo;
	}
	return $tgl_indo;
}

// Get Data on Database
if ($mysql->Connection()) {
    list($cId, $cName, $projectType, $mPayout, $dirEmail, $companyName) = $mysql->GetAllCampaignsListEmail();
}

// $no = '1';


for ($i = 0; $i < count($cId); $i++) {
    
    // echo $cId[$i] .'&nbsp'. $projectType[$i] .'&nbsp'. $mPayout[$i] .'<br>';

    // echo $no++. ') ';
    // echo $cName[$i]. ' - ';
    // echo $dirEmail[$i];
    // echo "<br>";
    

    $payoutA = explode("==", $mPayout[$i]);

    for($x = 0; $x < count($payoutA); $x++) {
        
        $payouts = explode("~", $payoutA[$x]);

        // Kondisi Jika Kosong Set Null
        $dateA[$i] = isset($payouts[1]) ? $payouts[1] : null;
        $statusA[$i] = isset($payouts[2]) ? $payouts[2] : null;

        // Ganti Format Tanggal
        $dateNow = date('Y-m-d');
        $dateDatabase = date("Y-m-d", strtotime($dateA[$i]));

        // Hitung Selisih Tanggal DB - Tanggal Sekarang
        $dateDB = strtotime($dateDatabase);
        $dateToday = time();
        $diff = $dateDB - $dateToday;
        $sendDate = floor($diff / (60 * 60 * 24)) + 1;

        // Edit Format Tanggal & Hari Indonesia
        $dateIndo = date('Y-m-d', strtotime($dateA[$i]));
        
        // Kondisi Kirim Email
        if ($statusA[$i] == '1' && $sendDate == '30') {

            $emailFile = "notif-email.text";

            $message = file_get_contents($emailFile);
            $message = str_replace("#Perusahaan#", $companyName[$i], $message);
            
            // $message = str_replace("#TanggalJatuhTempo#", $dateA[$i], $message);
            $message = str_replace("#TanggalJatuhTempo#", tanggal_indo($dateIndo, true), $message);
            $message = str_replace("#SisaHari#", $sendDate. ' Hari', $message);

            $email->SendNotifEmail($dirEmail[$i], "finance@kapitalboost.com", "Kapital Boost - Jatuh Tempo Pembayaran Campaign ". $cName[$i], $message);
            // print_r($message);

        }elseif($statusA[$i] == '1' && $sendDate == '15'){

            $emailFile = "notif-email.text";

            $message = file_get_contents($emailFile);
            $message = str_replace("#Perusahaan#", $companyName[$i], $message);

            // $message = str_replace("#TanggalJatuhTempo#", $dateA[$i], $message);
            $message = str_replace("#TanggalJatuhTempo#", tanggal_indo($dateIndo, true), $message);
            $message = str_replace("#SisaHari#", $sendDate. ' Hari', $message);

            $email->SendNotifEmail($dirEmail[$i], "finance@kapitalboost.com", "Kapital Boost - Jatuh Tempo Pembayaran Campaign ". $cName[$i], $message);
            // print_r($message);
            

        }elseif($statusA[$i] == '1' && $sendDate == '7'){

            $emailFile = "notif-email.text";

            $message = file_get_contents($emailFile);
            $message = str_replace("#Perusahaan#", $companyName[$i], $message);
            
            // $message = str_replace("#TanggalJatuhTempo#", $dateA[$i], $message);
            $message = str_replace("#TanggalJatuhTempo#", tanggal_indo($dateIndo, true), $message);
            $message = str_replace("#SisaHari#", $sendDate. ' Hari', $message);

            $email->SendNotifEmail($dirEmail[$i], "finance@kapitalboost.com", "Kapital Boost - Jatuh Tempo Pembayaran Campaign ". $cName[$i], $message);
            // print_r($cName[$i]);

        }

                       
        
    }

   
}

?>