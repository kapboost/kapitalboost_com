<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$mId = $_GET["m"];

if (isset($_POST["member_username"])) {
   $mUserNamef = $_POST["member_username"];
   $mFirstnamef = $_POST["member_firstname"];
   $mLastnamef = $_POST["member_lastname"];
   $mEmailf = $_POST["member_email"];
   $mIcNamef = $_POST["member_icname"];
   $mIcCountryf = $_POST["ic_country"];
   $mIcOptionf = $_POST["ic_option"];
   $mIcIdf = $_POST["ic_id"];
   $autoContractf = $_POST["auto_contract"];
   $mNricf = $_POST["nric"];
   $mDobf = $_POST["dob"];
   $mAddressf = $_POST["address"];
   $mPhonef = $_POST["member_mobile"];
   $mCountryf = $_POST["member_country"];
   $mNationalityf = $_POST["member_nationality"];
   $member_status = $_POST["member_status"];
   $cke1f = $_POST["check_1"];
   $cke2f = $_POST["check_2"];
   $cke3f = $_POST["check_3"];
   $cke4f = $_POST["check_4"];
   $cke5f = $_POST["check_5"];
   $cke6f = $_POST["check_6"];
   $cke7f = $_POST["check_7"];
   $cke8f = $_POST["check_8"];
   $cke9f = $_POST["check_9"];
   $cke10f = $_POST["check_10"];
   $cke11f = $_POST["check_11"];
   $cke12f = $_POST["check_12"];

   $arr = explode("@", $mEmailf, 2);
	$first = $arr[0];
	// echo "'<script>console.log(\"$first\")</script>'";

   if ($mUserNamef == NULL or $mUserNamef==""){

		$mUserNamef = $first;
   }


   // NRIC Front
   $nric_imgname = $_FILES["fileToUpload"]["name"];
   $nric_pisah = explode(".", $nric_imgname);
   $nric_imgtype = end($nric_pisah);
   $nric_file_img = $_FILES["fileToUpload"]["tmp_name"];
   $nric_newname = time() . rand() . "." . $nric_imgtype;
   if ($nric_imgtype == "") {
      $nric_newname = $_POST["nricFile"];
   }

   // NRIC Back
   $nric_imgname_back = $_FILES["nric_back"]["name"];
   $nric_pisah_back = explode(".", $nric_imgname_back);
   $nric_imgtype_back = end($nric_pisah_back);
   $nric_file_img_back = $_FILES["nric_back"]["tmp_name"];
   $nric_back_newname = time() . rand() . "." . $nric_imgtype_back;
   if ($nric_imgtype_back == "") {
      $nric_back_newname = $_POST["nricFileBack"];
   }

   // Address Proof
   $apName = $_FILES["address_proof"]["name"];
   $apExt = explode(".", $apName);
   $apType = end($apExt);
   $ap_file = $_FILES["address_proof"]["tmp_name"];
   $ap_newname = time() . rand() . "." . $apType;
   if ($apType == "") {
      $ap_newname = $_POST["addressProof"];
   }


   if ($mysql->Connection()) {

      if (!empty($nric_file_img)) {
         $mysql->UpdateUploadedMemberIcFront($mId, $nric_newname);
         move_uploaded_file($nric_file_img, "../assets/nric/datanric/" . $nric_newname);
      }
      if (!empty($nric_file_img_back)) {
         $mysql->UpdateUploadedMemberIcBack($mId, $nric_back_newname);
         move_uploaded_file($nric_file_img_back, "../assets/nric/datanricback/" . $nric_back_newname);
      }
      if (!empty($ap_file)) {
         $mysql->UpdateUploadedMemberAP($mId, $ap_newname);
         move_uploaded_file($ap_file, "../assets/nric/addressproof/" . $ap_newname);
      }
   }
   if ($mysql->Connection()) {

      $mysql->UpdateSingleMember($mId, $mUserNamef, $mFirstnamef, $mLastnamef, $mEmailf, $mIcNamef, $mIcCountryf,$mIcOptionf,$mIcIdf,$autoContractf, $mNricf, $mDobf, $mAddressf, $mPhonef, $mCountryf, $mNationalityf, $cke1f, $cke2f, $cke3f, $cke4f, $cke5f, $cke6f, $cke7f, $cke8f, $cke9f, $cke10f, $cke11f, $cke12f, $member_status);

      $access_token = $mysql->GetXfersAccessToken($mId);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);



      if ($access_token != "0") {
         $arr_xudata = array(
             "first_name" => $mFirstnamef,
             "last_name" => $mLastnamef,
             "email" => $mEmailf,
             "date_of_birth" => $mDobf,
             "address_line_1" => $mAddressf,
             "nationality" => $mNationalityf,
             "identity_no" => $mNricf,
             "country" => $mCountryf,
             "id_front_url" => "https://kapitalboost.com/assets/nric/datanric/$nric_newname",
             "id_back_url" => "https://kapitalboost.com/assets/nric/datanricback/$nric_back_newname",
             "proof_of_address_url" => "https://kapitalboost.com/assets/nric/addressproof/$ap_newname"
         );

         $xudata = json_encode($arr_xudata);

         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => "https://www.xfers.io/api/v3/user",
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_SSL_VERIFYPEER => false,
             CURLOPT_ENCODING => "",
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 50,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "PUT",
             CURLOPT_POSTFIELDS => $xudata,
             CURLOPT_HTTPHEADER => array(
                 "cache-control: no-cache",
                 "content-type: application/json",
                 "x-xfers-user-api-key: $access_token"
             ),
         ));

         $response = curl_exec($curl);
         $error = curl_error($curl);

         curl_close($curl);

         $mysql->UpdateXfersInfo($mId, $response);
      }
   }
   if (isset($_POST["member_password"]) && $_POST["member_password"] != "") {
      $mPasswordf = $_POST["member_password"];
      if ($mysql->Connection()) {
         $mysql->UpdateSingleMemberPassword($mId, $mPasswordf);
      }
   }

   // MailChimp Integration

   $mcArray1 = array(
       "email_address" => "$mEmailf",
       "status" => "subscribed",
       "merge_fields" => array("FNAME" => "$mFirstnamef", "LNAME" => "$mLastnamef", "MNUMBER" => "$mPhonef", "COUNTRY" => "$mCountryf")
   );
   $mcJson1 = json_encode($mcArray1);
   $mcCurl1 = curl_init();
   curl_setopt_array($mcCurl, array(
       CURLOPT_URL => "https://us11.api.mailchimp.com/3.0/lists/c2bdf8bd59/members",
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_SSL_VERIFYPEER => false,
       CURLOPT_ENCODING => "",
       CURLOPT_MAXREDIRS => 10,
       CURLOPT_TIMEOUT => 50,
       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
       CURLOPT_CUSTOMREQUEST => "POST",
       CURLOPT_POSTFIELDS => $mcJson1,
       CURLOPT_HTTPHEADER => array(
           "cache-control: no-cache",
           "content-type: application/json",
           "authorization: Basic b3dlbjoxOGQ0ZjNlNzAxNTQ3ZWUwYzljYzk4MmE0YjI1NDFhMC11czEx"
       ),
   ));
   curl_close($mcCurl1);

   $mcArray = array(
       "email_address" => "$mEmailf",
       "status" => "subscribed",
       "merge_fields" => array("FNAME" => "$mFirstnamef", "LNAME" => "$mLastnamef", "MNUMBER" => "$mPhonef", "COUNTRY" => "$mCountryf")
   );
   $md5Email = md5($mEmailf);
   $mcJson = json_encode($mcArray);
   $mcCurl = curl_init();
   curl_setopt_array($mcCurl, array(
       CURLOPT_URL => "https://us11.api.mailchimp.com/3.0/lists/c2bdf8bd59/members/$md5Email",
       CURLOPT_RETURNTRANSFER => true,
       CURLOPT_SSL_VERIFYPEER => false,
       CURLOPT_ENCODING => "",
       CURLOPT_MAXREDIRS => 10,
       CURLOPT_TIMEOUT => 50,
       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
       CURLOPT_CUSTOMREQUEST => "PUT",
       CURLOPT_POSTFIELDS => $mcJson,
       CURLOPT_HTTPHEADER => array(
           "cache-control: no-cache",
           "content-type: application/json",
           "authorization: Basic b3dlbjoxOGQ0ZjNlNzAxNTQ3ZWUwYzljYzk4MmE0YjI1NDFhMC11czEx"
       ),
   ));
   $response = curl_exec($mcCurl);
   $error = curl_error($mcCurl);
   curl_close($mcCurl);
}


if ($mysql->Connection()) {
   list($mUserName, $mEmail, $mFirstName, $mLastName, $mIcName, $mNric, $mIcCountry, $mIcOption, $mIcId, $autoContract, $mDob, $mAddress, $mPhone, $mNationality, $mCountry, $cke1, $cke2, $cke3, $cke4, $cke5, $cke6, $cke7, $cke8, $cke9, $cke10, $cke11, $cke12, $nricFile, $nricFileBack, $addressProof) = $mysql->GetSingleMembers($mId);
   $howYouKnowUs = $mysql->GetHowYouKnowUs($mId);
   $member_status = $mysql->GetMemberStatus($mId);
   switch ($howYouKnowUs) {
		case "1":
			$howYouKnowUs = "Search Engine";
			break;
		case "2":
			$howYouKnowUs = "Ads";
			break;
		case "3":
			$howYouKnowUs = "Social Media";
			break;
		case "4":
			$howYouKnowUs = "Friend/Family";
			break;
		case "5":
			$howYouKnowUs = "Community Event";
			break;
		case "6":
			$howYouKnowUs = "News/Blog/Magazine";
			break;
		default:
			$howYouKnowUs = "Other";
	}

  if (isset($_POST['account_name'])) {
     // print_r($_POST);
     // exit();
    unset($_POST['member_id']);
    list($result, $err, $id) = $mysql->updateDatas($_POST, "bank_account", "member_id", $mId);
    // echo $err;
    // exit();
  }

  $data_bank_account = $mysql->getDatas("bank_account", "member_id", $mId, false);
  if (is_null($data_bank_account)) {
    $bank_account_id = $mysql->createBankAccount($mId);

    if ($bank_account_id) {
      $data_bank_account = $mysql->getDatas("bank_account", "member_id", $mId, false);
    }
  }

  if (!is_null($data_bank_account->bank_code)) {
    $data_bic = $mysql->getDatas('bic_code', 'bank_code', $data_bank_account->bank_code);
  }
  list($bankCode, $bankName) = $mysql->GetBankCodeList();
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KB Admin Member Detail - <?= $mFirstName ?></title>

      <?php include_once 'include.php'; ?>

      <script>
          function sensative() {
             var person = prompt("Please enter Master Code");
             $.post("delete.php", {sensative: "", MC: person}, function (reply) {
                reply = JSON.parse(reply);
                $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                   if (reply === "Granted") {
                      $(".form-control").prop('readonly', false);
                      $(".form-control").prop('onclick', null).off('click');
                   }
                });
                $("#popupText").html(reply);
             });
          }


          $(document).ready(function () {
             var nationality = "<?= $mNationality ?>";
             var country = "<?= $mCountry ?>";
             var icCountry = "<?=$mIcCountry?>";
             $.post("posts.php", {job: 'getallnationalities'}, function (data) {
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {
                   var x = document.createElement("OPTION");
                   x.setAttribute("value", data[i]);
                   if (data[i] == nationality) {
                      x.setAttribute("selected", "true");
                   }
                   var t = document.createTextNode(data[i]);
                   x.appendChild(t);
                   document.getElementById("nationality").appendChild(x);
                }
             });
             $.post("posts.php", {job: 'getallcountriessimple'}, function (data) {
                data = JSON.parse(data);
                for (var i = 0; i < data.length; i++) {
                   var x = document.createElement("OPTION");
                   x.setAttribute("value", data[i]);
                   if (data[i] == country) {
                      x.setAttribute("selected", "true");
                   }
                   var t = document.createTextNode(data[i]);
                   x.appendChild(t);
                   document.getElementById("country").appendChild(x);

                   var a = document.createElement("OPTION");
                   a.setAttribute("value", data[i]);
                   if (data[i] == icCountry) {
                      a.setAttribute("selected", "true");
                   }
                   var b = document.createTextNode(data[i]);
                   a.appendChild(b);
                   document.getElementById("ic_country").appendChild(a);

                }
             });
          });
      </script>

   </head>
   <body>
      <?php include_once 'header.php'; ?>
      <?php include_once 'popup.php'; ?>


      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Member Detail - <?= $mFullname ?></span>
                     <div class="clearfix"></div>
                     <div class="pull-right" style="margin-top: 5px;">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#formBankAccount">Bank Account</button>
                     </div>
                  </div>

                  <div class="panel-body" style="text-align: center">


                     <form action="" method="POST" enctype="multipart/form-data">

                        <div class="col-xs-12 col-md-6">

                           <div class="form-group">
                              <label>User Name : </label>
                              <input onclick="sensative()"  type="text" name="member_username" value="<?= $mUserName ?>" class="form-control" readonly="">
                           </div>

                           <div class="form-group">
                              <label>First Name : </label>
                              <input onclick="sensative()" type="text" name="member_firstname" value="<?= $mFirstName ?>" class="form-control" readonly="">
                           </div>

                           <div class="form-group">
                              <label>Last Name : </label>
                              <input onclick="sensative()" type="text" name="member_lastname" value="<?= $mLastName ?>" class="form-control" readonly="">
                           </div>
                           <div class="form-group">
                              <label>Full Name in IC : </label>
                              <input onclick="sensative()" type="text" name="member_icname" value="<?= $mIcName ?>" class="form-control" readonly="" >
                           </div>

                           <div class="form-group">
                              <label>Email : </label>
                              <input onclick="sensative()" type="text" name="member_email" value="<?= $mEmail ?>" class="form-control" readonly="">
                           </div>

                           <div class="form-group">
                              <label>Auto Contract : </label>
                              <select name="auto_contract" id="auto_contract" class="form-control">
                                 <?php if ($autoContract == 0) { ?>
                                    <option value="0" selected="">Disabled</option>
                                    <option value="1">Enabled</option>
                                 <?php } else { ?>
                                    <option value="0">Disabled</option>
                                    <option value="1" selected="">Enabled</option>
                                 <?php } ?>
                              </select>
                           </div>

                           <div class="form-group">
                              <label>Password : </label>
                              <input onclick="sensative()" type="text" name="member_password" placeholder="!! Overwrite the existing password !!" value="" class="form-control" readonly="">
                           </div>

                           <div class="form-group">
                              <label>How You Know Us : </label>
                              <input onclick="sensative()" type="text" name="how_you_know"  value="<?= $howYouKnowUs ?>" class="form-control" readonly="">
                           </div>
						   <div class="form-group">
                              <label>Status Member : </label>
                              <select name="member_status" id="member_status" class="cat_dropdown form-control" >
                                 <option value="0" <?php if ($member_status == 0) echo "selected"; ?>>New</option>
                                 <option value="1" <?php if ($member_status == 1) echo "selected"; ?>>Waiting to be Reviewed</option>
                                 <option value="2" <?php if ($member_status == 2) echo "selected"; ?>>Approved</option>
                                 <option value="3" <?php if ($member_status == 3) echo "selected"; ?>>Blacklisted</option>
                                 <option value="4" <?php if ($member_status == 4) echo "selected"; ?>>Rejected</option>
                              </select>
                           </div>
                        </div>
                        <div class="col-xs-12 col-md-6">

                           <div class="form-group">
                              <label>IC Country : </label>
                              <select name="ic_country" id="ic_country" class="form-control">
                                 <option value="">----------</option>
                              </select>
                           </div>

                           <div class="form-group">
                              <label>IC Type : </label>
                              <select name="ic_option" class="form-control">
                                 <?php if ($mIcOption == 'IC') { ?>
                                    <option>Select One</option>
                                    <option value="IC" selected="">Identity Card</option>
                                    <option value="FIN">FIN</option>
                                    <option value="Passport">Passport</option>
                                 <?php } else if ($mIcOption == 'FIN') { ?>
                                    <option>Select One</option>
                                    <option value="IC">Identity Card</option>
                                    <option value="FIN" selected="">FIN</option>
                                    <option value="Passport">Passport</option>
                                 <?php } else if ($mIcOption == 'Passport') { ?>
                                    <option>Select One</option>
                                    <option value="IC">Identity Card</option>
                                    <option value="FIN">FIN</option>
                                    <option value="Passport" selected="">Passport</option>
                                 <?php } else { ?>
                                    <option value="" selected="">Select One</option>
                                    <option value="IC">Identity Card</option>
                                    <option value="FIN">FIN</option>
                                    <option value="Passport">Passport</option>
                                 <?php } ?>
                              </select>

                           </div>

                           <div class="form-group">
                              <label>IC Country  + Type (Bahasa) + ID No : </label>
                              <input  type="text" name="ic_id" value="<?= $mIcId ?>" class="form-control" placeholder="Kartu Identitas Singapura">
                           </div>

                           <div class="form-group">
                              <label>NRIC/Pasport No : </label>
                              <input onclick="sensative()" type="text" name="nric" value="<?= $mNric ?>" class="form-control" readonly="">
                           </div>
                           <div class="form-group">
                              <label>DOB: </label>
                              <input onclick="sensative()" type="text" id="dob" name="dob" value="<?= $mDob ?>" class="form-control" readonly="">
                           </div>

                           <div class="form-group">
                              <label>Residential Address  : </label>
                              <input onclick="sensative()" type="text" name="address" value="<?= $mAddress ?>" class="form-control" readonly="">
                           </div>
                           <div class="form-group">
                              <label>Handphone Number  : </label>
                              <input onclick="sensative()" type="text" name="member_mobile" value="<?= $mPhone ?>" class="form-control" readonly="">
                           </div>
                           <div class="form-group">
                              <label>Country : </label>
                              <select name="member_country" id="country" class="cat_dropdown form-control" >
                                 <option value="">----------</option>
                              </select>
                           </div>
                           <div class="form-group">
                              <label>Nationality : </label>
                              <select name="member_nationality" id="nationality" class="cat_dropdown form-control" >
                                 <option value="">----------</option>
                              </select>
                           </div>
                        </div>
                        <div class="row">
                           <br><hr><br>
                           <div class="form-group col-xs-12 col-md-4 col-lg-4 col-sm-4">

                              <label for="CAT_Custom_20043780_157746">Upload copy of NRIC (front)</label>
                              <br />
                              <input type="file" name="fileToUpload" style="margin: auto" />
                              <?php if ($nricFile != "") { ?>
                                 <div class="uploadImg">
                                    <p>
                                       <br />Current NRIC File Front<br />
                                    </p>
                                    <img src="../assets/nric/datanric/<?php echo $nricFile; ?>"  width="80%" />

									<?php
									$checkLastExtt = substr($nricFile, -4);
									if ($checkLastExtt==".pdf") {
										echo "<a href='/assets/nric/datanric/$nricFile'> Download NRIC (front) </a>";
									}
									?>

                                    <input type="hidden" name="nricFile" value="<?= $nricFile ?>" />
                                 </div>
                              <?php } else { ?>
                                 <br/>
                                 <p>No NRIC (front) uploaded</p>
                              <?php } ?>
                           </div>

                           <div class="form-group col-xs-12 col-md-4 col-lg-4 col-sm-4">

                              <label for="CAT_Custom_20043780_157746">Upload copy of NRIC (back)</label>
                              <br />
                              <input type="file" name="nric_back" style="margin: auto" />
                              <?php if ($nricFileBack != "") { ?>
                                 <div class="uploadImg">
                                    <p>
                                       <br />Current NRIC File Back<br />
                                    </p>
                                    <img src="/assets/nric/datanricback/<?= $nricFileBack ?>" width="80%" />

									<?php
									$checkLastExts = substr($nricFileBack, -4);
									if ($checkLastExts==".pdf") {
										echo "<a href='/assets/nric/datanricback/$nricFileBack'> NRIC File Back</a>";
									}
									?>

                                    <input type="hidden" name="nricFileBack" value="<?= $nricFileBack ?>"/>
                                 </div>
                              <?php } else { ?>
                                 <br/>
                                 <p>No NRIC (back) uploaded</p>
                              <?php } ?>
                           </div>
                           <div class="form-group col-xs-12 col-md-4 col-lg-4 col-sm-4">

                              <label for="CAT_Custom_20043780_157746">Upload copy of Address Proof</label>
                              <br />
                              <input type="file" name="address_proof" style="margin: auto" />
                              <?php if ($addressProof != "") { ?>
                                 <div class="uploadImg">
                                    <p>
                                       <br />Current Address Proof<br />
                                    </p>
                                    <!-- <img src="/assets/nric/addressproof/<? //$addressProof ?>"  width="80%" /> -->

									<?php
									$checkLastExt = substr($addressProof, -4);

                  if ($checkLastExt==".pdf" or $checkLastExt==".PDF") {
										echo "<a href='/assets/nric/addressproof/$addressProof'> Download NRIC File </a>";
									// }else if ($checkLastExtAddress == ".png" or $checkLastExtAddress == ".jpg" or $checkLastExtAddress == ".PNG" or $checkLastExtAddress == ".JPG"){
									}else if ($checkLastExt == ".png" or $checkLastExt == ".jpg" or $checkLastExt == "jpeg" or $checkLastExt == ".PNG" or $checkLastExt == ".JPG" or $checkLastExt == "JPEG"){
					echo "<img src='/assets/nric/addressproof/".$addressProof."'  width='80%' />";
                  }
									?>
									<br />
                                    <input type="hidden" name="addressProof" value="<?= $addressProof ?>" />
                                 </div>
                              <?php } else { ?>
                                 <br/>
                                 <p>No Proof of Address uploaded</p>
                              <?php } ?>
                           </div>
                           <br><hr><br>
						   </div>
                           <div>

                              <div class="invest-form-layer" style="margin-bottom: 30px;margin-top: 25px;">

                                 <input class="ver-inv" type="hidden" value="" />
                                 <label class="labelIn2">Which of these financial products have <?= $mFirstName ?> invested in?</label>
                                 <div class="checkIn">
                                    <?php if ($cke1 != NULL) { ?>
                                       <input type="checkbox" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks" checked/> Stocks
                                    <?php } else { ?>
                                       <input type="checkbox" name="check_1" id="CAT_Custom_20047261_157746_0" value="Stocks"/> Stocks
                                    <?php } ?>
                                    <?php if ($cke2 != NULL) { ?>
                                       <input type="checkbox" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" checked/>Bonds
                                    <?php } else { ?>
                                       <input type="checkbox" name="check_2" id="CAT_Custom_20047261_157746_1" value="Bonds" />Bonds
                                    <?php } ?>
                                    <?php if ($cke3 != NULL) { ?>
                                       <input type="checkbox" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" checked/>Mutual funds
                                    <?php } else { ?>
                                       <input type="checkbox" name="check_3" id="CAT_Custom_20047261_157746_2" value="Mutual funds" />Mutual funds
                                    <?php } ?>
                                    <?php if ($cke4 != NULL) { ?>
                                       <input type="checkbox" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" checked/>Alternative investments (specify)
                                    <?php } else { ?>
                                       <input type="checkbox" name="check_4" id="CAT_Custom_20047261_157746_5" value="Alternative investments (specify)" /> Alternative investments (specify)
                                    <?php } ?>
                                    <?php if ($cke5 != NULL) { ?>
                                       <input type="checkbox" name="check_5" id="CAT_Custom_20047261_157746_4" value="None of the above" checked/> None of the above
                                    <?php } else { ?>
                                       <input type="checkbox" name="check_5" id="CAT_Custom_20047261_157746_4" value="None of the above" /> None of the above
                                    <?php } ?>

                                 </div>
                              </div>
                              <div class="" style="padding-right: 0;padding-left: 0;width: 97%;margin-top: 25px;">

                                 <div class="invest-form-layer" style="margin-bottom: 30px;padding-right: 0;padding-left: 0;width: 100%;margin-top: 25px;">
                                    <input class="ver-inv" type="hidden" value="" />
                                    <label class="labelIn2"><?= $mFirstName ?>'s interest in investing through Kapital Boost</label>
                                    <div class="checkIn">
                                       <?php if ($cke6 != NULL) { ?>
                                          <input type="checkbox" name="check_6" id="CAT_Custom_20043779_157746_0" value="Attractive returns" checked/> Attractive returns
                                       <?php } else { ?>
                                          <input type="checkbox" name="check_6" id="CAT_Custom_20043779_157746_0" value="Attractive returns" /> Attractive returns
                                       <?php } ?>
                                       <?php if ($cke7 != NULL) { ?>
                                          <input type="checkbox" name="check_7" id="CAT_Custom_20043779_157746_1" value="Short tenor" checked/> Short tenor
                                       <?php } else { ?>
                                          <input type="checkbox" name="check_7" id="CAT_Custom_20043779_157746_1" value="Short tenor" /> Short tenor
                                       <?php } ?>
                                       <?php if ($cke8 != NULL) { ?>
                                          <input type="checkbox" name="check_8" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" checked /> Social/ethical reasons
                                       <?php } else { ?>
                                          <input type="checkbox" name="check_8" id="CAT_Custom_20043779_157746_2" value="Social/ethical reasons" /> Social/ethical reasons
                                       <?php } ?>
                                       <?php if ($cke9 != NULL) { ?>
                                          <input type="checkbox" name="check_9" id="CAT_Custom_20043779_157746_3" value="Investment diversification" checked/> Investment diversification
                                       <?php } else { ?>
                                          <input type="checkbox" name="check_9" id="CAT_Custom_20043779_157746_3" value="Investment diversification"/> Investment diversification
                                       <?php } ?>
                                       <?php if ($cke10 != NULL) { ?>
                                          <input type="checkbox" name="check_10" id="CAT_Custom_20043779_157746_4" value="Transparency" checked/> Transparency
                                       <?php } else { ?>
                                          <input type="checkbox" name="check_10" id="CAT_Custom_20043779_157746_4" value="Transparency"/> Transparency
                                       <?php } ?>
                                       <?php if ($cke11 != NULL) { ?>
                                          <input type="checkbox" name="check_11" id="CAT_Custom_20043779_157746_5" value="Others" checked/> Others
                                       <?php } else { ?>
                                          <input type="checkbox" name="check_11" id="CAT_Custom_20043779_157746_5" value="Others" /> Others
                                       <?php } ?>
                                       <br />
                                       <br />
									   <div style="text-align:center">
                                       <?php if ($cke12 != NULL) { ?>
                                          <div class="invest-form-layer" style="margin-bottom: 30px;width: 97.5%;">
                                             <input  type="checkbox" name="check_12" id="CAT_Custom_20043781_157746_0" value="yes" checked />
                                             <label style="margin-left: 10px;max-width: 95%;text-align: center;" class="labelIn3"><?= $mFirstName ?> agree to Kapital Boost’s <font style="color: #609edf;">Terms of Use</font> and <font style="color: #609edf;"> Privacy Policy </font>, and have read understand the <font style="color: #609edf;">Risk Statement</font>.</label>
                                          </div>
                                       <?php } else { ?>
                                          <div class="invest-form-layer" style="margin-bottom: 30px;width: 97.5%;">
                                             <input  type="checkbox" name="check_12" id="CAT_Custom_20043781_157746_0" value="yes" />
                                             <label style="margin-left: 10px;max-width: 95%;text-align: center;" class="labelIn3"><?= $mFirstName ?> agree to Kapital Boost’s <font style="color: #609edf;">Terms of Use</font> and <font style="color: #609edf;"> Privacy Policy </font>, and have read understand the <font style="color: #609edf;">Risk Statement</font>.</label>
                                          </div>
                                       <?php } ?>
									   </div>
                                    </div>

                                 </div>


                                 <input name="button" type="submit" class="btn btn-primary" id="button" value="Update" />


                              </div>

                              </form>







                           </div>
                        </div>
                  </div>
               </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="formBankAccount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document" style="width: 41%;">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Details of Bank Account</h4>
                  </div>
                  <form action="" method="post">
                    <div class="modal-body">
                      <input type="hidden" name="member_id" class="form-control" id="member_id" value="<?= $mId ?>">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label for="nama_lengkap">Account Holder Name *</label>
                            <input type="text" class="form-control" name="account_name" id="account_name" value="<?php echo $data_bank_account->account_name ?>">
                            <span id="error_name"></span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">
                          <div class="form-group">
                            <label for="alamat_email">Bank Name *</label>
                              <select class="form-control" id="bank_code" name="bank_code" value="<?php echo $data_bank_account->bank_code ?>">
                                  <option class="select-type" value="">-- Choose Bank Name --</option>
                                  <?php
                                    for ($i = 0; $i < count($bankCode); $i++) {
                                      echo "<option value='$bankCode[$i]' ".(isset($data_bic) && ($bankCode[$i] == $data_bic->bank_code) ? 'selected' : '').">$bankName[$i]</option>";
                                    } ?>
                              </select>
                            <span id="error_bank"></span>
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-7">
                          <div class="form-group">
                            <label for="no_telepon">Account Number *</label>
                            <input type="text" class="form-control" name="account_number" id="account_number" value="<?php echo $data_bank_account->account_number ?>">
                            <small><b>Notes: Please input numerical digits only</b></small>
                            <span id="error_number"></span>
                          </div>
                        </div>
                      </div>

                      <!-- <div class="row">
                        <div class="col-lg-7">
                          <div class="form-group">
                            <label for="no_telepon">Account Domicile *</label>
                            <input type="text" class="form-control" name="country_of_bank_account" id="country_of_bank_account" value="<?php echo $data_bank_account->country_of_bank_account ?>">
                            <span id="error_domicile"></span>
                          </div>
                        </div>
                      </div> -->

                    </div>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary btn-validasi">Save</button>
                    </div>

                  </form>
                </div>
              </div>
            </div>


            <script src="js/pickadate/picker.js"></script>
            <script src="js/pickadate/picker.date.js"></script>
            <script src="js/pickadate/legacy.js"></script>
            <script>

                $('#dob').pickadate({
                   format: 'yyyy-mm-dd'
                });

                $(document).ready(function() {
                  $('#account_number').bind('keyup paste', function(){
                    this.value = this.value.replace(/[^0-9]/g, '');
                  });
                })

            </script>


            </body>


            </html>
