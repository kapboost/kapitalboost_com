<div class="top-area-wrapper navbar-fixed-top">
       <div class="main-menu navbar navbar-inverse" role="navigation">
              <div class="container-fluid">
                     <div class="navbar-header">
                            <button type="button" class="main-menu navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                   <span class="sr-only">Toggle navigation</span>
                                   <span class="icon-bar"></span>
                                   <span class="icon-bar"></span>
                                   <span class="icon-bar"></span>
                            </button>
                     </div>
                     <div class="navbar-collapse collapse">
                            <ul class="main-menu nav navbar-nav">

                                   <li><a href="<?php echo ADMIN_PATH ?>/index.php"><img src="https://kapitalboost.com/assets/images/logo/kapitalboost-logo.png" width="100px"  style="margin-top:10px;"></a></li>
                                   <li><a href="campaigns.php">Campaigns</a></li>
                                   <li><a href="listupdates.php">Campaigns Update</a></li>

                                   <!-- <li><a href="get-funded.php">Get Funded List</a></li> -->
                                   <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Get Funded<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="get-funded.php">All</a></li>
                                                 <li><a href="get-funded.php?source=/get-funded-new">Get Funded New</a></li>
                                          </ul>
                                   </li>

                                   <!-- <li><a href='index.php?t=&session={$session}&system_id=33&module_id=114&action_id=78&action=LoadGetFundedList'>Add Investment</a></li>-->

                                   <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Investments<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="investments.php?show=all"w>All</a></li>
                                                 <li><a href="investments.php?show=paypal">Paypal</a></li>
                                                 <li><a href="investments.php?show=bank">Bank</a></li>
                                                 <li><a href="investments.php?show=xfers">Xfers</a></li>
                                                 <li><a href="investments.php?show=unpaid">Unpaid</a></li>
                                          </ul>
                                   </li>

                                   <li><a href="withdraw-wallet.php">Withdrawal</a></li>
                                   <li><a href="blogs.php">Blogs</a></li>

                                   <li><a href="enquiries.php">Enquiries</a></li>
                                   <!--<li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blogs<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="blogs.php">Blogs List</a></li>
                                                 <li><a href="blogs-add.php">Blog - Add</a></li>
                                          </ul>
                                   </li>-->

                                   <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">General Pages<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="general-career.php">Career</a></li>
                                                 <li><a href="general-about-us.php">About Us</a></li>
                                                 <li><a href="general-about-us-id.php">About Us ID</a></li>
                                                 <!--<li><a href="general-contact-us.php">Contact Us</a></li>-->
                                                 <li><a href="general-faqs.php">FAQs</a></li>
                                                 <li><a href="general-faqs-id.php">FAQs ID</a></li>
                                                 <!--<li><a href="general-how-it-works.php">How it works</a></li>-->
                                                 <li><a href="general-legal.php">Legal</a></li>
                                                 <li><a href="general-legal-id.php">Legal ID</a></li>
                                                 <!--<li><a href="general-team.php">The Team</a></li>-->
                                          </ul>
                                   </li>

                                   <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Members<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="members.php">All Member</a></li>
                                                 <li><a href="member-landingpage.php">Landing Page</a></li>
                                                 
                                          </ul>
                                   </li>
								   <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">ValuePenguin<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="vpinvestor.php">Investor</a></li>
                                                 <li><a href="vpborrower.php">Borrower</a></li>
                                                 
                                          </ul>
                                   </li>
                                   <li class="dropdown">
                                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Others<b class="caret"></b></a>
                                          <ul class="dropdown-menu">
                                                 <li><a href="event-list.php">Events</a></li>
                                                 <li><a href="messages.php">Contact Us</a></li>
                                                 <li><a href="partners.php">Partners</a></li>
                                                 <!--<li><a href="member-tracking.php">Member Tracking - List</a></li>
                                                 <li><a href="media.php">Media - List</a></li>
                                                 <li><a href="media-add.php">Media - Add</a></li>
                                                 
                                                 <li><a href="testimonials.php">Testimonials - List</a></li>
                                                 <li><a href="testimonials-add.php">Testimonials - Add</a></li>-->
                                          </ul>
                                   </li>

                                   <li>
                                          <a href="logout.php">Logout <i class="fa fa-sign-out" aria-hidden="true" style="color:white;font-size:1em"></i> </a>
                                   </li>
                            </ul>
                     </div>
              </div>
       </div>
       <!-------------------------------------- //nav bar -------------------------------------->
</div>