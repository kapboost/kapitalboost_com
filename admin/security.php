<?php
// define("ADMIN_PATH","/K^B!*$");
define("ADMIN_PATH","/admin");
session_start();

if(!isset($_SESSION["StateAdminId"])){
       header("Location: login.php");
}

if (!isset($_SESSION['CREATED'])) {
    $_SESSION['CREATED'] = time();
} else if (time() - $_SESSION['CREATED'] > 86400) {
    // session started more than 1 day ago
    session_unset();
    session_destroy();
    header('Location: login.php');
} else if (time() - $_SESSION['CREATED'] <= 86400) {
    $_SESSION['CREATED'] = time();
}

?>