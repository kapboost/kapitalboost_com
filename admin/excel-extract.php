<?php

require '../vendor/autoload.php';
include_once 'mysql.php';
$mysql = new mysql();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

$action = $_POST["action"];

if ($action=="member"){
	$status = array('0' => 'New', '1' => 'Waiting to be Reviewed', '2' => 'Approved', '3' => 'Blacklisted', '4' => 'Rejected');

	$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	$sheet->setCellValueByColumnAndRow(1, 1, 'Full Name');
	$sheet->setCellValueByColumnAndRow(2, 1, 'Email Address');
	$sheet->setCellValueByColumnAndRow(3, 1, 'Handphone Number');
	$sheet->setCellValueByColumnAndRow(4, 1, 'Country');
	$sheet->setCellValueByColumnAndRow(5, 1, 'Created At');
	$sheet->setCellValueByColumnAndRow(6, 1, 'How You Know KB');
	$sheet->setCellValueByColumnAndRow(7, 1, 'Cek1');
	$sheet->setCellValueByColumnAndRow(8, 1, 'Cek2');
	$sheet->setCellValueByColumnAndRow(9, 1, 'Cek3');
	$sheet->setCellValueByColumnAndRow(10, 1, 'Cek4');
	$sheet->setCellValueByColumnAndRow(11, 1, 'Cek5');
	$sheet->setCellValueByColumnAndRow(12, 1, 'Cek6');
	$sheet->setCellValueByColumnAndRow(13, 1, 'Cek7');
	$sheet->setCellValueByColumnAndRow(14, 1, 'Cek8');
	$sheet->setCellValueByColumnAndRow(15, 1, 'Cek9');
	$sheet->setCellValueByColumnAndRow(16, 1, 'Cek10');
	$sheet->setCellValueByColumnAndRow(17, 1, 'Cek11');
	$sheet->setCellValueByColumnAndRow(18, 1, 'Cek12');
	$sheet->setCellValueByColumnAndRow(19, 1, 'Cek13');
	$sheet->setCellValueByColumnAndRow(20, 1, 'Username');
	$sheet->setCellValueByColumnAndRow(21, 1, 'Gender');
	$sheet->setCellValueByColumnAndRow(22, 1, 'DOB');
	$sheet->setCellValueByColumnAndRow(23, 1, 'Status');
	
	if ($mysql->Connection()) {

       list($mId, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mHowYouKnow, $cek1, $cek2, $cek3, $cek4, $cek5, $cek6, $cek7, $cek8, $cek9, $cek10, $cek11, $cek12, $cek13, $mUsername, $gender, $mobile, $dob, $member_status) = $mysql->ExtractAllMembers();
	}

	for ($i = 2; $i < count($mId)+2; $i++) {
		$v = $i-2;
		$howYouKnowUs = "";
		switch ($mHowYouKnow[$v]) {
		case "1":
			$howYouKnowUs = "Search Engine";
			break;
		case "2":
			$howYouKnowUs = "Ads";
			break;
		case "3":
			$howYouKnowUs = "Social Media";
			break;
		case "4":
			$howYouKnowUs = "Friend/Family";
			break;
		case "5":
			$howYouKnowUs = "Community Event";
			break;
		case "6":
			$howYouKnowUs = "News/Blog/Magazine";
			break;
		case "7":
			$howYouKnowUs = "Other";
			break;
		default:
			$howYouKnowUs = "";
	}

		$sheet->setCellValueByColumnAndRow(1, $i, $mFullname[$v]);
		$sheet->setCellValueByColumnAndRow(2, $i, $mEmail[$v]);
		$sheet->setCellValueByColumnAndRow(3, $i, $mobile[$v]);
		$sheet->setCellValueByColumnAndRow(4, $i, $mCountry[$v]);
		$sheet->setCellValueByColumnAndRow(5, $i, $mCreatedDate[$v]);
		$sheet->setCellValueByColumnAndRow(6, $i, $howYouKnowUs);
		$sheet->setCellValueByColumnAndRow(7, $i, $cek1[$v]);
		$sheet->setCellValueByColumnAndRow(8, $i, $cek2[$v]);
		$sheet->setCellValueByColumnAndRow(9, $i, $cek3[$v]);
		$sheet->setCellValueByColumnAndRow(10, $i, $cek4[$v]);
		$sheet->setCellValueByColumnAndRow(11, $i, $cek5[$v]);
		$sheet->setCellValueByColumnAndRow(12, $i, $cek6[$v]);
		$sheet->setCellValueByColumnAndRow(13, $i, $cek7[$v]);
		$sheet->setCellValueByColumnAndRow(14, $i, $cek8[$v]);
		$sheet->setCellValueByColumnAndRow(15, $i, $cek9[$v]);
		$sheet->setCellValueByColumnAndRow(16, $i, $cek10[$v]);
		$sheet->setCellValueByColumnAndRow(17, $i, $cek11[$v]);
		$sheet->setCellValueByColumnAndRow(18, $i, $cek12[$v]);
		$sheet->setCellValueByColumnAndRow(19, $i, $cek13[$v]);
		$sheet->setCellValueByColumnAndRow(20, $i, $mUsername[$v]);
		$sheet->setCellValueByColumnAndRow(21, $i, $gender[$v]);
		$sheet->setCellValueByColumnAndRow(22, $i, $dob[$v]); 
		$sheet->setCellValueByColumnAndRow(23, $i, $status[$member_status[$v]]); 
	}

	$spreadsheet->setActiveSheetIndex(0);

	$filename = 'Member-report';
	
} else if ($action=="investments"){
	
	$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	
	$sheet->setCellValueByColumnAndRow(1, 1, 'Investment ID');
	$sheet->setCellValueByColumnAndRow(2, 1, 'Campaign Name');
	$sheet->setCellValueByColumnAndRow(3, 1, 'Campaign Type');
	$sheet->setCellValueByColumnAndRow(4, 1, 'Campaign Total Funding Amount');
	$sheet->setCellValueByColumnAndRow(5, 1, 'Campaign Return');
	$sheet->setCellValueByColumnAndRow(6, 1, 'Campaign Risk');
	$sheet->setCellValueByColumnAndRow(7, 1, 'Investor Name');
	$sheet->setCellValueByColumnAndRow(8, 1, 'Investor Email');
	$sheet->setCellValueByColumnAndRow(9, 1, 'Payment Type');
	$sheet->setCellValueByColumnAndRow(10, 1, 'Investment Amount');
	$sheet->setCellValueByColumnAndRow(11, 1, 'Date of Payment');
	$sheet->setCellValueByColumnAndRow(12, 1, 'Payment Status');
	$sheet->setCellValueByColumnAndRow(13, 1, 'Expected Payout');
	$sheet->setCellValueByColumnAndRow(14, 1, 'Payout Bulan 1');
	$sheet->setCellValueByColumnAndRow(15, 1, 'Tanggal Bulan 1');
	$sheet->setCellValueByColumnAndRow(16, 1, 'Status Bulan 1');
	$sheet->setCellValueByColumnAndRow(17, 1, 'Payout Bulan 2');
	$sheet->setCellValueByColumnAndRow(18, 1, 'Tanggal Bulan 2');
	$sheet->setCellValueByColumnAndRow(19, 1, 'Status Bulan 2');
	$sheet->setCellValueByColumnAndRow(20, 1, 'Payout Bulan 3');
	$sheet->setCellValueByColumnAndRow(21, 1, 'Tanggal Bulan 3');
	$sheet->setCellValueByColumnAndRow(22, 1, 'Status Bulan 3');
	$sheet->setCellValueByColumnAndRow(23, 1, 'Payout Bulan 4');
	$sheet->setCellValueByColumnAndRow(24, 1, 'Tanggal Bulan 4');
	$sheet->setCellValueByColumnAndRow(25, 1, 'Status Bulan 4');
	$sheet->setCellValueByColumnAndRow(26, 1, 'Payout Bulan 5');
	$sheet->setCellValueByColumnAndRow(27, 1, 'Tanggal Bulan 5');
	$sheet->setCellValueByColumnAndRow(28, 1, 'Status Bulan 5');
	$sheet->setCellValueByColumnAndRow(29, 1, 'Payout Bulan 6');
	$sheet->setCellValueByColumnAndRow(30, 1, 'Tanggal Bulan 6');
	$sheet->setCellValueByColumnAndRow(31, 1, 'Status Bulan 6');
	
	if ($mysql->Connection()) {

       list($invId, $campaignName, $campaignType, $campaignTotalFunding, $campaignReturn, $campaignRisk, $investorName, $investorEmail, $paymentType, $invAmount, $dateOfPayment, $paymentStatus, $expectedPayout, $payoutB1, $tglB1, $statB1, $payoutB2, $tglB2, $statB2, $payoutB3, $tglB3, $statB3, $payoutB4, $tglB4, $statB4, $payoutB5, $tglB5, $statB5, $payoutB6, $tglB6, $statB6) = $mysql->ExtractAllInvestments();
	
	}
	
	for ($i = 2; $i < count($invId)+2; $i++) {
		$v = $i-2;
		
		$sheet->setCellValueByColumnAndRow(1, $i, $invId[$v]);
		$sheet->setCellValueByColumnAndRow(2, $i, $campaignName[$v]);
		$sheet->setCellValueByColumnAndRow(3, $i, $campaignType[$v]);
		$sheet->setCellValueByColumnAndRow(4, $i, $campaignTotalFunding[$v]);
		$sheet->setCellValueByColumnAndRow(5, $i, $campaignReturn[$v]);
		$sheet->setCellValueByColumnAndRow(6, $i, $campaignRisk[$v]);
		$sheet->setCellValueByColumnAndRow(7, $i, $investorName[$v]);
		$sheet->setCellValueByColumnAndRow(8, $i, $investorEmail[$v]);
		$sheet->setCellValueByColumnAndRow(9, $i, $paymentType[$v]);
		$sheet->setCellValueByColumnAndRow(10, $i, $invAmount[$v]);
		$sheet->setCellValueByColumnAndRow(11, $i, $dateOfPayment[$v]);
		$sheet->setCellValueByColumnAndRow(12, $i, $paymentStatus[$v]);
		$sheet->setCellValueByColumnAndRow(13, $i, $expectedPayout[$v]);
		$sheet->setCellValueByColumnAndRow(14, $i, $payoutB1[$v]);
		$sheet->setCellValueByColumnAndRow(15, $i, $tglB1[$v]);
		$sheet->setCellValueByColumnAndRow(16, $i, $statB1[$v]);
		$sheet->setCellValueByColumnAndRow(17, $i, $payoutB2[$v]);
		$sheet->setCellValueByColumnAndRow(18, $i, $tglB2[$v]);
		$sheet->setCellValueByColumnAndRow(19, $i, $statB2[$v]);
		$sheet->setCellValueByColumnAndRow(20, $i, $payoutB3[$v]);
		$sheet->setCellValueByColumnAndRow(21, $i, $tglB3[$v]);
		$sheet->setCellValueByColumnAndRow(22, $i, $statB3[$v]);
		$sheet->setCellValueByColumnAndRow(23, $i, $payoutB4[$v]);
		$sheet->setCellValueByColumnAndRow(24, $i, $tglB4[$v]);
		$sheet->setCellValueByColumnAndRow(25, $i, $statB4[$v]);
		$sheet->setCellValueByColumnAndRow(26, $i, $payoutB5[$v]);
		$sheet->setCellValueByColumnAndRow(27, $i, $tglB5[$v]);
		$sheet->setCellValueByColumnAndRow(28, $i, $statB5[$v]);
		$sheet->setCellValueByColumnAndRow(29, $i, $payoutB6[$v]);
		$sheet->setCellValueByColumnAndRow(30, $i, $tglB6[$v]);
		$sheet->setCellValueByColumnAndRow(31, $i, $statB6[$v]);
	}

	$spreadsheet->setActiveSheetIndex(0);

	$filename = 'Investments-report';
	
} else if ($action=="payout"){
	
	$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	
	$sheet->setCellValueByColumnAndRow(1,1,'No.');
	$sheet->setCellValueByColumnAndRow(2,1,'Nama Campaign');
	$sheet->setCellValueByColumnAndRow(3,1,'Nama PIC Pinjaman');
	$sheet->setCellValueByColumnAndRow(4,1,'No Telp PIC Pinjaman');
	$sheet->setCellValueByColumnAndRow(5,1,'Jenis Pinjaman');
	$sheet->setCellValueByColumnAndRow(6,1,'Tipe Campaign');
	$sheet->setCellValueByColumnAndRow(7,1,'Limit (IDR)');
	$sheet->setCellValueByColumnAndRow(8,1,'Banyaknya angsuran');
	$sheet->setCellValueByColumnAndRow(9,1,'Jangka Waktu (Bulan)');
	$sheet->setCellValueByColumnAndRow(10,1,'Return');
	$sheet->setCellValueByColumnAndRow(11,1,'Return Amount');
	$sheet->setCellValueByColumnAndRow(12,1,'Total pengembalian');
	$sheet->setCellValueByColumnAndRow(13,1,'Agunan (Cek / BG)');
	$sheet->setCellValueByColumnAndRow(14,1,'Nominal Cek / BG');
	$sheet->setCellValueByColumnAndRow(15,1,'Tanggal Expired Cek / BG');
	$sheet->setCellValueByColumnAndRow(16,1,'Tanggal Pencairan');
	$sheet->setCellValueByColumnAndRow(17,1,'Bulan Pencairan');
	$sheet->setCellValueByColumnAndRow(18,1,'Tahun Pencairan');
	$sheet->setCellValueByColumnAndRow(19,1,'Tanggal Mulai Angsuran');
	$sheet->setCellValueByColumnAndRow(20,1,'Angsuran Pokok');
	$sheet->setCellValueByColumnAndRow(21,1,'Platform Fee');
	$sheet->setCellValueByColumnAndRow(22,1,'Amount');
	$sheet->setCellValueByColumnAndRow(23,1,'Ujrah Wakalah');
	$sheet->setCellValueByColumnAndRow(24,1,'Amount (Ditambahkan ke kewajiban terakhir)');
	$sheet->setCellValueByColumnAndRow(25,1,'Sisa Outstanding');
	$sheet->setCellValueByColumnAndRow(26,1,'Staff Survey');
	$sheet->setCellValueByColumnAndRow(27,1,'Staff Analisa');
	$sheet->setCellValueByColumnAndRow(28,1,'Berapa kali mengangsur');
	$sheet->setCellValueByColumnAndRow(29,1,'Sisa bulan angsuran');
	$sheet->setCellValueByColumnAndRow(30,1,'Lama Hari Tunggakan (Isi manual terhitung jatuh tempo)');
	$sheet->setCellValueByColumnAndRow(31,1,'Status Pinjaman');
	$dup = 0;
	for ($i=1;$i <= 12; $i++){
		$p1 = 31+$i+$dup; $p2 = 32+$i+$dup; $p3 = 33+$i+$dup; $p4 = 34+$i+$dup; $p5 = 35+$i+$dup; $p6 = 36+$i+$dup;
		$sheet->setCellValueByColumnAndRow($p1,1,'Persentase besar angsuran '.$i);
		$sheet->setCellValueByColumnAndRow($p2,1,'Besar angsuran '.$i);
		$sheet->setCellValueByColumnAndRow($p3,1,'Jatuh tempo angsuran '.$i);
		$sheet->setCellValueByColumnAndRow($p4,1,'Tanggal bayar angsuran '.$i);
		$sheet->setCellValueByColumnAndRow($p5,1,'Pembayaran angsuran '.$i);
		$sheet->setCellValueByColumnAndRow($p6,1,'Kekurangan pembayaran angsuran '.$i);
		$dup=$dup+5;
	}
	
	
	if ($mysql->Connection()) {

       list($campaignName, $smeSubtype, $projectType, $totalFundingAmt, $fundingSummary, $projectReturn, $expiryDate, $mPayout) = $mysql->ExtractAllMonitoringPayout();
	
	}
	
	for ($i = 2; $i < count($campaignName)+2; $i++) {
		$v = $i-2;
		
		$no = $i-1;
		$returnAmount = number_format($projectReturn[$v]) * number_format($totalFundingAmt[$v]);
		$totalPengembalian = (100+number_format($projectReturn[$v])) * number_format($totalFundingAmt[$v]);
		$d = date_parse_from_format("Y-m-d", $expiryDate[$v]);
		$bulanPencairan = $d["month"];
		$tahunPencairan = $d["year"];
		$payoutA = explode("==", $mPayout[$v]);
		$amountA = $dateA = $statusA = array();
		
		$sheet->setCellValueByColumnAndRow(1,$i, $no);
		$sheet->setCellValueByColumnAndRow(2,$i, $campaignName[$v]);
		$sheet->setCellValueByColumnAndRow(3,$i, '-');
		$sheet->setCellValueByColumnAndRow(4,$i, '-');
		$sheet->setCellValueByColumnAndRow(5,$i, $smeSubtype[$v]);
		$sheet->setCellValueByColumnAndRow(6,$i, $projectType[$v]);
		$sheet->setCellValueByColumnAndRow(7,$i, $totalFundingAmt[$v]);
		$sheet->setCellValueByColumnAndRow(8,$i, '-');
		$sheet->setCellValueByColumnAndRow(9,$i, $fundingSummary[$v]);
		$sheet->setCellValueByColumnAndRow(10,$i, $projectReturn[$v]);
		$sheet->setCellValueByColumnAndRow(11,$i, $returnAmount);
		$sheet->setCellValueByColumnAndRow(12,$i, $totalPengembalian);
		$sheet->setCellValueByColumnAndRow(13,$i, '-');
		$sheet->setCellValueByColumnAndRow(14,$i, '-');
		$sheet->setCellValueByColumnAndRow(15,$i, '-');
		$sheet->setCellValueByColumnAndRow(16,$i, $expiryDate[$v]);
		$sheet->setCellValueByColumnAndRow(17,$i, $bulanPencairan);
		$sheet->setCellValueByColumnAndRow(18,$i, $tahunPencairan);
		$sheet->setCellValueByColumnAndRow(19,$i, '-');
		$sheet->setCellValueByColumnAndRow(20,$i, $totalFundingAmt[$v]);
		$sheet->setCellValueByColumnAndRow(21,$i, '-');
		$sheet->setCellValueByColumnAndRow(22,$i, '-');
		$sheet->setCellValueByColumnAndRow(23,$i, '-');
		$sheet->setCellValueByColumnAndRow(24,$i, '-');
		$sheet->setCellValueByColumnAndRow(25,$i, '-');
		$sheet->setCellValueByColumnAndRow(26,$i, '-');
		$sheet->setCellValueByColumnAndRow(27,$i, '-');
		$sheet->setCellValueByColumnAndRow(28,$i, '-');
		$sheet->setCellValueByColumnAndRow(29,$i, '-');
		$sheet->setCellValueByColumnAndRow(30,$i, '-');
		$sheet->setCellValueByColumnAndRow(31,$i, '-');
		$dup = 0;
		for ($j=0;$j < 12; $j++){
			$p1 = 32+$j+$dup; $p2 = 33+$j+$dup; $p3 = 34+$j+$dup; $p4 = 35+$j+$dup; $p5 = 36+$j+$dup; $p6 = 37+$j+$dup;
			
			$payouts = explode("~", $payoutA[$j]);
			$amountA = $payouts[0];
			$dateA = $payouts[1];
			$statusA = $payouts[2];
			$besarAngsuran = number_format($amountA ) * $totalPengembalian;
			
			$sheet->setCellValueByColumnAndRow($p1,$i, $amountA);
			$sheet->setCellValueByColumnAndRow($p2,$i, $besarAngsuran);
			$sheet->setCellValueByColumnAndRow($p3,$i, $dateA);
			$sheet->setCellValueByColumnAndRow($p4,$i, '-');
			$sheet->setCellValueByColumnAndRow($p5,$i, '-');
			$sheet->setCellValueByColumnAndRow($p6,$i, '-');
			$dup=$dup+5;
		}
	}

	$spreadsheet->setActiveSheetIndex(0);

	$filename = 'Payouts-report';
	
} else if ($action == "report-payout") {
	$spreadsheet = new Spreadsheet();
	$sheet = $spreadsheet->getActiveSheet();
	
	$sheet->setCellValueByColumnAndRow(1, 1, 'A/C Number');
	$sheet->setCellValueByColumnAndRow(2, 1, 'Investor Name');
	$sheet->setCellValueByColumnAndRow(3, 1, 'Email');
	$sheet->setCellValueByColumnAndRow(4, 1, 'BIC Code');
	for ($i=1; $i <= 4; $i++) {
		$sheet->setCellValueByColumnAndRow(4+$i, 1, "Payout $i (SGD)");
	}

	if ($mysql->Connection()) {
		$report_payouts = $mysql->reportPayouts($_POST['id']);
		$campaign = $mysql->getDatas("tcampaign", "campaign_id", $_POST['id'], false);
	}

	$row = 2;
	foreach ($report_payouts as $report) {
		$sheet->setCellValueByColumnAndRow(1, $row,"'".$report['account_number']);
		$sheet->setCellValueByColumnAndRow(2, $row, $report['nama']);
		$sheet->setCellValueByColumnAndRow(3, $row, $report['email']);
		$sheet->setCellValueByColumnAndRow(4, $row, $report['bic']);
		for ($i=1; $i <= 4; $i++) {
			if (isset($report["bulan_$i"])) {
				$sheet->setCellValueByColumnAndRow(4+$i, $row, " ".number_format($report["bulan_$i"], 2, '.', ','));
			}else{
				$sheet->setCellValueByColumnAndRow(4+$i, $row, "-");
			}
		}

		$row++;
	}

	$spreadsheet->setActiveSheetIndex(0);

	$filename = "Report-Payout  ({$campaign->campaign_name})";
}



header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$filename.'.xlsx"');
header('Cache-Control: max-age=0');

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');

$writer->save('php://output');

?>