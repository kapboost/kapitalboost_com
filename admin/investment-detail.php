<?php
include_once 'security.php';

$n = test_input($_GET["n"]);


include_once 'mysql.php';
$mysql = new mysql();


if (isset($_POST["fullname"])) {
   $namef = test_input($_POST["fullname"]);
   $emailf = test_input($_POST["email"]);
   $countryf = test_input($_POST["country"]);
   $campaignNamef = test_input($_POST["campaign_name"]);
   $campaignIdf = test_input($_POST["campaign_id"]);
   $pTypef = test_input($_POST["ptype"]);
   $totalAmountf = test_input($_POST["total_funding"]);
   $bankNamef = test_input($_POST["bank_name"]);
   $cTypef = test_input($_POST["ctype"]);
   $datef = test_input($_POST["date"]);
   $bankAccNamef = test_input($_POST["bankaccname"]);
   $bankAccNumf = test_input($_POST["bankaccnum"]);
   $statusf = test_input($_POST["status"]);

   if (isset($_FILES["fileToUpload"]) && $_FILES["fileToUpload"]["name"] != "") {
      $file = $_FILES["fileToUpload"]["tmp_name"];
      $ext = strtolower($_FILES["fileToUpload"]['extension']);
      $uploaddir .= "../assets/images/bukti/";
      $image_name = rand(1, 10) . time() . $ext;
      move_uploaded_file($file, $uploaddir . $image_name);
      if ($mysql->Connection()) {
         $mysql->UpdateUploadedImgInvestment($n, $image_name);
      }
   }

   if ($statusf=="Wallet"){
	   //ambil total inv
	   $amount = $totalAmountf;
	   if ($mysql->Connection()) {
			$member_ids = $mysql->GetMemberIdbyEmail($emailf);
			//ambil jumlah wallet sekarang
			$member_balance = $mysql->GetMmeberBalance($member_ids);
		}

	   //cek apakah isi wallet lebih besar dari total inv
	   if ($member_balance >= $amount) {
			//kurangi wallet dengan total inv
			$wallet = $member_balance - $amount;
			//tambah di history wallet
			if ($mysql->Connection()) {
				$mysql->InsertSingleTransaction($member_ids, "$campaignNamef - Investment", "Investment", $amount);
				//update wallet
				$mysql->UpdateMemberWalletBalance($member_ids, $wallet);
			}

			if ($mysql->Connection()) {
			  list($pReturn, $mPayout) = $mysql->GetCampaignReturnPayout($campaignIdf);
			  $expectedPayoutf = $totalAmountf + ($totalAmountf * $pReturn / 100);
			  if ($mPayout != "") {
				 $payoutA = explode("==", $mPayout);
				 $amountA = $dateA = $statusA = array();
				 for ($i = 0; $i < count($payoutA); $i++) {
					$payout = explode("~", $payoutA[$i]);
					$amountA[$i] = $payout[0];
					$dateA[$i] = $payout[1];
					$statusA[$i] = $payout[2];
					if ($amountA[$i] != "") {
					   $singleAmount = $expectedPayoutf * ($amountA[$i] / 100);
					   $mysql->UpdatePayout($n, $i + 1, $singleAmount, $dateA[$i], $statusA[$i]);
					}
				 }
			  }
			  if ($cTypef == "donation") {
				 $expectedPayoutf = "0";
			  }

			  $mysql->EditInvestment($n, $namef, $emailf, $countryf, $campaignNamef, $totalAmountf, $expectedPayoutf, $bankNamef, $cTypef, $pTypef, $datef, $bankAccNamef, $bankAccNumf, $statusf);
		   }
	   } else {
		   //munculkan notifikasi
		   echo "<script>
			alert('Insufficient Wallet Balance');
			window.history.back(5000);
			</script>";
		   //kembali ke halaman sebelumnya
	   }

	   //
   } else {

	   if ($mysql->Connection()) {
      list($pReturn, $mPayout) = $mysql->GetCampaignReturnPayout($campaignIdf);
      $expectedPayoutf = $totalAmountf + ($totalAmountf * $pReturn / 100);
      if ($mPayout != "") {
         $payoutA = explode("==", $mPayout);
         $amountA = $dateA = $statusA = array();
         for ($i = 0; $i < count($payoutA); $i++) {
            $payout = explode("~", $payoutA[$i]);
            $amountA[$i] = $payout[0];
            $dateA[$i] = $payout[1];
            $statusA[$i] = $payout[2];
            if ($amountA[$i] != "") {
               $singleAmount = $expectedPayoutf * ($amountA[$i] / 100);
               $mysql->UpdatePayout($n, $i + 1, $singleAmount, $dateA[$i], $statusA[$i]);
            }
         }
      }
      if ($cTypef == "donation") {
         $expectedPayoutf = "0";
      }

      $mysql->EditInvestment($n, $namef, $emailf, $countryf, $campaignNamef, $totalAmountf, $expectedPayoutf, $bankNamef, $cTypef, $pTypef, $datef, $bankAccNamef, $bankAccNumf, $statusf);
   }

   }



}


if ($mysql->Connection()) {
   list($name, $mId, $email, $country, $campaignName, $campaignId, $totalFunding, $expectedPayout, $noMasterPayout, $bankName, $cType, $pType, $date, $images, $bankAccName, $bankAccNum, $status, $attachment) = $mysql->GetSingleInvestment($n);
   list($pAmount, $pMonth, $pStatus) = $mysql->GetSinglePayout($n);
   $firstname = $mysql->GetFirstNameFromMId($mId);

   if ($pAmount[1] == "" || is_null($pAmount[1])) {
      list($pReturn, $mPayout) = $mysql->GetCampaignReturnPayout($campaignId);
      $expectedPayoutf = $totalFunding + ($totalFunding * $pReturn / 100);
      if ($mPayout != "") {
         $payoutA = explode("==", $mPayout);
         $amountA = $dateA = $statusA = array();
         for ($i = 0; $i < count($payoutA); $i++) {
            $payout = explode("~", $payoutA[$i]);
            $amountA[$i] = $payout[0];
            $dateA[$i] = $payout[1];
            $statusA[$i] = $payout[2];
            if ($amountA[$i] != "") {
               $singleAmount = $expectedPayoutf * ($amountA[$i] / 100);
               $mysql->UpdatePayout($n, $i + 1, $singleAmount, $dateA[$i], $statusA[$i]);
            }
         }
      }

      list($pAmount, $pMonth, $pStatus) = $mysql->GetSinglePayout($n);

   }

   $imagesA = explode("~", $images);
   
   $bank_account = $mysql->getDatas("bank_account", "member_id", $mId, false);
}

function test_input($data) {
   $data = trim($data);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <?php include_once 'initialize.php'; ?>

      <title>KB Admin Investment Detail of <?= $name ?></title>

      <?php include_once 'include.php'; ?>


   </head>
   <body>
      <?php include_once 'header.php'; ?>
      <?php include_once 'popup.php'; ?>

      <!-- Modal -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Member Bank Account</h4>
            </div>
            <div class="modal-body">
               <table class="table table-bordered">
                  <thead>
                     <tr>
                        <th>No</th>
                        <th>Bank Name</th>
                        <th>Account Holder Name</th>
                        <th>Account Number</th>
                        <th>Country of Bank Account</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php if (!is_null($bank_account)): ?>
                        <tr>
                           <td>1</td>
                           <td><?php echo $bank_account->bank_name ?></td>
                           <td><?php echo $bank_account->account_name ?></td>
                           <td><?php echo $bank_account->account_number ?></td>
                           <td><?php echo $bank_account->country_of_bank_account ?></td>
                        </tr>
                     <?php else: ?>
                        <tr>
                           <td colspan="5" align="center">bank account not found</td>
                        </tr>
                     <?php endif ?>
                  </tbody>
               </table>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>

      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Investment Detail of <?= $name ?></span>
                     <div class="clearfix"></div>
                     <div class="pull-right" style="margin-top: 5px;">
                        <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">Bank Account</button>
                     </div>
                  </div>

                  <div class="panel-body">




                     <link rel="stylesheet" href="https://kapitalboost.com/assets/css/kp-default.css" />
                     <link rel="stylesheet" href="https://kapitalboost.com/assets/css/input.css" />

                     <form action="investment-detail.php?n=<?= $n ?>" method="post"  enctype="multipart/form-data">
                        <legend> Account & Campaign Detail </legend>
                        <div class="col-xs-12 col-md-6">

                           <div class="form-group ">
                              <label>Name : </label>
                              <input onclick="sensative()" type="text" name="fullname" value="<?= $name ?>" class="form-control" readonly=""/>
                           </div>

                           <div class="form-group ">
                              <label>Email Address : </label>
                              <input onclick="sensative()" type="text" name="email" value="<?= $email ?>" class="form-control"  readonly=""/>
                           </div>

                           <div class="form-group ">
                              <label>Country : </label>
                              <input onclick="sensative()" type="text" name="country" value="<?= $country ?>" class="form-control" readonly="" />
                           </div>

                           <div class="form-group">
                              <label>Campaign Name : </label>
                              <input onclick="sensative()" type="text" name="campaign_name" value="<?= $campaignName ?>" class="form-control"  readonly=""/>
                              <input type="hidden" name="campaign_id" value="<?= $campaignId ?>" />
                           </div>
                           <div class="form-group">
                              <label>Project Type : </label>
                              <input onclick="sensative()" type="text" name="ctype" value="<?= $cType ?>" class="form-control"  readonly=""/>
                           </div>



                           <div class="form-group">
                              <label>Total Funding : </label>
                              <input onclick="sensative()" type="text" name="total_funding" value="<?= $totalFunding ?>" class="form-control"  readonly=""/>
                           </div>

                           <div class="form-group">
                              <label>Expected Payout Amount</label>
                              <input onclick="sensative()" type="text" name="expected_payout" value="<?= $expectedPayout ?>" class="form-control" readonly=""/>
                           </div>


                        </div>
                        <div class="col-xs-12 col-md-6">
                           <div class="form-group">
                              <label>Date </label>
                              <input onclick="sensative()" type="text" name="date" value="<?= $date ?>" class="form-control" readonly=""/>
                           </div>
                           <div class="form-group">
                              <label>Payment Type : </label>
                              <input onclick="sensative()" type="text" name="ptype" value="<?= $pType ?>" class="form-control"  readonly=""/>
                           </div>
                           <div class="form-group">
                              <label>Bank Name : </label>
                              <input onclick="sensative()" type="text" name="bank_name" value="<?= $bankName ?>" class="form-control"  readonly=""/>
                           </div>


                           <div class="form-group">
                              <label>Account name</label>
                              <input onclick="sensative()" type="text" name="bankaccname" value="<?= $bankAccName ?>" class="form-control"  readonly=""/>
                           </div>
                           <div class="form-group">
                              <label>Account number</label>
                              <input onclick="sensative()" type="text" name="bankaccnum" value="<?= $bankAccNum ?>" class="form-control"  readonly=""/>
                           </div>

                              <div class="form-group">
                                 <label>Update Payment Status  : </label>
                                 <select id="status" name="status" >
                                    <option> Select Status </option>
                                    <option value="Paid" style="" <?php if ($status == "Paid") {echo "selected";}?>>Paid </option>
                                    <option value="Unpaid" <?php if ($status == "Unpaid") {echo "selected";}?>>Unpaid</option>
                                     <option value="Wallet" <?php if ($status == "Wallet") {echo "selected";}?>>KB Wallet</option>
                                 </select>
                              </div>
                           <?php if ($pType == "Bank Transfer" && $attachment != "") { ?>

                              <div class="form-group">
                                 <label>Attachment : </label>
                                 <a href="/assets/images/bukti/<?= $attachment ?>" target="_blank">Download</a>
                              </div>

                           <?php } if ($pType == "Bank Transfer" && $attachment == "") { ?>
                              <div class="form-group">
                                 <label>Upload Receipt</label>
                                 <input  type="file"  name="fileToUpload" id="fileToUpload">
                              </div>
                           <?php } ?>

                           <input type="submit" class="btn btn-primary" id="button2" value="Update" style="margin-left:15px;margin-top:10px;text-align: center"/>

                        </div>
                        <div class="clearfix"></div>
                        <br><br>
                     </form>

                     <script>

                         function sensative() {
                            var person = prompt("Please enter Master Code");
                            $.post("delete.php", {sensative: "", MC: person}, function (reply) {
                               reply = JSON.parse(reply);
                               $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                  if (reply === "Granted") {
                                     $(".form-control").prop('readonly', false);
                                     $(".form-control").prop('onclick', null).off('click');
                                  }
                               });
                               $("#popupText").html(reply);
                            });
                         }

                         $(document).ready(function () {

                            $('#row_dim').hide();
                            $('#type').change(function () {
                               $('#row_dim').hide();
                               if (this.options[this.selectedIndex].value == 'month') {
                                  $('#row_dim').show();
                               }
                            });

                         });

                     </script>


                     <style>
                        input[type="text"]{
                           background: #eee;
                           padding: 0 15px;
                           width: 90%;
                           border-radius:5px;
                        }
                        .deleteLink{
                           cursor: pointer;
                        }
                        .item{
                           display:inline-block;width:42%;margin:10px 5px;
                        }
                        .items{
                           padding:0px 20px;margin-top:20px;
                        }
                        .subitems{
                           background-color:#eee;padding:15px 10px;border-radius: 10px;
                        }
                        .itemtext{
                           height:45px;margin:auto !important;background-color:white !important;
                        }

                     </style>
                     <legend>Payout Schedule </legend>

                     <select id="followMaster">
                        <?php if ($noMasterPayout == 0) { ?>
                           <option value="0" selected="">Follow Master Payout</option>
                           <option value="1">Do Not Follow Master Payout</option>
                        <?php } else { ?>
                           <option value="0">Follow Master Payout</option>
                           <option value="1" selected="">Do Not Follow Master Payout</option>
                        <?php } ?>


                     </select>

                     <?php
                     for ($i = 1; $i <= 12; $i++) {
                        if ($pMonth[$i] != "") {
                           $pMonth[$i] = date("d-m-Y", strtotime($pMonth[$i]));
                        }
                        ?>
                        <div class="col-xs-12 col-md-6 items">
                           <div class="subitems">

                              <label>Payout <?= $i ?> : </label>
                              <div class="">
                                 <div class="item">
                                    <div class="montFormWrap">
                                       <input type="text" id="pAmount<?= $i ?>" value="<?= $pAmount[$i] ?>" data-value="<?= $pAmount[$i] ?>" data-no="<?= $i ?>" name="Month_1" class="itemtext" placeholder="Amount">

                                    </div>
                                 </div>
                                 <div class="item">
                                    <input type="text"  name="release_date" id="pDate<?= $i ?>" placeholder="date" value="<?= $pMonth[$i] ?>" class="itemtext datepickers">

                                 </div>

                                    <div class="item">
                                       <select name="status" id="pStatus<?= $i ?>">
                                          <option value=""  <?php if ($pStatus[$i] == "") {echo "selected";}?>> ------- </option>
                                          <option value="1" <?php if ($pStatus[$i] == "1") {echo "selected";}?>>On Going</option>
										   <option value="2" <?php if ($pStatus[$i] == "2") {echo "selected";}?>>Paid </option>
                                          <option value="3" <?php if ($pStatus[$i] == "3") {echo "selected";}?>>Paid to KB E-Wallet</option>
                                       </select>
                                    </div>

                                 <div class="item">
                                    <input id="<?= $i ?>" name="button" type="submit"  class="btn btn-primary pUpdates" value="Update" style="margin-left:5px;margin-top:6px;"/>
                                 </div>

                                 <input class="files" id="<?= $i ?>"  type="file" name="file<?= $i ?>" style="display:inline-block"/>

                                 <?php if ($imagesA[$i - 1]) { ?>
                                    <div class="imageLink" style="display:inline-block"><a target="_blank" href="../assets/images/payouts/<?= $imagesA[$i - 1] ?>">Proof of Payout</a></div>
                                    <div class="deleteLink" id="<?= $i ?>" style="display: inline-block;color:red;margin-left:20px">Delete</div>
                                 <?php } else { ?>
                                    <div class="imageLink" style="display:inline-block"></div>
                                    <div class="deleteLink" id="<?= $i ?>" style="display: none;color:red;margin-left:20px">Delete</div>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                     <?php } ?>


                  </div>


               </div>
            </div>
         </div>
      </div>
   </div>
   <script src="js/pickadate/picker.js"></script>
   <script src="js/pickadate/picker.date.js"></script>
   <script src="js/pickadate/picker.time.js"></script>
   <script src="js/pickadate/legacy.js"></script>
   <script>
                         
                         function statusPayout(idx) {
                           if (idx == "1") {
                              return "On Going";
                           }else if(idx == "2") {
                              return "Paid";
                           }else if(idx == "3"){
                              return "Paid to KB E-Wallet";
                           }else{
                              return "---";
                           }
                         }

                         $('.datepickers').pickadate({
                            format: 'dd-mm-yyyy'

                         });

                         $(document).ready(function () {

                            $(".pUpdates").on('click', function () {
                              var updateId = this.id;
                              $pAmountElm = $("#pAmount" + updateId);
                              var updateAmount = $("#pAmount" + updateId).val();
                              var updateDate = $("#pDate" + updateId).val();
                              var updateStatus = $("#pStatus" + updateId).val();
                              var r = confirm("Update Payout " + $pAmountElm.data("no") + 
                                 "\nAmount (from): " + $pAmountElm.data("value") + "\nAmount (to): " + $pAmountElm.val() +
                                 "\nDue Date: " + updateDate + "\nStatus: " + statusPayout(updateStatus) + "\n\nAre you sure?");
                              if (r == true) {
                                 $.post("posts.php", {job: "updatePayout", iId: "<?= $n ?>", email: "<?= $email ?>", cname: "<?= $campaignName ?>", dId: updateId, amount: updateAmount, date: updateDate, status: updateStatus}, function (reply) {
                                    reply = JSON.parse(reply);
                                    $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                                    $("#popupText").html(reply);
                                 });
                              }



                            });

                            $(".files").on("change", function () {
                               var dId = this.id;
                               var formData = new FormData();
                               var amount = $("#pAmount" + dId).val();
                               var totalAmount = '<?= $expectedPayout ?>';
                               var share = (amount / totalAmount) * 100;
                               var share1 = Math.round(share * 100) / 100;
                               formData.append("file" + dId, $('.files')[dId - 1].files[0]);
                               $.ajax({
                                  type: 'POST',
                                  url: 'posts.php?duty=updatePayout&iId=<?= $n ?>' + '&dId=' + dId + '&email=<?= $email ?>&name=<?= $firstname ?>' + '&amount=' + amount + '&cName=<?= $campaignName ?>&pType=<?= $pType ?>&tPayout=<?= $expectedPayout ?>&share=' + share1,
                                  data: formData,
                                  cache: false,
                                  contentType: false,
                                  processData: false,
                                  success: function (data) {
                                     var reply = JSON.parse(data);
                                     $("#popup").fadeIn(50).delay(3000).fadeOut(200);
                                     $("#popupText").html(reply[0]);
                                     $(".imageLink").eq(dId - 1).html("<a target='_blank' href='../assets/images/payouts/" + reply[1] + "'>Proof of Payout</a>");
                                     $(".deleteLink").eq(dId - 1).css('display', 'inline-block');
                                     $(".files").val("");
                                  },
                                  error: function (data) {
                                     $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                                     $("#popupText").html(data);
                                  }
                               });

                            });

                            $(".deleteLink").on("click", function () {
                               var dId = this.id;
                               var r = confirm("Do you really need to delete?");
                               if (r == true) {
                                  $.post("posts.php", {job: 'removeinvimglink', iId: '<?= $n ?>', dId: dId}, function (reply) {
                                     $("#popup").fadeIn(50).delay(2000).fadeOut(200);
                                     $("#popupText").html(reply);
                                     $(".imageLink").eq(dId - 1).html("");
                                     $(".deleteLink").eq(dId - 1).hide();


                                  });
                               }
                            });

                            $("#followMaster").on("change", function () {
                               var val = $(this).val();

                               $.post("posts.php", {job:"updatefollowmaster",iId:'<?=$n?>',val:val}, function (reply) {
                                  if (reply == 1) {
                                     $("#popup").fadeIn(50).delay(1000).fadeOut(200);
                                     $("#popupText").html("Changes Made");
                                  }
                               });

                            });


                         });




   </script>
</body>


</html>
