<?php
include_once 'security.php';

include_once 'mysql.php';
include_once 'email.php';
$mysql = new mysql();
$email = new email();
$updateID=isset($_GET['updateid']) ? $_GET['updateid'] : '';

if ($_POST["submit"]=="Save") {
       $campaign = test_input($_POST["campaign"]);
       $titlef = test_input($_POST["title"]);
       $desf = $_POST["content"];
       $datef = test_input($_POST["date"]);
	   $image_name = "";
	   
	   if ($updateID==null or $updateID==""){
		    if ($mysql->Connection()) {
				
				if (isset($_FILES["image"]) && $_FILES["image"]["name"] != "") {
				  $file = $_FILES["image"]["tmp_name"];
				  $path = $_FILES["image"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name = $titlef . "." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name);
				}
				if (isset($_FILES["image2"]) && $_FILES["image2"]["name"] != "") {
				  $file = $_FILES["image2"]["tmp_name"];
				  $path = $_FILES["image2"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name2 = $titlef . "2." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name2);
				}
				if (isset($_FILES["image3"]) && $_FILES["image3"]["name"] != "") {
				  $file = $_FILES["image3"]["tmp_name"];
				  $path = $_FILES["image3"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name3 = $titlef . "3." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name3);
				}
				if (isset($_FILES["image4"]) && $_FILES["image4"]["name"] != "") {
				  $file = $_FILES["image4"]["tmp_name"];
				  $path = $_FILES["image4"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name4 = $titlef . "4." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name4);
				}
				if (isset($_FILES["image5"]) && $_FILES["image5"]["name"] != "") {
				  $file = $_FILES["image5"]["tmp_name"];
				  $path = $_FILES["image5"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name5 = $titlef . "5." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name5);
				}
				if (isset($_FILES["image6"]) && $_FILES["image6"]["name"] != "") {
				  $file = $_FILES["image6"]["tmp_name"];
				  $path = $_FILES["image6"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name6 = $titlef . "6." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name6);
				}
				if (isset($_FILES["image7"]) && $_FILES["image7"]["name"] != "") {
				  $file = $_FILES["image7"]["tmp_name"];
				  $path = $_FILES["image7"]["name"];
				  $ext = pathinfo($path, PATHINFO_EXTENSION);

				  $uploaddir = "../assets/images/blog/";
				  $image_name7 = $titlef . "7." . $ext;
				  move_uploaded_file($file, $uploaddir . $image_name7);
				}
				$newId = $mysql->UpdateCampaignStatus($campaign, $titlef, $desf, $datef, $image_name, $image_name2, $image_name3, $image_name4, $image_name5, $image_name6, $image_name7);

				//ambil data investor
				if ($mysql->Connection()) {
					list($name, $cName, $emailAddr) = $mysql->GetAllInvestorEmailforSingleCampaign($campaign);
				}
				
				//ambil nama campaign
				$campaignName = $cName[0];
				
				//potong konten
				$cleanContent = substr(strip_tags($desf), 0, 200);
				
				//ambil template
				$emailId = "project-update";
				if ($mysql->Connection()) {
					$emailHeader 	= $mysql->GetEmailHeader();
					$emailFooter 	= $mysql->GetEmailFooter();
					$emailSubject	= $mysql->GetEmailSubject($emailId);
				}
				
				//looping 
				for ($i=0; $i<count($cName); $i++) {
					$emailBody = $mysql->GetEmailBody($emailId);
					$emailBody = str_replace('{user_name}', $name[$i], $emailBody);
					$emailBody = str_replace('{campaign_name}', $cName[$i], $emailBody);
					$emailBody = str_replace('{campaign_update}', $cleanContent, $emailBody);
					$emailContent	= $emailHeader."".$emailBody."".$emailFooter;
					//ganti isi template
					
					$email->SendEmail($emailAddr[$i], "", $emailSubject, $emailContent, TRUE);
					//kirim email
				}
				//akhir looping
			}
	   } else {
		    if ($mysql->Connection()) {
              $mysql->UpdateCampaignUpdateStatus($updateID, $titlef, $desf);
			}
			if (isset($_FILES["image"]) && $_FILES["image"]["name"] != "") {
              $file = $_FILES["image"]["tmp_name"];
              $path = $_FILES["image"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/"; 
              $image_name = hash('sha256', openssl_random_pseudo_bytes(8)) . "." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate($updateID, $image_name);
				}
			}
			if (isset($_FILES["image2"]) && $_FILES["image2"]["name"] != "") {
              $file = $_FILES["image2"]["tmp_name"];
              $path = $_FILES["image2"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/";
              $image_name2 = hash('sha256', openssl_random_pseudo_bytes(8)) . "2." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name2);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate2($updateID, $image_name2);
				}
			}
			if (isset($_FILES["image3"]) && $_FILES["image3"]["name"] != "") {
              $file = $_FILES["image3"]["tmp_name"];
              $path = $_FILES["image3"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/";
              $image_name3 = hash('sha256', openssl_random_pseudo_bytes(8)) . "3." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name3);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate3($updateID, $image_name3);
				}
			}
			if (isset($_FILES["image4"]) && $_FILES["image4"]["name"] != "") {
              $file = $_FILES["image4"]["tmp_name"];
              $path = $_FILES["image4"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/";
              $image_name4 = hash('sha256', openssl_random_pseudo_bytes(8)) . "4." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name4);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate4($updateID, $image_name4);
				}
			}
			if (isset($_FILES["image5"]) && $_FILES["image5"]["name"] != "") {
              $file = $_FILES["image5"]["tmp_name"];
              $path = $_FILES["image5"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/";
              $image_name5 = hash('sha256', openssl_random_pseudo_bytes(8)) . "5." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name5);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate5($updateID, $image_name5);
				}
			}
			if (isset($_FILES["image6"]) && $_FILES["image6"]["name"] != "") {
              $file = $_FILES["image6"]["tmp_name"];
              $path = $_FILES["image6"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/";
              $image_name6 = hash('sha256', openssl_random_pseudo_bytes(8)) . "6." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name6);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate6($updateID, $image_name6);
				}
			}
			if (isset($_FILES["image7"]) && $_FILES["image7"]["name"] != "") {
              $file = $_FILES["image7"]["tmp_name"];
              $path = $_FILES["image7"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);

              $uploaddir = "../assets/images/blog/";
              $image_name7 = hash('sha256', openssl_random_pseudo_bytes(8)) . "7." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name7);
			  if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgCupdate7($updateID, $image_name7);
				}
			}
			
	   }
	   
	   
	   
      
	   echo '<script type="text/javascript">
           window.location = "listupdates.php"
      </script>';
	      
}




if ($mysql->Connection()) {
       list($campaigids, $titles, $contents, $dates, $images, $images2, $images3, $images4, $images5, $images6, $images7) = $mysql->GetSingleProjectUpdate($updateID);
}
if ($mysql->Connection()) {
       list($campaignName, $campaignID) = $mysql->GetUpdateCampaignList();
}

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}

if ($dates[0]=="" or $dates[0] == null){$dates[0]=date("Y-m-d");}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin - Project Update</title>

              <?php include_once 'include.php'; ?>

              <script>


                     $(document).ready(function () {

                     });

              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Project Update</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body" style="text-align: center">


                                                 <form action="" method="POST" enctype="multipart/form-data">

                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Choose Project  : </label>
                                                                      <select name="campaign" id="campaignSelect" class="cat_dropdown addNewIs form-control"  required>
																		   <option value="">Select Campaign</option>
																		   <?php
																			for ($i = 0; $i < count($campaignID); $i++) {
																			?>
																			<option value="<?= $campaignID[$i] ?>" <?php if ($campaigids[0]==$campaignID[$i]){echo "selected";}?>> <?= $campaignName[$i] ?> </option>
																			<?php 

																			}

																			?>
																		   
																	</select>
                                                               </div>



 

                                                        </div>
                                                        <div class="col-xs-12 col-md-6">

                                                               <div class="form-group">
                                                                      <label>Released Date : </label>
                                                                      <input  type="text" name="date" id="date" value="<?= $dates[0] ?>" class="form-control" required>
                                                               </div>
                                                        </div>


                                                       

                                                        <div class="row">
                                                               <div class="col-xs-12 col-md-12">

                                                               <div class="form-group">
                                                                      <label>Title  : </label>
                                                                      <input  type="text" name="title" value="<?= $titles[0] ?>" class="form-control" required>
                                                               </div>

															</div>
                                                        </div>  
														<div class="row">
                                                               <div class="col-xs-12 col-md-12">
                                                                      <div class="form-group">
                                                                             <label>Content : </label>
                                                                             <textarea name="content" class="form-control"><?= $contents[0] ?></textarea>
                                                                      </div>
                                                               </div>
                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 1</label>
                                                               <input type='file' name='image'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images[0] ?>' alt='<?=$images[0]?>'><br>

                                                               

                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 2</label>
                                                               <input type='file' name='image2'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images2[0] ?>' alt='<?=$images2[0]?>'><br>

                                                               

                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 3</label>
                                                               <input type='file' name='image3'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images3[0] ?>' alt='<?=$images3[0]?>'><br>

                                                               

                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 4</label>
                                                               <input type='file' name='image4'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images4[0] ?>' alt='<?=$images4[0]?>'><br>

                                                               

                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 5</label>
                                                               <input type='file' name='image5'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images5[0] ?>' alt='<?=$images5[0]?>'><br>

                                                               

                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 6</label>
                                                               <input type='file' name='image6'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images6[0] ?>' alt='<?=$images6[0]?>'><br>

                                                               

                                                        </div>
														<div class='col-lg-12 col-md-12 col-xs-12' >
                                                               <label>Campaign Update Image 7</label>
                                                               <input type='file' name='image7'  /><br>

                                                               
                                                                      <img width="90%" src='../assets/images/blog/<?= $images7[0] ?>' alt='<?=$images7[0]?>'><br>

                                                               

                                                        </div>

                                                      <br /><br /><br />

                                                        <input style="margin-top:50px;" type="submit" name="submit" class="btn btn-primary" value="Save"/>
													</form>
                                          </div>
                                   </div>
                            </div>


                            <script src="js/pickadate/picker.js"></script>
                            <script src="js/pickadate/picker.date.js"></script>
                            <script src="js/pickadate/legacy.js"></script>
                            <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css"> 
                            <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
                            <script>

                     $('#date').pickadate({
                            format: 'yyyy-mm-dd'
                     });

                     $(document).ready(function () {
                            $('.image-link').magnificPopup({type: 'image'});


                     });


                            </script>
                            <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>
                            <script>CKEDITOR.replace('content', {

                     });
                            </script>


                            </body>


                            </html>