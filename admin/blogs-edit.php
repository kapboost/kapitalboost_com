<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

$bId = test_input($_GET["b"]);

// if (isset($_POST)) {
if ($_POST['submit'] == "Update") {
    $titlef = test_input($_POST["title"]);
    $desf = $_POST["content"];
    $tagf = test_input($_POST["tag"]);
    $langf = test_input($_POST["lang"]);
    $slugf = test_input($_POST["slug"]);
    $meta_keywords = test_input($_POST['meta_keywords']);
    $meta_description = test_input($_POST['meta_description']);
    $formatted_slug = slug_formatter($slugf);
	echo "<script>console.log('formatted_slug = $formatted_slug')</script>";

    $enabledf = test_input($_POST["enabled"]);
    $datef = test_input($_POST["date"]);

    $title1 = strtolower(str_replace(" ", "-", $titlef));
    $title1 = str_replace("(", "-", $title1);
    $title1 = str_replace(")", "-", $title1);
    $tagff = str_replace(",", ",~", $tagf);
    $slugf = $title1;

    if ($mysql->Connection()) {
      $existSlugBySlug = $mysql->getPostSlugBySlug($formatted_slug, $bId);
      
	  echo "<script>console.log('existSlugBySlug = $existSlugBySlug')</script>";
      if ($existSlugBySlug >= 1) {
		  
          echo "<script>alert('Slug is already exists')</script>";
		  echo "<script>window.location = window.location.href;</script>";
		  
      } else {
			$slugf = $formatted_slug;
		  
		    if ($mysql->Connection()) {
				$mysql->EditBlog($bId, $titlef, $desf, $enabledf, $datef, $slugf, $tagff, $langf, $meta_keywords, $meta_description);
			}

			if (isset($_FILES["image"]) && $_FILES["image"]["name"] != "") {
				$file = $_FILES["image"]["tmp_name"];
				$path = $_FILES["image"]["name"];
				$ext = pathinfo($path, PATHINFO_EXTENSION);

				$uploaddir = "../assets/images/blog/";
				$image_name = $title1 . "." . $ext;
				move_uploaded_file($file, $uploaddir . $image_name);
				if ($mysql->Connection()) {
					$mysql->UpdateUploadedImgBlog($bId, $image_name);
				}
			}
      }
    }

    
}



if ($mysql->Connection()) {
    list($slug, $tags, $title, $des, $images, $enabled, $date, $lang, $meta_keywords, $meta_description) = $mysql->GetSingleBlog($bId);
}

$tags = str_replace("~", "", $tags);

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

function slug_formatter($slug)
{
    $formatted_slug = str_replace(" ", "-", $slug);
    $formatted_slug = str_replace("amp", "", $slug);
    $formatted_slug = preg_replace("/[^A-Za-z0-9\-]/", "", $formatted_slug);
    $formatted_slug = preg_replace("/\-+/", "-", $formatted_slug);
    $formatted_slug = strtolower($formatted_slug);

    return $formatted_slug;
}

if ($date=="" or $date == null) {
    $date=date("Y-m-d");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once 'initialize.php'; ?>

	<title>KB Admin Edit Blog - <?= $title ?></title>

	<?php include_once 'include.php'; ?>

	<script>


		$(document).ready(function () {

		});
	</script>

</head>
<body>
	<?php include_once 'header.php'; ?>
	<?php include_once 'popup.php'; ?>


	<div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

		<div class="row">
			<div class="col-xs-12">
				<div class="general-panel panel">

					<div class="blue-panel-heading panel-heading">
						<span class="header-panel">Blog Detail - <?= $title ?></span>
						<div class="clearfix"></div>
					</div>

					<div class="panel-body" style="text-align: center">


						<form action="" method="POST" enctype="multipart/form-data">

							<div class="col-xs-12 col-md-6">

								<div class="form-group">
									<label>Blog Title : </label>
									<input  type="text" name="title" value="<?= $title ?>" class="form-control" required>
								</div>
							</div>

							<div class="col-xs-12 col-md-6">

								<div class="form-group">
									<label>Released Date : </label>
									<input  type="text" name="date" id="date" value="<?= $date ?>" class="form-control" required>
								</div>
							</div>

							<div class="col-xs-12 col-md-12">

								<div class="form-group">
									<label>Edit permalink </label>
									<input  type="text" name="slug" id="slug" value="<?= $slug?>" class="form-control" required>
								</div>
							</div>


							<div class='col-lg-12 col-md-12 col-xs-12' >
								<label>Blog Image</label>
								<input type='file' name='image'  /><br>

								<?php if ($images != "") {
    ?>
									<img width="90%" src='../assets/images/blog/<?= $images ?>' alt='<?=$images?>'><br>

								<?php
} ?>

							</div>

							<div class="row">
								<div class="col-xs-12 col-md-12">
									<div class="form-group">
										<label>Blog Content : </label>
										<textarea name="content" class="form-control"><?= $des ?></textarea>
									</div>
								</div>
							</div>

							<label>Status : </label>
							<select name="enabled">
								<?php if ($enabled == 0) {
        ?>
									<option value="1" >Enabled</option>
									<option value ="0" selected>Disabled</option>
								<?php
    } else {
        ?>
									<option value="1" selected="" >Enabled</option>
									<option value ="0" >Disabled</option>
								<?php
    } ?>
							</select><br><br>

							<label>Languange : </label>
							<select name="lang">
								<?php if ($lang == 0) {
        ?>
									<option value="1" >Bahasa Indonesia</option>
									<option value ="0" selected>English</option>
								<?php
    } else {
        ?>
									<option value="1" selected="" >Bahasa Indonesia</option>
									<option value ="0" >English</option>
								<?php
    } ?>
							</select><br><br>

							<div class="form-group col-md-8 col-md-offset-2">
								<label for="tag">Tags</label>
								<input type="text" class="form-control" name="tag" id="tag" value="<?=$tags?>" placeholder="Tags | seperate with comma, no space before and after comma">
							</div>

							<div class="form-group col-md-8 col-md-offset-2">
								<label for="meta_keywords">Meta Keywords</label>
								<input type="text" class="form-control" name="meta_keywords" id="meta_keywords" placeholder="blog, fintech, other" value="<?= $meta_keywords ?>">
							</div>

							<div class="form-group col-md-8 col-md-offset-2">
								<label for="meta_description">Meta Description</label>
								<textarea class="form-control" name="meta_description" id="meta_description" rows="4"><?= $meta_description ?></textarea>
							</div>
							<div class="clearfix"></div>
							<input type="submit" class="btn btn-primary" name="submit" type="submit" value="Update"/>
						</form>
					</div>
				</div>
			</div>


			<script src="js/pickadate/picker.js"></script>
			<script src="js/pickadate/picker.date.js"></script>
			<script src="js/pickadate/legacy.js"></script>
			<link rel="stylesheet" href="js/magnific-popup/magnific-popup.css">
			<script src="js/magnific-popup/jquery.magnific-popup.js"></script>
			<script>

				$('#date').pickadate({
					format: 'yyyy-mm-dd'
				});

				$(document).ready(function () {
					$('.image-link').magnificPopup({type: 'image'});


				});


			</script>
			<script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>
			<script>CKEDITOR.replace('content', {

			});
		</script>


	</body>


	</html>
