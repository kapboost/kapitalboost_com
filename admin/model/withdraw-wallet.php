<?php

class WithdrawWallet {
	var $dbhost = null;
	var $dbuser = null;
	var $dbpass = null;
	var $dbname = null;
	var $conn = null;
	var $result = null;

	function __construct() {
		include 'db-config.php';
	}

	public function Connection() {
		$this->conn = new mysqli($this->dbhost, $this->dbuser, $this->dbpass, $this->dbname);
		if (mysqli_connect_error()) {
			return false;
		}
		mysqli_set_charset($this->conn, 'utf8');
		return true;
	}

	// Function connect to sql

	public function CloseConnection() {
		if ($this->conn != null) {
			mysqli_close($this->conn);
		}
	}

	// Function disconnect to sql
	
	public function GetAllWithdrawRequest() {
	  
	  $sql = "SELECT * FROM wallet_withdraw inner join tmember where wallet_withdraw.member_id=tmember.member_id order by wallet_withdraw.id desc";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
             $id[$i] = $row["id"];
			 $mId[$i] = $row["member_id"];
			 $amount[$i] = $row["amount"];
			 $date[$i] = $row["date"];
			 $status[$i] = $row["status"];
			 $proof_payment[$i] = $row["proof_payment"];
			 $date_transfer[$i] = $row["date_transfer"];
			 $mFName = $row["member_firstname"];
			 $mSName = $row["member_surname"];
			 $bankName[$i] = $row["bank_name"];
			 $bankAccName[$i] = $row["account_name"];
			 $bankAccNumber[$i] = $row["account_number"];
			 $bankSwift[$i] = $row["swift_code"];
			 $mName[$i] = $mFName." ".$mSName;
            $i++;
         }
         return array($id, $mId, $amount, $date, $status, $proof_payment, $date_transfer, $mName, $bankName, $bankAccName, $bankAccNumber, $bankSwift);
      }
   }
   
   public function GetWithdrawRequestDetail($id) {
	  
	  $sql = "SELECT * FROM wallet_withdraw inner join tmember where wallet_withdraw.member_id=tmember.member_id and id=$id";
      $result = mysqli_query($this->conn, $sql);
      if (mysqli_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysqli_fetch_assoc($result)) {
             $id[$i] = $row["id"];
			 $mId[$i] = $row["member_id"];
			 $amount[$i] = $row["amount"];
			 $date[$i] = $row["date"];
			 $status[$i] = $row["status"];
			 $proof_payment[$i] = $row["proof_payment"];
			 $date_transfer[$i] = $row["date_transfer"];
			 $mFName = $row["member_firstname"];
			 $mSName = $row["member_surname"];
			 $bankName[$i] = $row["bank_name"];
			 $bankAccName[$i] = $row["account_name"];
			 $bankAccNumber[$i] = $row["account_number"];
			 $bankSwift[$i] = $row["swift_code"];
			 $mName[$i] = $mFName." ".$mSName;
            $i++;
         }
         return array($mId, $amount, $date, $status, $proof_payment, $date_transfer, $mName, $bankName, $bankAccName, $bankAccNumber, $bankSwift);
      } 
   }
   
   public function UpdateUploadedImgTransferWithdraw($bId, $iName, $mId, $amount) {
      $sql = "UPDATE wallet_withdraw SET proof_payment = '$iName', status = '1', date_transfer=now() WHERE id = '$bId'";
	  // echo "'<script>console.log(\"sql : $sql\")</script>'";  
      $result = mysqli_query($this->conn, $sql);
	  $this->InsertSingleTransaction($mId, "Withdrawal of funds to bank account", "Withdrawal", $amount) ;
      return $result;
   }
	
	public function InsertSingleTransaction($mId, $transDesc, $transStats, $transAmount) {
		$sql="INSERT INTO twallet_transaction (member_id,stats,amount,description,date) VALUES ('$mId','$transStats','$transAmount','$transDesc',NOW())";
		echo "'<script>console.log(\"sql : ".$sql."\")</script>'";
      $stmt = $this->conn->prepare("INSERT INTO twallet_transaction (member_id,stats,amount,description,date) VALUES ('$mId','$transStats','$transAmount','$transDesc',NOW())"); 
      $result = $stmt->execute();
      return $result;
   }

	 

}






?>