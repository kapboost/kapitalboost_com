<!DOCTYPE html>
<html>

<?php
include_once 'security.php';
include_once 'mysql.php';
include_once 'lib/helper.php';

$mysql = new mysql();
$id = $_GET['c'];

if ($mysql->Connection()) {

	contract_number_generator($mysql);

	if ($_POST) {
		list($result, $err, $saved_id) = $mysql->updateDatas($_POST, 'tcontract_ukm', 'campaign_id', $id);

		// print_r($_POST);
		// exit();

		$mysql->updateDatas( [
			'exchange' => $_POST['exchange_rate'], 'dir_name' => $_POST['signee_1'], 'dir_email' => $_POST['email_1'], 'dir_title' => $_POST['position_1'], 'dir_ic_id' => $_POST['id_number_1'],
			'company_name' => $_POST['company_name'], 'company_reg_type_eng' => $_POST['company_reg_type_eng'], 'company_reg_type_ina' => $_POST['company_reg_type_ina'],
			'company_register_id' => $_POST['company_reg_number'], 'company_register' => $_POST['company_reg_number']
			] , 'tcontract', 'campaign_id', $id);

			if ($result) {
				$msg = array("Contract data has been successfully saved.", "alert-success");
			}else{
				$msg = array("Ops, Contract data was not successfully to save.", "alert-danger");
			}
		}

		$contract_data = $mysql->getDataContract($id);
		if (count($contract_data) > 0) {
			$contract_data = $contract_data[0];
		}else{
			$contract_number = contract_number_generator($mysql);
			$mysql->AddNewContractUKM($id, $contract_number);

			$contract_data = $mysql->getDataContract($id);
			$contract_data = $contract_data[0];
		}

		$contract_ukm = auto_filled_contract_datas($mysql, $contract_data);

		// Upload Actions
		if (isset($_FILES["files"]) && $_FILES["files"]["name"][0] != "") {
			// print_r("uploads");
			upload_contract($mysql, $contract_ukm['id'], $contract_ukm['id'].$contract_ukm['campaign_id'], $_FILES, "CU_".$contract_ukm['id']);

		}
		$contract_files = $mysql->getUKMFiles($contract_ukm['id']);

		// var_dump(  $contract_files = $mysql->getUKMFiles($contract_ukm['id']) );
		// exit();
	}
	?>

	<head>
		<?php include_once 'initialize.php'; ?>
		<title>Investment Payout</title>
		<?php include_once 'include.php'; ?>
		<link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/bootstrap/datepicker/datepicker.css">

		<style type="text/css">
		.title-line {
			position: relative;
			margin-bottom: 40px;
		}
		.title-line:after {
			content: ' ';
			width: 100px;
			height: 4px;
			background: #7f8c8d;
			position: absolute;
			bottom: -15px;
			left: 0;
		}
		.btn-status {
			width: 170px;
		}
		.panel {
			padding-bottom: 30px;
		}
		.c-alert {
			position: absolute;
			top: 10px;
			right: 10px;
			z-index: 999;
		}
		.f-alert {
			position: fixed;
			z-index: 999;
			right: 10px;
			top: 50px;
		}
		#contract-files .thumbnail {
			height: 250px;overflow-y: scroll;position: relative;
		}
		#contract-files .thumbnail .btn-delete {
			position: absolute;top: 5px;right: 5px;background: none;border: none;
		}
		#contract-files .thumbnail .btn-delete .fa {
			padding: 5px 6px;
			border-radius: 50%;
			background: rgba(183, 24, 24, 0.4);
			color: #fff;
		}
		#contract-files .thumbnail .btn-delete:hover .fa {
			background: rgba(183, 24, 24, 0.9);
		}
		#loading {
			width: 100%;
			height: 100%;
			position: fixed;
			top: 0;
			z-index: 9999;
			display: table;
			background: rgba(0,0,0,.5);
		}
		#loading .animation-size {
			vertical-align: middle;
			display: table-cell;
			text-align: center;
		}
		#loading .animation-size h5 {
			color: #fff;
			font-size: 20px;
		}
		#loading .animation-size h3 {
			color: #fff;
			font-size: 42px;
		}
		#loading.hide {
			display: none;
		}
		</style>
	</head>
	<body>

		<div class="loading hide" id="loading">
			<div class="animation-size">
				<svg width="150" height="130" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-infinity">
					<path fill="none" ng-attr-stroke="{{config.stroke}}" ng-attr-stroke-width="{{config.width}}" ng-attr-stroke-dasharray="{{config.dasharray}}" d="M24.3,30C11.4,30,5,43.3,5,50s6.4,20,19.3,20c19.3,0,32.1-40,51.4-40 C88.6,30,95,43.3,95,50s-6.4,20-19.3,20C56.4,70,43.6,30,24.3,30z" stroke="#fdfdfd" stroke-width="4" stroke-dasharray="205.271142578125 51.317785644531256"><animate attributeName="stroke-dashoffset" calcMode="linear" values="0;256.58892822265625" keyTimes="0;1" dur="1" begin="0s" repeatCount="indefinite"></animate></path>
				</svg>
				<h5 class="text-loading"></h5>
			</div>
		</div>

		<?php include_once 'header.php'; ?>

		<div class="main-content-area-wrapper container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<div class="panel" style="margin-top: 30px;position: relative;">
						<?php if (isset($msg)): ?>
							<div class="c-alert alert <?php echo $msg[1]?>" role="alert">
								<?php echo $msg[0]?>
							</div>
						<?php endif; ?>
						<div class="container">
							<div class="row">
								<div class="col-lg-4">
									<h3 class="title-line">Contract UKM</h3>
								</div>
								<div class="col-lg-8" style="margin-top: 15px;text-align: right;">
									<?#php if ($id == '125'): ?>
									<a href="/admin/akad-generator/edit-contract.php?id=<?php echo $contract_ukm['id'] ?>" class="btn btn-info btn-md">Edit Contract</a>
									<?#php endif; ?>
									<?php if ($contract_ukm['is_generated']): ?>
										<!-- <button class="btn btn-info btn-md generate-contract" data-id="<?#php echo $contract_ukm['id'] ?>">Re-Generate</button> -->
										<a href="<?php echo($contract_ukm['pdf_file']) ?>" target="_blank" class="btn btn-default btn-md">Preview PDF</a>
										<button class="btn btn-primary btn-md sent-document" data-id="<?php echo $contract_ukm['id'] ?>">Sent Contract</button>
									<?php else: ?>
										<!-- <button class="btn btn-info btn-md generate-contract" data-id="<?#php echo $contract_ukm['id'] ?>">Generate to PDF</button> -->
									<?php endif ?>
								</div>
								<div class="col-lg-12">

									<form action="/admin/contract_ukm.php?c=<?php echo $id ?>" method="post" enctype="multipart/form-data">
										<div class="row">
											<div class="col-lg-2">
												<div class="form-group">
													<label for="contract_number">Contract No.</label>
													<input type="text" class="form-control" id="contract_number" name="contract_number" value="<?php echo($contract_ukm['contract_number']) ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="company_code">Company Code</label>
													<input type="text" class="form-control" id="company_code" name="company_code" value="<?php echo(company_code_generator($contract_ukm['company_name'])) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label for="company_name">Company Name</label>
													<input type="text" class="form-control" id="company_name" name="company_name" value="<?php echo($contract_ukm['company_name']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="signee_1">Signee 1</label>
													<input type="text" class="form-control" id="signee_1" name="signee_1" value="<?php echo($contract_ukm['signee_1']) ?>">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="signee_2">Signee 2</label>
													<input type="text" class="form-control" id="signee_2" name="signee_2" value="<?php echo($contract_ukm['signee_2']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="email_1">Email UKM 1</label>
													<input type="text" class="form-control" id="email_1" name="email_1" value="<?php echo($contract_ukm['email_1']) ?>">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="email_2">Email UKM 2</label>
													<input type="text" class="form-control" id="email_2" name="email_2" value="<?php echo($contract_ukm['email_2']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="id_number_1">ID Number 1</label>
													<input type="text" class="form-control" id="id_number_1" name="id_number_1" value="<?php echo($contract_ukm['id_number_1']) ?>">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="id_number_2">ID Number 2</label>
													<input type="text" class="form-control" id="id_number_2" name="id_number_2" value="<?php echo($contract_ukm['id_number_2']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="position_1">Job Position 1</label>
													<input type="text" class="form-control" id="position_1" name="position_1" value="<?php echo($contract_ukm['position_1']) ?>">
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="position_2">Job Position 2</label>
													<input type="text" class="form-control" id="position_2" name="position_2" value="<?php echo($contract_ukm['position_2']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="company_reg_type_eng">Company Registration Type (English)</label>
													<select class="form-control" id="company_reg_type_eng" name="company_reg_type_eng">
														<option value="0" <?php echo ($contract_ukm['company_reg_type_eng'] == 0) ? "selected":"" ?>>--- Please Select ---</option>
														<option value="1" <?php echo ($contract_ukm['company_reg_type_eng'] == 1) ? "selected":"" ?>>Company Registration No.</option>
														<option value="2" <?php echo ($contract_ukm['company_reg_type_eng'] == 2) ? "selected":"" ?>>Single Business No.</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label for="company_reg_type_ina">Company Registration Type (Bahasa)</label>
													<select class="form-control" id="company_reg_type_ina" name="company_reg_type_ina">
														<option value="0" <?php echo ($contract_ukm['company_reg_type_ina'] == 0) ? "selected":"" ?>>--- Please Select ---</option>
														<option value="1" <?php echo ($contract_ukm['company_reg_type_ina'] == 1) ? "selected":"" ?>>Nomor Tanda Daftar Perusahaan (TDP)</option>
														<option value="2" <?php echo ($contract_ukm['company_reg_type_ina'] == 2) ? "selected":"" ?>>Nomor Induk Berusaha (NIB)</option>
													</select>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="company_reg_number">Company Registration No.</label>
													<input type="text" class="form-control" id="company_reg_number" name="company_reg_number" value="<?php echo($contract_ukm['company_reg_number']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="customer_name_eng">Customer Name (English)</label>
													<input type="text" class="form-control" id="customer_name_eng" name="customer_name_eng" value="<?php echo($contract_ukm['customer_name_eng']) ?>">
												</div>
											</div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="customer_name_ina">Customer Name (Bahasa)</label>
													<input type="text" class="form-control" id="customer_name_ina" name="customer_name_ina" value="<?php echo($contract_ukm['customer_name_ina']) ?>">
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="target_funding_amount">Target Funding Amount</label>
													<input type="text" class="form-control" id="target_funding_amount" name="target_funding_amount" value="<?php echo($contract_ukm['target_funding_amount']) ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="tenor_eng">Tenor (English)</label>
													<input type="text" class="form-control" id="tenor_eng" name="tenor_eng" value="<?php echo($contract_ukm['tenor_eng']) ?>">
													<small>Note : input just support for a number and dot (.)</small>
													<small>Example : 1.5</small>
												</div>
											</div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="tenor_ina">Tenor (Bahasa)</label>
													<input type="text" class="form-control" id="tenor_ina" name="tenor_ina" value="<?php echo($contract_ukm['tenor_ina']) ?>">
													<small>Note : input just support for a number and dot (.)</small>
													<small>Example : 1.5</small>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="wakalah_fee_kb">Wakalah Fee (Kapital Boost)</label>
													<input type="text" class="form-control" id="wakalah_fee_kb" name="wakalah_fee_kb" value="<?php echo(number_format($contract_ukm['wakalah_fee_kb'], 1, '.', ',')) ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-3">
												<div class="form-group">
													<label for="wakalah_fee_investor">Wakalah Fee (Investor)</label>
													<input type="text" class="form-control" id="wakalah_fee_investor" name="wakalah_fee_investor" value="<?php echo(number_format($contract_ukm['wakalah_fee_investor'], 1, '.', ',')) ?>" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
											<div class="col-lg-5">
												<div class="form-group">
													<label for="bilyet_giro_note_eng">Bilyet Giro Information (English)</label>
													<textarea class="form-control" id="bilyet_giro_note_eng" name="bilyet_giro_note_eng" rows="4"><?php echo($contract_ukm['bilyet_giro_note_eng']) ?></textarea>
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group">
													<label for="bilyet_giro_note_ina">Bilyet Giro Information (Bahasa)</label>
													<textarea class="form-control" id="bilyet_giro_note_ina" name="bilyet_giro_note_ina" rows="4"><?php echo($contract_ukm['bilyet_giro_note_ina']) ?></textarea>
												</div>
											</div>
											<div class="clearfix"></div>
											<!-- <div class="col-lg-3">
											<div class="form-group">
											<label for="repayment_date_eng">Repayment Date (English)</label>
											<input type="text" class="form-control" id="repayment_date_eng" name="repayment_date_eng" readonly value="<//?php echo($contract_ukm['repayment_date_eng']) ?>" readonly>
										</div>
									</div> -->
									<!-- <div class="col-lg-3">
									<div class="form-group">
									<label for="repayment_date_ina">Repayment Date (Bahasa)</label>
									<input type="text" class="form-control" id="repayment_date_ina" name="repayment_date_ina" readonly value="<//?php echo($contract_ukm['repayment_date_ina']) ?>" readonly>
								</div>
							</div> -->
							<div class="clearfix"></div>
							<div class="col-lg-5">
								<div class="form-group">
									<label for="other_requirements_eng">Other Requirements (English)</label>
									<textarea class="form-control" id="other_requirements_eng" name="other_requirements_eng" rows="4"><?php echo($contract_ukm['other_requirements_eng']) ?></textarea>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									<label for="other_requirements_ina">Other Requirements (Bahasa)</label>
									<textarea class="form-control" id="other_requirements_ina" name="other_requirements_ina" rows="4"><?php echo($contract_ukm['other_requirements_ina']) ?></textarea>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-3">
								<div class="form-group">
									<label for="exchange_rate">Exchange Rate</label>
									<input type="text" class="form-control" id="exchange_rate" name="exchange_rate" value="<?php echo($contract_ukm['exchange_rate']) ?>">
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="total_payout_sgd">Total Payout (SGD)</label>
									<input type="text" class="form-control" id="total_payout_sgd" data-value="<?php echo($contract_ukm['total_payout_sgd']) ?>" name="total_payout_sgd" readonly value="<?php echo(number_format($contract_ukm['total_payout_sgd'], 1, '.', ',')) ?>">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="total_payout_idr">Total Payout (IDR)</label>
									<input type="text" class="form-control" id="total_payout_idr" name="total_payout_idr" readonly value="<?php echo(number_format($contract_ukm['total_payout_idr'], 1, '.', ',')) ?>">
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-5">
								<div class="form-group">
									<label for="payment_method_eng">Payment Method (English)</label>
									<!-- <input type="text" class="form-control" id="payment_method_eng" name="payment_method_eng" value="<?php echo($contract_ukm['payment_method_eng']) ?>"> -->
									<textarea class="form-control" rows="4" id="payment_method_eng" name="payment_method_eng"><?php echo($contract_ukm['payment_method_eng']) ?></textarea>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									<label for="payment_method_ina">Payment Method (Bahasa)</label>
									<!-- <input type="text" class="form-control" id="payment_method_ina" name="payment_method_ina" value="<?php echo($contract_ukm['payment_method_ina']) ?>"> -->
									<textarea class="form-control" rows="4" id="payment_method_ina" name="payment_method_ina"><?php echo($contract_ukm['payment_method_ina']) ?></textarea>
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-2">
								<div class="form-group">
									<label for="admin_fee_pct">Admin Fee (PCT)</label>
									<input type="text" class="form-control" id="admin_fee_pct" name="admin_fee_pct" value="<?php echo($contract_ukm['admin_fee_pct']) ?>">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="admin_fee_amt">Admin Fee (AMT)</label>
									<input type="text" class="form-control" id="admin_fee_amt" name="admin_fee_amt" value="<?php echo($contract_ukm['admin_fee_amt']) ?>">
								</div>
							</div>
							<div class="clearfix"></div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="files">Documents</label>
									<input type="file" class="form-control" id="files" name="files[]" multiple>
								</div>
							</div>
							<?php if (count($contract_files) > 0): ?>
								<div class="col-lg-4">
									<div class="form-group">
										<label for="files">&nbsp;</label>
										<br>
										<button type="button" class="btn btn-default" data-toggle="modal" data-target="#contract-files">Show Images</button>
									</div>
								</div>
							<?php endif ?>
						</div>
						<hr>
						<div class="row">
							<div class="clearfix"></div>
							<div class="col-lg-2">
								<button class="btn btn-primary btn-block" type="submit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="contract-files" tabindex="-1" role="dialog" aria-labelledby="contract-files">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Contract Files</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="alert alert-info hide"></div>
					</div>
					<?php foreach ($contract_files as $cf): ?>
						<div class="col-lg-6">
							<h5><?php echo $cf['file_name'] ?></h5>
							<div class="thumbnail">
								<button type="button" class="btn-delete" data-id="<?php echo $cf['id'] ?>"><i class="fa fa-times"></i></button>
								<img src="<?php echo $cf['folder_path'].$cf['file_name'] ?>" alt="<?php echo $cf['tcontract_ukm_id'] ?>" width="100%">
							</div>
						</div>
					<?php endforeach ?>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo ADMIN_PATH ?>/bootstrap/datepicker/datepicker.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	$('[data-toggle="datepicker"]').datepicker({format: 'yyyy-mm-dd'});

	setTimeout(function() {
		$(".c-alert").delay(1000).fadeOut(300);
	})

	<?php if(isset($_GET['p']) && $_GET['p'] == '1'){ echo "$('#contract-files').modal('show')"; } ?>

	$(".btn-delete").click(function() {
		$id = $(this).data('id');

		var r = confirm("Are you sure?");
		if (r == true) {
			$.ajax({
				type: 'GET',
				url: "./api.php?api_action=delete-contract-files&c="+$id,
				contentType: false,
				cache: false,
				processData:false,
				success: function(callback){
					console.log(callback);
					var result = JSON.parse(callback);
					if (result['result'] == 1) {
						$("#contract-files .alert").removeClass('hide');
						$("#contract-files .alert").text("Document was deleted.");
						$("#contract-files").animate({scrollTop:$("#contract-files .alert").offset().top},"500");

						setTimeout(function() {
							window.location.replace("<?php echo ADMIN_PATH ?>/contract_ukm.php?c=<?php echo $id ?>&p=1");
						}, 2000);
					}else{
						$("#contract-files .alert").text("Ops, Something when wrong. Failed to delete document.");
					}
				}
			})
		}

	})

	$(".generate-contract").click(function() {
		$("#loading .text-loading").text("Generating Contract to PDF...");
		$("#loading").removeClass("hide");

		id = $(this).data('id');

		$.ajax({
			type: 'GET',
			url: "./ukmPDF.php?id="+id,
			contentType: false,
			cache: false,
			processData:false,
			success: function(callback){
				var result = JSON.parse(callback);
				// console.log(result);
				if (result['result']) {
					$elm = "<h3>PDF File successfully to create.</h3>";
					$("#loading .animation-size").html($elm);

					setTimeout(function() {
						window.location.replace("<?php echo ADMIN_PATH ?>/contract_ukm.php?c=<?php echo $id ?>");
					}, 1000);
				}else{
					$elm = "<h3>Failed to generate PDF.</h3>";
					$("#loading .animation-size").html($elm);

					setTimeout(function() {
						$("#loading").addClass("hide");
					}, 1000);
				}


			}
		})
	})

	$(".sent-document").click(function() {
		$("#loading .text-loading").text("Sending Contract...");
		$("#loading").removeClass("hide");

		id = $(this).data('id');

		$.ajax({
			type: 'GET',
			url: "./sendPDF_UKM.php?id="+id,
			contentType: false,
			cache: false,
			processData:false,
			success: function(callback){
				var result = JSON.parse(callback);
				// console.log(result);
				if (result['result']) {
					$elm = "<h3>Contract has been sent.</h3>";
					$("#loading .animation-size").html($elm);

					setTimeout(function() {
						$("#loading").addClass("hide");
						// window.location.replace("<?php #echo ADMIN_PATH ?>/contract_ukm.php?c=<?php echo $id ?>");
					}, 1000);
				}else{
					$elm = "<h3>Failed to send contract.</h3>";
					$("#loading .animation-size").html($elm);

					setTimeout(function() {
						$("#loading").addClass("hide");
						// window.location.replace("<?php #echo ADMIN_PATH ?>/contract_ukm.php?c=<?php echo $id ?>");
					}, 1000);
				}

			}
		})
	})

	$("#exchange_rate").on("keyup", function() {
		$funding_amt = parseFloat($("#total_payout_sgd").data('value'));
		$cal = parseFloat($(this).val()) * $funding_amt;

		$("#total_payout_idr").val($cal);
	});
})
</script>
</body>
</html>
