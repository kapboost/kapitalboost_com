<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($cuId, $cuName, $cuEmail, $cuMsg, $cuDate) = $mysql->GetContactUs();
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Contact Us Enquiries</title>

              <?php include_once 'include.php'; ?>
              
              <script>
                     $(document).ready(function () {
                            $(".DelBtn").click(function () {
                                   var index = $(this).parent().parent().index();
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {dcontactus: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        $("tr").eq(index + 1).remove();
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });

                     });
              </script>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Overview</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">


                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="2%">No</th>
                                                                             <th width="3%">Name</th>
                                                                             <th width="4%">Email</th>
                                                                             <th width="*%">Message</th>
                                                                             <th width="8%">Date</th>
                                                                             <th width="7%">Delete</th>
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($cuId); $i++) { ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><?= $cuName[$i] ?></td>
                                                                                    <td><?= $cuEmail[$i] ?></td>
                                                                                    <td><?= $cuMsg[$i] ?></td>
                                                                                    <td><?=$cuDate[$i]?></td>
                                                                                    <td style="text-align: center"><button value="<?= $cuId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                                                                             </tr>

                                                                      <?php } ?>

                                                               </tbody>
                                                        </table>
                                                 </div>







                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>




       </body>


</html>