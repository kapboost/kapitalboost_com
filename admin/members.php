<?php
include_once 'security.php';

// include_once 'mysql.php';
// $mysql = new mysql();

// if (isset($_GET["add"])) {
//        if ($_GET["add"] == "1") {
//               if ($mysql->Connection()) {
//                      $newmId = $mysql->AddNewMember1();
//                      header("Location: member-edit.php?m=$newmId");
//               }
//        }
// }

// if (isset($_GET["keyword"])) {
//        $keyword = $_GET["keyword"];
// } else {
//        $keyword = "";
// }
// if ($mysql->Connection()) {

//        list($mId, $validated, $mUserName, $mFullname, $mEmail, $mCountry, $mCreatedDate, $mWallet, $member_status, $phone_number) = $mysql->GetAllMembers($keyword);
// }
?>


<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Members</title>

              <?php include_once 'include.php'; ?>

              <script>
                function deletMember($this) {
                  // $(".DelBtn").click(function () {
                        $this = $($this);
                         var index = $this.parent().parent().index();
                         console.log(index);
                         var person = prompt("Please enter Master Code");
                         $.post("delete.php", {dmember: $this.val(), MC: person}, function (reply) {
                                reply = JSON.parse(reply);
                                $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                       if (reply === "Done") {
                                              $("tr").eq(index + 1).remove();
                                       }
                                });
                                $("#popupText").html(reply);
                         });
                  // });
                }
                     $(document).ready(function () {


                            $(".validated").change(function () {
                                   var data = $this.val();
                                   var res = data.split("~");
                                   $.post("posts.php", {job: "validatedalter",mId: res[0], val: res[1]}, function (reply) {

                                   });
                            });

                     });

              </script>

       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Kapital Boost's Members</span>
                                                 <a class="btn btn-warning" style="margin: auto 20px;width:200px" href="members.php?add=1">Add New Member</a>
                                                 <!--<form method="get" action="members.php" style="display:inline-block;">
                                                        <input type="text" name="keyword" placeholder="Username | Firstname | Surname | Email Address" value="<?= $keyword ?>" style="width: 500px;color:black;font-size:0.7em;padding:5px;border-radius:5px" />
                                                        <input class="btn btn-primary" type="submit" value="Search"/>
                                                 </form>-->
												 <form method="post" action="excel-extract.php" style="display:inline-block;">
														<input type="hidden" name="action"  value="member"/>
                                                        <input class="btn btn-info" type="submit" value="Extract Member Data"/>
                                                 </form>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div class="panel-body">



                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover" name="myTable" id="myTable">
                                                               <thead>
                                                                      <tr>
                                                                             <th width="5%">No</th>
                                                                             <th width="15%">Full Name</th>
                                                                             <!--<th width="20%">Username</th>-->
                                                                             <th width="20%">Member Email</th>
                                                                             <th width="15%">Country</th>
                                                                             <th width="*">Created Date</th>
                                                                             <th width="*">Wallet Amount</th>
                                                                             <th width="*">Phone</th>
																			 <th width="*">Status</th>
                                                                             <!--<th width="5%">Validated</th>-->
                                                                             <th width="7%">Delete</th>
                                                                      </tr>
                                                               </thead>

                                                        </table>
                                                 </div>





                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


		<script>
		$(document).ready( function () {
			var myTable = $('#myTable').DataTable( {

           "iDisplayLength": 10,
            "bProcessing": true,
            stateSave: true,
            "sAjaxSource": "getJson.php?table=member",
            "aoColumns": [
                  { mData: 'no' },
                  { mData: 'full_name' },
                  { mData: 'member_email' },
                  { mData: 'current_location' },
                  { mData: 'insert_dt' },
                  { mData: 'wallet' },
                  { mData: 'member_mobile_no' },
                  { mData: 'member_status' },
                  { mData: 'delete_button' }
            ]
        });
		} );
		</script>

       </body>


</html>
