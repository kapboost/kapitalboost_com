<?php

include_once 'mysql.php';
$mysql = new mysql();

if ($mysql->Connection()) {
       list($cName, $cId,$cReturn) = $mysql->GetAllCampaignNameAndcReturn();
       for ($i = 0; $i < count($cId); $i++) {
              list($iId, $totalFunding,$expectedPayout) = $mysql->GetAllInvestmentsSimple($cId[$i]);
              for($ii=0;$ii<count($iId);$ii++){
                     $expectedPayout = $totalFunding[$ii] + ($totalFunding[$ii] * $cReturn[$i]/100);
                     $mysql->UpdateExpectedPayout($iId[$ii], $expectedPayout);
              }
       }
}