<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($pageContent) = $mysql->GetGeneralContent(64);
       $pageContents = explode("<!-- ~~~ -->", $pageContent);
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Campaign Content</title>

              <?php include_once 'include.php'; ?>
              <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>

              <script>
                     var headers = "";
                     var contents = "";
                     $(document).ready(function () {
                            $("#AddBtn1").on('click', function () {
                                   var headerInput = document.createElement("INPUT");
                                   headerInput.setAttribute("class", "headers1");
                                   var headerDiv = document.createElement("DIV");
                                   headerDiv.appendChild(headerInput);
                                   var contentInput = document.createElement("TEXTAREA");
                                   contentInput.setAttribute("class", "contents1");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea1");
                                   mainDiv.appendChild(headerDiv);
                                   mainDiv.appendChild(contentDiv);
                            });
                            

                            $(".UpdateBtns").on('click', function () {
                                   $(".headers1").each(function () {
                                          if ($(this).val() != "") {
                                                 headers += $(this).val() + "~~~";
                                          }
                                   });
                                   headers.slice(0, -3);
                                   headers += "+```+";
                                   
                                   headers.slice(0, -3);
                                   $(".contents1").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });


                                   $.post("posts.php", {job: "", headers: headers, contents: contents}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(2000).fadeOut(100);
                                          $("#popupText").html(reply);
                                          headers = "";
                                          contents = "";
                                   });
                            });



                            
                     });
              </script>
              <style>
                     .headers1,.headers2,.headers3,.headers4,.headers5{
                            width: 95%;margin: 15px;padding:10px;border-radius: 10px;font-weight: bolder;font-size: 1.5em
                     }
                     .contents1,.contents2,.contents3,.contents4,.contents5{
                            width:  95%;
                            min-height: 150px;margin:15px;padding:10px;border-radius: 10px;font-size: 1.1em;background-color: #d7e6f4
                     }
                     #AddBtn1,#AddBtn2,#AddBtn3,#AddBtn4,#AddBtn5{
                            background-color: white;border: none;display: inline-block
                     }

              </style>
       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading" style="text-align:center">
                                                 <span class="header-panel">Campaign Content</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div id="smeDiv" class="panel-body" style="text-align:center">
                                                 <h1>Campaign Content</h1>
                                                 <div id="contentArea1">
                                                        <?php for ($i = 0; $i < count($pageContents) -1; $i++) { ?>
															
															<?php if ($i%2==0) { ?>
                                                               <div><input class="headers1" value="<?= strip_tags($pageContents[$i])?>" /></div>
															<?php } else { ?>
                                                               <div><textarea name="smeContent" class="contents1" ><?= $pageContents[$i] ?></textarea></div>

															<?php } ?>
                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn1"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>
                                                 <button class="btn btn-primary UpdateBtns"   style="margin-bottom: 20px">Update All</button>
                                          </div>
                                          
                                          

                                   </div>
                            </div>
                     </div>
              </div>


              <script>

                     CKEDITOR.replace('contents1', {

                     });
              </script>

       </body>


</html>