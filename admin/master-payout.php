<?php
// ini_set('display_errors', 1);
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
$cId = $_GET["c"];

if (isset($_POST["Amount0"])) {

   $mPayoutf = "";
   for ($i = 0; $i < 12; $i++) {
      $amountf[$i] = $_POST["Amount$i"];
      $datef[$i] = $_POST["Date$i"];
      $statusf[$i] = $_POST["Status$i"];
      $mPayoutf .= $amountf[$i] . "~" . $datef[$i] . "~" . $statusf[$i] . "==";
   }

   if ($mysql->Connection()) {
      $mysql->UpdateCampaignMasterPayout($cId, $mPayoutf);
      list($iId, $totalFunding, $expectedPayout) = $mysql->GetAllInvestmentsSimple($cId, 1);

      for ($i = 0; $i < count($iId); $i++) {
         for ($ii = 0; $ii < count($amountf); $ii++) {
            if ($amountf[$ii] != "") {
               $singleAmount = $expectedPayout[$i] * ($amountf[$ii] / 100);
               $mysql->UpdateEachPayout($iId[$i], ($ii + 1), round($singleAmount, 2), $datef[$ii], $statusf[$ii]);
            } else {
               $mysql->UpdateEachPayout($iId[$i], ($ii + 1), '', '', '');
            }
         }
      }
   }
}



if ($mysql->Connection()) {
   list($cName, $mPayout) = $mysql->GetSingleCampaignForPayout($cId);
}
// print_r($mPayout);
// exit();
$payoutA = explode("==", $mPayout);
$amountA = $dateA = $statusA = array();

for ($i = 0; $i < count($payoutA); $i++) {
   $payouts = explode("~", $payoutA[$i]);
   $amountA[$i] = $payouts[0];
   $dateA[$i] = $payouts[1];
   $statusA[$i] = $payouts[2];
}
?>

<!DOCTYPE html>
<html lang="en">
   <head>
<?php include_once 'initialize.php'; ?>

      <title>KB Admin Master Payout Schedule for <?= $cName ?></title>

<?php include_once 'include.php'; ?>
      <link rel="stylesheet" href="https://kapitalboost.com/assets/css/kp-default.css" />
      <link rel="stylesheet" href="https://kapitalboost.com/assets/css/input.css" />

      <script>

          $(document).ready(function () {
             $("#submitButton").on('click', function () {
                var amount = [];
                var totalAmount = 0;
                var notInt = 0;
                for (var i = 0; i < 12; i++) {
                   if ($("#amount" + i).val() != "") {
                      amount[i] = $("#amount" + i).val();
                      if (isNaN(amount[i]) === false) {
                         totalAmount = totalAmount + parseFloat(amount[i]);
                      } else {
                         notInt = 1;
                      }
                   }
                }
                if (notInt === 1) {
                   alert("One or more of the Amount Percentage field is wrong");
                } else {
                   if (parseFloat((totalAmount).toFixed(2)) < 100) {
                      alert("Total Amount Percentage is less than 100%");
                   } else if (parseFloat((totalAmount).toFixed(2)) > 100) {
                      alert("Total Amount Percentage is more than 100%");
                   } else {
                      $('form#myForm').submit();
                   }

                }
             });


          });



      </script>

   </head>
   <body>
<?php include_once 'header.php'; ?>
<?php include_once 'popup.php'; ?>


      <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

         <div class="row">
            <div class="col-xs-12">
               <div class="general-panel panel">

                  <div class="blue-panel-heading panel-heading">
                     <span class="header-panel">Master Payout Schedule for <b><?= $cName ?></b></span>
           <form method="post" action="excel-extract.php" style="display:inline-block;margin: auto 20px;">
              <input type="hidden" name="action"  value="payout"/>
              <input class="btn btn-info" type="submit" value="Extract Payout Data"/>
           </form>
                     <div class="clearfix"></div>
                  </div>

                  <style>
                     .monItem,.monItem2{
                        width:22%;margin:5px;background-color: white;
                     }
                     .monContent{
                        background-color:#ccffff;border-radius: 10px;padding:15px 5px;margin:15px 10px
                     }

                     .monthForm{
                        margin:0 !important; background-color: white !important;border: 1px solid gray !important;
                        font-size:1em !important
                     }

                  </style>
                  <div class="panel-body">
                     <div class="row" style="text-align:center">
                        <form action="" method="POST" id="myForm" >
<?php
for ($i = 0; $i < 12; $i++) {
  $month = $i + 1;
  if ($mysql->Connection()) {
    $currtotalFunding = $mysql->GetTotalInvestmentAmountPerCampaign($cId);
    $returnFunding = $mysql->GetTotalReturnPerCampaign($cId);
    $jumlahBelumPayout = $mysql->GetUnpaidPayout($cId, $month);
    $hitungJumlahInvestor = $mysql->GetTotalInvestorPerCampaign($cId);
    
    $investor_ongoing = $mysql->investorsStatus($cId, $month);
    $mp = explode("==", $mPayout);
    $mp_datas = explode("~", $mp[$i]);
    // echo "<script>alert(".$investor_ongoing.")</script>";
  }

  $totalReturn = ((100+$returnFunding)/100)*$currtotalFunding;
  $totalPayout = ($amountA[$i]/100)*$totalReturn;
  $updateAutoPayout = "";

  // if ($jumlahBelumPayout==0 and $totalPayout>10 and $statusA[$i]!=2 and $hitungJumlahInvestor>0){
  if ($hitungJumlahInvestor > 0 && $investor_ongoing == 0 && $mp_datas[0] != null && $mp_datas[2] == 1) {
    $payoutCheck = explode("==", $mPayout);
    $amountCheck = $dateCheck = $statusCheck = array();

    for ($k = 0; $k < count($payoutCheck); $k++) {
       $payoutsInput = explode("~", $payoutCheck[$k]);
       $amountCheck[$k] = $payoutsInput[0];
       $dateCheck[$k] = $payoutsInput[1];
       if ($k==$i){
         $statusCheck[$k]=2;
       } else {
         $statusCheck[$k] = $payoutsInput[2];
       }
       $updateAutoPayout .= $amountCheck[$k] . "~" . $dateCheck[$k] . "~" . $statusCheck[$k] . "==";

    }
    // echo "'<script>console.log(\"updateAutoPayout : $cId , $updateAutoPayout\")</script>'";
    $mysql->UpdateCampaignMasterPayout($cId, $updateAutoPayout);
    echo "<script>location.reload(true);</script>";
    // echo "<script>console.log(data status perbulan investor perlu dicheck);</script>";
  }

  // if ($totalPayout != 0) {
   ?>
                              <div class="form-group col-xs-12 col-md-6 col-lg-6">

                                 <label>Payout <?= $i + 1 ?>, Total : SGD <?php echo number_format($totalPayout, 1, '.', ','); ?> </label>
                                 <div class="monContent">
                                    <div class="monItem2">
                                       <input type="text" id="amount<?= $i ?>" value="<?= $amountA[$i] ?>" name="Amount<?= $i ?>" class="monthForm" placeholder="Amount %">
                                    </div>
                                    <div class="monItem2">
                                       <input type="text"   name="Date<?= $i ?>" id="date<?= $i ?>" placeholder="date" value="<?= $dateA[$i] ?>" class="monthForm datepickers">

                                    </div>
   <?php if ($statusA[$i] == "") { ?>
                                       <div class="monItem2">
                                          <select  name="Status<?= $i ?>" >
                                             <option value="" selected> ------- </option>
                                             <option value="1">On Going</option>
                                             <option value="2" >Paid </option>
                                             <option value="3" >Delayed </option>

                                          </select>
                                       </div>
                                    <?php } else if ($statusA[$i] == "1") { ?>
                                       <div class="monItem2" >
                                          <select name="Status<?= $i ?>">
                                             <option value=""> ------- </option>
                                             <option value="1" selected>On Going </option>
                                             <option value="2" >Paid</option>
                       <option value="3" >Delayed </option>
                                          </select>
                                       </div>
                                    <?php } else if ($statusA[$i] == "2") { ?>
                                       <div class="monItem2" >
                                          <select name="Status<?= $i ?>">
                                             <option value=""> ------- </option>
                                             <option value="1" >On Going </option>
                                             <option value="2" selected>Paid</option>
                       <option value="3" >Delayed </option>
                                          </select>
                                       </div>
                                    <?php } else if ($statusA[$i] == "3") { ?>
                                       <div class="monItem2" >
                                          <select name="Status<?= $i ?>">
                                             <option value=""> ------- </option>
                                             <option value="1" >On Going </option>
                                             <option value="2" >Paid</option>
                       <option value="3" selected>Delayed </option>
                                          </select>
                                       </div>
                                    <?php } ?>

                                    <div class="monItem2">
                                       <a href="xfers-payout.php?c=<?= $cId ?>&m=<?= $i + 1 ?>" class="btn btn-info" style="height:75%">Make Payout</a>
                                    </div>

                                 </div>
                              </div>
                                 <?php //} ?>
                                 <?php } ?>
                           <div class="col-xs-12 col-md-12 col-lg-12" style="text-align:center;width:100%">
                              <input id="submitButton" class="pUpdates" name="button" type="button"  class="btn btn-primary pUpdate" value="Update" style="margin-left:auto;"/>
                           </div>
                        </form>
                     </div>



                  </div>
               </div>
            </div>
         </div>
      </div>

      <script src="js/pickadate/picker.js"></script>
      <script src="js/pickadate/picker.date.js"></script>
      <script src="js/pickadate/legacy.js"></script>
      <script>

          $('.datepickers').pickadate({
             format: 'dd-mm-yyyy',
             selectMonths: true,
             selectYears: true
          });
      </script>


   </body>


</html>
