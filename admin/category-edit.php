<?php
include_once 'security.php';
include_once 'mysql.php';

$mysql = new mysql();
$cId = test_input($_GET["c"]);
//$category = $mysql->getCategory($cId);
if ($mysql->Connection()){
    $category = $mysql->getCategory($cId);
}

function test_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

if (($_POST['submit']) == "Save Changes") {
    $name      = test_input($_POST['name']);
    $slug   = test_input($_POST['slug']);
    $description = test_input($_POST['description']);

    if ($mysql->Connection()) {
        $mysql->updateCategory($cId, $name, $slug, $description);
        header('Location: '.'category-list.php');
    }
}

// if (($_POST['submit']) == "Delete") {
//     if ($mysql->Connection()) {
//         $mysql->deleteCategory($cId);
//         header('Location: '.'category-list.php');
//     }
// }


if ($date == "" or $date == null) {
    $date = date("Y-m-d");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include_once 'initialize.php'; ?>
    <title>Category List - <?= $title ?></title>
    <?php include_once 'include.php'; ?>
    <script>
        console.log("<?= $category ?>");
    </script>

</head>
<body>
<?php include_once 'header.php'; ?>
<?php include_once 'popup.php'; ?>
<div class="main-content-area-wrapper container-fluid" style="min-height: 800px;">

    <div class="row">
        <div class="col-xs-12">
            <div class="general-panel panel">
                <div class="blue-panel-heading panel-heading">
                    <span class="header-panel" style="padding-right: 10px;">Edit Category</span>
                    <a href="category-list.php"> Category List </a>
                    <div class="clearfix"></div>
                </div>

                <div class="panel-body" style="text-align: center">
                    <h1>Edit Category</h1>
                    <form class="" action="" method="POST">
                        <div class="form-group">
                            <label class="require">Category Name</label>
                            <input class="form-control" type="text" name="name" id="name" value="<?= $category["name"] ?>" required>
                        </div>
                        <div class="form-group">
                            <label class="require">Category Slug</label>
                            <input class="form-control" type="text" name="slug" id="slug" value="<?= $category["slug"] ?>"  required>
                        </div>
                        <div class="form-group">
                            <label class="require">Category Description</label>
                            <input class="form-control" type="text" name="description" value="<?= $category["description"] ?>"  id="description" required>
                        </div>
                        <input class="btn btn-info" type="submit" name="submit" value="Save Changes">
                        <!-- <input class="btn btn-danger" type="submit" name="submit" value="Delete"> -->
                    </form>
                </div>
            </div>
        </div>
        <script src="js/pickadate/picker.js"></script>
        <script src="js/pickadate/picker.date.js"></script>
        <script src="js/pickadate/legacy.js"></script>
        <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css">
        <script src="js/magnific-popup/jquery.magnific-popup.js"></script>
        <script>
            $('#date').pickadate({
                format: 'yyyy-mm-dd'
            });
            $(document).ready(function () {
                $('.image-link').magnificPopup({type: 'image'});
            });
        </script>
        <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>
        <script>CKEDITOR.replace('content', {});
        </script>
</body>
</html>
