<?php
session_start();
define("ADMIN_PATH","/admin");
$error = '';

if (isset($_POST["name"]) && isset($_POST["password"])) {

       $name = test_input($_POST["name"]);
       $password = test_input($_POST["password"]);
       include_once 'mysql.php';
       $mysql = new mysql();

       if ($mysql->Connection()) {
              $login = $mysql->Login($name, $password);
              if ($login > 0) {
                     $_SESSION["StateAdminId"] = $login;
                     $_SESSION["AdminName"] = $name;
                     $_SESSION["MasterCode"] = "2020";
					 // $amount = $_SESSION["StateAdminId"];
					 // echo "'<script>console.log(\"$amount\")</script>'";
                     header("Location: index.php");
              } else if ($login == 0) {
                     $error = "Wrong Password";
              } else if ($login == -1) {
                     $error = "Wrong Admin Name";
              }
       }
}

function test_input($data) {
       $data = trim($data);
       $data = stripslashes($data);
       $data = htmlspecialchars($data);
       return $data;
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>Login - KapitalBoost Admin</title>

              <?php include_once 'include.php'; ?>

              <link rel="stylesheet" href="<?php echo ADMIN_PATH ?>/css/login.css">

       </head>
       <body>

       <body role="document" style="background: #e9e5dc;overflow: hidden;">
              <!-------------------------------------------- content area -------------------------------------------->
              <div class="main-content-area-login-wrapper container-fluid">
                     <!-- Start login new -->
                     <form method="post" action="" id="form_login" name="form_login" class="form-horizontal" role="form">
                            <input type="hidden" name="action" value="login">
                            <input type="hidden" name="session" value="{$session}">

                            <div id="box" class="animated bounceIn">
                                   <div class="top" style="padding-bottom: 10px; line-height: 15px;">
                                          <a class="right_a btn-forgot-password" href="#" style="color: #eec104; font-family: Helvetica;"><i>Forgot Password?</i></a>

                                   </div>
                                   <div id="top_header" style="padding-bottom: 40px">
                                          <a href="#">
                                                 <img class="logo" src="https://kapitalboost.com/assets/images/logo/kapitalboost-logo.png" width="230" alt="logo">
                                          </a>
                                          <h5>
                                                 <i>Welcome to kapitalboost Admin!</i>
                                          </h5>
                                   </div>
                                   <div id="inputs">
                                          <div class="form-controlC" style="padding-bottom: 10px">
                                                 <input type="text" placeholder="Username" name="name" value="">
                                          </div>
                                          <div class="form-controlC" style="padding-bottom: 30px">
                                                 <input type="password" placeholder="Password" name="password">
                                          </div>
                                          <div id="bottom">
                                                 <input type="submit" value="Log In!" style="float: right; font-weight: bold" onclick="$('#form_login').submit();">

                                                 <div class="squared-check">
                                                        <input type="checkbox" id="remember" name="remember">
                                                        <label for="remember"></label>

                                                        <div class="cb-label">Remember me?</div>
                                                 </div>
                                          </div>

                                          <div>
                                                 <p style="color:red;text-align: center"><?= $error ?></p>
                                          </div>
                                   </div>
                            </div>
                     </form>




       </body>



</html>
