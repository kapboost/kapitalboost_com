<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();

echo 'hi';
if (isset($_POST["name"]) && isset($_POST["date"])) {
       $name = $_POST["name"];
       $date = $_POST["date"];
       $link = $_POST["link"];
       $des = $_POST["description"];
       $enable = $_POST["enable"];

       if ($mysql->Connection()) {
              $id = $mysql->AddNewEvent($name, $des, $link, $enable, $date);
       }
       if (isset($_FILES["image"]) && $_FILES["image"]["name"] != "") {
              $file = $_FILES["image"]["tmp_name"];
              $path = $_FILES["image"]["name"];
              $ext = pathinfo($path, PATHINFO_EXTENSION);
              $uploaddir = "../assets/images/events/";
              $image_name = rand(1, 10) . time() . "." . $ext;
              move_uploaded_file($file, $uploaddir . $image_name);
              if ($mysql->Connection()) {
                     $mysql->UpdateUploadedImgEvent($id, $image_name);
              }
       }
}

if ($mysql->Connection()) {
       list($eId, $eName, $eDate, $eImage, $eDes, $eLink, $eEnabled) = $mysql->GetEvents();
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin Events</title>

              <?php include_once 'include.php'; ?>


       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading">
                                                 <span class="header-panel">Overview</span>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <style>
                                                 #addIDiv{
                                                        background-color: #e6e6e6;border-radius: 10px;padding: 5px;margin:10px 20px 10px ;text-align: center
                                                 }
                                                 input{
                                                        margin: auto 15px;padding: 5px;
                                                 }
                                                 #name,#link{
                                                        width: 250px;
                                                 }
                                          </style>

                                          <div class="panel-body">

                                                 <div id="addIDiv" >
                                                        <form  method="POST" enctype="multipart/form-data">
                                                               <input id="name"  name="name" class="addNewIs" type="text" placeholder="Name (FRO)" />
                                                               <input id="date" type="date" name="date" placeholder="Event Date" />
                                                               <input id="link" type="text" name="link" placeholder="link" />
                                                               <input type="file" name="image"/>

                                                               <div><textarea style="width:100%;" rows="5" name="description"  placeholder="Description"></textarea></div>
                                                               <select name="enable">
                                                                      <option value="1" selected="">Enable</option>
                                                                      <option value="0">Disable</option>
                                                               </select>
                                                               <input type="submit" class="btn btn-success" value="Add New Event" />
                                                        </form>
                                                 </div>


                                                 <div class="table-responsive">
                                                        <table class="table table-striped table-bordered table-hover">
                                                               <thead>
                                                                      <tr>
                                                                             <th>No</th>
                                                                             <th>Event Name</th>
                                                                             <th>Event Date</th>
                                                                             <th >Event Image</th>
                                                                             <th >Event Description</th>
                                                                             <th >Link</th>
                                                                             <th >Enabled</th>
                                                                             <th >Edit</th>
                                                                      </tr>
                                                               </thead>
                                                               <tbody>
                                                                      <?php for ($i = 0; $i < count($eId); $i++) { ?>
                                                                             <tr class="even">
                                                                                    <td><?= $i + 1 ?></td>
                                                                                    <td><a href="events-edit.php?e=<?= $eId[$i] ?>" ><?= $eName[$i] ?></a></td>
                                                                                    <td><?= $eDate[$i] ?></td>

                                                                                    <td>
                                                                                           <a href="../Classes/phpThumb/phpThumb.php?src=../../assets/images/events/<?= $eImage[$i] ?>" class="image-link" ><img src="../Classes/phpThumb/phpThumb.php?src=../../assets/images/events/<?= $eImage[$i] ?>&w=150" alt=""></a>
                                                                                    </td>
                                                                                    <td><?= $eDes[$i] ?></td>
                                                                                    <td><a href="<?= $eLink[$i] ?>" target="_blank"><i class="fa fa-external-link-square fa-3x"></i></a></td>
                                                                                    <?php if ($eEnabled[$i] == 1) { ?>
                                                                                           <td style="color:white"><p style="background-color:green;padding:5px;">Enabled</p></td>
                                                                                    <?php } else { ?>
                                                                                           <td style="color:white"><p style="background-color:gray;padding:5px;">Disabled</p></td>
                                                                                    <?php } ?>
                                                                                    <td style="text-align: center"><button value="<?= $eId[$i] ?>" class="btn DelBtn"><i class="fa fa-trash" style="color:red"></i></button></td>
                                                                             </tr>

                                                                      <?php } ?>

                                                               </tbody>
                                                        </table>
                                                 </div>



                                          </div>
                                   </div>
                            </div>
                     </div>
              </div>


              <link rel="stylesheet" href="js/magnific-popup/magnific-popup.css"> 
              <script src="js/magnific-popup/jquery.magnific-popup.js"></script>

              <script src="js/pickadate/picker.js"></script>
              <script src="js/pickadate/picker.date.js"></script>
              <script>
                     $(document).ready(function () {
                            $('#date').pickadate({
                                   format: 'yyyy-mm-dd'
                            });

                            $('.image-link').magnificPopup({type: 'image'});

                            $(".DelBtn").click(function () {
                                   var index = $(this).parent().parent().index();
                                   var person = prompt("Please enter Master Code");
                                   $.post("delete.php", {event: $(this).val(), MC: person}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(500).fadeOut(100, function () {
                                                 if (reply === "Done") {
                                                        $("tr").eq(index + 1).remove();
                                                 }
                                          });
                                          $("#popupText").html(reply);
                                   });
                            });


                            /*$(".editBtn").on("click", function () {
                             var index = $(this).parent().parent().index();
                             
                             if (edit == 0) {
                             
                             $(this).children().removeClass("fa-edit");
                             $(this).children().addClass("fa-check-square-o");
                             
                             $("tr").eq(index + 1).animate({
                             height: "500px"
                             });
                             } else {
                             $(this).children().addClass("fa-edit");
                             $(this).children().removeClass("fa-check-square-o");
                             
                             $("tr").eq(index + 1).animate({
                             height: "200px"
                             });
                             }
                             });*/


                     });
              </script>

       </body>


</html>