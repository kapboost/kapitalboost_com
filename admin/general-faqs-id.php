<?php
include_once 'security.php';

include_once 'mysql.php';
$mysql = new mysql();
if ($mysql->Connection()) {
       list($pageHeaders, $pageContents, $releaseDate) = $mysql->GetGeneralFAQsId();
       $pageHeadersTopLevel = explode("+```+", $pageHeaders);
       $pageContentsTopLevel = explode("+```+", $pageContents);
       $pageHeadersAsme = explode("~~~", $pageHeadersTopLevel[0]);
       $pageContentsAsme = explode("~~~", $pageContentsTopLevel[0]);
       $pageHeadersAMember = explode("~~~", $pageHeadersTopLevel[1]);
       $pageContentsAMember = explode("~~~", $pageContentsTopLevel[1]);
       $pageHeadersAdonatioin = explode("~~~", $pageHeadersTopLevel[2]);
       $pageContentsAdonation = explode("~~~", $pageContentsTopLevel[2]);
       $pageHeadersAprivate = explode("~~~", $pageHeadersTopLevel[3]);
       $pageContentsAprivate = explode("~~~", $pageContentsTopLevel[3]);
       $pageHeadersAXfers = explode("~~~", $pageHeadersTopLevel[4]);
       $pageContentsAXfers = explode("~~~", $pageContentsTopLevel[4]);
}
?>

<!DOCTYPE html>
<html lang="en">
       <head>
              <?php include_once 'initialize.php'; ?>

              <title>KB Admin General Page FAQ</title>

              <?php include_once 'include.php'; ?>
              <script src="<?php echo ADMIN_PATH ?>/lib/ckeditor/ckeditor.js"></script>

              <script>
                     var headers = "";
                     var contents = "";
                     $(document).ready(function () {
                            $("#AddBtn1").on('click', function () {
                                   var headerInput = document.createElement("INPUT");
                                   headerInput.setAttribute("class", "headers1");
                                   var headerDiv = document.createElement("DIV");
                                   headerDiv.appendChild(headerInput);
                                   var contentInput = document.createElement("TEXTAREA");
                                   contentInput.setAttribute("class", "contents1");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea1");
                                   mainDiv.appendChild(headerDiv);
                                   mainDiv.appendChild(contentDiv);
                            });
                            $("#AddBtn2").on('click', function () {
                                   var headerInput = document.createElement("INPUT");
                                   headerInput.setAttribute("class", "headers2");
                                   var headerDiv = document.createElement("DIV");
                                   headerDiv.appendChild(headerInput);
                                   var contentInput = document.createElement("TEXTAREA");
                                   contentInput.setAttribute("class", "contents2");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea2");
                                   mainDiv.appendChild(headerDiv);
                                   mainDiv.appendChild(contentDiv);
                            });
                            $("#AddBtn3").on('click', function () {
                                   var headerInput = document.createElement("INPUT");
                                   headerInput.setAttribute("class", "headers3");
                                   var headerDiv = document.createElement("DIV");
                                   headerDiv.appendChild(headerInput);
                                   var contentInput = document.createElement("TEXTAREA");
                                   contentInput.setAttribute("class", "contents3");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea3");
                                   mainDiv.appendChild(headerDiv);
                                   mainDiv.appendChild(contentDiv);
                            });
                             $("#AddBtn4").on('click', function () {
                                   var headerInput = document.createElement("INPUT");
                                   headerInput.setAttribute("class", "headers4");
                                   var headerDiv = document.createElement("DIV");
                                   headerDiv.appendChild(headerInput);
                                   var contentInput = document.createElement("TEXTAREA");
                                   contentInput.setAttribute("class", "contents4");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea4");
                                   mainDiv.appendChild(headerDiv);
                                   mainDiv.appendChild(contentDiv);
                            });
                            $("#AddBtn5").on('click', function () {
                                   var headerInput = document.createElement("INPUT");
                                   headerInput.setAttribute("class", "headers5");
                                   var headerDiv = document.createElement("DIV");
                                   headerDiv.appendChild(headerInput);
                                   var contentInput = document.createElement("TEXTAREA");
                                   contentInput.setAttribute("class", "contents5");
                                   var contentDiv = document.createElement("DIV");
                                   contentDiv.appendChild(contentInput);
                                   var mainDiv = document.getElementById("contentArea5");
                                   mainDiv.appendChild(headerDiv);
                                   mainDiv.appendChild(contentDiv);
                            });

                            $(".UpdateBtns").on('click', function () {
                                   $(".headers1").each(function () {
                                          if ($(this).val() != "") {
                                                 headers += $(this).val() + "~~~";
                                          }
                                   });
                                   headers.slice(0, -3);
                                   headers += "+```+";
                                   $(".headers2").each(function () {
                                          if ($(this).val() != "") {
                                                 headers += $(this).val() + "~~~";
                                          }
                                   });
                                   headers.slice(0, -3);
                                   headers += "+```+";
                                   $(".headers3").each(function () {
                                          if ($(this).val() != "") {
                                                 headers += $(this).val() + "~~~";
                                          }
                                   });
                                   headers.slice(0, -3);
                                   headers += "+```+";
                                   $(".headers4").each(function () {
                                          if ($(this).val() != "") {
                                                 headers += $(this).val() + "~~~";
                                          }
                                   });
                                   headers.slice(0, -3);
                                   headers += "+```+";
                                   $(".headers5").each(function () {
                                          if ($(this).val() != "") {
                                                 headers += $(this).val() + "~~~";
                                          }
                                   });
                                   headers.slice(0, -3);
                                   $(".contents1").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });
                                   contents.slice(0, -3);
                                   contents += "+```+";
                                   $(".contents2").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });
                                   contents.slice(0, -3);
                                   contents += "+```+";
                                   $(".contents3").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });
                                   contents.slice(0, -3);
                                   contents += "+```+";
                                   $(".contents4").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });
                                   contents.slice(0, -3);
                                   contents += "+```+";
                                   $(".contents5").each(function () {
                                          if ($(this).val() != "") {
                                                 contents += $(this).val() + "~~~";
                                          }
                                   });
                                   contents.slice(0, -3);


                                   $.post("posts.php", {job: "updategeneralfaqsid", headers: headers, contents: contents}, function (reply) {
                                          reply = JSON.parse(reply);
                                          $("#popup").fadeIn(100).delay(2000).fadeOut(100);
                                          $("#popupText").html(reply);
                                          headers = "";
                                          contents = "";
                                   });
                            });



                            $("#smeDivBtn").on('click', function () {
                                   $("#smeDiv").fadeIn();
                                   $("#memberDiv").hide();
                                   $("#donationDiv").hide();
                                   $("#privateDiv").hide();
                                   $("#xfersDiv").hide();
                            });
                            $("#memberDivBtn").on('click', function () {
                                   $("#smeDiv").hide();
                                   $("#memberDiv").fadeIn();
                                   $("#donationDiv").hide();
                                   $("#privateDiv").hide();
                                   $("#xfersDiv").hide();
                            });
                            $("#donationDivBtn").on('click', function () {
                                   $("#smeDiv").hide();
                                   $("#memberDiv").hide();
                                   $("#donationDiv").fadeIn();
                                   $("#privateDiv").hide();
                                   $("#xfersDiv").hide();
                            });
                            $("#privateDivBtn").on('click', function () {
                                   $("#smeDiv").hide();
                                   $("#memberDiv").hide();
                                   $("#donationDiv").hide();
                                   $("#privateDiv").fadeIn();
                                   $("#xfersDiv").hide();
                            });
                            $("#xfersBtn").on('click',function(){
                                   $("#smeDiv").hide();
                                   $("#memberDiv").hide();
                                   $("#donationDiv").hide();
                                   $("#privateDiv").hide();
                                   $("#xfersDiv").fadeIn();
                            });
                     });
              </script>
              <style>
                     .headers1,.headers2,.headers3,.headers4,.headers5{
                            width: 95%;margin: 15px;padding:10px;border-radius: 10px;font-weight: bolder;font-size: 1.5em
                     }
                     .contents1,.contents2,.contents3,.contents4,.contents5{
                            width:  95%;
                            min-height: 150px;margin:15px;padding:10px;border-radius: 10px;font-size: 1.1em;background-color: #d7e6f4
                     }
                     #AddBtn1,#AddBtn2,#AddBtn3,#AddBtn4,#AddBtn5{
                            background-color: white;border: none;display: inline-block
                     }

              </style>
       </head>
       <body>
              <?php include_once 'header.php'; ?>
              <?php include_once 'popup.php'; ?>


              <div class="main-content-area-wrapper container-fluid"  style="min-height: 800px;">

                     <div class="row">
                            <div class="col-xs-12">
                                   <div class="general-panel panel">

                                          <div class="blue-panel-heading panel-heading" style="text-align:center">
                                                 <span class="header-panel">FAQs in Indonesian</span>
                                                 <button class="btn btn-info" id="smeDivBtn">SME</button>
                                                 <button class="btn btn-info" id="memberDivBtn">Financiers (Member)</button>
                                                 <button class="btn btn-info" id="donationDivBtn">Donation</button>
                                                 <button class="btn btn-info" id="privateDivBtn">Private</button>
                                                 <button class="btn btn-info" id="xfersBtn">e-wallet</button>
                                                 <div class="clearfix"></div>
                                          </div>

                                          <div id="smeDiv" class="panel-body" style="text-align:center">
                                                 <h1>SME Crowdfunding</h1>
                                                 <div id="contentArea1">
                                                        <?php for ($i = 0; $i < count($pageContentsAsme) -1; $i++) { ?>

                                                               <div><input class="headers1" value="<?= $pageHeadersAsme[$i] ?>" /></div>

                                                               <div><textarea name="smeContent" class="contents1" ><?= $pageContentsAsme[$i] ?></textarea></div>

                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn1"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>
                                                 <button class="btn btn-primary UpdateBtns"   style="margin-bottom: 20px">Update All</button>
                                          </div>
                                          
                                          <div id="memberDiv" class="panel-body" style="text-align:center;display:none;">
                                                 <h1>For Financier (Members)</h1>
                                                 <div id="contentArea2">
                                                        <?php for ($i = 0; $i < count($pageContentsAMember) -1; $i++) { ?>

                                                               <div><input class="headers2" value="<?= $pageHeadersAMember[$i] ?>" /></div>

                                                               <div><textarea name="memberContent" class="contents2" ><?= $pageContentsAMember[$i] ?></textarea></div>

                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn2"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>
                                                 <button class="btn btn-primary UpdateBtns"   style="margin-bottom: 20px">Update All</button>
                                          </div>

                                          <div id="donationDiv" class="panel-body" style="text-align:center;display:none;">
                                                 <h1>Donation Crowdfunding</h1>
                                                 <div id="contentArea3">
                                                        <?php for ($i = 0; $i < count($pageContentsAdonation) -1; $i++) { ?>

                                                               <div><input class="headers3" value="<?= $pageHeadersAdonatioin[$i] ?>" /></div>

                                                               <div><textarea class="contents3" ><?= $pageContentsAdonation[$i] ?></textarea></div>

                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn3"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>

                                                 <button class="btn btn-primary UpdateBtns"   style="margin-bottom: 20px">Update All</button>


                                          </div>

                                          <div id="privateDiv" class="panel-body" style="text-align:center;display:none;">
                                                 <h1>Private Crowdfunding</h1>
                                                 <div id="contentArea4">
                                                        <?php for ($i = 0; $i < count($pageContentsAprivate) -1; $i++) { ?>
                                                               <div><input class="headers4" value="<?= $pageHeadersAprivate[$i] ?>" /></div>
                                                               <div><textarea class="contents4" ><?= $pageContentsAprivate[$i] ?></textarea></div>
                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn4"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>
                                                 <button class="btn btn-primary UpdateBtns"  style="margin-bottom: 20px">Update All</button>
                                          </div>
                                          
                                          <div id="xfersDiv" class="panel-body" style="text-align:center;display:none;">
                                                 <h1>e-wallet</h1>
                                                 <div id="contentArea5">
                                                        <?php for ($i = 0; $i < count($pageContentsAXfers) -1; $i++) { ?>
                                                               <div><input class="headers5" value="<?= $pageHeadersAXfers[$i] ?>" /></div>
                                                               <div><textarea class="contents5" ><?= $pageContentsAXfers[$i] ?></textarea></div>
                                                        <?php } ?>
                                                 </div>
                                                 <button id="AddBtn5"><i  class="fa fa-plus-circle" style="color:green;font-size:3em;text-align: center"></i></button>
                                                 <button class="btn btn-primary UpdateBtns"  style="margin-bottom: 20px">Update All</button>
                                          </div>

                                   </div>
                            </div>
                     </div>
              </div>


              <script>

                     CKEDITOR.replace('contents1', {

                     });
              </script>

       </body>


</html>